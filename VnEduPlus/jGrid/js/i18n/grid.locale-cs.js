;(function($){
/**
 * jqGrid Czech Translation
 * Pavel Jirak pavel.jirak@jipas.cz
 * doplnil Thomas Wagner xwagne01@stud.fit.vutbr.cz
 * http://trirand.com/blog/ 
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
**/
$.jgrid = {
	defaults : {
		recordtext: "Zobrazeno {0} - {1} z {2} záznamů",
	    emptyrecords: "Nenalezeny žádné záznamy",
		loadtext: "Načítám...",
		pgtext : "Strana {0} z {1}"
	},
	search : {
		caption: "Vyhledávám...",
		Find: "Hledat",
		Reset: "Reset",
	    odata : ['rovno', 'nerovono', 'menší', 'menší nebo rovno','větší', 'větší nebo rovno', 'začíná s', 'nezačíná s', 'je v', 'není v', 'končí s', 'nekončí s', 'obahuje', 'neobsahuje'],
	    groupOps: [	{ op: "AND", text: "všech" },	{ op: "OR",  text: "některého z" }	],
		matchText: " hledat podle",
		rulesText: " pravidel"
	},
	edit : {
		addCaption: "Přidat záznam",
		editCaption: "Editace záznamu",
		bSubmit: "Uložit",
		bCancel: "Storno",
		bClose: "Zavřít",
		saveData: "Data byla změněna! Uložit změny?",
		bYes : "Ano",
		bNo : "Ne",
		bExit : "Zrušit",
		msg: {
		    required:"Pole je vyžadováno",
		    number:"Prosím, vložte validní číslo",
		    minValue:"hodnota musí být větší než nebo rovná ",
		    maxValue:"hodnota musí být menší než nebo rovná ",
		    email: "není validní e-mail",
		    integer: "Prosím, vložte celé číslo",
			date: "Prosím, vložte validní datum",
			url: "není platnou URL. Vyžadován prefix ('http://' or 'https://')",
			nodefined : " není definován!",
			novalue : " je vyžadována návratová hodnota!",
			customarray : "Custom function mělá vrátit pole!",
			customfcheck : "Custom function by měla být přítomna v případě custom checking!"
		}
	},
	view : {
	    caption: "Zobrazit záznam",
	    bClose: "Zavřít"
	},
	del : {
		caption: "Smazat",
		msg: "Smazat vybraný(é) z��������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������:i:sO",
		        YearMonth: "F, Y"
		    },
		    reformatAfterEdit : false
		},
		baseLinkUrl: '',
		showAction: '',
	    target: '',
	    checkbox : {disabled:true},
		idName : 'id'
	}
};
})(jQuery);
