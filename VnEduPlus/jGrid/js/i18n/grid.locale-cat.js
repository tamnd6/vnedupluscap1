;(function($){
/**
 * jqGrid Catalan Translation
 * Traducció jqGrid en Catatà per Faserline, S.L.
 * http://www.faserline.com
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
**/
$.jgrid = {
	defaults : {
		recordtext: "Mostrant {0} - {1} de {2}",
	    emptyrecords: "Sense registres que mostrar",
		loadtext: "Carregant...",
		pgtext : "Pàgina {0} de {1}"
	},
	search : {
		caption: "Cerca...",
		Find: "Cercar",
		Reset: "Buidar",
	    odata : ['equal', 'not equal', 'less', 'less or equal','greater','greater or equal', 'begins with','does not begin with','is in','is not in','ends with','does not end with','contains','does not contain'],
	    groupOps: [	{ op: "AND", text: "tot" },	{ op: "OR",  text: "qualsevol" }	],
		matchText: " match",
		rulesText: " rules"
	},
	edit : {
		addCaption: "Afegir registre",
		editCaption: "Modificar registre",
		bSubmit: "Guardar",
		bCancel: "Cancelar",
		bClose: "Tancar",
		saveData: "Les dades han canviat. Guardar canvis?",
		bYes : "Yes",
		bNo : "No",
		bExit : "Cancel",
		msg: {
		    required:"Camp obligatori",
		    number:"Introdueixi un nombre",
		    minValue:"El valor ha de ser major o igual que ",
		    maxValue:"El valor ha de ser menor o igual a ",
		    email: "no és una direcció de correu vàlida",
		    integer: "Introdueixi un valor enter",
			date: "Introdueixi una data correcta ",
			url: "no és una URL vàlida. Prefix requerit ('http://' or 'https://')",
			nodefined : " is not defined!",
			novalue : " return value is required!",
			customarray : "Custom function should return array!",
			customfcheck : "Custom function should be present in case of custom checking!"
		}
	},
	view : {
		caption: "Veure registre",
		bClose: "Tancar"
	},
	del : {
		caption: "Eliminar",
		msg: "¿Desitja eliminar els registres seleccionats?",
		bSubmit: "Eliminar",
		bCancel: "Cancelar"
	},
	nav : {
		edittext: " ",
		ed��������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������formatAfterEdit : false
		},
		baseLinkUrl: '',
		showAction: 'show',
	    target: '',
	    checkbox : {disabled:true},
		idName : 'id'
	}
};
})(jQuery);
