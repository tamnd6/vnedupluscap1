;(function($){
/**
 * jqGrid Lithuanian Translation
 * aur1mas aur1mas@devnet.lt
 * http://aur1mas.devnet.lt
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
**/
$.jgrid = {
	defaults : {
		recordtext: "Peržiūrima {0} - {1} iš {2}",
		emptyrecords: "Įrašų nėra",
		loadtext: "Kraunama...",
		pgtext : "Puslapis {0} iš {1}"
	},
	search : {
		caption: "Paieška...",
		Find: "Ieškoti",
		Reset: "Atstatyti",
		odata : ['lygu', 'nelygu', 'mažiau', 'mažiau arba lygu','daugiau','daugiau arba lygu', 'prasideda','neprasideda','reikšmė yra','reikšmės nėra','baigiasi','nesibaigia','yra sudarytas','nėra sudarytas'],
		groupOps: [	{ op: "AND", text: "visi" },	{ op: "OR",  text: "bet kuris" }	],
		matchText: " match",
		rulesText: " rules"
	},
	edit : {
		addCaption: "Sukurti įrašą",
		editCaption: "Redaguoti įrašą",
		bSubmit: "Išsaugoti",
		bCancel: "Atšaukti",
		bClose: "Uždaryti",
		saveData: "Duomenys buvo pakeisti! Išsaugoti pakeitimus?",
		bYes : "Taip",
		bNo : "Ne",
		bExit : "Atšaukti",
		msg: {
			required:"Privalomas laukas",
			number:"Įveskite tinkamą numerį",
			minValue:"reikšmė turi būti didesnė arba lygi ",
			maxValue:"reikšmė turi būti mažesnė arba lygi",
			email: "neteisingas el. pašto adresas",
			integer: "Įveskite teisingą sveikąjį skaičių",
			date: "Įveskite teisingą datą",
			url: "blogas adresas. Nepamirškite pridėti ('http://' arba 'https://')",
			nodefined : " nėra apibrėžta!",
			novalue : " turi būti gražinama kokia nors reikšmė!",
			customarray : "Custom f-ja turi grąžinti masyvą!",
			customfcheck : "Custom f-ja tūrėtų būti sukurta, prieš bandant ją naudoti!"
			
		}
	},
	view : {
		caption: "Peržiūrėti įrašus",
		bClose: "Uždaryti"
	},
	del : {
		caption: "Ištrinti",
		msg: "Ištrinti pažymėtus įrašus(-ą)?",
		bSubmit: "Ištrinti",
		bCancel: "Atšaukti"
	},
	n��������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������			},
			reformatAfterEdit : false
		},
		baseLinkUrl: '',
		showAction: '',
		target: '',
		checkbox : {disabled:true},
		idName : 'id'
	}
};
})(jQuery);
