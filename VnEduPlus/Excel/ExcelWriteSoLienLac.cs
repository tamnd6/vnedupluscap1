﻿using System.Collections.Generic;
using VnEduPlus.Models;
using System.Linq;
using System;

namespace VnEduPlus
{
    public class ExcelWriteSoLienLac : ExcelFileWriterSoLienLac<object>
    {
        public object[,] myExcelData;
        private int myRowCnt;
 
        private int startRow = 2;
        

        string ColumnName(int index)
        {
            char[] chars = new char[] { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };

            int quotient = index / 26;
            if (quotient > 0)
                return ColumnName(quotient-1) + chars[index % 26].ToString();
            else
                return chars[index % 26].ToString();
        }


        string ColumnNames(int index)
        {
            string[] chars = new string[] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z","AA","AB","AC" };
           /*  if (index <= 0) {
                return chars[index].ToString();
            }  else return chars[index - 1].ToString();
           */

            int quotient = index / 29;
            if (quotient > 0)
                return ColumnNames(quotient - 1) + chars[index % 29].ToString();
            else
                return chars[index % 29].ToString();
            

        }

        public override void FillSoLienLacHocSinhToExcell(List<SoLienLac> SoLienLac)
        {
            string lop = "";
            string siso = "";
        }

        public override void WriteDiemSoLienLacForOneDayDuPerformance(System.Data.DataRow[] dr, string thoigian, string namhoc, string tenHS, string tengiaovien)
        {
            string lop = "";
            string siso = "";
            lop = dr[0]["TenLop"].ToString();
            siso = dr[0]["SiSo"].ToString();
            int RowCount = 100;
            myRowCnt = 32; //list.Count;
            myExcelData = new object[RowCount, ColumnCount];

            myExcelData[0, 0] = "PHIẾU LIÊN LẠC  " + thoigian + "  NĂM HỌC  " + namhoc;
            BoldCell(ColumnNames(0) + 2, ColumnNames(28) + 2, true);
            BorderRow(ColumnNames(0) + 2, ColumnNames(28) + 2);
            setFontAndSize(ColumnNames(0) + 2, "Arial", 10);

            myExcelData[1, 0] = "Họ và tên học sinh:";
            BoldCell(ColumnNames(0) + 3, ColumnNames(0) + 3, true);// A3 cell
            setFontAndSize(ColumnNames(0) + 3, "Arial", 10);

            myExcelData[1, 1] = tenHS;
            BoldCell(ColumnNames(1) + 3, ColumnNames(9) + 3, true);// A1--> B1

            myExcelData[1, 15] = "Lớp: ";
            BoldCell(ColumnNames(15) + 3, ColumnNames(15) + 3, true);// A1--> B1
            setFontAndSize(ColumnNames(15) + 3, "Arial", 10);

            myExcelData[1, 19] = " " + lop.ToString();
            BoldCell(ColumnNames(19) + 3, ColumnNames(19) + 3, true);

            myExcelData[1, 20] = "Sĩ số: ";
            BoldCell(ColumnNames(20) + 3, ColumnNames(20) + 3, true);// A1--> B1
            setFontAndSize(ColumnNames(20) + 3, "Arial", 10);

            myExcelData[1, 21] = siso;
            BoldCell(ColumnNames(21) + 3, ColumnNames(21) + 3, true);// A1--> B1

            myExcelData[2, 0] = "Môn học";
            BoldCell(ColumnNames(0) + 4, ColumnNames(0) + 5, true);
            setWidth(ColumnNames(0) + 4, 17);// set width for  A colum is 17 pixel
            BorderRow(ColumnNames(0) + 4, ColumnNames(0) + 5);
            setFontAndSize(ColumnNames(0) + 4, "Arial", 10);

            myExcelData[2, 1] = "Điểm";
            BoldCell(ColumnNames(1) + 4, ColumnNames(25) + 4, true);
            BorderRow(ColumnNames(1) + 4, ColumnNames(25) + 4);
            setFontAndSize(ColumnNames(1) + 4, "Arial", 10);

            myExcelData[2, 26] = "HK";
            BoldCell(ColumnNames(26) + 4, ColumnNames(26) + 5, true);
            BorderRow(ColumnNames(26) + 4, ColumnNames(26) + 5);
            setFontAndSize(ColumnNames(26) + 4, "Arial", 10);

            myExcelData[2, 27] = "TBHK";
            BoldCell(ColumnNames(27) + 4, ColumnNames(27) + 5, true);
            BorderRow(ColumnNames(27) + 4, ColumnNames(27) + 5);
            setFontAndSize(ColumnNames(27) + 4, "Arial", 10);

            myExcelData[2, 28] = "Ghi chú";
            BoldCell(ColumnNames(28) + 4, ColumnNames(28) + 5, true);
            BorderRow(ColumnNames(28) + 4, ColumnNames(28) + 5);
            setFontAndSize(ColumnNames(28) + 4, "Arial", 10);

            myExcelData[3, 1] = "Miệng";
            BoldCell(ColumnNames(1) + 5, ColumnNames(10) + 5, true);
            BorderRow(ColumnNames(1) + 5, ColumnNames(10) + 5);
            setFontAndSize(ColumnNames(1) + 5, "Arial", 10);

            myExcelData[3, 11] = "15 phút";
            BoldCell(ColumnNames(11) + 5, ColumnNames(15) + 5, true);
            BorderRow(ColumnNames(11) + 5, ColumnNames(15) + 5);
            setFontAndSize(ColumnNames(11) + 5, "Arial", 10);

            myExcelData[3, 16] = "1 Tiết";
            BoldCell(ColumnNames(16) + 5, ColumnNames(25) + 5, true);
            BorderRow(ColumnNames(16) + 5, ColumnNames(25) + 5);
            setFontAndSize(ColumnNames(16) + 5, "Arial", 10);

            int rowsHead = 4;// in content header rowss
            int rowsHeadExcell = 0;
            for (int j = 0; j < dr.Count(); j++)
            {
                // kiem tra xem mon hoc co phai la mon tieng viet khong  ?
                int socot = 0;
                rowsHeadExcell = rowsHead + 2;// do dinh dang cua excell la so dong them 2 dong 
                myExcelData[rowsHead, socot] = dr[j]["TenMonHoc"].ToString();
                MergeCell(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell, true);
                BorderRow(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell);
                socot++;

                MergeCell(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell, true);
                BorderRow(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = dr[j]["M1"].ToString();
                setWidth(ColumnNames(socot) + rowsHeadExcell, 2.57);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 8);

                socot++;
                MergeCell(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell, true);
                BorderRow(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = dr[j]["M2"].ToString();
                setWidth(ColumnNames(socot) + rowsHeadExcell, 2.57);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 8);

                socot++;
                MergeCell(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell, true);
                BorderRow(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = dr[j]["M3"].ToString();
                setWidth(ColumnNames(socot) + rowsHeadExcell, 2.57);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 8);

                socot++;
                MergeCell(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell, true);
                BorderRow(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = dr[j]["M4"].ToString();
                setWidth(ColumnNames(socot) + rowsHeadExcell, 2.57);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 8);

                socot++;
                MergeCell(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell, true);
                BorderRow(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = dr[j]["M5"].ToString();
                setWidth(ColumnNames(socot) + rowsHeadExcell, 2.57);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 8);

                socot++;
                MergeCell(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell, true);
                BorderRow(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = dr[j]["V15P1"].ToString();
                setWidth(ColumnNames(socot) + rowsHeadExcell, 2.57);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 8);

                socot++;
                MergeCell(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell, true);
                BorderRow(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = dr[j]["V15P2"].ToString();
                setWidth(ColumnNames(socot) + rowsHeadExcell, 2.57);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 8);

                socot++;
                MergeCell(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell, true);
                BorderRow(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = dr[j]["TH15P3"].ToString();
                setWidth(ColumnNames(socot) + rowsHeadExcell, 2.57);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 8);

                socot++;
                MergeCell(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell, true);
                BorderRow(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = dr[j]["TH15P4"].ToString();
                setWidth(ColumnNames(socot) + rowsHeadExcell, 2.57);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 8);

                socot++;
                MergeCell(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell, true);
                BorderRow(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = dr[j]["TH15P5"].ToString();
                setWidth(ColumnNames(socot) + rowsHeadExcell, 2.57);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 8);

                socot++;
                MergeCell(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell, true);
                BorderRow(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = dr[j]["V15P3"].ToString();
                setWidth(ColumnNames(socot) + rowsHeadExcell, 2.57);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 8);

                socot++;
                MergeCell(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell, true);
                BorderRow(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = dr[j]["V15P4"].ToString();
                setWidth(ColumnNames(socot) + rowsHeadExcell, 2.57);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 8);

                socot++;
                MergeCell(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell, true);
                BorderRow(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = dr[j]["V15P5"].ToString();
                setWidth(ColumnNames(socot) + rowsHeadExcell, 2.57);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 8);

                socot++;
                MergeCell(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell, true);
                BorderRow(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = dr[j]["TH15P1"].ToString();
                setWidth(ColumnNames(socot) + rowsHeadExcell, 2.57);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 8);

                socot++;
                MergeCell(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell, true);
                BorderRow(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = dr[j]["TH15P1"].ToString();
                setWidth(ColumnNames(socot) + rowsHeadExcell, 2.57);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 8);

                socot++;
                MergeCell(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell, true);
                BorderRow(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = dr[j]["V45P1"].ToString();
                setWidth(ColumnNames(socot) + rowsHeadExcell, 2.57);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 8);

                socot++;
                MergeCell(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell, true);
                BorderRow(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = dr[j]["V45P2"].ToString();
                setWidth(ColumnNames(socot) + rowsHeadExcell, 2.57);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 8);

                socot++;
                MergeCell(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell, true);
                BorderRow(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = dr[j]["V45P3"].ToString();
                setWidth(ColumnNames(socot) + rowsHeadExcell, 2.57);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 8);

                socot++;
                MergeCell(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell, true);
                BorderRow(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = dr[j]["V45P4"].ToString();
                setWidth(ColumnNames(socot) + rowsHeadExcell, 2.57);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 8);

                socot++;
                MergeCell(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell, true);
                BorderRow(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = dr[j]["V45P5"].ToString();
                setWidth(ColumnNames(socot) + rowsHeadExcell, 2.57);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 8);

                socot++;
                MergeCell(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell, true);
                BorderRow(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = dr[j]["TH45P1"].ToString();
                setWidth(ColumnNames(socot) + rowsHeadExcell, 2.57);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 8);
                socot++;

                MergeCell(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell, true);
                BorderRow(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = dr[j]["TH45P2"].ToString();
                setWidth(ColumnNames(socot) + rowsHeadExcell, 2.57);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 8);

                socot++;
                MergeCell(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell, true);
                BorderRow(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = dr[j]["TH45P3"].ToString();
                setWidth(ColumnNames(socot) + rowsHeadExcell, 2.57);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 8);

                socot++;
                MergeCell(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell, true);
                BorderRow(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = dr[j]["TH45P4"].ToString();
                setWidth(ColumnNames(socot) + rowsHeadExcell, 2.57);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 8);

                socot++;
                MergeCell(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell, true);
                BorderRow(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = dr[j]["TH45P5"].ToString();
                setWidth(ColumnNames(socot) + rowsHeadExcell, 2.57);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 8);

                socot++;
                MergeCell(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell, true);
                BorderRow(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = dr[j]["HK"].ToString();
                setWidth(ColumnNames(socot) + rowsHeadExcell, 2.57);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 8);

                socot++;
                MergeCell(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell, true);
                BorderRow(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = dr[j]["TBM"].ToString();
                setWidth(ColumnNames(socot) + rowsHeadExcell, 2.57);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 8);

                socot++;
                MergeCell(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell, true);
                BorderRow(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = "";
                rowsHead = rowsHead + 1;
            }

            // tang len mot dong chuan bị in ra dữ liệu khác 
            rowsHead++;
            rowsHeadExcell = rowsHead + 2;
            int columnx = 15;
            BoldCell(ColumnNames(columnx) + rowsHeadExcell, ColumnNames(columnx + 5) + rowsHeadExcell, true);
            //BorderRow(ColumnName(columnx) + rowsHeadExcell, ColumnName(columnx+5) + rowsHeadExcell);
            myExcelData[rowsHead, columnx] = "Nhận xét của giáo viên chủ nhiệm";
            setFontAndSize(ColumnNames(columnx) + rowsHeadExcell, "Arial", 10);
            // chuan bi in nhung du lieu tiếp theo
            rowsHead++;
            rowsHeadExcell = rowsHead + 2;
            int columtk = 0;
            BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk + 1) + rowsHeadExcell, true);
            BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk + 1) + (rowsHeadExcell + 1));
            myExcelData[rowsHead, columtk] = "Điểm trung bình";
            setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

            // rowsHeadExcell++;
            columtk = columtk + 2;
            BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk + 4) + rowsHeadExcell, true);
            BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk + 4) + (rowsHeadExcell + 1));
            myExcelData[rowsHead, columtk] = "Danh Hiệu";
            setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);
            // rowsHeadExcell++;
            columtk = columtk + 5;
            BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk + 1) + rowsHeadExcell, true);
            BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk + 1) + (rowsHeadExcell + 1));
            myExcelData[rowsHead, columtk] = "Xếp hạng";
            setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);
            // rowsHeadExcell++;
            columtk = columtk + 2;
            BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk + 1) + rowsHeadExcell, true);
            BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk + 1) + rowsHeadExcell);
            myExcelData[rowsHead, columtk] = "Xếp loại";
            setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

            BoldCell(ColumnNames(columtk) + (rowsHeadExcell + 1), ColumnNames(columtk) + (rowsHeadExcell + 1), false);
            BorderRow(ColumnNames(columtk) + (rowsHeadExcell + 1), ColumnNames(columtk) + (rowsHeadExcell + 1));
            myExcelData[rowsHead + 1, columtk] = "HL";
            setFontAndSize(ColumnNames(columtk) + (rowsHeadExcell + 1), "Arial", 10);
            columtk++;
            BoldCell(ColumnNames(columtk) + (rowsHeadExcell + 1), ColumnNames(columtk) + (rowsHeadExcell + 1), false);
            BorderRow(ColumnNames(columtk) + (rowsHeadExcell + 1), ColumnNames(columtk) + (rowsHeadExcell + 1));
            myExcelData[rowsHead + 1, columtk] = "HK";
            setFontAndSize(ColumnNames(columtk) + (rowsHeadExcell + 1), "Arial", 10);

            // rowsHeadExcell++;
            columtk = columtk + 1;
            BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk + 1) + rowsHeadExcell, true);
            BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk + 1) + rowsHeadExcell);
            myExcelData[rowsHead, columtk] = "Vắng";
            setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

            BoldCell(ColumnNames(columtk) + (rowsHeadExcell + 1), ColumnNames(columtk) + (rowsHeadExcell + 1), false);
            BorderRow(ColumnNames(columtk) + (rowsHeadExcell + 1), ColumnNames(columtk) + (rowsHeadExcell + 1));
            myExcelData[rowsHead + 1, columtk] = "P";
            setFontAndSize(ColumnNames(columtk) + (rowsHeadExcell + 1), "Arial", 10);
            columtk = columtk + 1;
            BoldCell(ColumnNames(columtk) + (rowsHeadExcell + 1), ColumnNames(columtk) + (rowsHeadExcell + 1), false);
            BorderRow(ColumnNames(columtk) + (rowsHeadExcell + 1), ColumnNames(columtk) + (rowsHeadExcell + 1));
            myExcelData[rowsHead + 1, columtk] = "K";
            setFontAndSize(ColumnNames(columtk) + (rowsHeadExcell + 1), "Arial", 10);

            // in ket qua cho phan nay  
            if (thoigian == "HK1")
            {
                rowsHead = rowsHead + 2;
                rowsHeadExcell = rowsHead + 2;
                columtk = 0;

                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);
                myExcelData[rowsHead, columtk] = thoigian;
                columtk++;

                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);
                myExcelData[rowsHead, columtk] = dr[0]["TBHK1"].ToString();


                // rowsHeadExcell++;
                columtk = columtk + 1;
                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk + 4) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk + 4) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = dr[0]["DanhHieuK1"].ToString();
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                columtk = columtk + 5;
                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk + 1) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk + 1) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = "";
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                columtk = columtk + 2;

                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = dr[0]["XLHLK1"].ToString();//"HL";
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                columtk = columtk + 1;
                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = dr[0]["HKHK1"].ToString();//            "HK";
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                columtk = columtk + 1;

                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = dr[0]["VangCoPhep"].ToString();
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);
                columtk = columtk + 1;
                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = dr[0]["VangKoPhep"].ToString();
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);
            }
            else
            {

                ////---- in thong tin ca nam  -------------------------------------------------------------
                // hoc ky 2
                rowsHead = rowsHead + 2;
                rowsHeadExcell = rowsHead + 2;
                columtk = 0;

                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);
                myExcelData[rowsHead, columtk] = "HK1";
                columtk++;

                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);
                myExcelData[rowsHead, columtk] = dr[0]["TBHK1"].ToString();


                // rowsHeadExcell++;
                columtk = columtk + 1;
                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk + 4) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk + 4) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = dr[0]["DanhHieuK1"].ToString();
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                columtk = columtk + 5;
                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk + 1) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk + 1) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = "";
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                columtk = columtk + 2;

                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = dr[0]["XLHLK1"].ToString();//"HL";
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                columtk = columtk + 1;
                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = dr[0]["HKHK1"].ToString();//            "HK";
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                columtk = columtk + 1;

                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = dr[0]["VangCoPhep"].ToString();
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);
                columtk = columtk + 1;
                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = dr[0]["VangKoPhep"].ToString();
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                columtk = 0;
                rowsHeadExcell++;
                rowsHead++;

                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);
                myExcelData[rowsHead, columtk] = thoigian;
                columtk++;

                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);
                myExcelData[rowsHead, columtk] = dr[0]["TBHK2"].ToString();


                // rowsHeadExcell++;
                columtk = columtk + 1;
                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk + 4) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk + 4) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = dr[0]["DanhHieuK2"].ToString();
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                columtk = columtk + 5;
                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk + 1) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk + 1) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = "";
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                columtk = columtk + 2;

                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = dr[0]["XLHLK2"].ToString();//"HL";
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                columtk = columtk + 1;
                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = dr[0]["HKHK2"].ToString();//            "HK";
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                columtk = columtk + 1;

                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = dr[0]["VangCoPhep"].ToString() == "-1" ? "" : dr[0]["VangCoPhep"].ToString().ToString();
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);
                columtk = columtk + 1;
                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = dr[0]["VangKoPhep"].ToString() == "-1" ? "" : dr[0]["VangKoPhep"].ToString().ToString();
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                // thong tin ca nam

                columtk = 0;
                rowsHeadExcell++;
                rowsHead++;

                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);
                myExcelData[rowsHead, columtk] = "Cả năm";
                columtk++;

                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);
                myExcelData[rowsHead, columtk] = dr[0]["TBCN"].ToString();


                // rowsHeadExcell++;
                columtk = columtk + 1;
                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk + 4) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk + 4) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = dr[0]["DanhHieuCN"].ToString();
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                columtk = columtk + 5;
                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk + 1) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk + 1) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = "";
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                columtk = columtk + 2;

                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = dr[0]["XLHLCN"].ToString();//"HL";
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                columtk = columtk + 1;
                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = dr[0]["HKCN"].ToString();//            "HK";
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                columtk = columtk + 1;

                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = dr[0]["VangCoPhep"].ToString() == "-1" ? "" : dr[0]["VangCoPhep"].ToString().ToString();
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);
                columtk = columtk + 1;
                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = dr[0]["VangKoPhep"].ToString() == "-1" ? "" : dr[0]["VangKoPhep"].ToString().ToString();
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

            }   

            // in y kien cua phu huynh hoc sinh

            rowsHead += 3;
            rowsHeadExcell = rowsHead + 2;
            columtk = 0;
            BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
            myExcelData[rowsHead, columtk] = "* Ý kiến và chữ ký của phụ huynh:";
            setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

            rowsHead = rowsHead + 5;
            rowsHeadExcell = rowsHead + 2;
            BoldCell(ColumnNames(columtk + 17) + rowsHeadExcell, ColumnNames(columtk + 20) + rowsHeadExcell, true);

            myExcelData[rowsHead, columtk + 17] = tengiaovien;

        }

        public override void WriteDiemSoLienLacForOneThuGonPerformance(System.Data.DataRow[] dr, string thoigian, string namhoc, string tenHS, string tengiaovien)
        {
            string lop = "";
            string siso = "";
            lop = dr[0]["TenLop"].ToString();
            siso = dr[0]["SiSo"].ToString();

            myRowCnt = 30; //list.Count;
            int RowCount = 100;
            myExcelData = new object[RowCount, ColumnCount];

            myExcelData[0, 0] = "PHIẾU LIÊN LẠC  " + thoigian + "  NĂM HỌC  " + namhoc;
            BoldCell(ColumnName(0) + 2, ColumnName(18) + 2, true);
            BorderRow(ColumnName(0) + 2, ColumnName(18) + 2);

            myExcelData[1, 0] = "Họ và tên học sinh:";
            BoldCell(ColumnName(0) + 3, ColumnName(0) + 3, true);// A1--> B1
            myExcelData[1, 2] = tenHS;
            BoldCell(ColumnName(2) + 3, ColumnName(9) + 3, true);// A1--> B1
            myExcelData[1, 10] = "Lớp: ";
            BoldCell(ColumnName(10) + 3, ColumnName(10) + 3, true);// A1--> B1
            myExcelData[1, 11] = " " + lop.ToString();
            BoldCell(ColumnName(11) + 3, ColumnName(11) + 3, true);
            myExcelData[1, 12] = "Sĩ số: ";
            BoldCell(ColumnName(12) + 3, ColumnName(12) + 3, true);// A1--> B1
            myExcelData[1, 13] = siso;
            BoldCell(ColumnName(13) + 3, ColumnName(13) + 3, true);// A1--> B1

            myExcelData[2, 0] = "Môn học";
            BoldCell(ColumnName(0) + 4, ColumnName(0) + 5, true);
            BorderRow(ColumnName(0) + 4, ColumnName(0) + 5);
            myExcelData[2, 1] = "Điểm";
            BoldCell(ColumnName(1) + 4, ColumnName(15) + 4, true);
            BorderRow(ColumnName(1) + 4, ColumnName(15) + 4);

            myExcelData[2, 16] = "HK";
            BoldCell(ColumnName(16) + 4, ColumnName(16) + 5, true);
            BorderRow(ColumnName(16) + 4, ColumnName(16) + 5);

            myExcelData[2, 17] = "TBHK";
            BoldCell(ColumnName(17) + 4, ColumnName(17) + 5, true);
            BorderRow(ColumnName(17) + 4, ColumnName(17) + 5);

            myExcelData[2, 18] = "Ghi chú";
            BoldCell(ColumnName(18) + 4, ColumnName(18) + 5, true);
            BorderRow(ColumnName(18) + 4, ColumnName(18) + 5);

            myExcelData[3, 1] = "Miệng";
            BoldCell(ColumnName(1) + 5, ColumnName(5) + 5, true);
            BorderRow(ColumnName(1) + 5, ColumnName(5) + 5);

            myExcelData[3, 6] = "15 phút";
            BoldCell(ColumnName(6) + 5, ColumnName(10) + 5, true);
            BorderRow(ColumnName(6) + 5, ColumnName(10) + 5);


            myExcelData[3, 11] = "1 Tiết";
            BoldCell(ColumnName(11) + 5, ColumnName(15) + 5, true);
            BorderRow(ColumnName(11) + 5, ColumnName(15) + 5);

            int rowsHead = 4;// in content header rowss
            int rowsHeadExcell = 0;

            for (int j = 0; j < dr.Count(); j++)
            {
                // kiem tra xem mon hoc co phai la mon tieng viet khong  ?
                int socot = 0;
                rowsHeadExcell = rowsHead + 2;// do dinh dang cua excell la so dong them 2 dong 
                myExcelData[rowsHead, socot] = dr[j]["TenMonHoc"].ToString();
                setWidth(ColumnNames(socot) + rowsHeadExcell, 3);
                MergeCell(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell, true);
                BorderRow(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell);

                socot++;
                MergeCell(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell, true);
                BorderRow(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = dr[j]["M1"].ToString();
                setWidth(ColumnNames(socot) + rowsHeadExcell, 3);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 10);

                socot++;
                MergeCell(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell, true);
                BorderRow(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = dr[j]["M2"].ToString();
                setWidth(ColumnNames(socot) + rowsHeadExcell, 3);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 10);

                socot++;
                MergeCell(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell, true);
                BorderRow(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = dr[j]["M3"].ToString();
                setWidth(ColumnNames(socot) + rowsHeadExcell, 3);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 10);

                socot++;
                MergeCell(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell, true);
                BorderRow(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = dr[j]["M4"].ToString();
                setWidth(ColumnNames(socot) + rowsHeadExcell, 3);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 10);

                socot++;
                MergeCell(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell, true);
                BorderRow(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = dr[j]["M5"].ToString();
                setWidth(ColumnNames(socot) + rowsHeadExcell, 3);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 10);

                socot++;
                MergeCell(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell, true);
                BorderRow(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = dr[j]["V15P3"].ToString();
                setWidth(ColumnNames(socot) + rowsHeadExcell, 3);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 10);

                socot++;
                MergeCell(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell, true);
                BorderRow(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = dr[j]["V15P4"].ToString();
                setWidth(ColumnNames(socot) + rowsHeadExcell, 3);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 10);

                socot++;
                MergeCell(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell, true);
                BorderRow(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = dr[j]["V15P5"].ToString();
                setWidth(ColumnNames(socot) + rowsHeadExcell, 3);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 10);

                socot++;
                MergeCell(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell, true);
                BorderRow(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = dr[j]["TH15P1"].ToString();
                setWidth(ColumnNames(socot) + rowsHeadExcell, 3);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 10);

                socot++;
                MergeCell(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell, true);
                BorderRow(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = dr[j]["TH15P2"].ToString();
                setWidth(ColumnNames(socot) + rowsHeadExcell, 3);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 10);

                socot++;
                MergeCell(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell, true);
                BorderRow(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = dr[j]["V45P1"].ToString();
                setWidth(ColumnNames(socot) + rowsHeadExcell, 3);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 10);

                socot++;
                MergeCell(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell, true);
                BorderRow(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = dr[j]["V45P2"].ToString();
                setWidth(ColumnNames(socot) + rowsHeadExcell, 3);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 10);

                socot++;
                MergeCell(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell, true);
                BorderRow(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = dr[j]["V45P3"].ToString();
                setWidth(ColumnNames(socot) + rowsHeadExcell, 3);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 10);

                socot++;
                MergeCell(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell, true);
                BorderRow(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = dr[j]["V45P4"].ToString();
                setWidth(ColumnNames(socot) + rowsHeadExcell, 3);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 10);

                socot++;
                MergeCell(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell, true);
                BorderRow(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = dr[j]["V45P5"].ToString();
                setWidth(ColumnNames(socot) + rowsHeadExcell, 3);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 10);

                socot++;
                MergeCell(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell, true);
                BorderRow(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = dr[j]["HK"].ToString();
                setWidth(ColumnNames(socot) + rowsHeadExcell, 3);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 10);

                socot++;
                MergeCell(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell, true);
                BorderRow(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = dr[j]["TBM"].ToString();
                setWidth(ColumnNames(socot) + rowsHeadExcell, 3);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 10);

                socot++;
                MergeCell(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell, true);
                BorderRow(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = "";
                rowsHead = rowsHead + 1;
            }

            // tang len mot dong chuan bị in ra dữ liệu khác 
            // tang len mot dong chuan bị in ra dữ liệu khác 
            rowsHead++;
            rowsHeadExcell = rowsHead + 2;
            int columnx = 12;
            BoldCell(ColumnName(columnx) + rowsHeadExcell, ColumnName(columnx + 5) + rowsHeadExcell, true);
            //BorderRow(ColumnName(columnx) + rowsHeadExcell, ColumnName(columnx+5) + rowsHeadExcell);
            myExcelData[rowsHead, columnx] = "Nhận xét của giáo viên chủ nhiệm";

            // chuan bi in nhung du lieu tiếp theo
            rowsHead++;
            rowsHeadExcell = rowsHead + 2;
            int columtk = 0;
            BoldCell(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk + 1) + rowsHeadExcell, true);
            BorderRow(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk + 1) + (rowsHeadExcell + 1));
            myExcelData[rowsHead, columtk] = "Điểm trung bình";
            setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
            setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

            // rowsHeadExcell++;
            columtk = columtk + 2;
            BoldCell(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk + 1) + rowsHeadExcell, true);
            BorderRow(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk + 1) + (rowsHeadExcell + 1));
            myExcelData[rowsHead, columtk] = "Danh Hiệu";
            setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
            setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);
            // rowsHeadExcell++;
            columtk = columtk + 2;
            BoldCell(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk + 1) + rowsHeadExcell, true);
            BorderRow(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk + 1) + (rowsHeadExcell + 1));
            myExcelData[rowsHead, columtk] = "Xếp hạng";
            setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
            setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

            // rowsHeadExcell++;
            columtk = columtk + 2;
            BoldCell(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk + 1) + rowsHeadExcell, true);
            BorderRow(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk + 1) + rowsHeadExcell);
            myExcelData[rowsHead, columtk] = "Xếp loại";
            setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
            setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

            BoldCell(ColumnName(columtk) + (rowsHeadExcell + 1), ColumnName(columtk) + (rowsHeadExcell + 1), false);
            BorderRow(ColumnName(columtk) + (rowsHeadExcell + 1), ColumnName(columtk) + (rowsHeadExcell + 1));
            myExcelData[rowsHead + 1, columtk] = "HL";
            setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
            setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);
            columtk++;
            BoldCell(ColumnName(columtk) + (rowsHeadExcell + 1), ColumnName(columtk) + (rowsHeadExcell + 1), false);
            BorderRow(ColumnName(columtk) + (rowsHeadExcell + 1), ColumnName(columtk) + (rowsHeadExcell + 1));
            myExcelData[rowsHead + 1, columtk] = "HK";
            setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
            setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);


            // rowsHeadExcell++;
            columtk = columtk + 1;
            BoldCell(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk + 1) + rowsHeadExcell, true);
            BorderRow(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk + 1) + rowsHeadExcell);
            myExcelData[rowsHead, columtk] = "Vắng";
            setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
            setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

            BoldCell(ColumnName(columtk) + (rowsHeadExcell + 1), ColumnName(columtk) + (rowsHeadExcell + 1), false);
            BorderRow(ColumnName(columtk) + (rowsHeadExcell + 1), ColumnName(columtk) + (rowsHeadExcell + 1));
            myExcelData[rowsHead + 1, columtk] = "P";
            setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
            setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);
            columtk = columtk + 1;
            BoldCell(ColumnName(columtk) + (rowsHeadExcell + 1), ColumnName(columtk) + (rowsHeadExcell + 1), false);
            BorderRow(ColumnName(columtk) + (rowsHeadExcell + 1), ColumnName(columtk) + (rowsHeadExcell + 1));
            myExcelData[rowsHead + 1, columtk] = "K";
            setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
            setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

            // in ket qua cho phan nay 


            if (thoigian == "HK1")
            {
                rowsHead = rowsHead + 2;
                rowsHeadExcell = rowsHead + 2;

                columtk = 0;
                BoldCell(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = thoigian;
                setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);
                columtk++;

                BoldCell(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = dr[0]["TBHK1"].ToString();
                setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);


                // rowsHeadExcell++;
                columtk = columtk + 1;
                BoldCell(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk + 1) + rowsHeadExcell, true);
                BorderRow(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk + 1) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = dr[0]["DanhHieuK1"].ToString();
                setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);


                columtk = columtk + 2;
                BoldCell(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk + 1) + rowsHeadExcell, true);
                BorderRow(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk + 1) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = "";
                setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                columtk = columtk + 2;

                BoldCell(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = dr[0]["XLHLK1"].ToString();//"HL";
                setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                columtk = columtk + 1;
                BoldCell(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = dr[0]["HKHK1"].ToString();//            "HK";
                setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);


                // rowsHeadExcell++;
                columtk = columtk + 1;

                BoldCell(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = dr[0]["VangCoPhep"].ToString();
                setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);
                columtk = columtk + 1;
                BoldCell(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = dr[0]["VangKoPhep"].ToString();
                setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                ////---- in thong tin ca nam  -------------------------------------------------------------
                // hoc ky 2
            }
            else
            {
                rowsHead = rowsHead + 2;
                rowsHeadExcell = rowsHead + 2;

                columtk = 0;
                BoldCell(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk) + rowsHeadExcell);
                setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);
                myExcelData[rowsHead, columtk] = "HK1";
                columtk++;

                BoldCell(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = dr[0]["TBHK1"].ToString();
                setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);


                // rowsHeadExcell++;
                columtk = columtk + 1;
                BoldCell(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk + 1) + rowsHeadExcell, true);
                BorderRow(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk + 1) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = dr[0]["DanhHieuK1"].ToString();
                setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                columtk = columtk + 2;
                BoldCell(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk + 1) + rowsHeadExcell, true);
                BorderRow(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk + 1) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = "";
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                columtk = columtk + 2;

                BoldCell(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = dr[0]["XLHLK1"].ToString();//"HL";
                setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                columtk = columtk + 1;
                BoldCell(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = dr[0]["HKHK1"].ToString();//            "HK";
                setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);


                // rowsHeadExcell++;
                columtk = columtk + 1;

                BoldCell(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = dr[0]["VangCoPhep"].ToString();
                setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);
                columtk = columtk + 1;
                BoldCell(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = dr[0]["VangKoPhep"].ToString();
                setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                columtk = 0;
                rowsHeadExcell++;
                rowsHead++;

                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);
                myExcelData[rowsHead, columtk] = thoigian;
                columtk++;

                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);
                myExcelData[rowsHead, columtk] = dr[0]["TBHK2"].ToString();


                // rowsHeadExcell++;
                columtk = columtk + 1;
                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk + 1) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk + 1) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = dr[0]["DanhHieuK2"].ToString();
                setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                columtk = columtk + 2;
                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk + 1) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk + 1) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = "";
                setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                columtk = columtk + 2;

                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = dr[0]["XLHLK2"].ToString();//"HL";
                setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                columtk = columtk + 1;
                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = dr[0]["HKHK2"].ToString();//            "HK";
                setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                columtk = columtk + 1;

                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = dr[0]["VangCoPhep"].ToString();
                setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);
                columtk = columtk + 1;
                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = dr[0]["VangKoPhep"].ToString();
                setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                // thong tin ca nam

                columtk = 0;
                rowsHeadExcell++;
                rowsHead++;

                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);
                setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                myExcelData[rowsHead, columtk] = "Cả năm";
                columtk++;

                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);
                setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                myExcelData[rowsHead, columtk] = dr[0]["TBCN"].ToString();


                // rowsHeadExcell++;
                columtk = columtk + 1;
                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk + 1) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk + 1) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = dr[0]["DanhHieuCN"].ToString();
                setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                columtk = columtk + 2;
                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk + 1) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk + 1) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = "";
                setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                columtk = columtk + 2;

                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = dr[0]["XLHLCN"].ToString();//"HL";
                setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                columtk = columtk + 1;
                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = dr[0]["HKCN"].ToString();//            "HK";
                setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                columtk = columtk + 1;

                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = dr[0]["VangCoPhep"].ToString();
                setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                columtk = columtk + 1;
                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = dr[0]["VangKoPhep"].ToString();
                setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

            }    

            // in y kien cua phu huynh hoc sinh

            rowsHead += 3;
            rowsHeadExcell = rowsHead + 2;
            columtk = 0;
            BoldCell(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk) + rowsHeadExcell, true);
            myExcelData[rowsHead, columtk] = "* Ý kiến và chữ ký của phụ huynh:";


            rowsHead = rowsHead + 5;
            rowsHeadExcell = rowsHead + 2;
            BoldCell(ColumnName(columtk + 14) + rowsHeadExcell, ColumnName(columtk + 16) + rowsHeadExcell, true);

            myExcelData[rowsHead, columtk + 14] = tengiaovien;

        }

        public override void WriteDiemSoLienLacForOneThuGon(ViewSoLienLac SoLienLac, string thoigian,string namhoc,string tenHS, string lop, string siso)
        {

            myRowCnt = 30; //list.Count;
            int RowCount = 100;
            myExcelData = new object[RowCount, ColumnCount];

            myExcelData[0, 0] = "PHIẾU LIÊN LẠC  " + thoigian + "  NĂM HỌC  "+ namhoc  ;           
            BoldCell(ColumnName(0) + 2, ColumnName(18) + 2, true);
            BorderRow(ColumnName(0) + 2, ColumnName(18) + 2);   

            myExcelData[1, 0] = "Họ và tên học sinh:";
            BoldCell(ColumnName(0) + 3, ColumnName(0) + 3, true);// A1--> B1
            myExcelData[1, 2] = tenHS;
            BoldCell(ColumnName(2) + 3, ColumnName(9) + 3, true);// A1--> B1
            myExcelData[1, 10] = "Lớp: ";
            BoldCell(ColumnName(10) + 3, ColumnName(10) + 3, true);// A1--> B1
            myExcelData[1, 11] = " " + lop.ToString();
            BoldCell(ColumnName(11) + 3, ColumnName(11) + 3, true);
            myExcelData[1, 12] = "Sĩ số: ";
            BoldCell(ColumnName(12) + 3, ColumnName(12) + 3, true);// A1--> B1
            myExcelData[1, 13] = siso;
            BoldCell(ColumnName(13) + 3, ColumnName(13) + 3, true);// A1--> B1

            myExcelData[2, 0] = "Môn học";
            BoldCell(ColumnName(0) + 4, ColumnName(0) + 5, true);
            BorderRow(ColumnName(0) + 4, ColumnName(0) + 5);
            myExcelData[2, 1] = "Điểm";
            BoldCell(ColumnName(1) + 4, ColumnName(15) + 4, true);
            BorderRow(ColumnName(1) + 4, ColumnName(15) + 4);

            myExcelData[2, 16] = "HK";
            BoldCell(ColumnName(16) + 4, ColumnName(16) + 5, true);
            BorderRow(ColumnName(16) + 4, ColumnName(16) + 5);

            myExcelData[2, 17] = "TBHK";
            BoldCell(ColumnName(17) + 4, ColumnName(17) + 5, true);
            BorderRow(ColumnName(17) + 4, ColumnName(17) + 5);

            myExcelData[2, 18] = "Ghi chú";
            BoldCell(ColumnName(18) + 4, ColumnName(18) + 5, true);
            BorderRow(ColumnName(18) + 4, ColumnName(18) + 5); 

            myExcelData[3, 1] = "Miệng";
            BoldCell(ColumnName(1) + 5, ColumnName(5) + 5, true);
            BorderRow(ColumnName(1) + 5, ColumnName(5) + 5);

            myExcelData[3, 6] = "15 phút";
            BoldCell(ColumnName(6) + 5, ColumnName(10) + 5, true);
            BorderRow(ColumnName(6) + 5, ColumnName(10) + 5);


            myExcelData[3, 11] = "1 Tiết";
            BoldCell(ColumnName(11) + 5, ColumnName(15) + 5, true);
            BorderRow(ColumnName(11) + 5, ColumnName(15) + 5);

            IQueryable<ViewDiemMonHoc> valsTinhDiemList = (IQueryable<ViewDiemMonHoc>)SoLienLac.KQTD;
            List<ViewDiemMonHoc> valsTinhDiem = valsTinhDiemList.ToList();

           
            int rowsHead = 4;// in content header rowss
            int rowsHeadExcell = 0;
            
            for (int j = 0; j < valsTinhDiem.Count; j++)// list ra nhung mon tinh diem
            {
                // kiem tra xem mon hoc co phai la mon tieng viet khong  ?
                int socot = 0;
                rowsHeadExcell = rowsHead + 2;// do dinh dang cua excell la so dong them 2 dong 
                myExcelData[rowsHead, socot] = valsTinhDiem[j].tenMonHoc;
                setWidth(ColumnNames(socot) + rowsHeadExcell, 3);
                MergeCell(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell, true);
                BorderRow(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell);
                socot++;
                MergeCell(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell, true);
                BorderRow(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = valsTinhDiem[j].M1;
                setWidth(ColumnNames(socot) + rowsHeadExcell, 3);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 10);

                socot++;
                MergeCell(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell, true);
                BorderRow(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = valsTinhDiem[j].M2;
                setWidth(ColumnNames(socot) + rowsHeadExcell, 3);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 10);

                socot++;
                MergeCell(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell, true);
                BorderRow(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = valsTinhDiem[j].M3;
                setWidth(ColumnNames(socot) + rowsHeadExcell, 3);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 10);

                socot++;
                MergeCell(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell, true);
                BorderRow(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = valsTinhDiem[j].M4;
                setWidth(ColumnNames(socot) + rowsHeadExcell, 3);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 10);

                socot++;
                MergeCell(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell, true);
                BorderRow(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = valsTinhDiem[j].M5;
                setWidth(ColumnNames(socot) + rowsHeadExcell, 3);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 10);

                socot++;
                MergeCell(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell, true);
                BorderRow(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = valsTinhDiem[j].V15P1;
                setWidth(ColumnNames(socot) + rowsHeadExcell, 3);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 10);

                socot++;
                MergeCell(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell, true);
                BorderRow(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = valsTinhDiem[j].V15P2;
                setWidth(ColumnNames(socot) + rowsHeadExcell, 3);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 10);

                socot++;
                MergeCell(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell, true);
                BorderRow(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = valsTinhDiem[j].V15P3;
                setWidth(ColumnNames(socot) + rowsHeadExcell, 3);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 10);

                socot++;
                MergeCell(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell, true);
                BorderRow(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = valsTinhDiem[j].V15P4;
                setWidth(ColumnNames(socot) + rowsHeadExcell, 3);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 10);

                socot++;
                MergeCell(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell, true);
                BorderRow(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = valsTinhDiem[j].V15P5;
                setWidth(ColumnNames(socot) + rowsHeadExcell, 3);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 10);

                socot++;
                MergeCell(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell, true);
                BorderRow(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = valsTinhDiem[j].V45P1;
                setWidth(ColumnNames(socot) + rowsHeadExcell, 3);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 10);

                socot++;
                MergeCell(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell, true);
                BorderRow(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = valsTinhDiem[j].V45P2;
                setWidth(ColumnNames(socot) + rowsHeadExcell, 3);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 10);

                socot++;
                MergeCell(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell, true);
                BorderRow(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = valsTinhDiem[j].V45P3;
                setWidth(ColumnNames(socot) + rowsHeadExcell, 3);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 10);

                socot++;
                MergeCell(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell, true);
                BorderRow(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = valsTinhDiem[j].V45P4;
                setWidth(ColumnNames(socot) + rowsHeadExcell, 3);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 10);

                socot++;
                MergeCell(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell, true);
                BorderRow(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = valsTinhDiem[j].V45P5;
                setWidth(ColumnNames(socot) + rowsHeadExcell, 3);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 10);

                socot++;
                MergeCell(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell, true);
                BorderRow(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = valsTinhDiem[j].HK;
                setWidth(ColumnNames(socot) + rowsHeadExcell, 3);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 10);

                socot++;
                MergeCell(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell, true);
                BorderRow(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = valsTinhDiem[j].TBM;
                setWidth(ColumnNames(socot) + rowsHeadExcell, 3);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 10);

                socot++;
                MergeCell(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell, true);
                BorderRow(ColumnName(socot) + rowsHeadExcell, ColumnName(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = "";
                rowsHead = rowsHead +  1;
            
            }// end for
            
                   // tang len mot dong chuan bị in ra dữ liệu khác 
                   rowsHead ++;
                   rowsHeadExcell = rowsHead+2;
                   int columnx = 12;
                   BoldCell(ColumnName(columnx) + rowsHeadExcell, ColumnName(columnx+5) + rowsHeadExcell, true);
                   //BorderRow(ColumnName(columnx) + rowsHeadExcell, ColumnName(columnx+5) + rowsHeadExcell);
                   myExcelData[rowsHead, columnx] = "Nhận xét của giáo viên chủ nhiệm";
            
                    // chuan bi in nhung du lieu tiếp theo
                    rowsHead++;
                    rowsHeadExcell = rowsHead + 2;
                    int columtk = 0;
                    BoldCell(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk +1 ) + rowsHeadExcell, true);                 
                    BorderRow(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk + 1) + (rowsHeadExcell + 1));                
                    myExcelData[rowsHead, columtk] = "Điểm trung bình";
                    setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                    setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);
             
                   // rowsHeadExcell++;
                    columtk = columtk+2;
                    BoldCell(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk + 1) + rowsHeadExcell, true);
                    BorderRow(ColumnName(columtk) + rowsHeadExcell , ColumnName(columtk + 1) + (rowsHeadExcell + 1));
                    myExcelData[rowsHead, columtk] = "Danh Hiệu";
                    setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                    setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);
                   // rowsHeadExcell++;
                    columtk = columtk + 2;
                    BoldCell(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk + 1) + rowsHeadExcell, true);
                    BorderRow(ColumnName(columtk) + rowsHeadExcell , ColumnName(columtk + 1) + (rowsHeadExcell + 1));
                    myExcelData[rowsHead, columtk] = "Xếp hạng";
                    setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                    setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                   // rowsHeadExcell++;
                    columtk = columtk + 2;
                    BoldCell(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk+1) + rowsHeadExcell, true);
                    BorderRow(ColumnName(columtk) + rowsHeadExcell , ColumnName(columtk+1) + rowsHeadExcell);
                    myExcelData[rowsHead, columtk] = "Xếp loại";
                    setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                    setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);
            
                    BoldCell(ColumnName(columtk) + (rowsHeadExcell+1), ColumnName(columtk) + (rowsHeadExcell+1), false);            
                    BorderRow(ColumnName(columtk) + (rowsHeadExcell + 1), ColumnName(columtk) + (rowsHeadExcell + 1));
                    myExcelData[rowsHead+1, columtk] = "HL";
                    setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                    setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);
                    columtk++;
                    BoldCell(ColumnName(columtk) + (rowsHeadExcell + 1), ColumnName(columtk) + (rowsHeadExcell + 1), false);
                   BorderRow(ColumnName(columtk) + (rowsHeadExcell + 1), ColumnName(columtk) + (rowsHeadExcell + 1));
                    myExcelData[rowsHead+1, columtk] = "HK";
                    setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                    setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);


                   // rowsHeadExcell++;
                    columtk = columtk + 1;
                    BoldCell(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk + 1) + rowsHeadExcell, true);
                    BorderRow(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk + 1) + rowsHeadExcell);
                    myExcelData[rowsHead, columtk] = "Vắng";
                    setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                    setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                    BoldCell(ColumnName(columtk) + (rowsHeadExcell + 1), ColumnName(columtk) + (rowsHeadExcell + 1), false);
                    BorderRow(ColumnName(columtk) + (rowsHeadExcell + 1), ColumnName(columtk) + (rowsHeadExcell + 1));
                    myExcelData[rowsHead+1, columtk] = "P";
                    setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                    setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);
                    columtk = columtk + 1;
                    BoldCell(ColumnName(columtk) + (rowsHeadExcell + 1), ColumnName(columtk) + (rowsHeadExcell + 1), false);
                    BorderRow(ColumnName(columtk) + (rowsHeadExcell + 1), ColumnName(columtk ) + (rowsHeadExcell + 1));
                    myExcelData[rowsHead+1, columtk] = "K";
                    setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                    setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);
            
                    // in ket qua cho phan nay 


                    if (thoigian == "HK1")
                    {
                        rowsHead = rowsHead + 2;
                        rowsHeadExcell = rowsHead + 2;

                        columtk = 0;
                        BoldCell(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk) + rowsHeadExcell, true);
                        BorderRow(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk) + rowsHeadExcell);
                        myExcelData[rowsHead, columtk] = thoigian;
                        setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                        setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);
                        columtk++;

                        BoldCell(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk) + rowsHeadExcell, true);
                        BorderRow(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk) + rowsHeadExcell);
                        myExcelData[rowsHead, columtk] = SoLienLac.DTBHK1;
                        setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                        setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);


                        // rowsHeadExcell++;
                        columtk = columtk + 1;
                        BoldCell(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk + 1) + rowsHeadExcell, true);
                        BorderRow(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk + 1) + rowsHeadExcell);
                        myExcelData[rowsHead, columtk] = SoLienLac.danhHieuHK1;
                        setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                        setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);


                        columtk = columtk + 2;
                        BoldCell(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk + 1) + rowsHeadExcell, true);
                        BorderRow(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk + 1) + rowsHeadExcell);
                        myExcelData[rowsHead, columtk] = SoLienLac.xepHangHK1;
                        setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                        setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                        columtk = columtk + 2;

                        BoldCell(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk) + rowsHeadExcell, true);
                        BorderRow(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk) + rowsHeadExcell);
                        myExcelData[rowsHead, columtk] = SoLienLac.hocLucHK1;//"HL";
                        setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                        setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                        columtk = columtk + 1;
                        BoldCell(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk) + rowsHeadExcell, true);
                        BorderRow(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk) + rowsHeadExcell);
                        myExcelData[rowsHead, columtk] = SoLienLac.hanhKiemHK1;//            "HK";
                        setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                        setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);


                        // rowsHeadExcell++;
                        columtk = columtk + 1;

                        BoldCell(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk) + rowsHeadExcell, true);
                        BorderRow(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk) + rowsHeadExcell);
                        myExcelData[rowsHead, columtk] = SoLienLac.vangCoPhepHK1;
                        setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                        setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);
                        columtk = columtk + 1;
                        BoldCell(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk) + rowsHeadExcell, true);
                        BorderRow(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk) + rowsHeadExcell);
                        myExcelData[rowsHead, columtk] = SoLienLac.vangKhongPhepHK1;
                        setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                        setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                        ////---- in thong tin ca nam  -------------------------------------------------------------
                        // hoc ky 2
                    }else 
                    {
                        rowsHead = rowsHead + 2;
                        rowsHeadExcell = rowsHead + 2;

                        columtk = 0;
                        BoldCell(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk) + rowsHeadExcell, true);
                        BorderRow(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk) + rowsHeadExcell);
                        setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                        setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);
                        myExcelData[rowsHead, columtk] = "HK1";
                        columtk++;

                        BoldCell(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk) + rowsHeadExcell, true);
                        BorderRow(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk) + rowsHeadExcell);
                        myExcelData[rowsHead, columtk] = SoLienLac.DTBHK1;
                        setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                        setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);


                        // rowsHeadExcell++;
                        columtk = columtk + 1;
                        BoldCell(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk + 1) + rowsHeadExcell, true);
                        BorderRow(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk + 1) + rowsHeadExcell);
                        myExcelData[rowsHead, columtk] = SoLienLac.danhHieuHK1;
                        setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                        setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                        columtk = columtk + 2;
                        BoldCell(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk + 1) + rowsHeadExcell, true);
                        BorderRow(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk + 1) + rowsHeadExcell);
                        myExcelData[rowsHead, columtk] = SoLienLac.xepHangHK1;
                        setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                        columtk = columtk + 2;

                        BoldCell(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk) + rowsHeadExcell, true);
                        BorderRow(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk) + rowsHeadExcell);
                        myExcelData[rowsHead, columtk] = SoLienLac.hocLucHK1;//"HL";
                        setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                        setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                        columtk = columtk + 1;
                        BoldCell(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk) + rowsHeadExcell, true);
                        BorderRow(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk) + rowsHeadExcell);
                        myExcelData[rowsHead, columtk] = SoLienLac.hanhKiemHK1;//            "HK";
                        setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                        setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);


                        // rowsHeadExcell++;
                        columtk = columtk + 1;

                        BoldCell(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk) + rowsHeadExcell, true);
                        BorderRow(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk) + rowsHeadExcell);
                        myExcelData[rowsHead, columtk] = SoLienLac.vangCoPhepHK1;
                        setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                        setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);
                        columtk = columtk + 1;
                        BoldCell(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk) + rowsHeadExcell, true);
                        BorderRow(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk) + rowsHeadExcell);
                        myExcelData[rowsHead, columtk] = SoLienLac.vangKhongPhepHK1;
                        setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                        setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                       columtk = 0;
                       rowsHeadExcell++;
                       rowsHead++;

                       BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                       BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                       setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                       setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);
                       myExcelData[rowsHead, columtk] = thoigian;
                       columtk++;

                       BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                       BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                       setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                       setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);
                       myExcelData[rowsHead, columtk] = SoLienLac.DTBHK2;


                       // rowsHeadExcell++;
                       columtk = columtk + 1;
                       BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk + 1) + rowsHeadExcell, true);
                       BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk + 1) + rowsHeadExcell);
                       myExcelData[rowsHead, columtk] = SoLienLac.danhHieuHK2;
                       setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                       setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                       columtk = columtk + 2;
                       BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk + 1) + rowsHeadExcell, true);
                       BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk + 1) + rowsHeadExcell);
                       myExcelData[rowsHead, columtk] = SoLienLac.xepHangHK2;
                       setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                       setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                       columtk = columtk + 2;

                       BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                       BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                       myExcelData[rowsHead, columtk] = SoLienLac.hocLucHK2;//"HL";
                       setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                       setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                       columtk = columtk + 1;
                       BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                       BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                       myExcelData[rowsHead, columtk] = SoLienLac.hanhKiemHK2;//            "HK";
                       setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                       setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                       columtk = columtk + 1;

                       BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                       BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                       myExcelData[rowsHead, columtk] = SoLienLac.vangCoPhepHK2;
                       setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                       setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);
                       columtk = columtk + 1;
                       BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                       BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                       myExcelData[rowsHead, columtk] = SoLienLac.vangKhongPhepHK2;
                       setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                       setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                       // thong tin ca nam

                       columtk = 0;
                       rowsHeadExcell++;
                       rowsHead++;

                       BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                       BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                       setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);
                       setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                       myExcelData[rowsHead, columtk] = "Cả năm";
                       columtk++;

                       BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                       BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                       setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);
                       setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                       myExcelData[rowsHead, columtk] = SoLienLac.DTBCN;


                       // rowsHeadExcell++;
                       columtk = columtk + 1;
                       BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk + 1) + rowsHeadExcell, true);
                       BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk + 1) + rowsHeadExcell);
                       myExcelData[rowsHead, columtk] = SoLienLac.danhHieuCN;
                       setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                       setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                       columtk = columtk + 2;
                       BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk + 1) + rowsHeadExcell, true);
                       BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk + 1) + rowsHeadExcell);
                       myExcelData[rowsHead, columtk] = SoLienLac.xepHangCN;
                       setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                       setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                       columtk = columtk + 2;

                       BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                       BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                       myExcelData[rowsHead, columtk] = SoLienLac.hocLucCN;//"HL";
                       setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                       setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                       columtk = columtk + 1;
                       BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                       BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                       myExcelData[rowsHead, columtk] = SoLienLac.hanhKiemCN;//            "HK";
                       setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                       setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                       columtk = columtk + 1;

                       BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                       BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                       myExcelData[rowsHead, columtk] = SoLienLac.vangCoPhepCN;
                       setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                       setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                       columtk = columtk + 1;
                       BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                       BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                       myExcelData[rowsHead, columtk] = SoLienLac.vangKhongPhepCN;
                       setWidth(ColumnNames(columtk) + rowsHeadExcell, 3);
                       setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                   }                 
                   // in y kien cua phu huynh hoc sinh

                  rowsHead+=3;
                  rowsHeadExcell = rowsHead + 2;
                  columtk = 0;
                  BoldCell(ColumnName(columtk) + rowsHeadExcell, ColumnName(columtk) + rowsHeadExcell, true);                 
                  myExcelData[rowsHead, columtk] = "* Ý kiến và chữ ký của phụ huynh:";

            
                   rowsHead = rowsHead + 5;
                   rowsHeadExcell = rowsHead + 2;
                    BoldCell(ColumnName(columtk + 14) + rowsHeadExcell, ColumnName(columtk + 16) + rowsHeadExcell, true);
                 
                    myExcelData[rowsHead, columtk+14] = SoLienLac.tenGiaoVien;
               
         
        } 

        public override void WriteDiemSoLienLacForOneDayDu(ViewSoLienLac SoLienLac, string thoigian, string namhoc, string tenHS, string lop, string siso)
        {

            myRowCnt = 30; //list.Count;
            int RowCount = 100;
            myExcelData = new object[RowCount, ColumnCount];

            myExcelData[0, 0] = "PHIẾU LIÊN LẠC  " + thoigian + "  NĂM HỌC  " + namhoc;
            BoldCell(ColumnNames(0) + 2, ColumnNames(28) + 2, true);
            BorderRow(ColumnNames(0) + 2, ColumnNames(28) + 2);
            setFontAndSize(ColumnNames(0) + 2, "Arial", 10);

            myExcelData[1, 0] = "Họ và tên học sinh:";
            BoldCell(ColumnNames(0) + 3, ColumnNames(0) + 3, true);// A3 cell
            setFontAndSize(ColumnNames(0) + 3, "Arial", 10);
            
            myExcelData[1, 1] = tenHS;
            BoldCell(ColumnNames(1) + 3, ColumnNames(9) + 3, true);// A1--> B1

            myExcelData[1, 15] = "Lớp: ";
            BoldCell(ColumnNames(15) + 3, ColumnNames(15) + 3, true);// A1--> B1
            setFontAndSize(ColumnNames(15) + 3, "Arial", 10);

            myExcelData[1, 19] = " " + lop.ToString();
            BoldCell(ColumnNames(19) + 3, ColumnNames(19) + 3, true);
            
            myExcelData[1, 20] = "Sĩ số: ";
            BoldCell(ColumnNames(20) + 3, ColumnNames(20) + 3, true);// A1--> B1
            setFontAndSize(ColumnNames(20) + 3, "Arial", 10);

            myExcelData[1, 21] = siso;
            BoldCell(ColumnNames(21) + 3, ColumnNames(21) + 3, true);// A1--> B1

            myExcelData[2, 0] = "Môn học";
            BoldCell(ColumnNames(0) + 4, ColumnNames(0) + 5, true);
            setWidth(ColumnNames(0) + 4,17);// set width for  A colum is 17 pixel
            BorderRow(ColumnNames(0) + 4, ColumnNames(0) + 5);
            setFontAndSize(ColumnNames(0) + 4, "Arial", 10);

            myExcelData[2, 1] = "Điểm";
            BoldCell(ColumnNames(1) + 4, ColumnNames(25) + 4, true);
            BorderRow(ColumnNames(1) + 4, ColumnNames(25) + 4);
            setFontAndSize(ColumnNames(1) + 4, "Arial", 10);

            myExcelData[2, 26] = "HK";
            BoldCell(ColumnNames(26) + 4, ColumnNames(26) + 5, true);
            BorderRow(ColumnNames(26) + 4, ColumnNames(26) + 5);
            setFontAndSize(ColumnNames(26) + 4, "Arial", 10);

            myExcelData[2, 27] = "TBHK";
            BoldCell(ColumnNames(27) + 4, ColumnNames(27) + 5, true);
            BorderRow(ColumnNames(27) + 4, ColumnNames(27) + 5);
            setFontAndSize(ColumnNames(27) + 4, "Arial", 10);

            myExcelData[2, 28] = "Ghi chú";
            BoldCell(ColumnNames(28) + 4, ColumnNames(28) + 5, true);
            BorderRow(ColumnNames(28) + 4, ColumnNames(28) + 5);
            setFontAndSize(ColumnNames(28) + 4, "Arial", 10);

            myExcelData[3, 1] = "Miệng";
            BoldCell(ColumnNames(1) + 5, ColumnNames(10) + 5, true);
            BorderRow(ColumnNames(1) + 5, ColumnNames(10) + 5);
            setFontAndSize(ColumnNames(1) + 5, "Arial", 10);

            myExcelData[3, 11] = "15 phút";
            BoldCell(ColumnNames(11) + 5, ColumnNames(15) + 5, true);
            BorderRow(ColumnNames(11) + 5, ColumnNames(15) + 5);
            setFontAndSize(ColumnNames(11) + 5, "Arial", 10);
             
            myExcelData[3, 16] = "1 Tiết";
            BoldCell(ColumnNames(16) + 5, ColumnNames(25) + 5, true);
            BorderRow(ColumnNames(16) + 5, ColumnNames(25) + 5);
            setFontAndSize(ColumnNames(16) + 5, "Arial", 10);

            IQueryable<ViewDiemMonHoc> valsTinhDiemList = (IQueryable<ViewDiemMonHoc>)SoLienLac.KQTD;
            List<ViewDiemMonHoc> valsTinhDiem = valsTinhDiemList.ToList();


            int rowsHead = 4;// in content header rowss
            int rowsHeadExcell = 0;

            for (int j = 0; j < valsTinhDiem.Count; j++)// list ra nhung mon tinh diem
            {
                // kiem tra xem mon hoc co phai la mon tieng viet khong  ?
                int socot = 0;
                rowsHeadExcell = rowsHead + 2;// do dinh dang cua excell la so dong them 2 dong 
                myExcelData[rowsHead, socot] = valsTinhDiem[j].tenMonHoc;
                MergeCell(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell, true);
                BorderRow(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell);
                socot++;
                MergeCell(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell, true);
                BorderRow(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = valsTinhDiem[j].M1;
                setWidth(ColumnNames(socot) + rowsHeadExcell, 2.57);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 8);

                socot++;
                MergeCell(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell, true);
                BorderRow(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = valsTinhDiem[j].M2;
                setWidth(ColumnNames(socot) + rowsHeadExcell, 2.57);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 8);

                socot++;
                MergeCell(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell, true);
                BorderRow(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = valsTinhDiem[j].M3;
                setWidth(ColumnNames(socot) + rowsHeadExcell, 2.57);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 8);

                socot++;
                MergeCell(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell, true);
                BorderRow(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = valsTinhDiem[j].M4;
                setWidth(ColumnNames(socot) + rowsHeadExcell, 2.57);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 8);

                socot++;
                MergeCell(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell, true);
                BorderRow(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = valsTinhDiem[j].M5;
                setWidth(ColumnNames(socot) + rowsHeadExcell, 2.57);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 8);

                socot++;
                MergeCell(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell, true);
                BorderRow(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = valsTinhDiem[j].V15P1;
                setWidth(ColumnNames(socot) + rowsHeadExcell, 2.57);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 8);

                socot++;
                MergeCell(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell, true);
                BorderRow(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = valsTinhDiem[j].V15P2;
                setWidth(ColumnNames(socot) + rowsHeadExcell, 2.57);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 8); 

                socot++;
                MergeCell(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell, true);
                BorderRow(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = valsTinhDiem[j].TH15P3;
                setWidth(ColumnNames(socot) + rowsHeadExcell, 2.57);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 8);

                socot++;
                MergeCell(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell, true);
                BorderRow(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = valsTinhDiem[j].TH15P4;
                setWidth(ColumnNames(socot) + rowsHeadExcell, 2.57);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 8);

                socot++;
                MergeCell(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell, true);
                BorderRow(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = valsTinhDiem[j].TH15P5;
                setWidth(ColumnNames(socot) + rowsHeadExcell, 2.57);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 8);

                socot++;
                MergeCell(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell, true);
                BorderRow(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = valsTinhDiem[j].V15P3;
                setWidth(ColumnNames(socot) + rowsHeadExcell, 2.57);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 8);

                socot++;
                MergeCell(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell, true);
                BorderRow(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = valsTinhDiem[j].V15P4;
                setWidth(ColumnNames(socot) + rowsHeadExcell, 2.57);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 8);
               
                socot++;
                MergeCell(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell, true);
                BorderRow(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = valsTinhDiem[j].V15P5;
                setWidth(ColumnNames(socot) + rowsHeadExcell, 2.57);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 8);

                socot++;
                MergeCell(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell, true);
                BorderRow(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = valsTinhDiem[j].TH15P1;
                setWidth(ColumnNames(socot) + rowsHeadExcell, 2.57);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 8);

                socot++;
                MergeCell(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell, true);
                BorderRow(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = valsTinhDiem[j].TH15P2;
                setWidth(ColumnNames(socot) + rowsHeadExcell, 2.57);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 8);

                socot++;
                MergeCell(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell, true);
                BorderRow(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = valsTinhDiem[j].V45P1;
                setWidth(ColumnNames(socot) + rowsHeadExcell, 2.57);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 8);

                socot++;
                MergeCell(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell, true);
                BorderRow(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = valsTinhDiem[j].V45P2;
                setWidth(ColumnNames(socot) + rowsHeadExcell, 2.57);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 8);
                
                socot++;
                MergeCell(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell, true);
                BorderRow(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = valsTinhDiem[j].V45P3;
                setWidth(ColumnNames(socot) + rowsHeadExcell, 2.57);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 8);
                
                socot++;
                MergeCell(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell, true);
                BorderRow(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = valsTinhDiem[j].V45P4;
                setWidth(ColumnNames(socot) + rowsHeadExcell, 2.57);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 8);
                
                socot++;
                MergeCell(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell, true);
                BorderRow(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = valsTinhDiem[j].V45P5;
                setWidth(ColumnNames(socot) + rowsHeadExcell, 2.57);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 8); 

                socot++;
                MergeCell(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell, true);
                BorderRow(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = valsTinhDiem[j].TH45P1;
                setWidth(ColumnNames(socot) + rowsHeadExcell, 2.57);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 8);
                socot++;
                
                MergeCell(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell, true);
                BorderRow(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = valsTinhDiem[j].TH45P2;
                setWidth(ColumnNames(socot) + rowsHeadExcell, 2.57);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 8);

                socot++;
                MergeCell(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell, true);
                BorderRow(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = valsTinhDiem[j].TH45P3;
                setWidth(ColumnNames(socot) + rowsHeadExcell, 2.57);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 8);

                socot++;
                MergeCell(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell, true);
                BorderRow(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = valsTinhDiem[j].TH45P4;
                setWidth(ColumnNames(socot) + rowsHeadExcell, 2.57);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 8);

                socot++;
                MergeCell(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell, true);
                BorderRow(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = valsTinhDiem[j].TH45P5;
                setWidth(ColumnNames(socot) + rowsHeadExcell, 2.57);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 8); 

                socot++;
                MergeCell(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell, true);
                BorderRow(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = valsTinhDiem[j].HK;
                setWidth(ColumnNames(socot) + rowsHeadExcell, 2.57);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 8);

                socot++;
                MergeCell(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell, true);
                BorderRow(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = valsTinhDiem[j].TBM;
                setWidth(ColumnNames(socot) + rowsHeadExcell, 2.57);
                setFontAndSize(ColumnNames(socot) + rowsHeadExcell, "Arial", 8);
                
                socot++;
                MergeCell(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell, true);
                BorderRow(ColumnNames(socot) + rowsHeadExcell, ColumnNames(socot) + rowsHeadExcell);
                myExcelData[rowsHead, socot] = "";
                rowsHead = rowsHead + 1;

            }// end for

            // tang len mot dong chuan bị in ra dữ liệu khác 
            rowsHead++;
            rowsHeadExcell = rowsHead + 2;
            int columnx = 15;
            BoldCell(ColumnNames(columnx) + rowsHeadExcell, ColumnNames(columnx + 5) + rowsHeadExcell, true);
            //BorderRow(ColumnName(columnx) + rowsHeadExcell, ColumnName(columnx+5) + rowsHeadExcell);
            myExcelData[rowsHead, columnx] = "Nhận xét của giáo viên chủ nhiệm";
            setFontAndSize(ColumnNames(columnx) + rowsHeadExcell, "Arial", 10);
            // chuan bi in nhung du lieu tiếp theo
            rowsHead++;
            rowsHeadExcell = rowsHead + 2;
            int columtk = 0;
            BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk + 1) + rowsHeadExcell, true);
            BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk + 1) + (rowsHeadExcell + 1));
            myExcelData[rowsHead, columtk] = "Điểm trung bình";
            setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

            // rowsHeadExcell++;
            columtk = columtk + 2;
            BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk + 4) + rowsHeadExcell, true);
            BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk + 4) + (rowsHeadExcell + 1));
            myExcelData[rowsHead, columtk] = "Danh Hiệu";
            setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);
            // rowsHeadExcell++;
            columtk = columtk + 5;
            BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk + 1) + rowsHeadExcell, true);
            BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk + 1) + (rowsHeadExcell + 1));
            myExcelData[rowsHead, columtk] = "Xếp hạng";
            setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);
            // rowsHeadExcell++;
            columtk = columtk + 2;
            BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk + 1) + rowsHeadExcell, true);
            BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk + 1) + rowsHeadExcell);
            myExcelData[rowsHead, columtk] = "Xếp loại";
            setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

            BoldCell(ColumnNames(columtk) + (rowsHeadExcell + 1), ColumnNames(columtk) + (rowsHeadExcell + 1), false);
            BorderRow(ColumnNames(columtk) + (rowsHeadExcell + 1), ColumnNames(columtk) + (rowsHeadExcell + 1));
            myExcelData[rowsHead + 1, columtk] = "HL";
            setFontAndSize(ColumnNames(columtk) + (rowsHeadExcell + 1), "Arial", 10);
            columtk++;
            BoldCell(ColumnNames(columtk) + (rowsHeadExcell + 1), ColumnNames(columtk) + (rowsHeadExcell + 1), false);
            BorderRow(ColumnNames(columtk) + (rowsHeadExcell + 1), ColumnNames(columtk) + (rowsHeadExcell + 1));
            myExcelData[rowsHead + 1, columtk] = "HK";
            setFontAndSize(ColumnNames(columtk) + (rowsHeadExcell + 1), "Arial", 10);

            // rowsHeadExcell++;
            columtk = columtk + 1;
            BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk+1) + rowsHeadExcell, true);
            BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk+1) + rowsHeadExcell);
            myExcelData[rowsHead, columtk] = "Vắng";
            setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);
            
            BoldCell(ColumnNames(columtk) + (rowsHeadExcell + 1), ColumnNames(columtk) + (rowsHeadExcell + 1), false);
            BorderRow(ColumnNames(columtk) + (rowsHeadExcell + 1), ColumnNames(columtk) + (rowsHeadExcell + 1));
            myExcelData[rowsHead + 1, columtk] = "P";
            setFontAndSize(ColumnNames(columtk) + (rowsHeadExcell + 1), "Arial", 10);
            columtk = columtk + 1;
            BoldCell(ColumnNames(columtk) + (rowsHeadExcell + 1), ColumnNames(columtk) + (rowsHeadExcell + 1), false);
            BorderRow(ColumnNames(columtk) + (rowsHeadExcell + 1), ColumnNames(columtk) + (rowsHeadExcell + 1));
            myExcelData[rowsHead + 1, columtk] = "K";
            setFontAndSize(ColumnNames(columtk) + (rowsHeadExcell + 1), "Arial", 10);

            // in ket qua cho phan nay  
            if (thoigian == "HK1")
            {
                rowsHead = rowsHead + 2;
                rowsHeadExcell = rowsHead + 2; 
                columtk = 0;
            
                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);
                myExcelData[rowsHead, columtk] = thoigian;
                columtk++;

                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);
                myExcelData[rowsHead, columtk] = SoLienLac.DTBHK1;


                // rowsHeadExcell++;
                columtk = columtk + 1;
                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk + 4) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk + 4) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = SoLienLac.danhHieuHK1;
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                columtk = columtk + 5;
                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk + 1) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk + 1) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = SoLienLac.xepHangHK1;
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                columtk = columtk + 2;

                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = SoLienLac.hocLucHK1;//"HL";
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                columtk = columtk + 1;
                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = SoLienLac.hanhKiemHK1;//            "HK";
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                columtk = columtk + 1;

                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] =  SoLienLac.vangCoPhepHK1.ToString();
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);
                columtk = columtk + 1;
                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = SoLienLac.vangKhongPhepHK1.ToString();
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);
            } else {

            ////---- in thong tin ca nam  -------------------------------------------------------------
            // hoc ky 2
                rowsHead = rowsHead + 2;
                rowsHeadExcell = rowsHead + 2;
                columtk = 0;

                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);
                myExcelData[rowsHead, columtk] = "HK1";
                columtk++;

                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);
                myExcelData[rowsHead, columtk] = SoLienLac.DTBHK1;


                // rowsHeadExcell++;
                columtk = columtk + 1;
                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk + 4) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk + 4) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = SoLienLac.danhHieuHK1;
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                columtk = columtk + 5;
                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk + 1) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk + 1) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = SoLienLac.xepHangHK1;
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                columtk = columtk + 2;

                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = SoLienLac.hocLucHK1;//"HL";
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                columtk = columtk + 1;
                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = SoLienLac.hanhKiemHK1;//            "HK";
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                columtk = columtk + 1;

                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = SoLienLac.vangCoPhepHK1.ToString();
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);
                columtk = columtk + 1;
                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] =  SoLienLac.vangKhongPhepHK1.ToString();
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                columtk = 0;
                rowsHeadExcell++;
                rowsHead++;

                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);
                myExcelData[rowsHead, columtk] = thoigian;
                columtk++;

                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);
                myExcelData[rowsHead, columtk] = SoLienLac.DTBHK2;


                // rowsHeadExcell++;
                columtk = columtk + 1;
                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk + 4) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk + 4) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = SoLienLac.danhHieuHK2;
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                columtk = columtk + 5;
                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk + 1) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk + 1) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = SoLienLac.xepHangHK2;
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                columtk = columtk + 2;

                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = SoLienLac.hocLucHK2;//"HL";
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                columtk = columtk + 1;
                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = SoLienLac.hanhKiemHK2;//            "HK";
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                columtk = columtk + 1;

                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = SoLienLac.vangCoPhepHK2 == -1 ? "" : SoLienLac.vangCoPhepHK2.ToString();
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);
                columtk = columtk + 1;
                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = SoLienLac.vangKhongPhepHK2 == -1 ? "" : SoLienLac.vangKhongPhepHK2.ToString();
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10); 

                // thong tin ca nam

                columtk = 0;
                rowsHeadExcell++;
                rowsHead++;

                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);
                myExcelData[rowsHead, columtk] = "Cả năm";
                columtk++;

                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);
                myExcelData[rowsHead, columtk] = SoLienLac.DTBCN;


                // rowsHeadExcell++;
                columtk = columtk + 1;
                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk + 4) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk + 4) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = SoLienLac.danhHieuCN;
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                columtk = columtk + 5;
                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk + 1) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk + 1) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = SoLienLac.xepHangCN;
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                columtk = columtk + 2;

                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = SoLienLac.hocLucCN;//"HL";
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                columtk = columtk + 1;
                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = SoLienLac.hanhKiemCN;//            "HK";
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

                columtk = columtk + 1;

                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = SoLienLac.vangCoPhepCN == -1 ? "" : SoLienLac.vangCoPhepCN.ToString();
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);
                columtk = columtk + 1;
                BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
                BorderRow(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell);
                myExcelData[rowsHead, columtk] = SoLienLac.vangKhongPhepCN == -1 ? "" : SoLienLac.vangKhongPhepCN.ToString();
                setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

            }                    
            // in y kien cua phu huynh hoc sinh

            rowsHead+=3;
            rowsHeadExcell = rowsHead + 2;
            columtk = 0;
            BoldCell(ColumnNames(columtk) + rowsHeadExcell, ColumnNames(columtk) + rowsHeadExcell, true);
            myExcelData[rowsHead, columtk] = "* Ý kiến và chữ ký của phụ huynh:";
            setFontAndSize(ColumnNames(columtk) + rowsHeadExcell, "Arial", 10);

            rowsHead = rowsHead + 5;
            rowsHeadExcell = rowsHead + 2;
            BoldCell(ColumnNames(columtk + 17) + rowsHeadExcell, ColumnNames(columtk + 20) + rowsHeadExcell, true);

            myExcelData[rowsHead, columtk + 17] = SoLienLac.tenGiaoVien;


        }
         

  //--- ham sau se thuc hien hien thi bia cua so lien lac ra ngoai cho tung hoc sinh 
        public override void WriteBiaSoLienLacForOne(ViewHocSinhSoLienLac SoLienLac, string thoigian, bool viewAllColumns)
        {

            myRowCnt = 30; //list.Count;
           int RowCount = 100;
           
           myExcelData = new object[RowCount, ColumnCount];
             
            myExcelData[1, 1] = "  CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM";
            MergeCell(ColumnName(1) + 3, ColumnName(8) + 3, true,true); //merge B3-I3
            myExcelData[2, 1] = "      ĐỘC LẬP- TỰ DO- HẠNH PHÚC     ";
            MergeCell(ColumnName(1) + 4, ColumnName(8) + 4, true,true);//merge B4-I4
            
            // bo cach mot dong  in ra don vi chu quan 
            
            myExcelData[4, 1] = SoLienLac.donvichuquan.ToUpper();
            BoldCell(ColumnName(1) + 6, ColumnName(8) + 6, true);//merge B6-I6
            
            myExcelData[5, 1] = SoLienLac.tentruong;
            BoldCell(ColumnName(1) + 7, ColumnName(8) + 7, true); //merge B7-I7
            myExcelData[7, 1] = " PHIẾU LIÊN LẠC ";
            // bo cach mot dong  in ra don vi chu quan 

            BoldCell(ColumnName(1) + 9, ColumnName(8) + 9, true); //merge B6-I6
            myExcelData[8, 1] = "GIỮA NHÀ TRƯỜNG VÀ GIA ĐÌNH HỌC SINH";
            BoldCell(ColumnName(1) + 10, ColumnName(8) + 10, true);

            myExcelData[9, 1] = " Năm Học: " + SoLienLac.namhoc;
            MergeCell(ColumnName(1) + 11, ColumnName(8) + 11, true,true);

            // bo cach 4 dong  in ra don vi chu quan 
            // bat dau tu cot thu hai tuc la cot C

            myExcelData[13, 2] = " 1/ Họ và tên học sinh:   " + SoLienLac.tenHS;
            MergeCell(ColumnName(2) + 15, ColumnName(5) + 15, true);
            myExcelData[13, 6] = "Lớp: " + SoLienLac.lop;
            MergeCell(ColumnName(6) + 15, ColumnName(7) + 15, true);

            // ngay sinh 
            myExcelData[14, 2] = "    Sinh ngày  " + ((SoLienLac.ngaysinh == null) ? "" : ((DateTime)SoLienLac.ngaysinh).Day.ToString());
            MergeCell(ColumnName(2) + 16, ColumnName(3) + 16, true);// ngay sinh merge 2cot

            myExcelData[14, 4] = "  tháng  " + ((SoLienLac.ngaysinh == null) ? "" : ((DateTime)SoLienLac.ngaysinh).Month.ToString());
            MergeCell(ColumnName(4) + 16, ColumnName(5) + 16, true);// thang sinh merge 2cot

            myExcelData[14, 6] = "  năm  " + ((SoLienLac.ngaysinh == null) ? "" : ((DateTime)SoLienLac.ngaysinh).Year.ToString());
            MergeCell(ColumnName(6) + 16, ColumnName(7) + 16, true);// thang sinh merge 2cot
             
            // in Noi sinh  cach mot dong 
            myExcelData[15, 2] = "Nơi sinh: " + SoLienLac.noisinh;
            MergeCell(ColumnName(2) + 17, ColumnName(7) + 17, true);// Noi  sinh merge 6 cot
            // in  Ho ten cha va nghe nghiep 

            myExcelData[16, 2] = " 2/ Họ, tên cha:   " + SoLienLac.hotencha   ;
            MergeCell(ColumnName(2) + 18, ColumnName(4) + 18, true);// Noi  sinh merge 3 cot    

            myExcelData[16, 5] = "Nghề nghiệp:" + SoLienLac.nghenghiepcha;
            MergeCell(ColumnName(5) + 18, ColumnName(8) + 18, true);// Noi  sinh merge 3 cot    

            myExcelData[17, 2] = "Họ, tên mẹ:" + SoLienLac.hotenme;
            MergeCell(ColumnName(2) + 19, ColumnName(4) + 19, true);// Noi  sinh merge 3 cot    

            myExcelData[17, 5] = "Nghề nghiệp:" + SoLienLac.nghenghiepme;
            MergeCell(ColumnName(5) + 19, ColumnName(8) + 19, true);// Noi  sinh merge 3 cot    
            // in dia chi va dien thoai 
            myExcelData[18, 2] = "Địa chỉ liên lạc:" + SoLienLac.diachilienlac;
            MergeCell(ColumnName(2) + 20, ColumnName(5) + 20, true);// Noi  sinh merge 3 cot    
            
            myExcelData[18, 6] = "Điện thoại:" + SoLienLac.dienthoai;
            MergeCell(ColumnName(6) + 20, ColumnName(6) + 20, true);// Noi  sinh merge 3 cot    

            // in ten giao vien phu trach lop
            myExcelData[19, 2] = "3/ Họ và tên giáo viên phụ trách lớp: " + SoLienLac.giaovienphutrachlop;
            MergeCell(ColumnName(2) + 21, ColumnName(7) + 21, true);// Noi  sinh merge 3 cot    

            // in dia chi cua giao vien phu trach lop
            myExcelData[20, 2] = "Địa chỉ: " + SoLienLac.diachiGV;
            MergeCell(ColumnName(2) + 22, ColumnName(8) + 22, true);// Noi  sinh merge 3 cot    
            
             
            DateTime ngayhientai = DateTime.Now;
            string ngayhientaiSt = "Ngày  "
               + ngayhientai.Day.ToString() + " Tháng  "
               + ngayhientai.Month.ToString() + " Năm "
               + ngayhientai.Year.ToString();

            // cach ra 2 dong de in ngay thang nam 

            myExcelData[23, 5] = "Ngày  " + ngayhientai.Day.ToString();

            myExcelData[23, 6] = "Tháng  " + ngayhientai.Month.ToString();

            myExcelData[23, 7] = "Năm  " + ngayhientai.Year.ToString();
            MergeCell(ColumnName(7) + 25, ColumnName(8) + 25, true);// Noi  sinh merge 3 cot    


            myExcelData[24, 2] = "Chữ ký của phụ huynh";              
            BoldCell(ColumnName(2) + 26, ColumnName(4) + 26, true);// meger từ B5-F5
           
            myExcelData[24, 5] = " TL. Hiệu Trưởng  ";
            BoldCell(ColumnName(5) + 26, ColumnName(7) + 26, true);// meger từ B5-F5
          
            myExcelData[25, 5] = " Giáo viên chủ nhiệm ";
            BoldCell(ColumnName(5) + 27, ColumnName(7) + 27, true);// meger từ B5-F5
          

            myExcelData[29, 5] = SoLienLac.giaovienphutrachlop;
            BoldCell(ColumnName(5) + 31, ColumnName(7) + 31, true);// meger từ B5-F5
            //BorderRow(ColumnName(1) + 3, ColumnName(8) + 31);
        } 
   
        //--- ham sau se thuc hien hien thi bia sau ve thong tin cuoi nam  cua so lien lac ra ngoai cho tung hoc sinh 
        public override void WriteBiaCuoiNamSoLienLacForOne(ViewHocSinhSoLienLacCuoiNam SoLienLac, string thoigian, bool viewAllColumns)
        {

            myRowCnt = 30; //list.Count;
            int RowCount = 100;
            myExcelData = new object[RowCount, ColumnCount];

            myExcelData[1, 1] = "  NHẬN XÉT GIÁO VIÊN CẢ NĂM HỌC";
            BoldCell(ColumnName(1) + 3, ColumnName(3) + 3, true);
          
            myExcelData[3, 1] = " NHẬN XÉT GIÁO VIÊN ";
            BoldCell(ColumnName(1) + 5, ColumnName(3) + 5, true);
            BorderRow(ColumnName(1) + 5, ColumnName(3) + 5);// dong khung lai

            myExcelData[4, 1] = SoLienLac.nhanxetcuagv;
            BoldCell(ColumnName(1) + 6, ColumnName(3) + 14, true);
            BorderRow(ColumnName(1) + 6, ColumnName(3) + 14);


            myExcelData[15, 1] = "- Danh hiệu học sinh được khen thưởng:   " + SoLienLac.ketqualenlop;
            MergeCell(ColumnName(1) + 17, ColumnName(3) + 17, true);

            myExcelData[16, 1] = "- Thi lại môn:   " ;
            MergeCell(ColumnName(1) + 18, ColumnName(3) + 18, true);
            BorderRow(ColumnName(1) + 17, ColumnName(3) + 18);

 
            myExcelData[19, 1] = "LỊCH MỜI HỌP ";
            BoldCell(ColumnName(1) + 21, ColumnName(3) + 21, true);

            myExcelData[21, 1] = "LẦN 1";
            myExcelData[21, 2] = "LẦN 2";
            myExcelData[21, 3] = "LẦN 3";
            BorderRow(ColumnName(1) + 23, ColumnName(3) + 23);

            myExcelData[22, 1] = "Thời gian:";
            myExcelData[22, 2] = "Thời gian:";
            myExcelData[22, 3] = "Thời gian:";
           
           
            myExcelData[23, 1] = "Địa điểm:";
            myExcelData[23, 2] = "Địa điểm:";
            myExcelData[23, 3] = "Địa điểm:";

            
            myExcelData[25, 1] = "Nội dung:";
            myExcelData[25, 2] = "Nội dung:";
            myExcelData[25, 3] = "Nội dung:";

            BorderRow(ColumnName(1) + 24, ColumnName(3) + 27);
             
            myExcelData[27, 1] = " HOẠT ĐỘNG HÈ ";
            BoldCell(ColumnName(1) + 29, ColumnName(2) + 29, true);
            myExcelData[28, 1] = "(Nhận xét của ban chị đạo hè địa phương)";
            MergeCell(ColumnName(1) + 30, ColumnName(2) + 30, true, true);

            //  BoldCell(ColumnName(3) + 20, ColumnName(3) + 20, true);// meger từ B5-F5
            //  BorderRow(ColumnName(3) + 20, ColumnName(3) + 20);

            
        }
        //-------------------------------------
        public override object[,] ExcelData
        {
            get
            {
                return myExcelData;
            }
        }

     

        public override int RowCount
        {
            get
            {
                return myRowCnt + 2;
                //return myRowCnt + 1;
            }
        }

        public override int StartRow
        {
            get
            {
                return startRow;
            }
        }
    }
}