﻿using System.Collections.Generic;
using System.IO;
using System.Reflection;
using VnEduPlus.Models;
using System;
using System.Data.SqlClient;
using System.Data;
namespace VnEduPlus
{
    public abstract class ExcelFileWriterSoLienLac<T>
    {
        private Excel.Application _excelApplication = null;
        private Excel.Workbooks _workBooks = null;
        private Excel._Workbook _workBook = null;
        private object _value = Missing.Value;
        private Excel.Sheets _excelSheets = null;
        private Excel._Worksheet _excelSheet = null;
        private Excel.Range _excelRange = null;
        private Excel.Font _excelFont = null;
        private int  columnCount=30;
        private string[] index = new string[] {"A","B","C","D","E","F","I","J","L","M","N","O","P","Q","R","S","T","U","V","W","X"};       


        /// <summary>
        /// user have to parse the data from the list and pass each data along with the
        /// column and row name to the base fun, FillExcelWithData().
        /// </summary>
        /// <param name="list"></param>
    

        public abstract void WriteDiemSoLienLacForOneThuGon(ViewSoLienLac SoLienLac, string thoigian,string namhoc,string tenHS, string lop, string siso);

        public abstract void WriteDiemSoLienLacForOneDayDu(ViewSoLienLac SoLienLac, string thoigian, string namhoc, string tenHS, string lop, string siso);

        public abstract void WriteDiemSoLienLacForOneThuGonPerformance(DataRow[] dr, string thoigian, string namhoc, string tenHS,string tengiaovien);

        public abstract void WriteDiemSoLienLacForOneDayDuPerformance(DataRow[] dr, string thoigian, string namhoc, string tenHS,string tengiaovien);
        
        public abstract void WriteBiaSoLienLacForOne(ViewHocSinhSoLienLac SoLienLac, string thoigian, bool viewAllColumns = true);

        public abstract void WriteBiaCuoiNamSoLienLacForOne(ViewHocSinhSoLienLacCuoiNam SoLienLac, string thoigian, bool viewAllColumns = true);

        public abstract void FillSoLienLacHocSinhToExcell(List<SoLienLac> SoLienLac);

        /// <summary>
        /// get the data of object which will be saved to the excel sheet
        /// </summary>
        public abstract object[,] ExcelData { get; }
        /// <summary>
        /// get the no of columns
        /// </summary>
        public int ColumnCount
        {
            get
            {
                return columnCount;
            }
            set
            {
                columnCount = value;
            }
        }
        /// <summary>
        /// get the now of rows to fill
        /// </summary>
        public abstract int RowCount { get; }

        /// <summary>
        /// get the start roeto fill
        /// </summary>
        public abstract int StartRow { get; }

        /// <summary>
        /// user can override this to make the headers not be in bold.
        /// by default it is true
        /// </summary>
        protected virtual Excel._Worksheet ExcelSheet
        {
            get
            {
                return _excelSheet;
            }
        }

        /// <summary>
        /// user can override this to make the headers not be in bold.
        /// by default it is true
        /// </summary>
        protected virtual bool BoldHeaders
        {
            get
            {
                return true;
            }
        }


        public void WriteDataSoLienLacToExcelPerformance(string ListMahocsinh, string fileName, string thoigian, string namhoc, string tentruong, DataSet ds, bool checkUI, string tengiaovien)
        {
            string[] LstMaHocSinh = ListMahocsinh.Split(',');
            _excelApplication = new Excel.Application();
            _workBooks = (Excel.Workbooks)_excelApplication.Workbooks;
            foreach (string hs in LstMaHocSinh)
            {
                DataRow[] dr = ds.Tables[0].Select("MaHocSinh='" + hs + "'");
                if (dr != null)
                {

                    if (_workBook != null)
                    {
                        _excelSheet = _excelSheets.Add(Type.Missing);
                        _excelSheet.Move(After: _workBook.Sheets[_workBook.Sheets.Count]);
                        _excelSheet.Name = (string.IsNullOrEmpty(dr[0]["HoTen"].ToString())) ? "tên_không_xác_định" : dr[0]["HoTen"].ToString();
                        if (checkUI)
                        {
                            this.WriteDiemSoLienLacForOneDayDuPerformance(dr, thoigian, namhoc, dr[0]["HoTen"].ToString(), tengiaovien);
                        }
                        else
                        {
                            this.WriteDiemSoLienLacForOneThuGonPerformance(dr, thoigian, namhoc, dr[0]["HoTen"].ToString(), tengiaovien);

                        }

                    }
                    else
                    {
                        _workBook = (Excel._Workbook)(_workBooks.Add(1));

                        _excelSheets = (Excel.Sheets)_workBook.Worksheets;
                        _excelSheet = _excelSheets.Add(Type.Missing);

                        _excelSheet.Name = (string.IsNullOrEmpty(dr[0]["HoTen"].ToString())) ? "tên_không_xác_định" : dr[0]["HoTen"].ToString();
                        if (checkUI)
                        {
                            this.WriteDiemSoLienLacForOneDayDuPerformance(dr, thoigian, namhoc, dr[0]["HoTen"].ToString(), tengiaovien);
                        }
                        else
                        {
                            this.WriteDiemSoLienLacForOneThuGonPerformance(dr, thoigian, namhoc, dr[0]["HoTen"].ToString(), tengiaovien);

                        }
                    }
                    object[,] info = new object[1, 1];
                    info[0, 0] = "Trường : " + tentruong;
                    //info[0, 0] = "PHIẾU LIÊN LẠC  " + thoigian + "  NĂM HỌC  " + namhoc;

                    _excelRange = _excelSheet.get_Range("A1", "B1");
                    _excelRange.Cells[1, 1].Font.Size = 18;

                    _excelRange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                    _excelRange.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                    _excelRange.Cells[1, 1].Font.Color = System.Drawing.Color.Red;
                    _excelRange = _excelRange.get_Resize(1, 1);
                    _excelRange.NumberFormat = "@";
                    _excelRange.Cells[1, 1] = "Trường : " + tentruong;
                    BoldCell("A1", "S1", true);

                    this.FillExcelWithData();
                }
            }

            
            this.SaveExcel(fileName);
        }



        public void ExportSoLienLacToExcel(string fileName, string hocky, string namhoc, string tenDonViChuQuan, string tentruong, string tenlop, string makhoi, List<SoLienLac> dsSolienlac, string tengiaovien, string malop)
        {
            _excelApplication = new Excel.Application();
            _workBook = _excelApplication.Workbooks.Open(fileName, _value, _value, _value, _value, _value, _value, _value, _value, _value, _value, _value, _value);
            _excelSheets = (Excel.Sheets)_workBook.Worksheets;
            taoCacPhieuLienLacs(dsSolienlac);
          
            cungCapThongTinChoTungPhieuLienLac(dsSolienlac, tenDonViChuQuan, tentruong, tenlop, makhoi, tengiaovien, namhoc, hocky, malop);

            this.SaveExcel(fileName);
            
        }


        public void ExportSoBoMonToExcel(string fileName, int thang, string namhoc, string tenTruong, string tenlop, List<HocSinhNhanXetThang> hsnx, string tengiaovien,string tenPhongGiaoDuc)
        {
            _excelApplication = new Excel.Application();
            _workBook = _excelApplication.Workbooks.Open(fileName, _value, _value, _value, _value, _value, _value, _value, _value, _value, _value, _value, _value);
            _excelSheets = (Excel.Sheets)_workBook.Worksheets;

            TaoSoBoMonTheoDoiChatLuongGiaoDuc(fileName, thang, namhoc, tengiaovien,tenPhongGiaoDuc, tenlop, tenTruong, hsnx);
            
            this.SaveExcel(fileName);

        }

        public void ExportSoNhapDiemToExcel(string fileName, string namhoc, string tenTruong, string tenlop, DataTable table, string tengiaovien, string tenPhongGiaoDuc)
        {
            _excelApplication = new Excel.Application();
            _workBook = _excelApplication.Workbooks.Open(fileName, _value, _value, _value, _value, _value, _value, _value, _value, _value, _value, _value, _value);
            _excelSheets = (Excel.Sheets)_workBook.Worksheets;

            TaoSoNhapDiem(fileName, namhoc, tengiaovien, tenPhongGiaoDuc, tenlop, tenTruong, table);

            this.SaveExcel(fileName);

        }


        public void ExportSoChuNhiemToExcel(string fileName, int thang, string namhoc, string tenTruong, string tenlop, List<HocSinhNhanXetNhom> hsnx, string tengiaovien, string tenPhongGiaoDuc)
        {
            _excelApplication = new Excel.Application();
            _workBook = _excelApplication.Workbooks.Open(fileName, _value, _value, _value, _value, _value, _value, _value, _value, _value, _value, _value, _value);
            _excelSheets = (Excel.Sheets)_workBook.Worksheets;

            TaoSoChuNhiemTheoDoiChatLuongGiaoDuc(fileName, thang, namhoc, tengiaovien, tenPhongGiaoDuc, tenlop, tenTruong, hsnx);

            this.SaveExcel(fileName);

        }



        public static void copyFileTemplate(string fileNameSource, string fileNameDestinate)
        {
            File.Copy(fileNameSource, fileNameDestinate, true);
        }

        /// <summary>
        /// Copy tạo ra nhiều sheet với nội dung giống nhau cho template
        /// </summary>
        /// <param name="soLienLac"></param>
        public void taoCacPhieuLienLacs(List<SoLienLac> table)
        {

            int k = 0;
            _excelApplication.DisplayAlerts = false;
            // duyệt qua danh sách các học sinh mỗi học sinh sẽ được tạo thành một sheet
            for (int i = 0; i < table.Count; i++)
            {
                int j = i + 1;
               
                Excel.Worksheet sheet1 = ((Excel.Worksheet)_excelApplication.ActiveWorkbook.Worksheets[j]);

                _excelSheet = sheet1;
                  _excelSheet.Name = table[i].stt.ToString() + "_" +table[i].hoTenHS.ToString();
                Excel.Worksheet sheet2 = ((Excel.Worksheet)_excelApplication.ActiveWorkbook.Worksheets[j]);
                sheet2.Copy(Type.Missing,sheet1);              
                _excelSheet = sheet2;
                _workBook.Save();
                
                k++;

            } 
        }

        public string gethocKy(string mahk)
        {
            if (mahk == "HK1")
            {
                return "HỌC KỲ I";
            }
            else if (mahk == "GHK1")
            {
                return "GIỮA HỌC KỲ I";
            }
            else if (mahk == "GHK2")
            {
                return "GIỮA HỌC KỲ II";
            }
            else
            {
                return "CẢ NĂM";
            }
        }
        public void cungCapThongTinChoTungPhieuLienLac(List<SoLienLac> soLienLac, string tenDonViChuQuan, string tenTruong, string tenLop, string makhoi, string tenGV, string namHoc, string hocKy, string malop)
        {

            bool isLoaiKhoi1 = (makhoi.Substring(1) == "1" || makhoi.Substring(1) == "2" || makhoi.Substring(1) == "3") ? true : false;
            for (int i = 0; i < soLienLac.Count; i++)
            {
                int j = i + 1;


                Excel.Worksheet sheet = ((Excel.Worksheet)_excelApplication.ActiveWorkbook.Worksheets[j]);
                sheet.Select(Type.Missing);

                _excelRange = sheet.get_Range("B2", "J2"); // xác định khoảng để điền dữ liệu
                _excelRange.Cells[1, 1] = tenDonViChuQuan;

                _excelRange = sheet.get_Range("B3", "J3"); // xác định khoảng để điền dữ liệu


                _excelRange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                _excelRange.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;

                _excelRange.Cells[1, 1] = tenTruong;

                _excelRange = sheet.get_Range("L2", "S2");
                _excelRange.Cells[1, 1] = sheet.get_Range("L2", "S2").Cells[1, 1].Value + gethocKy(hocKy);

                sheet.get_Range("B9", "J9").Cells[1, 1] = sheet.get_Range("B9", "J9").Cells[1, 1].Value + namHoc;
                sheet.get_Range("C13", "G13").Cells[1, 1] = sheet.get_Range("C13", "G13").Cells[1, 1].Value + soLienLac[i].hoTenHS;
                sheet.get_Range("H13", "I13").Cells[1, 1] = sheet.get_Range("H13", "I13").Cells[1, 1].Value + tenLop;

                sheet.get_Range("C14", "I14").Cells[1, 1] = sheet.get_Range("C14", "I14").Cells[1, 1].Value + soLienLac[i].hoTenCha;
                sheet.get_Range("C15", "I15").Cells[1, 1] = sheet.get_Range("C15", "I15").Cells[1, 1].Value + soLienLac[i].ngheNghiepCha;

                sheet.get_Range("C16", "I16").Cells[1, 1] = sheet.get_Range("C16", "I16").Cells[1, 1].Value + soLienLac[i].hoTenMe;
                sheet.get_Range("C17", "I17").Cells[1, 1] = sheet.get_Range("C17", "I17").Cells[1, 1].Value + soLienLac[i].ngheNghiepMe;

                sheet.get_Range("C18", "I18").Cells[1, 1] = sheet.get_Range("C18", "I18").Cells[1, 1].Value + soLienLac[i].diaChi;
                sheet.get_Range("C19", "I19").Cells[1, 1] = sheet.get_Range("C19", "I19").Cells[1, 1].Value + soLienLac[i].dienThoai;
                sheet.get_Range("C20", "I20").Cells[1, 1] = sheet.get_Range("C20", "I20").Cells[1, 1].Value + tenGV;

                // nội dung cho điểm các môn học và nhận xét
                sheet.get_Range("N5", "N5").Cells[1, 1] = sheet.get_Range("N5", "N5").Cells[1, 1].Value + soLienLac[i].KQMonHocTV;
                sheet.get_Range("O5", "O5").Cells[1, 1] = sheet.get_Range("O5", "O5").Cells[1, 1].Value + soLienLac[i].TV.ToString();

                sheet.get_Range("N6", "N6").Cells[1, 1] = sheet.get_Range("N6", "N6").Cells[1, 1].Value + soLienLac[i].KQMonHocToan;
                sheet.get_Range("O6", "O6").Cells[1, 1] = sheet.get_Range("O6", "O6").Cells[1, 1].Value + soLienLac[i].Toan.ToString();
                if (isLoaiKhoi1 == true)
                {
                    sheet.get_Range("N7", "N7").Cells[1, 1] = sheet.get_Range("N7", "N7").Cells[1, 1].Value + soLienLac[i].KQMonHocTNXH;
                    sheet.get_Range("O7", "O7").Cells[1, 1] = sheet.get_Range("O7", "O7").Cells[1, 1].Value + soLienLac[i].TNXH.ToString();
                }
                else
                {
                    sheet.get_Range("N7", "N7").Cells[1, 1] = sheet.get_Range("N7", "N7").Cells[1, 1].Value + soLienLac[i].KQMonHocKH;
                    sheet.get_Range("O7", "O7").Cells[1, 1] = sheet.get_Range("O7", "O7").Cells[1, 1].Value + soLienLac[i].KH.ToString();
                }

                

                sheet.get_Range("N8", "N8").Cells[1, 1] = sheet.get_Range("N8", "N8").Cells[1, 1].Value + soLienLac[i].KQMonHocLS;
                sheet.get_Range("O8", "O8").Cells[1, 1] = sheet.get_Range("O8", "O8").Cells[1, 1].Value + soLienLac[i].LS.ToString();

                sheet.get_Range("N9", "N9").Cells[1, 1] = sheet.get_Range("N9", "N9").Cells[1, 1].Value + soLienLac[i].KQMonHocDD;
                sheet.get_Range("O9", "O9").Cells[1, 1] = sheet.get_Range("O9", "O9").Cells[1, 1].Value + soLienLac[i].DD.ToString();

                if (isLoaiKhoi1 == true)
                {
                    sheet.get_Range("N10", "N10").Cells[1, 1] = sheet.get_Range("N10", "N10").Cells[1, 1].Value + soLienLac[i].KQMonHocTC;
                    sheet.get_Range("O10", "O10").Cells[1, 1] = sheet.get_Range("O10", "O10").Cells[1, 1].Value + soLienLac[i].TC.ToString();
                }
                else
                {
                    sheet.get_Range("N10", "N10").Cells[1, 1] = sheet.get_Range("N10", "N10").Cells[1, 1].Value + soLienLac[i].KQMonHocKT;
                    sheet.get_Range("O10", "O10").Cells[1, 1] = sheet.get_Range("O10", "O10").Cells[1, 1].Value + soLienLac[i].KT.ToString();
                }
               


                sheet.get_Range("Q5", "Q5").Cells[1, 1] = sheet.get_Range("Q5", "Q5").Cells[1, 1].Value + soLienLac[i].KQMonHocMT;
                sheet.get_Range("R5", "R5").Cells[1, 1] = sheet.get_Range("R5", "R5").Cells[1, 1].Value + soLienLac[i].MT.ToString();

                sheet.get_Range("Q6", "Q6").Cells[1, 1] = sheet.get_Range("Q6", "Q6").Cells[1, 1].Value + soLienLac[i].KQMonHocAN;
                sheet.get_Range("R6", "R6").Cells[1, 1] = sheet.get_Range("R6", "R6").Cells[1, 1].Value + soLienLac[i].AN.ToString();

                sheet.get_Range("Q7", "Q7").Cells[1, 1] = sheet.get_Range("Q7", "Q7").Cells[1, 1].Value + soLienLac[i].KQMonHocTD;
                sheet.get_Range("R7", "R7").Cells[1, 1] = sheet.get_Range("R7", "R7").Cells[1, 1].Value + soLienLac[i].TD.ToString();

                sheet.get_Range("Q8", "Q8").Cells[1, 1] = sheet.get_Range("Q8", "Q8").Cells[1, 1].Value + soLienLac[i].KQMonHocNN;
                sheet.get_Range("R8", "R8").Cells[1, 1] = sheet.get_Range("R8", "R8").Cells[1, 1].Value + soLienLac[i].NN.ToString();

                sheet.get_Range("Q9", "Q9").Cells[1, 1] = sheet.get_Range("Q9", "Q9").Cells[1, 1].Value + soLienLac[i].KQMonHocTinHoc;
                sheet.get_Range("R9", "R9").Cells[1, 1] = sheet.get_Range("R9", "R9").Cells[1, 1].Value + soLienLac[i].TinHoc.ToString();

                sheet.get_Range("Q10", "Q10").Cells[1, 1] = sheet.get_Range("Q10", "Q10").Cells[1, 1].Value + soLienLac[i].KQMonHocTiengDanToc;
                sheet.get_Range("R10", "R10").Cells[1, 1] = sheet.get_Range("R10", "R10").Cells[1, 1].Value + soLienLac[i].TiengDanToc.ToString();
                

                string NXKT = (soLienLac[i].nhanXetGVCN != null && soLienLac[i].nhanXetGVCN.NXKT != null) ? soLienLac[i].nhanXetGVCN.NXKT.ToString() : "";
                string NXNL = (soLienLac[i].nhanXetGVCN != null && soLienLac[i].nhanXetGVCN.NXNL != null) ? soLienLac[i].nhanXetGVCN.NXNL.ToString() : "";
               // string NXNL2 = (soLienLac[i].nhanXetGVCN != null && soLienLac[i].nhanXetGVCN.NXNL != null) ? soLienLac[i].nhanXetGVCN.NXNL.ToString() : "";
               // string NXNL3 = (soLienLac[i].nhanXetGVCN != null && soLienLac[i].nhanXetGVCN.NXNL != null) ? soLienLac[i].nhanXetGVCN.NXNL.ToString() : "";
                string NXPC = (soLienLac[i].nhanXetGVCN != null && soLienLac[i].nhanXetGVCN.NXPC != null) ? soLienLac[i].nhanXetGVCN.NXPC.ToString() : "";

                sheet.get_Range("M13", "R15").Cells[1, 1] = sheet.get_Range("M13", "R15").Cells[1, 1].Value + NXKT;

                sheet.get_Range("M19", "R21").Cells[1, 1] = sheet.get_Range("M19", "R21").Cells[1, 1].Value + NXNL;

                sheet.get_Range("M25", "R27").Cells[1, 1] = sheet.get_Range("M25", "R27").Cells[1, 1].Value + NXPC;

                sheet.get_Range("O17", "O17").Cells[1, 1] = sheet.get_Range("O17", "O17").Cells[1, 1].Value + soLienLac[i].NXNangLucTX1;
                sheet.get_Range("Q17", "Q17").Cells[1, 1] = sheet.get_Range("Q17", "Q17").Cells[1, 1].Value + soLienLac[i].NXNangLucTX2;
                sheet.get_Range("O18", "O18").Cells[1, 1] = sheet.get_Range("O18", "O18").Cells[1, 1].Value + soLienLac[i].NXNangLucTX3;

                sheet.get_Range("O23", "O23").Cells[1, 1] = sheet.get_Range("O23", "O23").Cells[1, 1].Value + soLienLac[i].NXPhamChatTX1;
                sheet.get_Range("R23", "R23").Cells[1, 1] = sheet.get_Range("R23", "R23").Cells[1, 1].Value + soLienLac[i].NXPhamChatTX2;
                sheet.get_Range("O24", "O24").Cells[1, 1] = sheet.get_Range("O24", "O24").Cells[1, 1].Value + soLienLac[i].NXPhamChatTX3;
                sheet.get_Range("R24", "R24").Cells[1, 1] = sheet.get_Range("R24", "R24").Cells[1, 1].Value + soLienLac[i].NXPhamChatTX4;

                string noiDungLenLop = soLienLac[i].DuocLenLop == 0 ? "Hoàn thành chương trình lớp " + malop.Substring(0,1) + " - Được lên lớp" : "" ;
                sheet.get_Range("M27", "R27").Cells[1, 1] = noiDungLenLop;

                List<HocSinh_KhenThuongKyLuat> khenthuongs = (List<HocSinh_KhenThuongKyLuat>)soLienLac[i].KhenThuong;
                if (khenthuongs.Count > 0)
                {
                    sheet.get_Range("M28", "R28").Cells[1, 1] = getNoiDungKhenThuong(khenthuongs);
                }

            }
        }

        private string getNoiDungKhenThuong(List<HocSinh_KhenThuongKyLuat> khenThuongs)
        {
            string noiDung ="";
            foreach (HocSinh_KhenThuongKyLuat khenthuong in khenThuongs) {
              if (!String.IsNullOrEmpty(khenthuong.NoiDung)) {
                  noiDung += khenthuong.NoiDung + ",";

              }
                
            }
            return noiDung;
        }

        public void cungCapThongTinChoTungPhieuLienLacCaNam(List<SoLienLac> soLienLac, string tenDonViChuQuan, string tenTruong, string tenLop, string tenGV, string namHoc, string hocKy)
        {


            for (int i = 0; i < soLienLac.Count; i++)
            {
                int j = i + 1; 

                Excel.Worksheet sheet = ((Excel.Worksheet)_excelApplication.ActiveWorkbook.Worksheets[j]);
                sheet.Select(Type.Missing);

                _excelRange = sheet.get_Range("A2", "I2"); // xác định khoảng để điền dữ liệu
                _excelRange.Cells[1, 1] = tenDonViChuQuan;
                _excelRange = sheet.get_Range("A3", "I3"); // xác định khoảng để điền dữ liệu

                _excelRange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                _excelRange.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                _excelRange.Cells[1, 1] = _excelRange.Cells[1, 1].Value + tenTruong;
                sheet.get_Range("A9", "I9").Cells[1, 1] = sheet.get_Range("A9", "I9").Cells[1, 1].Value + namHoc;
                sheet.get_Range("B13", "F13").Cells[1, 1] = sheet.get_Range("B13", "F13").Cells[1, 1].Value + soLienLac[i].hoTenHS;
                sheet.get_Range("G13", "H13").Cells[1, 1] = sheet.get_Range("G13", "H13").Cells[1, 1].Value + tenLop;

                sheet.get_Range("B14", "H14").Cells[1, 1] = sheet.get_Range("B14", "H14").Cells[1, 1].Value + soLienLac[i].hoTenCha;
                sheet.get_Range("B15", "H15").Cells[1, 1] = sheet.get_Range("B15", "H15").Cells[1, 1].Value + soLienLac[i].ngheNghiepCha;

                sheet.get_Range("B16", "H16").Cells[1, 1] = sheet.get_Range("B16", "H16").Cells[1, 1].Value + soLienLac[i].hoTenMe;
                sheet.get_Range("B17", "H17").Cells[1, 1] = sheet.get_Range("B17", "H17").Cells[1, 1].Value + soLienLac[i].ngheNghiepMe;

                sheet.get_Range("B18", "H18").Cells[1, 1] = sheet.get_Range("B18", "H18").Cells[1, 1].Value + soLienLac[i].diaChi;
                sheet.get_Range("B19", "H19").Cells[1, 1] = sheet.get_Range("B19", "H19").Cells[1, 1].Value + soLienLac[i].dienThoai;
                sheet.get_Range("B20", "H20").Cells[1, 1] = sheet.get_Range("B20", "H20").Cells[1, 1].Value + tenGV;

                // nội dung cho điểm các môn học và nhận xét
                sheet.get_Range("L5", "M5").Cells[1, 1] = sheet.get_Range("L5", "M5").Cells[1, 1].Value + soLienLac[i].Toan;
                sheet.get_Range("N5", "P5").Cells[1, 1] = sheet.get_Range("N5", "P5").Cells[1, 1].Value + soLienLac[i].TV;
                sheet.get_Range("S5", "S5").Cells[1, 1] = sheet.get_Range("S5", "S5").Cells[1, 1].Value + soLienLac[i].TiengDanToc;
                sheet.get_Range("L6", "N6").Cells[1, 1] = sheet.get_Range("L6", "N6").Cells[1, 1].Value + soLienLac[i].LichSu;
                sheet.get_Range("O6", "P6").Cells[1, 1] = sheet.get_Range("O6", "P6").Cells[1, 1].Value + soLienLac[i].KhoaHoc;
                sheet.get_Range("L7", "N7").Cells[1, 1] = sheet.get_Range("L7", "N7").Cells[1, 1].Value + soLienLac[i].NN;
                sheet.get_Range("O7", "P7").Cells[1, 1] = sheet.get_Range("O7", "P7").Cells[1, 1].Value + soLienLac[i].TinHoc;

                string NXKT = (soLienLac[i].nhanXetGVCN != null && soLienLac[i].nhanXetGVCN.NXKT != null) ? soLienLac[i].nhanXetGVCN.NXKT : "";
                string NXNL = (soLienLac[i].nhanXetGVCN != null && soLienLac[i].nhanXetGVCN.NXNL != null) ? soLienLac[i].nhanXetGVCN.NXNL : "";
                string NXPC = (soLienLac[i].nhanXetGVCN != null && soLienLac[i].nhanXetGVCN.NXPC != null) ? soLienLac[i].nhanXetGVCN.NXPC : "";
                string noiDungHocTap = "Môn học và hoạt động giáo dục:" + "\n" + (NXKT.Trim().Length > 0 ? ("- " + NXKT) : "")
                    + "\n\n" + "Năng lực:" + "\n" + (NXNL.Trim().Length > 0 ? ("- " + NXNL) : "") + "\n\n" + "Phẩm chất:" + "\n" +
                    (NXPC.Trim().Length > 0 ? ("- " + NXPC) : "");


                sheet.get_Range("L10", "S19").Cells[1, 1] = noiDungHocTap;
                sheet.get_Range("L20", "R20").Cells[1, 1] = sheet.get_Range("L20", "R20").Cells[1, 1].Value + soLienLac[i].hoanThanhCT;
                sheet.get_Range("L21", "S23").Cells[1, 1] = soLienLac[i].KhenThuong != null ? sheet.get_Range("L21", "S23").Cells[1, 1].Value + getKhenThuong(soLienLac[i].KhenThuong) : "";

                

            }
        }

        public string getKhenThuong(IEnumerable<HocSinh_KhenThuongKyLuat> khenThuongs)
        {
            var enumerator = khenThuongs.GetEnumerator();
            string noiDungKt = "";
            while (enumerator.MoveNext())
            {
                noiDungKt += enumerator.Current.NoiDung + '\n';                
            }

            return noiDungKt;
        }


        public void TaoSoChuNhiemTheoDoiChatLuongGiaoDuc(string fileName, int thang, string namhoc, string tengiaovien, string tenPhongGiaoDuc, string tenlop, string tenTruong, List<HocSinhNhanXetNhom> hsnx)
        {
                Excel.Worksheet sheet = ((Excel.Worksheet)_excelApplication.ActiveWorkbook.Worksheets[1]);
                sheet.Select(Type.Missing);
                _excelApplication.DisplayAlerts = false; // very important when we needn't to ask user "Exits file do you want overwrite.."
                _excelRange = sheet.get_Range("A1", "E1"); // xác định khoảng để điền dữ liệu

                _excelRange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                _excelRange.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                _excelRange.Cells[1, 1] = tenPhongGiaoDuc;
                sheet.get_Range("I1", "L1").Cells[1, 1] = "SỔ THEO DÕI CHẤT LƯỢNG GIÁO DỤC";
                sheet.get_Range("J2", "J2").Cells[1, 1] = sheet.get_Range("J2", "J2").Cells[1, 1].Value + " " + thang.ToString();
                sheet.get_Range("A2", "E2").Cells[1, 1] = tenTruong;
                sheet.get_Range("J3", "J3").Cells[1, 1] = tengiaovien;
                sheet.get_Range("M3", "M3").Cells[1, 1] = tenlop;

              
                int j = 6;
                for (int i = 0; i < hsnx.Count; i++)
                {
                    sheet.get_Range("A" + (j + i).ToString(), "A" + (j + i).ToString()).Cells[1, 1] = (i+1).ToString();
                    sheet.get_Range("B" + (j + i).ToString(), "D" + (j + i).ToString()).Cells[1, 1] = hsnx[i].tenHocSinh.ToString();
                    sheet.get_Range("E" + (j + i).ToString(), "E" + (j + i).ToString()).Cells[1, 1] = String.Format("{0:dd/MM/yyyy}", hsnx[i].ngaySinh);
                    sheet.get_Range("F" + (j + i).ToString(), "H" + (j + i).ToString()).Cells[1, 1] = hsnx[i].nhanXetKienThuc;
                    sheet.get_Range("I" + (j + i).ToString(), "K" + (j + i).ToString()).Cells[1, 1] = hsnx[i].nhanXetNangLuc;
                    sheet.get_Range("L" + (j + i).ToString(), "N" + (j + i).ToString()).Cells[1, 1] = hsnx[i].nhanXetPhamChat;
 
                }
        }



        public void TaoSoBoMonTheoDoiChatLuongGiaoDuc(string fileName, int thang, string namhoc, string tengiaovien, string tenPhongGiaoDuc, string tenlop,string tenTruong, List<HocSinhNhanXetThang> hsnx)
        {
                Excel.Worksheet sheet = ((Excel.Worksheet)_excelApplication.ActiveWorkbook.Worksheets[1]);
                sheet.Select(Type.Missing);
                _excelApplication.DisplayAlerts = false; // very important when we needn't to ask user "Exits file do you want overwrite.."
                _excelRange = sheet.get_Range("A1", "E1"); // xác định khoảng để điền dữ liệu

                _excelRange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                _excelRange.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                _excelRange.Cells[1, 1] = tenPhongGiaoDuc;
                sheet.get_Range("I1", "L1").Cells[1, 1] = "SỔ THEO DÕI CHẤT LƯỢNG GIÁO DỤC";
                sheet.get_Range("J2", "J2").Cells[1, 1] = sheet.get_Range("J2", "J2").Cells[1, 1].Value + " " + thang.ToString();
                sheet.get_Range("A2", "E2").Cells[1, 1] = tenTruong;
                sheet.get_Range("J3", "J3").Cells[1, 1] = tengiaovien;
                sheet.get_Range("M3", "M3").Cells[1, 1] = tenlop;

              
                int j = 6;
                for (int i = 0; i < hsnx.Count; i++)
                {
                    sheet.get_Range("A" + (j + i).ToString(), "A" + (j + i).ToString()).Cells[1, 1] = (i+1).ToString();
                    sheet.get_Range("B" + (j + i).ToString(), "D" + (j + i).ToString()).Cells[1, 1] = hsnx[i].HoTen.ToString();
                    sheet.get_Range("E" + (j + i).ToString(), "E" + (j + i).ToString()).Cells[1, 1] = String.Format("{0:dd/MM/yyyy}", hsnx[i].ngaySinh);
                    sheet.get_Range("F" + (j + i).ToString(), "H" + (j + i).ToString()).Cells[1, 1] = hsnx[i].NXKT;
                    sheet.get_Range("I" + (j + i).ToString(), "K" + (j + i).ToString()).Cells[1, 1] = hsnx[i].NXNL;
                    sheet.get_Range("L" + (j + i).ToString(), "N" + (j + i).ToString()).Cells[1, 1] = hsnx[i].NXPC;
 
                }
        }


        public void TaoSoNhapDiem(string fileName,string namhoc, string tengiaovien, string tenPhongGiaoDuc, string tenlop, string tenTruong, DataTable table)
        {
            Excel.Worksheet sheet = ((Excel.Worksheet)_excelApplication.ActiveWorkbook.Worksheets[1]);
            sheet.Select(Type.Missing);
            _excelApplication.DisplayAlerts = false; // very important when we needn't to ask user "Exits file do you want overwrite.."
            _excelRange = sheet.get_Range("A1", "E1"); // xác định khoảng để điền dữ liệu


            //_excelRange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
            //_excelRange.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
            _excelRange.Cells[1, 1] = tenPhongGiaoDuc;

            
            sheet.get_Range("F1", "N1").Cells[1, 1] = "SỔ NHẬP ĐIỂM";
           // sheet.get_Range("J2", "J2").Cells[1, 1] = sheet.get_Range("J2", "J2").Cells[1, 1].Value + " " ;
            sheet.get_Range("A2", "E2").Cells[1, 1] = tenTruong;
            sheet.get_Range("E3", "J3").Cells[1, 1] = tengiaovien;
          
            sheet.get_Range("M3", "M3").Cells[1, 1] =  sheet.get_Range("J3", "J3").Cells[1, 1].Value + tenlop.ToString() ;
            
            // add them ten cot vao
            int cols = 5;
            
            foreach (DataRow row in table.Rows)
            {
                foreach (DataColumn col in table.Columns)
                {
                     if (col.ColumnName != "MaHocSinh" && col.ColumnName != "HoTen" && col.ColumnName != "NgaySinh")
                        {

                            sheet.get_Range(index[cols].ToString() + (5).ToString(), index[cols].ToString() + (5).ToString()).Cells.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                            
                            sheet.get_Range(index[cols].ToString() + (5).ToString(), index[cols].ToString() + (5).ToString()).Cells[1, 1] = col.ColumnName.ToString();
                          
                         //Excel.Borders borders = sheet.get_Range(index[cols].ToString() + (5).ToString(), index[cols].ToString() + (5).ToString()).Borders;
                            ////Set the thin lines style.
                            //borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                            //borders.Weight = 2d;

                            //borders[Excel.XlBordersIndex.xlEdgeLeft].Weight = 1d;
                            //borders[Excel.XlBordersIndex.xlEdgeRight].Weight = 1d;
                            //borders[Excel.XlBordersIndex.xlEdgeTop].Weight = 1d;
                            //borders[Excel.XlBordersIndex.xlEdgeBottom].Weight = 1d;

                            ////borders.Weight = L.XlBorderWeight.xlMedium;
                         

                         
                         cols++; 
                        }
                }
                break;
            }

            // them noi dung 
            int j = 6;
            int stt =1;
            foreach (DataRow row in table.Rows)
            {
                int i=5;
                sheet.get_Range("A" + (j).ToString(), "A" + (j).ToString()).Cells[1, 1] =stt.ToString();
                foreach (DataColumn col in table.Columns)
                {
                    
                    if (col.ColumnName == "HoTen")
                    {
                        //sheet.get_Range("B" + (j).ToString(), "D" + (j).ToString()).Cells.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                        sheet.get_Range("B" + (j).ToString(), "D" + (j).ToString()).Cells[1, 1] = row[col.ColumnName].ToString();
                    }else if (col.ColumnName == "NgaySinh")
                        {
                            string ngaySinh ="";
                            if (row[col.ColumnName].ToString().Length > 0) {
                               DateTime nsinh = ((DateTime)row[col.ColumnName]);
                               ngaySinh = nsinh.Day + "/" + nsinh.Month + "/" + nsinh.Year;
                            }
                            sheet.get_Range("E" + (j).ToString(), "E" + (j).ToString()).Cells.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                            sheet.get_Range("E" + (j).ToString(), "E" + (j).ToString()).Cells[1, 1] = ngaySinh;
                        }
                    else if (col.ColumnName != "MaHocSinh")
                        {
                            sheet.get_Range(index[i].ToString() + (j).ToString(), index[i].ToString() + (j).ToString()).Cells.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                            sheet.get_Range(index[i].ToString() + (j).ToString(), index[i].ToString() + (j).ToString()).Cells[1, 1] = row[col.ColumnName].ToString();
                            i++; 
                        }
                }
                    stt ++;
                    j++;
            }
           
        }



       
        /// <summary>
        /// activate the excel application
        /// </summary>
        protected virtual void ActivateExcel()
        {
         //   _excelApplication = new Excel.Application();
         //   _workBooks = (Excel.Workbooks)_excelApplication.Workbooks;
           // _workBook = (Excel._Workbook)(_workBooks.Add(_value));
           // _workBook = (Excel._Workbook)(_workBooks.Add(1));
            
            _excelSheets = (Excel.Sheets)_workBook.Worksheets;
            
           _excelSheet = (Excel._Worksheet)(_excelSheets.get_Item(1));
           // _excelSheet.Name = "Nguyễn Văn Tuấn";
        }


        /// <summary>
        /// Fill the excel sheet with data along with the position specified
        /// </summary>
        /// <param name="columnrow"></param>
        /// <param name="data"></param>
        private void FillExcelWithData()
        {
            _excelRange = _excelSheet.get_Range("A" + StartRow, _value);
            _excelRange = _excelRange.get_Resize(RowCount , ColumnCount); 
            _excelRange.Value=ExcelData; 

            _excelRange.EntireColumn.AutoFit();
        }
        /// <summary>
        /// save the excel sheet to the location with file name
        /// </summary>
        /// <param name="fileName"></param>
        protected virtual void SaveExcel(string fileName)
        {
            
             
            //_workBook.Saved = true;          
            _workBook.SaveAs(fileName);
            _workBook.Close(false, _value, _value);
            

            if (_excelApplication != null)
            {
                _excelApplication.Quit();
                if (_excelSheets != null)
                {
                    releaseObject(_excelSheets);
                }
                if (_workBook != null)
                {
                    releaseObject(_workBook);
                }
                releaseObject(_excelApplication);
            }

            //if (File.Exists(fileName))
            //    File.Delete(fileName);
        }
        private void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                //MessageBox.Show("Exception Occured while releasing object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }
        /// <summary>
        /// make the range of rows bold
        /// </summary>
        /// <param name="cell1"></param>
        /// <param name="cell2"></param>
        protected void BoldCell(string cell1, string cell2, bool merge = false)
        {
            _excelRange = _excelSheet.get_Range(cell1, cell2);
            
            _excelFont = _excelRange.Font;
          
            _excelFont.Bold = true;
            if (merge)
            {
                _excelRange.Merge();
                   _excelRange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                   _excelRange.VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;

            }
        }

        /// <summary>
        /// Ham nay se thuc hien merge cell nhung khong bolder
        /// </summary>
        /// <param name="cell1"></param>
        /// <param name="cell2"></param>
        /// <param name="merge"></param>
        protected void MergeCell(string cell1, string cell2, bool merge = false, bool alignment=false)
        {
            _excelRange = _excelSheet.get_Range(cell1, cell2);
            _excelFont = _excelRange.Font; 
            _excelFont.Bold = false;
            if (merge)
            {
                _excelRange.Merge();
                 _excelRange.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
                 _excelRange.VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;

            }
            if (alignment)// neu align= true thi se canh giua
            {
                _excelRange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                _excelRange.VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;

            }
        }
   
        protected void BorderRow(string cell1, string cell2)
        {
            _excelRange = _excelSheet.get_Range(cell1, cell2);
            _excelRange.Merge();
            _excelRange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
          

        }

        protected void setWidth(string cell, double size) 
        {
            _excelRange = _excelSheet.get_Range(cell);
            _excelRange.ColumnWidth = size;
            
        }

        protected void setFontAndSize(string cell, string fontName, float size)
        {
            _excelRange = _excelSheet.get_Range(cell);
            _excelRange.Style.Font.Name = fontName;
            _excelRange.Cells.Font.Size = size;

        }

    }
}
