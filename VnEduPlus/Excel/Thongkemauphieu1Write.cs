﻿using System.Collections.Generic;
using VnEduPlus.Models;
namespace VnEduPlus
{
    public class Thongkemauphieu1Write : ExcelFileWriter<object>
    {
        public object[,] myExcelData;
        private int myRowCnt;
        private int startRow = 5;


        string ColumnName(int index)
        {
            char[] chars = new char[] { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };

            int quotient = index / 26;
            if (quotient > 0)
                return ColumnName(quotient - 1) + chars[index % 26].ToString();
            else
                return chars[index % 26].ToString();
        }


        public override void FillRowDataEntities(List<HocSinhDiem> list, bool viewAllColumns)
        {

             
        }

        public override void FillRowDataEntities(List<HocSinhDiem> list)
        {


        }

        public override void FillRowData(List<object> list, bool viewAllColumns)
        {

            myRowCnt = list.Count;
            myExcelData = new object[RowCount, ColumnCount];

            BoldCell("A" + (StartRow), ColumnName(ColumnCount - 1) + (StartRow), true);
            BoldCell("A" + (StartRow + 1), ColumnName(ColumnCount - 1) + (StartRow + 1));
            for (int row = 0; row < myRowCnt; row++)
            {
                string[] values = (string[])list[row];
                for (int col = 0; col < values.Length; col++)
                {
                    myExcelData[row, col] = values[col];
                }
            }

            BorderRow("A" + (StartRow + 1), ColumnName(ColumnCount - 1) + (StartRow + myRowCnt));
        }

        public override object[,] ExcelData
        {
            get
            {
                return myExcelData;
            }
        }


        public override int RowCount
        {
            get
            {
                return myRowCnt;
            }
        }

        public override int StartRow
        {
            get
            {
                return startRow;
            }
        }
    }
}