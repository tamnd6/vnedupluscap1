﻿using System.Collections.Generic;
using System.IO;
using System.Reflection;
using VnEduPlus.Models;
using VnEduPlus.CmonFunction;


namespace VnEduPlus
{
    public abstract class ExcelFileWriter<T>
    {
        private Excel.Application _excelApplication = null;
        private Excel.Workbooks _workBooks = null;
        private Excel._Workbook _workBook = null;
        private object _value = Missing.Value;
        private Excel.Sheets _excelSheets = null;
        private Excel._Worksheet _excelSheet = null;
        private Excel.Range _excelRange = null;
        private Excel.Font _excelFont = null;
        //private int  columnCount=30;
        private int columnCount = 32;
        /// <summary>
        /// user have to parse the data from the list and pass each data along with the
        /// column and row name to the base fun, FillExcelWithData().
        /// </summary>
        /// <param name="list"></param>
        public abstract void FillRowData(List<T> list, bool viewAllColumns =true);

        public abstract void FillRowDataEntities(List<HocSinhDiem> list, bool viewAllColumns = true);
        public abstract void FillRowDataEntities(List<HocSinhDiem> list);

        /// <summary>
        /// get the data of object which will be saved to the excel sheet
        /// </summary>
        public abstract object[,] ExcelData { get; }
        /// <summary>
        /// get the no of columns
        /// </summary>
        public int ColumnCount
        {
            get
            {
                return columnCount;
            }
            set
            {
                columnCount = value;
            }
        }
        /// <summary>
        /// get the now of rows to fill
        /// </summary>
        public abstract int RowCount { get; }

        /// <summary>
        /// get the start roeto fill
        /// </summary>
        public abstract int StartRow { get; }

        /// <summary>
        /// user can override this to make the headers not be in bold.
        /// by default it is true
        /// </summary>
        protected virtual Excel._Worksheet ExcelSheet
        {
            get
            {
                return _excelSheet;
            }
        }

        /// <summary>
        /// user can override this to make the headers not be in bold.
        /// by default it is true
        /// </summary>
        protected virtual bool BoldHeaders
        {
            get
            {
                return true;
            }
        }


        protected void FormatDataCell(string cell1, string cell2)
        {
            _excelRange = _excelSheet.get_Range(cell1, cell2);
            _excelRange.NumberFormat = "dd/MM/yyyy;@";
        }


        public void WriteDateToExcel(string fileName, List<HocSinhDiem> list, string truong, string lop, string hocky, string namhoc, bool viewAllColumns = false)
        {
            this.ActivateExcel();

            object[,] info = new object[2, 2];
            info[0, 0] = "Trường";
            info[0, 1] = truong;
            info[1, 0] = "Lớp";
            info[1, 1] = lop;
            _excelRange = _excelSheet.get_Range("A" + (StartRow - 3), _value);
            _excelRange = _excelRange.get_Resize(2, 2);
            _excelRange.NumberFormat = "@";
            // _excelRange.set_Value(Missing.Value, info);
            _excelRange.Cells[1, 1] = "Trường : " + truong;
            _excelRange.Cells[2, 1] = "Lớp : " + lop;
            _excelRange.Cells[3, 2] = hocky;
            _excelRange.Cells[3, 3] = "Năm học : " + namhoc;

            // _excelRange.Value

            BoldCell("A" + (StartRow - 3), "A" + (StartRow - 2));
           
            this.FillRowDataEntities(list);// dang thu gon 
            

            this.FillExcelWithData();
            this.SaveExcel(fileName);

        }

         
        /// <summary>
        /// Tuannv
        ///  Mon nhan xet
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="list"></param>
        /// <param name="truong"></param>
        /// <param name="lop"></param>
        /// <param name="hocky"></param>
        /// <param name="monhoc"></param>
        /// <param name="namhoc"></param>
        /// <param name="MonTinhDiem"></param>
        /// <param name="viewAllColumns"></param>
          public void WriteDateToExcelHocSinhDiem(string fileName, List<HocSinhDiem> list, string truong, string lop, string hocky, string monhoc, string namhoc, string MonTinhDiem, bool viewAllColumns = false)
          {
             // this.ActivateExcel(fileName);
              this.ActivateExcel();
              object[,] info = new object[2, 2];
              info[0, 0] = "Trường";
              info[0, 1] = truong;
              info[1, 0] = "Lớp";
              info[1, 1] = lop;
              _excelRange = _excelSheet.get_Range("A" + (StartRow - 3), _value);
              _excelRange = _excelRange.get_Resize(2, 2);
              _excelRange.NumberFormat = "@";
              // _excelRange.set_Value(Missing.Value, info);
              _excelRange.Cells[1, 1] = "Trường : " + truong;
              _excelRange.Cells[2, 1] = "Lớp : " + lop;
              _excelRange.Cells[3, 1] = "Môn : " + monhoc;
              _excelRange.Cells[3, 2] = hocky;
              _excelRange.Cells[3, 3] = "Năm học : " + namhoc;

              // _excelRange.Value

              BoldCell("A" + (StartRow - 3), "A" + (StartRow - 2));
              if (viewAllColumns)
              {
                  this.FillRowDataEntities(list, viewAllColumns);// dang day du 

              }
              else
              {
                  this.FillRowDataEntities(list);// dang thu gon 
              }

             // this.FillRowDataEntities(list, viewAllColumns);
              this.FillExcelWithData();

              this.SaveExcel(fileName);
          }


        /// <summary>
        /// activate the excel application
        /// </summary>
        protected virtual void ActivateExcel()
        {
            _excelApplication = new Excel.Application(); 
            _workBooks = (Excel.Workbooks)_excelApplication.Workbooks;
            _workBook = (Excel._Workbook)(_workBooks.Add(_value));
            _excelSheets = (Excel.Sheets)_workBook.Worksheets;
            _excelSheet = (Excel._Worksheet)(_excelSheets.get_Item(1));
        }

        /// <summary>
        /// activate the excel application
        /// </summary>
        protected virtual void ActivateExcel(string path)
        {
            _excelApplication = new Excel.Application();

            _workBooks = (Excel.Workbooks)_excelApplication.Workbooks.Open(path, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value);
            _workBook = (Excel.Workbook)_excelApplication.Workbooks.Open(path, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value);
           
          //  _workBook = (Excel._Workbook)(_workBooks.Add(_value));
         //   _excelSheets = (Excel.Sheets)_workBook.Worksheets;
            
            //_excelSheet = (Excel._Worksheet)(_excelSheets.get_Item(1));
            _excelSheets = _workBooks.Application.Worksheets;
            _excelSheet = (Excel._Worksheet)(_excelSheets.get_Item(1));
        }

        /// <summary>
        /// Fill the excel sheet with data along with the position specified
        /// </summary>
        /// <param name="columnrow"></param>
        /// <param name="data"></param>
        private void FillExcelWithData()
        {
            _excelRange = _excelSheet.get_Range("A" + StartRow, _value);
            _excelRange = _excelRange.get_Resize(RowCount , ColumnCount);

           // _excelRange.set_Value(Missing.Value, ExcelData);
            _excelRange.Value = ExcelData;
            _excelRange.EntireColumn.AutoFit();
        }
        /// <summary>
        /// save the excel sheet to the location with file name
        /// </summary>
        /// <param name="fileName"></param>
        protected virtual void SaveExcel(string fileName)
        {
            if (File.Exists(fileName))
               // File.Delete(fileName);

            _workBook.Saved = true;
            _workBook.SaveCopyAs(fileName);
           // _workBook.Save();

        //    _workBook.SaveAs(fileName, _value, _value,
           //     _value, _value, _value, Excel.XlSaveAsAccessMode.xlNoChange,
          //      _value, _value, _value, _value, null);
            _workBook.Close(false, _value, _value);
            _excelApplication.Quit();
        }
        /// <summary>
        /// make the range of rows bold
        /// </summary>
        /// <param name="cell1"></param>
        /// <param name="cell2"></param>
        protected void BoldCell(string cell1, string cell2, bool merge = false)
        {
            _excelRange = _excelSheet.get_Range(cell1, cell2);
            _excelFont = _excelRange.Font;
            _excelFont.Bold = true;
            if (merge)
            {
                _excelRange.Merge();
                //   _excelRange.Style.HorizontallAlignment = Excel.XlHAlign.xlHAlignCenter;
                //   _excelRange.Style.VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;

            }
        }

   
        protected void BorderRow(string cell1, string cell2)
        {
            _excelRange = _excelSheet.get_Range(cell1, cell2);

            _excelRange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

        }
        
        /// <summary>
        /// Nguyen anh
        /// </summary>
        /// <param name="cell1"></param>
        /// <param name="cell2"></param>
        /// <param name="ExAlign"></param>
        /// <param name="merge"></param>
        protected void BoldCell(string cell1, string cell2, Cmon.ExcelAlign ExAlign, bool merge = false)
        {
            _excelRange = _excelSheet.get_Range(cell1, cell2);
            _excelFont = _excelRange.Font;
            _excelFont.Bold = true;
            if (merge)
            {
                _excelRange.Merge();

                //   _excelRange.Style.VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
                switch (ExAlign)
                {
                    case Cmon.ExcelAlign.Center:
                        {
                            _excelRange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                            _excelRange.VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
                        }
                        break;
                    case Cmon.ExcelAlign.Justify:
                        {
                            _excelRange.HorizontalAlignment = Excel.XlHAlign.xlHAlignJustify;
                            _excelRange.VerticalAlignment = Excel.XlVAlign.xlVAlignJustify;
                        }
                        break;
                    case Cmon.ExcelAlign.left:
                        _excelRange.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
                        break;
                    case Cmon.ExcelAlign.right:
                        _excelRange.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight;
                        break;
                    default:
                        _excelRange.HorizontalAlignment = Excel.XlHAlign.xlHAlignGeneral;
                        break;
                }

            }
        }

    }
}
