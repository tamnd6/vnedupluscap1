using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace VnEduPlus
{
    public class ExcelFileReader
    {
        private Excel.Application _excelApplication = null;
        private Excel.Workbooks _workBooks = null;
        private Excel._Workbook _workBook = null;
        private object _value = Missing.Value;
        private Excel.Sheets _excelSheets = null;
        private Excel._Worksheet _excelSheet = null;
        private Excel.Range _excelRange = null;
        private Excel.Font _excelFont = null;



        protected virtual Excel._Worksheet ExcelSheet
        {
            get
            {
                return _excelSheet;
            }
        }


        /// <summary>
        /// activate the excel application
        /// </summary>
        protected virtual void ActivateExcel()
        {
            _excelApplication = new Excel.Application();
            _workBooks = (Excel.Workbooks)_excelApplication.Workbooks;
            _workBook = (Excel._Workbook)(_workBooks.Add(_value));
            _excelSheets = (Excel.Sheets)_workBook.Worksheets;
            _excelSheet = (Excel._Worksheet)(_excelSheets.get_Item(1));
        }

        public IList<System.Array> ReadBangDiem(string fileName, out bool viewAllColumns)
        {
            _excelApplication = new Excel.Application();
            _workBooks = (Excel.Workbooks)_excelApplication.Workbooks;
            _workBook = _workBooks.Open(fileName, _value, _value, _value, _value, _value, _value, _value, _value, _value, _value, _value, _value);

            _excelSheets = (Excel.Sheets)_workBook.Worksheets;
            _excelSheet = (Excel._Worksheet)(_excelSheets.get_Item(1));
            

            
            int i = 6;

            IList<System.Array> results = new List<System.Array>();
            System.Array myvalues;

            Excel.Range range = _excelSheet.get_Range("AB" + i.ToString(), "AB" + i.ToString());
            //    myvalues = (System.Array)range.Cells.Value;
            if (range.Cells.Value != null)
                viewAllColumns = true;
            else
                viewAllColumns = false;

            // i++;
            // range = _excelSheet.get_Range("A" + i.ToString(), "AD" + i.ToString());
            while (true)
            {
                i++;
                range = _excelSheet.get_Range("A" + i.ToString(), "AD" + i.ToString());
                myvalues = (System.Array)range.Cells.Value;

                if (myvalues.GetValue(1, 1) == null)
                    break;

                results.Add(myvalues);
                
            }
            
            _workBook.Close(false, _value, _value);
            
            _excelApplication.Quit();
            return results;
        }


        public IList<System.Array> ReadHanhKiem(string fileName)
        {
            _excelApplication = new Excel.Application();
            _workBooks = (Excel.Workbooks)_excelApplication.Workbooks;
            _workBook = _workBooks.Open(fileName, _value, _value, _value, _value, _value, _value, _value, _value, _value, _value, _value);

            _excelSheets = (Excel.Sheets)_workBook.Worksheets;
            _excelSheet = (Excel._Worksheet)(_excelSheets.get_Item(1));


            int i = 6;

            IList<System.Array> results = new List<System.Array>();
            System.Array myvalues;

            Excel.Range range;
            while (true)
            {
                i++;
                range = _excelSheet.get_Range("A" + i.ToString(), "I" + i.ToString());
                myvalues = (System.Array)range.Cells.Value;

                if (myvalues.GetValue(1, 1) == null)
                    break;

                myvalues.SetValue(range.Cells[1, 4].Text, 1, 4);

                results.Add(myvalues);
            }

            _workBook.Close(false, _value, _value);
            _excelApplication.Quit();
            return results;
        }




        public IList<System.Array> ReadDiemDanh(string fileName)
        {
            _excelApplication = new Excel.Application();
            _workBooks = (Excel.Workbooks)_excelApplication.Workbooks;
            _workBook = _workBooks.Open(fileName, _value, _value, _value, _value, _value, _value, _value, _value, _value, _value, _value);
            
            _excelSheets = (Excel.Sheets)_workBook.Worksheets;
            _excelSheet = (Excel._Worksheet)(_excelSheets.get_Item(1));


            int i = 7;

            IList<System.Array> results = new List<System.Array>();
            System.Array myvalues;

            Excel.Range range;
            while (true)
            {
                i++;
                range = _excelSheet.get_Range("B" + i.ToString(), "AH" + i.ToString());
                myvalues = (System.Array)range.Cells.Value;

                if (myvalues.GetValue(1, 1) == null)
                    break;

                results.Add(myvalues);
            }

            _workBook.Close(false, _value, _value);
            _excelApplication.Quit();
            return results;
        }



        public IList<System.Array> ReadDiemMonHoc(string fileName, string state, int rows, string column)
        {
            _excelApplication = new Excel.Application();
            _workBooks = (Excel.Workbooks)_excelApplication.Workbooks;
            _workBook = _workBooks.Open(fileName, _value, _value, _value, _value, _value, _value, _value, _value, _value, _value, _value);

            _excelSheets = (Excel.Sheets)_workBook.Worksheets;
            _excelSheet = (Excel._Worksheet)(_excelSheets.get_Item(1));


            int i = rows;// dong thu 6 trong file excel

            IList<System.Array> results = new List<System.Array>();
            System.Array myvalues;

            Excel.Range range;
            while (true)
            {
                i++;
                if (state == "0")
                {
                    range = _excelSheet.get_Range("A" + i.ToString(), column + i.ToString());
                    myvalues = (System.Array)range.Cells.Value;
                    if (myvalues.GetValue(1, 1) == null)
                        break;

                    results.Add(myvalues);
                }
                else if (state == "1")
                {
                    range = _excelSheet.get_Range("A" + i.ToString(), column + i.ToString());
                    myvalues = (System.Array)range.Cells.Value;
                    if (myvalues.GetValue(1, 1) == null)
                        break;

                    results.Add(myvalues);
                }




            }

            _workBook.Close(false, _value, _value);
            _excelApplication.Quit();
            return results;
        }

        public IList<System.Array> ReadDiemMonHoc(string fileName,string state)
        {
            _excelApplication = new Excel.Application();
            _workBooks = (Excel.Workbooks)_excelApplication.Workbooks;
            _workBook = _workBooks.Open(fileName, _value, _value, _value, _value, _value, _value, _value, _value, _value, _value, _value);
            
            _excelSheets = (Excel.Sheets)_workBook.Worksheets;
            _excelSheet = (Excel._Worksheet)(_excelSheets.get_Item(1));


            int i = 6;// dong thu 6 trong file excel

            IList<System.Array> results = new List<System.Array>();
            System.Array myvalues;

            Excel.Range range;
            while (true)
            {
                i++;
                if (state == "0")
                {
                    range = _excelSheet.get_Range("A" + i.ToString(), "V" + i.ToString());
                    myvalues = (System.Array)range.Cells.Value;
                    if (myvalues.GetValue(1, 1) == null)
                        break;

                    results.Add(myvalues);
                }
                else if (state == "1") {
                    range = _excelSheet.get_Range("A" + i.ToString(), "AD" + i.ToString());
                    myvalues = (System.Array)range.Cells.Value;
                    if (myvalues.GetValue(1, 1) == null)
                        break;

                    results.Add(myvalues);
                }

               

               
            }

            _workBook.Close(false, _value, _value);
            _excelApplication.Quit();
            return results;
        }


        






    }
}
