﻿using System.Collections.Generic;
using VnEduPlus.Models;
namespace VnEduPlus
{
    public class ExcelWrite : ExcelFileWriter<object>
    {
        public object[,] myExcelData;
        private int myRowCnt;
 
        private int startRow = 5;
        //private int columnCount=30;
        private int columnCount = 32;
        string ColumnName(int index)
        {
            char[] chars = new char[] { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };

            int quotient = index / 26;
            if (quotient > 0)
                return ColumnName(quotient-1) + chars[index % 26].ToString();
            else
                return chars[index % 26].ToString();
        }


        public override void FillRowData(List<object> list, bool viewAllColumns)
        {

            myRowCnt = list.Count;
            myExcelData = new object[RowCount, ColumnCount];


            myExcelData[0, 0] = "STT";
            BoldCell("A" + StartRow, "A" + (StartRow + 1), true);

            myExcelData[0, 1] = "Mã HS";
            BoldCell("B" + StartRow, "B" + (StartRow + 1), true);

            myExcelData[0, 2] = "Họ và tên";
            BoldCell("C" + StartRow, "C" + (StartRow + 1), true);

            int columnIndex = 2;
            myExcelData[0, columnIndex + 1] = "Miệng";
            BoldCell("D" + StartRow, "H" + (StartRow), true);
            for (int i = 1; i <= 5; i++)
            {
                columnIndex++;
                myExcelData[1, columnIndex] = "M" + i.ToString();
            }


            myExcelData[0, columnIndex + 1] = "Viết 15'";
            BoldCell(ColumnName(columnIndex + 1) + StartRow, ColumnName(columnIndex + 5) + (StartRow), true);
            for (int i = 1; i <= 5; i++)
            {
                columnIndex++;
                myExcelData[1, columnIndex] = "V" + i.ToString();
            }

            if (viewAllColumns)
            {
                myExcelData[0, columnIndex + 1] = "TH 15'";
                BoldCell(ColumnName(columnIndex + 1) + StartRow, ColumnName(columnIndex + 5) + (StartRow), true);
                for (int i = 1; i <= 5; i++)
                {
                    columnIndex++;
                    myExcelData[1, columnIndex] = "TH" + i.ToString();
                }
            }


            myExcelData[0, columnIndex + 1] = "Viết 45'";
            BoldCell(ColumnName(columnIndex + 1) + StartRow, ColumnName(columnIndex + 5) + (StartRow), true);
            for (int i = 1; i <= 5; i++)
            {
                columnIndex++;
                myExcelData[1, columnIndex] = "V" + i.ToString();
            }

            if (viewAllColumns)
            {
                myExcelData[0, columnIndex + 1] = "TH 45'";
                BoldCell(ColumnName(columnIndex + 1) + StartRow, ColumnName(columnIndex + 5) + (StartRow), true);
                for (int i = 1; i <= 5; i++)
                {
                    columnIndex++;
                    myExcelData[1, columnIndex] = "TH" + i.ToString();
                }
            }
            else
            {
                myExcelData[0, columnIndex + 1] = "TH 45'";
                BoldCell(ColumnName(columnIndex + 1) + StartRow, ColumnName(columnIndex + 2) + (StartRow), true);
                for (int i = 1; i <= 2; i++)
                {
                    columnIndex++;
                    myExcelData[1, columnIndex] = "TH" + i.ToString();
                }
            }

            columnIndex++;
            myExcelData[0, columnIndex] = "HK";
            BoldCell(ColumnName(columnIndex) + StartRow, ColumnName(columnIndex) + (StartRow + 1), true);

            columnIndex++;
            myExcelData[0, columnIndex] = "TBM";
            BoldCell(ColumnName(columnIndex) + StartRow, ColumnName(columnIndex) + (StartRow + 1), true);

            for (int row = 0; row < myRowCnt; row++)
            {
                myExcelData[row + 2, 0] = (row + 1).ToString();
                List<string> values = (List<string>)list[row];
                if (viewAllColumns)// in day day 
                {

                    for (int col = 0; col < values.Count; col++)
                    {
                        myExcelData[row + 2, col + 1] = values[col];
                    }
                }
                else // in theo customes
                {
                    for (int col = 0; col < 12; col++)// values.Count
                    {
                        
                        myExcelData[row + 2, col + 1] = values[col];
                    }

                    for (int col = 17; col < 24; col++)// values.Count
                    {
                        myExcelData[row + 2, col -4] = values[col];
                    }

                    for (int col = 27; col < 29; col++)// values.Count
                    {
                        myExcelData[row + 2, col -7] = values[col];
                    }

                }
            }

            BorderRow("A" + StartRow, ColumnName(columnIndex) + (StartRow + 1 + myRowCnt));
        }


        /// <summary>
        /// dang thu gon 
        /// </summary>
        /// <param name="list"></param>
        public override void FillRowDataEntities(List<HocSinhDiem> list)
        {
            myRowCnt = list.Count;
            myExcelData = new object[RowCount, ColumnCount];


            myExcelData[0, 0] = "STT";
            BoldCell("A" + StartRow, "A" + (StartRow + 1), true);

            myExcelData[0, 1] = "Mã HS";
            BoldCell("B" + StartRow, "B" + (StartRow + 1), true);

            myExcelData[0, 2] = "Họ và tên";
            BoldCell("C" + StartRow, "C" + (StartRow + 1), true);

            int columnIndex = 2;
            myExcelData[0, columnIndex + 1] = "Miệng";
            BoldCell("D" + StartRow, "H" + (StartRow), true);
            for (int i = 1; i <= 5; i++)
            {
                columnIndex++;
                myExcelData[1, columnIndex] = "M" + i.ToString();
            }

            myExcelData[0, columnIndex + 1] = "Viết 15'";
            BoldCell(ColumnName(columnIndex + 1) + StartRow, ColumnName(columnIndex + 5) + (StartRow), true);
            for (int i = 1; i <= 5; i++)
            {
                columnIndex++;
                myExcelData[1, columnIndex] = "V" + i.ToString();
            }             

            myExcelData[0, columnIndex + 1] = "Viết 45'";
            BoldCell(ColumnName(columnIndex + 1) + StartRow, ColumnName(columnIndex + 5) + (StartRow), true);
            for (int i = 1; i <= 5; i++)
            {
                columnIndex++;
                myExcelData[1, columnIndex] = "V" + i.ToString();
            }

            
                myExcelData[0, columnIndex + 1] = "TH 45'";
                BoldCell(ColumnName(columnIndex + 1) + StartRow, ColumnName(columnIndex + 2) + (StartRow), true);
                for (int i = 1; i <= 2; i++)
                {
                    columnIndex++;
                    myExcelData[1, columnIndex] = "TH" + i.ToString();
                }            

            columnIndex++;
            myExcelData[0, columnIndex] = "HK";
            BoldCell(ColumnName(columnIndex) + StartRow, ColumnName(columnIndex) + (StartRow + 1), true);

            columnIndex++;
            myExcelData[0, columnIndex] = "TBM";
            BoldCell(ColumnName(columnIndex) + StartRow, ColumnName(columnIndex) + (StartRow + 1), true);

            for (int row = 0; row < myRowCnt; row++)// duyet tu dau den cuoi mang
            {
                HocSinhDiem hsdiem = (HocSinhDiem)list[row];
               
                    myExcelData[row + 2, 0] = hsdiem.Stt;
                    myExcelData[row + 2, 1] = hsdiem.MaHocSinh;
                    myExcelData[row + 2, 2] = hsdiem.HoTen;

                    myExcelData[row + 2, 3] = hsdiem.KQHT == null ? "" : hsdiem.KQHT.M1;
                    myExcelData[row + 2, 4] = hsdiem.KQHT == null ? "" : hsdiem.KQHT.M2;
                    myExcelData[row + 2, 5] = hsdiem.KQHT == null ? "" : hsdiem.KQHT.M3;
                    myExcelData[row + 2, 6] = hsdiem.KQHT == null ? "" : hsdiem.KQHT.M4;
                    myExcelData[row + 2, 7] = hsdiem.KQHT == null ? "" : hsdiem.KQHT.M5;

                    myExcelData[row + 2, 8] = hsdiem.KQHT == null ? "" : hsdiem.KQHT.V15P3;
                    myExcelData[row + 2, 9] = hsdiem.KQHT == null ? "" : hsdiem.KQHT.V15P4;
                    myExcelData[row + 2, 10] = hsdiem.KQHT == null ? "" : hsdiem.KQHT.V15P5;
                    myExcelData[row + 2, 11] = hsdiem.KQHT == null ? "" : hsdiem.KQHT.TH15P1;
                    myExcelData[row + 2, 12] = hsdiem.KQHT == null ? "" : hsdiem.KQHT.TH15P2;


                    myExcelData[row + 2, 13] = hsdiem.KQHT == null ? "" : hsdiem.KQHT.V45P1;
                    myExcelData[row + 2, 14] = hsdiem.KQHT == null ? "" : hsdiem.KQHT.V45P2;
                    myExcelData[row + 2, 15] = hsdiem.KQHT == null ? "" : hsdiem.KQHT.V45P3;
                    myExcelData[row + 2, 16] = hsdiem.KQHT == null ? "" : hsdiem.KQHT.V45P4;
                    myExcelData[row + 2, 17] = hsdiem.KQHT == null ? "" : hsdiem.KQHT.V45P5;
                    myExcelData[row + 2, 18] = hsdiem.KQHT == null ? "" : hsdiem.KQHT.TH45P1;
                    myExcelData[row + 2, 19] = hsdiem.KQHT == null ? "" : hsdiem.KQHT.TH45P2;
                    myExcelData[row + 2, 20] = hsdiem.KQHT == null ? "" : hsdiem.KQHT.HK;
                    myExcelData[row + 2, 21] = hsdiem.KQHT == null ? "" : hsdiem.KQHT.TBM;
                 
            }

            BorderRow("A" + StartRow, ColumnName(columnIndex) + (StartRow + 1 + myRowCnt));

        }

        /// <summary>
        /// dang day du
        /// </summary>
        /// <param name="list"></param>
        /// <param name="viewAllColumns"></param>
        public override void FillRowDataEntities(List<HocSinhDiem> list, bool viewAllColumns)
        {
            if (viewAllColumns == false)
            {
                myRowCnt = list.Count;
                myExcelData = new object[RowCount, ColumnCount];


                myExcelData[0, 0] = "STT";
                BoldCell("A" + StartRow, "A" + (StartRow + 1), true);

                myExcelData[0, 1] = "Mã HS";
                BoldCell("B" + StartRow, "B" + (StartRow + 1), true);

                myExcelData[0, 2] = "Họ và tên";
                BoldCell("C" + StartRow, "C" + (StartRow + 1), true);

                int columnIndex = 2;
                myExcelData[0, columnIndex + 1] = "Miệng";
                BoldCell("D" + StartRow, "H" + (StartRow), true);
                for (int i = 1; i <= 5; i++)
                {
                    columnIndex++;
                    myExcelData[1, columnIndex] = "M" + i.ToString();
                }

                myExcelData[0, columnIndex + 1] = "Viết 15'";
                BoldCell(ColumnName(columnIndex + 1) + StartRow, ColumnName(columnIndex + 5) + (StartRow), true);
                for (int i = 1; i <= 5; i++)
                {
                    columnIndex++;
                    myExcelData[1, columnIndex] = "V" + i.ToString();
                }

                myExcelData[0, columnIndex + 1] = "Viết 45'";
                BoldCell(ColumnName(columnIndex + 1) + StartRow, ColumnName(columnIndex + 5) + (StartRow), true);
                for (int i = 1; i <= 5; i++)
                {
                    columnIndex++;
                    myExcelData[1, columnIndex] = "V" + i.ToString();
                }


                myExcelData[0, columnIndex + 1] = "TH 45'";
                BoldCell(ColumnName(columnIndex + 1) + StartRow, ColumnName(columnIndex + 2) + (StartRow), true);
                for (int i = 1; i <= 2; i++)
                {
                    columnIndex++;
                    myExcelData[1, columnIndex] = "TH" + i.ToString();
                }

                columnIndex++;
                myExcelData[0, columnIndex] = "HK";
                BoldCell(ColumnName(columnIndex) + StartRow, ColumnName(columnIndex) + (StartRow + 1), true);

                columnIndex++;
                myExcelData[0, columnIndex] = "TBM";
                BoldCell(ColumnName(columnIndex) + StartRow, ColumnName(columnIndex) + (StartRow + 1), true);

                for (int row = 0; row < myRowCnt; row++)// duyet tu dau den cuoi mang
                {
                    HocSinhDiem hsdiem = (HocSinhDiem)list[row];

                    myExcelData[row + 2, 0] = hsdiem.Stt;
                    myExcelData[row + 2, 1] = hsdiem.MaHocSinh;
                    myExcelData[row + 2, 2] = hsdiem.HoTen;

                    myExcelData[row + 2, 3] = hsdiem.KQHT == null ? "" : hsdiem.KQHT.M1;
                    myExcelData[row + 2, 4] = hsdiem.KQHT == null ? "" : hsdiem.KQHT.M2;
                    myExcelData[row + 2, 5] = hsdiem.KQHT == null ? "" : hsdiem.KQHT.M3;
                    myExcelData[row + 2, 6] = hsdiem.KQHT == null ? "" : hsdiem.KQHT.M4;
                    myExcelData[row + 2, 7] = hsdiem.KQHT == null ? "" : hsdiem.KQHT.M5;

                    myExcelData[row + 2, 8] = hsdiem.KQHT == null ? "" : hsdiem.KQHT.V15P3;
                    myExcelData[row + 2, 9] = hsdiem.KQHT == null ? "" : hsdiem.KQHT.V15P4;
                    myExcelData[row + 2, 10] = hsdiem.KQHT == null ? "" : hsdiem.KQHT.V15P5;
                    myExcelData[row + 2, 11] = hsdiem.KQHT == null ? "" : hsdiem.KQHT.TH15P1;
                    myExcelData[row + 2, 12] = hsdiem.KQHT == null ? "" : hsdiem.KQHT.TH15P2;


                    myExcelData[row + 2, 13] = hsdiem.KQHT == null ? "" : hsdiem.KQHT.V45P1;
                    myExcelData[row + 2, 14] = hsdiem.KQHT == null ? "" : hsdiem.KQHT.V45P2;
                    myExcelData[row + 2, 15] = hsdiem.KQHT == null ? "" : hsdiem.KQHT.V45P3;
                    myExcelData[row + 2, 17] = hsdiem.KQHT == null ? "" : hsdiem.KQHT.V45P4;

                    myExcelData[row + 2, 18] = hsdiem.KQHT == null ? "" : hsdiem.KQHT.TH45P1;
                    myExcelData[row + 2, 19] = hsdiem.KQHT == null ? "" : hsdiem.KQHT.TH45P2;
                    myExcelData[row + 2, 20] = hsdiem.KQHT == null ? "" : hsdiem.KQHT.HK;
                    myExcelData[row + 2, 21] = hsdiem.KQHT == null ? "" : hsdiem.KQHT.TBM;

                }

                BorderRow("A" + StartRow, ColumnName(columnIndex) + (StartRow + 1 + myRowCnt));
            }
            else
            {
                myRowCnt = list.Count;
                myExcelData = new object[RowCount, ColumnCount];


                myExcelData[0, 0] = "STT";
                BoldCell("A" + StartRow, "A" + (StartRow + 1), true);

                myExcelData[0, 1] = "Mã HS";
                BoldCell("B" + StartRow, "B" + (StartRow + 1), true);

                myExcelData[0, 2] = "Họ và tên";
                BoldCell("C" + StartRow, "C" + (StartRow + 1), true);

                int columnIndex = 2;
                myExcelData[0, columnIndex + 1] = "Miệng";
                BoldCell("D" + StartRow, "M" + (StartRow), true);
                for (int i = 1; i <= 5; i++)
                {
                    columnIndex++;
                    myExcelData[1, columnIndex] = "M" + i.ToString();
                }

                for (int i = 1; i <= 2; i++)
                {
                    columnIndex++;
                    myExcelData[1, columnIndex] = "V" + i.ToString();
                }

                for (int i = 3; i <= 5; i++)
                {
                    columnIndex++;
                    myExcelData[1, columnIndex] = "TH" + i.ToString();
                }

                myExcelData[0, columnIndex + 1] = "Viết 15'";
                BoldCell(ColumnName(columnIndex + 1) + StartRow, ColumnName(columnIndex + 5) + (StartRow), true);

                for (int i = 3; i <= 5; i++)
                {
                    columnIndex++;
                    myExcelData[1, columnIndex] = "V" + i.ToString();
                }

                for (int i = 1; i <= 2; i++)
                {
                    columnIndex++;
                    myExcelData[1, columnIndex] = "TH" + i.ToString();
                }

                myExcelData[0, columnIndex + 1] = "Viết 45'";
                BoldCell(ColumnName(columnIndex + 1) + StartRow, ColumnName(columnIndex + 5) + (StartRow), true);


                for (int i = 1; i <= 5; i++)
                {
                    columnIndex++;
                    myExcelData[1, columnIndex] = "V" + i.ToString();
                }


                myExcelData[0, columnIndex + 1] = "TH 45'";
                BoldCell(ColumnName(columnIndex + 1) + StartRow, ColumnName(columnIndex + 5) + (StartRow), true);
                for (int i = 1; i <= 5; i++)
                {
                    columnIndex++;
                    myExcelData[1, columnIndex] = "TH" + i.ToString();
                }

                columnIndex++;
                myExcelData[0, columnIndex] = "HK";
                BoldCell(ColumnName(columnIndex) + StartRow, ColumnName(columnIndex) + (StartRow + 1), true);

                columnIndex++;
                myExcelData[0, columnIndex] = "TBM";
                BoldCell(ColumnName(columnIndex) + StartRow, ColumnName(columnIndex) + (StartRow + 1), true);

                for (int row = 0; row < myRowCnt; row++)// duyet tu dau den cuoi mang
                {
                    HocSinhDiem hsdiem = (HocSinhDiem)list[row];
                    myExcelData[row + 2, 0] = hsdiem.Stt;
                    myExcelData[row + 2, 1] = hsdiem.MaHocSinh;
                    myExcelData[row + 2, 2] = hsdiem.HoTen;

                    myExcelData[row + 2, 3] = hsdiem.KQHT == null ? "" : hsdiem.KQHT.M1;
                    myExcelData[row + 2, 4] = hsdiem.KQHT == null ? "" : hsdiem.KQHT.M2;
                    myExcelData[row + 2, 5] = hsdiem.KQHT == null ? "" : hsdiem.KQHT.M3;
                    myExcelData[row + 2, 6] = hsdiem.KQHT == null ? "" : hsdiem.KQHT.M4;
                    myExcelData[row + 2, 7] = hsdiem.KQHT == null ? "" : hsdiem.KQHT.M5;

                    myExcelData[row + 2, 8] = hsdiem.KQHT == null ? "" : hsdiem.KQHT.V15P1;
                    myExcelData[row + 2, 9] = hsdiem.KQHT == null ? "" : hsdiem.KQHT.V15P2;
                    myExcelData[row + 2, 10] = hsdiem.KQHT == null ? "" : hsdiem.KQHT.TH15P3;
                    myExcelData[row + 2, 11] = hsdiem.KQHT == null ? "" : hsdiem.KQHT.TH15P4;
                    myExcelData[row + 2, 12] = hsdiem.KQHT == null ? "" : hsdiem.KQHT.TH15P5;

                    myExcelData[row + 2, 13] = hsdiem.KQHT == null ? "" : hsdiem.KQHT.V15P3;
                    myExcelData[row + 2, 14] = hsdiem.KQHT == null ? "" : hsdiem.KQHT.V15P4;
                    myExcelData[row + 2, 15] = hsdiem.KQHT == null ? "" : hsdiem.KQHT.V15P5;
                    myExcelData[row + 2, 16] = hsdiem.KQHT == null ? "" : hsdiem.KQHT.TH15P1;
                    myExcelData[row + 2, 17] = hsdiem.KQHT == null ? "" : hsdiem.KQHT.TH15P2;


                    myExcelData[row + 2, 18] = hsdiem.KQHT == null ? "" : hsdiem.KQHT.V45P1;
                    myExcelData[row + 2, 19] = hsdiem.KQHT == null ? "" : hsdiem.KQHT.V45P2;
                    myExcelData[row + 2, 20] = hsdiem.KQHT == null ? "" : hsdiem.KQHT.V45P3;
                    myExcelData[row + 2, 21] = hsdiem.KQHT == null ? "" : hsdiem.KQHT.V45P4;
                    myExcelData[row + 2, 22] = hsdiem.KQHT == null ? "" : hsdiem.KQHT.V45P5;

                    myExcelData[row + 2, 23] = hsdiem.KQHT == null ? "" : hsdiem.KQHT.TH45P1;
                    myExcelData[row + 2, 24] = hsdiem.KQHT == null ? "" : hsdiem.KQHT.TH45P2;
                    myExcelData[row + 2, 25] = hsdiem.KQHT == null ? "" : hsdiem.KQHT.TH45P3;
                    myExcelData[row + 2, 26] = hsdiem.KQHT == null ? "" : hsdiem.KQHT.TH45P4;
                    myExcelData[row + 2, 27] = hsdiem.KQHT == null ? "" : hsdiem.KQHT.TH45P5;
                    myExcelData[row + 2, 28] = hsdiem.KQHT == null ? "" : hsdiem.KQHT.HK;
                    myExcelData[row + 2, 29] = hsdiem.KQHT == null ? "" : hsdiem.KQHT.TBM;

                }

                BorderRow("A" + StartRow, ColumnName(columnIndex) + (StartRow + 1 + myRowCnt));
            }
        }
         
        public override object[,] ExcelData
        {
            get
            {
                return myExcelData;
            }
        }

     

        public override int RowCount
        {
            get
            {
                return myRowCnt + 2;
            }
        }

        public override int StartRow
        {
            get
            {
                return startRow;
            }
        }
    }
}