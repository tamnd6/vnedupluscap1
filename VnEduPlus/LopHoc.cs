//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace VnEduPlus
{
    using System;
    using System.Collections.Generic;
    
    public partial class LopHoc
    {
        public int idTruong { get; set; }
        public string NamHoc { get; set; }
        public int STT { get; set; }
        public string MaLop { get; set; }
        public string TenLop { get; set; }
        public string MaKhoi { get; set; }
        public string MaGV { get; set; }
        public string MaBanNganh { get; set; }
        public string Ghichu { get; set; }
    }
}
