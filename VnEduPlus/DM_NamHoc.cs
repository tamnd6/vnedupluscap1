//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace VnEduPlus
{
    using System;
    using System.Collections.Generic;
    
    public partial class DM_NamHoc
    {
        public DM_NamHoc()
        {
            this.CongThucTinhDiemTBs = new HashSet<CongThucTinhDiemTB>();
            this.DM_CongThuc = new HashSet<DM_CongThuc>();
            this.DM_HocKy = new HashSet<DM_HocKy>();
            this.HocSinhs = new HashSet<HocSinh>();
            this.HocSinh_HanhKiem = new HashSet<HocSinh_HanhKiem>();
            this.TieuChuanXepLoais = new HashSet<TieuChuanXepLoai>();
        }
    
        public string NamHoc { get; set; }
        public Nullable<System.DateTime> NgayBatDau { get; set; }
        public Nullable<System.DateTime> NgayKetThuc { get; set; }
        public Nullable<bool> IsHienTai { get; set; }
    
        public virtual ICollection<CongThucTinhDiemTB> CongThucTinhDiemTBs { get; set; }
        public virtual ICollection<DM_CongThuc> DM_CongThuc { get; set; }
        public virtual ICollection<DM_HocKy> DM_HocKy { get; set; }
        public virtual ICollection<HocSinh> HocSinhs { get; set; }
        public virtual ICollection<HocSinh_HanhKiem> HocSinh_HanhKiem { get; set; }
        public virtual ICollection<TieuChuanXepLoai> TieuChuanXepLoais { get; set; }
    }
}
