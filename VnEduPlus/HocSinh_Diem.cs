//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace VnEduPlus
{
    using System;
    using System.Collections.Generic;
    
    public partial class HocSinh_Diem
    {
        public int idTruong { get; set; }
        public string NamHoc { get; set; }
        public string MaHocKy { get; set; }
        public string MaLop { get; set; }
        public string MaHocSinh { get; set; }
        public string MaHocSinhFriendly { get; set; }
        public string MaMonHoc { get; set; }
        public string HoTen { get; set; }
        public string M1 { get; set; }
        public string M2 { get; set; }
        public string M3 { get; set; }
        public string M4 { get; set; }
        public string M5 { get; set; }
        public string V15P1 { get; set; }
        public string V15P2 { get; set; }
        public string V15P3 { get; set; }
        public string V15P4 { get; set; }
        public string V15P5 { get; set; }
        public string TH15P1 { get; set; }
        public string TH15P2 { get; set; }
        public string TH15P3 { get; set; }
        public string TH15P4 { get; set; }
        public string TH15P5 { get; set; }
        public string V45P1 { get; set; }
        public string V45P2 { get; set; }
        public string V45P3 { get; set; }
        public string V45P4 { get; set; }
        public string V45P5 { get; set; }
        public string TH45P1 { get; set; }
        public string TH45P2 { get; set; }
        public string TH45P3 { get; set; }
        public string TH45P4 { get; set; }
        public string TH45P5 { get; set; }
        public string HK { get; set; }
        public string TBM { get; set; }
        public string UserCreated { get; set; }
        public System.DateTime DateCreated { get; set; }
        public string UserUpdated { get; set; }
        public Nullable<System.DateTime> DateUpdated { get; set; }
    }
}
