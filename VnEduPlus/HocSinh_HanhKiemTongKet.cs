//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace VnEduPlus
{
    using System;
    using System.Collections.Generic;
    
    public partial class HocSinh_HanhKiemTongKet
    {
        public int idTruong { get; set; }
        public string NamHoc { get; set; }
        public string MaLop { get; set; }
        public string MaHocSinh { get; set; }
        public string MaHocSinhFriendly { get; set; }
        public string HKHK1 { get; set; }
        public string HKHK2 { get; set; }
        public string HKCN { get; set; }
    }
}
