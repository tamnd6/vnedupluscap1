﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using VnEduPlus;
using VnEduPlus.Models;
using VnEduPlus.CmonFunction;
using System.Web.Mvc;
using VnEduPlus.Controllers;
 

namespace VnEduPlus.Repositories
{

    /// <summary>
    /// Cài đặt 2 hàm, trong inter face
    /// </summary>
    public interface DanhSachHocSinhChuyenCanRepository
    {
        int getHocSinhCount(string filterExpression);

        IQueryable<HocSinhChuyenCan> getAllHocSinh(string malop, int idtruong, string namhoc, string sortExpression, int pageIndex, int pageSize, int pagesCount);
    }

    public class MemoryHocsinhChuyenCanRespository : DanhSachHocSinhChuyenCanRepository
    {
        #region Fields

        EDUNP_CAP1Entities db = new EDUNP_CAP1Entities();
        #endregion

        #region IProductsRepository Members
        /// <summary>
        /// Ham nay lay tong so hoc sinh trong lisst
        /// </summary>
        /// <param name="filterExpression"></param>
        /// <returns></returns>
        public int getHocSinhCount(string filterExpression)
        {
            if (!String.IsNullOrWhiteSpace(filterExpression))
                return db.HocSinhs.Where(filterExpression).Count();
            else
                return db.HocSinhs.Count();
        }
         

        /// <summary>
        /// thuc hien lay ra danh sach hoc sinh thoa man dieu kien
        /// </summary>
        /// <param name="malop"></param>
        /// <param name="sortExpression"></param>
        /// <param name="sortDirection"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="pagesCount"></param>
        /// <returns></returns>
        public IQueryable<HocSinhChuyenCan> getAllHocSinh(string malop,int idtruong, string namhoc, string sortExpression, int pageIndex, int pageSize, int pagesCount)
        {
             

            var rows = (from hs in db.HocSinhs.Where(hs => hs.MaLop == malop && hs.idTruong == idtruong && hs.NamHoc == namhoc)
                          orderby hs.STT ascending
                          select new HocSinhChuyenCan
                          {
                              Stt=hs.STT,
                              HoTen = hs.HoTen,
                              NgaySinh=hs.NgaySinh,
                              GioiTinh=hs.GioiTinh,
                              Ngaycophep = (from loainghihoc in db.HocSinh_NghiHoc
                                                where loainghihoc.MaLop == malop && (loainghihoc.LoaiNghi == "P" || loainghihoc.LoaiNghi == "CP") && loainghihoc.MaHocSinh == hs.MaHocSinh
                                                select loainghihoc).Count(),
                              Ngaykhongphep = (from loainghihoc in db.HocSinh_NghiHoc
                                                     where loainghihoc.MaLop == malop && (loainghihoc.LoaiNghi == "KP" || loainghihoc.LoaiNghi == "K") && loainghihoc.MaHocSinh == hs.MaHocSinh
                                                     select loainghihoc).Count()

                          }
                        ).Skip(pageIndex * pageSize).Take(pageSize);

            return rows;

        
        }
       
 

    }
        #endregion

}