using System;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using VnEduPlus;

namespace VnEduPlus.Repositories
{ 


    public interface ITinhThanhRepository
    {
        int GetTinhThanhCount(string filterExpression);

        IQueryable<DM_TinhThanh> GetTinhThanhs(string filterExpression, string sortExpression, string sortDirection, int pageIndex, int pageSize, int pagesCount);

        DM_TinhThanh GetTinhThanh(string primaryKeyProperty);

        bool DeleteTinhThanh(string primaryKeyProperty);

        bool AddTinhThanh(DM_TinhThanh newTinhThanh);
        void SavingChanges();
    }

	public class MemoryTinhThanhRespository : ITinhThanhRepository
    {
        


        #region Fields

        EDUNP_CAP1Entities db = new EDUNP_CAP1Entities();
        #endregion


        #region IProductsRepository Members
        public int GetTinhThanhCount(string filterExpression)
        {
            if (!String.IsNullOrWhiteSpace(filterExpression))
                return db.DM_TinhThanh.Where(filterExpression).Count();
            else
                return db.DM_TinhThanh.Count();
        }

        public IQueryable<DM_TinhThanh> GetTinhThanhs(string filterExpression, string sortExpression, string sortDirection, int pageIndex, int pageSize, int pagesCount)
        {
            if (!String.IsNullOrWhiteSpace(filterExpression))
                return db.DM_TinhThanh.Where(filterExpression).OrderBy(sortExpression + " " + sortDirection).Skip(pageIndex * pageSize).Take(pageSize);
            else
                return db.DM_TinhThanh.OrderBy("it." + sortExpression + " " + sortDirection).Skip(pageIndex * pageSize).Take(pagesCount * pageSize);
        }

        public DM_TinhThanh GetTinhThanh(string maTinhThanh)
        {
            return db.DM_TinhThanh.First(p => p.MaTinh == maTinhThanh);
        }

        public bool DeleteTinhThanh(string maTinhThanh)
        {
            try
            {
                db.DM_TinhThanh.DeleteObject(db.DM_TinhThanh.First(p => p.MaTinh == maTinhThanh));
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
               // Debug.Write(ex.Message);
                return false;
            }
        }

        public bool AddTinhThanh(DM_TinhThanh newTinhThanh)
        {
            try
            {
                db.DM_TinhThanh.AddObject(newTinhThanh);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                //Debug.Write(ex.Message);
                return false;
            }
        }

        public void SavingChanges()
        {
            db.SaveChanges();
        }


    }
        #endregion

}