using System;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using VnEduPlus;

namespace VnEduPlus.Repositories
{ 


    public interface ILophocMonhocRepository
    {
        int GetLopmonhocCount(string filterExpression);

        IQueryable<Lop_MonHoc> GetLopmonhocs(string filterExpression, string sortExpression, string sortDirection, int pageIndex, int pageSize, int pagesCount);

        Lop_MonHoc GetLopMonhoc(int idtruong, string namhoc, string malop, string mamon);
         
        IQueryable<LopHoc> Getmalophoc();

        bool DeleteLopmonhoc(int idtruong, string namhoc, string malop, string mamon);

        bool AddLopmonhoc(Lop_MonHoc lopmonhoc);
        void SavingChanges();
    }

    public class MemoryLopMonhocRespository : ILophocMonhocRepository
    {
        


        #region Fields

        EDUNP_CAP1Entities db = new EDUNP_CAP1Entities();
        #endregion


        #region IProductsRepository Members
        public int GetLopmonhocCount(string filterExpression)
        {
            if (!String.IsNullOrWhiteSpace(filterExpression))
                return db.Lop_MonHoc.Where(filterExpression).Count();
            else
                return db.Lop_MonHoc.Count();
        }

        public IQueryable<Lop_MonHoc> GetLopmonhocs(string filterExpression, string sortExpression, string sortDirection, int pageIndex, int pageSize, int pagesCount)
        {
            if (!String.IsNullOrWhiteSpace(filterExpression))
                return db.Lop_MonHoc.Where(filterExpression).OrderBy(sortExpression + " " + sortDirection).Skip(pageIndex * pageSize).Take(pageSize);
            else
                return db.Lop_MonHoc.OrderBy("it." + sortExpression + " " + sortDirection).Skip(pageIndex * pageSize).Take(pagesCount * pageSize);
        }

        // tra ve gia tri la mot record 
        public Lop_MonHoc GetLopMonhoc(int idtruong,string namhoc,string malop,string mamon)
        {
            return db.Lop_MonHoc.First(p => p.idTruong == idtruong && p.NamHoc == namhoc && p.MaLop==malop && p.MaMonHoc==mamon);
        }

        public bool DeleteLopmonhoc(int idtruong, string namhoc, string malop, string mamon)
        {
            try
            {
                db.Lop_MonHoc.DeleteObject(db.Lop_MonHoc.First(p => p.idTruong == idtruong && p.NamHoc == namhoc && p.MaLop == malop && p.MaMonHoc == mamon));
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
               // Debug.Write(ex.Message);
                return false;
            }
        }

        public bool AddLopmonhoc(Lop_MonHoc lopmonhoc)
        {
            try
            {
                db.Lop_MonHoc.AddObject(lopmonhoc);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                //Debug.Write(ex.Message);
                return false;
            }
        }

        public void SavingChanges()
        {
            db.SaveChanges();
        }



        

        public IQueryable<LopHoc> Getmalophoc()
        {
            return db.LopHocs;

        }


    }
        #endregion

}