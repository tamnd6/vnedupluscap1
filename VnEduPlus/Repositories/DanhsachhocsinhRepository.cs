﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using VnEduPlus;
using VnEduPlus.Models;

namespace VnEduPlus.Repositories
{ 


    public interface HocsinhListRepository
    {
        int getHocSinhCount(string filterExpression);

        IQueryable<HocSinh> getHocSinhList(string filterExpression, string sortExpression, string sortDirection, int pageIndex, int pageSize, int pagesCount);

        HocSinh getHocSinh(string primaryKeyProperty);

        bool delHocSinh(string primaryKeyProperty);

        bool addHocSinh(HocSinh newHocsinh);
        void SavingChanges();
         // IQueryable<HocSinh> getAllHocSinh(string malop, string sortExpression);
        IQueryable<AllHocSinh> getAllHocSinh(string malop,int idtruong,string namhoc, string sortExpression, int pageIndex, int pageSize, int pagesCount);
    }

	public class MemoryHocsinhRespository :HocsinhListRepository
    {
        #region Fields

        EDUNP_CAP1Entities db = new EDUNP_CAP1Entities();
        #endregion

        #region IProductsRepository Members
        /// <summary>
        /// Ham nay lay tong so hoc sinh trong lisst
        /// </summary>
        /// <param name="filterExpression"></param>
        /// <returns></returns>
        public int getHocSinhCount(string filterExpression)
        {
            if (!String.IsNullOrWhiteSpace(filterExpression))
                return db.HocSinhs.Where(filterExpression).Count();
            else
                return db.HocSinhs.Count();
        }

         
    
        /// <summary>
        /// Ham nay se lay ve mot danh sach cac hoc sinh thoa man mot dieu kien nao do
        /// </summary>
        /// <param name="filterExpression"></param>
        /// <param name="sortExpression"></param>
        /// <param name="sortDirection"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="pagesCount"></param>
        /// <returns></returns>
        public IQueryable<HocSinh> getHocSinhList(string filterExpression, string sortExpression, string sortDirection, int pageIndex, int pageSize, int pagesCount)
        {
            if (!String.IsNullOrWhiteSpace(filterExpression))
                return db.HocSinhs.Where(filterExpression).OrderBy(sortExpression + " " + sortDirection).Skip(pageIndex * pageSize).Take(pageSize);
            else
                return db.HocSinhs.OrderBy("it." + sortExpression + " " + sortDirection).Skip(pageIndex * pageSize).Take(pagesCount * pageSize);
        }


        /// <summary>
        /// thuc hien lay ra danh sach hoc sinh thoa man dieu kien
        /// </summary>
        /// <param name="malop"></param>
        /// <param name="sortExpression"></param>
        /// <param name="sortDirection"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="pagesCount"></param>
        /// <returns></returns>
        public IQueryable<AllHocSinh> getAllHocSinh(string malop, int idtruong, string namhoc, string sortExpression, int pageIndex, int pageSize, int pagesCount)
        {

            
            var rows = (from hocsinh in db.HocSinhs                       
                        where hocsinh.MaLop == malop && hocsinh.idTruong==idtruong && hocsinh.NamHoc==namhoc
                        where !(from hsqt in db.HocSinh_QuaTrinh.Where(hsqt => hsqt.idTruong == idtruong && hsqt.NamHoc == namhoc && hsqt.MaLop == malop && hsqt.MaQuaTrinh == "BH")
                                select hsqt.MaHocSinh).Contains(hocsinh.MaHocSinh)
                        orderby hocsinh.STT, sortExpression
                        select new AllHocSinh
                        {
                            stt=hocsinh.STT,
                            MaHocSinh = hocsinh.MaHocSinh,
                            NgaySinh=(DateTime) hocsinh.NgaySinh,
                            HoTen = hocsinh.HoTen,
                            GioiTinh = hocsinh.GioiTinh,
                            Dantoc = (from dt in db.DM_DanToc
                                   where dt.MaDanToc == hocsinh.MaDanToc 
                                      select dt).FirstOrDefault().TenDanToc,
                          
                           diachihiennay=hocsinh.Diachilienlac,
                            hotenChavaNgheNghiep = hocsinh.HoTenCha + (hocsinh.NgheCha.Length>0 ? (": " + hocsinh.NgheCha) : ""),
                            hotenMevaNgheNghiep=hocsinh.HoTenMe + (hocsinh.NgheMe.Length>0?(": "+hocsinh.NgheMe):""),
                            conThuongBenhBinh = (hocsinh.ConLietSi == true ? "Con Liệt Sĩ," : " ") + (hocsinh.CoCongCM==true ?" Con cách mạng, ":"  ") +
                            (from cs in db.DM_ChinhSach
                             where cs.MaChinhSach == hocsinh.MaChinhSach
                             select cs).FirstOrDefault().TenChinhSach
                             
                        }).Skip(pageIndex * pageSize).Take(pageSize);

            return rows;
        
        }
        /// <summary>
        /// Lay ra mot thong tin cua mot hoc sinh
        /// </summary>
        /// <param name="mahocsinh"></param>
        /// <returns></returns>
        public HocSinh getHocSinh(string mahocsinh)
        {
            return db.HocSinhs.First(p => p.MaHocSinh == mahocsinh);
        }
        /// <summary>
        /// Xoa mot hoc sinh
        /// </summary>
        /// <param name="maTinhThanh"></param>
        /// <returns></returns>
        public bool delHocSinh(string mahocsinh)
        {
            try
            {
                db.HocSinhs.DeleteObject(db.HocSinhs.First(p => p.MaHocSinh == mahocsinh));
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
               // Debug.Write(ex.Message);
                return false;
            }
        }
        /// <summary>
        /// Them moi mot hoc sinh
        /// </summary>
        /// <param name="newHocsinh"></param>
        /// <returns></returns>
        public bool addHocSinh(HocSinh newHocsinh)
        {
            try
            {
                db.HocSinhs.AddObject(newHocsinh);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                //Debug.Write(ex.Message);
                return false;
            }
        }

        public void SavingChanges()
        {
            db.SaveChanges();
        }


    }
        #endregion

}