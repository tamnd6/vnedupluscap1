﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace VnEduPlus.Models
{
    public class MonHocDS
    {
        public int Stt { get; set; }
        public string Mamonhoc { get; set; }         
        public string Tenmonhoc { get; set; }
        public IEnumerable<user> tenGiaoVienLists { get; set; }
        public string tenGiaoVienGiangDay { get; set; }
    }
}