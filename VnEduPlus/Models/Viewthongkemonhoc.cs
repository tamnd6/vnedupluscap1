﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace VnEduPlus.Models
{
    public class Viewthongkemonhoc
    {

        public string mamon { get; set; }
        public string tenmonhoc{get;set;} 
    }

    public class Thongkemonhoc
    {


        public int TongHs { get; set; }
        public int TongHsNu { get; set; }
        public List<Thongkemonhoctinhdiem> Montinhdiem { get; set; }

        public List<Thongkemonhocdanhgia> Mondanhgia { get; set; }
    }

    public class Thongkemonhoctinhdiem
    {

        public string MaMonhoc { get; set; }
        public string TenMonhoc { get; set; }
        public int SLGioi { get; set; }
        public int SLKha { get; set; }
        public int SLTB { get; set; }
        public int SLYeu { get; set; }
        public int SLKem { get; set; }

        public int SLGioiNu { get; set; }
        public int SLKhaNu { get; set; }
        public int SLTBNu { get; set; }
        public int SLYeuNu { get; set; }
        public int SLKemNu { get; set; }
    }
    public class Thongkemonhocdanhgia
    {

        public string MaMonhoc { get; set; }
        public string TenMonhoc { get; set; }
        public int SLDat { get; set; }
        public int SLChuaDat { get; set; }
        public int SLDatNu { get; set; }
        public int SLChuaDatNu { get; set; }
    }
}