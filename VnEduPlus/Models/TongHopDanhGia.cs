﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VnEduPlus.Models
{
    public class TongHopDanhGia
    {
        public Khoi khoi { get; set; }
        public LopHocGiaoVien lophoc { get; set; }
        public HocKy hocky { get; set; }
        public HocSinhTongHop hocsinh { get; set; }
        public List<DanhMucMonHoc> dsMonHoc { get; set; }
    }

    public class HocSinhTongHop
    {
        public string MaHocSinh { get; set; }
        public string TenHocSinh { get; set; }
        public string MaLop { get; set; }
        public string TenLop { get; set; }

    }

    public class Khoi
    {
        public string MaKhoi { get; set; }
        public string TenKhoi { get; set; }
    }

    public class TapHocSinhDiem
    {
        public string HocSinh { get; set; }
        public string MaLop { get; set; }
        public string HocKy { get; set; }
    }

    public class ExcelBoMon
    {
        public string maLop { get; set; }
        public string thang { get; set; }
        public string maMonHoc { get; set; }
    }

    public class MonHocThi
    {
        public string maMon { get; set; }
        public bool thiHK { get; set; }
        public bool thiGHK { get; set; }
    }

    public class MonHocDanhGia
    {
        public string maMonHoc { get; set; }
        public string tenMonHoc { get; set; }
    }

    public class KetQuaMonDanhGia
    {
        public string maMonHoc { get; set; }
        public System.Data.DataTable dataTable { get; set; }
    }
}