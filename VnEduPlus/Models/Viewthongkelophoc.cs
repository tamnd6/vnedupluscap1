﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
 
namespace VnEduPlus.Models
{
    public class Viewthongkelophoc
    {

        public String Tengiaovien { get; set; }
        public string makhoi{get;set;}
        public string malop { get; set; }
        public string tenLop { get; set; }
        public string maky { get; set; }
        public int siso { get; set; }
        public int cophep { get; set; }
        public int khongphep { get; set; }
        public int idtruong { get; set; }
        public string namhoc { get; set; }
        public IEnumerable<user> tenGiaoVienLists { get; set; }
        public HttpPostedFileBase MyFile { get; set; }
        
    }

     public class TaiLieuMau
    {
        public String Tengiaovien { get; set; }
        public IEnumerable<user> tenGiaoVienLists { get; set; }
        public HttpPostedFileBase MyFile { get; set; }
        public string makhoi { get; set; }
        public string malop { get; set; }
        public string tenLop { get; set; }
        public int idtruong { get; set; }
        public string namhoc { get; set; }
        public bool quyenUploadFile { get; set; }
    }

    public class TaiLieuThamKhao
    {
        public string namTaiLieu { get; set; }
        public List<DM_NamHoc> namHoc { get; set; }
        public IEnumerable<TaiLieuMau> taiLieuMau { get; set; }

    }

    public class HieuTruongUploadFiles
    {
        public int idtruong { get; set; }
        public IEnumerable<UploadFile> fileuploads { get; set; }
        public HttpPostedFileBase MyFile { get; set; }
    }


    public class user
    {
        public string fullName { get; set; }

    }

    public class FilesForClasses
    {
        public string malop { get; set; } // Tương đương với thư mục chứa file sổ chủ nhiệm
        public string idTrương { get; set; } // Tương đương với thư muc xác định thư mục của trường hiện tại  
        public string fileName { get; set; } // Tên file để xác định xem file này có tồn tại hay không 
        public bool isExits { get; set; }
    }
}