﻿using System; 
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace VnEduPlus.Models
{
    

    public class HocSinhKhoaCotDiem
    {
         
        public bool M1 { get; set; }
        public bool M2 { get; set; }
        public bool M3 { get; set; }
        public bool M4 { get; set; }
        public bool M5 { get; set; }
        public bool M6 { get; set; }
        public bool M7 { get; set; }
        public bool V15P1 { get; set; }
        public bool V15P2 { get; set; }
        public bool V15P3 { get; set; }
        public bool V15P4 { get; set; }
        public bool V15P5 { get; set; }
        public bool V15P6 { get; set; }
        public bool V15P7 { get; set; }
        public bool V15P8 { get; set; }

        public bool V45P1 { get; set; }
        public bool V45P2 { get; set; }
        public bool V45P3 { get; set; }
        public bool V45P4 { get; set; }
        public bool V45P5 { get; set; }
        public bool V45P6 { get; set; }
        public bool V45P7 { get; set; }
        public bool V45P8 { get; set; }
        public bool V45P9 { get; set; }
        public bool V45P10 { get; set; }
        
        public bool HK { get; set; }
        
    }


    public class HocSinhDiem
    {
        public int Stt { get; set; }
        public string MaHocSinh { get; set; }
        public string HoTen { get; set; }
        public DateTime? NgaySinh { get; set; }
        public Diem KQHT { get; set; }
        public string DuocMien { get; set; }
        public HocSinhKhoaCotDiem hsKHoaCotDiem { get; set; }
        public Diem OldDiem { get; set; }
        public string bohoc { get; set; }
       // public List<Diem> ListHS { get; set; }

    }
    public class Diem
    {
        public string M1 { get; set; }
        public string M2 { get; set; }
        public string M3 { get; set; }
        public string M4 { get; set; }
        public string M5 { get; set; }

        public string V15P1 { get; set; }
        public string V15P2 { get; set; }
        public string TH15P3 { get; set; }
        public string TH15P4 { get; set; }
        public string TH15P5 { get; set; }

        public string V15P3 { get; set; }
        public string V15P4 { get; set; }
        public string V15P5 { get; set; }

        public string TH15P1 { get; set; }
        public string TH15P2 { get; set; }

        public string V45P1 { get; set; }
        public string V45P2 { get; set; }
        public string V45P3 { get; set; }
        public string V45P4 { get; set; }
        public string V45P5 { get; set; }


        public string TH45P1 { get; set; }
        public string TH45P2 { get; set; }
        public string TH45P3 { get; set; }
        public string TH45P4 { get; set; }
        public string TH45P5 { get; set; }
        public string HK { get; set; }
        public string TBM { get; set; }
        public string TBMCN { get; set; }
    }

    public class HocSinhDiemThongke
    {
        
        public string MaHocSinh { get; set; }
        public string HoTen { get; set; }
        public DateTime? NgaySinh { get; set; }

        public string M1 { get; set; }
        public string M2 { get; set; }
        public string M3 { get; set; }
       

        public string V15P1 { get; set; }
        public string V15P2 { get; set; }
        public string V15P3 { get; set; }
        
       

        public string V45P1 { get; set; }
        public string V45P2 { get; set; }
        public string V45P3 { get; set; }
        public string V45P4 { get; set; }
       
        public string HK { get; set; }
        public string TBM { get; set; }
    
    }

    public class SearchHocSinhTheoKhoangDiem
    {
        public int IdTruong { get; set; }
        public string NamHoc { get; set; }
        public string MaHocKy { get; set; }
        public string MaLop { get; set; }
        public string TenLop { get; set; }
        public string MaHocSinh { get; set; }
        public string HoTen { get; set; }
        public string MaMonHoc { get; set; }
        public string M1 { get; set; }
        public string M2 { get; set; }
        public string M3 { get; set; }
        public string M4 { get; set; }
        public string M5 { get; set; }
        public string V15P1 { get; set; }
        public string V15P2 { get; set; }
        public string V15P3 { get; set; }
        public string V15P4 { get; set; }
        public string V15P5 { get; set; }
        public string TH15P1 { get; set; }
        public string TH15P2 { get; set; }
        public string TH15P3 { get; set; }
        public string TH15P4 { get; set; }
        public string TH15P5 { get; set; }
        public string V45P1 { get; set; }
        public string V45P2 { get; set; }
        public string V45P3 { get; set; }
        public string V45P4 { get; set; }
        public string V45P5 { get; set; }
        public string TH45P1 { get; set; }
        public string TH45P2 { get; set; }
        public string TH45P3 { get; set; }
        public string TH45P4 { get; set; }
        public string TH45P5 { get; set; }
        public string HK { get; set; }
        public string TBM { get; set; }

    }

}