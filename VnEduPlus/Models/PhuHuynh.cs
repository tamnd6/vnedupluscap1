﻿using System;
namespace VnEduPlus.Models
{
    public class PhuHuynhPhanHoi
    {
        public int STT { get; set; }
        public string hoTenPhuHuynh { get; set; }
        public string hoTenHocSinh { get; set; }
        public DateTime? ngayTao { get; set; }
        public string maLop { get; set; }
        public string tenTruong { get; set; }
        public string noiDungPhanHoi { get; set; }
     } 
}