﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VnEduPlus.Models
{
    public class GiaoVien
    {
        public string username { get; set; }
        public string hoten { get; set; }
        public string chucvu { get; set; }
        public string truong { get; set; }
        public string sodienthoai { get; set; }
        public string email { get; set; }
        public string diachi { get; set; }

    }
}