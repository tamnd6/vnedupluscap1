﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VnEduPlus.Models
{
    public class DiemMonHoc
    {
        public string mamon { get; set; }
        public string tenmon { get; set; }

        public string M1 { get; set; }
        public string M2 { get; set; }
        public string M3 { get; set; }
        public string M4 { get; set; }
        public string M5 { get; set; }


        public string V15P1 { get; set; }
        public string V15P2 { get; set; }
        public string V15P3 { get; set; }
        public string V15P4 { get; set; }
        public string V15P5 { get; set; }

        public string V45P1 { get; set; }
        public string V45P2 { get; set; }
        public string V45P3 { get; set; }
        public string V45P4 { get; set; }
        public string V45P5 { get; set; }

        public string HK { get; set; }
        public string TBM { get; set; }


    }
    /// <summary>
    /// Chua thong tin ket qua hoc tap cua mot hoc sinh theo cac mon hoc
    /// </summary>
    public class ThongKeKetQuaHocTap
    {
        public string stt { get; set; }
        public string mahocsinh { get; set; }
        public string tenhocsinh { get; set; }
        public string malop { get; set; }
        public string tenlop { get; set; }
        public IQueryable<DiemMonHoc> KQHT { get; set; }
        public string TBCM { get; set; }
        public string xephang { get; set; }
        public int? vangkhongphep { get; set; }
        public int? vangcophep { get; set; }
        public string hocluc { get; set; }
        public string hanhkiem { get; set; }
        public string danhhieu { get; set; }
         
    }


    public class ThongKeKhoiHoc
    {
        public string makhoi { get; set; }
        public string tenkhoi { get; set; }
        public int? tongsolop { get; set; }
        public string malop { get; set; }
        public string tenlop { get; set; }
        public int? siso { get; set; }

        public int? soLuongHocLucGioi { get; set; }
        public float? tileLoaiGioi { get; set; }

        public int? soLuongHocLucKha { get; set; }
        public float? tiLeLoaiKha { get; set; }

        public int? soLuongHocLucTrungBinh { get; set; }
        public float? tiLeLoaiTB { get; set; }

        public int? soLuongHocLucYeu { get; set; }
        public float? tiLeLoaiYeu { get; set; }

        public int? soLuongHocLucKem { get; set; }
        public float? tiLeLoaiKem { get; set; }

        public int? soLuongHanhKiemGioi { get; set; }
        public float? tiLeHanhKiemGioi { get; set; }

        public int? soLuongHanhKiemKha { get; set; }
        public float? tiLeHanhKiemKha { get; set; }

        public int? soLuongHanhKiemTrungBinh { get; set; }
        public float? tiLeHanhKiemTrungBinh { get; set; }

        public int? soLuongHanhKiemYeu { get; set; }
        public float? tiLeHanhKiemYeu { get; set; }

        public int? soLuongHanhKiemKem { get; set; }
        public float? tiLeHanhKiemKem { get; set; }

        public int? soLuongHocSinhNu { get; set; }
        public float? tiLeHSNu { get; set; }
    }


    //---------------------------------Mau phieu 2 theo hoc ky--------------------------------

    public class mauPhieu2DiemMonHoc
    {
        public string mamon { get; set; }
        public string tenmon { get; set; }

        public string diemMieng { get; set; }
        public string diemKTHS1 { get; set; }
        public string diemKTHS2 { get; set; }

        public string HK { get; set; }
        public string TBM { get; set; }

    }


    /// <summary>
    /// Chua thong tin ket qua hoc tap cua mot hoc sinh theo cac mon hoc
    /// </summary>
    public class mauPhieu2KQHT
    {
        public string stt { get; set; }
        public string mahocsinh { get; set; }
        public string tenhocsinh { get; set; }
        public string malop { get; set; }
        public string tenlop { get; set; }
        public IQueryable<mauPhieu2DiemMonHoc> KQHT { get; set; }
        public string TBCM { get; set; }
        public string xephang { get; set; }
        public int? vangkhongphep { get; set; }
        public int? vangcophep { get; set; }
        public string hocluc { get; set; }
        public string hanhkiem { get; set; }
        public string danhhieu { get; set; }
    }


    //---------------------------------Mau phieu 2 theo ca nam hoc--------------------------------

    public class mauPhieu2DiemMonHocCaNam
    {
        public string mamon { get; set; }
        public string tenmon { get; set; }

        public string TBMK1 { get; set; }
        public string TBMk2 { get; set; }
        public string TBMTL { get; set; }

        public string TBMCN { get; set; }
        public string Ghichu { get; set; }

    }


    /// <summary>
    /// Chua thong tin ket qua hoc tap cua mot hoc sinh theo cac mon hoc
    /// </summary>
    public class mauPhieu2KQHTCaNam
    {
        public string stt { get; set; }
        public string mahocsinh { get; set; }
        public string tenhocsinh { get; set; }
        public string malop { get; set; }
        public string tenlop { get; set; }
        public IQueryable<mauPhieu2DiemMonHocCaNam> KQHT { get; set; }
        public string TBCM { get; set; }
        public string xephang { get; set; }
        public int? vangkhongphep { get; set; }
        public int? vangcophep { get; set; }
        public string hocluc { get; set; }
        public string hanhkiem { get; set; }
        public string danhhieu { get; set; }
    }

    /// <summary>
    /// chua so luong va ti le phan tram 
    /// trong do
    /// ti le phan tram duoc tinh la: soluong/ tong so hoc sinh lop
    /// </summary>

    /// <summary>
    /// Lop hoc
    /// </summary>
    public class lophoctk
    {
        public string malop { get; set; }
        public string tenlop { get; set; }
        public int? siso { get; set; }

        public int? soLuongHocLucGioi { get; set; }
        public float? tileLoaiGioi { get; set; }

        public int? soLuongHocLucKha { get; set; }
        public float? tiLeLoaiKha { get; set; }

        public int? soLuongHocLucTrungBinh { get; set; }
        public float? tiLeLoaiTB { get; set; }

        public int? soLuongHocLucYeu { get; set; }
        public float? tiLeLoaiYeu { get; set; }

        public int? soLuongHocLucKem { get; set; }
        public float? tiLeLoaiKem { get; set; }

        public int? soLuongHanhKiemGioi { get; set; }
        public float? tiLeHanhKiemGioi { get; set; }

        public int? soLuongHanhKiemKha { get; set; }
        public float? tiLeHanhKiemKha { get; set; }

        public int? soLuongHanhKiemTrungBinh { get; set; }
        public float? tiLeHanhKiemTrungBinh { get; set; }

        public int? soLuongHanhKiemYeu { get; set; }
        public float? tiLeHanhKiemYeu { get; set; }

        public int? soLuongHanhKiemKem { get; set; }
        public float? tiLeHanhKiemKem { get; set; }

        public int? soLuongHocSinhNu { get; set; }
        public float? tiLeHSNu { get; set; }

    }
    /// <summary>
    ///  Khoi hoc
    /// </summary>
    public class khoihoc
    {
        public string makhoi { get; set; }
        public string tenkhoi { get; set; }
        public int? tongsolop { get; set; }
        public IQueryable<lophoctk> LHOC { get; set; }
    }

    public class DiemTheoMonHoc
    {
        public string makhoi { get; set; }
        public string tenkhoi { get; set; }
        public string malop { get; set; }
        public string tenlop { get; set; }
        public int? siso { get; set; }
        public int? tongsolop { get; set; }
        // muc diem TBMHK=0

        public int? level1 { get; set; }

        // muc diem 0<TBMHK<2
        public int? level2 { get; set; }

        // muc diem 2<TBM<4
        public int? level3 { get; set; }


        // muc diem 4<TBM<5
        public int? level4 { get; set; }

        // muc diem 5<TBM<7
        public int? level5 { get; set; }

        // muc diem 7<TBM<8
        public int? level6 { get; set; }

        // muc diem 8<TBM<10
        public int? level7 { get; set; }

        // muc diem TBM=10
        public int? level8 { get; set; }

        // muc diem Tren TB
        public int? level9 { get; set; }

    }

    public class DiemMonDanhGia
    {
        public string makhoi { get; set; }
        public string tenkhoi { get; set; }

        public string malop { get; set; }
        public string tenlop { get; set; }
        public int? siso { get; set; }
        public int? tongsolop { get; set; }

        public int? Dat { get; set; }

        public int? ChuaDat { get; set; }

        public int? ChuaTK { get; set; }


    }

    public class LopThongTu58
    {
        public List<MonHocDS> DsMon { get; set; } 
        public MonHocDS MonChuyen { get; set; }
        public List<HocSinhThongTu58> KQHT { get; set; }

    }

    public class LopDanhHieu
    {
        public List<MonHocDS> DsMon { get; set; }
        public MonHocDS MonChuyen { get; set; }
        public List<HocSinhDanhHieu> KQHT { get; set; }
        public List<DanhHieu> DanhHieu {get;set;}
        public List<DtbMon> KetQuaMonHoc { get; set; }

    }

    public class DanhHieu
    {
        public string MaHocSinh { get; set; }
        public string danhHieu { get; set; }

    }

    
    public class HocSinhDanhHieu
    {
        public int? stt { get; set; }
        public string Hoten { get; set; }
        public DateTime? Ngaysinh { get; set; }
        public string XepLoai { get; set; }
        public string XepLoaiHLK1 { get; set; }
        public string XepLoaiHLK2 { get; set; }
        public string XepLoaiHK { get; set; }
        public string XepLoaiDH { get; set; }
        public string MaHocSinh { get; set; }
        public string TBHK { get; set; }
        public string TBHKTL { get; set; }
        public string XepLoaiHKTL { get; set; }
        public string LenLop { get; set; }
        public string KQTL { get; set; }

        public IQueryable<DtbMon> DtbMons { get; set; }

    }
    public class MonThiLai
    {
        public string maMon { get; set; }
        public string tenMon { get; set; }
        public string DTL { get; set; }
    }
    public class HocSinhThongTu58
    {
        public int? stt { get; set; }
        public string Hoten { get; set; }
        public DateTime? Ngaysinh { get; set; }
        public string XepLoai { get; set; }
        public string MaHocSinh { get; set; }
        public string TBHK { get; set; }
        public string maHanhKiem { get; set; }
        public string HocLuc { get; set; }
        public string danhHieu { get; set; }
        public int soNgayKP { get; set; }
        public int soNgayCP { get; set; }
        public string lenLop { get; set; }
        public IQueryable<DtbMon> DtbMons { get; set; }
        public string diemKiemTraLai { get; set; }         
        public string XLHLLai { get; set; }
        public string XLHKlai { get; set; }
        public string xeplaiOlaiLenLop { get; set; }
        public IQueryable<MonThiLai> listMonThiLai { get; set; }
        public string CheckXepLoai { get; set; }
        
    }


    public class LopThongkeCN
    {
        public List<MonHocDS> DsMon { get; set; }
        public MonHocDS MonChuyen { get; set; }
        public List<HocSinhThongkeCN> KQHT { get; set; }

    }

    public class HocSinhThongkeCN
    {
        public int? stt { get; set; }
        public string Hoten { get; set; }
        public DateTime? Ngaysinh { get; set; }
        public string MaHocSinh { get; set; }
        public string TBHK1 { get; set; }
        public string XLHLK1 { get; set; }
        public string DanhHieuK1 { get; set; }

        public string TBHK2 { get; set; }
        public string XLHLHK2 { get; set; }
        public string DanhHieuK2 { get; set; }

        public string TBCN { get; set; }
        public string XLHLCN { get; set; }
        public string KQ { get; set; }

        public string DanhHieuCN { get; set; }
        public string TBCNTL { get; set; }
        public string XLHLCNTL { get; set; }
        public string KQTL { get; set; }
        public string HKHK1 { get; set; }
        public string HKHK2 { get; set; }
        public string HKCN { get; set; }

        public int CP { get; set; }
        public int KP { get; set; }

        public IQueryable<DtbMonCN> DtbMons { get; set; }

    }
    public class DtbMonCN
    {

        public string MaMon { get; set; }

        public string DtbHK1 { get; set; }
        public string DtbHK2 { get; set; }
        public string DtbCN { get; set; }

    }
    public class DtbMon
    {

        public string MaMon { get; set; }

        public string Dtb { get; set; }
        public bool isDuocMien { get; set; }
    }

}