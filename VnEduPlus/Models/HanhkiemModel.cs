﻿using System;
namespace VnEduPlus.Models
{
    public class HanhkiemModel
    {
        public int STT { get; set; }
        public string MaHocSinh { get; set; }
        public string HoTen { get; set; }
        public DateTime? NgaySinh { get; set; }
        public string HocLuc { get; set; }
        public string MaHanhKiem { get; set; }
        public string GhiChu { get; set; }
        public string nxGHK { get; set; }
        public string nxCHK { get; set; }
        public string XLHLK1 { get; set; }
        public string XLHLK2 { get; set; }
    }


    public class DiemThiLai
    {
        public int STT { get; set; }
        public string MaHocSinh { get; set; }
        public string HoTen { get; set; }
        public DateTime? NgaySinh { get; set; }
        public string HocLuc { get; set; }
        public string Diem { get; set; }
        public string DiemTBCN { get; set; }
        
    }

}