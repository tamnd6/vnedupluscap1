﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
 
namespace VnEduPlus.Models
{
    public class AllHocSinh
    {
         

        public int idTruong { get; set; }
        public string NamHoc { get; set; }
        public string MaLop { get; set; }
        public int stt { get; set; }
        public string MaHocSinh { get; set; }
        public string HoTen { get; set; }
        public string NoiSinh { get; set; }
        public string NoiO_MaTinh { get; set; }
        public string NoiO_MaHuyen { get; set; }
        public string NoiO_MaXa { get; set; }
        public string NoiO_MaKP { get; set; }
        public string Dantoc { get; set; }
        public string MaTonGiao { get; set; }
        public DateTime? NgaySinh { get; set; }
        public bool GioiTinh { get; set; }
        public string diachihiennay { get; set; }
        public string conThuongBenhBinh { get; set; }// ghi thong tin la con thuong binh hay liet si 
        public string hotenChavaNgheNghiep { get; set; }// ho ten cha và nghề nghiệp của cha
        public string hotenMevaNgheNghiep { get; set; }// Họ tên mẹ và nghề nghiệp của mẹ

        public string hoTenCha { get; set; }
        public string ngheNghiepCha { get; set; }
        public string hoTenMe { get; set; }
        public string ngheNghiepMe { get; set; }
        public string chinhSach { get; set; }
        public bool conLietSi { get; set; }
        public bool conCM { get; set; }

            
    }

    public class QTHOCTAP
    {
        public int IDQuaTrinh { get; set; }
        public int idTruong { get; set; }
        public string NamHoc { get; set; }
        public string MaKhoi { get; set; }
        public string MaLop { get; set; }
        public string MaHocSinh { get; set; }
        public string MaQuaTrinh { get; set; }
        public string Noidung { get; set; }
        public DateTime Thoigiansukien { get; set; }
        public DateTime DateCreated { get; set; }
        public string UserName { get; set; }

        public string tenuser { get; set; }

    }

    public class GIAOVIENCHUNHIEMNHANXETHOCSINH
    {
       
        public BoMonNhanXet thang8 { get; set; }
        public BoMonNhanXet thang9 { get; set; }
        public BoMonNhanXet thang10 { get; set; }
        public BoMonNhanXet thang11 { get; set; }
        public BoMonNhanXet thang12 { get; set; }
        public BoMonNhanXet thang1 { get; set; }
        public BoMonNhanXet thang2 { get; set; }
        public BoMonNhanXet thang3 { get; set; }
        public BoMonNhanXet thang4 { get; set; }
        public BoMonNhanXet thang5 { get; set; }         
    }

    public class BoMonNhanXet
    {
        public string kienThuc { get; set; }
        public string nangLuc { get; set; }
        public string phamChat { get; set; }
    }


    public class VIEWALSTUDENTINFOMATION
    {
        public List<QTHOCTAP> list_QTHT { get; set; }
        public GIAOVIENCHUNHIEMNHANXETHOCSINH giaoVienChuNhiemNhanXetHocSinh { get; set; }
        public List<HocSinh_KhenThuongKyLuat> khenthuongs { get; set; }
        public HocSinh hocSinh { get; set; }
        public HocSinh_Details hocSinhDetail { get; set; }
        public string makhuyettat { get; set; }
        public bool coHoSoKhuyetTat { get; set; }
        public HttpPostedFileBase MyFile { get; set; }

    }

    public class HocSinhKhenThuongKyLuat
    {
        public string MaHocSinh { get; set; }
        public string MaLop { get; set; }
        public string NamHoc { get; set; }
        public string maHK { get; set; }
        public int idTruong { get; set; }
        public DateTime Ngay { get; set; }
        public string NoiDung { get; set; }
        public bool KhenThuong { get; set; } 
        public bool KhenThuongCapTruong { get; set; }  
    }


    public class HocSinhNghiHoc
    {
        public int stt { get; set; }
        public string maHocSinh { get; set; }
        public string hoTen { get; set; }
        public DateTime ?ngaysinh{get;set;}
        public string malop { get; set; }
        public string CP { get; set; }
        public int tongCP { get; set; }
        public string KP { get; set; }
        public int tongKP { get; set; }
        public string ghichu { get; set; }
         
    }

    public class ThuVienMau
    {
        public string maKhoi { get; set; }
        public string thangnx { get; set; }
        public string loainx { get; set; }
        public string[] danhsachmau { get; set; }
    }
}