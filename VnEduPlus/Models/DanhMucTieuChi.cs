﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VnEduPlus.Models
{
    public class DanhMucTieuChi
    {
        public int id { get; set; }
        public string name { get; set; }

    }
    public class Month
    {
        public int id { get; set; }
        public string name { get; set; }
    }

    public class FormNhanXet
    {
        public DanhMucTieuChi danhMucTieuChi { get; set; }
        public Month thang { get; set; }
        public MonHoc monHoc { get; set; }
        public NangLuc nangLuc { get; set; }
        public PhamChat phamChat { get; set; }
        public HocKy hocKy { get; set; }
        public LopHoc lopHoc { get; set; }
             
    }

    public class LopHocGiaoVien
    {
        public int IdTruong { get; set; }
        public string NamHoc { get; set; }
        public string MaLop { get; set; }
        public string TenLop { get; set; }
    }

    public class LopHocForUser
    {
        public int IdTruong { get; set; }
        public string NamHoc { get; set; }
        public string MaLop { get; set; }
        public string userName { get; set; }
    }

    public class MauNhanXet
    {
        public string manhanxet { get; set; }
        public string tennhanxet { get; set; }
    }

    public class HocSinhNhanXetThang
    {
        public int IdTruong { get; set; }
        public int STT { get; set; }
        public string NamHoc { get; set; }
        public string MaLop { get; set; }
        public string MaHocSinh { get; set; }
        public DateTime? ngaySinh { get; set; }
        public string MaMonHoc { get; set; }
        public string HoTen { get; set; }
        public string NXKT { get; set; }
        public string NXPC { get; set; }
        public string NXNL { get; set; }
        public string UserCreated { get; set; }
        public string UserUpdated { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
    }

    public class soBoMon
    {
        public IList<HocSinhNhanXetThang> hocSinhNhanXetThang { get; set;}
        public string malop { get; set; }
        public string thang { get; set; }
        public string monhoc { get; set; }
    }


    public class GiaoVienBoMonNhanXetThang
    {
        public IList<HocSinhNhanXetThang> hocSinhNhanXetThang { get; set; }
        public string giaoVienBoMon { get; set; }
    }


    public class FormKienThuc
    {
        public DanhMucTieuChi danhMucTieuChi { get; set; }

        public DanhMucMonHoc danhMucMonHoc { get; set; }
        public Month thang { get; set; }

        public LopHocGiaoVien lophoc { get; set; }
        public HocKy hocky { get; set; }

        public MauNhanXet MauKienThuc { get; set; }
        public MauNhanXet MauPhamChat { get; set; }
        public MauNhanXet MauNangLuc { get; set; }

    }

    public class DanhMucMonHoc
    {
        public string MaMon { get; set; }
        public string TenMon { get; set; }
        public int TinhDiem { get; set; }
    }

    public class MonHoc
    {
        public string maMonHoc { get; set; }
        public string tenMonHoc { get; set; }
        public string diemMonHoc { get; set; }
        public bool isNhanXet { get; set; }  

    }

   

    public class NangLuc
    {
        public string maNangLuc { get; set; }
        public string tenNangLuc { get; set; }
    }

    public class PhamChat
    {
        public string maPhamChat { get; set; }
        public string tenPhamChat { get; set; }
    }

    public class HocKy
    {
        public string maHocKy { get; set; }
        public string tenHocKy { get; set; }
    }

    public class HocSinhNhanXetNhom {
        public int STT { get; set; }
        public string maHocSinh { get; set; }
        public DateTime? ngaySinh { get; set; }
        public string tenHocSinh { get; set; }
        public string nhanXetKienThuc { get; set; }
        public string nhanXetPhamChat { get; set; }
        public string nhanXetNangLuc { get; set; }
    
    }

    public class DmTieuChi
    {
        public string maNhanXet { get; set; }
        public string tenNhanXet { get; set; }
    
    }

    public class MauCauNhanXet
    {
        public IList<HocSinhNhanXetNhom> hocSinhNhanXetNhom { get; set; }
        public IList<DmTieuChi> DMKienThuc { get; set; }
        public IList<DmTieuChi> DMNangLuc { get; set; }
        public IList<DmTieuChi> DMPhamChat { get; set; }
        public string malop { get; set; }
        public string thang { get; set; }
    }

    public class TapNhanXet
    {
        public string maHS { get; set; }
        public string hoTen { get; set; }
        public string kienThuc { get; set; }
        public string nangLuc { get; set; }
        public string phamChat { get; set; }
    }

    public class NoiDungNhanXetThangs
    {
        
        public int idTruong { get; set; }
        public string maLop { get; set; }
        public string maHs { get; set; }
        public List<noiDungNhanXet> noiDungNhanXets { get; set; }
        
    }

    public class noiDungNhanXet
    {
        public string id { get; set; }
        public string value { get; set; }

    }



    public class NhanXetMonHoc
    {
        public string maMon { get; set; }
        public string noiDungNhanXet { get; set; }
    }

    public class HocSinhDiemMonHoc
    {
        public string maHocSinh { get; set; }
        public string maMonHoc { get; set; }
        public string diemMonHoc { get; set; }
    }

    public class NhapDiemHocSinh
    {
        public int STT { get; set; }
        public string maHocSinh { get; set; }
        public string tenHocSinh { get; set; }
        public IList<MonHoc> monHocs { get; set; }
        public IList<DiemHS> diemHS { get; set; }
        public string AN { get; set; }
      
        public HocKy hocKy { get; set; }
        public LopHoc lopHoc { get; set; }
    }

    public class DiemHS
    {
        public string maMon { get; set; }
        public string NhanXet { get; set; }
        public decimal? Diem { get; set; }
    }

    public class ChuongTrinh
    {
        public short tuan { get; set; }
        public string noidung { get; set; }
    }

    public class ChuongTrinhKhungLop
    {
        public IEnumerable<ChuongTrinh> khungCT { get; set; }
        public string maMon { get; set; }
    }

    public class ThongTinHocSinh
    {
        public string maHS { get; set; }
        public string hoTenHS { get; set; }
        public int STT { get; set; }
    }

    public class DiemHocSinh
    {
        public string maMon { get; set; }
        public string maHs { get; set; }
        public string tenMon { get; set; }
        public string nhanxet { get; set; }
        public decimal? DiemDKy { get; set; }
        public string DiemTX { get; set; }
    }

    public class NhanXetChung
    {
        public string NXNangLuc1 { get; set; }
        public string NXNangLuc2 { get; set; }
        public string NXNangLuc3 { get; set; }
        public string NXPhamChat1 { get; set; }
        public string NXPhamChat2 { get; set; }
        public string NXPhamChat3 { get; set; }
        public string NXPhamChat4 { get; set; }
        public IEnumerable<HocSinh_KhenThuongKyLuat> KhenThuong { get; set; }
        public string HoanThanhCT { get; set; }
        public int? DuocLenLop { get; set; }

    }

    

    public class NhanXetHocSinh
    {
        public IEnumerable<DiemHocSinh> DanhSachDiemMonHoc { get; set; }
        public NhanXetChung nhanXetChung { get; set; }
        public int phanQuyen { get; set; }
        public string maHS { get; set; }
         
    }

    public class MonThiHocKy
    {
        public string maMon { get; set; }
        public string maHK { get; set; }    
        public bool thiHK { get; set; }
        public bool thiGHK { get; set; }
    }


}