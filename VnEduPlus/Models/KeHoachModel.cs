﻿using System;
using System.Collections.Generic;
namespace VnEduPlus.Models
{
    public class KeHoachGVCN
    {
        public LopHoc lopHoc { get; set; }
        public string maLop { get; set; }
        public string tenLop { get; set; }
        public string noiDung { get; set; }
    }

    public class ThongKeDauNam
    {
        public int tongSoHocSinh { get; set; }
        public int tongSoHocSinhNu { get; set; }
        public int tongSoDoiVien { get; set; }
        public int tongSoHocSinhConLietSi { get; set; }
        public int tongSoHocSinhConThuongBinh { get; set; }
        public int tongSoHocSinhConMoCoi { get; set; }
        public int tongSoKhuyetTatHoaNhap { get; set; }
        public int tongSoLenLopThang { get; set; }
        public int tongSoThiLaiDuocLenLop { get; set; }
        public int luuBan { get; set; }
        public int moiVao { get; set; }
    }

    public class BanDaiDien
    {
        public LopHoc lopHoc { get; set; }
        public string maHocSinh { get; set; }
        public List<DSHoiPhuHuynh> danhsachBDD { get; set; }         
    }

    public class DanhSachHoiPhuHuynh
    {
        public int idTruong { get; set; }
        public string namHoc { get; set; }
        public string maLop { get; set; }
        public string maHocSinh { get; set; }
        public string tenHocSinh { get; set; }
        public string hoTenPH { get; set; }
        public string chucDanh { get; set; }
        public string diachi { get; set; }
        public string donviCongTac { get; set; }
        public string dienThoai { get; set; }
        public string email { get; set; }
    }

    public class DanhSachBanDaiDien
    {
        public List<DanhSachHoiPhuHuynh> danhSachHoiPhuHuynh { get; set; }
        public string MaHocSinh { get; set; }
    }
}