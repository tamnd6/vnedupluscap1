﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
 
 
namespace VnEduPlus.Models
{
    public class ViewSoLienLac
    {
        public int stt { get; set; }
        public string maHS { get; set; }
        public string tenHS { get; set; }
        
        public string DTBHK1 { get; set; }// Diem trung binh, neu la hoc ky thi la diem trung binh hoc ky, neu la ca nam thi la diem tb ca nam hoc
        public string danhHieuHK1 { get; set; }
        public string xepHangHK1 { get; set; }// xep hang 
        public int vangCoPhepHK1 { get; set; }// tong so ngay vang co phep 
        public int vangKhongPhepHK1 { get; set; }// tong so ngay vang khong phep 
        public string hocLucHK1 { get; set; } // xep loai hoc luc
        public string hanhKiemHK1 { get; set; }// xep loai hanh kiem

        public string DTBHK2 { get; set; }// Diem trung binh, neu la hoc ky thi la diem trung binh hoc ky, neu la ca nam thi la diem tb ca nam hoc
        public string danhHieuHK2 { get; set; }
        public string xepHangHK2 { get; set; }// xep hang 
        public int vangCoPhepHK2 { get; set; }// tong so ngay vang co phep 
        public int vangKhongPhepHK2 { get; set; }// tong so ngay vang khong phep 
        public string hocLucHK2 { get; set; } // xep loai hoc luc
        public string hanhKiemHK2 { get; set; }// xep loai hanh kiem
  
        public string DTBCN { get; set; }// Diem trung binh, neu la hoc ky thi la diem trung binh hoc ky, neu la ca nam thi la diem tb ca nam hoc
        public string danhHieuCN { get; set; }
        public string xepHangCN { get; set; }// xep hang 
        public int vangCoPhepCN { get; set; }// tong so ngay vang co phep 
        public int vangKhongPhepCN { get; set; }// tong so ngay vang khong phep 
        public string hocLucCN { get; set; } // xep loai hoc luc
        public string hanhKiemCN { get; set; }// xep loai hanh kiem
  
        public DateTime? ngaysinh { get; set; }
        public string thoiDiem { get; set; } 
        public string tenGiaoVien { get; set; }
        public IList<string> listMonHocName { get; set; }
        public IQueryable<ViewDiemMonHoc> KQTD { get; set; }        
        public string tenTruong { get; set; }
        public string tenLop { get; set; }
        public int siSo { get; set; }
       
        public string nhanxetGVCN { get; set; }  


    }
    /// <summary>
    /// View mon tinh diem nhung không phai la mon tu chon
    /// </summary>
    public class ViewDiemMonHoc
    {
        public string maMonTinhDiem { get; set; }
        public string tenMonHoc { get; set; }
        public string M1 { get; set; }
        public string M2 { get; set; }
        public string M3 { get; set; }
        public string M4 { get; set; }
        public string M5 { get; set; }


        public string V15P1 { get; set; }
        public string V15P2 { get; set; }
        public string V15P3 { get; set; }
        public string V15P4 { get; set; }
        public string V15P5 { get; set; }

        public string TH15P1 { get; set; }
        public string TH15P2 { get; set; }
        public string TH15P3 { get; set; }
        public string TH15P4 { get; set; }
        public string TH15P5 { get; set; }

        public string V45P1 { get; set; }
        public string V45P2 { get; set; }
        public string V45P3 { get; set; }
        public string V45P4 { get; set; }
        public string V45P5 { get; set; }

        public string TH45P1 { get; set; }
        public string TH45P2 { get; set; }
        public string TH45P3 { get; set; }
        public string TH45P4 { get; set; }
        public string TH45P5 { get; set; }

        public string HK { get; set; }
        public string TBM { get; set; }
    }
    

    public class ViewHocSinhSoLienLac
    {
        public string donvichuquan { get; set; }
        public string tentruong { get; set; }
        public int stt { get; set; }
        public string namhoc { get; set; }
        public string tenHS { get; set; }// ho ten hoc sinh 
        public string lop { get; set; }
        public DateTime? ngaysinh { get; set; }
        public string noisinh { get; set; }
        public string hotencha { get; set; }
        public string nghenghiepcha { get; set; }
        public string nghenghiepme { get; set; }
        public string hotenme { get; set; }
        public string diachilienlac { get; set; }        
        public string giaovienphutrachlop { get; set; }
        public string diachiGV { get; set; }
        public string dienthoai { get; set; }

    }

    public class ViewHocSinhSoLienLacCuoiNam
    {
        public string tentruong { get; set; }
        public int stt { get; set; }
        public string namhoc { get; set; }
        public string tenHS { get; set; }// ho ten hoc sinh 
        public string lop { get; set; }
        public DateTime? ngaysinh { get; set; }
        public string noisinh { get; set; }
        public string danhhieu { get; set; }
        public string nhanxetcuagv { get; set; }
        public string monthilai { get; set; }
        public string ketqualenlop { get; set; }
    
    }

    public class ViewHocSinh
    {
        public int Stt { get; set; }
        public string MaHocSinh { get; set; }
        public string HoTen { get; set; }
        public int idTruong { get; set; }
        public string MaLop { get; set; }
        public string NamHoc { get; set; }
        public string GioiTinh { get; set; }
        public DateTime? NgaySinh { get; set; }
        public string DiaChi { get; set; }
    }

    public class HocSinhSoLienLac
    {
        public string MaLop { get; set; }
        public string idtruong { get; set; }
        public string NamHoc { get; set; }
        public List<ViewHocSinh> lstHocSinh { get; set; }
    }


    public class SoLienLac
    {
        public int stt { get; set; }
        public string phongGiaoDuc { get; set; }
        public string maHS { get; set; }
        public string hoTenHS { get; set; }
        public string hoTenCha { get; set; }
        public string ngheNghiepCha { get; set; }
        public string hoTenMe { get; set; }
        public string ngheNghiepMe { get; set; }
        public string diaChi { get; set; }
        public string dienThoai { get; set; }
        public string giaoVien { get; set; }
        public decimal? Toan { get; set; }
        public string KQMonHocToan { get; set; }

        public decimal? TV { get; set; }
        public string KQMonHocTV { get; set; }

        public decimal? TNXH { get; set; }
        public string KQMonHocTNXH { get; set; }

        public decimal? KH { get; set; }
        public string KQMonHocKH { get; set; }

        public decimal? LS { get; set; }
        public string KQMonHocLS { get; set; }

        public decimal? DD { get; set; }
        public string KQMonHocDD { get; set; }

        public decimal? TC { get; set; }
        public string KQMonHocTC { get; set; }

        public decimal? KT { get; set; }
        public string KQMonHocKT { get; set; }

        public decimal? MT { get; set; }
        public string KQMonHocMT { get; set; }

        public decimal? AN { get; set; }
        public string KQMonHocAN { get; set; }

        public decimal? TD { get; set; }
        public string KQMonHocTD { get; set; }

        public decimal? NN { get; set; }
        public string KQMonHocNN { get; set; }

        public decimal? TinHoc { get; set; }
        public string KQMonHocTinHoc { get; set; }

        public decimal? TiengDanToc { get; set; }
        public string KQMonHocTiengDanToc { get; set; }

        public decimal? LichSu { get; set; }

        public decimal? KhoaHoc { get; set; }

        public IEnumerable<HocSinh_KhenThuongKyLuat> KhenThuong { get; set; }
        public string hoanThanhCT { get; set; }
        public string NXNangLucTX1 { get; set; }
        public string NXNangLucTX2 { get; set; }
        public string NXNangLucTX3 { get; set; }
        public string NXPhamChatTX1 { get; set; }
        public string NXPhamChatTX2 { get; set; }
        public string NXPhamChatTX3 { get; set; }
        public string NXPhamChatTX4 { get; set; }
        public short? DuocLenLop { get; set; }

        public NhanXetGVCNMonHoc nhanXetGVCN { get; set; }

    }

    public class DiemKiemTraMonHoc
    {
        public string maMon { get; set; }        
        public decimal diemKiemTra { get; set; }
    }

    public class NhanXetGVCNMonHoc
    {
        public string NXKT { get; set; }
        public string NXNL { get; set; }
        public string NXPC { get; set; }
    }
}