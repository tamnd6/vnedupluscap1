﻿using System;
namespace VnEduPlus.Models
{
    public class HocSinhChuyenCan
    {
        public int Stt { get; set; }
        public string MaHocSinh { get; set; }
        public string HoTen { get; set; }         
        public DateTime? NgaySinh { get; set; }
        public bool GioiTinh { get; set; }
        public int Ngaycophep { get; set; }
        public int Ngaykhongphep { get; set; }
    }
}