﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections.Generic;
namespace VnEduPlus.Models
{
    public class ViewModelHocsinh
    {

        public List<Users_LopHoc> ListKhoiHoc { get; set; }
        public List<LopHoc> ListlopHoc { get; set; }
        public string  mahocsinh { get; set; }
        public String TenHocsinh { get; set; }
        public int songaynghiphep { get; set; }
        public int songaynghikhongphep { get; set; }
            
    }
}