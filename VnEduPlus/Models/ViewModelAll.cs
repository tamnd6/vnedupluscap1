﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
 
namespace VnEduPlus.Models
{
    public class ViewModelAll
    {
        public User_Profile user_Profile { get; set; }
        public ScrUser scrUser { get; set; }
        public IEnumerable<ScrFunction> chucnangCha { get; set; }
        public IEnumerable<ScrFunction> chucnangCon { get; set; }
        public HocSinh Hocsinh { get; set; }
        
            
    }

    public class ViewMessageDiem
    {
        public string soCotDiem15P { get; set; }
        public string soCotDiemM { get; set; }
        public string soCotDiem45P { get; set; }

        public int InputType { get; set; }
        public int checkKhoaKy { get; set; }
      //   public List<HocSinhDiemMonhoc> hocSinh_DiemList { get; set; }
        public List<HocSinhDiem> hocSinh_DiemList { get; set; }
        public HocSinhKhoaCotDiem hocSinhKhoaCotDiemList { get; set; }
        public string update { get; set; }
    }

    public class ViewDiem
    {
       // public string soCotDiemKTTX { get; set; }
       // public string soCotDiemKTDK { get; set; }

        public string soCotDiem15P { get; set; }
        public string soCotDiemM { get; set; }
        public string soCotDiem45P { get; set; }

        public int InputType { get; set; }
        public int checkKhoaKy { get; set; }
        public List<HocSinhDiem> hocSinh_DiemList { get; set; }
        public string update { get; set; }
    }

    public class ViewLopHocForUser
    {
        public string monHocChuNhiem { get; set; }
        public List<System.Web.Mvc.SelectListItem> LopList { get; set; }
       
       
    }
    public class LopHocKhoi
    {
         
        public string Malop { get; set; }
        public string Tenlop { get; set; }
       
    }

    public class KhoiMonHoc
    {
        public string makhoi { get; set; }
        public int tinhdiem { get; set; }
        public int sumDKM { get; set; }
        public int sumDK15P { get; set; }
        public int sumDK45P { get; set; }
    }
}