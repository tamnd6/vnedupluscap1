﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Lib.Web.Mvc.JQuery.JqGrid;
using VnEduPlus.CmonFunction;
using VnEduPlus.Models;
using System.Globalization;
using System.Threading;
namespace VnEduPlus.Controllers
{
    public class GiaoVienChuNhiemController : Controller
    {

        /// <summary>
        /// Khai bao bien toan cuc
        /// </summary>
        EDUNP_CAP1Entities db = new EDUNP_CAP1Entities();

        /// <summary>
        /// lay username dang nhap 
        /// </summary>
        /// <returns></returns>
        public string getUserName()
        {
            return User.Identity.Name.ToString() == "" ? "superadmin" : User.Identity.Name.ToString();

        }
        [Authorize]
        public ActionResult Index()
        {
            ViewBag.Message = "Chương trình ứng dụng công nghệ thông tin quản lý trường học 2012-2020!";

            return View();
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Authorize]



        #region Quản lý nhận xét

        public ActionResult TemplateNhanXetThang()
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Thongbao");

            }


            // lay thong tin truong 
            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            bool isHieuTruong = Cmon.isHieuTruong(idTruong, userName);
            ViewBag.isHieuTruong = isHieuTruong;
            string namHoc = Cmon.GetNamHienTai(idTruong);
            ViewBag.namhoc = namHoc;
            ViewBag.idtruong = idTruong;
            ViewBag.danhMucTieuChi = Cmon.createDanhMuc();
            ViewBag.danhMucThang = Cmon.createCacThangCoDinh(idTruong, db);
            ViewBag.tenuser = Cmon.GetnameUser(userName);

            var listkhoi = (from khoi in db.DM_Khoi
                            let makhoiList = from lophoc in db.LopHocs
                                             join userlh in db.Users_LopHoc on lophoc.MaLop equals userlh.MaLop
                                             where userlh.UserName == userName && userlh.idTruong == idTruong && userlh.NamHoc == namHoc
                                             select lophoc.MaKhoi
                            where makhoiList.Contains(khoi.MaKhoi) && khoi.idTruong == idTruong && khoi.NamHoc == namHoc
                            select khoi).ToList();

            ViewBag.makhoi = new SelectList(listkhoi, "MaKhoi", "TenKhoi");
            return View();
        }


        #region Quản lý thêm mới nhận xét

        public ActionResult AddNhanXet()
        {
            return PartialView(@"~/Views/GiaoVienChuNhiem/AddNhanXet.cshtml");
        }

        public bool checkExitMaNhanXet(int type, string maNhanXet, string thang, int idTruong)
        {
            bool isExit = false;
            short th = Convert.ToInt16(thang);
            DM_NhanXetMau_GVCN kt = (from kthuc in db.DM_NhanXetMau_GVCN.Where(t => t.LoaiNhanXet == type && t.MaNhanXet == maNhanXet && t.Thang == th && t.idTruong == idTruong) select kthuc).FirstOrDefault();
            if (kt!=null) {
                isExit = true;
            }
            return isExit;

        }


        [HttpGet]
        public JsonResult ThemMoiNhanXet(int type, string maNhanXet, string tenNhanXet, bool isdefault, string thang, int idtruong)
        {
            string mess = "";
            
                Cmon.UpdateStatusCNNX(type,Int16.Parse(thang), db);
                DM_NhanXetMau_GVCN kt = new DM_NhanXetMau_GVCN();
                kt.Thang = Convert.ToInt16(thang);

                kt.idTruong = idtruong;
                kt.LoaiNhanXet = (short)type;
                kt.TenNhanXet = tenNhanXet;
                kt.MaNhanXet = DateTime.Now.Ticks.ToString();
                kt.UserCreated = getUserName();
                kt.isDefault = isdefault;
                db.DM_NhanXetMau_GVCN.AddObject(kt);
                int rs = db.SaveChanges();
                if (rs > 0)
                    mess = "OK";
                else
                    mess = "Thêm mới tiêu chí không thành công!";
          
            return Json(mess, JsonRequestBehavior.AllowGet);
        }


        #endregion

        #region Quản lý sửa nhận xét

        public ActionResult EditNhanXet(string loaiNhanXet, string manx, string thang, int idTruong)
        {

            short th = Convert.ToInt16(thang);
            short loaiNX = Convert.ToInt16(loaiNhanXet);
            var kthuc = (from kt in db.DM_NhanXetMau_GVCN.Where(t => t.LoaiNhanXet == loaiNX && t.Thang == th && t.MaNhanXet == manx && t.idTruong == idTruong) select kt).FirstOrDefault();

            return PartialView(@"~/Views/GiaoVienChuNhiem/EditNhanXet.cshtml", kthuc);
        }

        [HttpGet]
        public JsonResult UpdateNhanXet(int type, string maNhanXet, string tenNhanXet, bool isdefault, string thang, int idtruong)
        {
            string mess = "";
            //if (string.IsNullOrEmpty(maNhanXet.Trim()))
            //    mess = "Yêu cầu nhập mã nhận xét";
            //else if (string.IsNullOrEmpty(tenNhanXet.Trim()))
            //    mess = "Yêu cầu nhập tên nhận xét";
            //else
            //{
                Cmon.UpdateStatusCNNX(type,Int16.Parse(thang), db);
                short th = Convert.ToInt16(thang);
                DM_NhanXetMau_GVCN kt = (from kthuc in db.DM_NhanXetMau_GVCN.Where(t => t.LoaiNhanXet == type && t.MaNhanXet == maNhanXet && t.Thang == th && t.idTruong == idtruong) select kthuc).FirstOrDefault();
                if (kt != null)
                {
                    kt.TenNhanXet = tenNhanXet;
                    kt.UserCreated = getUserName();
                    kt.isDefault = isdefault;
                    int rs = db.SaveChanges();
                    if (rs > 0)
                        mess = "OK";
                    else
                        mess = "Cập nhật nhận xét không thành công!";
                }
                else
                {
                    mess = "Không có thông tin cập nhật!";
                }
            //}
            return Json(mess, JsonRequestBehavior.AllowGet);
        }


        #endregion

        #region Quản lý xóa nhận xét
        [HttpGet]
        public JsonResult DeleteNhanXet(int type, string maNhanXet, string thang, int idtruong)
        {
            string mess = "";
            short th = Convert.ToInt16(thang);
            DM_NhanXetMau_GVCN kt = (from kthuc in db.DM_NhanXetMau_GVCN.Where(t => t.MaNhanXet == maNhanXet && t.LoaiNhanXet == type && t.Thang == th && t.idTruong == idtruong) select kthuc).FirstOrDefault();
            if (kt != null)
            {

                db.DM_NhanXetMau_GVCN.DeleteObject(kt);
                int del = db.SaveChanges();
                if (del > 0)
                    mess = "OK";
                else
                    mess = "Xóa tiêu chí không thành công!";
            }
            else
            {
                mess = "Không có thông tin để xóa!";
            }
            return Json(mess, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Quản lý hiện thị danh sách nhận xét

        [AcceptVerbs(HttpVerbs.Get)]
        [Authorize]
        public ActionResult hienDanhSachNhanXet(int idTruong, int type, int thang)
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Thongbao");

            }
            string userName = getUserName();

            var dsNhanXet = (from nhanxet in db.DM_NhanXetMau_GVCN
                             where nhanxet.idTruong == idTruong && nhanxet.LoaiNhanXet == type && nhanxet.Thang == thang && nhanxet.UserCreated == userName
                             orderby nhanxet.TenNhanXet
                             select nhanxet).ToList();

            return PartialView("HienThiDanhSachNhanXet", dsNhanXet);
        }




        [HttpPost]
        public ActionResult ThemDanhSachThuVienMauCau(string maKhoi, string thangNX, string loaiNX, int[] danhSachMau)
        {
            //1. Kieem tra xem noi dung dua vao da co hay chua
            //2. Neu chua co thi thuc hien viec chen moi, neu co roi thi thuc hien cap nhat


            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            string namHoc = Cmon.GetNamHienTai(idTruong);
            string mess = "";
            try
            {
                for (int i = 0; i < danhSachMau.Length; i++)
                {
                    int id = danhSachMau[i];
                    var thuvienMau = (from thuvien in db.ThuVien_NhanXetMauGVCN
                                      where thuvien.ID == id
                                      select thuvien.Noidung).FirstOrDefault();
                    // lay ra noi dung chen vao bang
                    DM_NhanXetMau_GVCN DMNX = new DM_NhanXetMau_GVCN();
                    DMNX.idTruong = idTruong;
                    DMNX.LoaiNhanXet = (short)Convert.ToInt16(loaiNX);
                    DMNX.Thang = (short)Convert.ToInt16(thangNX);
                    DMNX.MaNhanXet = DateTime.Now.Ticks.ToString();
                    DMNX.TenNhanXet = thuvienMau;
                    DMNX.UserCreated = userName;
                    DMNX.isDefault = false;
                    db.AddToDM_NhanXetMau_GVCN(DMNX);
                    db.SaveChanges();
                }
            }
            catch
            {
                mess = "ERROR";

            }
            
            return Json(mess, JsonRequestBehavior.AllowGet);
        }



        [AcceptVerbs(HttpVerbs.Post)]
        [Authorize]
        public ActionResult timKiemMauNhanXet(string keyword)
        {
            
            string userName = getUserName();

            var dsNhanXet = (from maunx in db.ThuVien_NhanXetMauGVCN
                             where maunx.Noidung.Contains(keyword)
                             orderby maunx.ID
                             select maunx).ToList();
            return View(@"~/Views/GiaoVienChuNhiem/KetQuaTimKiemMauNhanXet.cshtml", dsNhanXet);
           
        }

        [HttpGet]
        public ActionResult danhSachThuVienMauCau(string maKhoi, string loaiNX, int thang)
        {
            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            string namhoc = Cmon.GetNamHienTai(idTruong);

            ViewBag.idtruong = idTruong;
            ViewBag.tenTruong = Cmon.GetnameSchool(idTruong);
            ViewBag.namHoc = namhoc;
            ViewBag.maKhoi = maKhoi;
            ViewBag.thang = thang;
            ViewBag.loaiNX = loaiNX;

            var comments = (from maunx in db.ThuVien_NhanXetMauGVCN
                            where maunx.LoaiNX == loaiNX && maunx.Thang == thang && maunx.MaKhoi == maKhoi
                            orderby maunx.ID descending
                            select maunx).ToList();
            return View(@"~/Views/GiaoVienChuNhiem/DanhSachMauNhanXet.cshtml", comments);
        }
        

        #endregion


        #endregion Quản lý nhận xét


        #region Quản lý nhận xét theo học kỳ

            public ActionResult TemplateNhanXetKy()
            {
                if (!Request.IsAuthenticated)
                {
                    return RedirectToAction("Index", "Thongbao");

                }

                // lay thong tin truong 
                string userName = getUserName();
                int idTruong = Cmon.getIdtruong(userName, db);
                bool isHieuTruong = Cmon.isHieuTruong(idTruong, userName);
                ViewBag.isHieuTruong = isHieuTruong;
                ViewBag.namhoc = Cmon.GetNamHienTai(idTruong); 
                ViewBag.idtruong = idTruong;
                ViewBag.danhMucTieuChi = Cmon.createDanhMuc();
                ViewBag.danhMucThang = Cmon.createCacThangCoDinh(idTruong, db);
                ViewBag.tenuser = Cmon.GetnameUser(userName); 
                ViewBag.danhmucMonHoc = LoadMonhoc();
                ViewBag.nangLuc = Cmon.CreateNangLuc();
                ViewBag.phamChat = Cmon.CreatePhamChat();
                ViewBag.hocKy = Cmon.CreateHocKy();
                return View();
            }


            /// <summary>
            /// id : la ma lop
            /// load nhung mon hoc thuoc mot lop hoc
            /// </summary>
            /// <param name="id"></param>
            /// <returns></returns>
            
            public List<MonHoc> LoadMonhoc()
            {
                string userName = getUserName();
                int idTruong = Cmon.getIdtruong(userName, db);
                string namhoc = Cmon.GetNamHienTai(idTruong);

                List<MonHoc> monHocs =  new List<MonHoc>();
                MonHoc mhoc = new MonHoc();
                mhoc.maMonHoc="-1";
                mhoc.tenMonHoc="--Lựa chọn môn học--";
                monHocs.Add(mhoc);
                                
                var modelData = (from monhoc in db.ASSIGN_LOP_MonHoc
                                 join userMon in db.Users_LopHoc
                                 on monhoc.MaLop equals userMon.MaLop
                                 where userMon.UserName == userName && userMon.NamHoc== namhoc && userMon.idTruong == idTruong
                                 orderby monhoc.MaMonHoc
                                 select new MonHoc
                                 {
                                    maMonHoc= monhoc.MaMonHoc,
                                    tenMonHoc= monhoc.TenMonHoc

                                 }).Distinct().ToList();

                modelData.Insert(0, mhoc);
                 
                return modelData;
            }
        
        #region Quản lý nhận xét theo học kỳ

            [AcceptVerbs(HttpVerbs.Get)]
            [Authorize]
            public ActionResult hienDanhSachNhanXetTheoHocKy(int idTruong, int type, int hocKy, string maMonHoc)
            {
                if (!Request.IsAuthenticated)
                {
                    return RedirectToAction("Index", "Thongbao");

                }
                string userName = getUserName();
                 
                var dsNhanXet = (from nhanxet in db.DM_NhanXetMau_CuoiKyNam
                                 where nhanxet.idTruong == idTruong && nhanxet.LoaiNhanXet == type && nhanxet.MaMonHoc == maMonHoc && nhanxet.HocKy == hocKy && nhanxet.UserCreated == userName
                                 orderby nhanxet.TenNhanXet
                                 select nhanxet).ToList();

                return PartialView("HienThiDanhSachNhanXetTheoHocKy", dsNhanXet);
            }


        #endregion


        #region Quản lý thêm mới nhận xét theo học kỳ 

            public ActionResult AddNhanXetTheoHocKy()
            {
                return PartialView(@"~/Views/GiaoVienChuNhiem/AddNhanXetTheoHocKy.cshtml");
            }

            [HttpGet]
            public JsonResult ThemMoiNhanXetTheoHocKy(int type, string maNhanXet, string tenNhanXet, bool isdefault, int hocKy, int idtruong, string maMonHoc)
            {
                string mess = "";
                //if (string.IsNullOrEmpty(maNhanXet.Trim()))
                //    mess = "Yêu cầu nhập mã nhận xét";
                //else if (string.IsNullOrEmpty(tenNhanXet.Trim()))
                //    mess = "Yêu cầu nhập tên nhận xét";
                //else
                //{
                    Cmon.UpdateStatusNhanXetTheoHocKy(type, maMonHoc,hocKy, db);
                    DM_NhanXetMau_CuoiKyNam kt = new DM_NhanXetMau_CuoiKyNam();
                    kt.HocKy =(short) hocKy;
                    kt.idTruong = idtruong;
                    kt.MaMonHoc = maMonHoc;
                    kt.LoaiNhanXet = (short)type;
                    kt.TenNhanXet = tenNhanXet;
                    kt.MaNhanXet = DateTime.Now.Ticks.ToString();
                    kt.UserCreated = getUserName();
                    kt.isDefault = isdefault;
                    db.DM_NhanXetMau_CuoiKyNam.AddObject(kt);
                    int rs = db.SaveChanges();
                    if (rs > 0)
                        mess = "OK";
                    else
                        mess = "Thêm mới tiêu chí không thành công!";
                //}
                return Json(mess, JsonRequestBehavior.AllowGet);
            }

        #endregion

        #region Quản lý sửa nhận xét theo học kỳ

            public ActionResult EditNhanXetTheoHocKy(string loaiNhanXet, string manx, int hocKy, int idTruong, string maMonHoc)
            {

                short hky = Convert.ToInt16(hocKy);
                short loaiNX = Convert.ToInt16(loaiNhanXet);
                var kthuc = (from kt in db.DM_NhanXetMau_CuoiKyNam.Where(t => t.LoaiNhanXet == loaiNX && t.HocKy == hky && t.MaNhanXet == manx && t.idTruong == idTruong && t.MaMonHoc == maMonHoc) select kt).FirstOrDefault();

                return PartialView(@"~/Views/GiaoVienChuNhiem/EditNhanXetTheoHocKy.cshtml", kthuc);
            }

            [HttpGet]
            public JsonResult UpdateNhanXetTheoHocKy(int type, string maNhanXet, string tenNhanXet, bool isdefault, int hocKy, int idtruong, string maMonHoc)
            {
                string mess = "";
                //if (string.IsNullOrEmpty(maNhanXet.Trim()))
                //    mess = "Yêu cầu nhập mã nhận xét";
                //else if (string.IsNullOrEmpty(tenNhanXet.Trim()))
                //    mess = "Yêu cầu nhập tên nhận xét";
                //else
                //{
                    Cmon.UpdateStatusNhanXetTheoHocKy(type, maMonHoc,hocKy, db);
                    short hKy = Convert.ToInt16(hocKy);
                    short loaiNhanXet = Convert.ToInt16(type);
                    DM_NhanXetMau_CuoiKyNam kt = (from kthuc in db.DM_NhanXetMau_CuoiKyNam.Where(t => t.LoaiNhanXet == loaiNhanXet && t.MaNhanXet == maNhanXet && t.HocKy == hKy && t.idTruong == idtruong && t.MaMonHoc == maMonHoc) select kthuc).FirstOrDefault();
                    if (kt != null)
                    {
                        kt.MaNhanXet = maNhanXet.Trim();
                        kt.TenNhanXet = tenNhanXet;
                        kt.isDefault = isdefault;
                        int rs = db.SaveChanges();
                        if (rs > 0)
                            mess = "OK";
                        else
                            mess = "Cập nhật nhận xét không thành công!";
                    }
                    else
                    {
                        mess = "Không có thông tin cập nhật!";
                    }
                //}
                return Json(mess, JsonRequestBehavior.AllowGet);
            }


            #endregion


        #region Quản lý xóa nhận xét
            [HttpGet]
            public JsonResult DeleteNhanXetTheoHocKy(int type, string maNhanXet, int hocKy, int idtruong, string maMonHoc)
            {
                string mess = "";
                short hky = Convert.ToInt16(hocKy);
                DM_NhanXetMau_CuoiKyNam kt = (from kthuc in db.DM_NhanXetMau_CuoiKyNam.Where(t => t.MaNhanXet == maNhanXet && t.LoaiNhanXet == type && t.MaMonHoc == maMonHoc && t.HocKy == hky && t.idTruong == idtruong) select kthuc).FirstOrDefault();
                if (kt != null)
                {
                    db.DM_NhanXetMau_CuoiKyNam.DeleteObject(kt);
                    int del = db.SaveChanges();
                    if (del > 0)
                        mess = "OK";
                    else
                        mess = "Xóa tiêu chí không thành công!";
                }
                else
                {
                    mess = "Không có thông tin để xóa!";
                }
                return Json(mess, JsonRequestBehavior.AllowGet);
            }

            #endregion

        #endregion Quản lý nhận xét theo học kỳ

    }
    
}
