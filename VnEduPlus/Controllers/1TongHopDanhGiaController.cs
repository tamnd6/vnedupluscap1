﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Lib.Web.Mvc.JQuery.JqGrid;
using VnEduPlus.CmonFunction;
using VnEduPlus.Models;
using System.Globalization;
using System.Threading;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.IO;

namespace VnEduPlus.Controllers
{
    public class TongHopDanhGiaController: Controller
    {
        EDUNP_CAP1Entities db = new EDUNP_CAP1Entities();
        public string getUserName()
        {
            return User.Identity.Name.ToString() == "" ? "superadmin" : User.Identity.Name.ToString();

        }


        public ActionResult TongHopNhanXetHocSinh()
        {
            string userName = getUserName();

            int idTruong = Cmon.getIdtruong(userName, db);
            string namhoc = Cmon.GetNamHienTai(idTruong);
            bool isHieuTruong = Cmon.isHieuTruong(idTruong, userName);
            ViewBag.isHieuTruong = isHieuTruong;
            var khois = (from khoi in db.DM_Khoi
                         let makhoiList = from lh in db.LopHocs
                                          join userlh in db.Users_LopHoc
                                          on lh.MaLop equals userlh.MaLop
                                          where lh.idTruong == idTruong && lh.NamHoc == namhoc && userlh.UserName == userName
                                          select lh.MaKhoi
                         where makhoiList.Contains(khoi.MaKhoi) && khoi.idTruong == idTruong && khoi.NamHoc == namhoc
                         select khoi).ToList();

            DM_Khoi kl = new DM_Khoi();
            kl.MaKhoi = "";
            kl.TenKhoi = "--Chọn khối--";
            kl.idTruong = idTruong;
            kl.NamHoc = namhoc;
            khois.Insert(0, kl);

            ViewBag.khoi = khois;
            ViewBag.namhientai = namhoc;

            var lophoc = (from lh in db.LopHocs
                          join ul in db.Users_LopHoc
                          on new { lh.MaLop } equals new { ul.MaLop }
                          where lh.idTruong == idTruong && lh.NamHoc == namhoc && ul.UserName == userName && ul.idTruong == idTruong && ul.NamHoc == namhoc
                          select new LopHocGiaoVien
                          {
                              MaLop = lh.MaLop,
                              TenLop = lh.TenLop,
                              IdTruong = lh.idTruong,
                              NamHoc = lh.NamHoc
                          }).Distinct().ToList();
            LopHocGiaoVien lhoc = new LopHocGiaoVien();
            lhoc.MaLop = "";
            lhoc.TenLop = "--Chọn lớp học--";
            lhoc.NamHoc = namhoc;
            lhoc.IdTruong = idTruong;
            lophoc.Insert(0, lhoc);

            ViewBag.lophoc = lophoc;

            ViewBag.hocky = Cmon.CreateHocKy();

            //Cmon.listMonHoc = (from mh in db.DM_MonHoc select mh).ToList();


            return View(@"~/Views/TongHop/TongHopNhanXetHocSinh.cshtml");
        }

        public string getHoTen(String maHocSinh, int idTruong, string maLop, string namHoc)
        {
            return  (from hocsinh in db.HocSinhs
                           where hocsinh.idTruong == idTruong && hocsinh.MaLop == maLop && hocsinh.NamHoc == namHoc && hocsinh.MaHocSinh == maHocSinh
                           select hocsinh).FirstOrDefault().HoTen.ToString();
        }

        [HttpPost]
        [Authorize]
        public ActionResult saveKhenThuong(string ngayKhen, string noidung, string khenThuong, string khenThuongCT, int idtruong, string namhoc, string malop, string mahocsinh, string maHK)
        {
            string notice ="OK";
            // string userName = User.Identity.Name.ToString() == "" ? "superadmin" : User.Identity.Name.ToString();
            string ngayKt = ngayKhen.Split('/')[2] + "-" + ngayKhen.Split('/')[1] + "-" + ngayKhen.Split('/')[0];
            DateTime ngayK = DateTime.Parse(ngayKt);
            var hocsinhkt = (from kt in db.HocSinh_KhenThuongKyLuat
                             where kt.idTruong == idtruong && kt.NamHoc == namhoc
                             && kt.MaLop == malop && kt.MaHocSinh == mahocsinh
                             && kt.Ngay == ngayK
                             select kt).FirstOrDefault();
            if (hocsinhkt != null)
            {
                notice = "NOT";
            }
            else
            {

                try
                {

                    HocSinh_KhenThuongKyLuat hocSinhKhenThuongKyLuat = new HocSinh_KhenThuongKyLuat();
                    hocSinhKhenThuongKyLuat.MaHocSinh = mahocsinh;
                    hocSinhKhenThuongKyLuat.MaHocSinhFriendly = mahocsinh;
                    hocSinhKhenThuongKyLuat.KhenThuong = khenThuong != null && khenThuong == "1" ? true : false;
                    hocSinhKhenThuongKyLuat.NoiDung = noidung;
                    hocSinhKhenThuongKyLuat.KhenThuongCapTruong = khenThuongCT != null && khenThuongCT == "1" ? true : false;

                    hocSinhKhenThuongKyLuat.MaLop = malop;
                    hocSinhKhenThuongKyLuat.idTruong = idtruong;
                    hocSinhKhenThuongKyLuat.Ngay = DateTime.ParseExact(ngayKhen, "dd/MM/yyyy", null);
                    hocSinhKhenThuongKyLuat.NamHoc = namhoc;
                    hocSinhKhenThuongKyLuat.MaHocKy = maHK;
                    hocSinhKhenThuongKyLuat.HoTen = getHoTen(mahocsinh, idtruong, malop, namhoc);

                    db.HocSinh_KhenThuongKyLuat.AddObject(hocSinhKhenThuongKyLuat);
                    db.SaveChanges();
                }
                catch (Exception ex)
                {

                }
            }

            return new JqGridJsonResult() { Data = notice };
        }

        [HttpGet]
        [Authorize] 
       
        public JsonResult XoaMotKhenThuong(string maHS, string maLop, string namHoc, int idTruong, string hocKy, string ngay)
        {
            string mess = "";
            
            string username = getUserName();
            DateTime ngayTruyVan = DateTime.ParseExact(ngay, "dd/MM/yyyy", null);
            var kt = (from khenThuong in db.HocSinh_KhenThuongKyLuat.Where(t => t.idTruong == idTruong && t.NamHoc == namHoc && t.MaLop == maLop
                && t.MaHocSinh == maHS &&  t.Ngay == ngayTruyVan)
                      select khenThuong).FirstOrDefault();
            if (kt != null)
            {

                db.HocSinh_KhenThuongKyLuat.DeleteObject(kt);
                int del = db.SaveChanges();
                if (del > 0)
                    mess = "OK";
                else
                    mess = "Xóa tiêu chí không thành công!";
            }
            else
            {
                mess = "Không có thông tin để xóa!";
            }
            return Json(mess, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [Authorize]

        public JsonResult XoaToanBoKhenThuong(string maHS, string maLop, string namHoc, int idTruong, string hocKy)
        {
            string mess = "";

            string username = getUserName();
            
            var kt = (from khenThuong in db.HocSinh_KhenThuongKyLuat.Where(t => t.idTruong == idTruong && t.NamHoc == namHoc && t.MaLop == maLop
                && t.MaHocSinh == maHS)
                      select khenThuong).FirstOrDefault();
            if (kt != null)
            {

                db.HocSinh_KhenThuongKyLuat.DeleteObject(kt);
                int del = db.SaveChanges();
                if (del > 0)
                    mess = "OK";
                else
                    mess = "Xóa tiêu chí không thành công!";
            }
            else
            {
                mess = "Không có thông tin để xóa!";
            }
            return Json(mess, JsonRequestBehavior.AllowGet);
        }



        [AcceptVerbs(HttpVerbs.Get)]
        [Authorize]
        public ActionResult DanhSachHocSinhLopHoc(string maLop)
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Thongbao");

            }
            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            string namHoc = Cmon.GetNamHienTai(idTruong);

            var danhsach = (from hs in db.HocSinhs
                            where hs.idTruong == idTruong && hs.NamHoc == namHoc && hs.MaLop == maLop
                            && !(from hsqt in db.HocSinh_QuaTrinh.Where(hsqt => hsqt.idTruong == idTruong && hsqt.NamHoc == namHoc && hsqt.MaLop == maLop && hsqt.MaQuaTrinh == "BH")
                                    select hsqt.MaHocSinh).Contains(hs.MaHocSinh)
                            orderby hs.STT ascending
                            select new ThongTinHocSinh { 
                                STT = hs.STT,
                                maHS = hs.MaHocSinh,
                                hoTenHS = hs.HoTen
                            
                            }
                            ).ToList();

            return PartialView(@"~/Views/TongHop/TemplateHocSinh.cshtml", danhsach);
        }


        [AcceptVerbs(HttpVerbs.Get)]
        [Authorize]
        public ActionResult GetDiemHocSinh(string maHs, string maLop, string hocKy)
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Thongbao");

            }

            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            string namHoc = Cmon.GetNamHienTai(idTruong);
            string hoTenHs = Cmon.getHoTenHocSinh(maHs, idTruong, maLop, namHoc);

            short maKy = (short)Int64.Parse(hocKy);
            string maHocKy = hocKy == "0" ? "HK1" : "HK2";
            var diemMonHoc = (from monhoc in db.ASSIGN_LOP_MonHoc
                                  join hs in db.HocSinh_Diem_NhanXet
                                  on new { monhoc.MaLop, monhoc.MaMonHoc} equals new { hs.MaLop, hs.MaMonHoc}
                                  where monhoc.idTruong == idTruong && monhoc.NamHoc == namHoc && hs.MaLop == maLop && hs.MaHocSinh == maHs
                                  select new DiemHocSinh
                                  {
                                      maMon = monhoc.MaMonHoc,
                                      tenMon = monhoc.TenMonHoc,
                                      nhanxet = hocKy == "0" ? hs.NhanXetHK1 : hs.NhanXetCN,
                                      DiemDKy = hocKy == "0" ? hs.DiemHK1 : hs.DiemCN,
                                      DiemTX = hocKy == "0" ?hs.KQMonHocHK1 : hs.KQMonHocCN

                                  }).ToList();

            var nhanXetChung = (from nl in db.HocSinh_TongHopNX 
                                where nl.idTruong == idTruong && nl.NamHoc == namHoc && nl.MaLop == maLop 
                                      && nl.MaHocSinh == maHs && nl.HocKy == maKy
                                  select new NhanXetChung {
                                    NXNangLuc1 = nl.NXNangLuc1,
                                    NXNangLuc2 = nl.NXNangLuc2,
                                    NXNangLuc3 = nl.NXNangLuc3,

                                    NXPhamChat1 = nl.NXPhamChat1,
                                    NXPhamChat2 = nl.NXPhamChat2,
                                    NXPhamChat3 = nl.NXPhamChat3,
                                    NXPhamChat4 = nl.NXPhamChat4,
                                    KhenThuong = (from hskt in db.HocSinh_KhenThuongKyLuat where hskt.idTruong == idTruong && hskt.MaHocKy == maHocKy && hskt.MaLop == maLop && hskt.MaHocSinh == maHs select hskt).AsEnumerable(),
                                    HoanThanhCT = nl.HoanThanhCT,
                                    DuocLenLop = nl.DuocLenLop


                                  }).FirstOrDefault();


            NhanXetHocSinh hocSinhNhanXet = new NhanXetHocSinh();
            hocSinhNhanXet.DanhSachDiemMonHoc = diemMonHoc;
            hocSinhNhanXet.nhanXetChung = nhanXetChung;
            hocSinhNhanXet.maHS = maHs;

            var QuyenNhapDiem = (from RU in db.ScrJRoleUsers
                                 join RF in db.ScrJRoleFunctions
                                     on RU.RoleName equals RF.RoleName
                                 where RU.UserName == userName && RF.idTruong == idTruong
                                 && RF.FunctionID == 116 && RF.Insert_ == true && RF.Enable == true && RF.Update_ == true
                                 select RF
                         ).FirstOrDefault();

            ViewBag.phanQuyen = QuyenNhapDiem != null ? 1 : 0;
            ViewBag.maHK = hocKy;
            ViewBag.maLop = maLop;
            ViewBag.maHocSinh = maHs;
            ViewBag.idTruong = idTruong;
            ViewBag.maKhoi = Cmon.getKhoi(maLop, namHoc, idTruong);
            ViewBag.hoTenHocSinh = hoTenHs;
            ViewBag.btnId = "btnRefresh";
            ViewBag.formId = "formqt";
            return PartialView(@"~/Views/TongHop/TemplateBangDanhGiaHocSinh.cshtml", hocSinhNhanXet);
        }



        [AcceptVerbs(HttpVerbs.Get)]
        [Authorize]
        public ActionResult LoadMacDinhNhanXet(string maHs, string maLop, string hocKy)
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Thongbao");

            }

            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            string namHoc = Cmon.GetNamHienTai(idTruong);
            short maKy = (short)Int64.Parse(hocKy);
            ViewBag.mahs = maHs;
            var diemMonHocHienTai = (from monhoc in db.ASSIGN_LOP_MonHoc
                              join hs in db.HocSinh_Diem_NhanXet
                              on new { monhoc.MaLop, monhoc.MaMonHoc} equals new { hs.MaLop, hs.MaMonHoc}
                              where monhoc.idTruong == idTruong && monhoc.NamHoc == namHoc && hs.MaLop == maLop && hs.MaHocSinh == maHs
                              select new DiemHocSinh
                              {
                                  maMon = monhoc.MaMonHoc,
                                  tenMon = monhoc.TenMonHoc,
                                  nhanxet = hocKy == "0" ? hs.NhanXetHK1 : hs.NhanXetCN,
                                  DiemDKy = hocKy == "0" ? hs.DiemHK1 : hs.DiemCN,
                                  DiemTX = hocKy == "0" ? hs.KQMonHocHK1 : hs.KQMonHocCN

                              }).ToList();


            var nhanXetChungHienTai = new NhanXetChung();
           var nhanXetChungHienTai_= (from nl in db.HocSinh_TongHopNX
                                where nl.idTruong == idTruong && nl.NamHoc == namHoc && nl.MaLop == maLop
                                    && nl.MaHocSinh == maHs && nl.HocKy == maKy
                                select new NhanXetChung
                                {
                                    NXNangLuc1 = nl.NXNangLuc1,
                                    NXNangLuc2 = nl.NXNangLuc2,
                                    NXNangLuc3 = nl.NXNangLuc3,

                                    NXPhamChat1 = nl.NXPhamChat1,
                                    NXPhamChat2 = nl.NXPhamChat2,
                                    NXPhamChat3 = nl.NXPhamChat3,
                                    NXPhamChat4 = nl.NXPhamChat4,
                                    //KhenThuong = nl.KhenThuong,
                                    HoanThanhCT = nl.HoanThanhCT,
                                    DuocLenLop = nl.DuocLenLop


                                }).FirstOrDefault();

          
            var diemMonHoc = (from monhoc in db.ASSIGN_LOP_MonHoc
                             join dmnx in db.DM_NhanXetMau_CuoiKyNam
                              on monhoc.MaMonHoc equals dmnx.MaMonHoc
                              where monhoc.idTruong == idTruong && monhoc.NamHoc == namHoc && monhoc.MaLop == maLop  
                              && dmnx.HocKy== maKy && dmnx.isDefault == true  && dmnx.UserCreated == userName
                              select new DiemHocSinh
                              {
                                  maMon = monhoc.MaMonHoc,
                                  tenMon = monhoc.TenMonHoc,
                                  nhanxet =dmnx.TenNhanXet

                              }).ToList();

            var nhanXetNangLuc = (from nl in db.DM_NhanXetMau_CuoiKyNam
                                  where nl.idTruong == idTruong && nl.LoaiNhanXet == 2 && nl.isDefault == true
                                  select new DiemHocSinh
                                  {
                                      maMon = nl.MaMonHoc,
                                      nhanxet = nl.TenNhanXet
                                  }).ToList();


            var nhanXetPhamChat = (from nl in db.DM_NhanXetMau_CuoiKyNam
                                  where nl.idTruong == idTruong && nl.LoaiNhanXet == 3 && nl.isDefault == true
                                  select new DiemHocSinh
                                  {
                                      maMon = nl.MaMonHoc,
                                      nhanxet = nl.TenNhanXet
                                  }).ToList();
            
             
           
             NhanXetChung nhanXetChung = new NhanXetChung();

             nhanXetChung.NXNangLuc1 = "";
             nhanXetChung.NXNangLuc2 = "";
             nhanXetChung.NXNangLuc3 = "";

             nhanXetChung.NXPhamChat1 = "";
             nhanXetChung.NXPhamChat2 ="";
             nhanXetChung.NXPhamChat3 ="";
             nhanXetChung.NXPhamChat4 = "";
    
             if (nhanXetNangLuc != null && nhanXetNangLuc.Count > 0)
             {
                 if (nhanXetNangLuc.Count <= 1)
                 {
                     nhanXetChung.NXNangLuc1 = nhanXetNangLuc[0].nhanxet;
                 }
                 else if (nhanXetNangLuc.Count <= 2)
                 {
                     nhanXetChung.NXNangLuc1 = nhanXetNangLuc[0].nhanxet;
                     nhanXetChung.NXNangLuc2 = nhanXetNangLuc[1].nhanxet;
                 }
                 else
                 {
                     nhanXetChung.NXNangLuc1 = nhanXetNangLuc[0].nhanxet;
                     nhanXetChung.NXNangLuc2 = nhanXetNangLuc[1].nhanxet;
                     nhanXetChung.NXNangLuc3 = nhanXetNangLuc[2].nhanxet;
                 }
             }

             if (nhanXetPhamChat != null && nhanXetPhamChat.Count > 0)
             {
                 if (nhanXetPhamChat.Count <= 1)
                 {
                     nhanXetChung.NXPhamChat1 = nhanXetPhamChat[0].nhanxet;
                 } else if (nhanXetPhamChat.Count <= 2)
                 {
                     nhanXetChung.NXPhamChat1 = nhanXetPhamChat[0].nhanxet;
                     nhanXetChung.NXPhamChat2 = nhanXetPhamChat[1].nhanxet;
                 } else if (nhanXetPhamChat.Count <= 3) {
                     nhanXetChung.NXPhamChat1 = nhanXetPhamChat[0].nhanxet;
                     nhanXetChung.NXPhamChat2 = nhanXetPhamChat[1].nhanxet;
                     nhanXetChung.NXPhamChat3 = nhanXetPhamChat[2].nhanxet;
                 } else 
                 {
                     nhanXetChung.NXPhamChat1 = nhanXetPhamChat[0].nhanxet;
                     nhanXetChung.NXPhamChat2 = nhanXetPhamChat[1].nhanxet;
                     nhanXetChung.NXPhamChat3 = nhanXetPhamChat[2].nhanxet;
                     nhanXetChung.NXPhamChat4 = nhanXetPhamChat[3].nhanxet;
                 } 
                 
             }


            // process data to load default value to current value for each subject


             for (int i = 0; i < diemMonHoc.Count; i++)
             {
                 if (diemMonHoc[i].maMon != null)
                 {
                     for (int j = 0; j < diemMonHocHienTai.Count; j++)
                     {
                         if (diemMonHocHienTai[j].maMon == diemMonHoc[i].maMon)
                         {
                             diemMonHocHienTai[j].nhanxet = diemMonHoc[i].nhanxet;
                             break;

                         }
                     }
                 }
             }

             if (nhanXetChung != null)
             {

                 nhanXetChungHienTai.NXNangLuc1 = nhanXetChung.NXNangLuc1 != null ? nhanXetChung.NXNangLuc1
                     : ((nhanXetChungHienTai_ == null) ? "" : (nhanXetChungHienTai_.NXNangLuc1 == null ? "" : nhanXetChungHienTai_.NXNangLuc1));

                 nhanXetChungHienTai.NXNangLuc2 = nhanXetChung.NXNangLuc2 != null ? nhanXetChung.NXNangLuc2
                     : ((nhanXetChungHienTai_ == null) ? "" : (nhanXetChungHienTai_.NXNangLuc2 == null ? "" : nhanXetChungHienTai_.NXNangLuc2));


                 nhanXetChungHienTai.NXNangLuc3 = nhanXetChung.NXNangLuc3 != null ? nhanXetChung.NXNangLuc3
                     : ((nhanXetChungHienTai_ == null) ? "" : (nhanXetChungHienTai_.NXNangLuc3 == null ? "" : nhanXetChungHienTai_.NXNangLuc3));

                 nhanXetChungHienTai.NXPhamChat1 = nhanXetChung.NXPhamChat1 != null ? nhanXetChung.NXPhamChat1
                     : ((nhanXetChungHienTai_ == null) ? "" : (nhanXetChungHienTai_.NXPhamChat1 == null ? "" : nhanXetChungHienTai_.NXPhamChat1));


                 nhanXetChungHienTai.NXPhamChat2 = nhanXetChung.NXPhamChat2 != null ? nhanXetChung.NXPhamChat2
                     : ((nhanXetChungHienTai_ == null) ? "" : (nhanXetChungHienTai_.NXPhamChat2 == null ? "" : nhanXetChungHienTai_.NXPhamChat2));


                 nhanXetChungHienTai.NXPhamChat3 = nhanXetChung.NXPhamChat3 != null ? nhanXetChung.NXPhamChat3
                     : ((nhanXetChungHienTai == null) ? "" : (nhanXetChungHienTai.NXPhamChat3 == null ? "" : nhanXetChungHienTai.NXPhamChat3));

                 nhanXetChungHienTai.NXPhamChat4 = nhanXetChung.NXPhamChat4 != null ? nhanXetChung.NXPhamChat4
                     : ((nhanXetChungHienTai == null) ? "" : (nhanXetChungHienTai.NXPhamChat4 == null ? "" : nhanXetChungHienTai.NXPhamChat4)); 
                  
             }

             NhanXetHocSinh hocSinhNhanXet = new NhanXetHocSinh();
             hocSinhNhanXet.DanhSachDiemMonHoc = diemMonHocHienTai;
             hocSinhNhanXet.nhanXetChung = nhanXetChungHienTai;
             hocSinhNhanXet.maHS = maHs;

             return PartialView(@"~/Views/TongHop/TemplateBangDanhGiaHocSinh.cshtml", hocSinhNhanXet);
        }


        [HttpGet]
        public ActionResult mauCauNhanXetHocKy(string loaiNhanXet, string maMon, string maLop, short hocKy)
        {
            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            string namhoc = Cmon.GetNamHienTai(idTruong);
            int loaiNX = Int16.Parse(loaiNhanXet);

            var dsNhanXet = (from nhanxet in db.DM_NhanXetMau_CuoiKyNam
                             where nhanxet.idTruong == idTruong && nhanxet.LoaiNhanXet == loaiNX && nhanxet.MaMonHoc == maMon && nhanxet.HocKy == hocKy && nhanxet.UserCreated == userName
                             orderby nhanxet.TenNhanXet
                             select nhanxet).ToList();

            ViewBag.maMon = maMon;


            return PartialView(@"~/Views/TongHop/maucaunhanxethocky.cshtml", dsNhanXet);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        
        public JsonResult LoadMacDinhGVBMNhanXet(string maHs, string maLop, string maMon, string hocKy)
        {
         
            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            string namHoc = Cmon.GetNamHienTai(idTruong);
            short maKy = (short)Int64.Parse(hocKy);

            var diemMonHocHienTai = (from monhoc in db.HocSinh_DanhGiaMonHoc
                                     where monhoc.idTruong == idTruong && monhoc.NamHoc == namHoc && monhoc.MaLop == maLop && monhoc.MaHocSinh == maHs && monhoc.MaMonHoc == maMon
                                     select new DiemHocSinh
                                     {
                                         maMon = monhoc.MaMonHoc,
                                         nhanxet = hocKy == "0" ? monhoc.NXT5_KT : monhoc.NXT10_KT,
                                         DiemDKy = 0,
                                         DiemTX = ""
                                     }).FirstOrDefault();



            string noidungnx = diemMonHocHienTai == null ? "" : (diemMonHocHienTai.nhanxet == null) ? "" : diemMonHocHienTai.nhanxet;

           return Json(noidungnx, JsonRequestBehavior.AllowGet);
        }

        public ActionResult BaoCaoThongKe()
        {
            string userName = getUserName();

            int idTruong = Cmon.getIdtruong(userName, db);
            string namhoc = Cmon.GetNamHienTai(idTruong);
            ViewBag.namhientai = namhoc;
            ViewBag.tenuser = Cmon.GetnameUser(userName);
            ViewBag.idTruong = idTruong;
            ViewBag.hocKyMacDinh = new SelectList(Cmon.CreateHocKyTheoDanhGia(), "maHocKy", "tenHocKy", "GHK1");
            return View(@"~/Views/TongHop/ViewThongKe.cshtml");
        }
        
        public ActionResult ThongKeDiemKiemTra(string MaHocKy, string NamHoc)
        {
            string userName = getUserName();

            int idTruong = Cmon.getIdtruong(userName, db);
            string namhoc = NamHoc;
            ViewBag.CapQuanLy = Cmon.getDonViChuQuan(idTruong);
            ViewBag.TenTruong = Cmon.GetnameSchool(idTruong);
            string TenHocKy = "";
            if (MaHocKy == "K1" || MaHocKy == "HK1") TenHocKy = "HỌC KỲ I";
            else if (MaHocKy == "GK1" || MaHocKy == "GHK1") TenHocKy = "GIỮA HỌC KỲ I";
            else if (MaHocKy == "GK2" || MaHocKy == "GHK2") TenHocKy = "GIỮA HỌC KỲ II";
            else TenHocKy = "CUỐI NĂM";
            ViewBag.HocKy = TenHocKy;
            ViewBag.NamHoc = namhoc;
            DataTable dt = new DataTable();

            SqlParameter[] para = new SqlParameter[3];
            para[0] = new SqlParameter("@IdTruong", idTruong);
            para[1] = new SqlParameter("@NamHoc", namhoc);
            para[2] = new SqlParameter("@MaHocKy", MaHocKy);
            //string connectionString = ConfigurationManager.ConnectionStrings["ConnectionStr"].ConnectionString;

            HNAFramework.DataAccess.Framework.DBHelper dbHelper = new HNAFramework.DataAccess.Framework.DBHelper();
            dt = dbHelper.GetDataTabel("spThongKeDiem", para);
            Session["TongHopDiem"] = dt;
            Session["MaHocKy"] = MaHocKy;
            Session["NamHoc"] = NamHoc;
            

            return PartialView(@"~/Views/TongHop/ThongKeDiemKiemTra.cshtml", dt);
        }

        public ActionResult ThongKeSoLieu()
        {
            string userName = getUserName();

            int idTruong = Cmon.getIdtruong(userName, db);
            string namhoc = Cmon.GetNamHienTai(idTruong);
            ViewBag.namhientai = namhoc;
            ViewBag.tenuser = Cmon.GetnameUser(userName);
            ViewBag.idTruong = idTruong;
            ViewBag.hocKyMacDinh = new SelectList(Cmon.CreateHocKyTheoDanhGia(), "maHocKy", "tenHocKy", "GHK1");
            var listkhoi = (from khoi in db.DM_Khoi
                            let makhoiList = from lophoc in db.LopHocs
                                             join userlh in db.Users_LopHoc on lophoc.MaLop equals userlh.MaLop
                                             where userlh.UserName == userName && userlh.idTruong == idTruong && userlh.NamHoc == namhoc
                                             select lophoc.MaKhoi
                            where makhoiList.Contains(khoi.MaKhoi) && khoi.idTruong == idTruong && khoi.NamHoc == namhoc
                            select khoi).ToList();

            if (listkhoi.Count() == 0)
            {
                listkhoi = (from khoi in db.DM_Khoi
                            where khoi.idTruong == idTruong && khoi.NamHoc == namhoc
                            select khoi).ToList();

            }
            ViewBag.makhoi = new SelectList(listkhoi, "MaKhoi", "TenKhoi");

            return View(@"~/Views/TongHop/ViewThongKeSoLieu.cshtml");

        }

        public ActionResult BaoCaoThongKeSoLieuHocSinh(string NamHoc, string MaKhoi)
        {
            string userName = getUserName();

            int idTruong = Cmon.getIdtruong(userName, db);
            string namhoc = NamHoc;
            ViewBag.CapQuanLy = Cmon.getDonViChuQuan(idTruong);
            ViewBag.TenTruong = Cmon.GetnameSchool(idTruong);
            ViewBag.NamHoc = namhoc;
            DataTable dt = new DataTable();
            HNAFramework.DataAccess.Framework.DBHelper dbHelper = new HNAFramework.DataAccess.Framework.DBHelper();

            SqlParameter[] para = new SqlParameter[3];
            para[0] = new SqlParameter("@IdTruong", idTruong);
            para[1] = new SqlParameter("@NamHoc", namhoc);
            para[2] = new SqlParameter("@MaKhoi", MaKhoi);
            dt = dbHelper.GetDataTabel("spThongKeSoLieuHocSinh", para);
            Session["ThongKeSoLieuHocSinh"] = dt;
            return PartialView(@"~/Views/TongHop/ThongKeSoLieuHocSinh.cshtml", dt);
        }

        public JsonResult ThongKeSoLieuExcel()
        {
            string json = "NoData";
            string userName = getUserName();

            int idTruong = Cmon.getIdtruong(userName, db);
            string CapQuanLy = Cmon.getDonViChuQuan(idTruong);
            string TenTruong = Cmon.GetnameSchool(idTruong);
            string namhoc = Cmon.GetNamHienTai(idTruong);
            string file = "ThongKeSLHocSinh.xls";
            string templateFile = Path.Combine(Server.MapPath("~/templateExcell"), file);
            FileInfo f = new FileInfo(templateFile);
            string newfile = "ThongKeSLHocSinh_" + userName + "_" + DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + ".xls";
            if(Session["ThongKeSoLieuHocSinh"]!=null)
            {
                DataTable dt = (DataTable)Session["ThongKeSoLieuHocSinh"];

                string filename = Path.Combine(Server.MapPath("~/ProcessTemplate"), newfile);
                f.CopyTo(filename);

                Microsoft.Office.Interop.Excel.Application oXL = null;
                Microsoft.Office.Interop.Excel._Workbook oWB = null;
                Microsoft.Office.Interop.Excel._Worksheet oSheet = null;

                oXL = new Microsoft.Office.Interop.Excel.Application();
                oWB = oXL.Workbooks.Open(filename);
                oSheet = String.IsNullOrEmpty("Diem") ? (Microsoft.Office.Interop.Excel._Worksheet)oWB.ActiveSheet : (Microsoft.Office.Interop.Excel._Worksheet)oWB.Worksheets["Diem"];
                oSheet.Cells[1, 1] = CapQuanLy;
                oSheet.Cells[2, 1] = TenTruong;

                oSheet.Cells[4, 1] = "THỐNG KÊ SỐ LIỆU HỌC SINH NĂM HỌC " + namhoc;
                oSheet.Cells[5, 1] = "Năm học: " + namhoc;

                int TongSiSo = 0;
                int TongHocSinhNu = 0;
                int TongDanToc = 0;
                int TongNuDanToc = 0;
                int TongDoanVien = 0;
                int TongHoNgheo = 0;
                int TongCanNgheo = 0;
                int TongKhuyetTat = 0;
                int TongTS6Tuoi = 0;
                int TongNu6Tuoi = 0;
                int TongTS7Tuoi = 0;
                int TongNu7Tuoi = 0;
                int TongTS8Tuoi = 0;
                int TongNu8Tuoi = 0;
                int TongTS9Tuoi = 0;
                int TongNu9Tuoi = 0;
                int TongTS10Tuoi = 0;
                int TongNu10Tuoi = 0;
                int TongTS11Tuoi = 0;
                int TongNu11Tuoi = 0;
                int TongTS12Tuoi = 0;
                int TongNu12Tuoi = 0;
                int TongTS13Tuoi = 0;
                int TongNu13Tuoi = 0;

                int TongKhoiSiSo = 0;
                int TongKhoiHocSinhNu = 0;
                int TongKhoiDanToc = 0;
                int TongKhoiNuDanToc = 0;
                int TongKhoiDoanVien = 0;
                int TongKhoiHoNgheo = 0;
                int TongKhoiCanNgheo = 0;
                int TongKhoiKhuyetTat = 0;
                int TongKhoiTS6Tuoi = 0;
                int TongKhoiNu6Tuoi = 0;
                int TongKhoiTS7Tuoi = 0;
                int TongKhoiNu7Tuoi = 0;
                int TongKhoiTS8Tuoi = 0;
                int TongKhoiNu8Tuoi = 0;
                int TongKhoiTS9Tuoi = 0;
                int TongKhoiNu9Tuoi = 0;
                int TongKhoiTS10Tuoi = 0;
                int TongKhoiNu10Tuoi = 0;
                int TongKhoiTS11Tuoi = 0;
                int TongKhoiNu11Tuoi = 0;
                int TongKhoiTS12Tuoi = 0;
                int TongKhoiNu12Tuoi = 0;
                int TongKhoiTS13Tuoi = 0;
                int TongKhoiNu13Tuoi = 0;

                bool isCreateFoolter = false;
                int row = 8;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    int SiSo = Convert.ToInt32(dt.Rows[i]["SiSo"]);
                    int SoHocSinhNu = Convert.ToInt32(dt.Rows[i]["HocSinhNu"]);
                    int DanToc = Convert.ToInt32(dt.Rows[i]["DanToc"]);
                    int NuDanToc = Convert.ToInt32(dt.Rows[i]["NuDanToc"]);
                    int DoanVien = Convert.ToInt32(dt.Rows[i]["DoanVien"]);
                    int HoNgheo = Convert.ToInt32(dt.Rows[i]["HoNgheo"]);
                    int CanNgheo = Convert.ToInt32(dt.Rows[i]["CanNgheo"]);
                    int KhuyetTat = Convert.ToInt32(dt.Rows[i]["KhuyetTat"]);
                    int TS6Tuoi = Convert.ToInt32(dt.Rows[i]["TS6Tuoi"]);
                    int Nu6Tuoi = Convert.ToInt32(dt.Rows[i]["Nu6Tuoi"]);
                    int TS7Tuoi = Convert.ToInt32(dt.Rows[i]["TS7Tuoi"]);
                    int Nu7Tuoi = Convert.ToInt32(dt.Rows[i]["Nu7Tuoi"]);
                    int TS8Tuoi = Convert.ToInt32(dt.Rows[i]["TS8Tuoi"]);
                    int Nu8Tuoi = Convert.ToInt32(dt.Rows[i]["Nu8Tuoi"]);
                    int TS9Tuoi = Convert.ToInt32(dt.Rows[i]["TS9Tuoi"]);
                    int Nu9Tuoi = Convert.ToInt32(dt.Rows[i]["Nu9Tuoi"]);
                    int TS10Tuoi = Convert.ToInt32(dt.Rows[i]["TS10Tuoi"]);
                    int Nu10Tuoi = Convert.ToInt32(dt.Rows[i]["Nu10Tuoi"]);
                    int TS11Tuoi = Convert.ToInt32(dt.Rows[i]["TS11Tuoi"]);
                    int Nu11Tuoi = Convert.ToInt32(dt.Rows[i]["Nu11Tuoi"]);
                    int TS12Tuoi = Convert.ToInt32(dt.Rows[i]["TS12Tuoi"]);
                    int Nu12Tuoi = Convert.ToInt32(dt.Rows[i]["Nu12Tuoi"]);
                    int TS13Tuoi = Convert.ToInt32(dt.Rows[i]["TS13Tuoi"]);
                    int Nu13Tuoi = Convert.ToInt32(dt.Rows[i]["Nu13Tuoi"]);
                    TongSiSo = TongSiSo + SiSo;
                    TongHocSinhNu = TongHocSinhNu + SoHocSinhNu;
                    TongDanToc = TongDanToc + DanToc;
                    TongNuDanToc = TongNuDanToc + NuDanToc;
                    TongDoanVien = TongDoanVien + DoanVien;
                    TongHoNgheo = TongHoNgheo + HoNgheo;
                    TongCanNgheo = TongCanNgheo + CanNgheo;
                    TongKhuyetTat = TongKhuyetTat + KhuyetTat;
                    TongTS6Tuoi = TongTS6Tuoi + TS6Tuoi;
                    TongNu6Tuoi = TongNu6Tuoi + Nu6Tuoi;
                    TongTS7Tuoi = TongTS7Tuoi + TS7Tuoi;
                    TongNu7Tuoi = TongNu7Tuoi + Nu7Tuoi;
                    TongTS8Tuoi = TongTS8Tuoi + TS8Tuoi;
                    TongNu8Tuoi = TongNu8Tuoi + Nu8Tuoi;
                    TongTS9Tuoi = TongTS9Tuoi + TS9Tuoi;
                    TongNu9Tuoi = TongNu9Tuoi + Nu9Tuoi;
                    TongTS10Tuoi = TongTS10Tuoi + TS10Tuoi;
                    TongNu10Tuoi = TongNu10Tuoi + Nu10Tuoi;
                    TongTS11Tuoi = TongTS11Tuoi + TS11Tuoi;
                    TongNu11Tuoi = TongNu11Tuoi + Nu11Tuoi;
                    TongTS12Tuoi = TongTS12Tuoi + TS12Tuoi;
                    TongNu12Tuoi = TongNu12Tuoi + Nu12Tuoi;
                    TongTS13Tuoi = TongTS13Tuoi + TS13Tuoi;
                    TongNu13Tuoi = TongNu13Tuoi + Nu13Tuoi;

                    TongKhoiSiSo = TongKhoiSiSo + SiSo;
                    TongKhoiHocSinhNu = TongKhoiHocSinhNu + SoHocSinhNu;
                    TongKhoiDanToc = TongKhoiDanToc + DanToc;
                    TongKhoiNuDanToc = TongKhoiNuDanToc + NuDanToc;
                    TongKhoiDoanVien = TongKhoiDoanVien + DoanVien;
                    TongKhoiHoNgheo = TongKhoiHoNgheo + HoNgheo;
                    TongKhoiCanNgheo = TongKhoiCanNgheo + CanNgheo;
                    TongKhoiKhuyetTat = TongKhoiKhuyetTat + KhuyetTat;
                    TongKhoiTS6Tuoi = TongKhoiTS6Tuoi + TS6Tuoi;
                    TongKhoiNu6Tuoi = TongKhoiNu6Tuoi + Nu6Tuoi;
                    TongKhoiTS7Tuoi = TongKhoiTS7Tuoi + TS7Tuoi;
                    TongKhoiNu7Tuoi = TongKhoiNu7Tuoi + Nu7Tuoi;
                    TongKhoiTS8Tuoi = TongKhoiTS8Tuoi + TS8Tuoi;
                    TongKhoiNu8Tuoi = TongKhoiNu8Tuoi + Nu8Tuoi;
                    TongKhoiTS9Tuoi = TongKhoiTS9Tuoi + TS9Tuoi;
                    TongKhoiNu9Tuoi = TongKhoiNu9Tuoi + Nu9Tuoi;
                    TongKhoiTS10Tuoi = TongKhoiTS10Tuoi + TS10Tuoi;
                    TongKhoiNu10Tuoi = TongKhoiNu10Tuoi + Nu10Tuoi;
                    TongKhoiTS11Tuoi = TongKhoiTS11Tuoi + TS11Tuoi;
                    TongKhoiNu11Tuoi = TongKhoiNu11Tuoi + Nu11Tuoi;
                    TongKhoiTS12Tuoi = TongKhoiTS12Tuoi + TS12Tuoi;
                    TongKhoiNu12Tuoi = TongKhoiNu12Tuoi + Nu12Tuoi;
                    TongKhoiTS13Tuoi = TongKhoiTS13Tuoi + TS13Tuoi;
                    TongKhoiNu13Tuoi = TongKhoiNu13Tuoi + Nu13Tuoi;

                    int c = i + 1;
                    string KhoiCurrent = dt.Rows[i]["MaKhoi"].ToString();
                    string KhoiNext = "";
                    if (c == dt.Rows.Count)
                    {
                        KhoiNext = dt.Rows[0]["MaKhoi"].ToString();
                    }
                    else
                    {
                        KhoiNext = dt.Rows[c]["MaKhoi"].ToString();
                    }
                    //row = 8;
                    row = row +1;
                    oSheet.Cells[row, 1] = (i+1).ToString();
                    SetBorder(oSheet, row, 1, false, false);

                    oSheet.Cells[row, 2] = dt.Rows[i]["TenLop"];
                    SetBorder(oSheet, row, 2, false, false);

                    oSheet.Cells[row, 3] = dt.Rows[i]["GVCN"];
                    SetBorder(oSheet, row, 3, false, false);

                    oSheet.Cells[row, 4] = (SiSo==0?"": SiSo.ToString());
                    SetBorder(oSheet, row, 4, false, false);

                    oSheet.Cells[row, 5] = (SoHocSinhNu==0?"":SoHocSinhNu.ToString());
                    SetBorder(oSheet, row, 5, false, false);

                    oSheet.Cells[row, 6] = (DanToc==0?"": DanToc.ToString());
                    SetBorder(oSheet, row, 6, false, false);

                    oSheet.Cells[row, 7] = (NuDanToc==0?"":NuDanToc.ToString());
                    SetBorder(oSheet, row, 7, false, false);

                    oSheet.Cells[row, 8] = (DoanVien==0?"":DoanVien.ToString());
                    SetBorder(oSheet, row, 8, false, false);

                    oSheet.Cells[row, 9] = (HoNgheo==0?"":HoNgheo.ToString());
                    SetBorder(oSheet, row, 9, false, false);

                    oSheet.Cells[row, 10] = (CanNgheo==0?"":CanNgheo.ToString());
                    SetBorder(oSheet, row, 10, false, false);

                    oSheet.Cells[row, 11] = (KhuyetTat==0?"":KhuyetTat.ToString());
                    SetBorder(oSheet, row, 11, false, false);

                    oSheet.Cells[row, 12] = (TS6Tuoi==0?"":TS6Tuoi.ToString());
                    SetBorder(oSheet, row, 12, false, false);

                    oSheet.Cells[row, 13] = (Nu6Tuoi==0?"":Nu6Tuoi.ToString());
                    SetBorder(oSheet, row, 13, false, false);

                    oSheet.Cells[row, 14] = (TS7Tuoi == 0 ? "" : TS7Tuoi.ToString());
                    SetBorder(oSheet, row, 14, false, false);

                    oSheet.Cells[row, 15] = (Nu7Tuoi == 0 ? "" : Nu7Tuoi.ToString());
                    SetBorder(oSheet, row, 15, false, false);

                    oSheet.Cells[row, 16] = (TS8Tuoi == 0 ? "" : TS8Tuoi.ToString());
                    SetBorder(oSheet, row, 16, false, false);

                    oSheet.Cells[row, 17] = (Nu8Tuoi == 0 ? "" : Nu8Tuoi.ToString());
                    SetBorder(oSheet, row, 17, false, false);

                    oSheet.Cells[row, 18] = (TS9Tuoi == 0 ? "" : TS9Tuoi.ToString());
                    SetBorder(oSheet, row, 18, false, false);

                    oSheet.Cells[row, 19] = (Nu9Tuoi == 0 ? "" : Nu9Tuoi.ToString());
                    SetBorder(oSheet, row, 19, false, false);

                    oSheet.Cells[row, 20] = (TS10Tuoi == 0 ? "" : TS10Tuoi.ToString());
                    SetBorder(oSheet, row, 20, false, false);

                    oSheet.Cells[row, 21] = (Nu10Tuoi == 0 ? "" : Nu10Tuoi.ToString());
                    SetBorder(oSheet, row, 21, false, false);

                    oSheet.Cells[row, 22] = (TS11Tuoi == 0 ? "" : TS11Tuoi.ToString());
                    SetBorder(oSheet, row, 22, false, false);

                    oSheet.Cells[row, 23] = (Nu11Tuoi == 0 ? "" : Nu11Tuoi.ToString());
                    SetBorder(oSheet, row, 23, false, false);

                    oSheet.Cells[row, 24] = (TS12Tuoi == 0 ? "" : TS12Tuoi.ToString());
                    SetBorder(oSheet, row, 24, false, false);

                    oSheet.Cells[row, 25] = (Nu12Tuoi == 0 ? "" : Nu12Tuoi.ToString());
                    SetBorder(oSheet, row, 25, false, false);

                    oSheet.Cells[row, 26] = (TS13Tuoi == 0 ? "" : TS13Tuoi.ToString());
                    SetBorder(oSheet, row, 26, false, false);

                    oSheet.Cells[row, 27] = (Nu13Tuoi == 0 ? "" : Nu13Tuoi.ToString());
                    SetBorder(oSheet, row, 27, true, false);
                    if (KhoiNext != KhoiCurrent)
                    {
                        row= row+1;
                        isCreateFoolter = true;
                        oSheet.Range[oSheet.Cells[row, 1], oSheet.Cells[row, 3]].Merge();
                        oSheet.Cells[row, 1] = dt.Rows[i]["TenKhoi"];
                        oSheet.Cells[row, 1].Font.Bold = true;
                        SetBorderTong(oSheet, row, 1, false, true);
                        SetBorderTong(oSheet, row, 2, false, true);
                        SetBorderTong(oSheet, row, 3, false, true);

                        oSheet.Cells[row, 4] = (TongKhoiSiSo == 0 ? "" : TongKhoiSiSo.ToString());
                        oSheet.Cells[row, 4].Font.Bold = true;
                        SetBorderTong(oSheet, row, 4, false, true);
                        SetBorderTong(oSheet, row, 4, false, true);
                        SetBorderTong(oSheet, row, 4, false, true);

                        oSheet.Cells[row, 5] = (TongKhoiHocSinhNu == 0 ? "" : TongKhoiHocSinhNu.ToString());
                        oSheet.Cells[row, 5].Font.Bold = true;
                        SetBorderTong(oSheet, row, 5, false, true);
                        SetBorderTong(oSheet, row, 5, false, true);
                        SetBorderTong(oSheet, row, 5, false, true);

                        oSheet.Cells[row, 6] = (TongKhoiDanToc == 0 ? "" : TongKhoiDanToc.ToString());
                        oSheet.Cells[row, 6].Font.Bold = true;
                        SetBorderTong(oSheet, row, 6, false, true);
                        SetBorderTong(oSheet, row, 6, false, true);
                        SetBorderTong(oSheet, row, 6, false, true);

                        oSheet.Cells[row, 7] = (TongKhoiNuDanToc == 0 ? "" : TongKhoiNuDanToc.ToString());
                        oSheet.Cells[row, 7].Font.Bold = true;
                        SetBorderTong(oSheet, row, 7, false, true);
                        SetBorderTong(oSheet, row, 7, false, true);
                        SetBorderTong(oSheet, row, 7, false, true);

                        oSheet.Cells[row, 8] = (TongKhoiDoanVien == 0 ? "" : TongKhoiDoanVien.ToString());
                        oSheet.Cells[row, 8].Font.Bold = true;
                        SetBorderTong(oSheet, row, 8, false, true);
                        SetBorderTong(oSheet, row, 8, false, true);
                        SetBorderTong(oSheet, row, 8, false, true);

                        oSheet.Cells[row, 9] = (TongKhoiHoNgheo == 0 ? "" : TongKhoiHoNgheo.ToString());
                        oSheet.Cells[row, 9].Font.Bold = true;
                        SetBorderTong(oSheet, row, 9, false, true);
                        SetBorderTong(oSheet, row, 9, false, true);
                        SetBorderTong(oSheet, row, 9, false, true);

                        oSheet.Cells[row, 10] = (TongKhoiCanNgheo == 0 ? "" : TongKhoiCanNgheo.ToString());
                        oSheet.Cells[row, 10].Font.Bold = true;
                        SetBorderTong(oSheet, row, 10, false, true);
                        SetBorderTong(oSheet, row, 10, false, true);
                        SetBorderTong(oSheet, row, 10, false, true);

                        oSheet.Cells[row, 11] = (TongKhoiKhuyetTat == 0 ? "" : TongKhoiKhuyetTat.ToString());
                        oSheet.Cells[row, 11].Font.Bold = true;
                        SetBorderTong(oSheet, row, 11, false, true);
                        SetBorderTong(oSheet, row, 11, false, true);
                        SetBorderTong(oSheet, row, 11, false, true);

                        oSheet.Cells[row, 12] = (TongKhoiTS6Tuoi == 0 ? "" : TongKhoiTS6Tuoi.ToString());
                        oSheet.Cells[row, 12].Font.Bold = true;
                        SetBorderTong(oSheet, row, 12, false, true);
                        SetBorderTong(oSheet, row, 12, false, true);
                        SetBorderTong(oSheet, row, 12, false, true);

                        oSheet.Cells[row, 13] = (TongKhoiNu6Tuoi == 0 ? "" : TongKhoiNu6Tuoi.ToString());
                        oSheet.Cells[row, 13].Font.Bold = true;
                        SetBorderTong(oSheet, row, 13, false, true);
                        SetBorderTong(oSheet, row, 13, false, true);
                        SetBorderTong(oSheet, row, 13, false, true);

                        oSheet.Cells[row, 14] = (TongKhoiTS7Tuoi == 0 ? "" : TongKhoiTS7Tuoi.ToString());
                        oSheet.Cells[row, 14].Font.Bold = true;
                        SetBorderTong(oSheet, row, 14, false, true);
                        SetBorderTong(oSheet, row, 14, false, true);
                        SetBorderTong(oSheet, row, 14, false, true);

                        oSheet.Cells[row, 15] = (TongKhoiNu7Tuoi == 0 ? "" : TongKhoiNu7Tuoi.ToString());
                        oSheet.Cells[row, 15].Font.Bold = true;
                        SetBorderTong(oSheet, row, 15, false, true);
                        SetBorderTong(oSheet, row, 15, false, true);
                        SetBorderTong(oSheet, row, 15, false, true);

                        oSheet.Cells[row, 16] = (TongKhoiTS8Tuoi == 0 ? "" : TongKhoiTS8Tuoi.ToString());
                        oSheet.Cells[row, 16].Font.Bold = true;
                        SetBorderTong(oSheet, row, 16, false, true);
                        SetBorderTong(oSheet, row, 16, false, true);
                        SetBorderTong(oSheet, row, 16, false, true);

                        oSheet.Cells[row, 17] = (TongKhoiNu8Tuoi == 0 ? "" : TongKhoiNu8Tuoi.ToString());
                        oSheet.Cells[row, 17].Font.Bold = true;
                        SetBorderTong(oSheet, row, 17, false, true);
                        SetBorderTong(oSheet, row, 17, false, true);
                        SetBorderTong(oSheet, row, 17, false, true);

                        oSheet.Cells[row, 18] = (TongKhoiTS9Tuoi == 0 ? "" : TongKhoiTS9Tuoi.ToString());
                        oSheet.Cells[row, 18].Font.Bold = true;
                        SetBorderTong(oSheet, row, 18, false, true);
                        SetBorderTong(oSheet, row, 18, false, true);
                        SetBorderTong(oSheet, row, 18, false, true);

                        oSheet.Cells[row, 19] = (TongKhoiNu9Tuoi == 0 ? "" : TongKhoiNu9Tuoi.ToString());
                        oSheet.Cells[row, 19].Font.Bold = true;
                        SetBorderTong(oSheet, row, 19, false, true);
                        SetBorderTong(oSheet, row, 19, false, true);
                        SetBorderTong(oSheet, row, 19, false, true);

                        oSheet.Cells[row, 20] = (TongKhoiTS10Tuoi == 0 ? "" : TongKhoiTS10Tuoi.ToString());
                        oSheet.Cells[row, 20].Font.Bold = true;
                        SetBorderTong(oSheet, row, 20, false, true);
                        SetBorderTong(oSheet, row, 20, false, true);
                        SetBorderTong(oSheet, row, 20, false, true);

                        oSheet.Cells[row, 21] = (TongKhoiNu10Tuoi == 0 ? "" : TongKhoiNu10Tuoi.ToString());
                        oSheet.Cells[row, 21].Font.Bold = true;
                        SetBorderTong(oSheet, row, 21, false, true);
                        SetBorderTong(oSheet, row, 21, false, true);
                        SetBorderTong(oSheet, row, 21, false, true);

                        oSheet.Cells[row, 22] = (TongKhoiTS11Tuoi == 0 ? "" : TongKhoiTS11Tuoi.ToString());
                        oSheet.Cells[row, 22].Font.Bold = true;
                        SetBorderTong(oSheet, row, 22, false, true);
                        SetBorderTong(oSheet, row, 22, false, true);
                        SetBorderTong(oSheet, row, 22, false, true);

                        oSheet.Cells[row, 23] = (TongKhoiNu11Tuoi == 0 ? "" : TongKhoiNu11Tuoi.ToString());
                        oSheet.Cells[row, 23].Font.Bold = true;
                        SetBorderTong(oSheet, row, 23, false, true);
                        SetBorderTong(oSheet, row, 23, false, true);
                        SetBorderTong(oSheet, row, 23, false, true);

                        oSheet.Cells[row, 24] = (TongKhoiTS12Tuoi == 0 ? "" : TongKhoiTS12Tuoi.ToString());
                        oSheet.Cells[row, 24].Font.Bold = true;
                        SetBorderTong(oSheet, row, 24, false, true);
                        SetBorderTong(oSheet, row, 24, false, true);
                        SetBorderTong(oSheet, row, 24, false, true);

                        oSheet.Cells[row, 25] = (TongKhoiNu12Tuoi == 0 ? "" : TongKhoiNu12Tuoi.ToString());
                        oSheet.Cells[row, 25].Font.Bold = true;
                        SetBorderTong(oSheet, row, 25, false, true);
                        SetBorderTong(oSheet, row, 25, false, true);
                        SetBorderTong(oSheet, row, 25, false, true);

                        oSheet.Cells[row, 26] = (TongKhoiTS13Tuoi == 0 ? "" : TongKhoiTS13Tuoi.ToString());
                        oSheet.Cells[row, 26].Font.Bold = true;
                        SetBorderTong(oSheet, row, 26, false, true);
                        SetBorderTong(oSheet, row, 26, false, true);
                        SetBorderTong(oSheet, row, 26, false, true);

                        oSheet.Cells[row, 27] = (TongKhoiNu13Tuoi == 0 ? "" : TongKhoiNu13Tuoi.ToString());
                        oSheet.Cells[row, 27].Font.Bold = true;
                        SetBorderTong(oSheet, row, 27, false, true);
                        SetBorderTong(oSheet, row, 27, false, true);
                        SetBorderTong(oSheet, row, 27, true, true);

                        TongKhoiSiSo = 0;
                        TongKhoiHocSinhNu = 0;
                        TongKhoiDanToc = 0;
                        TongKhoiNuDanToc = 0;
                        TongKhoiDoanVien = 0;
                        TongKhoiHoNgheo = 0;
                        TongKhoiCanNgheo = 0;
                        TongKhoiKhuyetTat = 0;
                        TongKhoiTS6Tuoi = 0;
                        TongKhoiNu6Tuoi = 0;
                        TongKhoiTS7Tuoi = 0;
                        TongKhoiNu7Tuoi = 0;
                        TongKhoiTS8Tuoi = 0;
                        TongKhoiNu8Tuoi = 0;
                        TongKhoiTS9Tuoi = 0;
                        TongKhoiNu9Tuoi = 0;
                        TongKhoiTS10Tuoi = 0;
                        TongKhoiNu10Tuoi = 0;
                        TongKhoiTS11Tuoi = 0;
                        TongKhoiNu11Tuoi = 0;
                        TongKhoiTS12Tuoi = 0;
                        TongKhoiNu12Tuoi = 0;
                        TongKhoiTS13Tuoi = 0;
                        TongKhoiNu13Tuoi = 0;
                    }
                }
                #region Tổng khối với việc chọn 1 khối
                if (!isCreateFoolter)
                {
                    row = row + 1;
                    isCreateFoolter = true;
                    oSheet.Range[oSheet.Cells[row, 1], oSheet.Cells[row, 3]].Merge();
                    oSheet.Cells[row, 1] = dt.Rows[0]["TenKhoi"];
                    oSheet.Cells[row, 1].Font.Bold = true;
                    SetBorderTong(oSheet, row, 1, false, true);
                    SetBorderTong(oSheet, row, 2, false, true);
                    SetBorderTong(oSheet, row, 3, false, true);

                    oSheet.Cells[row, 4] = (TongKhoiSiSo == 0 ? "" : TongKhoiSiSo.ToString());
                    oSheet.Cells[row, 4].Font.Bold = true;
                    SetBorderTong(oSheet, row, 4, false, true);
                    SetBorderTong(oSheet, row, 4, false, true);
                    SetBorderTong(oSheet, row, 4, false, true);

                    oSheet.Cells[row, 5] = (TongKhoiHocSinhNu == 0 ? "" : TongKhoiHocSinhNu.ToString());
                    oSheet.Cells[row, 5].Font.Bold = true;
                    SetBorderTong(oSheet, row, 5, false, true);
                    SetBorderTong(oSheet, row, 5, false, true);
                    SetBorderTong(oSheet, row, 5, false, true);

                    oSheet.Cells[row, 6] = (TongKhoiDanToc == 0 ? "" : TongKhoiDanToc.ToString());
                    oSheet.Cells[row, 6].Font.Bold = true;
                    SetBorderTong(oSheet, row, 6, false, true);
                    SetBorderTong(oSheet, row, 6, false, true);
                    SetBorderTong(oSheet, row, 6, false, true);

                    oSheet.Cells[row, 7] = (TongKhoiNuDanToc == 0 ? "" : TongKhoiNuDanToc.ToString());
                    oSheet.Cells[row, 7].Font.Bold = true;
                    SetBorderTong(oSheet, row, 7, false, true);
                    SetBorderTong(oSheet, row, 7, false, true);
                    SetBorderTong(oSheet, row, 7, false, true);

                    oSheet.Cells[row, 8] = (TongKhoiDoanVien == 0 ? "" : TongKhoiDoanVien.ToString());
                    oSheet.Cells[row, 8].Font.Bold = true;
                    SetBorderTong(oSheet, row, 8, false, true);
                    SetBorderTong(oSheet, row, 8, false, true);
                    SetBorderTong(oSheet, row, 8, false, true);

                    oSheet.Cells[row, 9] = (TongKhoiHoNgheo == 0 ? "" : TongKhoiHoNgheo.ToString());
                    oSheet.Cells[row, 9].Font.Bold = true;
                    SetBorderTong(oSheet, row, 9, false, true);
                    SetBorderTong(oSheet, row, 9, false, true);
                    SetBorderTong(oSheet, row, 9, false, true);

                    oSheet.Cells[row, 10] = (TongKhoiCanNgheo == 0 ? "" : TongKhoiCanNgheo.ToString());
                    oSheet.Cells[row, 10].Font.Bold = true;
                    SetBorderTong(oSheet, row, 10, false, true);
                    SetBorderTong(oSheet, row, 10, false, true);
                    SetBorderTong(oSheet, row, 10, false, true);

                    oSheet.Cells[row, 11] = (TongKhoiKhuyetTat == 0 ? "" : TongKhoiKhuyetTat.ToString());
                    oSheet.Cells[row, 11].Font.Bold = true;
                    SetBorderTong(oSheet, row, 11, false, true);
                    SetBorderTong(oSheet, row, 11, false, true);
                    SetBorderTong(oSheet, row, 11, false, true);

                    oSheet.Cells[row, 12] = (TongKhoiTS6Tuoi == 0 ? "" : TongKhoiTS6Tuoi.ToString());
                    oSheet.Cells[row, 12].Font.Bold = true;
                    SetBorderTong(oSheet, row, 12, false, true);
                    SetBorderTong(oSheet, row, 12, false, true);
                    SetBorderTong(oSheet, row, 12, false, true);

                    oSheet.Cells[row, 13] = (TongKhoiNu6Tuoi == 0 ? "" : TongKhoiNu6Tuoi.ToString());
                    oSheet.Cells[row, 13].Font.Bold = true;
                    SetBorderTong(oSheet, row, 13, false, true);
                    SetBorderTong(oSheet, row, 13, false, true);
                    SetBorderTong(oSheet, row, 13, false, true);

                    oSheet.Cells[row, 14] = (TongKhoiTS7Tuoi == 0 ? "" : TongKhoiTS7Tuoi.ToString());
                    oSheet.Cells[row, 14].Font.Bold = true;
                    SetBorderTong(oSheet, row, 14, false, true);
                    SetBorderTong(oSheet, row, 14, false, true);
                    SetBorderTong(oSheet, row, 14, false, true);

                    oSheet.Cells[row, 15] = (TongKhoiNu7Tuoi == 0 ? "" : TongKhoiNu7Tuoi.ToString());
                    oSheet.Cells[row, 15].Font.Bold = true;
                    SetBorderTong(oSheet, row, 15, false, true);
                    SetBorderTong(oSheet, row, 15, false, true);
                    SetBorderTong(oSheet, row, 15, false, true);

                    oSheet.Cells[row, 16] = (TongKhoiTS8Tuoi == 0 ? "" : TongKhoiTS8Tuoi.ToString());
                    oSheet.Cells[row, 16].Font.Bold = true;
                    SetBorderTong(oSheet, row, 16, false, true);
                    SetBorderTong(oSheet, row, 16, false, true);
                    SetBorderTong(oSheet, row, 16, false, true);

                    oSheet.Cells[row, 17] = (TongKhoiNu8Tuoi == 0 ? "" : TongKhoiNu8Tuoi.ToString());
                    oSheet.Cells[row, 17].Font.Bold = true;
                    SetBorderTong(oSheet, row, 17, false, true);
                    SetBorderTong(oSheet, row, 17, false, true);
                    SetBorderTong(oSheet, row, 17, false, true);

                    oSheet.Cells[row, 18] = (TongKhoiTS9Tuoi == 0 ? "" : TongKhoiTS9Tuoi.ToString());
                    oSheet.Cells[row, 18].Font.Bold = true;
                    SetBorderTong(oSheet, row, 18, false, true);
                    SetBorderTong(oSheet, row, 18, false, true);
                    SetBorderTong(oSheet, row, 18, false, true);

                    oSheet.Cells[row, 19] = (TongKhoiNu9Tuoi == 0 ? "" : TongKhoiNu9Tuoi.ToString());
                    oSheet.Cells[row, 19].Font.Bold = true;
                    SetBorderTong(oSheet, row, 19, false, true);
                    SetBorderTong(oSheet, row, 19, false, true);
                    SetBorderTong(oSheet, row, 19, false, true);

                    oSheet.Cells[row, 20] = (TongKhoiTS10Tuoi == 0 ? "" : TongKhoiTS10Tuoi.ToString());
                    oSheet.Cells[row, 20].Font.Bold = true;
                    SetBorderTong(oSheet, row, 20, false, true);
                    SetBorderTong(oSheet, row, 20, false, true);
                    SetBorderTong(oSheet, row, 20, false, true);

                    oSheet.Cells[row, 21] = (TongKhoiNu10Tuoi == 0 ? "" : TongKhoiNu10Tuoi.ToString());
                    oSheet.Cells[row, 21].Font.Bold = true;
                    SetBorderTong(oSheet, row, 21, false, true);
                    SetBorderTong(oSheet, row, 21, false, true);
                    SetBorderTong(oSheet, row, 21, false, true);

                    oSheet.Cells[row, 22] = (TongKhoiTS11Tuoi == 0 ? "" : TongKhoiTS11Tuoi.ToString());
                    oSheet.Cells[row, 22].Font.Bold = true;
                    SetBorderTong(oSheet, row, 22, false, true);
                    SetBorderTong(oSheet, row, 22, false, true);
                    SetBorderTong(oSheet, row, 22, false, true);

                    oSheet.Cells[row, 23] = (TongKhoiNu11Tuoi == 0 ? "" : TongKhoiNu11Tuoi.ToString());
                    oSheet.Cells[row, 23].Font.Bold = true;
                    SetBorderTong(oSheet, row, 23, false, true);
                    SetBorderTong(oSheet, row, 23, false, true);
                    SetBorderTong(oSheet, row, 23, false, true);

                    oSheet.Cells[row, 24] = (TongKhoiTS12Tuoi == 0 ? "" : TongKhoiTS12Tuoi.ToString());
                    oSheet.Cells[row, 24].Font.Bold = true;
                    SetBorderTong(oSheet, row, 24, false, true);
                    SetBorderTong(oSheet, row, 24, false, true);
                    SetBorderTong(oSheet, row, 24, false, true);

                    oSheet.Cells[row, 25] = (TongKhoiNu12Tuoi == 0 ? "" : TongKhoiNu12Tuoi.ToString());
                    oSheet.Cells[row, 25].Font.Bold = true;
                    SetBorderTong(oSheet, row, 25, false, true);
                    SetBorderTong(oSheet, row, 25, false, true);
                    SetBorderTong(oSheet, row, 25, false, true);

                    oSheet.Cells[row, 26] = (TongKhoiTS13Tuoi == 0 ? "" : TongKhoiTS13Tuoi.ToString());
                    oSheet.Cells[row, 26].Font.Bold = true;
                    SetBorderTong(oSheet, row, 26, false, true);
                    SetBorderTong(oSheet, row, 26, false, true);
                    SetBorderTong(oSheet, row, 26, false, true);

                    oSheet.Cells[row, 27] = (TongKhoiNu13Tuoi == 0 ? "" : TongKhoiNu13Tuoi.ToString());
                    oSheet.Cells[row, 27].Font.Bold = true;
                    SetBorderTong(oSheet, row, 27, false, true);
                    SetBorderTong(oSheet, row, 27, false, true);
                    SetBorderTong(oSheet, row, 27, true, true);
                }
                #endregion
                #region Tổng cộng
                row = row + 1;
                isCreateFoolter = true;
                oSheet.Range[oSheet.Cells[row, 1], oSheet.Cells[row, 3]].Merge();
                oSheet.Cells[row, 1] = "TỔNG CỘNG";
                oSheet.Cells[row, 1].Font.Bold = true;
                SetBorderTong(oSheet, row, 1, false, true);
                SetBorderTong(oSheet, row, 2, false, true);
                SetBorderTong(oSheet, row, 3, false, true);

                oSheet.Cells[row, 4] = (TongSiSo == 0 ? "" : TongSiSo.ToString());
                oSheet.Cells[row, 4].Font.Bold = true;
                SetBorderTong(oSheet, row, 4, false, true);
                SetBorderTong(oSheet, row, 4, false, true);
                SetBorderTong(oSheet, row, 4, false, true);

                oSheet.Cells[row, 5] = (TongHocSinhNu == 0 ? "" : TongHocSinhNu.ToString());
                oSheet.Cells[row, 5].Font.Bold = true;
                SetBorderTong(oSheet, row, 5, false, true);
                SetBorderTong(oSheet, row, 5, false, true);
                SetBorderTong(oSheet, row, 5, false, true);

                oSheet.Cells[row, 6] = (TongDanToc == 0 ? "" : TongDanToc.ToString());
                oSheet.Cells[row, 6].Font.Bold = true;
                SetBorderTong(oSheet, row, 6, false, true);
                SetBorderTong(oSheet, row, 6, false, true);
                SetBorderTong(oSheet, row, 6, false, true);

                oSheet.Cells[row, 7] = (TongNuDanToc == 0 ? "" : TongNuDanToc.ToString());
                oSheet.Cells[row, 7].Font.Bold = true;
                SetBorderTong(oSheet, row, 7, false, true);
                SetBorderTong(oSheet, row, 7, false, true);
                SetBorderTong(oSheet, row, 7, false, true);

                oSheet.Cells[row, 8] = (TongDoanVien == 0 ? "" : TongDoanVien.ToString());
                oSheet.Cells[row, 8].Font.Bold = true;
                SetBorderTong(oSheet, row, 8, false, true);
                SetBorderTong(oSheet, row, 8, false, true);
                SetBorderTong(oSheet, row, 8, false, true);

                oSheet.Cells[row, 9] = (TongHoNgheo == 0 ? "" : TongHoNgheo.ToString());
                oSheet.Cells[row, 9].Font.Bold = true;
                SetBorderTong(oSheet, row, 9, false, true);
                SetBorderTong(oSheet, row, 9, false, true);
                SetBorderTong(oSheet, row, 9, false, true);

                oSheet.Cells[row, 10] = (TongCanNgheo == 0 ? "" : TongCanNgheo.ToString());
                oSheet.Cells[row, 10].Font.Bold = true;
                SetBorderTong(oSheet, row, 10, false, true);
                SetBorderTong(oSheet, row, 10, false, true);
                SetBorderTong(oSheet, row, 10, false, true);

                oSheet.Cells[row, 11] = (TongKhuyetTat == 0 ? "" : TongKhuyetTat.ToString());
                oSheet.Cells[row, 11].Font.Bold = true;
                SetBorderTong(oSheet, row, 11, false, true);
                SetBorderTong(oSheet, row, 11, false, true);
                SetBorderTong(oSheet, row, 11, false, true);

                oSheet.Cells[row, 12] = (TongTS6Tuoi == 0 ? "" : TongTS6Tuoi.ToString());
                oSheet.Cells[row, 12].Font.Bold = true;
                SetBorderTong(oSheet, row, 12, false, true);
                SetBorderTong(oSheet, row, 12, false, true);
                SetBorderTong(oSheet, row, 12, false, true);

                oSheet.Cells[row, 13] = (TongNu6Tuoi == 0 ? "" : TongNu6Tuoi.ToString());
                oSheet.Cells[row, 13].Font.Bold = true;
                SetBorderTong(oSheet, row, 13, false, true);
                SetBorderTong(oSheet, row, 13, false, true);
                SetBorderTong(oSheet, row, 13, false, true);

                oSheet.Cells[row, 14] = (TongTS7Tuoi == 0 ? "" : TongTS7Tuoi.ToString());
                oSheet.Cells[row, 14].Font.Bold = true;
                SetBorderTong(oSheet, row, 14, false, true);
                SetBorderTong(oSheet, row, 14, false, true);
                SetBorderTong(oSheet, row, 14, false, true);

                oSheet.Cells[row, 15] = (TongNu7Tuoi == 0 ? "" : TongNu7Tuoi.ToString());
                oSheet.Cells[row, 15].Font.Bold = true;
                SetBorderTong(oSheet, row, 15, false, true);
                SetBorderTong(oSheet, row, 15, false, true);
                SetBorderTong(oSheet, row, 15, false, true);

                oSheet.Cells[row, 16] = (TongTS8Tuoi == 0 ? "" : TongTS8Tuoi.ToString());
                oSheet.Cells[row, 16].Font.Bold = true;
                SetBorderTong(oSheet, row, 16, false, true);
                SetBorderTong(oSheet, row, 16, false, true);
                SetBorderTong(oSheet, row, 16, false, true);

                oSheet.Cells[row, 17] = (TongNu8Tuoi == 0 ? "" : TongNu8Tuoi.ToString());
                oSheet.Cells[row, 17].Font.Bold = true;
                SetBorderTong(oSheet, row, 17, false, true);
                SetBorderTong(oSheet, row, 17, false, true);
                SetBorderTong(oSheet, row, 17, false, true);

                oSheet.Cells[row, 18] = (TongTS9Tuoi == 0 ? "" : TongTS9Tuoi.ToString());
                oSheet.Cells[row, 18].Font.Bold = true;
                SetBorderTong(oSheet, row, 18, false, true);
                SetBorderTong(oSheet, row, 18, false, true);
                SetBorderTong(oSheet, row, 18, false, true);

                oSheet.Cells[row, 19] = (TongNu9Tuoi == 0 ? "" : TongNu9Tuoi.ToString());
                oSheet.Cells[row, 19].Font.Bold = true;
                SetBorderTong(oSheet, row, 19, false, true);
                SetBorderTong(oSheet, row, 19, false, true);
                SetBorderTong(oSheet, row, 19, false, true);

                oSheet.Cells[row, 20] = (TongTS10Tuoi == 0 ? "" : TongTS10Tuoi.ToString());
                oSheet.Cells[row, 20].Font.Bold = true;
                SetBorderTong(oSheet, row, 20, false, true);
                SetBorderTong(oSheet, row, 20, false, true);
                SetBorderTong(oSheet, row, 20, false, true);

                oSheet.Cells[row, 21] = (TongNu10Tuoi == 0 ? "" : TongNu10Tuoi.ToString());
                oSheet.Cells[row, 21].Font.Bold = true;
                SetBorderTong(oSheet, row, 21, false, true);
                SetBorderTong(oSheet, row, 21, false, true);
                SetBorderTong(oSheet, row, 21, false, true);

                oSheet.Cells[row, 22] = (TongTS11Tuoi == 0 ? "" : TongTS11Tuoi.ToString());
                oSheet.Cells[row, 22].Font.Bold = true;
                SetBorderTong(oSheet, row, 22, false, true);
                SetBorderTong(oSheet, row, 22, false, true);
                SetBorderTong(oSheet, row, 22, false, true);

                oSheet.Cells[row, 23] = (TongNu11Tuoi == 0 ? "" : TongNu11Tuoi.ToString());
                oSheet.Cells[row, 23].Font.Bold = true;
                SetBorderTong(oSheet, row, 23, false, true);
                SetBorderTong(oSheet, row, 23, false, true);
                SetBorderTong(oSheet, row, 23, false, true);

                oSheet.Cells[row, 24] = (TongTS12Tuoi == 0 ? "" : TongTS12Tuoi.ToString());
                oSheet.Cells[row, 24].Font.Bold = true;
                SetBorderTong(oSheet, row, 24, false, true);
                SetBorderTong(oSheet, row, 24, false, true);
                SetBorderTong(oSheet, row, 24, false, true);

                oSheet.Cells[row, 25] = (TongNu12Tuoi == 0 ? "" : TongNu12Tuoi.ToString());
                oSheet.Cells[row, 25].Font.Bold = true;
                SetBorderTong(oSheet, row, 25, false, true);
                SetBorderTong(oSheet, row, 25, false, true);
                SetBorderTong(oSheet, row, 25, false, true);

                oSheet.Cells[row, 26] = (TongTS13Tuoi == 0 ? "" : TongTS13Tuoi.ToString());
                oSheet.Cells[row, 26].Font.Bold = true;
                SetBorderTong(oSheet, row, 26, false, true);
                SetBorderTong(oSheet, row, 26, false, true);
                SetBorderTong(oSheet, row, 26, false, true);

                oSheet.Cells[row, 27] = (TongNu13Tuoi == 0 ? "" : TongNu13Tuoi.ToString());
                oSheet.Cells[row, 27].Font.Bold = true;
                SetBorderTong(oSheet, row, 27, false, true);
                SetBorderTong(oSheet, row, 27, false, true);
                SetBorderTong(oSheet, row, 27, true, true);
                #endregion
                oWB.Save();
                oWB.Close();

                json = newfile;
            }
            

            return Json(json, JsonRequestBehavior.AllowGet);
        }
        private void SetBorderTong(Microsoft.Office.Interop.Excel._Worksheet worksheet, int row, int col, bool ColEnd, bool RowEnd)
        {
            worksheet.Cells[row, col].Borders[Excel.XlBordersIndex.xlEdgeLeft].Weight = 2d;
            if (ColEnd)
                worksheet.Cells[row, col].Borders[Excel.XlBordersIndex.xlEdgeRight].Weight = 2d;

            worksheet.Cells[row, col].Borders[Excel.XlBordersIndex.xlEdgeBottom].Weight = 1d;
            if (RowEnd)
                worksheet.Cells[row, col].Borders[Excel.XlBordersIndex.xlEdgeBottom].Weight = 2d;
            worksheet.Cells[row, col].Borders[Excel.XlBordersIndex.xlEdgeTop].Weight = 2d;
        }
        private void SetBorder(Microsoft.Office.Interop.Excel._Worksheet worksheet, int row, int col, bool ColEnd, bool RowEnd)
        {
            worksheet.Cells[row,col].Borders[Excel.XlBordersIndex.xlEdgeLeft].Weight = 2d;
            if(ColEnd)
                worksheet.Cells[row, col].Borders[Excel.XlBordersIndex.xlEdgeRight].Weight = 2d;

            worksheet.Cells[row, col].Borders[Excel.XlBordersIndex.xlEdgeBottom].Weight = 1d;
            if(RowEnd)
                worksheet.Cells[row, col].Borders[Excel.XlBordersIndex.xlEdgeBottom].Weight = 2d;
        }
        public ActionResult ThongKeChatLuong(string MaHocKy, string NamHoc)
        {
            string userName = getUserName();

            int idTruong = Cmon.getIdtruong(userName, db);
            string namhoc = NamHoc;
            ViewBag.CapQuanLy = Cmon.getDonViChuQuan(idTruong);
            ViewBag.TenTruong = Cmon.GetnameSchool(idTruong);
            string TenHocKy = "";
            if (MaHocKy == "K1" || MaHocKy == "HK1") TenHocKy = "HỌC KỲ I";
            else if (MaHocKy == "GK1" || MaHocKy == "GHK1") TenHocKy = "GIỮA HỌC KỲ I";
            else if (MaHocKy == "GK2" || MaHocKy == "GHK2") TenHocKy = "GIỮA HỌC KỲ II";
            else TenHocKy = "CUỐI NĂM";
            ViewBag.HocKy = TenHocKy;
            ViewBag.NamHoc = namhoc;
            DataSet ds = new DataSet();

            SqlParameter[] para = new SqlParameter[3];
            para[0] = new SqlParameter("@IdTruong", idTruong);
            para[1] = new SqlParameter("@NamHoc", namhoc);
            para[2] = new SqlParameter("@MaHocKy", MaHocKy);
            //string connectionString = ConfigurationManager.ConnectionStrings["ConnectionStr"].ConnectionString;

            HNAFramework.DataAccess.Framework.DBHelper dbHelper = new HNAFramework.DataAccess.Framework.DBHelper();
            ds = dbHelper.ExecuteDatasetPRO("spThongKeChatLuong", para);
            //Tables[0] -> Học tập, Table[1] -> Năng lực, Tables[2] -> Phẩm chất, Tables[3] -> Khác
            Session["TongHopChatLuong"] = ds;
            Session["MaHocKyCL"] = MaHocKy;
            Session["NamHocCL"] = NamHoc;



            return PartialView(@"~/Views/TongHop/ThongKeChatLuong.cshtml", ds);
        }

        public JsonResult ThongKeChatLuongExcel()
        {
            string json = "";
            if (Session["TongHopChatLuong"]!=null)
            {
                string userName = getUserName();

                int idTruong = Cmon.getIdtruong(userName, db);
                string CapQuanLy = Cmon.getDonViChuQuan(idTruong);
                string TenTruong = Cmon.GetnameSchool(idTruong);
                string MaHocKy = Session["MaHocKyCL"].ToString();
                string TenHocKy = "";
                if (MaHocKy == "K1" || MaHocKy == "HK1") TenHocKy = "HỌC KỲ I";
                else if (MaHocKy == "GK1" || MaHocKy == "GHK1") TenHocKy = "GIỮA HỌC KỲ I";
                else if (MaHocKy == "GK2" || MaHocKy == "GHK2") TenHocKy = "GIỮA HỌC KỲ II";
                else TenHocKy = "CUỐI NĂM";
                string file = "THONGKECHATLUONG.xls";
                string templateFile = Path.Combine(Server.MapPath("~/templateExcell"), file);
                FileInfo f = new FileInfo(templateFile);
                string newfile = "THONGKECHATLUONG_" + userName + "_" + MaHocKy + "_" + DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + ".xls";

                string filename = Path.Combine(Server.MapPath("~/ProcessTemplate"), newfile);
                f.CopyTo(filename);

                DataSet ds = (DataSet)Session["TongHopChatLuong"];
                Microsoft.Office.Interop.Excel.Application oXL = null;
                Microsoft.Office.Interop.Excel._Workbook oWB = null;
                Microsoft.Office.Interop.Excel._Worksheet oSheet = null;

                oXL = new Microsoft.Office.Interop.Excel.Application();
                oWB = oXL.Workbooks.Open(filename);
                oSheet = String.IsNullOrEmpty("Sheet1") ? (Microsoft.Office.Interop.Excel._Worksheet)oWB.ActiveSheet : (Microsoft.Office.Interop.Excel._Worksheet)oWB.Worksheets["Sheet1"];
                oSheet.Cells[3, 2] = CapQuanLy;
                oSheet.Cells[4, 2] = TenTruong;

                oSheet.Cells[3, 10] = "THỐNG KÊ CHẤT LƯỢNG " + TenHocKy;
                oSheet.Cells[4, 10] = "Năm học: " + Session["NamHocCL"].ToString();
                int row = 13;
                DataTable dtHT = ds.Tables[0];
                #region Chất lượng học tập
                for (int i = 0; i < dtHT.Rows.Count; i++)
                {
                    if (i == 0)
                    {
                        oSheet.Cells[9, 2] = "1. " + dtHT.Rows[i]["TenMonHoc"].ToString();
                        oSheet.Cells[10, 2] = "Hoàn thành tốt";
                        oSheet.Cells[11, 2] = "Hoàn thành";
                        oSheet.Cells[12, 2] = "Chưa hoàn thành";
                        //Khối 1 - Hoàn thành tốt
                        oSheet.Cells[10, 4] = dtHT.Rows[i]["SoHocSinhK1HTT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhK1HTT"].ToString();
                        oSheet.Cells[10, 5] = dtHT.Rows[i]["SoHocSinhNuK1HTT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhNuK1HTT"].ToString();
                        oSheet.Cells[10, 6] = dtHT.Rows[i]["SoHocSinhDanTocK1HTT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhDanTocK1HTT"].ToString();
                        oSheet.Cells[10, 7] = dtHT.Rows[i]["SoHocSinhNuDanTocK1HTT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhNuDanTocK1HTT"].ToString();
                        oSheet.Cells[10, 9] = dtHT.Rows[i]["SoHocSinhKhuyetTatK1HTT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhKhuyetTatK1HTT"].ToString();

                        //Khối 1 - Hoàn thành
                        oSheet.Cells[11, 4] = dtHT.Rows[i]["SoHocSinhK1HT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhK1HT"].ToString();
                        oSheet.Cells[11, 5] = dtHT.Rows[i]["SoHocSinhNuK1HT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhNuK1HT"].ToString();
                        oSheet.Cells[11, 6] = dtHT.Rows[i]["SoHocSinhDanTocK1HT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhDanTocK1HT"].ToString();
                        oSheet.Cells[11, 7] = dtHT.Rows[i]["SoHocSinhNuDanTocK1HT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhNuDanTocK1HT"].ToString();
                        oSheet.Cells[11, 9] = dtHT.Rows[i]["SoHocSinhKhuyetTatK1HT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhKhuyetTatK1HT"].ToString();

                        //Khối 1 - Chưa hoàn thành
                        oSheet.Cells[12, 4] = dtHT.Rows[i]["SoHocSinhK1CHT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhK1CHT"].ToString();
                        oSheet.Cells[12, 5] = dtHT.Rows[i]["SoHocSinhNuK1CHT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhNuK1CHT"].ToString();
                        oSheet.Cells[12, 6] = dtHT.Rows[i]["SoHocSinhDanTocK1CHT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhDanTocK1CHT"].ToString();
                        oSheet.Cells[12, 7] = dtHT.Rows[i]["SoHocSinhNuDanTocK1CHT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhNuDanTocK1CHT"].ToString();
                        oSheet.Cells[12, 9] = dtHT.Rows[i]["SoHocSinhKhuyetTatK1CHT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhKhuyetTatK1CHT"].ToString();

                        //Khối 2 - Hoàn thành tốt
                        oSheet.Cells[10, 10] = dtHT.Rows[i]["SoHocSinhK2HTT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhK2HTT"].ToString();
                        oSheet.Cells[10, 11] = dtHT.Rows[i]["SoHocSinhNuK2HTT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhNuK2HTT"].ToString();
                        oSheet.Cells[10, 12] = dtHT.Rows[i]["SoHocSinhDanTocK2HTT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhDanTocK2HTT"].ToString();
                        oSheet.Cells[10, 13] = dtHT.Rows[i]["SoHocSinhNuDanTocK2HTT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhNuDanTocK2HTT"].ToString();
                        oSheet.Cells[10, 15] = dtHT.Rows[i]["SoHocSinhKhuyetTatK2HTT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhKhuyetTatK2HTT"].ToString();

                        //Khối 2 - Hoàn thành
                        oSheet.Cells[11, 10] = dtHT.Rows[i]["SoHocSinhK2HT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhK2HT"].ToString();
                        oSheet.Cells[11, 11] = dtHT.Rows[i]["SoHocSinhNuK2HT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhNuK2HT"].ToString();
                        oSheet.Cells[11, 12] = dtHT.Rows[i]["SoHocSinhDanTocK2HT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhDanTocK2HT"].ToString();
                        oSheet.Cells[11, 13] = dtHT.Rows[i]["SoHocSinhNuDanTocK2HT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhNuDanTocK2HT"].ToString();
                        oSheet.Cells[11, 15] = dtHT.Rows[i]["SoHocSinhKhuyetTatK2HT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhKhuyetTatK2HT"].ToString();

                        //Khối 2 - Chưa hoàn thành
                        oSheet.Cells[12, 10] = dtHT.Rows[i]["SoHocSinhK2CHT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhK2CHT"].ToString();
                        oSheet.Cells[12, 11] = dtHT.Rows[i]["SoHocSinhNuK2CHT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhNuK2CHT"].ToString();
                        oSheet.Cells[12, 12] = dtHT.Rows[i]["SoHocSinhDanTocK2CHT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhDanTocK2CHT"].ToString();
                        oSheet.Cells[12, 13] = dtHT.Rows[i]["SoHocSinhNuDanTocK2CHT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhNuDanTocK2CHT"].ToString();
                        oSheet.Cells[12, 15] = dtHT.Rows[i]["SoHocSinhKhuyetTatK2CHT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhKhuyetTatK2CHT"].ToString();

                        //Khối 3 - Hoàn thành tốt
                        oSheet.Cells[10, 16] = dtHT.Rows[i]["SoHocSinhK3HTT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhK3HTT"].ToString();
                        oSheet.Cells[10, 17] = dtHT.Rows[i]["SoHocSinhNuK3HTT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhNuK3HTT"].ToString();
                        oSheet.Cells[10, 18] = dtHT.Rows[i]["SoHocSinhDanTocK3HTT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhDanTocK3HTT"].ToString();
                        oSheet.Cells[10, 19] = dtHT.Rows[i]["SoHocSinhNuDanTocK3HTT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhNuDanTocK3HTT"].ToString();
                        oSheet.Cells[10, 21] = dtHT.Rows[i]["SoHocSinhKhuyetTatK3HTT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhKhuyetTatK3HTT"].ToString();

                        //Khối 3 - Hoàn thành
                        oSheet.Cells[11, 16] = dtHT.Rows[i]["SoHocSinhK3HT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhK3HT"].ToString();
                        oSheet.Cells[11, 17] = dtHT.Rows[i]["SoHocSinhNuK3HT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhNuK3HT"].ToString();
                        oSheet.Cells[11, 18] = dtHT.Rows[i]["SoHocSinhDanTocK3HT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhDanTocK3HT"].ToString();
                        oSheet.Cells[11, 19] = dtHT.Rows[i]["SoHocSinhNuDanTocK3HT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhNuDanTocK3HT"].ToString();
                        oSheet.Cells[11, 21] = dtHT.Rows[i]["SoHocSinhKhuyetTatK3HT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhKhuyetTatK3HT"].ToString();

                        //Khối 3 - Chưa hoàn thành
                        oSheet.Cells[12, 16] = dtHT.Rows[i]["SoHocSinhK3CHT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhK3CHT"].ToString();
                        oSheet.Cells[12, 17] = dtHT.Rows[i]["SoHocSinhNuK3CHT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhNuK3CHT"].ToString();
                        oSheet.Cells[12, 18] = dtHT.Rows[i]["SoHocSinhDanTocK3CHT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhDanTocK3CHT"].ToString();
                        oSheet.Cells[12, 19] = dtHT.Rows[i]["SoHocSinhNuDanTocK3CHT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhNuDanTocK3CHT"].ToString();
                        oSheet.Cells[12, 21] = dtHT.Rows[i]["SoHocSinhKhuyetTatK3CHT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhKhuyetTatK3CHT"].ToString();

                        //Khối 4 - Hoàn thành tốt
                        oSheet.Cells[10, 22] = dtHT.Rows[i]["SoHocSinhK4HTT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhK4HTT"].ToString();
                        oSheet.Cells[10, 23] = dtHT.Rows[i]["SoHocSinhNuK4HTT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhNuK4HTT"].ToString();
                        oSheet.Cells[10, 24] = dtHT.Rows[i]["SoHocSinhDanTocK4HTT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhDanTocK4HTT"].ToString();
                        oSheet.Cells[10, 25] = dtHT.Rows[i]["SoHocSinhNuDanTocK4HTT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhNuDanTocK4HTT"].ToString();
                        oSheet.Cells[10, 27] = dtHT.Rows[i]["SoHocSinhKhuyetTatK4HTT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhKhuyetTatK4HTT"].ToString();

                        //Khối 4 - Hoàn thành
                        oSheet.Cells[11, 22] = dtHT.Rows[i]["SoHocSinhK4HT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhK4HT"].ToString();
                        oSheet.Cells[11, 23] = dtHT.Rows[i]["SoHocSinhNuK4HT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhNuK4HT"].ToString();
                        oSheet.Cells[11, 24] = dtHT.Rows[i]["SoHocSinhDanTocK4HT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhDanTocK4HT"].ToString();
                        oSheet.Cells[11, 25] = dtHT.Rows[i]["SoHocSinhNuDanTocK4HT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhNuDanTocK4HT"].ToString();
                        oSheet.Cells[11, 27] = dtHT.Rows[i]["SoHocSinhKhuyetTatK4HT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhKhuyetTatK4HT"].ToString();

                        //Khối 4 - Chưa hoàn thành
                        oSheet.Cells[12, 22] = dtHT.Rows[i]["SoHocSinhK4CHT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhK4CHT"].ToString();
                        oSheet.Cells[12, 23] = dtHT.Rows[i]["SoHocSinhNuK4CHT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhNuK4CHT"].ToString();
                        oSheet.Cells[12, 24] = dtHT.Rows[i]["SoHocSinhDanTocK4CHT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhDanTocK4CHT"].ToString();
                        oSheet.Cells[12, 25] = dtHT.Rows[i]["SoHocSinhNuDanTocK4CHT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhNuDanTocK4CHT"].ToString();
                        oSheet.Cells[12, 27] = dtHT.Rows[i]["SoHocSinhKhuyetTatK4CHT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhKhuyetTatK4CHT"].ToString();

                        //Khối 5 - Hoàn thành tốt
                        oSheet.Cells[10, 28] = dtHT.Rows[i]["SoHocSinhK5HTT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhK5HTT"].ToString();
                        oSheet.Cells[10, 29] = dtHT.Rows[i]["SoHocSinhNuK5HTT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhNuK5HTT"].ToString();
                        oSheet.Cells[10, 30] = dtHT.Rows[i]["SoHocSinhDanTocK5HTT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhDanTocK5HTT"].ToString();
                        oSheet.Cells[10, 31] = dtHT.Rows[i]["SoHocSinhNuDanTocK5HTT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhNuDanTocK5HTT"].ToString();
                        oSheet.Cells[10, 33] = dtHT.Rows[i]["SoHocSinhKhuyetTatK5HTT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhKhuyetTatK5HTT"].ToString();

                        //Khối 5 - Hoàn thành
                        oSheet.Cells[11, 28] = dtHT.Rows[i]["SoHocSinhK5HT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhK5HT"].ToString();
                        oSheet.Cells[11, 29] = dtHT.Rows[i]["SoHocSinhNuK5HT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhNuK5HT"].ToString();
                        oSheet.Cells[11, 30] = dtHT.Rows[i]["SoHocSinhDanTocK5HT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhDanTocK5HT"].ToString();
                        oSheet.Cells[11, 31] = dtHT.Rows[i]["SoHocSinhNuDanTocK5HT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhNuDanTocK5HT"].ToString();
                        oSheet.Cells[11, 33] = dtHT.Rows[i]["SoHocSinhKhuyetTatK5HT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhKhuyetTatK5HT"].ToString();

                        //Khối 4 - Chưa hoàn thành
                        oSheet.Cells[12, 28] = dtHT.Rows[i]["SoHocSinhK5CHT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhK5CHT"].ToString();
                        oSheet.Cells[12, 29] = dtHT.Rows[i]["SoHocSinhNuK5CHT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhNuK5CHT"].ToString();
                        oSheet.Cells[12, 30] = dtHT.Rows[i]["SoHocSinhDanTocK5CHT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhDanTocK5CHT"].ToString();
                        oSheet.Cells[12, 31] = dtHT.Rows[i]["SoHocSinhNuDanTocK5CHT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhNuDanTocK5CHT"].ToString();
                        oSheet.Cells[12, 33] = dtHT.Rows[i]["SoHocSinhKhuyetTatK5CHT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhKhuyetTatK5CHT"].ToString();
                    }
                    else
                    {
                        oSheet.Cells[row, 2] = (i + 1).ToString() + ". " + dtHT.Rows[i]["TenMonHoc"].ToString();
                        oSheet.Cells[row + 1, 2] = "Hoàn thành tốt";
                        oSheet.Cells[row + 2, 2] = "Hoàn thành";
                        oSheet.Cells[row + 3, 2] = "Chưa hoàn thành";

                        //Khối 1 - Hoàn thành tốt
                        oSheet.Cells[row + 1, 4] = dtHT.Rows[i]["SoHocSinhK1HTT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhK1HTT"].ToString();
                        oSheet.Cells[row + 1, 5] = dtHT.Rows[i]["SoHocSinhNuK1HTT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhNuK1HTT"].ToString();
                        oSheet.Cells[row + 1, 6] = dtHT.Rows[i]["SoHocSinhDanTocK1HTT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhDanTocK1HTT"].ToString();
                        oSheet.Cells[row + 1, 7] = dtHT.Rows[i]["SoHocSinhNuDanTocK1HTT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhNuDanTocK1HTT"].ToString();
                        oSheet.Cells[row + 1, 9] = dtHT.Rows[i]["SoHocSinhKhuyetTatK1HTT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhKhuyetTatK1HTT"].ToString();

                        //Khối 1 - Hoàn thành
                        oSheet.Cells[row + 2, 4] = dtHT.Rows[i]["SoHocSinhK1HT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhK1HT"].ToString();
                        oSheet.Cells[row + 2, 5] = dtHT.Rows[i]["SoHocSinhNuK1HT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhNuK1HT"].ToString();
                        oSheet.Cells[row + 2, 6] = dtHT.Rows[i]["SoHocSinhDanTocK1HT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhDanTocK1HT"].ToString();
                        oSheet.Cells[row + 2, 7] = dtHT.Rows[i]["SoHocSinhNuDanTocK1HT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhNuDanTocK1HT"].ToString();
                        oSheet.Cells[row + 2, 9] = dtHT.Rows[i]["SoHocSinhKhuyetTatK1HT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhKhuyetTatK1HT"].ToString();

                        //Khối 1 - Chưa hoàn thành
                        oSheet.Cells[row + 3, 4] = dtHT.Rows[i]["SoHocSinhK1CHT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhK1CHT"].ToString();
                        oSheet.Cells[row + 3, 5] = dtHT.Rows[i]["SoHocSinhNuK1CHT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhNuK1CHT"].ToString();
                        oSheet.Cells[row + 3, 6] = dtHT.Rows[i]["SoHocSinhDanTocK1CHT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhDanTocK1CHT"].ToString();
                        oSheet.Cells[row + 3, 7] = dtHT.Rows[i]["SoHocSinhNuDanTocK1CHT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhNuDanTocK1CHT"].ToString();
                        oSheet.Cells[row + 3, 9] = dtHT.Rows[i]["SoHocSinhKhuyetTatK1CHT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhKhuyetTatK1CHT"].ToString();

                        //Khối 2 - Hoàn thành tốt
                        oSheet.Cells[row + 1, 10] = dtHT.Rows[i]["SoHocSinhK2HTT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhK2HTT"].ToString();
                        oSheet.Cells[row + 1, 11] = dtHT.Rows[i]["SoHocSinhNuK2HTT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhNuK2HTT"].ToString();
                        oSheet.Cells[row + 1, 12] = dtHT.Rows[i]["SoHocSinhDanTocK2HTT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhDanTocK2HTT"].ToString();
                        oSheet.Cells[row + 1, 13] = dtHT.Rows[i]["SoHocSinhNuDanTocK2HTT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhNuDanTocK2HTT"].ToString();
                        oSheet.Cells[row + 1, 15] = dtHT.Rows[i]["SoHocSinhKhuyetTatK2HTT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhKhuyetTatK2HTT"].ToString();

                        //Khối 2 - Hoàn thành
                        oSheet.Cells[row + 2, 10] = dtHT.Rows[i]["SoHocSinhK2HT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhK2HT"].ToString();
                        oSheet.Cells[row + 2, 11] = dtHT.Rows[i]["SoHocSinhNuK2HT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhNuK2HT"].ToString();
                        oSheet.Cells[row + 2, 12] = dtHT.Rows[i]["SoHocSinhDanTocK2HT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhDanTocK2HT"].ToString();
                        oSheet.Cells[row + 2, 13] = dtHT.Rows[i]["SoHocSinhNuDanTocK2HT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhNuDanTocK2HT"].ToString();
                        oSheet.Cells[row + 2, 15] = dtHT.Rows[i]["SoHocSinhKhuyetTatK2HT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhKhuyetTatK2HT"].ToString();

                        //Khối 2 - Chưa hoàn thành
                        oSheet.Cells[row + 3, 10] = dtHT.Rows[i]["SoHocSinhK2CHT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhK2CHT"].ToString();
                        oSheet.Cells[row + 3, 11] = dtHT.Rows[i]["SoHocSinhNuK2CHT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhNuK2CHT"].ToString();
                        oSheet.Cells[row + 3, 12] = dtHT.Rows[i]["SoHocSinhDanTocK2CHT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhDanTocK2CHT"].ToString();
                        oSheet.Cells[row + 3, 13] = dtHT.Rows[i]["SoHocSinhNuDanTocK2CHT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhNuDanTocK2CHT"].ToString();
                        oSheet.Cells[row + 3, 15] = dtHT.Rows[i]["SoHocSinhKhuyetTatK2CHT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhKhuyetTatK2CHT"].ToString();

                        //Khối 3 - Hoàn thành tốt
                        oSheet.Cells[row + 1, 16] = dtHT.Rows[i]["SoHocSinhK3HTT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhK3HTT"].ToString();
                        oSheet.Cells[row + 1, 17] = dtHT.Rows[i]["SoHocSinhNuK3HTT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhNuK3HTT"].ToString();
                        oSheet.Cells[row + 1, 18] = dtHT.Rows[i]["SoHocSinhDanTocK3HTT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhDanTocK3HTT"].ToString();
                        oSheet.Cells[row + 1, 19] = dtHT.Rows[i]["SoHocSinhNuDanTocK3HTT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhNuDanTocK3HTT"].ToString();
                        oSheet.Cells[row + 1, 21] = dtHT.Rows[i]["SoHocSinhKhuyetTatK3HTT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhKhuyetTatK3HTT"].ToString();

                        //Khối 3 - Hoàn thành
                        oSheet.Cells[row + 2, 16] = dtHT.Rows[i]["SoHocSinhK3HT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhK3HT"].ToString();
                        oSheet.Cells[row + 2, 17] = dtHT.Rows[i]["SoHocSinhNuK3HT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhNuK3HT"].ToString();
                        oSheet.Cells[row + 2, 18] = dtHT.Rows[i]["SoHocSinhDanTocK3HT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhDanTocK3HT"].ToString();
                        oSheet.Cells[row + 2, 19] = dtHT.Rows[i]["SoHocSinhNuDanTocK3HT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhNuDanTocK3HT"].ToString();
                        oSheet.Cells[row + 2, 21] = dtHT.Rows[i]["SoHocSinhKhuyetTatK3HT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhKhuyetTatK3HT"].ToString();

                        //Khối 3 - Chưa hoàn thành
                        oSheet.Cells[row + 3, 16] = dtHT.Rows[i]["SoHocSinhK3CHT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhK3CHT"].ToString();
                        oSheet.Cells[row + 3, 17] = dtHT.Rows[i]["SoHocSinhNuK3CHT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhNuK3CHT"].ToString();
                        oSheet.Cells[row + 3, 18] = dtHT.Rows[i]["SoHocSinhDanTocK3CHT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhDanTocK3CHT"].ToString();
                        oSheet.Cells[row + 3, 19] = dtHT.Rows[i]["SoHocSinhNuDanTocK3CHT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhNuDanTocK3CHT"].ToString();
                        oSheet.Cells[row + 3, 21] = dtHT.Rows[i]["SoHocSinhKhuyetTatK3CHT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhKhuyetTatK3CHT"].ToString();

                        //Khối 4 - Hoàn thành tốt
                        oSheet.Cells[row + 1, 22] = dtHT.Rows[i]["SoHocSinhK4HTT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhK4HTT"].ToString();
                        oSheet.Cells[row + 1, 23] = dtHT.Rows[i]["SoHocSinhNuK4HTT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhNuK4HTT"].ToString();
                        oSheet.Cells[row + 1, 24] = dtHT.Rows[i]["SoHocSinhDanTocK4HTT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhDanTocK4HTT"].ToString();
                        oSheet.Cells[row + 1, 25] = dtHT.Rows[i]["SoHocSinhNuDanTocK4HTT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhNuDanTocK4HTT"].ToString();
                        oSheet.Cells[row + 1, 27] = dtHT.Rows[i]["SoHocSinhKhuyetTatK4HTT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhKhuyetTatK4HTT"].ToString();

                        //Khối 4 - Hoàn thành
                        oSheet.Cells[row + 2, 22] = dtHT.Rows[i]["SoHocSinhK4HT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhK4HT"].ToString();
                        oSheet.Cells[row + 2, 23] = dtHT.Rows[i]["SoHocSinhNuK4HT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhNuK4HT"].ToString();
                        oSheet.Cells[row + 2, 24] = dtHT.Rows[i]["SoHocSinhDanTocK4HT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhDanTocK4HT"].ToString();
                        oSheet.Cells[row + 2, 25] = dtHT.Rows[i]["SoHocSinhNuDanTocK4HT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhNuDanTocK4HT"].ToString();
                        oSheet.Cells[row + 2, 27] = dtHT.Rows[i]["SoHocSinhKhuyetTatK4HT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhKhuyetTatK4HT"].ToString();

                        //Khối 4 - Chưa hoàn thành
                        oSheet.Cells[row + 3, 22] = dtHT.Rows[i]["SoHocSinhK4CHT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhK4CHT"].ToString();
                        oSheet.Cells[row + 3, 23] = dtHT.Rows[i]["SoHocSinhNuK4CHT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhNuK4CHT"].ToString();
                        oSheet.Cells[row + 3, 24] = dtHT.Rows[i]["SoHocSinhDanTocK4CHT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhDanTocK4CHT"].ToString();
                        oSheet.Cells[row + 3, 25] = dtHT.Rows[i]["SoHocSinhNuDanTocK4CHT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhNuDanTocK4CHT"].ToString();
                        oSheet.Cells[row + 3, 27] = dtHT.Rows[i]["SoHocSinhKhuyetTatK4CHT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhKhuyetTatK4CHT"].ToString();

                        //Khối 5 - Hoàn thành tốt
                        oSheet.Cells[row + 1, 28] = dtHT.Rows[i]["SoHocSinhK5HTT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhK5HTT"].ToString();
                        oSheet.Cells[row + 1, 29] = dtHT.Rows[i]["SoHocSinhNuK5HTT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhNuK5HTT"].ToString();
                        oSheet.Cells[row + 1, 30] = dtHT.Rows[i]["SoHocSinhDanTocK5HTT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhDanTocK5HTT"].ToString();
                        oSheet.Cells[row + 1, 31] = dtHT.Rows[i]["SoHocSinhNuDanTocK5HTT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhNuDanTocK5HTT"].ToString();
                        oSheet.Cells[row + 1, 33] = dtHT.Rows[i]["SoHocSinhKhuyetTatK5HTT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhKhuyetTatK5HTT"].ToString();

                        //Khối 5 - Hoàn thành
                        oSheet.Cells[row + 2, 28] = dtHT.Rows[i]["SoHocSinhK5HT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhK5HT"].ToString();
                        oSheet.Cells[row + 2, 29] = dtHT.Rows[i]["SoHocSinhNuK5HT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhNuK5HT"].ToString();
                        oSheet.Cells[row + 2, 30] = dtHT.Rows[i]["SoHocSinhDanTocK5HT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhDanTocK5HT"].ToString();
                        oSheet.Cells[row + 2, 31] = dtHT.Rows[i]["SoHocSinhNuDanTocK5HT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhNuDanTocK5HT"].ToString();
                        oSheet.Cells[row + 2, 33] = dtHT.Rows[i]["SoHocSinhKhuyetTatK5HT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhKhuyetTatK5HT"].ToString();

                        //Khối 4 - Chưa hoàn thành
                        oSheet.Cells[row + 3, 28] = dtHT.Rows[i]["SoHocSinhK5CHT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhK5CHT"].ToString();
                        oSheet.Cells[row + 3, 29] = dtHT.Rows[i]["SoHocSinhNuK5CHT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhNuK5CHT"].ToString();
                        oSheet.Cells[row + 3, 30] = dtHT.Rows[i]["SoHocSinhDanTocK5CHT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhDanTocK5CHT"].ToString();
                        oSheet.Cells[row + 3, 31] = dtHT.Rows[i]["SoHocSinhNuDanTocK5CHT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhNuDanTocK5CHT"].ToString();
                        oSheet.Cells[row + 3, 33] = dtHT.Rows[i]["SoHocSinhKhuyetTatK5CHT"].ToString() == "0" ? "" : dtHT.Rows[i]["SoHocSinhKhuyetTatK5CHT"].ToString();

                        row = row + 4;
                    }
                }
                #endregion

                #region Chất lượng năng lực
                DataTable dtNL = ds.Tables[1];
                #region Năng lực tự phục vụ tự quản
                DataRow[] drNL = dtNL.Select("PhanLoai='TPVTQ'");
                if (drNL!=null && drNL.Count()>0)
                {
                    int rowNL = 63;
                    for (int j = 0; j < drNL.Count(); j++)
                    {
                        
                        oSheet.Cells[rowNL + j, 2] = drNL[j]["XepLoai"].ToString();
                        //Khối 1
                        oSheet.Cells[rowNL+j, 4] = drNL[j]["SoHocSinhK1"].ToString() == "0" ? "" : drNL[j]["SoHocSinhK1"].ToString();
                        oSheet.Cells[rowNL + j, 5] = drNL[j]["SoHocSinhNuK1"].ToString() == "0" ? "" : drNL[j]["SoHocSinhNuK1"].ToString();
                        oSheet.Cells[rowNL + j, 6] = drNL[j]["SoHocSinhDanTocK1"].ToString() == "0" ? "" : drNL[j]["SoHocSinhDanTocK1"].ToString();
                        oSheet.Cells[rowNL + j, 7] = drNL[j]["SoHocSinhNuDanTocK1"].ToString() == "0" ? "" : drNL[j]["SoHocSinhNuDanTocK1"].ToString();
                        oSheet.Cells[rowNL + j, 9] = drNL[j]["SoHocSinhKhuyetTatK1"].ToString() == "0" ? "" : drNL[j]["SoHocSinhKhuyetTatK1"].ToString();

                        //Khối 2
                        oSheet.Cells[rowNL + j, 10] = drNL[j]["SoHocSinhK2"].ToString() == "0" ? "" : drNL[j]["SoHocSinhK2"].ToString();
                        oSheet.Cells[rowNL + j, 11] = drNL[j]["SoHocSinhNuK2"].ToString() == "0" ? "" : drNL[j]["SoHocSinhNuK2"].ToString();
                        oSheet.Cells[rowNL + j, 12] = drNL[j]["SoHocSinhDanTocK2"].ToString() == "0" ? "" : drNL[j]["SoHocSinhDanTocK2"].ToString();
                        oSheet.Cells[rowNL + j, 13] = drNL[j]["SoHocSinhNuDanTocK2"].ToString() == "0" ? "" : drNL[j]["SoHocSinhNuDanTocK2"].ToString();
                        oSheet.Cells[rowNL + j, 15] = drNL[j]["SoHocSinhKhuyetTatK2"].ToString() == "0" ? "" : drNL[j]["SoHocSinhKhuyetTatK2"].ToString();

                        //Khối 3
                        oSheet.Cells[rowNL + j, 16] = drNL[j]["SoHocSinhK3"].ToString() == "0" ? "" : drNL[j]["SoHocSinhK3"].ToString();
                        oSheet.Cells[rowNL + j, 17] = drNL[j]["SoHocSinhNuK3"].ToString() == "0" ? "" : drNL[j]["SoHocSinhNuK3"].ToString();
                        oSheet.Cells[rowNL + j, 18] = drNL[j]["SoHocSinhDanTocK3"].ToString() == "0" ? "" : drNL[j]["SoHocSinhDanTocK3"].ToString();
                        oSheet.Cells[rowNL + j, 19] = drNL[j]["SoHocSinhNuDanTocK3"].ToString() == "0" ? "" : drNL[j]["SoHocSinhNuDanTocK3"].ToString();
                        oSheet.Cells[rowNL + j, 21] = drNL[j]["SoHocSinhKhuyetTatK3"].ToString() == "0" ? "" : drNL[j]["SoHocSinhKhuyetTatK3"].ToString();

                        //Khối 4
                        oSheet.Cells[rowNL + j, 22] = drNL[j]["SoHocSinhK4"].ToString() == "0" ? "" : drNL[j]["SoHocSinhK4"].ToString();
                        oSheet.Cells[rowNL + j, 23] = drNL[j]["SoHocSinhNuK4"].ToString() == "0" ? "" : drNL[j]["SoHocSinhNuK4"].ToString();
                        oSheet.Cells[rowNL + j, 24] = drNL[j]["SoHocSinhDanTocK4"].ToString() == "0" ? "" : drNL[j]["SoHocSinhDanTocK4"].ToString();
                        oSheet.Cells[rowNL + j, 25] = drNL[j]["SoHocSinhNuDanTocK4"].ToString() == "0" ? "" : drNL[j]["SoHocSinhNuDanTocK4"].ToString();
                        oSheet.Cells[rowNL + j, 27] = drNL[j]["SoHocSinhKhuyetTatK4"].ToString() == "0" ? "" : drNL[j]["SoHocSinhKhuyetTatK4"].ToString();

                        //Khối 5
                        oSheet.Cells[rowNL + j, 28] = drNL[j]["SoHocSinhK5"].ToString() == "0" ? "" : drNL[j]["SoHocSinhK5"].ToString();
                        oSheet.Cells[rowNL + j, 29] = drNL[j]["SoHocSinhNuK5"].ToString() == "0" ? "" : drNL[j]["SoHocSinhNuK5"].ToString();
                        oSheet.Cells[rowNL + j, 30] = drNL[j]["SoHocSinhDanTocK5"].ToString() == "0" ? "" : drNL[j]["SoHocSinhDanTocK5"].ToString();
                        oSheet.Cells[rowNL + j, 31] = drNL[j]["SoHocSinhNuDanTocK5"].ToString() == "0" ? "" : drNL[j]["SoHocSinhNuDanTocK5"].ToString();
                        oSheet.Cells[rowNL + j, 33] = drNL[j]["SoHocSinhKhuyetTatK5"].ToString() == "0" ? "" : drNL[j]["SoHocSinhKhuyetTatK5"].ToString();
                    }
                }
                #endregion

                #region Năng lực hợp tác
                DataRow[] drHT = dtNL.Select("PhanLoai='HT'");
                if (drHT != null && drHT.Count() > 0)
                {
                    int rowHT = 67;
                    for (int k = 0; k < drHT.Count(); k++)
                    {

                        oSheet.Cells[rowHT + k, 2] = drHT[k]["XepLoai"].ToString();
                        //Khối 1
                        oSheet.Cells[rowHT + k, 4] = drHT[k]["SoHocSinhK1"].ToString() == "0" ? "" : drHT[k]["SoHocSinhK1"].ToString();
                        oSheet.Cells[rowHT + k, 5] = drHT[k]["SoHocSinhNuK1"].ToString() == "0" ? "" : drHT[k]["SoHocSinhNuK1"].ToString();
                        oSheet.Cells[rowHT + k, 6] = drHT[k]["SoHocSinhDanTocK1"].ToString() == "0" ? "" : drHT[k]["SoHocSinhDanTocK1"].ToString();
                        oSheet.Cells[rowHT + k, 7] = drHT[k]["SoHocSinhNuDanTocK1"].ToString() == "0" ? "" : drHT[k]["SoHocSinhNuDanTocK1"].ToString();
                        oSheet.Cells[rowHT + k, 9] = drHT[k]["SoHocSinhKhuyetTatK1"].ToString() == "0" ? "" : drHT[k]["SoHocSinhKhuyetTatK1"].ToString();

                        //Khối 2
                        oSheet.Cells[rowHT + k, 10] = drHT[k]["SoHocSinhK2"].ToString() == "0" ? "" : drHT[k]["SoHocSinhK2"].ToString();
                        oSheet.Cells[rowHT + k, 11] = drHT[k]["SoHocSinhNuK2"].ToString() == "0" ? "" : drHT[k]["SoHocSinhNuK2"].ToString();
                        oSheet.Cells[rowHT + k, 12] = drHT[k]["SoHocSinhDanTocK2"].ToString() == "0" ? "" : drHT[k]["SoHocSinhDanTocK2"].ToString();
                        oSheet.Cells[rowHT + k, 13] = drHT[k]["SoHocSinhNuDanTocK2"].ToString() == "0" ? "" : drHT[k]["SoHocSinhNuDanTocK2"].ToString();
                        oSheet.Cells[rowHT + k, 15] = drHT[k]["SoHocSinhKhuyetTatK2"].ToString() == "0" ? "" : drHT[k]["SoHocSinhKhuyetTatK2"].ToString();

                        //Khối 3
                        oSheet.Cells[rowHT + k, 16] = drHT[k]["SoHocSinhK3"].ToString() == "0" ? "" : drHT[k]["SoHocSinhK3"].ToString();
                        oSheet.Cells[rowHT + k, 17] = drHT[k]["SoHocSinhNuK3"].ToString() == "0" ? "" : drHT[k]["SoHocSinhNuK3"].ToString();
                        oSheet.Cells[rowHT + k, 18] = drHT[k]["SoHocSinhDanTocK3"].ToString() == "0" ? "" : drHT[k]["SoHocSinhDanTocK3"].ToString();
                        oSheet.Cells[rowHT + k, 19] = drHT[k]["SoHocSinhNuDanTocK3"].ToString() == "0" ? "" : drHT[k]["SoHocSinhNuDanTocK3"].ToString();
                        oSheet.Cells[rowHT + k, 21] = drHT[k]["SoHocSinhKhuyetTatK3"].ToString() == "0" ? "" : drHT[k]["SoHocSinhKhuyetTatK3"].ToString();

                        //Khối 4
                        oSheet.Cells[rowHT + k, 22] = drHT[k]["SoHocSinhK4"].ToString() == "0" ? "" : drHT[k]["SoHocSinhK4"].ToString();
                        oSheet.Cells[rowHT + k, 23] = drHT[k]["SoHocSinhNuK4"].ToString() == "0" ? "" : drHT[k]["SoHocSinhNuK4"].ToString();
                        oSheet.Cells[rowHT + k, 24] = drHT[k]["SoHocSinhDanTocK4"].ToString() == "0" ? "" : drHT[k]["SoHocSinhDanTocK4"].ToString();
                        oSheet.Cells[rowHT + k, 25] = drHT[k]["SoHocSinhNuDanTocK4"].ToString() == "0" ? "" : drHT[k]["SoHocSinhNuDanTocK4"].ToString();
                        oSheet.Cells[rowHT + k, 27] = drHT[k]["SoHocSinhKhuyetTatK4"].ToString() == "0" ? "" : drHT[k]["SoHocSinhKhuyetTatK4"].ToString();

                        //Khối 5
                        oSheet.Cells[rowHT + k, 28] = drHT[k]["SoHocSinhK5"].ToString() == "0" ? "" : drHT[k]["SoHocSinhK5"].ToString();
                        oSheet.Cells[rowHT + k, 29] = drHT[k]["SoHocSinhNuK5"].ToString() == "0" ? "" : drHT[k]["SoHocSinhNuK5"].ToString();
                        oSheet.Cells[rowHT + k, 30] = drHT[k]["SoHocSinhDanTocK5"].ToString() == "0" ? "" : drHT[k]["SoHocSinhDanTocK5"].ToString();
                        oSheet.Cells[rowHT + k, 31] = drHT[k]["SoHocSinhNuDanTocK5"].ToString() == "0" ? "" : drHT[k]["SoHocSinhNuDanTocK5"].ToString();
                        oSheet.Cells[rowHT + k, 33] = drHT[k]["SoHocSinhKhuyetTatK5"].ToString() == "0" ? "" : drHT[k]["SoHocSinhKhuyetTatK5"].ToString();
                    }
                }
                #endregion

                #region Năng lực tự học và giải quyết vấn đề
                DataRow[] drTHGQVD = dtNL.Select("PhanLoai='THGQVD'");
                if (drTHGQVD != null && drTHGQVD.Count() > 0)
                {
                    int rowTHGQVD = 71;
                    for (int l = 0; l < drTHGQVD.Count(); l++)
                    {

                        oSheet.Cells[rowTHGQVD+l, 2] = drTHGQVD[l]["XepLoai"].ToString();
                        //Khối 1
                        oSheet.Cells[rowTHGQVD+l, 4] = drTHGQVD[l]["SoHocSinhK1"].ToString() == "0" ? "" : drTHGQVD[l]["SoHocSinhK1"].ToString();
                        oSheet.Cells[rowTHGQVD+l, 5] = drTHGQVD[l]["SoHocSinhNuK1"].ToString() == "0" ? "" : drTHGQVD[l]["SoHocSinhNuK1"].ToString();
                        oSheet.Cells[rowTHGQVD+l, 6] = drTHGQVD[l]["SoHocSinhDanTocK1"].ToString() == "0" ? "" : drTHGQVD[l]["SoHocSinhDanTocK1"].ToString();
                        oSheet.Cells[rowTHGQVD+l, 7] = drTHGQVD[l]["SoHocSinhNuDanTocK1"].ToString() == "0" ? "" : drTHGQVD[l]["SoHocSinhNuDanTocK1"].ToString();
                        oSheet.Cells[rowTHGQVD+l, 9] = drTHGQVD[l]["SoHocSinhKhuyetTatK1"].ToString() == "0" ? "" : drTHGQVD[l]["SoHocSinhKhuyetTatK1"].ToString();

                        //Khối 2
                        oSheet.Cells[rowTHGQVD+l, 10] = drTHGQVD[l]["SoHocSinhK2"].ToString() == "0" ? "" : drTHGQVD[l]["SoHocSinhK2"].ToString();
                        oSheet.Cells[rowTHGQVD+l, 11] = drTHGQVD[l]["SoHocSinhNuK2"].ToString() == "0" ? "" : drTHGQVD[l]["SoHocSinhNuK2"].ToString();
                        oSheet.Cells[rowTHGQVD+l, 12] = drTHGQVD[l]["SoHocSinhDanTocK2"].ToString() == "0" ? "" : drTHGQVD[l]["SoHocSinhDanTocK2"].ToString();
                        oSheet.Cells[rowTHGQVD+l, 13] = drTHGQVD[l]["SoHocSinhNuDanTocK2"].ToString() == "0" ? "" : drTHGQVD[l]["SoHocSinhNuDanTocK2"].ToString();
                        oSheet.Cells[rowTHGQVD+l, 15] = drTHGQVD[l]["SoHocSinhKhuyetTatK2"].ToString() == "0" ? "" : drTHGQVD[l]["SoHocSinhKhuyetTatK2"].ToString();

                        //Khối 3
                        oSheet.Cells[rowTHGQVD+l, 16] = drTHGQVD[l]["SoHocSinhK3"].ToString() == "0" ? "" : drTHGQVD[l]["SoHocSinhK3"].ToString();
                        oSheet.Cells[rowTHGQVD+l, 17] = drTHGQVD[l]["SoHocSinhNuK3"].ToString() == "0" ? "" : drTHGQVD[l]["SoHocSinhNuK3"].ToString();
                        oSheet.Cells[rowTHGQVD+l, 18] = drTHGQVD[l]["SoHocSinhDanTocK3"].ToString() == "0" ? "" : drTHGQVD[l]["SoHocSinhDanTocK3"].ToString();
                        oSheet.Cells[rowTHGQVD+l, 19] = drTHGQVD[l]["SoHocSinhNuDanTocK3"].ToString() == "0" ? "" : drTHGQVD[l]["SoHocSinhNuDanTocK3"].ToString();
                        oSheet.Cells[rowTHGQVD+l, 21] = drTHGQVD[l]["SoHocSinhKhuyetTatK3"].ToString() == "0" ? "" : drTHGQVD[l]["SoHocSinhKhuyetTatK3"].ToString();

                        //Khối 4
                        oSheet.Cells[rowTHGQVD+l, 22] = drTHGQVD[l]["SoHocSinhK4"].ToString() == "0" ? "" : drTHGQVD[l]["SoHocSinhK4"].ToString();
                        oSheet.Cells[rowTHGQVD+l, 23] = drTHGQVD[l]["SoHocSinhNuK4"].ToString() == "0" ? "" : drTHGQVD[l]["SoHocSinhNuK4"].ToString();
                        oSheet.Cells[rowTHGQVD+l, 24] = drTHGQVD[l]["SoHocSinhDanTocK4"].ToString() == "0" ? "" : drTHGQVD[l]["SoHocSinhDanTocK4"].ToString();
                        oSheet.Cells[rowTHGQVD+l, 25] = drTHGQVD[l]["SoHocSinhNuDanTocK4"].ToString() == "0" ? "" : drTHGQVD[l]["SoHocSinhNuDanTocK4"].ToString();
                        oSheet.Cells[rowTHGQVD+l, 27] = drTHGQVD[l]["SoHocSinhKhuyetTatK4"].ToString() == "0" ? "" : drTHGQVD[l]["SoHocSinhKhuyetTatK4"].ToString();

                        //Khối 5
                        oSheet.Cells[rowTHGQVD+l, 28] = drTHGQVD[l]["SoHocSinhK5"].ToString() == "0" ? "" : drTHGQVD[l]["SoHocSinhK5"].ToString();
                        oSheet.Cells[rowTHGQVD+l, 29] = drTHGQVD[l]["SoHocSinhNuK5"].ToString() == "0" ? "" : drTHGQVD[l]["SoHocSinhNuK5"].ToString();
                        oSheet.Cells[rowTHGQVD+l, 30] = drTHGQVD[l]["SoHocSinhDanTocK5"].ToString() == "0" ? "" : drTHGQVD[l]["SoHocSinhDanTocK5"].ToString();
                        oSheet.Cells[rowTHGQVD+l, 31] = drTHGQVD[l]["SoHocSinhNuDanTocK5"].ToString() == "0" ? "" : drTHGQVD[l]["SoHocSinhNuDanTocK5"].ToString();
                        oSheet.Cells[rowTHGQVD+l, 33] = drTHGQVD[l]["SoHocSinhKhuyetTatK5"].ToString() == "0" ? "" : drTHGQVD[l]["SoHocSinhKhuyetTatK5"].ToString();
                    }
                }
                #endregion
                #endregion

                #region Chất lượng phẩm chất
                DataTable dtPC = ds.Tables[2];
                #region Phẩm chất chăm học, chăm làm
                DataRow[] drCHCL = dtPC.Select("PhanLoai='CHCL'");
                if (drCHCL != null && drCHCL.Count() > 0)
                {
                    int rowCHCL = 76;
                    for (int m = 0; m < drCHCL.Count(); m++)
                    {

                        oSheet.Cells[rowCHCL+m, 2] = drCHCL[m]["XepLoai"].ToString();
                        //Khối 1
                        oSheet.Cells[rowCHCL+m, 4] = drCHCL[m]["SoHocSinhK1"].ToString() == "0" ? "" : drCHCL[m]["SoHocSinhK1"].ToString();
                        oSheet.Cells[rowCHCL+m, 5] = drCHCL[m]["SoHocSinhNuK1"].ToString() == "0" ? "" : drCHCL[m]["SoHocSinhNuK1"].ToString();
                        oSheet.Cells[rowCHCL+m, 6] = drCHCL[m]["SoHocSinhDanTocK1"].ToString() == "0" ? "" : drCHCL[m]["SoHocSinhDanTocK1"].ToString();
                        oSheet.Cells[rowCHCL+m, 7] = drCHCL[m]["SoHocSinhNuDanTocK1"].ToString() == "0" ? "" : drCHCL[m]["SoHocSinhNuDanTocK1"].ToString();
                        oSheet.Cells[rowCHCL+m, 9] = drCHCL[m]["SoHocSinhKhuyetTatK1"].ToString() == "0" ? "" : drCHCL[m]["SoHocSinhKhuyetTatK1"].ToString();

                        //Khối 2
                        oSheet.Cells[rowCHCL+m, 10] = drCHCL[m]["SoHocSinhK2"].ToString() == "0" ? "" : drCHCL[m]["SoHocSinhK2"].ToString();
                        oSheet.Cells[rowCHCL+m, 11] = drCHCL[m]["SoHocSinhNuK2"].ToString() == "0" ? "" : drCHCL[m]["SoHocSinhNuK2"].ToString();
                        oSheet.Cells[rowCHCL+m, 12] = drCHCL[m]["SoHocSinhDanTocK2"].ToString() == "0" ? "" : drCHCL[m]["SoHocSinhDanTocK2"].ToString();
                        oSheet.Cells[rowCHCL+m, 13] = drCHCL[m]["SoHocSinhNuDanTocK2"].ToString() == "0" ? "" : drCHCL[m]["SoHocSinhNuDanTocK2"].ToString();
                        oSheet.Cells[rowCHCL+m, 15] = drCHCL[m]["SoHocSinhKhuyetTatK2"].ToString() == "0" ? "" : drCHCL[m]["SoHocSinhKhuyetTatK2"].ToString();

                        //Khối 3
                        oSheet.Cells[rowCHCL+m, 16] = drCHCL[m]["SoHocSinhK3"].ToString() == "0" ? "" : drCHCL[m]["SoHocSinhK3"].ToString();
                        oSheet.Cells[rowCHCL+m, 17] = drCHCL[m]["SoHocSinhNuK3"].ToString() == "0" ? "" : drCHCL[m]["SoHocSinhNuK3"].ToString();
                        oSheet.Cells[rowCHCL+m, 18] = drCHCL[m]["SoHocSinhDanTocK3"].ToString() == "0" ? "" : drCHCL[m]["SoHocSinhDanTocK3"].ToString();
                        oSheet.Cells[rowCHCL+m, 19] = drCHCL[m]["SoHocSinhNuDanTocK3"].ToString() == "0" ? "" : drCHCL[m]["SoHocSinhNuDanTocK3"].ToString();
                        oSheet.Cells[rowCHCL+m, 21] = drCHCL[m]["SoHocSinhKhuyetTatK3"].ToString() == "0" ? "" : drCHCL[m]["SoHocSinhKhuyetTatK3"].ToString();

                        //Khối 4
                        oSheet.Cells[rowCHCL+m, 22] = drCHCL[m]["SoHocSinhK4"].ToString() == "0" ? "" : drCHCL[m]["SoHocSinhK4"].ToString();
                        oSheet.Cells[rowCHCL+m, 23] = drCHCL[m]["SoHocSinhNuK4"].ToString() == "0" ? "" : drCHCL[m]["SoHocSinhNuK4"].ToString();
                        oSheet.Cells[rowCHCL+m, 24] = drCHCL[m]["SoHocSinhDanTocK4"].ToString() == "0" ? "" : drCHCL[m]["SoHocSinhDanTocK4"].ToString();
                        oSheet.Cells[rowCHCL+m, 25] = drCHCL[m]["SoHocSinhNuDanTocK4"].ToString() == "0" ? "" : drCHCL[m]["SoHocSinhNuDanTocK4"].ToString();
                        oSheet.Cells[rowCHCL+m, 27] = drCHCL[m]["SoHocSinhKhuyetTatK4"].ToString() == "0" ? "" : drCHCL[m]["SoHocSinhKhuyetTatK4"].ToString();

                        //Khối 5
                        oSheet.Cells[rowCHCL+m, 28] = drCHCL[m]["SoHocSinhK5"].ToString() == "0" ? "" : drCHCL[m]["SoHocSinhK5"].ToString();
                        oSheet.Cells[rowCHCL+m, 29] = drCHCL[m]["SoHocSinhNuK5"].ToString() == "0" ? "" : drCHCL[m]["SoHocSinhNuK5"].ToString();
                        oSheet.Cells[rowCHCL+m, 30] = drCHCL[m]["SoHocSinhDanTocK5"].ToString() == "0" ? "" : drCHCL[m]["SoHocSinhDanTocK5"].ToString();
                        oSheet.Cells[rowCHCL+m, 31] = drCHCL[m]["SoHocSinhNuDanTocK5"].ToString() == "0" ? "" : drCHCL[m]["SoHocSinhNuDanTocK5"].ToString();
                        oSheet.Cells[rowCHCL+m, 33] = drCHCL[m]["SoHocSinhKhuyetTatK5"].ToString() == "0" ? "" : drCHCL[m]["SoHocSinhKhuyetTatK5"].ToString();
                    }
                }
                #endregion

                #region Phẩm chất tự tin, trách nhiệm
                DataRow[] drTTTN = dtPC.Select("PhanLoai='TTTN'");
                if (drTTTN != null && drTTTN.Count() > 0)
                {
                    int rowTTTN = 80;
                    for (int n = 0; n < drTTTN.Count(); n++)
                    {

                        oSheet.Cells[rowTTTN+n, 2] = drTTTN[n]["XepLoai"].ToString();
                        //Khối 1
                        oSheet.Cells[rowTTTN+n, 4] = drTTTN[n]["SoHocSinhK1"].ToString() == "0" ? "" : drTTTN[n]["SoHocSinhK1"].ToString();
                        oSheet.Cells[rowTTTN+n, 5] = drTTTN[n]["SoHocSinhNuK1"].ToString() == "0" ? "" : drTTTN[n]["SoHocSinhNuK1"].ToString();
                        oSheet.Cells[rowTTTN+n, 6] = drTTTN[n]["SoHocSinhDanTocK1"].ToString() == "0" ? "" : drTTTN[n]["SoHocSinhDanTocK1"].ToString();
                        oSheet.Cells[rowTTTN+n, 7] = drTTTN[n]["SoHocSinhNuDanTocK1"].ToString() == "0" ? "" : drTTTN[n]["SoHocSinhNuDanTocK1"].ToString();
                        oSheet.Cells[rowTTTN+n, 9] = drTTTN[n]["SoHocSinhKhuyetTatK1"].ToString() == "0" ? "" : drTTTN[n]["SoHocSinhKhuyetTatK1"].ToString();

                        //Khối 2
                        oSheet.Cells[rowTTTN+n, 10] = drTTTN[n]["SoHocSinhK2"].ToString() == "0" ? "" : drTTTN[n]["SoHocSinhK2"].ToString();
                        oSheet.Cells[rowTTTN+n, 11] = drTTTN[n]["SoHocSinhNuK2"].ToString() == "0" ? "" : drTTTN[n]["SoHocSinhNuK2"].ToString();
                        oSheet.Cells[rowTTTN+n, 12] = drTTTN[n]["SoHocSinhDanTocK2"].ToString() == "0" ? "" : drTTTN[n]["SoHocSinhDanTocK2"].ToString();
                        oSheet.Cells[rowTTTN+n, 13] = drTTTN[n]["SoHocSinhNuDanTocK2"].ToString() == "0" ? "" : drTTTN[n]["SoHocSinhNuDanTocK2"].ToString();
                        oSheet.Cells[rowTTTN+n, 15] = drTTTN[n]["SoHocSinhKhuyetTatK2"].ToString() == "0" ? "" : drTTTN[n]["SoHocSinhKhuyetTatK2"].ToString();

                        //Khối 3
                        oSheet.Cells[rowTTTN+n, 16] = drTTTN[n]["SoHocSinhK3"].ToString() == "0" ? "" : drTTTN[n]["SoHocSinhK3"].ToString();
                        oSheet.Cells[rowTTTN+n, 17] = drTTTN[n]["SoHocSinhNuK3"].ToString() == "0" ? "" : drTTTN[n]["SoHocSinhNuK3"].ToString();
                        oSheet.Cells[rowTTTN+n, 18] = drTTTN[n]["SoHocSinhDanTocK3"].ToString() == "0" ? "" : drTTTN[n]["SoHocSinhDanTocK3"].ToString();
                        oSheet.Cells[rowTTTN+n, 19] = drTTTN[n]["SoHocSinhNuDanTocK3"].ToString() == "0" ? "" : drTTTN[n]["SoHocSinhNuDanTocK3"].ToString();
                        oSheet.Cells[rowTTTN+n, 21] = drTTTN[n]["SoHocSinhKhuyetTatK3"].ToString() == "0" ? "" : drTTTN[n]["SoHocSinhKhuyetTatK3"].ToString();

                        //Khối 4
                        oSheet.Cells[rowTTTN+n, 22] = drTTTN[n]["SoHocSinhK4"].ToString() == "0" ? "" : drTTTN[n]["SoHocSinhK4"].ToString();
                        oSheet.Cells[rowTTTN+n, 23] = drTTTN[n]["SoHocSinhNuK4"].ToString() == "0" ? "" : drTTTN[n]["SoHocSinhNuK4"].ToString();
                        oSheet.Cells[rowTTTN+n, 24] = drTTTN[n]["SoHocSinhDanTocK4"].ToString() == "0" ? "" : drTTTN[n]["SoHocSinhDanTocK4"].ToString();
                        oSheet.Cells[rowTTTN+n, 25] = drTTTN[n]["SoHocSinhNuDanTocK4"].ToString() == "0" ? "" : drTTTN[n]["SoHocSinhNuDanTocK4"].ToString();
                        oSheet.Cells[rowTTTN+n, 27] = drTTTN[n]["SoHocSinhKhuyetTatK4"].ToString() == "0" ? "" : drTTTN[n]["SoHocSinhKhuyetTatK4"].ToString();

                        //Khối 5
                        oSheet.Cells[rowTTTN+n, 28] = drTTTN[n]["SoHocSinhK5"].ToString() == "0" ? "" : drTTTN[n]["SoHocSinhK5"].ToString();
                        oSheet.Cells[rowTTTN+n, 29] = drTTTN[n]["SoHocSinhNuK5"].ToString() == "0" ? "" : drTTTN[n]["SoHocSinhNuK5"].ToString();
                        oSheet.Cells[rowTTTN+n, 30] = drTTTN[n]["SoHocSinhDanTocK5"].ToString() == "0" ? "" : drTTTN[n]["SoHocSinhDanTocK5"].ToString();
                        oSheet.Cells[rowTTTN+n, 31] = drTTTN[n]["SoHocSinhNuDanTocK5"].ToString() == "0" ? "" : drTTTN[n]["SoHocSinhNuDanTocK5"].ToString();
                        oSheet.Cells[rowTTTN + n, 33] = drTTTN[n]["SoHocSinhKhuyetTatK5"].ToString() == "0" ? "" : drTTTN[n]["SoHocSinhKhuyetTatK5"].ToString();
                    }
                }
                #endregion

                #region Phẩm chất tự tin, trách nhiệm
                DataRow[] drTTKL = dtPC.Select("PhanLoai='TTKL'");
                if (drTTKL != null && drTTKL.Count() > 0)
                {
                    int rowTTKL = 84;
                    for (int o = 0; o < drTTKL.Count(); o++)
                    {

                        oSheet.Cells[rowTTKL + o, 2] = drTTKL[o]["XepLoai"].ToString();
                        //Khối 1
                        oSheet.Cells[rowTTKL + o, 4] = drTTKL[o]["SoHocSinhK1"].ToString() == "0" ? "" : drTTKL[o]["SoHocSinhK1"].ToString();
                        oSheet.Cells[rowTTKL + o, 5] = drTTKL[o]["SoHocSinhNuK1"].ToString() == "0" ? "" : drTTKL[o]["SoHocSinhNuK1"].ToString();
                        oSheet.Cells[rowTTKL + o, 6] = drTTKL[o]["SoHocSinhDanTocK1"].ToString() == "0" ? "" : drTTKL[o]["SoHocSinhDanTocK1"].ToString();
                        oSheet.Cells[rowTTKL + o, 7] = drTTKL[o]["SoHocSinhNuDanTocK1"].ToString() == "0" ? "" : drTTKL[o]["SoHocSinhNuDanTocK1"].ToString();
                        oSheet.Cells[rowTTKL + o, 9] = drTTKL[o]["SoHocSinhKhuyetTatK1"].ToString() == "0" ? "" : drTTKL[o]["SoHocSinhKhuyetTatK1"].ToString();

                        //Khối 2
                        oSheet.Cells[rowTTKL + o, 10] = drTTKL[o]["SoHocSinhK2"].ToString() == "0" ? "" : drTTKL[o]["SoHocSinhK2"].ToString();
                        oSheet.Cells[rowTTKL + o, 11] = drTTKL[o]["SoHocSinhNuK2"].ToString() == "0" ? "" : drTTKL[o]["SoHocSinhNuK2"].ToString();
                        oSheet.Cells[rowTTKL + o, 12] = drTTKL[o]["SoHocSinhDanTocK2"].ToString() == "0" ? "" : drTTKL[o]["SoHocSinhDanTocK2"].ToString();
                        oSheet.Cells[rowTTKL + o, 13] = drTTKL[o]["SoHocSinhNuDanTocK2"].ToString() == "0" ? "" : drTTKL[o]["SoHocSinhNuDanTocK2"].ToString();
                        oSheet.Cells[rowTTKL + o, 15] = drTTKL[o]["SoHocSinhKhuyetTatK2"].ToString() == "0" ? "" : drTTKL[o]["SoHocSinhKhuyetTatK2"].ToString();

                        //Khối 3
                        oSheet.Cells[rowTTKL + o, 16] = drTTKL[o]["SoHocSinhK3"].ToString() == "0" ? "" : drTTKL[o]["SoHocSinhK3"].ToString();
                        oSheet.Cells[rowTTKL + o, 17] = drTTKL[o]["SoHocSinhNuK3"].ToString() == "0" ? "" : drTTKL[o]["SoHocSinhNuK3"].ToString();
                        oSheet.Cells[rowTTKL + o, 18] = drTTKL[o]["SoHocSinhDanTocK3"].ToString() == "0" ? "" : drTTKL[o]["SoHocSinhDanTocK3"].ToString();
                        oSheet.Cells[rowTTKL + o, 19] = drTTKL[o]["SoHocSinhNuDanTocK3"].ToString() == "0" ? "" : drTTKL[o]["SoHocSinhNuDanTocK3"].ToString();
                        oSheet.Cells[rowTTKL + o, 21] = drTTKL[o]["SoHocSinhKhuyetTatK3"].ToString() == "0" ? "" : drTTKL[o]["SoHocSinhKhuyetTatK3"].ToString();

                        //Khối 4
                        oSheet.Cells[rowTTKL + o, 22] = drTTKL[o]["SoHocSinhK4"].ToString() == "0" ? "" : drTTKL[o]["SoHocSinhK4"].ToString();
                        oSheet.Cells[rowTTKL + o, 23] = drTTKL[o]["SoHocSinhNuK4"].ToString() == "0" ? "" : drTTKL[o]["SoHocSinhNuK4"].ToString();
                        oSheet.Cells[rowTTKL + o, 24] = drTTKL[o]["SoHocSinhDanTocK4"].ToString() == "0" ? "" : drTTKL[o]["SoHocSinhDanTocK4"].ToString();
                        oSheet.Cells[rowTTKL + o, 25] = drTTKL[o]["SoHocSinhNuDanTocK4"].ToString() == "0" ? "" : drTTKL[o]["SoHocSinhNuDanTocK4"].ToString();
                        oSheet.Cells[rowTTKL + o, 27] = drTTKL[o]["SoHocSinhKhuyetTatK4"].ToString() == "0" ? "" : drTTKL[o]["SoHocSinhKhuyetTatK4"].ToString();

                        //Khối 5
                        oSheet.Cells[rowTTKL + o, 28] = drTTKL[o]["SoHocSinhK5"].ToString() == "0" ? "" : drTTKL[o]["SoHocSinhK5"].ToString();
                        oSheet.Cells[rowTTKL + o, 29] = drTTKL[o]["SoHocSinhNuK5"].ToString() == "0" ? "" : drTTKL[o]["SoHocSinhNuK5"].ToString();
                        oSheet.Cells[rowTTKL + o, 30] = drTTKL[o]["SoHocSinhDanTocK5"].ToString() == "0" ? "" : drTTKL[o]["SoHocSinhDanTocK5"].ToString();
                        oSheet.Cells[rowTTKL + o, 31] = drTTKL[o]["SoHocSinhNuDanTocK5"].ToString() == "0" ? "" : drTTKL[o]["SoHocSinhNuDanTocK5"].ToString();
                        oSheet.Cells[rowTTKL + o, 33] = drTTKL[o]["SoHocSinhKhuyetTatK5"].ToString() == "0" ? "" : drTTKL[o]["SoHocSinhKhuyetTatK5"].ToString();
                    }
                }
                #endregion

                #region Phẩm chất đoàn kết, yêu thương
                DataRow[] drDKYT = dtPC.Select("PhanLoai='DKYT'");
                if (drDKYT != null && drDKYT.Count() > 0)
                {
                    int rowDKYT = 88;
                    for (int p = 0; p < drDKYT.Count(); p++)
                    {

                        oSheet.Cells[rowDKYT+p, 2] = drDKYT[p]["XepLoai"].ToString();
                        //Khối 1
                        oSheet.Cells[rowDKYT+p, 4] = drDKYT[p]["SoHocSinhK1"].ToString() == "0" ? "" : drDKYT[p]["SoHocSinhK1"].ToString();
                        oSheet.Cells[rowDKYT+p, 5] = drDKYT[p]["SoHocSinhNuK1"].ToString() == "0" ? "" : drDKYT[p]["SoHocSinhNuK1"].ToString();
                        oSheet.Cells[rowDKYT+p, 6] = drDKYT[p]["SoHocSinhDanTocK1"].ToString() == "0" ? "" : drDKYT[p]["SoHocSinhDanTocK1"].ToString();
                        oSheet.Cells[rowDKYT+p, 7] = drDKYT[p]["SoHocSinhNuDanTocK1"].ToString() == "0" ? "" : drDKYT[p]["SoHocSinhNuDanTocK1"].ToString();
                        oSheet.Cells[rowDKYT+p, 9] = drDKYT[p]["SoHocSinhKhuyetTatK1"].ToString() == "0" ? "" : drDKYT[p]["SoHocSinhKhuyetTatK1"].ToString();

                        //Khối 2
                        oSheet.Cells[rowDKYT+p, 10] = drDKYT[p]["SoHocSinhK2"].ToString() == "0" ? "" : drDKYT[p]["SoHocSinhK2"].ToString();
                        oSheet.Cells[rowDKYT+p, 11] = drDKYT[p]["SoHocSinhNuK2"].ToString() == "0" ? "" : drDKYT[p]["SoHocSinhNuK2"].ToString();
                        oSheet.Cells[rowDKYT+p, 12] = drDKYT[p]["SoHocSinhDanTocK2"].ToString() == "0" ? "" : drDKYT[p]["SoHocSinhDanTocK2"].ToString();
                        oSheet.Cells[rowDKYT+p, 13] = drDKYT[p]["SoHocSinhNuDanTocK2"].ToString() == "0" ? "" : drDKYT[p]["SoHocSinhNuDanTocK2"].ToString();
                        oSheet.Cells[rowDKYT+p, 15] = drDKYT[p]["SoHocSinhKhuyetTatK2"].ToString() == "0" ? "" : drDKYT[p]["SoHocSinhKhuyetTatK2"].ToString();

                        //Khối 3
                        oSheet.Cells[rowDKYT+p, 16] = drDKYT[p]["SoHocSinhK3"].ToString() == "0" ? "" : drDKYT[p]["SoHocSinhK3"].ToString();
                        oSheet.Cells[rowDKYT+p, 17] = drDKYT[p]["SoHocSinhNuK3"].ToString() == "0" ? "" : drDKYT[p]["SoHocSinhNuK3"].ToString();
                        oSheet.Cells[rowDKYT+p, 18] = drDKYT[p]["SoHocSinhDanTocK3"].ToString() == "0" ? "" : drDKYT[p]["SoHocSinhDanTocK3"].ToString();
                        oSheet.Cells[rowDKYT+p, 19] = drDKYT[p]["SoHocSinhNuDanTocK3"].ToString() == "0" ? "" : drDKYT[p]["SoHocSinhNuDanTocK3"].ToString();
                        oSheet.Cells[rowDKYT+p, 21] = drDKYT[p]["SoHocSinhKhuyetTatK3"].ToString() == "0" ? "" : drDKYT[p]["SoHocSinhKhuyetTatK3"].ToString();

                        //Khối 4
                        oSheet.Cells[rowDKYT+p, 22] = drDKYT[p]["SoHocSinhK4"].ToString() == "0" ? "" : drDKYT[p]["SoHocSinhK4"].ToString();
                        oSheet.Cells[rowDKYT+p, 23] = drDKYT[p]["SoHocSinhNuK4"].ToString() == "0" ? "" : drDKYT[p]["SoHocSinhNuK4"].ToString();
                        oSheet.Cells[rowDKYT+p, 24] = drDKYT[p]["SoHocSinhDanTocK4"].ToString() == "0" ? "" : drDKYT[p]["SoHocSinhDanTocK4"].ToString();
                        oSheet.Cells[rowDKYT+p, 25] = drDKYT[p]["SoHocSinhNuDanTocK4"].ToString() == "0" ? "" : drDKYT[p]["SoHocSinhNuDanTocK4"].ToString();
                        oSheet.Cells[rowDKYT+p, 27] = drDKYT[p]["SoHocSinhKhuyetTatK4"].ToString() == "0" ? "" : drDKYT[p]["SoHocSinhKhuyetTatK4"].ToString();

                        //Khối 5
                        oSheet.Cells[rowDKYT+p, 28] = drDKYT[p]["SoHocSinhK5"].ToString() == "0" ? "" : drDKYT[p]["SoHocSinhK5"].ToString();
                        oSheet.Cells[rowDKYT+p, 29] = drDKYT[p]["SoHocSinhNuK5"].ToString() == "0" ? "" : drDKYT[p]["SoHocSinhNuK5"].ToString();
                        oSheet.Cells[rowDKYT+p, 30] = drDKYT[p]["SoHocSinhDanTocK5"].ToString() == "0" ? "" : drDKYT[p]["SoHocSinhDanTocK5"].ToString();
                        oSheet.Cells[rowDKYT+p, 31] = drDKYT[p]["SoHocSinhNuDanTocK5"].ToString() == "0" ? "" : drDKYT[p]["SoHocSinhNuDanTocK5"].ToString();
                        oSheet.Cells[rowDKYT + p, 33] = drDKYT[p]["SoHocSinhKhuyetTatK5"].ToString() == "0" ? "" : drDKYT[p]["SoHocSinhKhuyetTatK5"].ToString();
                    }
                }
                #endregion
                #endregion

                DataTable dtKTKT = ds.Tables[3];
                #region Khen thưởng
                DataRow[] drKT = dtKTKT.Select("XepLoai='KT'");
                if (drKT != null && drKT.Count() > 0)
                {
                    //Khối 1
                    oSheet.Cells[92, 4] = drKT[0]["TongSoK1"].ToString() == "0" ? "" : drKT[0]["TongSoK1"].ToString();
                    oSheet.Cells[92, 5] = drKT[0]["TongSoNuK1"].ToString() == "0" ? "" : drKT[0]["TongSoNuK1"].ToString();
                    oSheet.Cells[92, 6] = drKT[0]["DanTocK1"].ToString() == "0" ? "" : drKT[0]["DanTocK1"].ToString();
                    oSheet.Cells[92, 7] = drKT[0]["DanTocNuK1"].ToString() == "0" ? "" : drKT[0]["SoHocSinhNuDanTocK1"].ToString();
                    oSheet.Cells[92, 9] = drKT[0]["KhuyetTatK1"].ToString() == "0" ? "" : drKT[0]["KhuyetTatK1"].ToString();

                    //Khối 2
                    oSheet.Cells[92, 10] = drKT[0]["TongSoK2"].ToString() == "0" ? "" : drKT[0]["TongSoK2"].ToString();
                    oSheet.Cells[92, 11] = drKT[0]["TongSoNuK2"].ToString() == "0" ? "" : drKT[0]["TongSoNuK2"].ToString();
                    oSheet.Cells[92, 12] = drKT[0]["DanTocK2"].ToString() == "0" ? "" : drKT[0]["DanTocK2"].ToString();
                    oSheet.Cells[92, 13] = drKT[0]["DanTocNuK2"].ToString() == "0" ? "" : drKT[0]["DanTocNuK2"].ToString();
                    oSheet.Cells[92, 15] = drKT[0]["KhuyetTatK2"].ToString() == "0" ? "" : drKT[0]["KhuyetTatK2"].ToString();

                    //Khối 3
                    oSheet.Cells[92, 16] = drKT[0]["TongSoK3"].ToString() == "0" ? "" : drKT[0]["TongSoK3"].ToString();
                    oSheet.Cells[92, 17] = drKT[0]["TongSoNuK3"].ToString() == "0" ? "" : drKT[0]["TongSoNuK3"].ToString();
                    oSheet.Cells[92, 18] = drKT[0]["DanTocK3"].ToString() == "0" ? "" : drKT[0]["DanTocK3"].ToString();
                    oSheet.Cells[92, 19] = drKT[0]["DanTocNuK3"].ToString() == "0" ? "" : drKT[0]["DanTocNuK3"].ToString();
                    oSheet.Cells[92, 21] = drKT[0]["KhuyetTatK3"].ToString() == "0" ? "" : drKT[0]["KhuyetTatK3"].ToString();

                    //Khối 4
                    oSheet.Cells[92, 22] = drKT[0]["TongSoK4"].ToString() == "0" ? "" : drKT[0]["TongSoK4"].ToString();
                    oSheet.Cells[92, 23] = drKT[0]["TongSoNuK4"].ToString() == "0" ? "" : drKT[0]["TongSoNuK4"].ToString();
                    oSheet.Cells[92, 24] = drKT[0]["DanTocK4"].ToString() == "0" ? "" : drKT[0]["DanTocK4"].ToString();
                    oSheet.Cells[92, 25] = drKT[0]["DanTocNuK4"].ToString() == "0" ? "" : drKT[0]["DanTocNuK4"].ToString();
                    oSheet.Cells[92, 27] = drKT[0]["KhuyetTatK4"].ToString() == "0" ? "" : drKT[0]["KhuyetTatK4"].ToString();

                    //Khối 5
                    oSheet.Cells[92, 28] = drKT[0]["TongSoK5"].ToString() == "0" ? "" : drKT[0]["TongSoK5"].ToString();
                    oSheet.Cells[92, 29] = drKT[0]["TongSoNuK5"].ToString() == "0" ? "" : drKT[0]["TongSoNuK5"].ToString();
                    oSheet.Cells[92, 30] = drKT[0]["DanTocK5"].ToString() == "0" ? "" : drKT[0]["DanTocK5"].ToString();
                    oSheet.Cells[92, 31] = drKT[0]["DanTocNuK5"].ToString() == "0" ? "" : drKT[0]["DanTocNuK5"].ToString();
                    oSheet.Cells[92, 33] = drKT[0]["KhuyetTatK5"].ToString() == "0" ? "" : drKT[0]["KhuyetTatK5"].ToString();
                }
                #endregion

                #region Khuyết tật không đánh giá
                DataRow[] drKTKDG = dtKTKT.Select("XepLoai='KTKDG'");
                if (drKTKDG != null && drKTKDG.Count() > 0)
                {
                    //Khối 1
                    oSheet.Cells[98, 4] = drKTKDG[0]["TongSoK1"].ToString() == "0" ? "" : drKTKDG[0]["TongSoK1"].ToString();
                    oSheet.Cells[98, 5] = drKTKDG[0]["TongSoNuK1"].ToString() == "0" ? "" : drKTKDG[0]["TongSoNuK1"].ToString();
                    oSheet.Cells[98, 6] = drKTKDG[0]["DanTocK1"].ToString() == "0" ? "" : drKTKDG[0]["DanTocK1"].ToString();
                    oSheet.Cells[98, 7] = drKTKDG[0]["DanTocNuK1"].ToString() == "0" ? "" : drKTKDG[0]["SoHocSinhNuDanTocK1"].ToString();
                    oSheet.Cells[98, 9] = drKTKDG[0]["KhuyetTatK1"].ToString() == "0" ? "" : drKTKDG[0]["KhuyetTatK1"].ToString();

                    //Khối 2
                    oSheet.Cells[98, 10] = drKTKDG[0]["TongSoK2"].ToString() == "0" ? "" : drKTKDG[0]["TongSoK2"].ToString();
                    oSheet.Cells[98, 11] = drKTKDG[0]["TongSoNuK2"].ToString() == "0" ? "" : drKTKDG[0]["TongSoNuK2"].ToString();
                    oSheet.Cells[98, 12] = drKTKDG[0]["DanTocK2"].ToString() == "0" ? "" : drKTKDG[0]["DanTocK2"].ToString();
                    oSheet.Cells[98, 13] = drKTKDG[0]["DanTocNuK2"].ToString() == "0" ? "" : drKTKDG[0]["DanTocNuK2"].ToString();
                    oSheet.Cells[98, 15] = drKTKDG[0]["KhuyetTatK2"].ToString() == "0" ? "" : drKTKDG[0]["KhuyetTatK2"].ToString();

                    //Khối 3
                    oSheet.Cells[98, 16] = drKTKDG[0]["TongSoK3"].ToString() == "0" ? "" : drKTKDG[0]["TongSoK3"].ToString();
                    oSheet.Cells[98, 17] = drKTKDG[0]["TongSoNuK3"].ToString() == "0" ? "" : drKTKDG[0]["TongSoNuK3"].ToString();
                    oSheet.Cells[98, 18] = drKTKDG[0]["DanTocK3"].ToString() == "0" ? "" : drKTKDG[0]["DanTocK3"].ToString();
                    oSheet.Cells[98, 19] = drKTKDG[0]["DanTocNuK3"].ToString() == "0" ? "" : drKTKDG[0]["DanTocNuK3"].ToString();
                    oSheet.Cells[98, 21] = drKTKDG[0]["KhuyetTatK3"].ToString() == "0" ? "" : drKTKDG[0]["KhuyetTatK3"].ToString();

                    //Khối 4
                    oSheet.Cells[98, 22] = drKTKDG[0]["TongSoK4"].ToString() == "0" ? "" : drKTKDG[0]["TongSoK4"].ToString();
                    oSheet.Cells[98, 23] = drKTKDG[0]["TongSoNuK4"].ToString() == "0" ? "" : drKTKDG[0]["TongSoNuK4"].ToString();
                    oSheet.Cells[98, 24] = drKTKDG[0]["DanTocK4"].ToString() == "0" ? "" : drKTKDG[0]["DanTocK4"].ToString();
                    oSheet.Cells[98, 25] = drKTKDG[0]["DanTocNuK4"].ToString() == "0" ? "" : drKTKDG[0]["DanTocNuK4"].ToString();
                    oSheet.Cells[98, 27] = drKTKDG[0]["KhuyetTatK4"].ToString() == "0" ? "" : drKTKDG[0]["KhuyetTatK4"].ToString();

                    //Khối 5
                    oSheet.Cells[98, 28] = drKTKDG[0]["TongSoK5"].ToString() == "0" ? "" : drKTKDG[0]["TongSoK5"].ToString();
                    oSheet.Cells[98, 29] = drKTKDG[0]["TongSoNuK5"].ToString() == "0" ? "" : drKTKDG[0]["TongSoNuK5"].ToString();
                    oSheet.Cells[98, 30] = drKTKDG[0]["DanTocK5"].ToString() == "0" ? "" : drKTKDG[0]["DanTocK5"].ToString();
                    oSheet.Cells[98, 31] = drKTKDG[0]["DanTocNuK5"].ToString() == "0" ? "" : drKTKDG[0]["DanTocNuK5"].ToString();
                    oSheet.Cells[98, 33] = drKTKDG[0]["KhuyetTatK5"].ToString() == "0" ? "" : drKTKDG[0]["KhuyetTatK5"].ToString();
                }
                #endregion

                oWB.Save();
                oWB.Close();

                json = newfile;
            }
            else
            {
                json = "NoData";
            }
            return Json(json, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ThongKeDiemKiemTraExcel()
        {
           string json = "";
            if(Session["TongHopDiem"]!=null)
            {
                string userName = getUserName();

                int idTruong = Cmon.getIdtruong(userName, db);
                string CapQuanLy = Cmon.getDonViChuQuan(idTruong);
                string TenTruong = Cmon.GetnameSchool(idTruong);
                string MaHocKy = Session["MaHocKy"].ToString();
                string TenHocKy = "";
                if (MaHocKy == "K1" || MaHocKy == "HK1") TenHocKy = "HỌC KỲ I";
                else if (MaHocKy == "GK1" || MaHocKy == "GHK1") TenHocKy = "GIỮA HỌC KỲ I";
                else if (MaHocKy == "GK2" || MaHocKy == "GHK2") TenHocKy = "GIỮA HỌC KỲ II";
                else TenHocKy = "CUỐI NĂM";
                string file = "THONGKEDIEMKIEMTRA.xls";
                string templateFile= Path.Combine(Server.MapPath("~/templateExcell"), file);
                FileInfo f = new FileInfo(templateFile);
                string newfile = "THONGKEDIEMKIEMTRA_" + userName + "_" + MaHocKy+"_" + DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString()+DateTime.Now.Hour.ToString()+DateTime.Now.Minute.ToString()+DateTime.Now.Second.ToString() + ".xls";
                
                string filename = Path.Combine(Server.MapPath("~/ProcessTemplate"), newfile);
                f.CopyTo(filename);

                DataTable dt = (DataTable)Session["TongHopDiem"];
                Microsoft.Office.Interop.Excel.Application oXL = null;
                Microsoft.Office.Interop.Excel._Workbook oWB = null;
                Microsoft.Office.Interop.Excel._Worksheet oSheet = null;

                oXL = new Microsoft.Office.Interop.Excel.Application();
                oWB = oXL.Workbooks.Open(filename);
                oSheet = String.IsNullOrEmpty("Sheet1") ? (Microsoft.Office.Interop.Excel._Worksheet)oWB.ActiveSheet : (Microsoft.Office.Interop.Excel._Worksheet)oWB.Worksheets["Sheet1"];
                oSheet.Cells[3, 2] = CapQuanLy;
                oSheet.Cells[4, 2] = TenTruong;

                oSheet.Cells[3, 10] = "THỐNG KÊ ĐIỂM KIỂM TRA "+ TenHocKy;
                oSheet.Cells[4, 10] = "Năm học: "+ Session["NamHoc"].ToString();
                int row = 17;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (i==0)
                    {
                        oSheet.Cells[9, 2] = "1. "+dt.Rows[i]["TenMonHoc"].ToString();
                        oSheet.Cells[10, 2] = "Điểm 10";
                        oSheet.Cells[11, 2] = "Điểm 9";
                        oSheet.Cells[12, 2] = "Điểm 8";
                        oSheet.Cells[13, 2] = "Điểm 7";
                        oSheet.Cells[14, 2] = "Điểm 6";
                        oSheet.Cells[15, 2] = "Điểm 5";
                        oSheet.Cells[16, 2] = "Điểm dưới 5";

                        //Khối 1 - Điểm 10
                        oSheet.Cells[10, 4] = dt.Rows[i]["SoHocSinhK1Diem10"].ToString()=="0"?"": dt.Rows[i]["SoHocSinhK1Diem10"].ToString();
                        oSheet.Cells[10, 5] = dt.Rows[i]["SoHocSinhNuK1Diem10"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK1Diem10"].ToString();
                        oSheet.Cells[10, 6] = dt.Rows[i]["SoHocSinhDanTocK1Diem10"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK1Diem10"].ToString();
                        oSheet.Cells[10, 7] = dt.Rows[i]["SoHocSinhNuDanTocK1Diem10"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK1Diem10"].ToString();
                        oSheet.Cells[10, 9] = dt.Rows[i]["SoHocSinhKhuyetTatK1Diem10"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK1Diem10"].ToString();
                        //Hết Khối 1 - Điểm 10

                        //Khối 2 - Điểm 10
                        oSheet.Cells[10, 10] = dt.Rows[i]["SoHocSinhK2Diem10"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhK2Diem10"].ToString();
                        oSheet.Cells[10, 11] = dt.Rows[i]["SoHocSinhNuK2Diem10"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK2Diem10"].ToString();
                        oSheet.Cells[10, 12] = dt.Rows[i]["SoHocSinhDanTocK2Diem10"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK2Diem10"].ToString();
                        oSheet.Cells[10, 13] = dt.Rows[i]["SoHocSinhNuDanTocK2Diem10"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK2Diem10"].ToString();
                        oSheet.Cells[10, 15] = dt.Rows[i]["SoHocSinhKhuyetTatK2Diem10"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK2Diem10"].ToString();
                        //Hết Khối 2 - Điểm 10

                        //Khối 3 - Điểm 10
                        oSheet.Cells[10, 16] = dt.Rows[i]["SoHocSinhK3Diem10"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhK3Diem10"].ToString();
                        oSheet.Cells[10, 17] = dt.Rows[i]["SoHocSinhNuK3Diem10"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK3Diem10"].ToString();
                        oSheet.Cells[10, 18] = dt.Rows[i]["SoHocSinhDanTocK3Diem10"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK3Diem10"].ToString();
                        oSheet.Cells[10, 19] = dt.Rows[i]["SoHocSinhNuDanTocK3Diem10"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK3Diem10"].ToString();
                        oSheet.Cells[10, 21] = dt.Rows[i]["SoHocSinhKhuyetTatK3Diem10"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK3Diem10"].ToString();
                        //Hết Khối 3 - Điểm 10

                        //Khối 4 - Điểm 10
                        oSheet.Cells[10, 22] = dt.Rows[i]["SoHocSinhK4Diem10"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhK4Diem10"].ToString();
                        oSheet.Cells[10, 23] = dt.Rows[i]["SoHocSinhNuK4Diem10"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK4Diem10"].ToString();
                        oSheet.Cells[10, 24] = dt.Rows[i]["SoHocSinhDanTocK4Diem10"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK4Diem10"].ToString();
                        oSheet.Cells[10, 25] = dt.Rows[i]["SoHocSinhNuDanTocK4Diem10"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK4Diem10"].ToString();
                        oSheet.Cells[10, 27] = dt.Rows[i]["SoHocSinhKhuyetTatK4Diem10"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK4Diem10"].ToString();
                        //Hết Khối 4 - Điểm 10

                        //Khối 5 - Điểm 10
                        oSheet.Cells[10, 28] = dt.Rows[i]["SoHocSinhK5Diem10"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhK5Diem10"].ToString();
                        oSheet.Cells[10, 29] = dt.Rows[i]["SoHocSinhNuK5Diem10"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK5Diem10"].ToString();
                        oSheet.Cells[10, 30] = dt.Rows[i]["SoHocSinhDanTocK5Diem10"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK5Diem10"].ToString();
                        oSheet.Cells[10, 31] = dt.Rows[i]["SoHocSinhNuDanTocK5Diem10"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK5Diem10"].ToString();
                        oSheet.Cells[10, 33] = dt.Rows[i]["SoHocSinhKhuyetTatK5Diem10"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK5Diem10"].ToString();
                        //Hết Khối 5 - Điểm 10

                        //Khối 1 - Điểm 9
                        oSheet.Cells[11, 4] = dt.Rows[i]["SoHocSinhK1Diem9"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhK1Diem9"].ToString();
                        oSheet.Cells[11, 5] = dt.Rows[i]["SoHocSinhNuK1Diem9"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK1Diem9"].ToString();
                        oSheet.Cells[11, 6] = dt.Rows[i]["SoHocSinhDanTocK1Diem9"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK1Diem9"].ToString();
                        oSheet.Cells[11, 7] = dt.Rows[i]["SoHocSinhNuDanTocK1Diem9"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK1Diem9"].ToString();
                        oSheet.Cells[11, 9] = dt.Rows[i]["SoHocSinhKhuyetTatK1Diem9"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK1Diem9"].ToString();
                        //Hết Khối 1 - Điểm 9

                        //Khối 2 - Điểm 9
                        oSheet.Cells[11, 10] = dt.Rows[i]["SoHocSinhK2Diem9"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhK2Diem9"].ToString();
                        oSheet.Cells[11, 11] = dt.Rows[i]["SoHocSinhNuK2Diem9"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK2Diem9"].ToString();
                        oSheet.Cells[11, 12] = dt.Rows[i]["SoHocSinhDanTocK2Diem9"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK2Diem9"].ToString();
                        oSheet.Cells[11, 13] = dt.Rows[i]["SoHocSinhNuDanTocK2Diem9"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK2Diem9"].ToString();
                        oSheet.Cells[11, 15] = dt.Rows[i]["SoHocSinhKhuyetTatK2Diem9"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK2Diem9"].ToString();
                        //Hết Khối 2 - Điểm 9

                        //Khối 3 - Điểm 9
                        oSheet.Cells[11, 16] = dt.Rows[i]["SoHocSinhK3Diem9"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhK3Diem9"].ToString();
                        oSheet.Cells[11, 17] = dt.Rows[i]["SoHocSinhNuK3Diem9"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK3Diem9"].ToString();
                        oSheet.Cells[11, 18] = dt.Rows[i]["SoHocSinhDanTocK3Diem9"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK3Diem9"].ToString();
                        oSheet.Cells[11, 19] = dt.Rows[i]["SoHocSinhNuDanTocK3Diem9"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK3Diem9"].ToString();
                        oSheet.Cells[11, 21] = dt.Rows[i]["SoHocSinhKhuyetTatK3Diem9"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK3Diem9"].ToString();
                        //Hết Khối 3 - Điểm 9

                        //Khối 4 - Điểm 9
                        oSheet.Cells[11, 22] = dt.Rows[i]["SoHocSinhK4Diem9"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhK4Diem9"].ToString();
                        oSheet.Cells[11, 23] = dt.Rows[i]["SoHocSinhNuK4Diem9"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK4Diem9"].ToString();
                        oSheet.Cells[11, 24] = dt.Rows[i]["SoHocSinhDanTocK4Diem9"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK4Diem9"].ToString();
                        oSheet.Cells[11, 25] = dt.Rows[i]["SoHocSinhNuDanTocK4Diem9"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK4Diem9"].ToString();
                        oSheet.Cells[11, 27] = dt.Rows[i]["SoHocSinhKhuyetTatK4Diem9"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK4Diem9"].ToString();
                        //Hết Khối 4 - Điểm 9

                        //Khối 5 - Điểm 9
                        oSheet.Cells[11, 28] = dt.Rows[i]["SoHocSinhK5Diem9"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhK5Diem9"].ToString();
                        oSheet.Cells[11, 29] = dt.Rows[i]["SoHocSinhNuK5Diem9"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK5Diem9"].ToString();
                        oSheet.Cells[11, 30] = dt.Rows[i]["SoHocSinhDanTocK5Diem9"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK5Diem9"].ToString();
                        oSheet.Cells[11, 31] = dt.Rows[i]["SoHocSinhNuDanTocK5Diem9"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK5Diem9"].ToString();
                        oSheet.Cells[11, 33] = dt.Rows[i]["SoHocSinhKhuyetTatK5Diem9"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK5Diem9"].ToString();
                        //Hết Khối 5 - Điểm 9

                        //Khối 1 - Điểm 8
                        oSheet.Cells[12, 4] = dt.Rows[i]["SoHocSinhK1Diem8"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhK1Diem8"].ToString();
                        oSheet.Cells[12, 5] = dt.Rows[i]["SoHocSinhNuK1Diem8"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK1Diem8"].ToString();
                        oSheet.Cells[12, 6] = dt.Rows[i]["SoHocSinhDanTocK1Diem8"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK1Diem8"].ToString();
                        oSheet.Cells[12, 7] = dt.Rows[i]["SoHocSinhNuDanTocK1Diem8"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK1Diem8"].ToString();
                        oSheet.Cells[12, 9] = dt.Rows[i]["SoHocSinhKhuyetTatK1Diem8"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK1Diem8"].ToString();
                        //Hết Khối 1 - Điểm 8

                        //Khối 2 - Điểm 8
                        oSheet.Cells[12, 10] = dt.Rows[i]["SoHocSinhK2Diem8"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhK2Diem8"].ToString();
                        oSheet.Cells[12, 11] = dt.Rows[i]["SoHocSinhNuK2Diem8"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK2Diem8"].ToString();
                        oSheet.Cells[12, 12] = dt.Rows[i]["SoHocSinhDanTocK2Diem8"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK2Diem8"].ToString();
                        oSheet.Cells[12, 13] = dt.Rows[i]["SoHocSinhNuDanTocK2Diem8"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK2Diem8"].ToString();
                        oSheet.Cells[12, 15] = dt.Rows[i]["SoHocSinhKhuyetTatK2Diem8"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK2Diem8"].ToString();
                        //Hết Khối 2 - Điểm 8

                        //Khối 3 - Điểm 8
                        oSheet.Cells[12, 16] = dt.Rows[i]["SoHocSinhK3Diem8"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhK3Diem8"].ToString();
                        oSheet.Cells[12, 17] = dt.Rows[i]["SoHocSinhNuK3Diem8"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK3Diem8"].ToString();
                        oSheet.Cells[12, 18] = dt.Rows[i]["SoHocSinhDanTocK3Diem8"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK3Diem8"].ToString();
                        oSheet.Cells[12, 19] = dt.Rows[i]["SoHocSinhNuDanTocK3Diem8"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK3Diem8"].ToString();
                        oSheet.Cells[12, 21] = dt.Rows[i]["SoHocSinhKhuyetTatK3Diem8"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK3Diem8"].ToString();
                        //Hết Khối 3 - Điểm 8

                        //Khối 4 - Điểm 8
                        oSheet.Cells[12, 22] = dt.Rows[i]["SoHocSinhK4Diem8"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhK4Diem8"].ToString();
                        oSheet.Cells[12, 23] = dt.Rows[i]["SoHocSinhNuK4Diem8"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK4Diem8"].ToString();
                        oSheet.Cells[12, 24] = dt.Rows[i]["SoHocSinhDanTocK4Diem8"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK4Diem8"].ToString();
                        oSheet.Cells[12, 25] = dt.Rows[i]["SoHocSinhNuDanTocK4Diem8"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK4Diem8"].ToString();
                        oSheet.Cells[12, 27] = dt.Rows[i]["SoHocSinhKhuyetTatK4Diem8"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK4Diem8"].ToString();
                        //Hết Khối 4 - Điểm 8

                        //Khối 5 - Điểm 8
                        oSheet.Cells[12, 28] = dt.Rows[i]["SoHocSinhK5Diem8"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhK5Diem8"].ToString();
                        oSheet.Cells[12, 29] = dt.Rows[i]["SoHocSinhNuK5Diem8"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK5Diem8"].ToString();
                        oSheet.Cells[12, 30] = dt.Rows[i]["SoHocSinhDanTocK5Diem8"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK5Diem8"].ToString();
                        oSheet.Cells[12, 31] = dt.Rows[i]["SoHocSinhNuDanTocK5Diem8"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK5Diem8"].ToString();
                        oSheet.Cells[12, 33] = dt.Rows[i]["SoHocSinhKhuyetTatK5Diem8"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK5Diem8"].ToString();
                        //Hết Khối 5 - Điểm 8

                        //Khối 1 - Điểm 7
                        oSheet.Cells[13, 4] = dt.Rows[i]["SoHocSinhK1Diem7"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhK1Diem7"].ToString();
                        oSheet.Cells[13, 5] = dt.Rows[i]["SoHocSinhNuK1Diem7"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK1Diem7"].ToString();
                        oSheet.Cells[13, 6] = dt.Rows[i]["SoHocSinhDanTocK1Diem7"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK1Diem7"].ToString();
                        oSheet.Cells[13, 7] = dt.Rows[i]["SoHocSinhNuDanTocK1Diem7"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK1Diem7"].ToString();
                        oSheet.Cells[13, 9] = dt.Rows[i]["SoHocSinhKhuyetTatK1Diem7"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK1Diem7"].ToString();
                        //Hết Khối 1 - Điểm 7

                        //Khối 2 - Điểm 7
                        oSheet.Cells[13, 10] = dt.Rows[i]["SoHocSinhK2Diem7"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhK2Diem7"].ToString();
                        oSheet.Cells[13, 11] = dt.Rows[i]["SoHocSinhNuK2Diem7"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK2Diem7"].ToString();
                        oSheet.Cells[13, 12] = dt.Rows[i]["SoHocSinhDanTocK2Diem7"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK2Diem7"].ToString();
                        oSheet.Cells[13, 13] = dt.Rows[i]["SoHocSinhNuDanTocK2Diem7"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK2Diem7"].ToString();
                        oSheet.Cells[13, 15] = dt.Rows[i]["SoHocSinhKhuyetTatK2Diem7"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK2Diem7"].ToString();
                        //Hết Khối 2 - Điểm 7

                        //Khối 3 - Điểm 7
                        oSheet.Cells[13, 16] = dt.Rows[i]["SoHocSinhK3Diem7"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhK3Diem7"].ToString();
                        oSheet.Cells[13, 17] = dt.Rows[i]["SoHocSinhNuK3Diem7"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK3Diem7"].ToString();
                        oSheet.Cells[13, 18] = dt.Rows[i]["SoHocSinhDanTocK3Diem7"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK3Diem7"].ToString();
                        oSheet.Cells[13, 19] = dt.Rows[i]["SoHocSinhNuDanTocK3Diem7"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK3Diem7"].ToString();
                        oSheet.Cells[13, 21] = dt.Rows[i]["SoHocSinhKhuyetTatK3Diem7"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK3Diem7"].ToString();
                        //Hết Khối 3 - Điểm 7

                        //Khối 4 - Điểm 7
                        oSheet.Cells[13, 22] = dt.Rows[i]["SoHocSinhK4Diem7"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhK4Diem7"].ToString();
                        oSheet.Cells[13, 23] = dt.Rows[i]["SoHocSinhNuK4Diem7"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK4Diem7"].ToString();
                        oSheet.Cells[13, 24] = dt.Rows[i]["SoHocSinhDanTocK4Diem7"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK4Diem7"].ToString();
                        oSheet.Cells[13, 25] = dt.Rows[i]["SoHocSinhNuDanTocK4Diem7"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK4Diem7"].ToString();
                        oSheet.Cells[13, 27] = dt.Rows[i]["SoHocSinhKhuyetTatK4Diem7"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK4Diem7"].ToString();
                        //Hết Khối 4 - Điểm 7

                        //Khối 5 - Điểm 7
                        oSheet.Cells[13, 28] = dt.Rows[i]["SoHocSinhK5Diem7"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhK5Diem7"].ToString();
                        oSheet.Cells[13, 29] = dt.Rows[i]["SoHocSinhNuK5Diem7"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK5Diem7"].ToString();
                        oSheet.Cells[13, 30] = dt.Rows[i]["SoHocSinhDanTocK5Diem7"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK5Diem7"].ToString();
                        oSheet.Cells[13, 31] = dt.Rows[i]["SoHocSinhNuDanTocK5Diem7"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK5Diem7"].ToString();
                        oSheet.Cells[13, 33] = dt.Rows[i]["SoHocSinhKhuyetTatK5Diem7"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK5Diem7"].ToString();
                        //Hết Khối 5 - Điểm 7

                        //Khối 1 - Điểm 6
                        oSheet.Cells[14, 4] = dt.Rows[i]["SoHocSinhK1Diem6"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhK1Diem6"].ToString();
                        oSheet.Cells[14, 5] = dt.Rows[i]["SoHocSinhNuK1Diem6"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK1Diem6"].ToString();
                        oSheet.Cells[14, 6] = dt.Rows[i]["SoHocSinhDanTocK1Diem6"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK1Diem6"].ToString();
                        oSheet.Cells[14, 7] = dt.Rows[i]["SoHocSinhNuDanTocK1Diem6"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK1Diem6"].ToString();
                        oSheet.Cells[14, 9] = dt.Rows[i]["SoHocSinhKhuyetTatK1Diem6"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK1Diem6"].ToString();
                        //Hết Khối 1 - Điểm 6

                        //Khối 2 - Điểm 6
                        oSheet.Cells[14, 10] = dt.Rows[i]["SoHocSinhK2Diem6"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhK2Diem6"].ToString();
                        oSheet.Cells[14, 11] = dt.Rows[i]["SoHocSinhNuK2Diem6"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK2Diem6"].ToString();
                        oSheet.Cells[14, 12] = dt.Rows[i]["SoHocSinhDanTocK2Diem6"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK2Diem6"].ToString();
                        oSheet.Cells[14, 13] = dt.Rows[i]["SoHocSinhNuDanTocK2Diem6"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK2Diem6"].ToString();
                        oSheet.Cells[14, 15] = dt.Rows[i]["SoHocSinhKhuyetTatK2Diem6"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK2Diem6"].ToString();
                        //Hết Khối 2 - Điểm 6

                        //Khối 3 - Điểm 6
                        oSheet.Cells[14, 16] = dt.Rows[i]["SoHocSinhK3Diem6"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhK3Diem6"].ToString();
                        oSheet.Cells[14, 17] = dt.Rows[i]["SoHocSinhNuK3Diem6"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK3Diem6"].ToString();
                        oSheet.Cells[14, 18] = dt.Rows[i]["SoHocSinhDanTocK3Diem6"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK3Diem6"].ToString();
                        oSheet.Cells[14, 19] = dt.Rows[i]["SoHocSinhNuDanTocK3Diem6"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK3Diem6"].ToString();
                        oSheet.Cells[14, 21] = dt.Rows[i]["SoHocSinhKhuyetTatK3Diem6"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK3Diem6"].ToString();
                        //Hết Khối 3 - Điểm 6

                        //Khối 4 - Điểm 6
                        oSheet.Cells[14, 22] = dt.Rows[i]["SoHocSinhK4Diem6"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhK4Diem6"].ToString();
                        oSheet.Cells[14, 23] = dt.Rows[i]["SoHocSinhNuK4Diem6"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK4Diem6"].ToString();
                        oSheet.Cells[14, 24] = dt.Rows[i]["SoHocSinhDanTocK4Diem6"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK4Diem6"].ToString();
                        oSheet.Cells[14, 25] = dt.Rows[i]["SoHocSinhNuDanTocK4Diem6"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK4Diem6"].ToString();
                        oSheet.Cells[14, 27] = dt.Rows[i]["SoHocSinhKhuyetTatK4Diem6"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK4Diem6"].ToString();
                        //Hết Khối 4 - Điểm 6

                        //Khối 5 - Điểm 6
                        oSheet.Cells[14, 28] = dt.Rows[i]["SoHocSinhK5Diem6"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhK5Diem6"].ToString();
                        oSheet.Cells[14, 29] = dt.Rows[i]["SoHocSinhNuK5Diem6"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK5Diem6"].ToString();
                        oSheet.Cells[14, 30] = dt.Rows[i]["SoHocSinhDanTocK5Diem6"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK5Diem6"].ToString();
                        oSheet.Cells[14, 31] = dt.Rows[i]["SoHocSinhNuDanTocK5Diem6"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK5Diem6"].ToString();
                        oSheet.Cells[14, 33] = dt.Rows[i]["SoHocSinhKhuyetTatK5Diem6"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK5Diem6"].ToString();
                        //Hết Khối 5 - Điểm 6

                        //Khối 1 - Điểm 5
                        oSheet.Cells[15, 4] = dt.Rows[i]["SoHocSinhK1Diem5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhK1Diem5"].ToString();
                        oSheet.Cells[15, 5] = dt.Rows[i]["SoHocSinhNuK1Diem5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK1Diem5"].ToString();
                        oSheet.Cells[15, 6] = dt.Rows[i]["SoHocSinhDanTocK1Diem5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK1Diem5"].ToString();
                        oSheet.Cells[15, 7] = dt.Rows[i]["SoHocSinhNuDanTocK1Diem5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK1Diem5"].ToString();
                        oSheet.Cells[15, 9] = dt.Rows[i]["SoHocSinhKhuyetTatK1Diem5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK1Diem5"].ToString();
                        //Hết Khối 1 - Điểm 5

                        //Khối 2 - Điểm 5
                        oSheet.Cells[15, 10] = dt.Rows[i]["SoHocSinhK2Diem5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhK2Diem5"].ToString();
                        oSheet.Cells[15, 11] = dt.Rows[i]["SoHocSinhNuK2Diem5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK2Diem5"].ToString();
                        oSheet.Cells[15, 12] = dt.Rows[i]["SoHocSinhDanTocK2Diem5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK2Diem5"].ToString();
                        oSheet.Cells[15, 13] = dt.Rows[i]["SoHocSinhNuDanTocK2Diem5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK2Diem5"].ToString();
                        oSheet.Cells[15, 15] = dt.Rows[i]["SoHocSinhKhuyetTatK2Diem5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK2Diem5"].ToString();
                        //Hết Khối 2 - Điểm 5

                        //Khối 3 - Điểm 5
                        oSheet.Cells[15, 16] = dt.Rows[i]["SoHocSinhK3Diem5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhK3Diem5"].ToString();
                        oSheet.Cells[15, 17] = dt.Rows[i]["SoHocSinhNuK3Diem5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK3Diem5"].ToString();
                        oSheet.Cells[15, 18] = dt.Rows[i]["SoHocSinhDanTocK3Diem5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK3Diem5"].ToString();
                        oSheet.Cells[15, 19] = dt.Rows[i]["SoHocSinhNuDanTocK3Diem5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK3Diem5"].ToString();
                        oSheet.Cells[15, 21] = dt.Rows[i]["SoHocSinhKhuyetTatK3Diem5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK3Diem5"].ToString();
                        //Hết Khối 3 - Điểm 5

                        //Khối 4 - Điểm 5
                        oSheet.Cells[15, 22] = dt.Rows[i]["SoHocSinhK4Diem5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhK4Diem5"].ToString();
                        oSheet.Cells[15, 23] = dt.Rows[i]["SoHocSinhNuK4Diem5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK4Diem5"].ToString();
                        oSheet.Cells[15, 24] = dt.Rows[i]["SoHocSinhDanTocK4Diem5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK4Diem5"].ToString();
                        oSheet.Cells[15, 25] = dt.Rows[i]["SoHocSinhNuDanTocK4Diem5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK4Diem5"].ToString();
                        oSheet.Cells[15, 27] = dt.Rows[i]["SoHocSinhKhuyetTatK4Diem5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK4Diem5"].ToString();
                        //Hết Khối 4 - Điểm 5

                        //Khối 5 - Điểm 5
                        oSheet.Cells[15, 28] = dt.Rows[i]["SoHocSinhK5Diem5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhK5Diem5"].ToString();
                        oSheet.Cells[15, 29] = dt.Rows[i]["SoHocSinhNuK5Diem5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK5Diem5"].ToString();
                        oSheet.Cells[15, 30] = dt.Rows[i]["SoHocSinhDanTocK5Diem5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK5Diem5"].ToString();
                        oSheet.Cells[15, 31] = dt.Rows[i]["SoHocSinhNuDanTocK5Diem5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK5Diem5"].ToString();
                        oSheet.Cells[15, 33] = dt.Rows[i]["SoHocSinhKhuyetTatK5Diem5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK5Diem5"].ToString();
                        //Hết Khối 5 - Điểm 5

                        //Khối 1 - Điểm dưới 5
                        oSheet.Cells[16, 4] = dt.Rows[i]["SoHocSinhK1DiemDuoi5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhK1DiemDuoi5"].ToString();
                        oSheet.Cells[16, 5] = dt.Rows[i]["SoHocSinhNuK1DiemDuoi5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK1DiemDuoi5"].ToString();
                        oSheet.Cells[16, 6] = dt.Rows[i]["SoHocSinhDanTocK1DiemDuoi5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK1DiemDuoi5"].ToString();
                        oSheet.Cells[16, 7] = dt.Rows[i]["SoHocSinhNuDanTocK1DiemDuoi5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK1DiemDuoi5"].ToString();
                        oSheet.Cells[16, 9] = dt.Rows[i]["SoHocSinhKhuyetTatK1DiemDuoi5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK1DiemDuoi5"].ToString();
                        //Hết Khối 1 - Điểm dưới 5

                        //Khối 2 - Điểm dưới 5
                        oSheet.Cells[16, 10] = dt.Rows[i]["SoHocSinhK2Diem5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhK2DiemDuoi5"].ToString();
                        oSheet.Cells[16, 11] = dt.Rows[i]["SoHocSinhNuK2Diem5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK2DiemDuoi5"].ToString();
                        oSheet.Cells[16, 12] = dt.Rows[i]["SoHocSinhDanTocK2Diem5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK2DiemDuoi5"].ToString();
                        oSheet.Cells[16, 13] = dt.Rows[i]["SoHocSinhNuDanTocK2Diem5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK2DiemDuoi5"].ToString();
                        oSheet.Cells[16, 15] = dt.Rows[i]["SoHocSinhKhuyetTatK2Diem5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK2Diem5"].ToString();
                        //Hết Khối 2 - Điểm dưới 5

                        //Khối 3 - Điểm 5
                        oSheet.Cells[15, 16] = dt.Rows[i]["SoHocSinhK3DiemDuoi5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhK3DiemDuoi5"].ToString();
                        oSheet.Cells[15, 17] = dt.Rows[i]["SoHocSinhNuK3DiemDuoi5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK3DiemDuoi5"].ToString();
                        oSheet.Cells[15, 18] = dt.Rows[i]["SoHocSinhDanTocK3DiemDuoi5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK3DiemDuoi5"].ToString();
                        oSheet.Cells[15, 19] = dt.Rows[i]["SoHocSinhNuDanTocK3DiemDuoi5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK3DiemDuoi5"].ToString();
                        oSheet.Cells[15, 21] = dt.Rows[i]["SoHocSinhKhuyetTatK3DiemDuoi5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK3DiemDuoi5"].ToString();
                        //Hết Khối 3 - Điểm dưới 5

                        //Khối 4 - Điểm dưới 5
                        oSheet.Cells[16, 22] = dt.Rows[i]["SoHocSinhK4DiemDuoi5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhK4DiemDuoi5"].ToString();
                        oSheet.Cells[16, 23] = dt.Rows[i]["SoHocSinhNuK4DiemDuoi5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK4DiemDuoi5"].ToString();
                        oSheet.Cells[16, 24] = dt.Rows[i]["SoHocSinhDanTocK4DiemDuoi5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK4DiemDuoi5"].ToString();
                        oSheet.Cells[16, 25] = dt.Rows[i]["SoHocSinhNuDanTocK4DiemDuoi5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK4DiemDuoi5"].ToString();
                        oSheet.Cells[16, 27] = dt.Rows[i]["SoHocSinhKhuyetTatK4DiemDuoi5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK4DiemDuoi5"].ToString();
                        //Hết Khối 4 - Điểm dưới 5

                        //Khối 5 - Điểm dưới 5
                        oSheet.Cells[16, 28] = dt.Rows[i]["SoHocSinhK5DiemDuoi5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhK5DiemDuoi5"].ToString();
                        oSheet.Cells[16, 29] = dt.Rows[i]["SoHocSinhNuK5DiemDuoi5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK5DiemDuoi5"].ToString();
                        oSheet.Cells[16, 30] = dt.Rows[i]["SoHocSinhDanTocK5DiemDuoi5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK5DiemDuoi5"].ToString();
                        oSheet.Cells[16, 31] = dt.Rows[i]["SoHocSinhNuDanTocK5DiemDuoi5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK5DiemDuoi5"].ToString();
                        oSheet.Cells[16, 33] = dt.Rows[i]["SoHocSinhKhuyetTatK5DiemDuoi5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK5DiemDuoi5"].ToString();
                        //Hết Khối 5 - Điểm dưới 5


                    }
                    else
                    {
                        oSheet.Cells[row, 2] = (i+1).ToString()+". "+dt.Rows[i]["TenMonHoc"].ToString();
                        oSheet.Cells[row + 1, 2] = "Điểm 10";
                        oSheet.Cells[row + 2, 2] = "Điểm 9";
                        oSheet.Cells[row + 3, 2] = "Điểm 8";
                        oSheet.Cells[row + 4, 2] = "Điểm 7";
                        oSheet.Cells[row + 5, 2] = "Điểm 6";
                        oSheet.Cells[row + 6, 2] = "Điểm 5";
                        oSheet.Cells[row + 7, 2] = "Điểm dưới 5";

                        //Khối 1 - Điểm 10
                        oSheet.Cells[row + 1, 4] = dt.Rows[i]["SoHocSinhK1Diem10"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhK1Diem10"].ToString();
                        oSheet.Cells[row + 1, 5] = dt.Rows[i]["SoHocSinhNuK1Diem10"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK1Diem10"].ToString();
                        oSheet.Cells[row + 1, 6] = dt.Rows[i]["SoHocSinhDanTocK1Diem10"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK1Diem10"].ToString();
                        oSheet.Cells[row + 1, 7] = dt.Rows[i]["SoHocSinhNuDanTocK1Diem10"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK1Diem10"].ToString();
                        oSheet.Cells[row + 1, 9] = dt.Rows[i]["SoHocSinhKhuyetTatK1Diem10"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK1Diem10"].ToString();
                        //Hết Khối 1 - Điểm 10

                        //Khối 2 - Điểm 10
                        oSheet.Cells[row + 1, 10] = dt.Rows[i]["SoHocSinhK2Diem10"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhK2Diem10"].ToString();
                        oSheet.Cells[row + 1, 11] = dt.Rows[i]["SoHocSinhNuK2Diem10"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK2Diem10"].ToString();
                        oSheet.Cells[row + 1, 12] = dt.Rows[i]["SoHocSinhDanTocK2Diem10"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK2Diem10"].ToString();
                        oSheet.Cells[row + 1, 13] = dt.Rows[i]["SoHocSinhNuDanTocK2Diem10"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK2Diem10"].ToString();
                        oSheet.Cells[row + 1, 15] = dt.Rows[i]["SoHocSinhKhuyetTatK2Diem10"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK2Diem10"].ToString();
                        //Hết Khối 2 - Điểm 10

                        //Khối 3 - Điểm 10
                        oSheet.Cells[row + 1, 16] = dt.Rows[i]["SoHocSinhK3Diem10"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhK3Diem10"].ToString();
                        oSheet.Cells[row + 1, 17] = dt.Rows[i]["SoHocSinhNuK3Diem10"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK3Diem10"].ToString();
                        oSheet.Cells[row + 1, 18] = dt.Rows[i]["SoHocSinhDanTocK3Diem10"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK3Diem10"].ToString();
                        oSheet.Cells[row + 1, 19] = dt.Rows[i]["SoHocSinhNuDanTocK3Diem10"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK3Diem10"].ToString();
                        oSheet.Cells[row + 1, 21] = dt.Rows[i]["SoHocSinhKhuyetTatK3Diem10"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK3Diem10"].ToString();
                        //Hết Khối 3 - Điểm 10

                        //Khối 4 - Điểm 10
                        oSheet.Cells[row + 1, 22] = dt.Rows[i]["SoHocSinhK4Diem10"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhK4Diem10"].ToString();
                        oSheet.Cells[row + 1, 23] = dt.Rows[i]["SoHocSinhNuK4Diem10"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK4Diem10"].ToString();
                        oSheet.Cells[row + 1, 24] = dt.Rows[i]["SoHocSinhDanTocK4Diem10"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK4Diem10"].ToString();
                        oSheet.Cells[row + 1, 25] = dt.Rows[i]["SoHocSinhNuDanTocK4Diem10"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK4Diem10"].ToString();
                        oSheet.Cells[row + 1, 27] = dt.Rows[i]["SoHocSinhKhuyetTatK4Diem10"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK4Diem10"].ToString();
                        //Hết Khối 4 - Điểm 10

                        //Khối 5 - Điểm 10
                        oSheet.Cells[row + 1, 28] = dt.Rows[i]["SoHocSinhK5Diem10"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhK5Diem10"].ToString();
                        oSheet.Cells[row + 1, 29] = dt.Rows[i]["SoHocSinhNuK5Diem10"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK5Diem10"].ToString();
                        oSheet.Cells[row + 1, 30] = dt.Rows[i]["SoHocSinhDanTocK5Diem10"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK5Diem10"].ToString();
                        oSheet.Cells[row + 1, 31] = dt.Rows[i]["SoHocSinhNuDanTocK5Diem10"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK5Diem10"].ToString();
                        oSheet.Cells[row + 1, 33] = dt.Rows[i]["SoHocSinhKhuyetTatK5Diem10"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK5Diem10"].ToString();
                        //Hết Khối 5 - Điểm 10

                        //Khối 1 - Điểm 9
                        oSheet.Cells[row + 2, 4] = dt.Rows[i]["SoHocSinhK1Diem9"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhK1Diem9"].ToString();
                        oSheet.Cells[row + 2, 5] = dt.Rows[i]["SoHocSinhNuK1Diem9"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK1Diem9"].ToString();
                        oSheet.Cells[row + 2, 6] = dt.Rows[i]["SoHocSinhDanTocK1Diem9"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK1Diem9"].ToString();
                        oSheet.Cells[row + 2, 7] = dt.Rows[i]["SoHocSinhNuDanTocK1Diem9"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK1Diem9"].ToString();
                        oSheet.Cells[row + 2, 9] = dt.Rows[i]["SoHocSinhKhuyetTatK1Diem9"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK1Diem9"].ToString();
                        //Hết Khối 1 - Điểm 9

                        //Khối 2 - Điểm 9
                        oSheet.Cells[row + 2, 10] = dt.Rows[i]["SoHocSinhK2Diem9"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhK2Diem9"].ToString();
                        oSheet.Cells[row + 2, 11] = dt.Rows[i]["SoHocSinhNuK2Diem9"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK2Diem9"].ToString();
                        oSheet.Cells[row + 2, 12] = dt.Rows[i]["SoHocSinhDanTocK2Diem9"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK2Diem9"].ToString();
                        oSheet.Cells[row + 2, 13] = dt.Rows[i]["SoHocSinhNuDanTocK2Diem9"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK2Diem9"].ToString();
                        oSheet.Cells[row + 2, 15] = dt.Rows[i]["SoHocSinhKhuyetTatK2Diem9"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK2Diem9"].ToString();
                        //Hết Khối 2 - Điểm 9

                        //Khối 3 - Điểm 9
                        oSheet.Cells[row + 2, 16] = dt.Rows[i]["SoHocSinhK3Diem9"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhK3Diem9"].ToString();
                        oSheet.Cells[row + 2, 17] = dt.Rows[i]["SoHocSinhNuK3Diem9"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK3Diem9"].ToString();
                        oSheet.Cells[row + 2, 18] = dt.Rows[i]["SoHocSinhDanTocK3Diem9"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK3Diem9"].ToString();
                        oSheet.Cells[row + 2, 19] = dt.Rows[i]["SoHocSinhNuDanTocK3Diem9"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK3Diem9"].ToString();
                        oSheet.Cells[row + 2, 21] = dt.Rows[i]["SoHocSinhKhuyetTatK3Diem9"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK3Diem9"].ToString();
                        //Hết Khối 3 - Điểm 9

                        //Khối 4 - Điểm 9
                        oSheet.Cells[row + 2, 22] = dt.Rows[i]["SoHocSinhK4Diem9"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhK4Diem9"].ToString();
                        oSheet.Cells[row + 2, 23] = dt.Rows[i]["SoHocSinhNuK4Diem9"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK4Diem9"].ToString();
                        oSheet.Cells[row + 2, 24] = dt.Rows[i]["SoHocSinhDanTocK4Diem9"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK4Diem9"].ToString();
                        oSheet.Cells[row + 2, 25] = dt.Rows[i]["SoHocSinhNuDanTocK4Diem9"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK4Diem9"].ToString();
                        oSheet.Cells[row + 2, 27] = dt.Rows[i]["SoHocSinhKhuyetTatK4Diem9"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK4Diem9"].ToString();
                        //Hết Khối 4 - Điểm 9

                        //Khối 5 - Điểm 9
                        oSheet.Cells[row + 2, 28] = dt.Rows[i]["SoHocSinhK5Diem9"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhK5Diem9"].ToString();
                        oSheet.Cells[row + 2, 29] = dt.Rows[i]["SoHocSinhNuK5Diem9"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK5Diem9"].ToString();
                        oSheet.Cells[row + 2, 30] = dt.Rows[i]["SoHocSinhDanTocK5Diem9"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK5Diem9"].ToString();
                        oSheet.Cells[row + 2, 31] = dt.Rows[i]["SoHocSinhNuDanTocK5Diem9"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK5Diem9"].ToString();
                        oSheet.Cells[row + 2, 33] = dt.Rows[i]["SoHocSinhKhuyetTatK5Diem9"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK5Diem9"].ToString();
                        //Hết Khối 5 - Điểm 9

                        //Khối 1 - Điểm 8
                        oSheet.Cells[row + 3, 4] = dt.Rows[i]["SoHocSinhK1Diem8"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhK1Diem8"].ToString();
                        oSheet.Cells[row + 3, 5] = dt.Rows[i]["SoHocSinhNuK1Diem8"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK1Diem8"].ToString();
                        oSheet.Cells[row + 3, 6] = dt.Rows[i]["SoHocSinhDanTocK1Diem8"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK1Diem8"].ToString();
                        oSheet.Cells[row + 3, 7] = dt.Rows[i]["SoHocSinhNuDanTocK1Diem8"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK1Diem8"].ToString();
                        oSheet.Cells[row + 3, 9] = dt.Rows[i]["SoHocSinhKhuyetTatK1Diem8"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK1Diem8"].ToString();
                        //Hết Khối 1 - Điểm 8

                        //Khối 2 - Điểm 8
                        oSheet.Cells[row + 3, 10] = dt.Rows[i]["SoHocSinhK2Diem8"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhK2Diem8"].ToString();
                        oSheet.Cells[row + 3, 11] = dt.Rows[i]["SoHocSinhNuK2Diem8"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK2Diem8"].ToString();
                        oSheet.Cells[row + 3, 12] = dt.Rows[i]["SoHocSinhDanTocK2Diem8"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK2Diem8"].ToString();
                        oSheet.Cells[row + 3, 13] = dt.Rows[i]["SoHocSinhNuDanTocK2Diem8"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK2Diem8"].ToString();
                        oSheet.Cells[row + 3, 15] = dt.Rows[i]["SoHocSinhKhuyetTatK2Diem8"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK2Diem8"].ToString();
                        //Hết Khối 2 - Điểm 8

                        //Khối 3 - Điểm 8
                        oSheet.Cells[row + 3, 16] = dt.Rows[i]["SoHocSinhK3Diem8"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhK3Diem8"].ToString();
                        oSheet.Cells[row + 3, 17] = dt.Rows[i]["SoHocSinhNuK3Diem8"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK3Diem8"].ToString();
                        oSheet.Cells[row + 3, 18] = dt.Rows[i]["SoHocSinhDanTocK3Diem8"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK3Diem8"].ToString();
                        oSheet.Cells[row + 3, 19] = dt.Rows[i]["SoHocSinhNuDanTocK3Diem8"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK3Diem8"].ToString();
                        oSheet.Cells[row + 3, 21] = dt.Rows[i]["SoHocSinhKhuyetTatK3Diem8"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK3Diem8"].ToString();
                        //Hết Khối 3 - Điểm 8

                        //Khối 4 - Điểm 8
                        oSheet.Cells[row + 3, 22] = dt.Rows[i]["SoHocSinhK4Diem8"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhK4Diem8"].ToString();
                        oSheet.Cells[row + 3, 23] = dt.Rows[i]["SoHocSinhNuK4Diem8"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK4Diem8"].ToString();
                        oSheet.Cells[row + 3, 24] = dt.Rows[i]["SoHocSinhDanTocK4Diem8"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK4Diem8"].ToString();
                        oSheet.Cells[row + 3, 25] = dt.Rows[i]["SoHocSinhNuDanTocK4Diem8"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK4Diem8"].ToString();
                        oSheet.Cells[row + 3, 27] = dt.Rows[i]["SoHocSinhKhuyetTatK4Diem8"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK4Diem8"].ToString();
                        //Hết Khối 4 - Điểm 8

                        //Khối 5 - Điểm 8
                        oSheet.Cells[row + 3, 28] = dt.Rows[i]["SoHocSinhK5Diem8"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhK5Diem8"].ToString();
                        oSheet.Cells[row + 3, 29] = dt.Rows[i]["SoHocSinhNuK5Diem8"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK5Diem8"].ToString();
                        oSheet.Cells[row + 3, 30] = dt.Rows[i]["SoHocSinhDanTocK5Diem8"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK5Diem8"].ToString();
                        oSheet.Cells[row + 3, 31] = dt.Rows[i]["SoHocSinhNuDanTocK5Diem8"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK5Diem8"].ToString();
                        oSheet.Cells[row + 3, 33] = dt.Rows[i]["SoHocSinhKhuyetTatK5Diem8"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK5Diem8"].ToString();
                        //Hết Khối 5 - Điểm 8

                        //Khối 1 - Điểm 7
                        oSheet.Cells[row + 4, 4] = dt.Rows[i]["SoHocSinhK1Diem7"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhK1Diem7"].ToString();
                        oSheet.Cells[row + 4, 5] = dt.Rows[i]["SoHocSinhNuK1Diem7"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK1Diem7"].ToString();
                        oSheet.Cells[row + 4, 6] = dt.Rows[i]["SoHocSinhDanTocK1Diem7"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK1Diem7"].ToString();
                        oSheet.Cells[row + 4, 7] = dt.Rows[i]["SoHocSinhNuDanTocK1Diem7"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK1Diem7"].ToString();
                        oSheet.Cells[row + 4, 9] = dt.Rows[i]["SoHocSinhKhuyetTatK1Diem7"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK1Diem7"].ToString();
                        //Hết Khối 1 - Điểm 7

                        //Khối 2 - Điểm 7
                        oSheet.Cells[row + 4, 10] = dt.Rows[i]["SoHocSinhK2Diem7"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhK2Diem7"].ToString();
                        oSheet.Cells[row + 4, 11] = dt.Rows[i]["SoHocSinhNuK2Diem7"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK2Diem7"].ToString();
                        oSheet.Cells[row + 4, 12] = dt.Rows[i]["SoHocSinhDanTocK2Diem7"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK2Diem7"].ToString();
                        oSheet.Cells[row + 4, 13] = dt.Rows[i]["SoHocSinhNuDanTocK2Diem7"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK2Diem7"].ToString();
                        oSheet.Cells[row + 4, 15] = dt.Rows[i]["SoHocSinhKhuyetTatK2Diem7"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK2Diem7"].ToString();
                        //Hết Khối 2 - Điểm 7

                        //Khối 3 - Điểm 7
                        oSheet.Cells[row + 4, 16] = dt.Rows[i]["SoHocSinhK3Diem7"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhK3Diem7"].ToString();
                        oSheet.Cells[row + 4, 17] = dt.Rows[i]["SoHocSinhNuK3Diem7"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK3Diem7"].ToString();
                        oSheet.Cells[row + 4, 18] = dt.Rows[i]["SoHocSinhDanTocK3Diem7"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK3Diem7"].ToString();
                        oSheet.Cells[row + 4, 19] = dt.Rows[i]["SoHocSinhNuDanTocK3Diem7"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK3Diem7"].ToString();
                        oSheet.Cells[row + 4, 21] = dt.Rows[i]["SoHocSinhKhuyetTatK3Diem7"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK3Diem7"].ToString();
                        //Hết Khối 3 - Điểm 7

                        //Khối 4 - Điểm 7
                        oSheet.Cells[row + 4, 22] = dt.Rows[i]["SoHocSinhK4Diem7"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhK4Diem7"].ToString();
                        oSheet.Cells[row + 4, 23] = dt.Rows[i]["SoHocSinhNuK4Diem7"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK4Diem7"].ToString();
                        oSheet.Cells[row + 4, 24] = dt.Rows[i]["SoHocSinhDanTocK4Diem7"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK4Diem7"].ToString();
                        oSheet.Cells[row + 4, 25] = dt.Rows[i]["SoHocSinhNuDanTocK4Diem7"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK4Diem7"].ToString();
                        oSheet.Cells[row + 4, 27] = dt.Rows[i]["SoHocSinhKhuyetTatK4Diem7"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK4Diem7"].ToString();
                        //Hết Khối 4 - Điểm 7

                        //Khối 5 - Điểm 7
                        oSheet.Cells[row + 4, 28] = dt.Rows[i]["SoHocSinhK5Diem7"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhK5Diem7"].ToString();
                        oSheet.Cells[row + 4, 29] = dt.Rows[i]["SoHocSinhNuK5Diem7"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK5Diem7"].ToString();
                        oSheet.Cells[row + 4, 30] = dt.Rows[i]["SoHocSinhDanTocK5Diem7"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK5Diem7"].ToString();
                        oSheet.Cells[row + 4, 31] = dt.Rows[i]["SoHocSinhNuDanTocK5Diem7"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK5Diem7"].ToString();
                        oSheet.Cells[row + 4, 33] = dt.Rows[i]["SoHocSinhKhuyetTatK5Diem7"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK5Diem7"].ToString();
                        //Hết Khối 5 - Điểm 7

                        //Khối 1 - Điểm 6
                        oSheet.Cells[row + 5, 4] = dt.Rows[i]["SoHocSinhK1Diem6"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhK1Diem6"].ToString();
                        oSheet.Cells[row + 5, 5] = dt.Rows[i]["SoHocSinhNuK1Diem6"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK1Diem6"].ToString();
                        oSheet.Cells[row + 5, 6] = dt.Rows[i]["SoHocSinhDanTocK1Diem6"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK1Diem6"].ToString();
                        oSheet.Cells[row + 5, 7] = dt.Rows[i]["SoHocSinhNuDanTocK1Diem6"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK1Diem6"].ToString();
                        oSheet.Cells[row + 5, 9] = dt.Rows[i]["SoHocSinhKhuyetTatK1Diem6"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK1Diem6"].ToString();
                        //Hết Khối 1 - Điểm 6

                        //Khối 2 - Điểm 6
                        oSheet.Cells[row + 5, 10] = dt.Rows[i]["SoHocSinhK2Diem6"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhK2Diem6"].ToString();
                        oSheet.Cells[row + 5, 11] = dt.Rows[i]["SoHocSinhNuK2Diem6"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK2Diem6"].ToString();
                        oSheet.Cells[row + 5, 12] = dt.Rows[i]["SoHocSinhDanTocK2Diem6"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK2Diem6"].ToString();
                        oSheet.Cells[row + 5, 13] = dt.Rows[i]["SoHocSinhNuDanTocK2Diem6"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK2Diem6"].ToString();
                        oSheet.Cells[row + 5, 15] = dt.Rows[i]["SoHocSinhKhuyetTatK2Diem6"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK2Diem6"].ToString();
                        //Hết Khối 2 - Điểm 6

                        //Khối 3 - Điểm 6
                        oSheet.Cells[row + 5, 16] = dt.Rows[i]["SoHocSinhK3Diem6"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhK3Diem6"].ToString();
                        oSheet.Cells[row + 5, 17] = dt.Rows[i]["SoHocSinhNuK3Diem6"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK3Diem6"].ToString();
                        oSheet.Cells[row + 5, 18] = dt.Rows[i]["SoHocSinhDanTocK3Diem6"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK3Diem6"].ToString();
                        oSheet.Cells[row + 5, 19] = dt.Rows[i]["SoHocSinhNuDanTocK3Diem6"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK3Diem6"].ToString();
                        oSheet.Cells[row + 5, 21] = dt.Rows[i]["SoHocSinhKhuyetTatK3Diem6"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK3Diem6"].ToString();
                        //Hết Khối 3 - Điểm 6

                        //Khối 4 - Điểm 6
                        oSheet.Cells[row + 5, 22] = dt.Rows[i]["SoHocSinhK4Diem6"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhK4Diem6"].ToString();
                        oSheet.Cells[row + 5, 23] = dt.Rows[i]["SoHocSinhNuK4Diem6"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK4Diem6"].ToString();
                        oSheet.Cells[row + 5, 24] = dt.Rows[i]["SoHocSinhDanTocK4Diem6"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK4Diem6"].ToString();
                        oSheet.Cells[row + 5, 25] = dt.Rows[i]["SoHocSinhNuDanTocK4Diem6"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK4Diem6"].ToString();
                        oSheet.Cells[row + 5, 27] = dt.Rows[i]["SoHocSinhKhuyetTatK4Diem6"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK4Diem6"].ToString();
                        //Hết Khối 4 - Điểm 6

                        //Khối 5 - Điểm 6
                        oSheet.Cells[row + 5, 28] = dt.Rows[i]["SoHocSinhK5Diem6"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhK5Diem6"].ToString();
                        oSheet.Cells[row + 5, 29] = dt.Rows[i]["SoHocSinhNuK5Diem6"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK5Diem6"].ToString();
                        oSheet.Cells[row + 5, 30] = dt.Rows[i]["SoHocSinhDanTocK5Diem6"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK5Diem6"].ToString();
                        oSheet.Cells[row + 5, 31] = dt.Rows[i]["SoHocSinhNuDanTocK5Diem6"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK5Diem6"].ToString();
                        oSheet.Cells[row + 5, 33] = dt.Rows[i]["SoHocSinhKhuyetTatK5Diem6"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK5Diem6"].ToString();
                        //Hết Khối 5 - Điểm 6

                        //Khối 1 - Điểm 5
                        oSheet.Cells[row + 6, 4] = dt.Rows[i]["SoHocSinhK1Diem5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhK1Diem5"].ToString();
                        oSheet.Cells[row + 6, 5] = dt.Rows[i]["SoHocSinhNuK1Diem5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK1Diem5"].ToString();
                        oSheet.Cells[row + 6, 6] = dt.Rows[i]["SoHocSinhDanTocK1Diem5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK1Diem5"].ToString();
                        oSheet.Cells[row + 6, 7] = dt.Rows[i]["SoHocSinhNuDanTocK1Diem5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK1Diem5"].ToString();
                        oSheet.Cells[row + 6, 9] = dt.Rows[i]["SoHocSinhKhuyetTatK1Diem5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK1Diem5"].ToString();
                        //Hết Khối 1 - Điểm 5

                        //Khối 2 - Điểm 5
                        oSheet.Cells[row + 6, 10] = dt.Rows[i]["SoHocSinhK2Diem5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhK2Diem5"].ToString();
                        oSheet.Cells[row + 6, 11] = dt.Rows[i]["SoHocSinhNuK2Diem5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK2Diem5"].ToString();
                        oSheet.Cells[row + 6, 12] = dt.Rows[i]["SoHocSinhDanTocK2Diem5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK2Diem5"].ToString();
                        oSheet.Cells[row + 6, 13] = dt.Rows[i]["SoHocSinhNuDanTocK2Diem5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK2Diem5"].ToString();
                        oSheet.Cells[row + 6, 15] = dt.Rows[i]["SoHocSinhKhuyetTatK2Diem5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK2Diem5"].ToString();
                        //Hết Khối 2 - Điểm 5

                        //Khối 3 - Điểm 5
                        oSheet.Cells[row + 6, 16] = dt.Rows[i]["SoHocSinhK3Diem5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhK3Diem5"].ToString();
                        oSheet.Cells[row + 6, 17] = dt.Rows[i]["SoHocSinhNuK3Diem5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK3Diem5"].ToString();
                        oSheet.Cells[row + 6, 18] = dt.Rows[i]["SoHocSinhDanTocK3Diem5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK3Diem5"].ToString();
                        oSheet.Cells[row + 6, 19] = dt.Rows[i]["SoHocSinhNuDanTocK3Diem5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK3Diem5"].ToString();
                        oSheet.Cells[row + 6, 21] = dt.Rows[i]["SoHocSinhKhuyetTatK3Diem5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK3Diem5"].ToString();
                        //Hết Khối 3 - Điểm 5

                        //Khối 4 - Điểm 5
                        oSheet.Cells[row + 6, 22] = dt.Rows[i]["SoHocSinhK4Diem5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhK4Diem5"].ToString();
                        oSheet.Cells[row + 6, 23] = dt.Rows[i]["SoHocSinhNuK4Diem5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK4Diem5"].ToString();
                        oSheet.Cells[row + 6, 24] = dt.Rows[i]["SoHocSinhDanTocK4Diem5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK4Diem5"].ToString();
                        oSheet.Cells[row + 6, 25] = dt.Rows[i]["SoHocSinhNuDanTocK4Diem5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK4Diem5"].ToString();
                        oSheet.Cells[row + 6, 27] = dt.Rows[i]["SoHocSinhKhuyetTatK4Diem5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK4Diem5"].ToString();
                        //Hết Khối 4 - Điểm 5

                        //Khối 5 - Điểm 5
                        oSheet.Cells[row + 6, 28] = dt.Rows[i]["SoHocSinhK5Diem5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhK5Diem5"].ToString();
                        oSheet.Cells[row + 6, 29] = dt.Rows[i]["SoHocSinhNuK5Diem5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK5Diem5"].ToString();
                        oSheet.Cells[row + 6, 30] = dt.Rows[i]["SoHocSinhDanTocK5Diem5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK5Diem5"].ToString();
                        oSheet.Cells[row + 6, 31] = dt.Rows[i]["SoHocSinhNuDanTocK5Diem5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK5Diem5"].ToString();
                        oSheet.Cells[row + 6, 33] = dt.Rows[i]["SoHocSinhKhuyetTatK5Diem5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK5Diem5"].ToString();
                        //Hết Khối 5 - Điểm 5

                        //Khối 1 - Điểm dưới 5
                        oSheet.Cells[row + 7, 4] = dt.Rows[i]["SoHocSinhK1DiemDuoi5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhK1DiemDuoi5"].ToString();
                        oSheet.Cells[row + 7, 5] = dt.Rows[i]["SoHocSinhNuK1DiemDuoi5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK1DiemDuoi5"].ToString();
                        oSheet.Cells[row + 7, 6] = dt.Rows[i]["SoHocSinhDanTocK1DiemDuoi5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK1DiemDuoi5"].ToString();
                        oSheet.Cells[row + 7, 7] = dt.Rows[i]["SoHocSinhNuDanTocK1DiemDuoi5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK1DiemDuoi5"].ToString();
                        oSheet.Cells[row + 7, 9] = dt.Rows[i]["SoHocSinhKhuyetTatK1DiemDuoi5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK1DiemDuoi5"].ToString();
                        //Hết Khối 1 - Điểm dưới 5

                        //Khối 2 - Điểm dưới 5
                        oSheet.Cells[row + 7, 10] = dt.Rows[i]["SoHocSinhK2Diem5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhK2DiemDuoi5"].ToString();
                        oSheet.Cells[row + 7, 11] = dt.Rows[i]["SoHocSinhNuK2Diem5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK2DiemDuoi5"].ToString();
                        oSheet.Cells[row + 7, 12] = dt.Rows[i]["SoHocSinhDanTocK2Diem5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK2DiemDuoi5"].ToString();
                        oSheet.Cells[row + 7, 13] = dt.Rows[i]["SoHocSinhNuDanTocK2Diem5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK2DiemDuoi5"].ToString();
                        oSheet.Cells[row + 7, 15] = dt.Rows[i]["SoHocSinhKhuyetTatK2Diem5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK2Diem5"].ToString();
                        //Hết Khối 2 - Điểm dưới 5

                        //Khối 3 - Điểm 5
                        oSheet.Cells[row + 7, 16] = dt.Rows[i]["SoHocSinhK3DiemDuoi5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhK3DiemDuoi5"].ToString();
                        oSheet.Cells[row + 7, 17] = dt.Rows[i]["SoHocSinhNuK3DiemDuoi5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK3DiemDuoi5"].ToString();
                        oSheet.Cells[row + 7, 18] = dt.Rows[i]["SoHocSinhDanTocK3DiemDuoi5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK3DiemDuoi5"].ToString();
                        oSheet.Cells[row + 7, 19] = dt.Rows[i]["SoHocSinhNuDanTocK3DiemDuoi5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK3DiemDuoi5"].ToString();
                        oSheet.Cells[row + 7, 21] = dt.Rows[i]["SoHocSinhKhuyetTatK3DiemDuoi5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK3DiemDuoi5"].ToString();
                        //Hết Khối 3 - Điểm dưới 5

                        //Khối 4 - Điểm dưới 5
                        oSheet.Cells[row + 7, 22] = dt.Rows[i]["SoHocSinhK4DiemDuoi5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhK4DiemDuoi5"].ToString();
                        oSheet.Cells[row + 7, 23] = dt.Rows[i]["SoHocSinhNuK4DiemDuoi5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK4DiemDuoi5"].ToString();
                        oSheet.Cells[row + 7, 24] = dt.Rows[i]["SoHocSinhDanTocK4DiemDuoi5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK4DiemDuoi5"].ToString();
                        oSheet.Cells[row + 7, 25] = dt.Rows[i]["SoHocSinhNuDanTocK4DiemDuoi5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK4DiemDuoi5"].ToString();
                        oSheet.Cells[row + 7, 27] = dt.Rows[i]["SoHocSinhKhuyetTatK4DiemDuoi5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK4DiemDuoi5"].ToString();
                        //Hết Khối 4 - Điểm dưới 5

                        //Khối 5 - Điểm dưới 5
                        oSheet.Cells[row + 7, 28] = dt.Rows[i]["SoHocSinhK5DiemDuoi5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhK5DiemDuoi5"].ToString();
                        oSheet.Cells[row + 7, 29] = dt.Rows[i]["SoHocSinhNuK5DiemDuoi5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuK5DiemDuoi5"].ToString();
                        oSheet.Cells[row + 7, 30] = dt.Rows[i]["SoHocSinhDanTocK5DiemDuoi5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhDanTocK5DiemDuoi5"].ToString();
                        oSheet.Cells[row + 7, 31] = dt.Rows[i]["SoHocSinhNuDanTocK5DiemDuoi5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhNuDanTocK5DiemDuoi5"].ToString();
                        oSheet.Cells[row + 7, 33] = dt.Rows[i]["SoHocSinhKhuyetTatK5DiemDuoi5"].ToString() == "0" ? "" : dt.Rows[i]["SoHocSinhKhuyetTatK5DiemDuoi5"].ToString();
                        //Hết Khối 5 - Điểm dưới 5

                        row = row + 8;
                    }

                }
                //oSheet.Cells[row, col] = data;

                oWB.Save();
                oWB.Close();

                json = newfile;
            }
            else
            {
                json = "NoData";
            }
            return Json(json, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public virtual ActionResult DownloadFile(string file)
        {
            file = file.Replace("~$", "");
            string fullPath = Path.Combine(Server.MapPath("~/ProcessTemplate"), file);
            return File(fullPath, "application/vnd.ms-excel", file);
        }
        public ActionResult TongHopDanhGia()
        {
            string userName = getUserName();

            int idTruong = Cmon.getIdtruong(userName, db);
            string namhoc = Cmon.GetNamHienTai(idTruong);
            bool isHieuTruong = Cmon.isHieuTruong(idTruong, userName);
            ViewBag.isHieuTruong = isHieuTruong;
            //Chi load nhung khoi ma giao vien do co lop day lam GVCN
            var khois = (from khoi in db.DM_Khoi
                      let makhoiList = from lh in db.LopHocs
                                       join userlh in db.Users_LopHoc
                                       on lh.MaLop equals userlh.MaLop
                                       where lh.idTruong == idTruong && lh.NamHoc == namhoc && userlh.UserName == userName
                                       select lh.MaKhoi
                      where makhoiList.Contains(khoi.MaKhoi) && khoi.idTruong == idTruong && khoi.NamHoc == namhoc
                      select khoi).ToList();

            DM_Khoi kl = new DM_Khoi();
            kl.MaKhoi = "";
            kl.TenKhoi = "--Chọn khối--";
            kl.idTruong = idTruong;
            kl.NamHoc = namhoc;
            khois.Insert(0, kl);

            ViewBag.khoi = khois;

            bool isGV = Cmon.isRight(userName, "GV", db);
            bool isrightGVHT = Cmon.isRightsHTAndGV(userName, "GV", "HT", db);
            //bool isrightGVCN = Cmon.isGVCN(userName, "403", db);
            if (isrightGVHT == true || isGV == true)
            {
                ViewBag.allowSave = 1;
            }
            else
            {
                ViewBag.allowSave = 0;
            }

            // neu la hieu truong va ko co quyen gv thi van hien thi nhung an di nut luu
            var lophoc = (from ul in db.Users_LopHoc
                          join lh in db.LopHocs
                          on new { ul.MaLop, ul.idTruong, ul.NamHoc } equals new { lh.MaLop, lh.idTruong, lh.NamHoc }

                          where ul.idTruong == idTruong && ul.NamHoc == namhoc && ul.UserName == userName
                          && (from usrole in db.ScrJRoleUsers where usrole.UserName == userName select usrole.UserName).Contains(ul.UserName)
                          select new LopHocGiaoVien
                         {
                             MaLop = ul.MaLop,
                             TenLop = lh.TenLop,
                             IdTruong = ul.idTruong,
                             NamHoc = ul.NamHoc
                         }).Distinct().OrderBy(lh => lh.MaLop).ToList();

            
            LopHocGiaoVien lhoc = new LopHocGiaoVien();
            lhoc.MaLop = "";
            lhoc.TenLop = "--Chọn lớp học--";
            lhoc.NamHoc = namhoc;
            lhoc.IdTruong = idTruong;
            lophoc.Insert(0, lhoc);

            ViewBag.lophoc = lophoc;
            ViewBag.tenuser = Cmon.GetnameUser(userName);
            ViewBag.namhientai = namhoc;

            ViewBag.hocky = Cmon.CreateHocKyTheoDanhGia();

            return View(@"~/Views/TongHop/TongHopDanhGia.cshtml");
        }


        public ActionResult GVBMTongHopDanhGia()
        {
            string userName = getUserName();

            int idTruong = Cmon.getIdtruong(userName, db);
            string namhoc = Cmon.GetNamHienTai(idTruong);
            bool isHieuTruong = Cmon.isHieuTruong(idTruong, userName);
            ViewBag.isHieuTruong = isHieuTruong;
            //Chi load nhung khoi ma giao vien do co lop day lam GVCN
            var khois = (from khoi in db.DM_Khoi
                         let makhoiList = from lh in db.LopHocs
                                          join userlh in db.Users_LopHoc
                                          on lh.MaLop equals userlh.MaLop
                                          where lh.idTruong == idTruong && lh.NamHoc == namhoc && userlh.UserName == userName
                                          select lh.MaKhoi
                         where makhoiList.Contains(khoi.MaKhoi) && khoi.idTruong == idTruong && khoi.NamHoc == namhoc
                         select khoi).ToList();

            DM_Khoi kl = new DM_Khoi();
            kl.MaKhoi = "";
            kl.TenKhoi = "--Chọn khối--";
            kl.idTruong = idTruong;
            kl.NamHoc = namhoc;
            khois.Insert(0, kl);

            ViewBag.khoi = khois;
            var lophoc = (from lh in db.LopHocs
                          join um in db.Users_Monhoc
                          on new { lh.MaLop } equals new { um.MaLop }
                          join asignLopHoc in db.ASSIGN_LOP_MonHoc
                          on new { um.MaLop, um.MaMonHoc } equals new { asignLopHoc.MaLop, asignLopHoc.MaMonHoc }
                          join roleuser in db.ScrJRoleUsers
                          on um.UserName equals roleuser.UserName
                          join funtion in db.ScrJRoleFunctions
                          on roleuser.RoleName equals funtion.RoleName
                          where lh.idTruong == idTruong && lh.NamHoc == namhoc && um.UserName == userName && um.idTruong == idTruong && um.NamHoc == namhoc
                          && funtion.FunctionID== 112
                          && funtion.Enable == true
                          && (funtion.Read_ == true || funtion.Insert_ == true || funtion.Update_ == true)
                        
                          select new LopHocGiaoVien
                          {
                              MaLop = lh.MaLop,
                              TenLop = lh.TenLop,
                              IdTruong = lh.idTruong,
                              NamHoc = lh.NamHoc
                          }).Distinct().OrderBy(lh=> lh.MaLop) .ToList();

            LopHocGiaoVien lhoc = new LopHocGiaoVien();
            lhoc.MaLop = "";
            lhoc.TenLop = "--Chọn lớp học--";
            lhoc.NamHoc = namhoc;
            lhoc.IdTruong = idTruong;
            lophoc.Insert(0, lhoc);
            ViewBag.tenuser = Cmon.GetnameUser(userName);
            ViewBag.namhientai = namhoc;
            ViewBag.lophoc = lophoc;

            ViewBag.hocky = Cmon.CreateHocKyTheoDanhGia();

            return View(@"~/Views/TongHop/GVBMTongHopDanhGia.cshtml");
        }

        public JsonResult LoadLopHoc(string makhoi)
        {
            string userName = getUserName();

            int idTruong = Cmon.getIdtruong(userName, db);
            string namhoc = Cmon.GetNamHienTai(idTruong);
            var lophoc = (from lh in db.Users_LopHoc.Where(t => t.idTruong == idTruong && t.NamHoc == namhoc && t.UserName == userName)
                          join l in db.LopHocs.Where(i => i.idTruong == idTruong && i.NamHoc == namhoc && i.MaKhoi==makhoi)
                          on new { lh.idTruong, lh.NamHoc } equals new { l.idTruong, l.NamHoc }
                          select new LopHocGiaoVien
                          {
                              MaLop = l.MaLop,
                              TenLop = l.TenLop,
                              IdTruong = l.idTruong,
                              NamHoc = l.NamHoc
                          }
                            ).GroupBy(x => x.MaLop).Select(z => z.OrderBy(i => i.MaLop).FirstOrDefault()).ToList();
            LopHocGiaoVien lhoc = new LopHocGiaoVien();
            lhoc.MaLop = "";
            lhoc.TenLop = "--Chọn lớp học--";
            lhoc.NamHoc = namhoc;
            lhoc.IdTruong = idTruong;
            lophoc.Insert(0, lhoc);

            return Json(lophoc, JsonRequestBehavior.AllowGet);
        }

        public string GetMonHoc(string mamon)
        {
            string tenmon = "";
            tenmon = (from mh in db.DM_MonHoc.Where(t => t.MaMonHoc == mamon) select mh.TenMonHoc).FirstOrDefault();
            return tenmon;
        }

        public JsonResult SaveDiemHocSinh(string mahocsinhDiem, string lop, string hocky)
        {
            if (hocky == "1") hocky = "1";
            else hocky = "0";
            string mess = "OK";
            string[] HSDiem = mahocsinhDiem.Split('|');
            if (HSDiem.Count()>0)
            {
                string[] mahsmon = HSDiem[0].Split('_');
                string mahs = mahsmon[0];
                string mamon = mahsmon[1];
                string diem = HSDiem[1];
                UpdateData(mahs, mamon, diem, lop, hocky);
            }
            return Json(mess, JsonRequestBehavior.AllowGet);
        }


        [AcceptVerbs(HttpVerbs.Post)]
        [Authorize]
        public JsonResult SaveThongTinNhanXet(string maLop, string hocky, string mahs, List<NhanXetMonHoc> nhanXetMonHocs)
        {

            string mess = "OK";
            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            string namHoc = Cmon.GetNamHienTai(idTruong);
           // short lenLop = Int16.Parse(duocLenLop);
           // if (nhanXetMonHocs != null && nhanXetMonHocs.Count > 0)
           // {
            for (int i = 0; i < nhanXetMonHocs.Count; i++)
            {
                if (!Cmon.bangTongHop.Contains(nhanXetMonHocs[i].maMon))
                {
                    UpdateNhanXetTrongBangTongHopNX_DiemNhanXet(idTruong, namHoc, maLop, mahs, hocky, nhanXetMonHocs[i].maMon, nhanXetMonHocs[i].noiDungNhanXet, 1, userName);

                }
                else
                {
                    UpdateNhanXetTrongBangTongHopNX_DiemNhanXet(idTruong, namHoc, maLop, mahs, hocky, nhanXetMonHocs[i].maMon, nhanXetMonHocs[i].noiDungNhanXet, 2, userName);
                }

            }

                // update duoc len lop 
               // UpdateDuocLenLopTrongBangTongHopNX(idTruong, namHoc, maLop, mahs, hocky, duocLenLop, userName);
                UpdateDuocLenLopTrongBangTongHopNX(idTruong, namHoc, maLop, mahs, hocky, userName);

            //}
            return Json(mess, JsonRequestBehavior.AllowGet);
        }


        public void UpdateNhanXetTrongBangTongHopNX_DiemNhanXet(int idTruong, string namHoc, string maLop, string mahs, string hocky, string maMon, string noiDungNX, int loaibang, string userName)
        {
            if (loaibang == 1) // Thực hiện update/ chèn dữ liệu vào bảng học sinh điểm nhận xét
            {
                //string noiDungNhanXet= "";
                var diemNX = (from dnx in db.HocSinh_Diem_NhanXet
                              where dnx.idTruong == idTruong && dnx.NamHoc == namHoc && dnx.MaLop == maLop
                                  && dnx.MaMonHoc == maMon && dnx.MaHocSinh == mahs
                              select dnx).FirstOrDefault();

                if (diemNX != null)
                {
                    UpdateDataDanhGiaNhanXet(idTruong, namHoc, maLop, mahs, hocky, maMon, noiDungNX,userName);
                }
                else
                {
                    ThemMoiDanhGiaNhanXet(idTruong, namHoc, maLop, mahs, hocky, maMon, noiDungNX, userName);
                }
                 

            }
            else // update vào bảng học sinh tổng hợp nhận xét
            {
                var diemTongHopNX = (from dth in db.HocSinh_TongHopNX
                                     where dth.idTruong == idTruong && dth.NamHoc == namHoc && dth.MaLop == maLop
                                   && dth.MaHocSinh == mahs
                                     select dth).FirstOrDefault();

                // update khen thưởng

                //if (maMon == "khenthuong")
                //{

                //    string maHK = hocky =="0" ? "HK1" :"HK2";
                     
                //    var khenThuongNX = (from dnx in db.HocSinh_KhenThuongKyLuat
                //                  where dnx.idTruong == idTruong && dnx.NamHoc == namHoc && dnx.MaLop == maLop && dnx.MaHocKy== maHK
                //                        && dnx.MaHocSinh == mahs
                //                  select dnx).FirstOrDefault();

                //    if (khenThuongNX != null)
                //    {
                //        khenThuongNX.NoiDung = noiDungNX;

                //        db.SaveChanges();
                //    }
                //    else
                //    { // Thêm mới

                //        HocSinh_KhenThuongKyLuat hocSinhKT = new HocSinh_KhenThuongKyLuat();
                //        hocSinhKT.idTruong = idTruong;
                //        hocSinhKT.NamHoc = namHoc;
                //        hocSinhKT.MaHocKy = maHK;
                //        hocSinhKT.MaHocSinh = mahs;
                //        hocSinhKT.MaHocSinhFriendly = mahs;
                //        hocSinhKT.MaLop = maLop;
                //        hocSinhKT.NoiDung = noiDungNX;
                //        hocSinhKT.Ngay = DateTime.Now;
                //        hocSinhKT.HoTen = Cmon.getHoTenHocSinh(mahs, idTruong, maLop, namHoc);
                //        db.AddToHocSinh_KhenThuongKyLuat(hocSinhKT);
                //        db.SaveChanges();

                //    }
                     
 
                //}

                if (diemTongHopNX != null)
                {
                    UpdateDataDanhGiaTongHop(idTruong, namHoc, maLop, mahs, hocky, maMon, noiDungNX,userName);

                }
                else
                {
                    ThemMoiDanhGiaTongHop(idTruong, namHoc, maLop, mahs, hocky, maMon, noiDungNX, userName);

                }

            }

        }


        public void ThemMoiDanhGiaNhanXet(int idTruong, string namHoc, string maLop, string mahs, string hocky, string maMon, string noiDungNX,string userName)
        {
            HocSinh_Diem_NhanXet diemNhanXet = new HocSinh_Diem_NhanXet();
            diemNhanXet.idTruong = idTruong;
            diemNhanXet.NamHoc = namHoc;
            diemNhanXet.MaHocSinh = mahs;
            diemNhanXet.MaMonHoc = maMon;
            diemNhanXet.MaLop = maLop;
            diemNhanXet.HoTen = Cmon.getHoTenHocSinh(mahs, idTruong, maLop, namHoc);
            diemNhanXet.MaHocSinhFriendly = mahs;
            diemNhanXet.UserCreated = userName;
            diemNhanXet.DateCreated = DateTime.Now;
            if (hocky == "0")
            {
                diemNhanXet.NhanXetHK1 = noiDungNX;
            }
            else
            {
                diemNhanXet.NhanXetCN = noiDungNX;
            }

            db.AddToHocSinh_Diem_NhanXet(diemNhanXet);
            db.SaveChanges();
        }


        public void UpdateDataDanhGiaNhanXet(int idTruong, string namHoc, string maLop, string mahs, string hocky, string maMon, string noiDungNX, string userName)
        {
            var diemNX = (from dnx in db.HocSinh_Diem_NhanXet
                          where dnx.idTruong == idTruong && dnx.NamHoc == namHoc && dnx.MaLop == maLop
                              && dnx.MaMonHoc == maMon && dnx.MaHocSinh == mahs
                          select dnx).FirstOrDefault();


            if (diemNX != null)
            {
                if (hocky == "0")
                {
                    diemNX.NhanXetHK1 = noiDungNX;
                }
                else
                {
                    diemNX.NhanXetCN = noiDungNX;
                }


                diemNX.UserUpdated = userName;
                diemNX.DateUpdated = DateTime.Now;
                db.SaveChanges();
            }
             
        }


        public void ThemMoiDanhGiaTongHop(int idTruong, string namHoc, string maLop, string mahs, string hocky, string maMon, string noiDungNX, string userName)
        {
            HocSinh_TongHopNX diemNhanXet = new HocSinh_TongHopNX();
            diemNhanXet.idTruong = idTruong;
            diemNhanXet.NamHoc = namHoc;
            diemNhanXet.MaLop = maLop;
            diemNhanXet.MaHocSinh = mahs;
            switch (maMon)
            {
                case "nangluc1": diemNhanXet.NXNangLuc1 = noiDungNX; break;
                case "nangluc2": diemNhanXet.NXNangLuc2 = noiDungNX; break;
                case "nangluc3": diemNhanXet.NXNangLuc3 = noiDungNX; break;
                case "phamchat1": diemNhanXet.NXPhamChat1 = noiDungNX; break;
                case "phamchat2": diemNhanXet.NXPhamChat2 = noiDungNX; break;
                case "phamchat3": diemNhanXet.NXPhamChat3 = noiDungNX; break;
                case "phamchat4": diemNhanXet.NXPhamChat4 = noiDungNX; break;
                case "khenthuong": diemNhanXet.KhenThuong = true; break; // TODO
                case "htct": diemNhanXet.HoanThanhCT = noiDungNX; break;

                default:
                    {
                        System.Console.WriteLine("Other number");
                        break;
                    }
            }
             
            
            diemNhanXet.UserCreated = userName;
            diemNhanXet.DateCreated = DateTime.Now;
           
            db.AddToHocSinh_TongHopNX(diemNhanXet);
            db.SaveChanges();
        }


        public void UpdateDuocLenLopTrongBangTongHopNX(int idTruong, string namHoc, string maLop, string mahs, string hocky, string userName)
        {
            short maKy = Int16.Parse(hocky);
            var diemTongHopNX = (from dth in db.HocSinh_TongHopNX
                                 where dth.idTruong == idTruong && dth.NamHoc == namHoc && dth.MaLop == maLop && dth.HocKy == maKy
                               && dth.MaHocSinh == mahs
                                 select dth).FirstOrDefault();

            if (diemTongHopNX != null)
            {

                //if (duocLenLop != null && duocLenLop.Length > 0)
                //{

                //    diemTongHopNX.DuocLenLop = (short) Int16.Parse(duocLenLop);
                //    diemTongHopNX.UserUpdated = userName;
                //    diemTongHopNX.DateUpdated = DateTime.Now;
                //}

            }
            else {

                HocSinh_TongHopNX diemNhanXet = new HocSinh_TongHopNX();
                diemNhanXet.idTruong = idTruong;
                diemNhanXet.NamHoc = namHoc;
                diemNhanXet.MaHocSinh = mahs;
                diemNhanXet.MaLop = maLop;
                //if (duocLenLop != null && duocLenLop.Length > 0)
                //{
                //    diemNhanXet.DuocLenLop = (short)Int16.Parse(duocLenLop);
                //}
                diemNhanXet.UserCreated = userName;
                diemNhanXet.DateCreated = DateTime.Now;

                db.AddToHocSinh_TongHopNX(diemNhanXet);
            }

            db.SaveChanges();
        }

        public void UpdateDataDanhGiaTongHop(int idTruong, string namHoc, string maLop, string mahs, string hocky, string maMon, string noiDungNX, string userName)
        {
            short maKy = Int16.Parse(hocky);
            var diemTongHopNX = (from dth in db.HocSinh_TongHopNX
                                 where dth.idTruong == idTruong && dth.NamHoc == namHoc && dth.MaLop == maLop && dth.HocKy == maKy
                               && dth.MaHocSinh == mahs
                                 select dth).FirstOrDefault();


            if (diemTongHopNX != null)
            {
                switch (maMon)
                {
                    case "nangluc1": diemTongHopNX.NXNangLuc1 = noiDungNX; break;
                    case "nangluc2": diemTongHopNX.NXNangLuc2 = noiDungNX; break;
                    case "nangluc3": diemTongHopNX.NXNangLuc3 = noiDungNX; break;
                    case "phamchat1": diemTongHopNX.NXPhamChat1 = noiDungNX; break;
                    case "phamchat2": diemTongHopNX.NXPhamChat2 = noiDungNX; break;
                    case "phamchat3": diemTongHopNX.NXPhamChat3 = noiDungNX; break;
                    case "phamchat4": diemTongHopNX.NXPhamChat4 = noiDungNX; break;
                    case "khenthuong": diemTongHopNX.KhenThuong = true; break; // TODO
                    case "htct": diemTongHopNX.HoanThanhCT = noiDungNX; break;

                    default:
                        {
                            System.Console.WriteLine("Other number");
                            break;
                        }
                }

                diemTongHopNX.UserUpdated = userName;
                diemTongHopNX.DateUpdated = DateTime.Now;
                db.SaveChanges();
            }

        }

         

        [HttpPost]
        public JsonResult CapNhatDiemHocSinh(List<TapHocSinhDiem> hocsinhdiem)
        {
            string mess = "NOTUPATE";
            if (hocsinhdiem!=null && hocsinhdiem.Count > 0)
            {
                foreach (var hs in hocsinhdiem)
                {
                    string hocky = hs.HocKy == "GHK1" ? "0" : hs.HocKy == "HK1" ?"1" : hs.HocKy == "GHK2" ?"2":"3" ;
                    
                    string[] HSDiem = hs.HocSinh.Split('|');
                    if (HSDiem.Count() > 0)
                    {
                        string[] mahsmon = HSDiem[0].Split('-');
                        string mahs = mahsmon[0];
                        string mamon = mahsmon[1];
                        string diem = HSDiem[1];

                        if (mamon != "NXNangLucTX1" && mamon != "NXNangLucTX2" && mamon != "NXNangLucTX3" && mamon != "NXPhamChatTX1" && mamon != "NXPhamChatTX2" && mamon != "NXPhamChatTX3" && mamon != "NXPhamChatTX4")
                        {
                            if (diem == "T")
                            {
                                diem = "HTT";
                            }
                            if (diem == "H")
                            {
                                diem = "HT";
                            }
                            if (diem == "C")
                            {
                                diem = "CHT";
                            }
                        }


                        UpdateData(mahs, mamon, diem, hs.MaLop, hocky);
                    }
                }
                mess = "OK";
            }
            
            
            return Json(mess, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult GVBMCapNhatDiemHocSinh(List<TapHocSinhDiem> hocsinhdiem)
        {
            string mess = "NOTUPATE";
            if (hocsinhdiem != null && hocsinhdiem.Count > 0)
            {
                foreach (var hs in hocsinhdiem)
                {
                    string hocky = hs.HocKy == "GHK1" ? "0" : hs.HocKy == "HK1" ? "1" : hs.HocKy == "GHK2" ? "2" : "3";

                    string[] HSDiem = hs.HocSinh.Split('|');
                    if (HSDiem.Count() > 0)
                    {
                        string[] mahsmon = HSDiem[0].Split('-');
                        string mahs = mahsmon[0];
                        string mamon = mahsmon[1].Split('_')[1];
                        string diem = HSDiem[1];

                        if (diem == "T")
                        {
                            diem = "HTT";
                        }
                        if (diem == "H")
                        {
                            diem = "HT";
                        }
                        if (diem == "C")
                        {
                            diem = "CHT";
                        }

                        UpdateData(mahs, mamon, diem, hs.MaLop, hocky);
                    }
                }
                mess = "OK";
            }


            return Json(mess, JsonRequestBehavior.AllowGet);
        }



        [AcceptVerbs(HttpVerbs.Get)]
        [Authorize]
        public ActionResult khenThuongHocSinh(string malop, string makhoi)
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Thongbao");

            }

            // lay thong tin truong 
            string userName = User.Identity.Name.ToString() == "" ? "superadmin" : User.Identity.Name.ToString();
            ScrUser userchange = (from us in db.ScrUsers
                                  where us.UserName == userName
                                  select us).FirstOrDefault();
            int idTruong = userchange.idTruong != null ? (int)userchange.idTruong : 26;
            ViewBag.idtruong = idTruong;
            bool isHieuTruong = Cmon.isHieuTruong(idTruong, userName);
            ViewBag.isHieuTruong = isHieuTruong;
            ViewBag.loaiNhapDiem = Cmon.checkLoaiNhapDiemCuaTruong(idTruong);
            ViewBag.style = Cmon.checkFormNhapDiemCuaTruong(idTruong);
            string namhoc = Cmon.GetNamHienTai(idTruong);
            var q1 = (from khoi in db.DM_Khoi
                      let makhoiList = from lophoc in db.LopHocs
                                       join userlh in db.Users_LopHoc on lophoc.MaLop equals userlh.MaLop
                                       where userlh.UserName == userName && userlh.idTruong == idTruong && userlh.NamHoc == namhoc
                                       select lophoc.MaKhoi
                      where makhoiList.Contains(khoi.MaKhoi) && khoi.idTruong == idTruong && khoi.NamHoc == namhoc
                      select khoi).ToList();

            ViewBag.danhsach = q1;
            ViewBag.namhientai = namhoc;
            if (string.IsNullOrEmpty(makhoi))
            {
                ViewBag.makhoi = new SelectList(q1, "MaKhoi", "TenKhoi");
                ViewBag.malop = new SelectList(new List<LopHoc>(), "MaLop", "TenLop", null);

            }
            else
            {
                ViewBag.makhoi = new SelectList(q1, "MaKhoi", "TenKhoi", makhoi);
                if (string.IsNullOrEmpty(malop))
                    ViewBag.malop = new SelectList(new List<LopHoc>(), "MaLop", "TenLop", null);
                else
                {
                    var lops = (from lop in db.LopHocs
                                let allowlops = from userLop in db.Users_LopHoc where userLop.UserName == userName && userLop.NamHoc == namhoc select userLop.MaLop
                                where allowlops.Contains(lop.MaLop) && lop.MaKhoi == makhoi && lop.NamHoc == namhoc
                                select lop).ToList();
                    ViewBag.malop = new SelectList(lops, "MaLop", "TenLop", malop);
                }
            }

            //  ViewBag.MaLop = new SelectList(new List<LopHoc>(), "MaLop", "TenLop", null);

            return View(@"~/Views/TongHop/khenThuongHocSinh.cshtml");

        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult LoadHocsinhtheolop(int idTruong, string malop)
        {
            string namhoc = Cmon.GetNamHienTai(idTruong);


            var dblist = (from hs in db.HocSinhs.Where(hs => hs.MaLop == malop && hs.idTruong == idTruong && hs.NamHoc == namhoc)
                          where !(from hsqt in db.HocSinh_QuaTrinh.Where(hsqt => hsqt.idTruong == idTruong && hsqt.NamHoc == namhoc && hsqt.MaLop == malop && hsqt.MaQuaTrinh == "BH")
                                  select hsqt.MaHocSinh).Contains(hs.MaHocSinh)
                          orderby hs.STT ascending
                          select new
                          {
                              MaHocSinh = hs.MaHocSinh,
                              HoTen = hs.HoTen,
                              STT = hs.STT,
                              ngaysinh = hs.NgaySinh == null ? null : hs.NgaySinh,
                              noisinh = hs.NoiSinh == null ? "" : hs.NoiSinh,

                          }).ToList();
            ViewBag.tongsohocsinh = dblist.Count();
            return Json(dblist, JsonRequestBehavior.AllowGet);


        }

        bool checkExistKhenThuong(int idTruong, string maLop, string namHoc, string maHocSinh)
        {
            var dblist = (from khenthuong in db.HocSinh_KhenThuongKyLuat where khenthuong.idTruong == idTruong  && khenthuong.MaLop == maLop && khenthuong.MaHocSinh == maHocSinh && khenthuong.NamHoc == namHoc select khenthuong).FirstOrDefault();

            if (dblist !=null) {
             return true;
            }
            return false;
        }

        //[AcceptVerbs(HttpVerbs.Get)]
        //[Authorize]
        //public ActionResult Viewhocsinh(string mahs, string malop, string makhoi, bool? inline)
        //{
        //    if (!Request.IsAuthenticated)
        //    {
        //        return RedirectToAction("Index", "Thongbao");

        //    }

        //    VIEWALSTUDENTINFOMATION viewAllINFS = new VIEWALSTUDENTINFOMATION();// hien thi toan bo thong tin cua hoc sinh 


        //    // lay thong tin truong 
        //    string userName = getUserName();
        //    int idTruong = Cmon.getIdtruong(userName, db);
        //    ViewBag.idtruong = idTruong;

        //    string namhoc = Cmon.GetNamHienTai(idTruong);
        //    HocSinh HocsinhEnties = (from hocsinh in db.HocSinhs
        //                             where hocsinh.idTruong == idTruong && hocsinh.MaLop == malop && hocsinh.MaHocSinh == mahs
        //                             join hsqt in db.HocSinh_QuaTrinh.Where(hsqt => hsqt.idTruong == idTruong && hsqt.NamHoc == namhoc && hsqt.MaLop == malop && hsqt.MaQuaTrinh != "BH")
        //                 on hocsinh.MaHocSinh equals hsqt.MaHocSinh
        //                             select hocsinh).FirstOrDefault();
        //    var hocSinhDetail = (from hocsinhDetail in db.HocSinh_Details
        //                         where hocsinhDetail.idTruong == idTruong && hocsinhDetail.NamHoc == namhoc
        //                         && hocsinhDetail.MaLop == malop
        //                         && hocsinhDetail.MaHocSinh == mahs
        //                         select hocsinhDetail).FirstOrDefault();


        //    viewAllINFS.hocSinh = HocsinhEnties;// gan cacs thong tin cua hoc sinh vao doi tuong 
        //    viewAllINFS.hocSinhDetail = hocSinhDetail;
        //    HocSinh_KhenThuongKyLuat hocSinhKyLuat = new HocSinh_KhenThuongKyLuat();

        //    if (checkExistKhenThuong(idTruong, malop, namhoc, mahs, "HK1"))
        //    {
        //        var dblist = (from khenthuong in db.HocSinh_KhenThuongKyLuat.Where(khenthuong => khenthuong.idTruong == idTruong && khenthuong.MaHocKy == "HK1" && khenthuong.MaLop == malop && khenthuong.MaHocSinh == mahs && khenthuong.NamHoc == namhoc)

        //                      select new HocSinh_KhenThuongKyLuat
        //                      {

        //                          NamHoc = khenthuong.NamHoc,
        //                          idTruong = khenthuong.idTruong,

        //                          MaLop = khenthuong.MaLop,
        //                          MaHocSinh = khenthuong.MaHocSinh,

        //                          Ngay = khenthuong.Ngay,
        //                          NoiDung = khenthuong.NoiDung,
        //                          KhenThuong = khenthuong.KhenThuong,
        //                          KhenThuongCapTruong = khenthuong.KhenThuongCapTruong

        //                      }
        //                 ).FirstOrDefault();
        //    } else 
            



        //    viewAllINFS.khenthuongs = dblist;


        //    ViewBag.namhientai = namhoc;

        //    ViewBag.NoiO_MaTinh = new SelectList(db.DM_TinhThanh.ToList(), "MaTinh", "TenTinh", HocsinhEnties.NoiO_MaTinh);

        //    var listQuanHuyen = db.DM_QuanHuyen.Where(q => q.MaTinh == HocsinhEnties.NoiO_MaTinh).ToList();
        //    ViewBag.NoiO_MaHuyen = new SelectList(listQuanHuyen, "MaHuyen", "TenHuyen", HocsinhEnties.NoiO_MaHuyen);

        //    var listXaPhuong = db.DM_XaPhuong.Where(q => q.MaHuyen == HocsinhEnties.NoiO_MaHuyen).ToList();

        //    ViewBag.NoiO_MaXa = new SelectList(listXaPhuong, "MaXa", "TenXa", HocsinhEnties.NoiO_MaXa);

        //    var listKhoiPho = db.DM_KhoiPho.Where(q => q.MaXa == HocsinhEnties.NoiO_MaXa).ToList();

        //    ViewBag.NoiO_MaKP = new SelectList(listKhoiPho, "MaKP", "TenKP", HocsinhEnties.NoiO_MaKP);


        //    ViewBag.MaDanToc = new SelectList(db.DM_DanToc.ToList(), "MaDanToc", "TenDanToc", HocsinhEnties.MaDanToc);

        //    if (hocSinhDetail != null)
        //    {

        //        ViewBag.KhuyetTat = new SelectList(db.DM_KhuyetTat.ToList(), "MaKhuyetTat", "TenKhuyetTat", hocSinhDetail.MaKhuyetTat);
        //    }
        //    else
        //    {
        //        ViewBag.KhuyetTat = new SelectList(db.DM_KhuyetTat.ToList(), "MaKhuyetTat", "TenKhuyetTat", "");
        //    }
        //    ViewBag.MaChinhSach = new SelectList(db.DM_ChinhSach.ToList(), "MaChinhSach", "TenChinhSach", HocsinhEnties.MaChinhSach);
        //    ViewBag.MaTonGiao = new SelectList(db.DM_TonGiao.ToList(), "MaTonGiao", "TenTonGiao", HocsinhEnties.MaTonGiao);
        //    ViewBag.MaDoanDoi = new SelectList(db.DM_DoanDoi.ToList(), "MaDoanDoi", "TenDoanDoi", HocsinhEnties.MaDoanDoi);


        //    ViewBag.namhientai = namhoc;
        //    ViewBag.makhoi = makhoi;
        //    ViewBag.malop = malop;
        //    ViewBag.mahocsinh = mahs;

        //    ViewBag.username = userName;

        //    ViewBag.btnId = "btnRefresh";
        //    //ViewBag.formId = "listhocsinh";

        //    ViewBag.btnId = "btnRefresh";
        //    ViewBag.formId = "formqt";

        //    if (inline.HasValue)
        //    {
        //        ViewBag.inline = true;
                 
        //        return View(@"~/Views/TongHop/Viewhocsinh.cshtml", viewAllINFS);
        //    }
        //    else
        //    {
        //        ViewBag.inline = false;
                                
        //        return View(@"~/Views/TongHop/Viewhocsinh.cshtml", viewAllINFS);
        //    }

        //}


        [AcceptVerbs(HttpVerbs.Get)]
        [Authorize]
        public ActionResult HocSinhDanhSachKhenThuong(string mahs, string malop, string makhoi, bool? inline)
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Thongbao");

            }

              // lay thong tin truong 
            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            ViewBag.idtruong = idTruong;

            string namhoc = Cmon.GetNamHienTai(idTruong);
            ViewBag.namHoc = namhoc;
            ViewBag.maLop = malop;
            ViewBag.maHocSinh = mahs;
            ViewBag.hoTenHocSinh = Cmon.getHoTenHocSinh(mahs, idTruong, malop, namhoc);
            ViewBag.makhoi = makhoi;
            ViewBag.btnId = "btnRefresh";
            ViewBag.formId = "formqt";

            var dblist = new List<HocSinhKhenThuongKyLuat>();


            if (checkExistKhenThuong(idTruong, malop, namhoc, mahs))
            {
                dblist = (from khenthuong in db.HocSinh_KhenThuongKyLuat.Where(khenthuong => khenthuong.idTruong == idTruong && khenthuong.MaLop == malop && khenthuong.MaHocSinh == mahs && khenthuong.NamHoc == namhoc)

                              select new HocSinhKhenThuongKyLuat
                              {

                                  NamHoc = khenthuong.NamHoc,
                                  idTruong = khenthuong.idTruong,

                                  MaLop = khenthuong.MaLop,
                                  MaHocSinh = khenthuong.MaHocSinh,
                                  maHK = khenthuong.MaHocKy,
                                  Ngay = khenthuong.Ngay,
                                  NoiDung = khenthuong.NoiDung,
                                  KhenThuong = khenthuong.KhenThuong,
                                  KhenThuongCapTruong = khenthuong.KhenThuongCapTruong

                              }
                         ).ToList();

                
            }
            return View(@"~/Views/TongHop/HocSinhDanhSachKhenThuong.cshtml", dblist);
              
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Authorize]
        public ActionResult reloadata(string mahs, string malop, string makhoi, bool? inline)
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Thongbao");

            }
            VIEWALSTUDENTINFOMATION viewAllINFS = new VIEWALSTUDENTINFOMATION();// hien thi toan bo thong tin cua hoc sinh 


            // lay thong tin truong 
            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            ViewBag.idtruong = idTruong;

            string namhoc = Cmon.GetNamHienTai(idTruong);
            HocSinh HocsinhEnties = (from hocsinh in db.HocSinhs
                                     where hocsinh.idTruong == idTruong && hocsinh.MaLop == malop && hocsinh.MaHocSinh == mahs
                                     select hocsinh).FirstOrDefault();

            viewAllINFS.hocSinh = HocsinhEnties;// gan cacs thong tin cua hoc sinh vao doi tuong 

            /*  var dblist = (from quatrinh in db.HocSinh_QuaTrinh
                            where quatrinh.idTruong == idTruong && quatrinh.NamHoc == namhoc && quatrinh.MaKhoi == makhoi && quatrinh.MaLop == malop && quatrinh.MaHocSinh == mahs
                            orderby quatrinh.DateCreated
                            select quatrinh
                           ).ToList();

            */
            var dblist = (from quatrinh in db.HocSinh_QuaTrinh.Where(quatrinh => quatrinh.idTruong == idTruong && quatrinh.MaKhoi == makhoi && quatrinh.MaLop == malop && quatrinh.MaHocSinh == mahs)
                          join user in db.ScrUsers on quatrinh.UserName equals user.UserName
                          orderby quatrinh.Thoigiansukien descending
                          select new QTHOCTAP
                          {
                              IDQuaTrinh = quatrinh.IDQuaTrinh,
                              NamHoc = quatrinh.NamHoc,
                              MaQuaTrinh = quatrinh.MaQuaTrinh,
                              idTruong = quatrinh.idTruong,
                              MaKhoi = quatrinh.MaKhoi,
                              MaLop = quatrinh.MaLop,
                              MaHocSinh = quatrinh.MaHocSinh,
                              tenuser = user.FullName,
                              Thoigiansukien = quatrinh.Thoigiansukien,
                              Noidung = quatrinh.Noidung,
                              UserName = quatrinh.UserName,
                              DateCreated = quatrinh.DateCreated

                          }
                      ).ToList();


            viewAllINFS.list_QTHT = dblist;
            ViewBag.NoiO_MaTinh = new SelectList(db.DM_TinhThanh.ToList(), "MaTinh", "TenTinh", HocsinhEnties.NoiO_MaTinh);

            var listQuanHuyen = db.DM_QuanHuyen.Where(q => q.MaTinh == HocsinhEnties.NoiO_MaTinh).ToList();
            ViewBag.NoiO_MaHuyen = new SelectList(listQuanHuyen, "MaHuyen", "TenHuyen", HocsinhEnties.NoiO_MaHuyen);
            var listXaPhuong = db.DM_XaPhuong.Where(q => q.MaHuyen == HocsinhEnties.NoiO_MaHuyen).ToList();
            ViewBag.NoiO_MaXa = new SelectList(listXaPhuong, "MaXa", "TenXa", HocsinhEnties.NoiO_MaXa);
            var listKhoiPho = db.DM_KhoiPho.Where(q => q.MaXa == HocsinhEnties.NoiO_MaXa).ToList();
            ViewBag.NoiO_MaKP = new SelectList(listKhoiPho, "MaKP", "TenKP", HocsinhEnties.NoiO_MaKP);

            ViewBag.MaDanToc = new SelectList(db.DM_DanToc.ToList(), "MaDanToc", "TenDanToc", HocsinhEnties.MaDanToc);
            ViewBag.MaChinhSach = new SelectList(db.DM_ChinhSach.ToList(), "MaChinhSach", "TenChinhSach", HocsinhEnties.MaChinhSach);
            ViewBag.MaTonGiao = new SelectList(db.DM_TonGiao.ToList(), "MaTonGiao", "TenTonGiao", HocsinhEnties.MaTonGiao);
            ViewBag.MaDoanDoi = new SelectList(db.DM_DoanDoi.ToList(), "MaDoanDoi", "TenDoanDoi", HocsinhEnties.MaDoanDoi);

            ViewBag.namhientai = namhoc;
            ViewBag.makhoi = makhoi;
            ViewBag.malop = malop;
            ViewBag.mahocsinh = mahs;
            ViewBag.username = userName;

            ViewBag.btnId = "btnRefresh";
            ViewBag.formId = "formqt";

            return PartialView("Viewhocsinh", viewAllINFS);


        }

        public ActionResult hienThiKhenThuong(int idTruong, string maLop, string namHoc, string maHS, string ngay)
        {

            DateTime ngaySua = DateTime.ParseExact(ngay, "dd/MM/yyyy", null);
            var thongTinKhenThuong = (from khenthuong in db.HocSinh_KhenThuongKyLuat 
                                      where khenthuong.idTruong == idTruong
                                      && khenthuong.MaLop == maLop && khenthuong.MaHocSinh == maHS 
                                      && khenthuong.NamHoc == namHoc
                                      && khenthuong.Ngay.Day == ngaySua.Day
                                      && khenthuong.Ngay.Month == ngaySua.Month
                                      && khenthuong.Ngay.Year == ngaySua.Year select khenthuong).FirstOrDefault();
             

            return PartialView(@"~/Views/TongHop/EditKhenThuong.cshtml", thongTinKhenThuong);
        }

        public ActionResult editKhenThuong(int idTruong, string maLop, string namHoc, string maHS, string ngay)
        {

            DateTime ngaySua = DateTime.ParseExact(ngay, "dd/MM/yyyy", null);
            var thongTinKhenThuong = (from khenthuong in db.HocSinh_KhenThuongKyLuat
                                      where khenthuong.idTruong == idTruong
                                      && khenthuong.MaLop == maLop && khenthuong.MaHocSinh == maHS
                                      && khenthuong.NamHoc == namHoc
                                      && khenthuong.Ngay.Day == ngaySua.Day
                                      && khenthuong.Ngay.Month == ngaySua.Month
                                      && khenthuong.Ngay.Year == ngaySua.Year
                                      select khenthuong).FirstOrDefault();


            return PartialView(@"~/Views/TongHop/EditKhenThuong.cshtml", thongTinKhenThuong);
        }


        [HttpGet]
        public JsonResult editKhenThuong(int idTruong, string maLop, string namHoc, string maHS, string ngay, string noiDung, string khenThuong, string khenThuongCapTruong, string hocKy)
        {
            string mess = "";
         
            
            string ngayKt = ngay.Split('/')[2] + "-" + ngay.Split('/')[1] + "-" + ngay.Split('/')[0];
            DateTime ngayK = DateTime.Parse(ngayKt);
            var thongTinKhenThuong = (from kt in db.HocSinh_KhenThuongKyLuat
                             where kt.idTruong == idTruong && kt.NamHoc == namHoc
                             && kt.MaLop == maLop && kt.MaHocSinh == maHS
                             && kt.Ngay == ngayK
                             select kt).FirstOrDefault();


            if (thongTinKhenThuong != null)
            {
                thongTinKhenThuong.NoiDung = noiDung;
                //thongTinKhenThuong.MaHocKy = hocKy;
                thongTinKhenThuong.KhenThuong = khenThuong == "1" ? true : false ;
                thongTinKhenThuong.KhenThuongCapTruong = khenThuongCapTruong == "1" ? true : false;
                int rs = db.SaveChanges();
                if (rs > 0)
                    mess = "OK";
                else
                    mess = "Cập nhật nhận xét không thành công!";
            }
            else
            {
                mess = "Không có thông tin cập nhật!";
            }
            //}
            return Json(mess, JsonRequestBehavior.AllowGet);
        }


        [Authorize]
        public ActionResult ThemKhenThuong(int idTruong, string makhoi, string maLop, string maHS)
        {

            string userName = getUserName();
            //int idTruong = Cmon.getIdtruong(userName, db);
            string namhoc = Cmon.GetNamHienTai(idTruong);
            ViewBag.idTruong = idTruong;
            ViewBag.namHoc = namhoc;
            ViewBag.makhoi = makhoi;
            ViewBag.maLop = maLop;
            ViewBag.maHocSinh = maHS;
            //ViewBag.hocKy = maHK == null ? "HK2" : maHK;
            
            return View(@"~/Views/TongHop/ThemKhenThuong.cshtml");

        }

          [Authorize]
        public ActionResult ThemMoiKhenThuong(int idTruong, string makhoi, string maLop, string maHS)
        {

            string userName = getUserName();
            //int idTruong = Cmon.getIdtruong(userName, db);
            string namhoc = Cmon.GetNamHienTai(idTruong);
            ViewBag.idTruong = idTruong;
            ViewBag.namHoc = namhoc;
            ViewBag.makhoi = makhoi;
            ViewBag.maLop = maLop;
            ViewBag.maHocSinh = maHS;
            //ViewBag.hocKy = maHK == null ? "HK2" : maHK;
            
            return View(@"~/Views/TongHop/ThemMoiKhenThuong.cshtml");

        }

        


        private void UpdateData(string mahocsinh, string mamon, string diem,string malop,string mahocky)
        {
            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            string namhoc = Cmon.GetNamHienTai(idTruong);
            DBHelper helper = new DBHelper();
            SqlParameter[] arParms = new SqlParameter[8];

            arParms[0] = new SqlParameter("@MaHocSinh", SqlDbType.VarChar);
            arParms[0].Value = mahocsinh.ToString();

            arParms[1] = new SqlParameter("@IdTruong", SqlDbType.VarChar);
            arParms[1].Value = idTruong.ToString();

            arParms[2] = new SqlParameter("@MaLop", SqlDbType.NVarChar);
            arParms[2].Value = malop;

            arParms[3] = new SqlParameter("@NamHoc", SqlDbType.VarChar);
            arParms[3].Value = namhoc;

            arParms[4] = new SqlParameter("@MaMonHoc", SqlDbType.VarChar);
            arParms[4].Value = mamon;

            arParms[5] = new SqlParameter("@MaHocKy", SqlDbType.VarChar);
            arParms[5].Value = mahocky;

            arParms[6] = new SqlParameter("@UserName", SqlDbType.VarChar);
            arParms[6].Value = userName;

            arParms[7] = new SqlParameter("@Diem", SqlDbType.NVarChar);
            arParms[7].Value = diem == "" ? "" : diem; //(diem.ToUpper() == "C") ? "CHT" : (diem.ToUpper() == "H") ? "HT" : (diem.ToUpper() == "T") ? "HTT" : diem.ToUpper();

            DataSet ds = helper.ExecuteDatasetPRO("spUpdateDataHocSinhDiem", arParms);
        }

        public ActionResult ViewHocSinhDiemMonHoc(string lophoc, string hocky)
        {
            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            string namhoc = Cmon.GetNamHienTai(idTruong);
            bool isGV = Cmon.isRight(userName, "GV", db);
            bool isrightGVHT = Cmon.isRightsHTAndGV(userName, "GV","HT", db);
            //bool isrightGVCN = Cmon.isGVCN(userName, "403", db);
            if (isrightGVHT == true || isGV == true)
            {
                ViewBag.allowSave = 1;
            }
            else 
            {
                ViewBag.allowSave = 0;
            }
            
            DBHelper helper = new DBHelper();
            SqlParameter[] arParms = new SqlParameter[5];
            string maHocKy = hocky == "GHK1" ? "0" : (hocky == "HK1" ? "1" : (hocky == "GHK2" ? "2" : "3"));
  

            arParms[0] = new SqlParameter("@idTruong", SqlDbType.VarChar);
            arParms[0].Value = idTruong.ToString();

            arParms[1] = new SqlParameter("@namHoc", SqlDbType.VarChar);
            arParms[1].Value = namhoc;

            arParms[2] = new SqlParameter("@maLop", SqlDbType.NVarChar);
            arParms[2].Value = lophoc;

            arParms[3] = new SqlParameter("@userName", SqlDbType.VarChar);
            arParms[3].Value = userName;

            arParms[4] = new SqlParameter("@hocky", SqlDbType.VarChar);
            arParms[4].Value = maHocKy;

            var dmmh = (from mh in db.ASSIGN_LOP_MonHoc.Where(t => t.idTruong == idTruong && t.MaLop == lophoc)
                        select new DanhMucMonHoc
                        {
                            TenMon = mh.TenMonHoc,
                            MaMon = mh.MaMonHoc,
                            TinhDiem = mh.Tinhdiem==true?1:0
                        }
                          ).ToList();
            Cmon.dmMonHoc = dmmh;


            var danhSachMonThi = (from assignLop in db.ASSIGN_LOP_MonHoc
                                  where assignLop.idTruong == idTruong && assignLop.NamHoc == namhoc
                                  && assignLop.MaLop == lophoc
                                  select new MonHocThi
                                  {
                                      maMon = assignLop.MaMonHoc,
                                      thiGHK = assignLop.ThiGHK,
                                      thiHK = assignLop.ThiHK
                                  }).ToList();


            var QuyenNhapDiem = (from RU in db.ScrJRoleUsers
                                 join RF in db.ScrJRoleFunctions
                                     on RU.RoleName equals RF.RoleName
                                 where RU.UserName == userName && RF.idTruong == idTruong
                                 && RF.FunctionID == 117 && RF.Insert_ == true && RF.Enable == true && RF.Update_ == true
                                 select RF
                           ).FirstOrDefault();

            var dsmh = (from usmon in db.Users_Monhoc where usmon.idTruong == idTruong && usmon.NamHoc == namhoc 
                            && usmon.UserName == userName && usmon.MaLop == lophoc 
                        select usmon.MaMonHoc);

            List<string> list = new List<string>();
            ViewBag.dsmh = null;
            if (dsmh != null)
            {
                ViewBag.dsmh = dsmh.ToList();

            }

            var choPhepDanhGiaLai = (from gvbmthaydiem in db.DM_Truong_Details where gvbmthaydiem.idTruong == idTruong select gvbmthaydiem).FirstOrDefault();
            ViewBag.khongchodanhgia = "0";
            if (choPhepDanhGiaLai != null && choPhepDanhGiaLai.GVBMThayDiemMon == true)
            {
                ViewBag.khongchodanhgia = "1";

            } 

            ViewBag.QuyenNhapDiem = QuyenNhapDiem !=null ? 1 : 0;
            ViewBag.DanhSachMonThi = danhSachMonThi;
            ViewBag.namhoc = namhoc;
            ViewBag.idTruong = idTruong;
            ViewBag.maLop = lophoc;
            ViewBag.isHiddenNhanxetButton = "0";
            ViewBag.hocky = hocky;

            DataSet ds = helper.ExecuteDatasetPRO("spAutoGenerateMonHocNhanXet", arParms);
            if (ds != null && ds.Tables != null && ds.Tables.Count>0)
            {
                return View(@"~/Views/TongHop/ViewHocSinhDiemMonHoc.cshtml",ds.Tables[0]);
            }
            else
            {
                ViewBag.isHiddenNhanxetButton = "1";
                return View(@"~/Views/TongHop/ViewHocSinhDiemMonHoc.cshtml", null);
            }
            
        }

        public ActionResult SoTongHopChungXuatExcell(string maLop, string mahk)
        {
            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            string namhoc = Cmon.GetNamHienTai(idTruong);
            string tenTruong = Cmon.GetnameSchool(idTruong);
            string tenLop = Cmon.GetnameClass(maLop, idTruong, namhoc);
            DBHelper helper = new DBHelper();
            SqlParameter[] arParms = new SqlParameter[4];           
           // string maHocKy = mahk == "GHK1" ? "0" : (mahk == "HK1" ? "1" : (mahk == "GHK2" ? "2" : "3"));


            arParms[0] = new SqlParameter("@idTruong", SqlDbType.VarChar);
            arParms[0].Value = idTruong.ToString();

            arParms[1] = new SqlParameter("@namHoc", SqlDbType.VarChar);
            arParms[1].Value = namhoc;

            arParms[2] = new SqlParameter("@maLop", SqlDbType.NVarChar);
            arParms[2].Value = maLop;

            arParms[3] = new SqlParameter("@maHk", SqlDbType.VarChar);
            arParms[3].Value = mahk;
            DataSet ds = helper.ExecuteDatasetPRO("spAutoGenerateMonHocExportExcell", arParms);
           
            if (ds.Tables.Count > 0)
            {
                string tenFile = "Bang_tong_hop_chung.xls";
                ExcelFileWriterBangTongHop<object> myExcel = new ExcelWriteBangTongHop();
            
                // định nghĩa thư mục chứa template
                string rootPathToTemplate = Server.MapPath("~/templateExcell/" + tenFile);

                string fileName = userName + "_bang_tong_hop_chung";

                string rootProcessTemplate = Server.MapPath(System.IO.Path.GetDirectoryName("~/ProcessTemplate/")); // thư mục chứa các template sẽ được đem xử lý đổ dữ liệu
                string fileNameExcute = rootProcessTemplate + "\\" + fileName + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xls"; // tạo một template để chuẩn bị cho xử lý
                  
                System.IO.File.Delete(fileNameExcute);// xóa file tạm trước đó và tạo lại
                System.IO.File.Copy(rootPathToTemplate, fileNameExcute, true);// copy từ thư mục template tới thư mục chuẩn bị xử lý

                // Xử lý đổ dữ liệu vào các ô tương ứng
                string tenHocKy = mahk == "HK2" ? "CẢ NĂM" : Cmon.getTenHocKy(idTruong, namhoc, mahk, db);
                myExcel.ExportBangTongHopToExcel(fileNameExcute, mahk, tenHocKy, namhoc, tenTruong, tenLop, maLop, (DataTable)ds.Tables[0]);

                System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
                Cmon.stopExcel();

                // lấy ngày giờ hiện tại của hệ thống 
                DateTime ngayHienTai = DateTime.Now;
                string ngayThang = ngayHienTai.Date.ToString("dd/MM/yyyy") + "_" + ngayHienTai.Hour.ToString() + "_" + ngayHienTai.Minute.ToString() + "_" + ngayHienTai.Second.ToString();

                return File(fileNameExcute, "application/ms-excel", "bang_tong_hop_kqdg_" + userName + "_" + ngayThang + ".xls");
            }

            
            return File("Bang_tong_hop_chung", "application/ms-excel", "bang_tong_hop_kqdg_" + userName + "_" + DateTime.Now.ToString() + ".xls");
        }

        public ActionResult gvbmViewHocSinhDiemMonHoc(string lophoc, string hocky)
        {
            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            string namhoc = Cmon.GetNamHienTai(idTruong);

            DBHelper helper = new DBHelper();
            SqlParameter[] arParms = new SqlParameter[4];
            //string maHocKy = hocky == "GHK1" ? "0" : (hocky == "HK1" ? "1" : (hocky == "GHK2" ? "2" : "3"));


            arParms[0] = new SqlParameter("@idTruong", SqlDbType.VarChar);
            arParms[0].Value = idTruong.ToString();

            arParms[1] = new SqlParameter("@namHoc", SqlDbType.VarChar);
            arParms[1].Value = namhoc;

            arParms[2] = new SqlParameter("@maLop", SqlDbType.NVarChar);
            arParms[2].Value = lophoc;

            arParms[3] = new SqlParameter("@maHk", SqlDbType.VarChar);
            arParms[3].Value = hocky;

            var dmmh = (from mh in db.ASSIGN_LOP_MonHoc.Where(t => t.idTruong == idTruong && t.MaLop == lophoc)
                        select new DanhMucMonHoc
                        {
                            TenMon = mh.TenMonHoc,
                            MaMon = mh.MaMonHoc
                            
                        }).ToList();
            Cmon.dmMonHoc = dmmh;

            var monHocTinhDiem = (hocky == "GHK1" || hocky == "GHK2") ?(from mh in db.ASSIGN_LOP_MonHoc.Where(t => t.idTruong == idTruong && t.MaLop == lophoc && t.NamHoc == namhoc && t.ThiGHK == true && t.Tinhdiem == true)
                                                                        select mh.MaMonHoc).ToList() : (from mh in db.ASSIGN_LOP_MonHoc.Where(t => t.idTruong == idTruong && t.MaLop == lophoc && t.NamHoc == namhoc && t.ThiHK == true && t.Tinhdiem == true)
                                                                                                        select mh.MaMonHoc).ToList();


            var danhSachMonThi = (from assignLop in db.ASSIGN_LOP_MonHoc
                                  where assignLop.idTruong == idTruong && assignLop.NamHoc == namhoc
                                  && assignLop.MaLop == lophoc
                                  select new MonHocThi
                                  {
                                      maMon = assignLop.MaMonHoc,
                                      thiGHK = assignLop.ThiGHK,
                                      thiHK = assignLop.ThiHK
                                  }).ToList();


            var QuyenNhapDiem = (from RU in db.ScrJRoleUsers
                                 join RF in db.ScrJRoleFunctions
                                     on RU.RoleName equals RF.RoleName
                                 where RU.UserName == userName && RF.idTruong == idTruong
                                 && RF.FunctionID == 117 && RF.Insert_ == true && RF.Enable == true && RF.Update_ == true
                                 select RF
                           ).FirstOrDefault();

            ViewBag.monHocTinhDiem = monHocTinhDiem;// đưa những môn học có tính điểm của một lớp lên UI 
            ViewBag.QuyenNhapDiem = QuyenNhapDiem != null ? 1 : 0;
            ViewBag.DanhSachMonThi = danhSachMonThi;
            ViewBag.namhoc = namhoc;
            ViewBag.idTruong = idTruong;
            ViewBag.maLop = lophoc;
            ViewBag.isHiddenNhanxetButton = "0";

            var monThiTheoKy = (from ass in db.ASSIGN_LOP_MonHoc
                          join uslm in db.Users_Monhoc
                          on new { ass.MaMonHoc, ass.MaLop } equals new { uslm.MaMonHoc, uslm.MaLop }
                          into usLopMon
                          from data in usLopMon 
                          join kmon in db.Khoi_MonHoc
                          on data.MaMonHoc equals kmon.MaMonHoc
                          where ass.idTruong == idTruong
                          && ass.NamHoc == namhoc
                          && ass.MaLop == lophoc
                          && data.UserName == userName
                          && kmon.MaHocKy == hocky
                          && kmon.ThiHK == true
                          select new MonThiHocKy
                          {
                              maMon = data.MaMonHoc,
                              thiHK = kmon.ThiHK
                              
                          }).ToList();

            // Lấy danh sách các môn học mà user đăng nhập vào có quyền được nhập
            var User_monHoc = (from userMon in db.Users_Monhoc
                               where userMon.idTruong == idTruong && userMon.NamHoc == namhoc && userMon.MaLop == lophoc && userMon.UserName == userName
                               select userMon.MaMonHoc).ToList();

            var danhSachMonHoc = (from dmMon in db.DM_MonHoc
                          join userMon in db.Users_Monhoc
                          on dmMon.MaMonHoc equals userMon.MaMonHoc
                          where userMon.idTruong == idTruong && userMon.NamHoc == namhoc && userMon.MaLop == lophoc && userMon.UserName == userName
                          orderby dmMon.MaMonHoc descending
                          select new MonHocDanhGia
                          {
                              maMonHoc = dmMon.MaMonHoc,
                              tenMonHoc = dmMon.TenMonHoc
                          }).ToList();

            ViewBag.danhSachMonHoc = danhSachMonHoc;
            ViewBag.maMon = "maMonHoc";
            ViewBag.DanhSachMonThiTheoKy = monThiTheoKy;
            ViewBag.UserMon = User_monHoc;
            DataSet ds = helper.ExecuteDatasetPRO("spAutoGenDanhGiaMonHoc", arParms);

            if (ds != null && ds.Tables != null && ds.Tables.Count > 0)
            {
                KetQuaMonDanhGia ketQuaMonDanhGia = new KetQuaMonDanhGia();
                ketQuaMonDanhGia.maMonHoc = "maMonHoc";
                ketQuaMonDanhGia.dataTable = ds.Tables[0];
                return View(@"~/Views/TongHop/gvbmViewHocSinhDiemMonHoc.cshtml", ketQuaMonDanhGia);
            }
            else
            {
                ViewBag.isHiddenNhanxetButton = "1";
                return View(@"~/Views/TongHop/gvbmViewHocSinhDiemMonHoc.cshtml", null);
            }

        }
    }
}