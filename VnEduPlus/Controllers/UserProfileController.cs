﻿using System.Linq;
using System.Web.Mvc;
using Lib.Web.Mvc.JQuery.JqGrid;
using VnEduPlus.Models;
using VnEduPlus.CmonFunction;
using System;
using System.Web.Mail;
using System.Net.Mail;
using System.Net;
using System.Configuration;

namespace VnEduPlus.Controllers
{
    public class UserProfileController : Controller
    {
        EDUNP_CAP1Entities db = new EDUNP_CAP1Entities();
        ViewModelAll viewmodel = new ViewModelAll();
        //
        // GET: /UserProfile/

        public string getUserName()
        {
            return User.Identity.Name.ToString() == "" ? "superadmin" : User.Identity.Name.ToString();

        }

        [Authorize]
        public ActionResult SendEmail()
        {
            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            ViewBag.loaiNhapDiem = Cmon.checkLoaiNhapDiemCuaTruong(idTruong);
            ViewBag.style = Cmon.checkFormNhapDiemCuaTruong(idTruong);
            bool isHieuTruong = Cmon.isHieuTruong(idTruong, userName);
            ViewBag.isHieuTruong = isHieuTruong;
            var giaovien = (from us in db.ScrUsers
                            where (us.UserName == userName)
                            select new GiaoVien
                            {
                                username=userName,
                                hoten=us.FullName,
                                sodienthoai=us.DienThoai,
                                email=us.Email,
                                diachi=us.Diachi,
                                truong=(from t in db.DM_Truong.Where(t=>t.idTruong==us.idTruong) select t).FirstOrDefault().TenTruong,
                                chucvu=(from cv in db.ScrJRoleUsers.Where(cv=>cv.UserName==userName) select cv).FirstOrDefault().RoleName,
                            }
                              ).FirstOrDefault();

            return View(@"~/Views/UserProfile/SendEmail.cshtml", giaovien);
        }

        [Authorize]
        public JsonResult SendEmailDetail(string hoten, string truong, string sodienthoai, string email, string noidung, string chucvu)
        {
            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            ViewBag.loaiNhapDiem = Cmon.checkLoaiNhapDiemCuaTruong(idTruong);
            ViewBag.style = Cmon.checkFormNhapDiemCuaTruong(idTruong);

            string _mailServer = "smtp.gmail.com";
            int _mailPort = 587;
            string _user = "noreply@netplus.vn";
            string _pass = "abcABC12345@";
            string _mailFrom = "noreply@netplus.vn";
            string _caphoc = ConfigurationSettings.AppSettings["caphoc"].ToString();

            System.Net.Mail.MailMessage mailMessage = new System.Net.Mail.MailMessage();
            SmtpClient mailClient = new SmtpClient(_mailServer, _mailPort);
            mailClient.Timeout = 15000;
            mailClient.Credentials = new NetworkCredential(_user, _pass);
            mailClient.EnableSsl = true;

            mailMessage.IsBodyHtml = true;
            mailMessage.From = new MailAddress(_mailFrom);
            mailMessage.Subject = "[" + _caphoc + " " + hoten + " " + chucvu + " trường " + truong + " gửi thông báo]";
            mailMessage.Body += "Họ tên: " + (string.IsNullOrEmpty(hoten) ? "" : hoten) + "<br/>";
            mailMessage.Body += "Trường: " + (string.IsNullOrEmpty(truong) ? "" : truong) + "<br/>";
            mailMessage.Body += "Email: " + (string.IsNullOrEmpty(email) ? "" : email) + "<br/>";
            mailMessage.Body += "SĐT: " + (string.IsNullOrEmpty(sodienthoai) ? "" : sodienthoai) + "<br/>";
            mailMessage.Body += "<br/>";
            mailMessage.Body += noidung;


            mailMessage.To.Add("CSKH@NETPLUS.VN");
            //mailMessage.To.Add("it@netplus.vn");
            //mailMessage.To.Add("contactnewsky@gmail.com");
            mailClient.Send(mailMessage);
            mailMessage.Dispose();
            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult Index()
        {
           
            // lay thong tin truong 
            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            bool isHieuTruong = Cmon.isHieuTruong(idTruong, userName);
            ViewBag.isHieuTruong = isHieuTruong;
            ViewBag.loaiNhapDiem = Cmon.checkLoaiNhapDiemCuaTruong(idTruong);
            ViewBag.style = Cmon.checkFormNhapDiemCuaTruong(idTruong);
            User_Profile data = (from user in db.User_Profile
                                 where user.UserName == userName
                                 select user).FirstOrDefault();


            ScrUser userInfo = (from us in db.ScrUsers
                                where us.UserName == userName
                                select us).FirstOrDefault();

            viewmodel.user_Profile = data;
            viewmodel.scrUser = userInfo;
            return View(viewmodel);
        }

        [HttpPost]
        [Authorize]
        public ActionResult SaveTaikhoan(string password, string userinfo)
        {

            string userName = User.Identity.Name.ToString() == "" ? "superadmin" : User.Identity.Name.ToString();
            string[] datavalues = userinfo.Split(',');

            ScrUser userchange = (from us in db.ScrUsers
                                  where us.UserName == userName
                                  select us).FirstOrDefault();
            if (datavalues[1] != null && datavalues[1].ToString().Length > 0)
            {

                if (CmonFunction.Cmon.genPassword(userName, datavalues[1]) == userchange.Pass)
                {
                    if (!string.IsNullOrEmpty(datavalues[0]))
                    {
                        userchange.Pass = CmonFunction.Cmon.genPassword(userName, datavalues[0]);
                        db.SaveChanges();
                        
                    }

                }
                else {

                    return new JqGridJsonResult() { Data = "errors" };
                }
            }

             return new JqGridJsonResult() { Data = "success" };
        }


        [HttpPost]
        [Authorize]
        public ActionResult SaveCauhinh(string userinfo)
        {
   
            string[] datavalues = userinfo.Split(',');
            string userName = User.Identity.Name.ToString() == "" ? "superadmin" : User.Identity.Name.ToString();
            User_Profile data = (from user in db.User_Profile
                                 where user.UserName == userName
                                 select user).FirstOrDefault();

            ScrUser userchange = (from us in db.ScrUsers
                                  where us.UserName == userName
                                  select us).FirstOrDefault();
            int idTruong = userchange.idTruong != null ? (int)userchange.idTruong : 26;
            if (userchange != null)
            {
                userchange.FullName = datavalues[0];
                if (!string.IsNullOrEmpty(datavalues[1]))
                    userchange.NgaySinh = DateTime.ParseExact(datavalues[1], "dd/MM/yyyy", null);
                userchange.GioiTinh = (datavalues[2] == "true");
               
                userchange.CMND = datavalues[3];
                if (!string.IsNullOrEmpty(datavalues[4]))
                    userchange.NgayCap = DateTime.ParseExact(datavalues[4], "dd/MM/yyyy", null);

                userchange.NoiCap = datavalues[5];
                userchange.DienThoai = datavalues[6];
                userchange.Email = datavalues[7];

                db.SaveChanges();

            }

            if (data != null)
            {
                data.InputType = (short)1;                
                data.SpyType = (short)1; 
                db.SaveChanges();
            }
            else
            {
                data = new User_Profile();
                data.UserName = userName;
                data.idTruong = idTruong;
                data.SpyType = (short)1; 
                data.InputType = (short)1;
               // data.ColDiemChiTietKy = ColDiemChiTietKy ? (short)1 : (short)0;
                db.AddToUser_Profile(data);
                db.SaveChanges();
            }




            return new JqGridJsonResult() { Data = "success" };
        }

    }




}
