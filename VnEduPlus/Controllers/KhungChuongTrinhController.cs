﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Lib.Web.Mvc.JQuery.JqGrid;
using VnEduPlus.CmonFunction;
using VnEduPlus.Models;
using System.Globalization;
using System.Threading;
using System.Data.SqlClient;
using System.Data;

namespace VnEduPlus.Controllers
{
    public class KhungChuongTrinhController: Controller
    {
        EDUNP_CAP1Entities db = new EDUNP_CAP1Entities();
        /// <summary>
        /// lay username dang nhap 
        /// </summary>
        /// <returns></returns>
        public string getUserName()
        {
            return User.Identity.Name.ToString() == "" ? "superadmin" : User.Identity.Name.ToString();

        }

        #region Khung chương trình cho giáo vien

        private bool CheckPermission()
        {
            string userName = getUserName();
            var _userName = (from ul in db.Users_LopHoc
                             join um in db.Users_Monhoc on new { ul.idTruong, ul.NamHoc, ul.UserName, ul.MaLop } equals new { um.idTruong, um.NamHoc, um.UserName, um.MaLop }
                             where ul.UserName == userName
                             select ul).FirstOrDefault();
            if (_userName != null && !string.IsNullOrEmpty(_userName.UserName))
                return true;
            else
                return false;
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Authorize]
        public ActionResult HienThiKhungChuongTrinh(int idTruong, string maLop, string maThang, string maMon)
        {
            if (!Request.IsAuthenticated)
            {
                return View(@"~/Views/ThongBao/Index.cshtml");

            }

             string namHoc = Cmon.GetNamHienTai(idTruong);
             string maKhoi = Cmon.getKhoi(maLop, namHoc, idTruong);
            short thang = Convert.ToInt16(maThang);
            var khungCT = (from chuongtrinh in db.KhungChuongTrinhs
                           where chuongtrinh.idTruong == idTruong
                               && chuongtrinh.MaKhoi == maKhoi && chuongtrinh.MaMonHoc == maMon && chuongtrinh.Thang == thang
                           select new ChuongTrinh
                           {
                               tuan = chuongtrinh.Tuan,
                               noidung = chuongtrinh.Noidung
                           }).ToList();

           
            return View(@"~/Views/KhungChuongTrinh/HienThiKhungChuongTrinh.cshtml", khungCT);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Authorize]
        public ActionResult HienThiKhungChuongTrinhGVCN(int idTruong, string maLop, string maThang)
        {
            if (!Request.IsAuthenticated )
            {
                return View(@"~/Views/ThongBao/Index.cshtml");

            }

            string namHoc = Cmon.GetNamHienTai(idTruong);
            string maKhoi = Cmon.getKhoi(maLop, namHoc, idTruong);
            short thang = Convert.ToInt16(maThang);

            var KhungCTCN = (from monhoc in db.ASSIGN_LOP_MonHoc
                             where monhoc.idTruong == idTruong && monhoc.NamHoc == namHoc
                                 && monhoc.MaLop == maLop
                             select new ChuongTrinhKhungLop
                             {
                                 maMon = monhoc.MaMonHoc,
                                 khungCT = (from ct in db.KhungChuongTrinhs
                                            where ct.idTruong == idTruong && ct.MaKhoi == maKhoi && ct.Thang == thang && ct.MaMonHoc== monhoc.MaMonHoc
                                            select new ChuongTrinh
                                            {
                                                tuan = ct.Tuan,
                                                noidung = ct.Noidung
                                            })
                             }).ToList();

            return View(@"~/Views/KhungChuongTrinh/HienThiTatCaKhungChuongTrinh.cshtml", KhungCTCN);
        }



 #endregion

    }
}

        