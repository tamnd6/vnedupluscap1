﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VnEduPlus.CmonFunction;
using VnEduPlus.Models;
using Lib.Web.Mvc.JQuery.JqGrid;

namespace VnEduPlus.Controllers
{
    public class QuanLyLopHocController : Controller
    {
        EDUNP_CAP1Entities db = new EDUNP_CAP1Entities();

        //
        // GET: /diemHocSinh/
        [Authorize]
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// lay username dang nhap 
        /// </summary>
        /// <returns></returns>
        public string getUserName()
        {
            return User.Identity.Name.ToString() == "" ? "superadmin" : User.Identity.Name.ToString();

        }
        public bool checkRightClass(string malop, string userName ){            
            int idTruong = Cmon.getIdtruong(userName, db);
            string namhoc = Cmon.GetNamHienTai(idTruong);
            bool result=false;
            var q1 =(from lophoc in db.LopHocs
                                 join userlh in
                                     ((from l1 in db.Users_LopHoc where l1.UserName == userName && l1.idTruong == idTruong && l1.NamHoc == namhoc select l1.MaLop).Union
                                     (from l2 in db.Users_Monhoc where l2.UserName == userName && l2.idTruong == idTruong && l2.NamHoc == namhoc select l2.MaLop))
                                 on lophoc.MaLop equals userlh
                                 where lophoc.idTruong == idTruong && lophoc.NamHoc == namhoc && lophoc.MaLop == malop
                                 orderby lophoc.MaLop
                                 select lophoc.MaLop).ToList();


            if (q1 != null) { result = true; }
            return result; 
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Authorize]
        public ActionResult thongkelophoc()
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Thongbao");
            }
            // lay thong tin truong 
            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            string namhoc = Cmon.GetNamHienTai(idTruong);
            ViewBag.loaiNhapDiem = Cmon.checkLoaiNhapDiemCuaTruong(idTruong);
            ViewBag.style = Cmon.checkFormNhapDiemCuaTruong(idTruong);

            bool isHieuTruong = Cmon.isHieuTruong(idTruong, userName);
           // bool isHieuTruong = Cmon.isHieuTruong(idTruong, "nduhieutruong");
            ViewBag.isHieuTruong = isHieuTruong;
            // DateTime ngayhientai=DateTime.Now;
            //string kyhientai = Cmon.getHocKyHienTai(ngayhientai, idTruong, namhoc);
            //Tim ngay bat dau va ngay ket thuc trong 1 hoc ky
            DateTime? ngayBD = null;
            DateTime? ngayKT = null;


            DateTime ngayHH = DateTime.Now;
            // cac truong hop lay ra ky hien tai voi thong so la ngay hien tai 
            /*1. hoc ky !=null nghia la ngay hien tai thuoc vao trog pham vi cua hoc ky-- > lay duoc ky hien tai
             * 2. ngay hien tai cua he thong nam o khoang giua 2 hoc ky=> hien thi hoc ky 1
             * 3. ngay hien tai cua he thong lon hon ngay cuoi cua hoc ky 2=> hien thi ca nam
             * 
             */
           // var HKs = "HK1";// mac dinh lay gia tri la hoc ky 1
          
            var q1 = (from lophoc in db.LopHocs
                      join userlh in
                          ((from l1 in db.Users_LopHoc where l1.UserName == userName && l1.idTruong == idTruong && l1.NamHoc == namhoc select l1.MaLop).Union
                          (from l2 in db.Users_Monhoc where l2.UserName == userName && l2.idTruong == idTruong && l2.NamHoc == namhoc select l2.MaLop))
                      on lophoc.MaLop equals userlh
                      where lophoc.idTruong == idTruong && lophoc.NamHoc == namhoc
                      orderby lophoc.MaLop

                      select new Viewthongkelophoc
                      {

                          tenGiaoVienLists = (
                                     from usrs in db.ScrUsers
                                     join ul in db.Users_LopHoc on usrs.UserName equals ul.UserName
                                     where ul.idTruong == idTruong && ul.NamHoc == namhoc && ul.MaLop == lophoc.MaLop && usrs.IsAdmin == false
                                     && (from RU in db.ScrJRoleUsers
                                         join RF in db.ScrJRoleFunctions
                                         on RU.RoleName equals RF.RoleName
                                         where RU.UserName == usrs.UserName && RF.idTruong == idTruong
                                        // && RU.RoleName=="GVCN" 
                                         && RF.Update_ == true 
                                         && RF.Read_ == true
                                         && RF.Insert_ == true
                                         && RF.FunctionID == 118
                                         select RU.UserName).Contains(usrs.UserName)
                                     
                                     select new user
                                     {
                                         fullName = usrs.FullName
                                     }),


 
                          Tengiaovien = (from users in db.ScrUsers
                                         where users.idTruong == idTruong && users.UserName == userName
                                         join userslop in db.Users_LopHoc on users.UserName equals userslop.UserName
                                         where userslop.idTruong == idTruong && userslop.MaLop == lophoc.MaLop
                                         select users.FullName).FirstOrDefault()
                                           ,
                          //maky = HKs,
                          makhoi = lophoc.MaKhoi,
                          malop =lophoc.MaLop,
                          tenLop=lophoc.TenLop,
                          siso = (from hs in db.HocSinhs.Where(hs => hs.idTruong == idTruong && hs.MaLop == lophoc.MaLop && hs.NamHoc == namhoc)
                           where !(from hsqt in db.HocSinh_QuaTrinh.Where(hsqt => hsqt.idTruong == idTruong && hsqt.NamHoc == namhoc && hsqt.MaLop == lophoc.MaLop && hsqt.MaQuaTrinh == "BH")
                                   select hsqt.MaHocSinh).Contains(hs.MaHocSinh)
                           select hs).Count(),


                          cophep = (from loainghihoc in db.HocSinh_NghiHoc
                                    where loainghihoc.MaLop == lophoc.MaLop && loainghihoc.NamHoc == namhoc && loainghihoc.idTruong == idTruong && lophoc.idTruong == idTruong &&
                                    (!ngayBD.HasValue || loainghihoc.Ngay >= ngayBD.Value) &&
                                    (!ngayKT.HasValue || loainghihoc.Ngay <= ngayKT.Value) &&
                                    lophoc.NamHoc == namhoc && (loainghihoc.LoaiNghi == "P" || loainghihoc.LoaiNghi == "CP")
                                    select loainghihoc).Count(),
                          khongphep = (from loainghihoc in db.HocSinh_NghiHoc
                                       where loainghihoc.MaLop == lophoc.MaLop && loainghihoc.NamHoc == namhoc && loainghihoc.idTruong == idTruong && lophoc.idTruong == idTruong &&
                                       (!ngayBD.HasValue || loainghihoc.Ngay >= ngayBD.Value) &&
                                       (!ngayKT.HasValue || loainghihoc.Ngay <= ngayKT.Value) &&
                                       lophoc.NamHoc == namhoc && (loainghihoc.LoaiNghi == "KP" || loainghihoc.LoaiNghi == "K")
                                       select loainghihoc).Count()


                      });


            DM_HocKy hkcn = new DM_HocKy();
            hkcn.MaHocKy = "CN";
            hkcn.TenHocKy = "Cả năm";
            var listhocky = db.DM_HocKy.Where(hk => hk.idTruong == idTruong && hk.NamHoc == namhoc).ToList();
            listhocky.Add(hkcn);

            //ViewBag.currenHocKy = HKs;
            ViewBag.MaHocKy = new SelectList(listhocky, "MaHocKy", "TenHocKy", null);
            ViewBag.idtruong = idTruong;
            ViewBag.tentruong = Cmon.GetnameSchool(idTruong);
            ViewBag.danhsach = q1;
            ViewBag.namhientai = namhoc;
            ViewBag.tendangnhap = userName;            
            ViewBag.userISGVCN = "1";
            //var userLopHoc = (from userLop in db.Users_LopHoc                                      
            //                           where userLop.idTruong == idTruong && userLop.NamHoc == namhoc && userLop.UserName == userName
            //                           select userLop) ;

            //if (userLopHoc != null && userLopHoc.ToList().Count >0)
            //{
            //    ViewBag.userISGVCN = "1";
            //}

           
            return View(q1);

        }

        [Authorize]
        public ActionResult danhsachmonhoc(string malop)
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Thongbao");

            }
            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);

            string namhoc = Cmon.GetNamHienTai(idTruong);
             
            var danhsachcacmonhoc = (from lopMonHoc in db.ASSIGN_LOP_MonHoc 
                                     join monhoc in db.DM_MonHoc
                                     on lopMonHoc.MaMonHoc equals monhoc.MaMonHoc
                                     where lopMonHoc.NamHoc == namhoc && lopMonHoc.idTruong == idTruong && lopMonHoc.MaLop == malop
                                     orderby monhoc.MaMonHoc ascending
                                     select new MonHocDS
                                     {
                                         Mamonhoc = monhoc.MaMonHoc,
                                         Tenmonhoc = monhoc.TenMonHoc,

                                         tenGiaoVienLists = (
                                             from usrs in db.ScrUsers
                                             join um in db.Users_Monhoc on usrs.UserName equals um.UserName
                                             join RU in db.ScrJRoleUsers on um.UserName equals RU.UserName
                                             join RF in db.ScrJRoleFunctions on RU.RoleName equals RF.RoleName
                                             where usrs.idTruong == idTruong && um.MaMonHoc == lopMonHoc.MaMonHoc && um.MaLop == lopMonHoc.MaLop && um.NamHoc == namhoc
                                             && RF.Enable == true
                                             && RF.Read_ == true
                                             && RF.Insert_ == true
                                             && RF.Update_ == true
                                             && RF.FunctionID == 119 // giao vien duoc quyen truy xuat doc ghi o mon hoc trong bang function no co Id=49 //bbimonhoc
                                            select new user
                                            {
                                                fullName = usrs.FullName
                                            }).Distinct()


                                     }).ToList();

           
            ViewBag.tentruong = Cmon.GetnameSchool(idTruong);
            ViewBag.tenlop = Cmon.GetnameClass(malop, idTruong, namhoc);
            ViewBag.tendangnhap = Cmon.GetnameUserName(userName, idTruong);
            ViewBag.tongso = danhsachcacmonhoc.Count();
            ViewBag.namhoc = namhoc;
            ViewBag.malop = malop;
            ViewBag.idtruong = idTruong;
            ViewBag.username = userName;
            ViewBag.PhongGiaoDuc = Cmon.getDonViChuQuan(idTruong);
            return View(@"~/Views/quanLyLopHoc/danhsachmonhoc.cshtml", danhsachcacmonhoc);


        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Authorize]
        public ActionResult ViewDanhSachMonHoc(JqGridRequest request, string malop, string userName, string namhoc, int idTruong)
        {

            int totalRecords = (from lopMonHoc in db.ASSIGN_LOP_MonHoc
                                where lopMonHoc.NamHoc == namhoc && lopMonHoc.idTruong == idTruong && lopMonHoc.MaLop == malop
                                select lopMonHoc).Count();



            //Prepare JqGridData instance
            JqGridResponse response = new JqGridResponse()
            {
                //Total pages count
                TotalPagesCount = (int)Math.Ceiling((float)totalRecords / (float)request.RecordsCount),
                //Page number
                PageIndex = request.PageIndex,
                //Total records count
                TotalRecordsCount = totalRecords
            };

            var pageIndex = request.PageIndex;
            var pageSize = request.RecordsCount;
            var pagesCount = request.PagesCount.HasValue ? request.PagesCount.Value : 1;

            var rowschuyencanList = (from lopMonHoc in db.ASSIGN_LOP_MonHoc.Where(lopMon => lopMon.NamHoc == namhoc && lopMon.idTruong == idTruong && lopMon.MaLop == malop)
                                     join monhoc in db.DM_MonHoc
                                     on lopMonHoc.MaMonHoc equals monhoc.MaMonHoc
                                     orderby monhoc.MaMonHoc ascending
                                     select new MonHocDS
                                     {
                                         Mamonhoc = monhoc.MaMonHoc,
                                         Tenmonhoc = monhoc.TenMonHoc,
                                         tenGiaoVienLists = (
                                         from usrs in db.ScrUsers
                                         join ul in db.Users_Monhoc on usrs.UserName equals ul.UserName
                                         where ul.idTruong == idTruong && ul.NamHoc == namhoc && ul.MaLop == malop && ul.MaMonHoc == monhoc.MaMonHoc


                                         let RoleName = from RU in db.ScrJRoleUsers
                                                        join RF in db.ScrJRoleFunctions
                                                        on RU.RoleName equals RF.RoleName
                                                        where RU.UserName == usrs.UserName && RF.idTruong == idTruong
                                                        && RF.FunctionID == 62 && RF.Update_ == true
                                                        select RU.UserName
                                         where RoleName.Contains(usrs.UserName)

                                         select new user
                                         {
                                             fullName = usrs.FullName
                                         })

                                     }).Skip(pageIndex * pageSize).Take(pageSize).ToList();



            // table lay du lieu kieu khac
            int stt = 1;
            foreach (MonHocDS monhoc in rowschuyencanList)
            {
                string tengv = "";
                foreach (user tengiaovien in monhoc.tenGiaoVienLists)
                {
                    tengv += tengiaovien.fullName + ",";

                }
                if (tengv != null && tengv.Length > 0)
                {
                    tengv = tengv.Substring(0, tengv.Length - 1);
                }

                response.Records.Add(new JqGridRecord(monhoc.Mamonhoc, new List<object>()
                {
					stt,
                    monhoc.Mamonhoc,
                    monhoc.Tenmonhoc,
                    tengv
                     
                }));

                stt++;
            }


            //Return data as json
            return new JqGridJsonResult() { Data = response };
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Authorize]
        public ActionResult thongKeLopHocTheoHocKy(string hocky)
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Thongbao");
            }
            // lay thong tin truong 
            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            string namhoc = Cmon.GetNamHienTai(idTruong);
            // DateTime ngayhientai=DateTime.Now;
            //string kyhientai = Cmon.getHocKyHienTai(ngayhientai, idTruong, namhoc);
            //Tim ngay bat dau va ngay ket thuc trong 1 hoc ky
            DateTime? ngayBD = null;
            DateTime? ngayKT = null;
            var HKs = "HK1";// mac dinh lay gia tri la hoc ky 1
 
            if (hocky == "CN")
            {
                HKs = "CN";
                
                //db.DM_HocKy.Where(hk => hk.MaHocKy == hocky && hk.NamHoc == namhoc && hk.idTruong == idTruong).SingleOrDefault();
                ngayBD = db.DM_HocKy.Where(hk => hk.MaHocKy == "HK1" && hk.NamHoc == namhoc && hk.idTruong == idTruong).First().NgayBatDau;
                ngayKT = db.DM_HocKy.Where(hk => hk.MaHocKy == "HK2" && hk.NamHoc == namhoc && hk.idTruong == idTruong).First().NgayKetThuc;
            }
            else {
                var HK = db.DM_HocKy.Where(hk => hk.MaHocKy == hocky && hk.NamHoc == namhoc && hk.idTruong == idTruong).SingleOrDefault();
                HKs = HK.MaHocKy;
               // ViewBag.currenHocKy = HK;
                ngayBD = HK.NgayBatDau;
                ngayKT = HK.NgayKetThuc;
            }

            var q1 = (from lophoc in db.LopHocs
                      join userlh in
                          ((from l1 in db.Users_LopHoc where l1.UserName == userName && l1.idTruong == idTruong && l1.NamHoc == namhoc select l1.MaLop).Union
                          (from l2 in db.Users_Monhoc where l2.UserName == userName && l2.idTruong == idTruong && l2.NamHoc == namhoc select l2.MaLop))
                      on lophoc.MaLop equals userlh
                      where lophoc.idTruong == idTruong && lophoc.NamHoc == namhoc
                      orderby lophoc.MaLop

                      select new Viewthongkelophoc
                      {

                          tenGiaoVienLists = (
                                    from usrs in db.ScrUsers
                                    join ul in db.Users_LopHoc on usrs.UserName equals ul.UserName
                                    where ul.idTruong == idTruong && ul.NamHoc == namhoc && ul.MaLop == lophoc.MaLop && usrs.IsAdmin == false
                                    && (from RU in db.ScrJRoleUsers
                                        join RF in db.ScrJRoleFunctions
                                        on RU.RoleName equals RF.RoleName
                                        where RU.UserName == usrs.UserName && RF.idTruong == idTruong
                                            // && RU.RoleName=="GVCN" 
                                        && RF.Update_ == true
                                        && RF.Read_ == true
                                        && RF.Insert_ == true
                                        && RF.FunctionID == 118
                                        select RU.UserName).Contains(usrs.UserName)

                                    select new user
                                    {
                                        fullName = usrs.FullName
                                    }),



                          Tengiaovien = (from users in db.ScrUsers
                                         where users.idTruong == idTruong && users.UserName == userName
                                         join userslop in db.Users_LopHoc on users.UserName equals userslop.UserName
                                         where userslop.idTruong == idTruong && userslop.MaLop == lophoc.MaLop
                                         select users.FullName).FirstOrDefault()
                                           ,
                          maky = HKs,
                          makhoi = lophoc.MaKhoi,
                          malop = lophoc.MaLop,
                          tenLop = lophoc.TenLop,
                          siso = (from hs in db.HocSinhs.Where(hs => hs.idTruong == idTruong && hs.MaLop == lophoc.MaLop && hs.NamHoc == namhoc)
                                  where !(from hsqt in db.HocSinh_QuaTrinh.Where(hsqt => hsqt.idTruong == idTruong && hsqt.NamHoc == namhoc && hsqt.MaLop == lophoc.MaLop && hsqt.MaQuaTrinh == "BH")
                                          select hsqt.MaHocSinh).Contains(hs.MaHocSinh)
                                  select hs).Count(),

                          cophep = (from loainghihoc in db.HocSinh_NghiHoc
                                    where loainghihoc.MaLop == lophoc.MaLop && loainghihoc.NamHoc == namhoc && loainghihoc.idTruong == idTruong && lophoc.idTruong == idTruong &&
                                    (!ngayBD.HasValue || loainghihoc.Ngay >= ngayBD.Value) &&
                                    (!ngayKT.HasValue || loainghihoc.Ngay <= ngayKT.Value) &&
                                    lophoc.NamHoc == namhoc && (loainghihoc.LoaiNghi == "P" || loainghihoc.LoaiNghi == "CP")
                                    select loainghihoc).Count(),
                          khongphep = (from loainghihoc in db.HocSinh_NghiHoc
                                       where loainghihoc.MaLop == lophoc.MaLop && loainghihoc.NamHoc == namhoc && loainghihoc.idTruong == idTruong && lophoc.idTruong == idTruong &&
                                       (!ngayBD.HasValue || loainghihoc.Ngay >= ngayBD.Value) &&
                                       (!ngayKT.HasValue || loainghihoc.Ngay <= ngayKT.Value) &&
                                       lophoc.NamHoc == namhoc && (loainghihoc.LoaiNghi == "KP" || loainghihoc.LoaiNghi == "K")
                                       select loainghihoc).Count()


                      });

          

            DM_HocKy hkcn = new DM_HocKy();
            hkcn.MaHocKy = "CN";
            hkcn.TenHocKy = "Cả năm";
            var listhocky = db.DM_HocKy.Where(hk => hk.idTruong == idTruong && hk.NamHoc == namhoc).ToList();
            listhocky.Add(hkcn);

            ViewBag.currenHocKy = HKs;
            ViewBag.MaHocKy = new SelectList(listhocky, "MaHocKy", "TenHocKy", null);
            ViewBag.idtruong = idTruong;
            ViewBag.tentruong = Cmon.GetnameSchool(idTruong);
            ViewBag.danhsach = q1;
            ViewBag.namhientai = namhoc;
            ViewBag.tendangnhap = userName;
            return PartialView(@"~/Views/quanLyLopHoc/thongKeLopHocTheoHocKy.cshtml", q1);
            

        }
        private static string GetTenHocKy(string hocky)
        {
            string tenhocky = "";
            if (hocky == "K1" || hocky == "HK1")
            {
                tenhocky = "Học kỳ 1";
            }
            else if (hocky == "K2" || hocky == "HK2")
            {
                tenhocky = "Học kỳ 2";
            }
            else tenhocky = "Cả năm";
            return tenhocky;
        }

      
        [AcceptVerbs(HttpVerbs.Get)]
        [Authorize]
        public ActionResult Chitietlophoc(string malop)
        {

            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            string namhoc = Cmon.GetNamHienTai(idTruong);
            var danhsachhs = (from lisths in db.HocSinhs
                              where lisths.idTruong == idTruong && lisths.NamHoc == namhoc && lisths.MaLop == malop

                              select lisths).ToList();
            ViewBag.malop = malop;
            var lop = db.LopHocs.Where(l => l.MaLop == malop).SingleOrDefault();
            ViewBag.tenlop = lop.TenLop;
            ViewBag.makhoi = lop.MaKhoi;
            //   ViewBag.malop = danhsachhs.ma;
            ViewBag.tentruong = Cmon.GetnameSchool(idTruong);
            //  ViewBag.tenlop = Cmon.GetnameClass(malop);
            ViewBag.tendangnhap = Cmon.GetnameUserName(userName, idTruong);
            ViewBag.tongso = danhsachhs.Count();
            ViewBag.namhoc = namhoc;
            return View(danhsachhs);


        }


        [AcceptVerbs(HttpVerbs.Get)]
        [Authorize]
        public JsonResult LoadLophoc(string id)
        {
            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            string namhoc = Cmon.GetNamHienTai(idTruong);


            var modelData = (from lop in db.LopHocs
                             where lop.idTruong == idTruong && lop.NamHoc == namhoc && lop.MaKhoi == id
                             let allowlops =
                             (from userLop in db.Users_LopHoc where userLop.UserName == userName && userLop.NamHoc == namhoc && userLop.idTruong == idTruong select userLop.MaLop).
                             Union(from usermon in db.Users_Monhoc where usermon.idTruong == idTruong && usermon.NamHoc == namhoc && usermon.UserName == userName select usermon.MaLop)

                             where allowlops.Contains(lop.MaLop) && lop.MaKhoi == id && lop.NamHoc == namhoc
                             select new SelectListItem()
                             {
                                 Value = lop.MaLop,
                                 Text = lop.TenLop,

                             }).ToList();


            return Json(modelData, JsonRequestBehavior.AllowGet);

        }

        [Authorize]
        public ActionResult ExportDiemDanh(int idtruong, int thang, int nam, string namhoc, string malop)
        {

            int tongsongaytrongthang = Cmon.songaytrongthang(thang, nam);
            var listHS = (from hs in db.HocSinhs.Where(hs => hs.MaLop == malop && hs.idTruong == idtruong && hs.NamHoc == namhoc)
                          orderby hs.STT ascending
                          select hs).ToList();

            DateTime ngayBd = new DateTime(nam, thang, 1);
            DateTime ngayKt = new DateTime(nam, thang, tongsongaytrongthang);
            var nghiHocs = db.HocSinh_NghiHoc.Where(hs => hs.MaLop == malop &&
                                                    hs.idTruong == idtruong && hs.NamHoc == namhoc &&
                                                    ngayBd <= hs.Ngay && hs.Ngay <= ngayKt).ToList();

            int tongsohocsinh = listHS.Count;
            List<object> dblist = new List<object>();


            var objHs = new string[tongsongaytrongthang + 3];
            objHs[0] = "SỔ ĐIỂM DANH- Tháng " + thang + " Năm " + nam + " Lớp " + malop;
            dblist.Add(objHs);

            objHs = new string[tongsongaytrongthang + 3];
            string[] Thus = new string[] { "CN", "T2", "T3", "T4", "T5", "T6", "T7" };

            objHs[0] = "STT";
            objHs[1] = "Mã học sinh";
            objHs[2] = "Họ tên";
            int thuBD = (int)ngayBd.DayOfWeek;
            for (int j = 1; j <= tongsongaytrongthang; j++)
            {
                int dayOfWeek = (thuBD + j - 1) % 7;

                objHs[j + 2] = j.ToString() + "\n" + Thus[dayOfWeek];
            }
            dblist.Add(objHs);


            for (int i = 0; i < tongsohocsinh; i++)
            {
                objHs = new string[tongsongaytrongthang + 3];
                objHs[0] = (i + 1).ToString();
                objHs[1] = listHS[i].MaHocSinh;
                objHs[2] = listHS[i].HoTen;
                for (int j = 1; j <= tongsongaytrongthang; j++)
                {
                    var nghihoc = nghiHocs.Where(nh => nh.MaHocSinh == listHS[i].MaHocSinh && nh.Ngay.Day == j).SingleOrDefault();
                    if (nghihoc != null)
                        objHs[j + 2] = nghihoc.LoaiNghi;
                    else
                        objHs[j + 2] = "";
                }
                dblist.Add(objHs);
            }

            string path = ControllerContext.HttpContext.Server.MapPath("~/diemdanh.xls");

            var truong = db.DM_Truong.Where(t => t.idTruong == idtruong).SingleOrDefault();
            var lopHoc = db.LopHocs.Where(lop => lop.MaLop == malop && lop.idTruong == idtruong && lop.NamHoc == namhoc).SingleOrDefault();
            ExcelFileWriter<object> myExcel = new DiemdanhWrite();
            myExcel.ColumnCount = tongsongaytrongthang + 3;
           // myExcel.WriteDateToExcel(path, dblist, truong.TenTruong, lopHoc.TenLop);
            System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
            return File(path, "application/octet-stream", "DiemDanh.xls");
        }

      

        [AcceptVerbs(HttpVerbs.Get)]
        [Authorize]
        public ActionResult Diemdanhhocsinh()
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Thongbao");

            }
            // lay thong tin truong 
            string userName = User.Identity.Name.ToString() == "" ? "superadmin" : User.Identity.Name.ToString();
            ScrUser userchange = (from us in db.ScrUsers
                                  where us.UserName == userName
                                  select us).FirstOrDefault();
            int idTruong = userchange.idTruong != null ? (int)userchange.idTruong : 26;
            ViewBag.idtruong = idTruong;

            string namhoc = Cmon.GetNamHienTai(idTruong);
            var q1 = (from khoi in db.DM_Khoi
                      let makhoiList = from lophoc in db.LopHocs
                                       join userlh in db.Users_LopHoc on lophoc.MaLop equals userlh.MaLop
                                       where userlh.UserName == userName && userlh.idTruong == idTruong && userlh.NamHoc == namhoc
                                       select lophoc.MaKhoi
                      where makhoiList.Contains(khoi.MaKhoi) && khoi.idTruong == idTruong && khoi.NamHoc == namhoc
                      select khoi).ToList();

            ViewBag.danhsach = q1;
            ViewBag.namhientai = namhoc;
            ViewBag.namtruoc = namhoc.Substring(0, 4);
            ViewBag.namsau = namhoc.Substring(5, 4);

            ViewBag.MaKhoi = new SelectList(q1, "MaKhoi", "TenKhoi", null);
            ViewBag.MaLop = new SelectList(new List<LopHoc>(), "MaLop", "TenLop", null);

            return View();

        }





        [AcceptVerbs(HttpVerbs.Get)]
        [Authorize]
        public ActionResult thongkechuyencan(string malop)
        {

            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            string namhoc = Cmon.GetNamHienTai(idTruong);

            ViewBag.idtruong = idTruong;
            ViewBag.tenlop = Cmon.GetnameClass(malop, idTruong, namhoc);
            ViewBag.malop = malop;
            ViewBag.tendangnhap = Cmon.GetnameUserName(userName, idTruong);
            ViewBag.namhoc = namhoc;
            return View();
        }


        [AcceptVerbs(HttpVerbs.Post)]
        [Authorize]
        public ActionResult ViewDanhSachHocSinhChuyenCan(JqGridRequest request, string malop, string namhoc, int idtruong, string fromDate, string toDate)
        {

            int totalRecords = db.HocSinhs.Where(hs => hs.MaLop == malop && hs.idTruong == idtruong && hs.NamHoc == namhoc).Count();
            //Prepare JqGridData instance
            JqGridResponse response = new JqGridResponse()
            {
                //Total pages count
                TotalPagesCount = (int)Math.Ceiling((float)totalRecords / (float)request.RecordsCount),
                //Page number
                PageIndex = request.PageIndex,
                //Total records count
                TotalRecordsCount = totalRecords
            };


            var pageIndex = request.PageIndex;
            var pageSize = request.RecordsCount;
            var pagesCount = request.PagesCount.HasValue ? request.PagesCount.Value : 1;


            DateTime? ngayBD;
            DateTime? ngayKT;
            GetThongKeRange(namhoc, idtruong, fromDate, toDate, out ngayBD, out ngayKT);

            var listHS = 
                        (from hs in db.HocSinhs.Where(hs => hs.MaLop == malop && hs.idTruong == idtruong && hs.NamHoc == namhoc)
                          where !(from hsqt in db.HocSinh_QuaTrinh.Where(hsqt => hsqt.idTruong == idtruong && hsqt.NamHoc == namhoc && hsqt.MaLop == malop && hsqt.MaQuaTrinh == "BH")
                                  select hsqt.MaHocSinh).Contains(hs.MaHocSinh)   
                         orderby hs.STT ascending 
                          select new HocSinhChuyenCan
                          {
                              Stt = hs.STT,
                              MaHocSinh = hs.MaHocSinh,
                              NgaySinh = (DateTime)hs.NgaySinh,
                              GioiTinh = hs.GioiTinh,
                              HoTen = hs.HoTen,
                              Ngaycophep = (from loainghihoc in db.HocSinh_NghiHoc
                                            where loainghihoc.MaLop == malop &&
                                            (!ngayBD.HasValue || loainghihoc.Ngay >= (DateTime)ngayBD) &&
                                            (!ngayKT.HasValue || loainghihoc.Ngay <= (DateTime)ngayKT) &&
                                            (loainghihoc.LoaiNghi == "P" || loainghihoc.LoaiNghi == "CP") && loainghihoc.MaHocSinh == hs.MaHocSinh
                                            select loainghihoc).Count(),
                              Ngaykhongphep = (from loainghihoc in db.HocSinh_NghiHoc
                                               where loainghihoc.MaLop == malop &&
                                              (!ngayBD.HasValue || loainghihoc.Ngay >= (DateTime)ngayBD) &&
                                              (!ngayKT.HasValue || loainghihoc.Ngay <= (DateTime)ngayKT) &&
                                              (loainghihoc.LoaiNghi == "KP" || loainghihoc.LoaiNghi == "K") && loainghihoc.MaHocSinh == hs.MaHocSinh
                                               select loainghihoc).Count()

                          }
                          ).Skip(pageIndex * pageSize).Take(pagesCount * pageSize).ToList();


            foreach (HocSinhChuyenCan hocsinh in listHS)
            {
                response.Records.Add(new JqGridRecord(hocsinh.MaHocSinh, new List<object>()
                {
                 	hocsinh.Stt,
                    hocsinh.HoTen ,
                    hocsinh.NgaySinh ,
					hocsinh.GioiTinh ,
					hocsinh.Ngaycophep ,
                    hocsinh.Ngaykhongphep,
				
                }));
            }


            //Return data as json
            return new JqGridJsonResult() { Data = response };
        }

        private void GetThongKeRange(string namhoc, int idtruong, string fromDate, string toDate, out DateTime? ngayBD, out DateTime? ngayKT)
        {
            ngayBD = null;
            ngayKT = null;

            if (string.IsNullOrEmpty(fromDate) && string.IsNullOrEmpty(toDate))
            {
                //Tim ngay bat dau va ngay ket thuc trong 1 hoc ky
                DateTime ngayHH = DateTime.Now;
                var hocky = db.DM_HocKy.Where(hk => hk.NgayBatDau <= ngayHH && hk.NgayKetThuc >= ngayHH && hk.NamHoc == namhoc && hk.idTruong == idtruong).SingleOrDefault();
                if (hocky != null)
                {
                    ngayBD = hocky.NgayBatDau;
                    ngayKT = hocky.NgayKetThuc;
                }
                else { // van không thuộc học kỳ nào thì lấy ngày bắt đầu là ngày bắt đầu kỳ 1, và ngày kết thúc là ngày kết thúc kỳ 2

                    ngayBD = db.DM_HocKy.Where(hk =>hk.NamHoc == namhoc && hk.idTruong == idtruong && hk.MaHocKy=="HK1").SingleOrDefault().NgayBatDau;
                    ngayKT = db.DM_HocKy.Where(hk => hk.NamHoc == namhoc && hk.idTruong == idtruong && hk.MaHocKy == "HK2").SingleOrDefault().NgayKetThuc;
                }

            }
            else
            {
                if (!string.IsNullOrEmpty(fromDate))
                    ngayBD = DateTime.ParseExact(fromDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                if (!string.IsNullOrEmpty(toDate))
                    ngayKT = DateTime.ParseExact(toDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            }
        }


    }
}
