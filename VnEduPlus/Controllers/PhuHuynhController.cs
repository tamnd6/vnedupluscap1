﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Lib.Web.Mvc.JQuery.JqGrid;
using VnEduPlus.CmonFunction;
using VnEduPlus.Models;
using System.Globalization;
using System.Threading;
using System.Data.SqlClient;
using System.Data;

namespace VnEduPlus.Controllers
{
    public class PhuHuynhController : Controller
    {
        EDUNP_CAP1Entities db = new EDUNP_CAP1Entities();
        /// <summary>
        /// lay username dang nhap 
        /// </summary>
        /// <returns></returns>
        public string getUserName()
        {
            return User.Identity.Name.ToString() == "" ? "superadmin" : User.Identity.Name.ToString();

        }

        #region Phụ huynh học sinh

        [HttpGet]
        public ActionResult danhSachPhanHoi(string malop)
        {
            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            string namhoc = Cmon.GetNamHienTai(idTruong);

            ViewBag.idtruong = idTruong;
            ViewBag.tenTruong = Cmon.GetnameSchool(idTruong);
            ViewBag.namHoc = namhoc;
            ViewBag.maLop = Cmon.GetnameClass(malop, idTruong, namhoc); ;

            var comments = (from phanhoi in db.PhuHuynh_Comments
                            join hocSinhPhuHuynh in db.HocSinh_PhuHuynh on phanhoi.UserName equals hocSinhPhuHuynh.UserName
                            join hocSinh in db.HocSinhs on hocSinhPhuHuynh.MaHocSinh equals hocSinh.MaHocSinh
                            where phanhoi.idTruong == idTruong && phanhoi.MaLop == malop && hocSinhPhuHuynh.idTruong == idTruong && hocSinh.idTruong == idTruong && hocSinh.MaLop ==malop
                            orderby phanhoi.DateCreate descending
                            select new PhuHuynhPhanHoi
                            {
                              hoTenPhuHuynh = phanhoi.FullName,
                              hoTenHocSinh = hocSinh.HoTen,
                              noiDungPhanHoi = phanhoi.Comment,
                              ngayTao = phanhoi.DateCreate
                            }
                            
                            ).ToList(); 
            return View(@"~/Views/PhuHuynh/danhSachPhanHoi.cshtml", comments);
        }
        #endregion

      
    }
        
}

        