﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Lib.Web.Mvc.JQuery.JqGrid;
using VnEduPlus.CmonFunction;
using VnEduPlus.Models;
using System.Globalization;
using System.Threading;
namespace VnEduPlus.Controllers
{
    public class KeHoachController : Controller
    {

        /// <summary>
        /// Khai bao bien toan cuc
        /// </summary>
        EDUNP_CAP1Entities db = new EDUNP_CAP1Entities();

        /// <summary>
        /// lay username dang nhap 
        /// </summary>
        /// <returns></returns>
        public string getUserName()
        {
            return User.Identity.Name.ToString() == "" ? "superadmin" : User.Identity.Name.ToString();

        }
        [Authorize]
        public ActionResult Index()
        {
            ViewBag.Message = "Chương trình ứng dụng công nghệ thông tin quản lý trường học 2012-2020!";

            return View();
        }

         
        [Authorize]
        public ActionResult TinhHinhDauNam()
        {
            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            string namHoc = Cmon.GetNamHienTai(idTruong);
            ViewBag.namHoc = namHoc;
            ViewBag.idTruong = idTruong;
            var lopHoc = (from lophoc in db.LopHocs
                          let maLop = (from userLop in db.Users_LopHoc
                                       join userMon in db.Users_Monhoc
                                       on userLop.UserName equals userMon.UserName
                                       where userLop.idTruong == idTruong && userLop.NamHoc == namHoc && userLop.UserName == userName
                                       select userLop.MaLop).Distinct()
                          where maLop.Contains(lophoc.MaLop) && lophoc.idTruong == idTruong && lophoc.NamHoc == namHoc
                          select lophoc).ToList();

            LopHoc emptyLopHoc = new LopHoc();
            emptyLopHoc.MaLop = "-1";
            emptyLopHoc.TenLop = "--Chọn lớp học--";
            lopHoc.Insert(0, emptyLopHoc);
            ViewBag.lopHoc = lopHoc;
            
            ViewBag.tenTruong = Cmon.GetnameSchool(idTruong);

            if (!Request.IsAuthenticated)
            {
                return View(@"~/Views/ThongBao/Index.cshtml");

            }

            return View(@"~/Views/KeHoach/TinhHinhDauNam.cshtml");
        }


        [Authorize]
        [HttpGet]
        public ActionResult ThongKeTinhHinhDauNam(string maLop)
        {
            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            string namHoc = Cmon.GetNamHienTai(idTruong);
            ViewBag.namHoc = namHoc;
            ViewBag.idTruong = idTruong;

            var thongKeDauNam = (from thongke in db.TK_DauNamHoc
                                 where thongke.idTruong == idTruong && thongke.NamHoc == namHoc & thongke.MaLop == maLop
                                 select thongke).FirstOrDefault();


            if (!Request.IsAuthenticated)
            {
                return View(@"~/Views/ThongBao/Index.cshtml");

            }

            ViewBag.maLop = maLop;
            ViewBag.thongbao= thongKeDauNam == null ? "Bạn chưa có thông tin về tình hình đầu năm của lớp " + Cmon.GetnameClass(maLop, idTruong,namHoc) : "";

            return PartialView(@"~/Views/KeHoach/ThongKeTinhHinhDauNam.cshtml", thongKeDauNam);
        }




        [Authorize]
        public ActionResult BanDaiDien()
        {
            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            string namHoc = Cmon.GetNamHienTai(idTruong);
            if (!Request.IsAuthenticated)
            {
                return View(@"~/Views/ThongBao/Index.cshtml");

            }

            var lopHoc = (from lophoc in db.LopHocs
                          let maLop = (from userLop in db.Users_LopHoc
                                       join userMon in db.Users_Monhoc
                                       on userLop.UserName equals userMon.UserName
                                       where userLop.idTruong == idTruong && userLop.NamHoc == namHoc && userLop.UserName == userName
                                       select userLop.MaLop).Distinct()
                          where maLop.Contains(lophoc.MaLop) && lophoc.idTruong == idTruong && lophoc.NamHoc == namHoc
                          select lophoc).ToList();

            LopHoc emptyLopHoc = new LopHoc();
            emptyLopHoc.MaLop = "-1";
            emptyLopHoc.TenLop = "--Chọn lớp học--";
            lopHoc.Insert(0, emptyLopHoc);
            ViewBag.lopHoc = lopHoc;
            ViewBag.namHoc = namHoc;
            ViewBag.idTruong = idTruong; 

            return View();
        }

        
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult DanhSachBanDaiDien (string maLop){
              
            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            string namHoc = Cmon.GetNamHienTai(idTruong);
            ViewBag.namHoc = namHoc;

            var banDaiDien = (from dshoi in db.DSHoiPhuHuynhs
                              join hs in db.HocSinhs
                              on dshoi.MaHocSinh equals hs.MaHocSinh 
                              where dshoi.idTruong == idTruong && dshoi.NamHoc == namHoc && dshoi.MaLop == maLop
                              && hs.idTruong == idTruong && hs.MaLop == maLop && hs.NamHoc == namHoc
                              select new DanhSachHoiPhuHuynh {
                                  idTruong = dshoi.idTruong,
                                  namHoc = dshoi.NamHoc,
                                  maLop = dshoi.MaLop,
                                  maHocSinh = dshoi.MaHocSinh,
                                  tenHocSinh = hs.HoTen,
                                  hoTenPH = dshoi.HoTenPH,
                                  chucDanh = dshoi.ChucDanh,
                                  diachi = dshoi.Diachi,
                                  donviCongTac = dshoi.DonviCongTac,
                                  dienThoai = dshoi.DienThoai,
                                  email = dshoi.Email
                              }
                                    ).ToList();
            var hocSinh = (from hocsinh in db.HocSinhs
                           where hocsinh.idTruong == idTruong && hocsinh.MaLop == maLop && hocsinh.NamHoc == namHoc
                           select hocsinh).ToList();


            ViewBag.maLop = maLop;
            ViewBag.hocSinh = hocSinh;
            DanhSachBanDaiDien danhSachBanDaiDien = new DanhSachBanDaiDien();
            danhSachBanDaiDien.danhSachHoiPhuHuynh = banDaiDien;
            return View(@"~/Views/KeHoach/DanhSachBanDaiDien.cshtml", danhSachBanDaiDien);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult layDuLieuPhuHuynh(string maLop, string maHS)
        {
              
            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            string namHoc = Cmon.GetNamHienTai(idTruong);
            ViewBag.namHoc = namHoc;

            var banDaiDien = (from dshoi in db.DSHoiPhuHuynhs
                                    where dshoi.idTruong == idTruong && dshoi.NamHoc == namHoc && dshoi.MaLop == maLop && dshoi.MaHocSinh == maHS
                                    select dshoi
                                    ).FirstOrDefault(); 

            var hocSinh = (from hocsinh in db.HocSinhs
                            where hocsinh.idTruong == idTruong && hocsinh.MaLop == maLop && hocsinh.NamHoc == namHoc
                           select hocsinh).ToList();


            ViewBag.hocSinh = hocSinh;
            ViewBag.maLop = maLop;

            return View(@"~/Views/KeHoach/ThongTinPhuHuynh.cshtml", banDaiDien);
        }
        

       // Thực hiện lưu lại các thông kê
        [AcceptVerbs(HttpVerbs.Post)]
       public ActionResult ThongKeDauNam (TK_DauNamHoc model)
        {
            string userName = getUserName();             
            int idTruong = Cmon.getIdtruong(userName, db);
            string namHoc = Cmon.GetNamHienTai(idTruong);
            ViewBag.namHoc = namHoc;
            string mess = "ERROR";

            //1. check xem đã tồn tại thì cập nhật
            //2. thêm mới neeusk hông tồn tại
            try
            {
                var tk = (from thongke in db.TK_DauNamHoc
                          where thongke.idTruong == idTruong && thongke.NamHoc == namHoc && thongke.MaLop == model.MaLop
                          select thongke).FirstOrDefault();

                if (tk != null)
                {
                    tk.Total_HS = model.Total_HS;
                    tk.Total_HSNu = model.Total_HSNu;
                    tk.Total_DoiVien = model.Total_DoiVien;
                    tk.Total_ConLietSi = model.Total_ConLietSi;
                    tk.Total_ConMoCoi = model.Total_ConMoCoi;
                    tk.Total_ConTB = model.Total_ConTB;
                    tk.Total_LenLopThang = model.Total_LenLopThang;
                    tk.Total_KhuyetTatHoaNhap = model.Total_KhuyetTatHoaNhap;
                    tk.Total_LuuBan = model.Total_LuuBan;
                    tk.Total_MoiVao = model.Total_MoiVao;
                    tk.Total_ThiLai_LenLop = model.Total_ThiLai_LenLop;
                    mess = "Cập nhật thành công !";
                }
                else
                {
                    TK_DauNamHoc tkdn = new TK_DauNamHoc();
                    tkdn.idTruong = idTruong;
                    tkdn.NamHoc = namHoc;
                    tkdn.MaLop = model.MaLop;
                    tkdn.Total_HS = model.Total_HS;
                    tkdn.Total_HSNu = model.Total_HSNu;
                    tkdn.Total_DoiVien = model.Total_DoiVien;
                    tkdn.Total_ConLietSi = model.Total_ConLietSi;
                    tkdn.Total_ConMoCoi = model.Total_ConMoCoi;
                    tkdn.Total_ConTB = model.Total_ConTB;
                    tkdn.Total_LenLopThang = model.Total_LenLopThang;
                    tkdn.Total_KhuyetTatHoaNhap = model.Total_KhuyetTatHoaNhap;
                    tkdn.Total_LuuBan = model.Total_LuuBan;
                    tkdn.Total_MoiVao = model.Total_MoiVao;
                    tkdn.Total_ThiLai_LenLop = model.Total_ThiLai_LenLop;
                    db.AddToTK_DauNamHoc(tkdn);
                    mess = "Thêm mới thành công !";

                }
                db.SaveChanges();
                  
            }
            catch (Exception ex)
            {
                //mess = "Dữ liệu bạn đưa vào thống kê có vấn đề, liên hệ với quản trị để biết thêm thông tin";
                mess = "ERROR";
            }

            return Json(mess, JsonRequestBehavior.AllowGet);
        }

        // Thực hiện lưu lại các thông kê
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult LuuThongTinDaiDien(DSHoiPhuHuynh model)
        {
            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            string namHoc = Cmon.GetNamHienTai(idTruong);
            ViewBag.namHoc = namHoc;
            string mess = "ERROR";

            //1. check xem đã tồn tại thì cập nhật
            //2. thêm mới neeusk hông tồn tại
            try
            {
                var tk = (from phuhuynh in db.DSHoiPhuHuynhs
                          where phuhuynh.idTruong == idTruong && phuhuynh.NamHoc == namHoc && phuhuynh.MaLop == model.MaLop && phuhuynh.MaHocSinh == model.MaHocSinh

                          select phuhuynh).FirstOrDefault();

                if (tk != null)
                {
                    tk.HoTenPH = model.HoTenPH;
                    tk.ChucDanh = model.ChucDanh;
                    tk.Diachi = model.Diachi;
                    tk.DonviCongTac = model.DonviCongTac;
                    tk.DienThoai = model.DienThoai;
                    tk.Email = model.Email;
                    tk.MaHocSinh = model.MaHocSinh;
                     
                    mess = "Cập nhật thành công !";
                }
               
                db.SaveChanges();

            }
            catch (Exception ex)
            {
                //mess = "Dữ liệu bạn đưa vào thống kê có vấn đề, liên hệ với quản trị để biết thêm thông tin";
                mess = "ERROR";
            }

            return Json(mess, JsonRequestBehavior.AllowGet);
        }

        // Thực hiện lưu lại các thông kê
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ThemMoiThongTinDaiDien(DSHoiPhuHuynh model)
        {
            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            string namHoc = Cmon.GetNamHienTai(idTruong);
            ViewBag.namHoc = namHoc;
            string mess = "ERROR";
             
            try
            {

                DSHoiPhuHuynh hoiPhuHuynh = new DSHoiPhuHuynh();
                hoiPhuHuynh.idTruong = idTruong;
                hoiPhuHuynh.NamHoc = namHoc;
                hoiPhuHuynh.MaLop = model.MaLop;
                hoiPhuHuynh.MaHocSinh = model.MaHocSinh;
                hoiPhuHuynh.HoTenPH = model.HoTenPH;
                hoiPhuHuynh.ChucDanh = model.ChucDanh;
                hoiPhuHuynh.Diachi = model.Diachi;
                hoiPhuHuynh.DonviCongTac = model.DonviCongTac;
                hoiPhuHuynh.DienThoai = model.DienThoai;
                hoiPhuHuynh.Email = model.Email;
                db.AddToDSHoiPhuHuynhs(hoiPhuHuynh);
                    mess = "Thêm mới thành công !"; 
                db.SaveChanges();

            }
            catch (Exception ex)
            {
                //mess = "Dữ liệu bạn đưa vào thống kê có vấn đề, liên hệ với quản trị để biết thêm thông tin";
                mess = "ERROR";
            }

            return Json(mess, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult XoaThongTinDaiDien(string maLop, string maHs)
        {
            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            string namHoc = Cmon.GetNamHienTai(idTruong);
            ViewBag.namHoc = namHoc;
            string mess = "ERROR";

            try
            {
                var hoiPH = (from hoiph in db.DSHoiPhuHuynhs
                             where hoiph.idTruong == idTruong && hoiph.NamHoc == namHoc && hoiph.MaLop == maLop && hoiph.MaHocSinh == maHs
                             select hoiph).FirstOrDefault();
                if (hoiPH != null)
                {
                    db.DSHoiPhuHuynhs.DeleteObject(hoiPH);
                    int del = db.SaveChanges();
                    if (del > 0)
                        mess = "OK";
                    
                } 

            }
            catch (Exception ex)
            {
                //mess = "Dữ liệu bạn đưa vào thống kê có vấn đề, liên hệ với quản trị để biết thêm thông tin";
                mess = "ERROR";
            }

            return Json(mess, JsonRequestBehavior.AllowGet);
        }


        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult XoaTatCaThongTinDaiDienTheoLopHoc(string maLop)
        {
            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            string namHoc = Cmon.GetNamHienTai(idTruong);
            ViewBag.namHoc = namHoc;
            string mess = "ERROR";

            try
            {
                var hoiPH = (from hoiph in db.DSHoiPhuHuynhs
                            where hoiph.idTruong == idTruong && hoiph.NamHoc == namHoc && hoiph.MaLop == maLop
                            select hoiph).ToList();
                if (hoiPH != null)
                {
                    foreach(DSHoiPhuHuynh ds in hoiPH)
                    {
                         db.DSHoiPhuHuynhs.DeleteObject(ds);
                    }
                    int del = db.SaveChanges();
                    if (del > 0)
                        mess = "OK";

                }

            }
            catch (Exception ex)
            {
                //mess = "Dữ liệu bạn đưa vào thống kê có vấn đề, liên hệ với quản trị để biết thêm thông tin";
                mess = "ERROR";
            }

            return Json(mess, JsonRequestBehavior.AllowGet);
        }


        [Authorize]
        public ActionResult KeHoachChuNhiem()
        {

            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            string namHoc = Cmon.GetNamHienTai(idTruong);
            if (!Request.IsAuthenticated)
            {
                return View(@"~/Views/ThongBao/Index.cshtml");

            }

			ViewBag.rightAccess = Cmon.getRole(userName, idTruong, 131, db);

            var lopHoc = (from lophoc in db.LopHocs
                          let maLop = (from userLop in db.Users_LopHoc
                                       
                                       where userLop.idTruong == idTruong && userLop.NamHoc == namHoc && userLop.UserName == userName
                                       select userLop.MaLop).Distinct()
                          where maLop.Contains(lophoc.MaLop) && lophoc.idTruong == idTruong && lophoc.NamHoc == namHoc
                          select lophoc).ToList();

            string maLopMacDinh = maLopMacDinh = (lopHoc != null && lopHoc.Count > 0) ? lopHoc[0].MaLop.ToString() : "";

            LopHoc emptyLopHoc = new LopHoc();
            emptyLopHoc.MaLop = "-1";
            emptyLopHoc.TenLop = "--Chọn lớp học--";
            lopHoc.Insert(0, emptyLopHoc);
            ViewBag.lopHoc = lopHoc;
            ViewBag.namHoc = namHoc;
            
            ViewBag.maLopMacDinh = maLopMacDinh;
            ViewBag.thoiDiem = "caNam";
            ViewBag.noiDungKeHoach = "";
             var keHoach = (from kh in db.KeHoachChuNhiems
                            where kh.idTruong == idTruong && kh.NamHoc == namHoc && kh.MaLop == maLopMacDinh
                               select kh).FirstOrDefault();
            
             if (keHoach != null)
             {
                 ViewBag.noiDungKeHoach = keHoach != null ? keHoach.NoidungCaNam : "";
                
             }

            return View();
        }

        [Authorize]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult NoiDungKeHoachChuNhiem(string maLop, string thoiDiem)
        {
            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            string namHoc = Cmon.GetNamHienTai(idTruong);
            string noiDungKeHoach= "";

            var keHoach = (from kh in db.KeHoachChuNhiems
                               where kh.idTruong == idTruong && kh.NamHoc == namHoc && kh.MaLop == maLop
                               select kh).FirstOrDefault();

            // lay noi dung ke hoach cua thoi diem hien tai hien len 

            if (keHoach != null)
            {
                if (thoiDiem == null || thoiDiem.Equals("caNam")) // Hiện thị mặc định
                {
                    noiDungKeHoach = keHoach != null ? keHoach.NoidungCaNam : "";

                }

                if (thoiDiem.Equals("hk1"))
                {
                    noiDungKeHoach = keHoach.NoidungHK1 == null ? "" : keHoach.NoidungHK1;
                }

                if (thoiDiem.Equals("hk2"))
                {
                    noiDungKeHoach = keHoach.NoidungHK2 == null ? "" : keHoach.NoidungHK2;
                }

                if (thoiDiem.Equals("t8"))
                {
                    noiDungKeHoach = keHoach.NoidungThang8 == null ? "" : keHoach.NoidungThang8;
                }

                if (thoiDiem.Equals("t9"))
                {
                    noiDungKeHoach = keHoach.NoidungThang9 == null ? "" : keHoach.NoidungThang9;
                }

                if (thoiDiem.Equals("t10"))
                {
                    noiDungKeHoach = keHoach.NoidungThang10 == null ? "" : keHoach.NoidungThang10;
                }

                if (thoiDiem.Equals("t11"))
                {
                    noiDungKeHoach = keHoach.NoidungThang11 == null ? "" : keHoach.NoidungThang11;
                }

                if (thoiDiem.Equals("t12"))
                {
                    noiDungKeHoach = keHoach.NoidungThang12 == null ? "" : keHoach.NoidungThang12;
                }

                if (thoiDiem.Equals("t1"))
                {
                    noiDungKeHoach = keHoach.NoidungThang1 == null ? "" : keHoach.NoidungThang1;
                }
                if (thoiDiem.Equals("t2"))
                {
                    noiDungKeHoach = keHoach.NoidungThang2 == null ? "" : keHoach.NoidungThang2;
                }
                if (thoiDiem.Equals("t3"))
                {
                    noiDungKeHoach = keHoach.NoidungThang3 == null ? "" : keHoach.NoidungThang3;
                }
                if (thoiDiem.Equals("t4"))
                {
                    noiDungKeHoach = keHoach.NoidungThang4 == null ? "" : keHoach.NoidungThang4;
                }
                if (thoiDiem.Equals("t5"))
                {
                    noiDungKeHoach = keHoach.NoidungThang5 == null ? "" : keHoach.NoidungThang5;
                }
            }
            else { // nếu chưa tồn tại giá trị nào cả  
                noiDungKeHoach = "";
            }
           

            return Json(noiDungKeHoach, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult LuuKeHoachChuNhiem(string maLop, string thoiGian, string noiDung)
        { 
            //1. Kieem tra xem noi dung dua vao da co hay chua
            //2. Neu chua co thi thuc hien viec chen moi, neu co roi thi thuc hien cap nhat


            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            string namHoc = Cmon.GetNamHienTai(idTruong);
            string ketqua = "";
            

            var keHoach = (from kh in db.KeHoachChuNhiems
                           where kh.idTruong == idTruong && kh.NamHoc == namHoc && kh.MaLop == maLop
                           select kh).FirstOrDefault();
            if (keHoach == null)
            {
              ketqua= themMoiKeHoachGiaoVienChuNhiem(maLop, idTruong, namHoc, thoiGian, noiDung);
            }
            else
            {
             ketqua= EditKeHoachGiaoVienChuNhiem(maLop, idTruong, namHoc, thoiGian, noiDung);
            }

            return Json(ketqua, JsonRequestBehavior.AllowGet);
            
        }


        public string themMoiKeHoachGiaoVienChuNhiem(string maLop, int idTruong, string namHoc, string thoiGian,string noiDung)
        {
            string mess = "Thêm mới dữ liệu thành công !";
            try
            {

                KeHoachChuNhiem keHoach = new KeHoachChuNhiem();
                keHoach.idTruong = idTruong;
                keHoach.MaLop = maLop;
                keHoach.NamHoc = namHoc;

                if (thoiGian.Equals("caNam"))
                {
                    keHoach.NoidungCaNam = noiDung;
                }

                if (thoiGian.Equals("hk1"))
                {
                    keHoach.NoidungHK1 = noiDung;
                }

                if (thoiGian.Equals("hk2"))
                {
                    keHoach.NoidungHK2 = noiDung;
                }

                if (thoiGian.Equals("t8"))
                {
                    keHoach.NoidungThang8 = noiDung;
                }

                if (thoiGian.Equals("t9"))
                {
                    keHoach.NoidungThang9 = noiDung;
                }

                if (thoiGian.Equals("t10"))
                {
                    keHoach.NoidungThang10 = noiDung;
                }

                if (thoiGian.Equals("t11"))
                {
                    keHoach.NoidungThang11 = noiDung;
                }

                if (thoiGian.Equals("t12"))
                {
                    keHoach.NoidungThang12 = noiDung;
                }

                if (thoiGian.Equals("t1"))
                {
                    keHoach.NoidungThang1 = noiDung;
                }
                if (thoiGian.Equals("t2"))
                {
                    keHoach.NoidungThang2 = noiDung;
                }
                if (thoiGian.Equals("t3"))
                {
                    keHoach.NoidungThang3 = noiDung;
                }
                if (thoiGian.Equals("t4"))
                {
                    keHoach.NoidungThang4 = noiDung;
                }
                if (thoiGian.Equals("t5"))
                {
                    keHoach.NoidungThang5 = noiDung;
                }

                db.AddToKeHoachChuNhiems(keHoach);
                db.SaveChanges();
            }
            catch
            {
                mess = "Thêm mới dữ liệu không thành công !";
            }
            return mess;
        }

        public string EditKeHoachGiaoVienChuNhiem(string maLop, int idTruong, string namHoc, string thoiGian, string noiDung)
        {

            var keHoach = (from kh in db.KeHoachChuNhiems
                           where kh.idTruong == idTruong && kh.NamHoc == namHoc && kh.MaLop == maLop
                           select kh).FirstOrDefault();


            string mess = "Cập nhật dữ liệu thành công !";
            try
            { 
                if (thoiGian.Equals("caNam"))
                {
                    keHoach.NoidungCaNam = noiDung;
                }

                if (thoiGian.Equals("hk1"))
                {
                    keHoach.NoidungHK1 = noiDung;
                }

                if (thoiGian.Equals("hk2"))
                {
                    keHoach.NoidungHK2 = noiDung;
                }

                if (thoiGian.Equals("t8"))
                {
                    keHoach.NoidungThang8 = noiDung;
                }

                if (thoiGian.Equals("t9"))
                {
                    keHoach.NoidungThang9 = noiDung;
                }

                if (thoiGian.Equals("t10"))
                {
                    keHoach.NoidungThang10 = noiDung;
                }

                if (thoiGian.Equals("t11"))
                {
                    keHoach.NoidungThang11 = noiDung;
                }

                if (thoiGian.Equals("t12"))
                {
                    keHoach.NoidungThang12 = noiDung;
                }

                if (thoiGian.Equals("t1"))
                {
                    keHoach.NoidungThang1 = noiDung;
                }
                if (thoiGian.Equals("t2"))
                {
                    keHoach.NoidungThang2 = noiDung;
                }
                if (thoiGian.Equals("t3"))
                {
                    keHoach.NoidungThang3 = noiDung;
                }
                if (thoiGian.Equals("t4"))
                {
                    keHoach.NoidungThang4 = noiDung;
                }
                if (thoiGian.Equals("t5"))
                {
                    keHoach.NoidungThang5 = noiDung;
                }

                db.SaveChanges();
            }
            catch
            {
                mess = "Cập nhật dữ liệu không thành công !";
            }
            return mess;
        }

       
    }
    
}
