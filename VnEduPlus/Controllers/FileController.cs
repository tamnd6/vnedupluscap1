﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VnEduPlus.Models;
using VnEduPlus.CmonFunction;

namespace TestAjaxUpload
{
    /// <summary>
    /// File Controller
    /// </summary>
    public class FileController : Controller
    {
        #region Actions

        /// <summary>
        /// Uploads the file.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public virtual ActionResult UploadFileFolder(string _folder, string _filename)
        {
            HttpPostedFileBase myFile = Request.Files["MyFile"];
            bool isUploaded = false;
            string message = "File upload failed";
            string _imagepath="";
            if (myFile != null && myFile.ContentLength != 0)
            {
                if (string.IsNullOrEmpty(_folder)) _folder = "Default";
                if (string.IsNullOrEmpty(_filename)) _folder = "namedefault";
                string pathForSaving = Server.MapPath("~/Content/uploads/" + _folder);
                if (this.CreateFolderIfNeeded(pathForSaving))
                {
                    try
                    {
                        //myFile.SaveAs(Path.Combine(pathForSaving, myFile.FileName));
                        myFile.SaveAs(Path.Combine(pathForSaving, myFile.FileName));
                        _imagepath = pathForSaving + "/" + myFile.FileName;
                        isUploaded = true;
                        message = _imagepath;//"File uploaded successfully!";
                    }
                    catch (Exception ex)
                    {
                        message = string.Format("File upload failed: {0}", ex.Message);
                    }
                }
            }
            return Json(new { isUploaded = isUploaded, message = message}, "text/html");
        }

        [HttpPost]
        [Authorize]
        public virtual ActionResult UploadFile()
        {
            HttpPostedFileBase myFile = Request.Files["MyFile"];
            string folder = Request.QueryString["folder"].ToString();
            string filename=Request.QueryString["filename"].ToString();
            string truong = Request.QueryString["truong"].ToString();
            bool isUploaded = false;
            string message = "File upload failed";
            string _imagepath = "";

            if (myFile != null && myFile.ContentLength != 0)
            {
                //string pathForSaving = Server.MapPath("~/Uploads/");
                string pathForSaving = Server.MapPath("~/Content/uploads/" + truong + "/");
                if (this.CreateFolderIfNeeded(pathForSaving))
                {
                    try
                    {
                        myFile.SaveAs(Path.Combine(pathForSaving, filename + Path.GetExtension(myFile.FileName)));
                        string ApplicationPath = (!string.IsNullOrWhiteSpace(Request.ApplicationPath) && Request.ApplicationPath != "/") ? Request.ApplicationPath : "";
                        string Domain = Request.Url.Scheme + System.Uri.SchemeDelimiter + Request.Url.Host + (Request.Url.IsDefaultPort ? "" : ":" + Request.Url.Port);
                        string link = "";
                        link = Domain + ApplicationPath;
                        //Get full path
                        _imagepath = link + "/Content/uploads/" + truong + "/" + filename + Path.GetExtension(myFile.FileName);
                        isUploaded = true;
                        message = _imagepath;//_imagepath;//"File uploaded successfully!";
                        
                    }
                    catch (Exception ex)
                    {
                        message = string.Format("File upload failed: {0}", ex.Message);
                    }
                }
            }
            return Json(new { isUploaded = isUploaded, message = message }, "text/html");
        }

        public string getCurrentYear()
        {
            return DateTime.Now.Year.ToString();
        }
         

        [HttpPost]
        [Authorize]
        public virtual ActionResult UploadFileSoChuNhiem()
        {
            HttpPostedFileBase myFile = Request.Files["item.MyFile"];
            string maLop = Request.QueryString["maLop"].ToString();
            string filename = Request.QueryString["filename"].ToString();
            string truong = Request.QueryString["truong"].ToString();
            string namHienTai = Request.QueryString["namHienTai"];
            namHienTai = namHienTai == null ? DateTime.Now.Year.ToString() + "-" + (DateTime.Now.Year + 1).ToString() : namHienTai.ToString();
            bool isUploaded = false;
            string message = "File upload failed";
            string _fileBaoCaopath = "";

            if (myFile != null && myFile.ContentLength != 0)
            {
                //string pathForSaving = Server.MapPath("~/Uploads/");
                string pathForSaving = Server.MapPath("~/Content/uploads/" + truong + "/FileBaoCao/" + maLop);
                if (this.CreateFolderIfNeeded(pathForSaving))
                {
                    try
                    {
                        myFile.SaveAs(Path.Combine(pathForSaving, namHienTai + "_" + filename + Path.GetExtension(myFile.FileName)));
                        string ApplicationPath = (!string.IsNullOrWhiteSpace(Request.ApplicationPath) && Request.ApplicationPath != "/") ? Request.ApplicationPath : "";
                        string Domain = Request.Url.Scheme + System.Uri.SchemeDelimiter + Request.Url.Host + (Request.Url.IsDefaultPort ? "" : ":" + Request.Url.Port);
                        string link = "";
                        link = Domain + ApplicationPath;
                        //Get full path
                        _fileBaoCaopath = link + "/Content/uploads/" + truong + "/FileBaoCao/" + maLop + "/" + namHienTai + "_" + filename + Path.GetExtension(myFile.FileName);
                        isUploaded = true;
                        message = _fileBaoCaopath;//_imagepath;//"File uploaded successfully!";

                    }
                    catch (Exception ex)
                    {
                        message = string.Format("File upload failed: {0}", ex.Message);
                    }
                }
            }
            return Json(new { isUploaded = isUploaded, message = message }, "text/html");
        }


        [HttpPost]
        [Authorize]
        public virtual ActionResult HieuTruongUploadFileMau()
        {
            HttpPostedFileBase myFile = Request.Files["MyFile"];
            string fileName = Request.QueryString["fileName"].ToString(); ;
            string truong = Request.QueryString["truong"].ToString();
            bool isUploaded = false;
            string message = "File upload failed";
            string _fileBaoCaopath = "";

            if (myFile != null && myFile.ContentLength != 0)
            {
                //string pathForSaving = Server.MapPath("~/Uploads/");
                string pathForSaving = Server.MapPath("~/Content/uploads/" + truong + "/TaiLieuMau/");
                if (this.CreateFolderIfNeeded(pathForSaving))
                {
                    try
                    {
                        myFile.SaveAs(Path.Combine(pathForSaving, fileName + Path.GetExtension(myFile.FileName)));
                        string ApplicationPath = (!string.IsNullOrWhiteSpace(Request.ApplicationPath) && Request.ApplicationPath != "/") ? Request.ApplicationPath : "";
                        string Domain = Request.Url.Scheme + System.Uri.SchemeDelimiter + Request.Url.Host + (Request.Url.IsDefaultPort ? "" : ":" + Request.Url.Port);
                        string link = "";
                        link = Domain + ApplicationPath;
                        //Get full path
                        _fileBaoCaopath = link + "/Content/uploads/" + truong + "/TaiLieuMau/" + fileName + Path.GetExtension(myFile.FileName);
                        isUploaded = true;
                        message = _fileBaoCaopath;//_imagepath;//"File uploaded successfully!";

                    }
                    catch (Exception ex)
                    {
                        message = string.Format("File upload failed: {0}", ex.Message);
                    }
                }
            }
            return Json(new { isUploaded = isUploaded, message = message }, "text/html");
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Creates the folder if needed.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <returns></returns>
        private bool CreateFolderIfNeeded(string path)
        {
            bool result = true;
            if (!Directory.Exists(path))
            {
                try
                {
                    Directory.CreateDirectory(path);
                }
                catch (Exception)
                {
                    /*TODO: You must process this exception.*/
                    result = false;
                }
            }
            return result;
        }

        #endregion
    }
}
