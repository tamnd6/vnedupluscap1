﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Web.Mvc;
using VnEduPlus.CmonFunction;
using VnEduPlus.Models;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
 


namespace VnEduPlus.Controllers
{
    /// <summary>
    /// Chuc nang nay se thuc hien tao ra cac report phuc vu cho cac chuc nang khac nhau 
    /// </summary>
    public class PhieuLienLacController : Controller
    {
        //
        // GET: /ReportResult/
        EDUNP_CAP1Entities db = new EDUNP_CAP1Entities();


        private string GetUserName()
        {
            string userName = User.Identity.Name.ToString() == "" ? "superadmin" : User.Identity.Name.ToString();
            return userName;
        }

        private void GetKhoangTG(string hocky, string namhoc, int idTruong, out DateTime? ngayBD, out DateTime? ngayKT)
        {
            ngayBD = null;
            ngayKT = null;
           // if (hocky == "CN")// neu la thoi gian ca nam thi se lay ngay bat dau hoc la ngay bat dau hoc ky 1 va ngay ket thu la ngay ket thuc hoc ky 2
            if (hocky == "HK2")// neu la thoi gian ca nam thi se lay ngay bat dau hoc la ngay bat dau hoc ky 1 va ngay ket thu la ngay ket thuc hoc ky 2
            {
                ngayBD = (DateTime)db.DM_HocKy.Where(hk => hk.MaHocKy == "HK1" &&
                                              hk.NamHoc == namhoc && hk.idTruong == idTruong).FirstOrDefault().NgayBatDau;

                ngayKT = (DateTime)db.DM_HocKy.Where(hk => hk.MaHocKy == "HK2" &&
                                              hk.NamHoc == namhoc && hk.idTruong == idTruong).FirstOrDefault().NgayKetThuc;


            }
            else
            {

                var mahocky = db.DM_HocKy.Where(hk => hk.MaHocKy == hocky &&
                                                hk.NamHoc == namhoc && hk.idTruong == idTruong).SingleOrDefault();
                if (mahocky != null)
                {
                    ngayBD = mahocky.NgayBatDau;
                    ngayKT = mahocky.NgayKetThuc;
                }
            }
        }

        private static string GetTenHocKy(string hocky)
        {
            string tenhocky = "";
            if (hocky == "K1" || hocky == "HKI" || hocky == "HK1")
            {
                tenhocky = "Học kỳ I";
            }
            else if (hocky == "K2" || hocky == "HKII" || hocky == "HK2")
            {
                tenhocky = "Học kỳ II";
            }
            else tenhocky = "Cả năm";
            return tenhocky;
        }


        /// <summary>
        /// Thông kê tổng hợp giáo dục
        /// </summary>
        /// <param name="namhoc"></param>
        /// <param name="idTruong"></param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Get)]
        [Authorize]
        public ActionResult SoLienLac()
        {
            if (Request.IsAuthenticated)
            {

                string userName = GetUserName();
                ScrUser user = (from us in db.ScrUsers
                                where us.UserName == userName
                                select us).FirstOrDefault();

                int idTruong = user.idTruong != null ? (int)user.idTruong : 26;
                string namhoc = Cmon.GetNamHienTai(idTruong);
                bool isHieuTruong = Cmon.isHieuTruong(idTruong, userName);
                ViewBag.isHieuTruong = isHieuTruong;
                ViewBag.loaiNhapDiem = Cmon.checkLoaiNhapDiemCuaTruong(idTruong);
                ViewBag.style = Cmon.checkFormNhapDiemCuaTruong(idTruong);
                var listkhoi = (from khoi in db.DM_Khoi
                                let makhoiList = from lophoc in db.LopHocs
                                                 join userlh in db.Users_LopHoc on lophoc.MaLop equals userlh.MaLop
                                                 where userlh.UserName == userName && userlh.idTruong == idTruong && userlh.NamHoc == namhoc
                                                 select lophoc.MaKhoi
                                where makhoiList.Contains(khoi.MaKhoi) && khoi.idTruong == idTruong && khoi.NamHoc == namhoc
                                select khoi).ToList();

                ViewBag.makhoi = new SelectList(listkhoi, "MaKhoi", "TenKhoi");
                ViewBag.malop = new SelectList(new List<LopHoc>(), "MaLop", "TenLop", null);
                ViewBag.tentruong = Cmon.GetnameSchool(idTruong);
                ViewBag.tendangnhap = Cmon.GetnameUserName(userName, idTruong);
                ViewBag.namhoc = namhoc;
                ViewBag.idtruong = idTruong;
                var listhocky = db.DM_HocKy.Where(hk => hk.idTruong == idTruong && hk.NamHoc == namhoc).ToList();
                string path = Server.MapPath("~/PhieuLienLac_Template.xls");
                string p2 = HttpContext.Server.MapPath("~/PhieuLienLac_Template.xls");
                ViewBag.path1 = path;
                ViewBag.path2 = p2;



                return View();
            }
            else
            {

                return RedirectToAction("Index", "Thongbao");
            }
        }
        
        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult LoadLophoc(string id, string namhoc, int idTruong)
        {
            string userName = GetUserName();

           var listlophoc = (from lop in db.LopHocs
                             where lop.idTruong == idTruong && lop.NamHoc == namhoc && lop.MaKhoi == id
                             let allowlops =
                             ( 
                             from userLop in db.Users_LopHoc.Where(userLop => userLop.idTruong == idTruong && userLop.NamHoc == namhoc && userLop.UserName == userName)
                             
                             select userLop.MaLop)
                             where allowlops.Contains(lop.MaLop) && lop.MaKhoi == id && lop.NamHoc == namhoc
                             select new SelectListItem()
                             {
                                 Value = lop.MaLop,
                                 Text = lop.TenLop,

                             }).ToList();


            return Json(listlophoc, JsonRequestBehavior.AllowGet);

        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult LoadMonHocTheoLop(string malop, string namhoc, int idTruong)
        {
            string userName = User.Identity.Name.ToString() == "" ? "superadmin" : User.Identity.Name.ToString();

            var listMonhoc = (from userlop in db.Users_Monhoc.Where(userlop => userlop.MaLop == malop && userlop.NamHoc == namhoc && userlop.idTruong == idTruong)
                              join dmmonhoc in db.DM_MonHoc on userlop.MaMonHoc equals dmmonhoc.MaMonHoc
                              select new SelectListItem()
                              {
                                  Value = userlop.MaMonHoc,
                                  Text = dmmonhoc.TenMonHoc,

                              }).ToList();


            return Json(listMonhoc, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// He thong tam thoi dang lay namhoc la nam hien tai cua he thong
        /// </summary>
        /// <param name="hocky"></param>
        /// <param name="malop"></param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Get)]
        [Authorize]
        public ActionResult ketQuaHocTap(string malop, string hocky, string namhoc, int idTruong, string tentruong, string tengiaovien, bool exportExcel = false)
        {
            if (!Request.IsAuthenticated)
            {

                return RedirectToAction("Index", "Thongbao");
            }
            else
            {


                ViewBag.hocky = GetTenHocKy(hocky);
                ViewBag.namhoc = namhoc;
                ViewBag.tentruong = tentruong;
                ViewBag.tengiaovien = tengiaovien;

                var lop = db.LopHocs.Where(l => l.MaLop == malop && l.idTruong == idTruong && l.NamHoc == namhoc).SingleOrDefault();
                ViewBag.tenlop = lop.TenLop;

                if (exportExcel)
                {
                    Response.AddHeader("Content-Type", "application/vnd.ms-excel");
                    ViewBag.exportExcel = true;
                }

                DateTime? ngayBD;
                DateTime? ngayKT;
                GetKhoangTG(hocky, namhoc, idTruong, out ngayBD, out ngayKT);

                // lay danh sach hoc sinh cua lop dua len
                var listketquahoctap = (from hs in db.HocSinhs.Where(hs => hs.idTruong == idTruong && hs.MaLop == malop && hs.NamHoc == namhoc)
                                        join kq in db.HocSinh_TongHopNX.Where(kq => kq.idTruong == idTruong && kq.NamHoc == namhoc && kq.MaLop == malop)
                                        on hs.MaHocSinh equals kq.MaHocSinh
                                        into join1

                                        from j1 in join1.DefaultIfEmpty()
                                        join hk in db.HocSinh_HanhKiemTongKet.Where(hk => hk.idTruong == idTruong && hk.NamHoc == namhoc && hk.MaLop == malop)
                                        on hs.MaHocSinh equals hk.MaHocSinh
                                        into join2

                                        from j2 in join2.DefaultIfEmpty()

                                        select new ThongKeKetQuaHocTap
                                        {
                                            mahocsinh = hs.MaHocSinh,
                                        }

                            ).ToList();


                return View(listketquahoctap);


            }

        }


        [AcceptVerbs(HttpVerbs.Get)]
        [Authorize]
        public ActionResult LoadDanhSachHocSinh(int idtruong, string malop,string namhoc)
        {
            var listHocSinh = (from hocsinh in db.HocSinhs.Where(hocsinh => hocsinh.idTruong == idtruong && hocsinh.MaLop == malop && hocsinh.NamHoc == namhoc && hocsinh.Bohoc == false)
                           where !(from hsqt in db.HocSinh_QuaTrinh.Where(hsqt => hsqt.idTruong == idtruong && hsqt.NamHoc == namhoc && hsqt.MaLop == malop && hsqt.MaQuaTrinh == "BH")
                                   select hsqt.MaHocSinh).Contains(hocsinh.MaHocSinh)
                           orderby hocsinh.STT ascending
                           select new ViewHocSinh()
                           {
                               Stt = hocsinh.STT,
                               MaHocSinh = hocsinh.MaHocSinh,
                               HoTen = hocsinh.HoTen,
                               idTruong = hocsinh.idTruong,
                               MaLop = hocsinh.MaLop,
                               NamHoc = hocsinh.NamHoc,
                               GioiTinh = hocsinh.GioiTinh == true ? "Nam" : "Nữ",
                               NgaySinh = hocsinh.NgaySinh,
                               DiaChi = hocsinh.Diachilienlac

                           }).ToList();

            return PartialView("ViewHocSinh", listHocSinh);
        }

        public List<SoLienLac> filterHocSinh(string hocsinhs, List<SoLienLac> soLienLac)
        {
            List<SoLienLac> hocSinhFiltered = new List<SoLienLac>();
            if (soLienLac != null && hocsinhs != null && hocsinhs.Trim().Length > 0 && soLienLac.Count > 0)
            {
                foreach (SoLienLac hocSinhSoLienLac in soLienLac)
                {
                    if (hocsinhs.IndexOf(hocSinhSoLienLac.maHS) != -1)
                    {
                        hocSinhFiltered.Add(hocSinhSoLienLac);

                    }
                }

            }
            return hocSinhFiltered;
        }


        [Authorize]
        public ActionResult SoLienLacXuatExcellGHK1(int idtruong, string makhoi, string malop, string mahk, string namhoc, string tentruong, string mahocsinh)
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Thongbao");
            }
            else
            {
                var gvcn = (from usname in db.ScrUsers
                            join usr in db.Users_LopHoc
                            on usname.UserName equals usr.UserName
                            where usr.idTruong == idtruong && usr.NamHoc == namhoc && usr.MaLop == malop
                            select usname).FirstOrDefault().FullName;
                var hocSinh = (from st in db.HocSinhs
                               where st.idTruong == idtruong && st.NamHoc == namhoc && st.MaLop == malop && mahocsinh.Contains(st.MaHocSinh)
                               select new SoLienLac
                               {
                                   stt = st.STT,
                                   maHS = st.MaHocSinh,
                                   hoTenHS = st.HoTen,
                                   hoTenCha = st.HoTenCha,
                                   ngheNghiepCha = st.NgheCha,
                                   hoTenMe = st.HoTenMe,
                                   ngheNghiepMe = st.NgheMe,
                                   diaChi = st.Diachilienlac,
                                   giaoVien = gvcn,
                                   dienThoai = st.DienThoai,

                                   Toan = (from diemToan in db.HocSinh_Diem_NhanXet
                                           where diemToan.idTruong == idtruong && diemToan.NamHoc == namhoc
                                               && diemToan.MaLop == malop && diemToan.MaMonHoc == "TOAN" && diemToan.MaHocSinh == st.MaHocSinh
                                           select diemToan.DiemGHK1).FirstOrDefault(),
                                   KQMonHocToan = (from diemToan in db.HocSinh_Diem_NhanXet
                                                   where diemToan.idTruong == idtruong && diemToan.NamHoc == namhoc
                                                       && diemToan.MaLop == malop && diemToan.MaMonHoc == "TOAN" && diemToan.MaHocSinh == st.MaHocSinh
                                                   select diemToan.KQMonHocGHK1).FirstOrDefault().Replace("HTT", "T").Replace("CHT", "C").Replace("HT", "H"),

                                   TV = (from diemTV in db.HocSinh_Diem_NhanXet
                                         where diemTV.idTruong == idtruong && diemTV.NamHoc == namhoc
                                               && diemTV.MaLop == malop && diemTV.MaMonHoc == "TV" && diemTV.MaHocSinh == st.MaHocSinh
                                         select  diemTV.DiemGHK1).FirstOrDefault(),

                                   KQMonHocTV = (from diemTV in db.HocSinh_Diem_NhanXet
                                                 where diemTV.idTruong == idtruong && diemTV.NamHoc == namhoc
                                                       && diemTV.MaLop == malop && diemTV.MaMonHoc == "TV" && diemTV.MaHocSinh == st.MaHocSinh
                                                 select diemTV.KQMonHocGHK1).FirstOrDefault().Replace("HTT", "T").Replace("CHT", "C").Replace("HT", "H"),

                                   TNXH = (from diemTNXH in db.HocSinh_Diem_NhanXet
                                           where diemTNXH.idTruong == idtruong && diemTNXH.NamHoc == namhoc
                                               && diemTNXH.MaLop == malop && diemTNXH.MaMonHoc == "TNXH" && diemTNXH.MaHocSinh == st.MaHocSinh
                                           select  diemTNXH.DiemGHK1).FirstOrDefault(),

                                   KQMonHocTNXH = (from diemTNXH in db.HocSinh_Diem_NhanXet
                                                   where diemTNXH.idTruong == idtruong && diemTNXH.NamHoc == namhoc
                                                       && diemTNXH.MaLop == malop && diemTNXH.MaMonHoc == "TNXH" && diemTNXH.MaHocSinh == st.MaHocSinh
                                                   select diemTNXH.KQMonHocGHK1).FirstOrDefault().Replace("HTT", "T").Replace("CHT", "C").Replace("HT", "H"),

                                   KH = (from diemKH in db.HocSinh_Diem_NhanXet
                                         where diemKH.idTruong == idtruong && diemKH.NamHoc == namhoc
                                             && diemKH.MaLop == malop && diemKH.MaMonHoc == "KH" && diemKH.MaHocSinh == st.MaHocSinh
                                         select  diemKH.DiemGHK1).FirstOrDefault(),

                                   KQMonHocKH = (from diemKH in db.HocSinh_Diem_NhanXet
                                                 where diemKH.idTruong == idtruong && diemKH.NamHoc == namhoc
                                                       && diemKH.MaLop == malop && diemKH.MaMonHoc == "KH" && diemKH.MaHocSinh == st.MaHocSinh
                                                 select diemKH.KQMonHocGHK1).FirstOrDefault().Replace("HTT", "T").Replace("CHT", "C").Replace("HT", "H"),

                                   LS = (from diemLS in db.HocSinh_Diem_NhanXet
                                         where diemLS.idTruong == idtruong && diemLS.NamHoc == namhoc
                                             && diemLS.MaLop == malop && diemLS.MaMonHoc == "LS" && diemLS.MaHocSinh == st.MaHocSinh
                                         select  diemLS.DiemGHK1).FirstOrDefault(),

                                   KQMonHocLS = (from diemLS in db.HocSinh_Diem_NhanXet
                                                 where diemLS.idTruong == idtruong && diemLS.NamHoc == namhoc
                                                       && diemLS.MaLop == malop && diemLS.MaMonHoc == "LS" && diemLS.MaHocSinh == st.MaHocSinh
                                                 select diemLS.KQMonHocGHK1).FirstOrDefault().Replace("HTT", "T").Replace("CHT", "C").Replace("HT", "H"),

                                   DD = (from diemDD in db.HocSinh_Diem_NhanXet
                                         where diemDD.idTruong == idtruong && diemDD.NamHoc == namhoc
                                             && diemDD.MaLop == malop && diemDD.MaMonHoc == "DD" && diemDD.MaHocSinh == st.MaHocSinh
                                         select  diemDD.DiemGHK1).FirstOrDefault(),

                                   KQMonHocDD = (from diemDD in db.HocSinh_Diem_NhanXet
                                                 where diemDD.idTruong == idtruong && diemDD.NamHoc == namhoc
                                                       && diemDD.MaLop == malop && diemDD.MaMonHoc == "DD" && diemDD.MaHocSinh == st.MaHocSinh
                                                 select diemDD.KQMonHocGHK1).FirstOrDefault().Replace("HTT", "T").Replace("CHT", "C").Replace("HT", "H"),


                                   TC = (from diemTC in db.HocSinh_Diem_NhanXet
                                         where diemTC.idTruong == idtruong && diemTC.NamHoc == namhoc
                                             && diemTC.MaLop == malop && diemTC.MaMonHoc == "TC" && diemTC.MaHocSinh == st.MaHocSinh
                                         select  diemTC.DiemGHK1).FirstOrDefault(),

                                   KQMonHocTC = (from diemTC in db.HocSinh_Diem_NhanXet
                                                 where diemTC.idTruong == idtruong && diemTC.NamHoc == namhoc
                                                       && diemTC.MaLop == malop && diemTC.MaMonHoc == "TC" && diemTC.MaHocSinh == st.MaHocSinh
                                                 select diemTC.KQMonHocGHK1).FirstOrDefault().Replace("HTT", "T").Replace("CHT", "C").Replace("HT", "H"),

                                   KT = (from diemKT in db.HocSinh_Diem_NhanXet
                                         where diemKT.idTruong == idtruong && diemKT.NamHoc == namhoc
                                             && diemKT.MaLop == malop && diemKT.MaMonHoc == "KT" && diemKT.MaHocSinh == st.MaHocSinh
                                         select  diemKT.DiemGHK1).FirstOrDefault(),

                                   KQMonHocKT = (from diemKT in db.HocSinh_Diem_NhanXet
                                                 where diemKT.idTruong == idtruong && diemKT.NamHoc == namhoc
                                                       && diemKT.MaLop == malop && diemKT.MaMonHoc == "KT" && diemKT.MaHocSinh == st.MaHocSinh
                                                 select diemKT.KQMonHocGHK1).FirstOrDefault().Replace("HTT", "T").Replace("CHT", "C").Replace("HT", "H"),

                                   MT = (from diemMT in db.HocSinh_Diem_NhanXet
                                         where diemMT.idTruong == idtruong && diemMT.NamHoc == namhoc
                                             && diemMT.MaLop == malop && diemMT.MaMonHoc == "MT" && diemMT.MaHocSinh == st.MaHocSinh
                                         select  diemMT.DiemGHK1).FirstOrDefault(),

                                   KQMonHocMT = (from diemMT in db.HocSinh_Diem_NhanXet
                                                 where diemMT.idTruong == idtruong && diemMT.NamHoc == namhoc
                                                       && diemMT.MaLop == malop && diemMT.MaMonHoc == "MT" && diemMT.MaHocSinh == st.MaHocSinh
                                                 select diemMT.KQMonHocGHK1).FirstOrDefault().Replace("HTT", "T").Replace("CHT", "C").Replace("HT", "H"),

                                   AN = (from diemAN in db.HocSinh_Diem_NhanXet
                                         where diemAN.idTruong == idtruong && diemAN.NamHoc == namhoc
                                             && diemAN.MaLop == malop && diemAN.MaMonHoc == "AN" && diemAN.MaHocSinh == st.MaHocSinh
                                         select  diemAN.DiemGHK1).FirstOrDefault(),

                                   KQMonHocAN = (from diemAN in db.HocSinh_Diem_NhanXet
                                                 where diemAN.idTruong == idtruong && diemAN.NamHoc == namhoc
                                                       && diemAN.MaLop == malop && diemAN.MaMonHoc == "AN" && diemAN.MaHocSinh == st.MaHocSinh
                                                 select diemAN.KQMonHocGHK1).FirstOrDefault().Replace("HTT", "T").Replace("CHT", "C").Replace("HT", "H"),

                                   TD = (from diemTD in db.HocSinh_Diem_NhanXet
                                         where diemTD.idTruong == idtruong && diemTD.NamHoc == namhoc
                                             && diemTD.MaLop == malop && diemTD.MaMonHoc == "TD" && diemTD.MaHocSinh == st.MaHocSinh
                                         select  diemTD.DiemGHK1).FirstOrDefault(),

                                   KQMonHocTD = (from diemTD in db.HocSinh_Diem_NhanXet
                                                 where diemTD.idTruong == idtruong && diemTD.NamHoc == namhoc
                                                       && diemTD.MaLop == malop && diemTD.MaMonHoc == "TD" && diemTD.MaHocSinh == st.MaHocSinh
                                                 select diemTD.KQMonHocGHK1).FirstOrDefault().Replace("HTT", "T").Replace("CHT", "C").Replace("HT", "H"),

                                   NN = (from diemNN in db.HocSinh_Diem_NhanXet
                                         where diemNN.idTruong == idtruong && diemNN.NamHoc == namhoc
                                             && diemNN.MaLop == malop && diemNN.MaMonHoc == "NN" && diemNN.MaHocSinh == st.MaHocSinh
                                         select  diemNN.DiemGHK1).FirstOrDefault(),

                                   KQMonHocNN = (from diemNN in db.HocSinh_Diem_NhanXet
                                                 where diemNN.idTruong == idtruong && diemNN.NamHoc == namhoc
                                                       && diemNN.MaLop == malop && diemNN.MaMonHoc == "NN" && diemNN.MaHocSinh == st.MaHocSinh
                                                 select diemNN.KQMonHocGHK1).FirstOrDefault().Replace("HTT", "T").Replace("CHT", "C").Replace("HT", "H"),


                                   TinHoc = (from diemTH in db.HocSinh_Diem_NhanXet
                                             where diemTH.idTruong == idtruong && diemTH.NamHoc == namhoc
                                             && diemTH.MaLop == malop && diemTH.MaMonHoc == "TH" && diemTH.MaHocSinh == st.MaHocSinh
                                             select  diemTH.DiemGHK1).FirstOrDefault(),

                                   KQMonHocTinHoc = (from diemNN in db.HocSinh_Diem_NhanXet
                                                     where diemNN.idTruong == idtruong && diemNN.NamHoc == namhoc
                                                           && diemNN.MaLop == malop && diemNN.MaMonHoc == "TH" && diemNN.MaHocSinh == st.MaHocSinh
                                                     select diemNN.KQMonHocGHK1).FirstOrDefault().Replace("HTT", "T").Replace("CHT", "C").Replace("HT", "H"),

                                   TiengDanToc = (from diemTDT in db.HocSinh_Diem_NhanXet
                                                  where diemTDT.idTruong == idtruong && diemTDT.NamHoc == namhoc
                                             && diemTDT.MaLop == malop && diemTDT.MaMonHoc == "TDT" && diemTDT.MaHocSinh == st.MaHocSinh
                                                  select  diemTDT.DiemGHK1).FirstOrDefault(),

                                   KQMonHocTiengDanToc = (from diemTDT in db.HocSinh_Diem_NhanXet
                                                          where diemTDT.idTruong == idtruong && diemTDT.NamHoc == namhoc
                                                       && diemTDT.MaLop == malop && diemTDT.MaMonHoc == "TDT" && diemTDT.MaHocSinh == st.MaHocSinh
                                                          select diemTDT.KQMonHocGHK1).FirstOrDefault().Replace("HTT", "T").Replace("CHT", "C").Replace("HT", "H"),

                                   NXNangLucTX1 =  (from nxnl in db.HocSinh_TongHopNX
                                                                     where nxnl.idTruong == idtruong && nxnl.NamHoc == namhoc
                                                                  && nxnl.MaLop == malop && nxnl.MaHocSinh == st.MaHocSinh && nxnl.HocKy == 0
                                                                     select nxnl.NXNangLucTX1).FirstOrDefault(),

                                   NXNangLucTX2 =  (from nxnl in db.HocSinh_TongHopNX
                                                                     where nxnl.idTruong == idtruong && nxnl.NamHoc == namhoc
                                                                  && nxnl.MaLop == malop && nxnl.MaHocSinh == st.MaHocSinh && nxnl.HocKy == 0
                                                                     select nxnl.NXNangLucTX2).FirstOrDefault() ,

                                   NXNangLucTX3 =  (from nxnl in db.HocSinh_TongHopNX
                                                                     where nxnl.idTruong == idtruong && nxnl.NamHoc == namhoc
                                                                  && nxnl.MaLop == malop && nxnl.MaHocSinh == st.MaHocSinh && nxnl.HocKy == 0
                                                                     select nxnl.NXNangLucTX3).FirstOrDefault(),


                                   NXPhamChatTX1 =  (from nxpc in db.HocSinh_TongHopNX
                                                                      where nxpc.idTruong == idtruong && nxpc.NamHoc == namhoc
                                                                  && nxpc.MaLop == malop && nxpc.MaHocSinh == st.MaHocSinh && nxpc.HocKy == 0
                                                                      select nxpc.NXPhamChatTX1).FirstOrDefault() ,

                                   NXPhamChatTX2 =  (from nxpc in db.HocSinh_TongHopNX
                                                                      where nxpc.idTruong == idtruong && nxpc.NamHoc == namhoc
                                                                  && nxpc.MaLop == malop && nxpc.MaHocSinh == st.MaHocSinh && nxpc.HocKy == 0
                                                                      select nxpc.NXPhamChatTX2).FirstOrDefault() ,

                                   NXPhamChatTX3 =  (from nxpc in db.HocSinh_TongHopNX
                                                                      where nxpc.idTruong == idtruong && nxpc.NamHoc == namhoc
                                                                  && nxpc.MaLop == malop && nxpc.MaHocSinh == st.MaHocSinh && nxpc.HocKy == 0
                                                                      select nxpc.NXPhamChatTX3).FirstOrDefault() ,

                                   NXPhamChatTX4 =  (from nxpc in db.HocSinh_TongHopNX
                                                                      where nxpc.idTruong == idtruong && nxpc.NamHoc == namhoc
                                                                  && nxpc.MaLop == malop && nxpc.MaHocSinh == st.MaHocSinh && nxpc.HocKy == 0
                                                                      select nxpc.NXPhamChatTX4).FirstOrDefault() ,

                                   KhenThuong = (from hskt in db.HocSinh_KhenThuongKyLuat where hskt.idTruong == idtruong && hskt.MaHocKy == mahk && hskt.MaLop == malop && hskt.MaHocSinh == st.MaHocSinh select hskt),

                                   hoanThanhCT =  "",


                                   nhanXetGVCN =  (from nx in db.HocSinh_DanhGiaThang_GVCN
                                                                    where nx.idTruong == idtruong && nx.NamHoc == namhoc && nx.MaLop == malop && nx.MaHocSinh == st.MaHocSinh
                                                                    select new NhanXetGVCNMonHoc
                                                                    {
                                                                        NXKT = nx.NXT3_KT,
                                                                        NXNL = nx.NXT3_NL,
                                                                        NXPC = nx.NXT3_PC
                                                                    }).FirstOrDefault() 


                               }).ToList();



                List<SoLienLac> hocSinhs = filterHocSinh(mahocsinh, hocSinh);
                string tenLop = Cmon.GetnameClass(malop, idtruong, namhoc);
                string userName = User.Identity.Name.ToString() == "" ? "superadmin" : User.Identity.Name.ToString();
                if (!string.IsNullOrEmpty(mahocsinh) && hocSinhs != null && hocSinhs.Count > 0)
                {
                    string tenDonViChuQuan = Cmon.getDonViChuQuan(idtruong);
                    ExcelFileWriterSoLienLac<object> myExcel = new ExcelWriteSoLienLac();
                    string tenFile = "PhieuLienLac_Template.xls" ;

                    // định nghĩa thư mục chứa template
                    string rootPathToTemplate = Server.MapPath("~/templateExcell/" + tenFile);

                    string fileName = userName + "_PhieuLienLac";

                    string rootProcessTemplate = Server.MapPath(System.IO.Path.GetDirectoryName("~/ProcessTemplate/")); // thư mục chứa các template sẽ được đem xử lý đổ dữ liệu

                    string fileNameExcute = rootProcessTemplate + "\\" + fileName + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xls"; // tạo một template để chuẩn bị cho xử lý
                    System.IO.File.Delete(fileNameExcute);// xóa file tạm trước đó và tạo lại
                    System.IO.File.Copy(rootPathToTemplate, fileNameExcute, true);// copy từ thư mục template tới thư mục chuẩn bị xử lý

                    // Xử lý đổ dữ liệu vào các ô tương ứng
                    myExcel.ExportSoLienLacToExcel(fileNameExcute, mahk, namhoc, tenDonViChuQuan, tentruong, tenLop, makhoi, hocSinhs, gvcn, malop);

                    System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
                    Cmon.stopExcel();

                    // lấy ngày giờ hiện tại của hệ thống 
                    DateTime ngayHienTai = DateTime.Now;
                    string ngayThang = ngayHienTai.Date.ToString("dd/MM/yyyy") + "_" + ngayHienTai.Hour.ToString() + "_" + ngayHienTai.Minute.ToString() + "_" + ngayHienTai.Second.ToString();

                    return File(fileNameExcute, "application/ms-excel", "PhieuLienLac_" + userName + "_" + ngayThang + ".xls");


                }


            }
            return File("PhieuLienLac.xls", "application/ms-excel", "PhieuLienLac.xls");
        }


        [Authorize]
        public ActionResult SoLienLacXuatExcellHK1(int idtruong, string makhoi, string malop, string mahk, string namhoc, string tentruong, string mahocsinh)
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Thongbao");
            }
            else
            {
                var gvcn = (from usname in db.ScrUsers
                            join usr in db.Users_LopHoc
                            on usname.UserName equals usr.UserName
                            where usr.idTruong == idtruong && usr.NamHoc == namhoc && usr.MaLop == malop
                            select usname).FirstOrDefault().FullName;
                var hocSinh = (from st in db.HocSinhs
                               where st.idTruong == idtruong && st.NamHoc == namhoc && st.MaLop == malop && mahocsinh.Contains(st.MaHocSinh)
                               select new SoLienLac
                               {
                                   stt = st.STT,
                                   maHS = st.MaHocSinh,
                                   hoTenHS = st.HoTen,
                                   hoTenCha = st.HoTenCha,
                                   ngheNghiepCha = st.NgheCha,
                                   hoTenMe = st.HoTenMe,
                                   ngheNghiepMe = st.NgheMe,
                                   diaChi = st.Diachilienlac,
                                   giaoVien = gvcn,
                                   dienThoai = st.DienThoai,

                                   Toan = (from diemToan in db.HocSinh_Diem_NhanXet
                                           where diemToan.idTruong == idtruong && diemToan.NamHoc == namhoc
                                               && diemToan.MaLop == malop && diemToan.MaMonHoc == "TOAN" && diemToan.MaHocSinh == st.MaHocSinh
                                           select diemToan.DiemHK1).FirstOrDefault(),
                                   KQMonHocToan = (from diemToan in db.HocSinh_Diem_NhanXet
                                                   where diemToan.idTruong == idtruong && diemToan.NamHoc == namhoc
                                                       && diemToan.MaLop == malop && diemToan.MaMonHoc == "TOAN" && diemToan.MaHocSinh == st.MaHocSinh
                                                   select diemToan.KQMonHocHK1).FirstOrDefault(),

                                   TV = (from diemTV in db.HocSinh_Diem_NhanXet
                                         where diemTV.idTruong == idtruong && diemTV.NamHoc == namhoc
                                               && diemTV.MaLop == malop && diemTV.MaMonHoc == "TV" && diemTV.MaHocSinh == st.MaHocSinh
                                         select diemTV.DiemHK1).FirstOrDefault(),

                                   KQMonHocTV = (from diemTV in db.HocSinh_Diem_NhanXet
                                                 where diemTV.idTruong == idtruong && diemTV.NamHoc == namhoc
                                                       && diemTV.MaLop == malop && diemTV.MaMonHoc == "TV" && diemTV.MaHocSinh == st.MaHocSinh
                                                 select diemTV.KQMonHocHK1).FirstOrDefault(),

                                   TNXH = (from diemTNXH in db.HocSinh_Diem_NhanXet
                                           where diemTNXH.idTruong == idtruong && diemTNXH.NamHoc == namhoc
                                               && diemTNXH.MaLop == malop && diemTNXH.MaMonHoc == "TNXH" && diemTNXH.MaHocSinh == st.MaHocSinh
                                           select diemTNXH.DiemHK1).FirstOrDefault(),

                                   KQMonHocTNXH = (from diemTNXH in db.HocSinh_Diem_NhanXet
                                                   where diemTNXH.idTruong == idtruong && diemTNXH.NamHoc == namhoc
                                                       && diemTNXH.MaLop == malop && diemTNXH.MaMonHoc == "TNXH" && diemTNXH.MaHocSinh == st.MaHocSinh
                                                   select diemTNXH.KQMonHocHK1).FirstOrDefault(),

                                   KH = (from diemKH in db.HocSinh_Diem_NhanXet
                                         where diemKH.idTruong == idtruong && diemKH.NamHoc == namhoc
                                             && diemKH.MaLop == malop && diemKH.MaMonHoc == "KH" && diemKH.MaHocSinh == st.MaHocSinh
                                         select diemKH.DiemHK1).FirstOrDefault(),

                                   KQMonHocKH = (from diemKH in db.HocSinh_Diem_NhanXet
                                                 where diemKH.idTruong == idtruong && diemKH.NamHoc == namhoc
                                                       && diemKH.MaLop == malop && diemKH.MaMonHoc == "KH" && diemKH.MaHocSinh == st.MaHocSinh
                                                 select diemKH.KQMonHocHK1).FirstOrDefault(),

                                   LS = (from diemLS in db.HocSinh_Diem_NhanXet
                                         where diemLS.idTruong == idtruong && diemLS.NamHoc == namhoc
                                             && diemLS.MaLop == malop && diemLS.MaMonHoc == "LS" && diemLS.MaHocSinh == st.MaHocSinh
                                         select diemLS.DiemHK1).FirstOrDefault(),

                                   KQMonHocLS = (from diemLS in db.HocSinh_Diem_NhanXet
                                                 where diemLS.idTruong == idtruong && diemLS.NamHoc == namhoc
                                                       && diemLS.MaLop == malop && diemLS.MaMonHoc == "LS" && diemLS.MaHocSinh == st.MaHocSinh
                                                 select diemLS.KQMonHocHK1).FirstOrDefault(),

                                   DD = (from diemDD in db.HocSinh_Diem_NhanXet
                                         where diemDD.idTruong == idtruong && diemDD.NamHoc == namhoc
                                             && diemDD.MaLop == malop && diemDD.MaMonHoc == "DD" && diemDD.MaHocSinh == st.MaHocSinh
                                         select diemDD.DiemHK1).FirstOrDefault(),

                                   KQMonHocDD = (from diemDD in db.HocSinh_Diem_NhanXet
                                                 where diemDD.idTruong == idtruong && diemDD.NamHoc == namhoc
                                                       && diemDD.MaLop == malop && diemDD.MaMonHoc == "DD" && diemDD.MaHocSinh == st.MaHocSinh
                                                 select diemDD.KQMonHocHK1).FirstOrDefault(),


                                   TC = (from diemTC in db.HocSinh_Diem_NhanXet
                                         where diemTC.idTruong == idtruong && diemTC.NamHoc == namhoc
                                             && diemTC.MaLop == malop && diemTC.MaMonHoc == "TC" && diemTC.MaHocSinh == st.MaHocSinh
                                         select diemTC.DiemHK1).FirstOrDefault(),

                                   KQMonHocTC = (from diemTC in db.HocSinh_Diem_NhanXet
                                                 where diemTC.idTruong == idtruong && diemTC.NamHoc == namhoc
                                                       && diemTC.MaLop == malop && diemTC.MaMonHoc == "TC" && diemTC.MaHocSinh == st.MaHocSinh
                                                 select diemTC.KQMonHocHK1).FirstOrDefault(),

                                   KT = (from diemKT in db.HocSinh_Diem_NhanXet
                                         where diemKT.idTruong == idtruong && diemKT.NamHoc == namhoc
                                             && diemKT.MaLop == malop && diemKT.MaMonHoc == "KT" && diemKT.MaHocSinh == st.MaHocSinh
                                         select diemKT.DiemHK1).FirstOrDefault(),

                                   KQMonHocKT = (from diemKT in db.HocSinh_Diem_NhanXet
                                                 where diemKT.idTruong == idtruong && diemKT.NamHoc == namhoc
                                                       && diemKT.MaLop == malop && diemKT.MaMonHoc == "KT" && diemKT.MaHocSinh == st.MaHocSinh
                                                 select diemKT.KQMonHocHK1).FirstOrDefault(),

                                   MT = (from diemMT in db.HocSinh_Diem_NhanXet
                                         where diemMT.idTruong == idtruong && diemMT.NamHoc == namhoc
                                             && diemMT.MaLop == malop && diemMT.MaMonHoc == "MT" && diemMT.MaHocSinh == st.MaHocSinh
                                         select diemMT.DiemHK1).FirstOrDefault(),

                                   KQMonHocMT = (from diemMT in db.HocSinh_Diem_NhanXet
                                                 where diemMT.idTruong == idtruong && diemMT.NamHoc == namhoc
                                                       && diemMT.MaLop == malop && diemMT.MaMonHoc == "MT" && diemMT.MaHocSinh == st.MaHocSinh
                                                 select diemMT.KQMonHocHK1).FirstOrDefault(),

                                   AN = (from diemAN in db.HocSinh_Diem_NhanXet
                                         where diemAN.idTruong == idtruong && diemAN.NamHoc == namhoc
                                             && diemAN.MaLop == malop && diemAN.MaMonHoc == "AN" && diemAN.MaHocSinh == st.MaHocSinh
                                         select diemAN.DiemHK1).FirstOrDefault(),

                                   KQMonHocAN = (from diemAN in db.HocSinh_Diem_NhanXet
                                                 where diemAN.idTruong == idtruong && diemAN.NamHoc == namhoc
                                                       && diemAN.MaLop == malop && diemAN.MaMonHoc == "AN" && diemAN.MaHocSinh == st.MaHocSinh
                                                 select diemAN.KQMonHocHK1).FirstOrDefault(),

                                   TD = (from diemTD in db.HocSinh_Diem_NhanXet
                                         where diemTD.idTruong == idtruong && diemTD.NamHoc == namhoc
                                             && diemTD.MaLop == malop && diemTD.MaMonHoc == "TD" && diemTD.MaHocSinh == st.MaHocSinh
                                         select diemTD.DiemHK1).FirstOrDefault(),

                                   KQMonHocTD = (from diemTD in db.HocSinh_Diem_NhanXet
                                                 where diemTD.idTruong == idtruong && diemTD.NamHoc == namhoc
                                                       && diemTD.MaLop == malop && diemTD.MaMonHoc == "TD" && diemTD.MaHocSinh == st.MaHocSinh
                                                 select diemTD.KQMonHocHK1).FirstOrDefault(),

                                   NN = (from diemNN in db.HocSinh_Diem_NhanXet
                                         where diemNN.idTruong == idtruong && diemNN.NamHoc == namhoc
                                             && diemNN.MaLop == malop && diemNN.MaMonHoc == "NN" && diemNN.MaHocSinh == st.MaHocSinh
                                         select diemNN.DiemHK1).FirstOrDefault(),

                                   KQMonHocNN = (from diemNN in db.HocSinh_Diem_NhanXet
                                                 where diemNN.idTruong == idtruong && diemNN.NamHoc == namhoc
                                                       && diemNN.MaLop == malop && diemNN.MaMonHoc == "NN" && diemNN.MaHocSinh == st.MaHocSinh
                                                 select diemNN.KQMonHocHK1).FirstOrDefault(),


                                   TinHoc = (from diemTH in db.HocSinh_Diem_NhanXet
                                             where diemTH.idTruong == idtruong && diemTH.NamHoc == namhoc
                                             && diemTH.MaLop == malop && diemTH.MaMonHoc == "TH" && diemTH.MaHocSinh == st.MaHocSinh
                                             select diemTH.DiemHK1).FirstOrDefault(),

                                   KQMonHocTinHoc = (from diemNN in db.HocSinh_Diem_NhanXet
                                                     where diemNN.idTruong == idtruong && diemNN.NamHoc == namhoc
                                                           && diemNN.MaLop == malop && diemNN.MaMonHoc == "TH" && diemNN.MaHocSinh == st.MaHocSinh
                                                     select diemNN.KQMonHocHK1).FirstOrDefault(),

                                   TiengDanToc = (from diemTDT in db.HocSinh_Diem_NhanXet
                                                  where diemTDT.idTruong == idtruong && diemTDT.NamHoc == namhoc
                                             && diemTDT.MaLop == malop && diemTDT.MaMonHoc == "TDT" && diemTDT.MaHocSinh == st.MaHocSinh
                                                  select diemTDT.DiemHK1).FirstOrDefault(),

                                   KQMonHocTiengDanToc = (from diemTDT in db.HocSinh_Diem_NhanXet
                                                          where diemTDT.idTruong == idtruong && diemTDT.NamHoc == namhoc
                                                       && diemTDT.MaLop == malop && diemTDT.MaMonHoc == "TDT" && diemTDT.MaHocSinh == st.MaHocSinh
                                                          select diemTDT.KQMonHocHK1).FirstOrDefault(),

                                   NXNangLucTX1 = (from nxnl in db.HocSinh_TongHopNX
                                                   where nxnl.idTruong == idtruong && nxnl.NamHoc == namhoc
                                                && nxnl.MaLop == malop && nxnl.MaHocSinh == st.MaHocSinh && nxnl.HocKy == 1
                                                   select nxnl.NXNangLucTX1).FirstOrDefault(),

                                   NXNangLucTX2 = (from nxnl in db.HocSinh_TongHopNX
                                                   where nxnl.idTruong == idtruong && nxnl.NamHoc == namhoc
                                                && nxnl.MaLop == malop && nxnl.MaHocSinh == st.MaHocSinh && nxnl.HocKy == 1
                                                   select nxnl.NXNangLucTX2).FirstOrDefault(),

                                   NXNangLucTX3 = (from nxnl in db.HocSinh_TongHopNX
                                                   where nxnl.idTruong == idtruong && nxnl.NamHoc == namhoc
                                                && nxnl.MaLop == malop && nxnl.MaHocSinh == st.MaHocSinh && nxnl.HocKy == 1
                                                   select nxnl.NXNangLucTX3).FirstOrDefault(),


                                   NXPhamChatTX1 = (from nxpc in db.HocSinh_TongHopNX
                                                    where nxpc.idTruong == idtruong && nxpc.NamHoc == namhoc
                                                && nxpc.MaLop == malop && nxpc.MaHocSinh == st.MaHocSinh && nxpc.HocKy == 1
                                                    select nxpc.NXPhamChatTX1).FirstOrDefault(),

                                   NXPhamChatTX2 = (from nxpc in db.HocSinh_TongHopNX
                                                    where nxpc.idTruong == idtruong && nxpc.NamHoc == namhoc
                                                && nxpc.MaLop == malop && nxpc.MaHocSinh == st.MaHocSinh && nxpc.HocKy == 1
                                                    select nxpc.NXPhamChatTX2).FirstOrDefault(),

                                   NXPhamChatTX3 = (from nxpc in db.HocSinh_TongHopNX
                                                    where nxpc.idTruong == idtruong && nxpc.NamHoc == namhoc
                                                && nxpc.MaLop == malop && nxpc.MaHocSinh == st.MaHocSinh && nxpc.HocKy == 1
                                                    select nxpc.NXPhamChatTX3).FirstOrDefault(),

                                   NXPhamChatTX4 = (from nxpc in db.HocSinh_TongHopNX
                                                    where nxpc.idTruong == idtruong && nxpc.NamHoc == namhoc
                                                && nxpc.MaLop == malop && nxpc.MaHocSinh == st.MaHocSinh && nxpc.HocKy == 1
                                                    select nxpc.NXPhamChatTX4).FirstOrDefault(),

                                   KhenThuong = (from hskt in db.HocSinh_KhenThuongKyLuat where hskt.idTruong == idtruong && hskt.MaHocKy == mahk && hskt.MaLop == malop && hskt.MaHocSinh == st.MaHocSinh select hskt),

                                   hoanThanhCT = "",


                                   nhanXetGVCN = (from nx in db.HocSinh_DanhGiaThang_GVCN
                                                  where nx.idTruong == idtruong && nx.NamHoc == namhoc && nx.MaLop == malop && nx.MaHocSinh == st.MaHocSinh
                                                  select new NhanXetGVCNMonHoc
                                                  {
                                                      NXKT = nx.NXT5_KT,
                                                      NXNL = nx.NXT5_NL,
                                                      NXPC = nx.NXT5_PC
                                                  }).FirstOrDefault()


                               }).ToList();



                List<SoLienLac> hocSinhs = filterHocSinh(mahocsinh, hocSinh);
                string tenLop = Cmon.GetnameClass(malop, idtruong, namhoc);
                string userName = User.Identity.Name.ToString() == "" ? "superadmin" : User.Identity.Name.ToString();
                if (!string.IsNullOrEmpty(mahocsinh) && hocSinhs != null && hocSinhs.Count > 0)
                {
                    string tenDonViChuQuan = Cmon.getDonViChuQuan(idtruong);
                    ExcelFileWriterSoLienLac<object> myExcel = new ExcelWriteSoLienLac();
                    string tenFile = "PhieuLienLac_Template.xls";

                    // định nghĩa thư mục chứa template
                    string rootPathToTemplate = Server.MapPath("~/templateExcell/" + tenFile);

                    string fileName = userName + "_PhieuLienLac";

                    string rootProcessTemplate = Server.MapPath(System.IO.Path.GetDirectoryName("~/ProcessTemplate/")); // thư mục chứa các template sẽ được đem xử lý đổ dữ liệu

                    string fileNameExcute = rootProcessTemplate + "\\" + fileName + ".xls"; // tạo một template để chuẩn bị cho xử lý
                    System.IO.File.Delete(fileNameExcute);// xóa file tạm trước đó và tạo lại
                    System.IO.File.Copy(rootPathToTemplate, fileNameExcute, true);// copy từ thư mục template tới thư mục chuẩn bị xử lý

                    // Xử lý đổ dữ liệu vào các ô tương ứng
                    myExcel.ExportSoLienLacToExcel(fileNameExcute, mahk, namhoc, tenDonViChuQuan, tentruong, tenLop, makhoi, hocSinhs, gvcn, malop);

                    System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
                    Cmon.stopExcel();

                    // lấy ngày giờ hiện tại của hệ thống 
                    DateTime ngayHienTai = DateTime.Now;
                    string ngayThang = ngayHienTai.Date.ToString("dd/MM/yyyy") + "_" + ngayHienTai.Hour.ToString() + "_" + ngayHienTai.Minute.ToString() + "_" + ngayHienTai.Second.ToString();

                    return File(fileNameExcute, "application/ms-excel", "PhieuLienLac_" + userName + "_" + ngayThang + ".xls");


                }


            }
            return File("PhieuLienLac.xls", "application/ms-excel", "PhieuLienLac.xls");
        }

        [Authorize]
        public ActionResult SoLienLacXuatExcellGHK2(int idtruong, string makhoi, string malop, string mahk, string namhoc, string tentruong, string mahocsinh)
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Thongbao");
            }
            else
            {
                var gvcn = (from usname in db.ScrUsers
                            join usr in db.Users_LopHoc
                            on usname.UserName equals usr.UserName
                            where usr.idTruong == idtruong && usr.NamHoc == namhoc && usr.MaLop == malop
                            select usname).FirstOrDefault().FullName;
                var hocSinh = (from st in db.HocSinhs
                               where st.idTruong == idtruong && st.NamHoc == namhoc && st.MaLop == malop && mahocsinh.Contains(st.MaHocSinh)
                               select new SoLienLac
                               {
                                   stt = st.STT,
                                   maHS = st.MaHocSinh,
                                   hoTenHS = st.HoTen,
                                   hoTenCha = st.HoTenCha,
                                   ngheNghiepCha = st.NgheCha,
                                   hoTenMe = st.HoTenMe,
                                   ngheNghiepMe = st.NgheMe,
                                   diaChi = st.Diachilienlac,
                                   giaoVien = gvcn,
                                   dienThoai = st.DienThoai,

                                   Toan = (from diemToan in db.HocSinh_Diem_NhanXet
                                           where diemToan.idTruong == idtruong && diemToan.NamHoc == namhoc
                                               && diemToan.MaLop == malop && diemToan.MaMonHoc == "TOAN" && diemToan.MaHocSinh == st.MaHocSinh
                                           select diemToan.DiemGHK2).FirstOrDefault(),
                                   KQMonHocToan = (from diemToan in db.HocSinh_Diem_NhanXet
                                                   where diemToan.idTruong == idtruong && diemToan.NamHoc == namhoc
                                                       && diemToan.MaLop == malop && diemToan.MaMonHoc == "TOAN" && diemToan.MaHocSinh == st.MaHocSinh
                                                   select diemToan.KQMonHocGHK2).FirstOrDefault(),

                                   TV = (from diemTV in db.HocSinh_Diem_NhanXet
                                         where diemTV.idTruong == idtruong && diemTV.NamHoc == namhoc
                                               && diemTV.MaLop == malop && diemTV.MaMonHoc == "TV" && diemTV.MaHocSinh == st.MaHocSinh
                                         select diemTV.DiemGHK2).FirstOrDefault(),

                                   KQMonHocTV = (from diemTV in db.HocSinh_Diem_NhanXet
                                                 where diemTV.idTruong == idtruong && diemTV.NamHoc == namhoc
                                                       && diemTV.MaLop == malop && diemTV.MaMonHoc == "TV" && diemTV.MaHocSinh == st.MaHocSinh
                                                 select diemTV.KQMonHocGHK2).FirstOrDefault(),

                                   TNXH = (from diemTNXH in db.HocSinh_Diem_NhanXet
                                           where diemTNXH.idTruong == idtruong && diemTNXH.NamHoc == namhoc
                                               && diemTNXH.MaLop == malop && diemTNXH.MaMonHoc == "TNXH" && diemTNXH.MaHocSinh == st.MaHocSinh
                                           select diemTNXH.DiemGHK2).FirstOrDefault(),

                                   KQMonHocTNXH = (from diemTNXH in db.HocSinh_Diem_NhanXet
                                                   where diemTNXH.idTruong == idtruong && diemTNXH.NamHoc == namhoc
                                                       && diemTNXH.MaLop == malop && diemTNXH.MaMonHoc == "TNXH" && diemTNXH.MaHocSinh == st.MaHocSinh
                                                   select diemTNXH.KQMonHocGHK2).FirstOrDefault(),

                                   KH = (from diemKH in db.HocSinh_Diem_NhanXet
                                         where diemKH.idTruong == idtruong && diemKH.NamHoc == namhoc
                                             && diemKH.MaLop == malop && diemKH.MaMonHoc == "KH" && diemKH.MaHocSinh == st.MaHocSinh
                                         select diemKH.DiemGHK2).FirstOrDefault(),

                                   KQMonHocKH = (from diemKH in db.HocSinh_Diem_NhanXet
                                                 where diemKH.idTruong == idtruong && diemKH.NamHoc == namhoc
                                                       && diemKH.MaLop == malop && diemKH.MaMonHoc == "KH" && diemKH.MaHocSinh == st.MaHocSinh
                                                 select diemKH.KQMonHocGHK2).FirstOrDefault(),

                                   LS = (from diemLS in db.HocSinh_Diem_NhanXet
                                         where diemLS.idTruong == idtruong && diemLS.NamHoc == namhoc
                                             && diemLS.MaLop == malop && diemLS.MaMonHoc == "LS" && diemLS.MaHocSinh == st.MaHocSinh
                                         select diemLS.DiemGHK2).FirstOrDefault(),

                                   KQMonHocLS = (from diemLS in db.HocSinh_Diem_NhanXet
                                                 where diemLS.idTruong == idtruong && diemLS.NamHoc == namhoc
                                                       && diemLS.MaLop == malop && diemLS.MaMonHoc == "LS" && diemLS.MaHocSinh == st.MaHocSinh
                                                 select diemLS.KQMonHocGHK2).FirstOrDefault(),

                                   DD = (from diemDD in db.HocSinh_Diem_NhanXet
                                         where diemDD.idTruong == idtruong && diemDD.NamHoc == namhoc
                                             && diemDD.MaLop == malop && diemDD.MaMonHoc == "DD" && diemDD.MaHocSinh == st.MaHocSinh
                                         select diemDD.DiemGHK2).FirstOrDefault(),

                                   KQMonHocDD = (from diemDD in db.HocSinh_Diem_NhanXet
                                                 where diemDD.idTruong == idtruong && diemDD.NamHoc == namhoc
                                                       && diemDD.MaLop == malop && diemDD.MaMonHoc == "DD" && diemDD.MaHocSinh == st.MaHocSinh
                                                 select diemDD.KQMonHocGHK2).FirstOrDefault(),


                                   TC = (from diemTC in db.HocSinh_Diem_NhanXet
                                         where diemTC.idTruong == idtruong && diemTC.NamHoc == namhoc
                                             && diemTC.MaLop == malop && diemTC.MaMonHoc == "TC" && diemTC.MaHocSinh == st.MaHocSinh
                                         select diemTC.DiemGHK2).FirstOrDefault(),

                                   KQMonHocTC = (from diemTC in db.HocSinh_Diem_NhanXet
                                                 where diemTC.idTruong == idtruong && diemTC.NamHoc == namhoc
                                                       && diemTC.MaLop == malop && diemTC.MaMonHoc == "TC" && diemTC.MaHocSinh == st.MaHocSinh
                                                 select diemTC.KQMonHocGHK2).FirstOrDefault(),

                                   KT = (from diemKT in db.HocSinh_Diem_NhanXet
                                         where diemKT.idTruong == idtruong && diemKT.NamHoc == namhoc
                                             && diemKT.MaLop == malop && diemKT.MaMonHoc == "KT" && diemKT.MaHocSinh == st.MaHocSinh
                                         select diemKT.DiemGHK2).FirstOrDefault(),

                                   KQMonHocKT = (from diemKT in db.HocSinh_Diem_NhanXet
                                                 where diemKT.idTruong == idtruong && diemKT.NamHoc == namhoc
                                                       && diemKT.MaLop == malop && diemKT.MaMonHoc == "KT" && diemKT.MaHocSinh == st.MaHocSinh
                                                 select diemKT.KQMonHocGHK2).FirstOrDefault(),

                                   MT = (from diemMT in db.HocSinh_Diem_NhanXet
                                         where diemMT.idTruong == idtruong && diemMT.NamHoc == namhoc
                                             && diemMT.MaLop == malop && diemMT.MaMonHoc == "MT" && diemMT.MaHocSinh == st.MaHocSinh
                                         select diemMT.DiemGHK2).FirstOrDefault(),

                                   KQMonHocMT = (from diemMT in db.HocSinh_Diem_NhanXet
                                                 where diemMT.idTruong == idtruong && diemMT.NamHoc == namhoc
                                                       && diemMT.MaLop == malop && diemMT.MaMonHoc == "MT" && diemMT.MaHocSinh == st.MaHocSinh
                                                 select diemMT.KQMonHocGHK2).FirstOrDefault(),

                                   AN = (from diemAN in db.HocSinh_Diem_NhanXet
                                         where diemAN.idTruong == idtruong && diemAN.NamHoc == namhoc
                                             && diemAN.MaLop == malop && diemAN.MaMonHoc == "AN" && diemAN.MaHocSinh == st.MaHocSinh
                                         select diemAN.DiemGHK2).FirstOrDefault(),

                                   KQMonHocAN = (from diemAN in db.HocSinh_Diem_NhanXet
                                                 where diemAN.idTruong == idtruong && diemAN.NamHoc == namhoc
                                                       && diemAN.MaLop == malop && diemAN.MaMonHoc == "AN" && diemAN.MaHocSinh == st.MaHocSinh
                                                 select diemAN.KQMonHocGHK2).FirstOrDefault(),

                                   TD = (from diemTD in db.HocSinh_Diem_NhanXet
                                         where diemTD.idTruong == idtruong && diemTD.NamHoc == namhoc
                                             && diemTD.MaLop == malop && diemTD.MaMonHoc == "TD" && diemTD.MaHocSinh == st.MaHocSinh
                                         select diemTD.DiemGHK2).FirstOrDefault(),

                                   KQMonHocTD = (from diemTD in db.HocSinh_Diem_NhanXet
                                                 where diemTD.idTruong == idtruong && diemTD.NamHoc == namhoc
                                                       && diemTD.MaLop == malop && diemTD.MaMonHoc == "TD" && diemTD.MaHocSinh == st.MaHocSinh
                                                 select diemTD.KQMonHocGHK2).FirstOrDefault(),

                                   NN = (from diemNN in db.HocSinh_Diem_NhanXet
                                         where diemNN.idTruong == idtruong && diemNN.NamHoc == namhoc
                                             && diemNN.MaLop == malop && diemNN.MaMonHoc == "NN" && diemNN.MaHocSinh == st.MaHocSinh
                                         select diemNN.DiemGHK2).FirstOrDefault(),

                                   KQMonHocNN = (from diemNN in db.HocSinh_Diem_NhanXet
                                                 where diemNN.idTruong == idtruong && diemNN.NamHoc == namhoc
                                                       && diemNN.MaLop == malop && diemNN.MaMonHoc == "NN" && diemNN.MaHocSinh == st.MaHocSinh
                                                 select diemNN.KQMonHocGHK2).FirstOrDefault(),


                                   TinHoc = (from diemTH in db.HocSinh_Diem_NhanXet
                                             where diemTH.idTruong == idtruong && diemTH.NamHoc == namhoc
                                             && diemTH.MaLop == malop && diemTH.MaMonHoc == "TH" && diemTH.MaHocSinh == st.MaHocSinh
                                             select diemTH.DiemGHK2).FirstOrDefault(),

                                   KQMonHocTinHoc = (from diemNN in db.HocSinh_Diem_NhanXet
                                                     where diemNN.idTruong == idtruong && diemNN.NamHoc == namhoc
                                                           && diemNN.MaLop == malop && diemNN.MaMonHoc == "TH" && diemNN.MaHocSinh == st.MaHocSinh
                                                     select diemNN.KQMonHocGHK2).FirstOrDefault(),

                                   TiengDanToc = (from diemTDT in db.HocSinh_Diem_NhanXet
                                                  where diemTDT.idTruong == idtruong && diemTDT.NamHoc == namhoc
                                             && diemTDT.MaLop == malop && diemTDT.MaMonHoc == "TDT" && diemTDT.MaHocSinh == st.MaHocSinh
                                                  select diemTDT.DiemGHK2).FirstOrDefault(),

                                   KQMonHocTiengDanToc = (from diemTDT in db.HocSinh_Diem_NhanXet
                                                          where diemTDT.idTruong == idtruong && diemTDT.NamHoc == namhoc
                                                       && diemTDT.MaLop == malop && diemTDT.MaMonHoc == "TDT" && diemTDT.MaHocSinh == st.MaHocSinh
                                                          select diemTDT.KQMonHocGHK2).FirstOrDefault(),

                                   NXNangLucTX1 = (from nxnl in db.HocSinh_TongHopNX
                                                   where nxnl.idTruong == idtruong && nxnl.NamHoc == namhoc
                                                && nxnl.MaLop == malop && nxnl.MaHocSinh == st.MaHocSinh && nxnl.HocKy == 2
                                                   select nxnl.NXNangLucTX1).FirstOrDefault(),

                                   NXNangLucTX2 = (from nxnl in db.HocSinh_TongHopNX
                                                   where nxnl.idTruong == idtruong && nxnl.NamHoc == namhoc
                                                && nxnl.MaLop == malop && nxnl.MaHocSinh == st.MaHocSinh && nxnl.HocKy == 2
                                                   select nxnl.NXNangLucTX2).FirstOrDefault(),

                                   NXNangLucTX3 = (from nxnl in db.HocSinh_TongHopNX
                                                   where nxnl.idTruong == idtruong && nxnl.NamHoc == namhoc
                                                && nxnl.MaLop == malop && nxnl.MaHocSinh == st.MaHocSinh && nxnl.HocKy == 2
                                                   select nxnl.NXNangLucTX3).FirstOrDefault(),


                                   NXPhamChatTX1 = (from nxpc in db.HocSinh_TongHopNX
                                                    where nxpc.idTruong == idtruong && nxpc.NamHoc == namhoc
                                                && nxpc.MaLop == malop && nxpc.MaHocSinh == st.MaHocSinh && nxpc.HocKy == 2
                                                    select nxpc.NXPhamChatTX1).FirstOrDefault(),

                                   NXPhamChatTX2 = (from nxpc in db.HocSinh_TongHopNX
                                                    where nxpc.idTruong == idtruong && nxpc.NamHoc == namhoc
                                                && nxpc.MaLop == malop && nxpc.MaHocSinh == st.MaHocSinh && nxpc.HocKy == 2
                                                    select nxpc.NXPhamChatTX2).FirstOrDefault(),

                                   NXPhamChatTX3 = (from nxpc in db.HocSinh_TongHopNX
                                                    where nxpc.idTruong == idtruong && nxpc.NamHoc == namhoc
                                                && nxpc.MaLop == malop && nxpc.MaHocSinh == st.MaHocSinh && nxpc.HocKy == 2
                                                    select nxpc.NXPhamChatTX3).FirstOrDefault(),

                                   NXPhamChatTX4 = (from nxpc in db.HocSinh_TongHopNX
                                                    where nxpc.idTruong == idtruong && nxpc.NamHoc == namhoc
                                                && nxpc.MaLop == malop && nxpc.MaHocSinh == st.MaHocSinh && nxpc.HocKy == 2
                                                    select nxpc.NXPhamChatTX4).FirstOrDefault(),

                                   KhenThuong = (from hskt in db.HocSinh_KhenThuongKyLuat where hskt.idTruong == idtruong && hskt.MaHocKy == mahk && hskt.MaLop == malop && hskt.MaHocSinh == st.MaHocSinh select hskt),

                                   hoanThanhCT = "",


                                   nhanXetGVCN = (from nx in db.HocSinh_DanhGiaThang_GVCN
                                                  where nx.idTruong == idtruong && nx.NamHoc == namhoc && nx.MaLop == malop && nx.MaHocSinh == st.MaHocSinh
                                                  select new NhanXetGVCNMonHoc
                                                  {
                                                      NXKT = nx.NXT8_KT,
                                                      NXNL = nx.NXT8_NL,
                                                      NXPC = nx.NXT8_PC
                                                  }).FirstOrDefault()


                               }).ToList();



                List<SoLienLac> hocSinhs = filterHocSinh(mahocsinh, hocSinh);
                string tenLop = Cmon.GetnameClass(malop, idtruong, namhoc);
                string userName = User.Identity.Name.ToString() == "" ? "superadmin" : User.Identity.Name.ToString();
                if (!string.IsNullOrEmpty(mahocsinh) && hocSinhs != null && hocSinhs.Count > 0)
                {
                    string tenDonViChuQuan = Cmon.getDonViChuQuan(idtruong);
                    ExcelFileWriterSoLienLac<object> myExcel = new ExcelWriteSoLienLac();
                    string tenFile = "PhieuLienLac_Template.xls";

                    // định nghĩa thư mục chứa template
                    string rootPathToTemplate = Server.MapPath("~/templateExcell/" + tenFile);

                    string fileName = userName + "_PhieuLienLac";

                    string rootProcessTemplate = Server.MapPath(System.IO.Path.GetDirectoryName("~/ProcessTemplate/")); // thư mục chứa các template sẽ được đem xử lý đổ dữ liệu

                    string fileNameExcute = rootProcessTemplate + "\\" + fileName + ".xls"; // tạo một template để chuẩn bị cho xử lý
                    System.IO.File.Delete(fileNameExcute);// xóa file tạm trước đó và tạo lại
                    System.IO.File.Copy(rootPathToTemplate, fileNameExcute, true);// copy từ thư mục template tới thư mục chuẩn bị xử lý

                    // Xử lý đổ dữ liệu vào các ô tương ứng
                    myExcel.ExportSoLienLacToExcel(fileNameExcute, mahk, namhoc, tenDonViChuQuan, tentruong, tenLop, makhoi, hocSinhs, gvcn,malop);

                    System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
                    Cmon.stopExcel();

                    // lấy ngày giờ hiện tại của hệ thống 
                    DateTime ngayHienTai = DateTime.Now;
                    string ngayThang = ngayHienTai.Date.ToString("dd/MM/yyyy") + "_" + ngayHienTai.Hour.ToString() + "_" + ngayHienTai.Minute.ToString() + "_" + ngayHienTai.Second.ToString();

                    return File(fileNameExcute, "application/ms-excel", "PhieuLienLac_" + userName + "_" + ngayThang + ".xls");


                }


            }
            return File("PhieuLienLac.xls", "application/ms-excel", "PhieuLienLac.xls");
        }


        [Authorize]
        public ActionResult SoLienLacXuatExcellHK2(int idtruong, string makhoi, string malop, string mahk, string namhoc, string tentruong, string mahocsinh)
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Thongbao");
            }
            else
            {
                var gvcn = (from usname in db.ScrUsers
                            join usr in db.Users_LopHoc
                            on usname.UserName equals usr.UserName
                            where usr.idTruong == idtruong && usr.NamHoc == namhoc && usr.MaLop == malop
                            select usname).FirstOrDefault().FullName;
                var hocSinh = (from st in db.HocSinhs
                               where st.idTruong == idtruong && st.NamHoc == namhoc && st.MaLop == malop && mahocsinh.Contains(st.MaHocSinh)
                               select new SoLienLac
                               {
                                   stt = st.STT,
                                   maHS = st.MaHocSinh,
                                   hoTenHS = st.HoTen,
                                   hoTenCha = st.HoTenCha,
                                   ngheNghiepCha = st.NgheCha,
                                   hoTenMe = st.HoTenMe,
                                   ngheNghiepMe = st.NgheMe,
                                   diaChi = st.Diachilienlac,
                                   giaoVien = gvcn,
                                   dienThoai = st.DienThoai,

                                   Toan = (from diemToan in db.HocSinh_Diem_NhanXet
                                           where diemToan.idTruong == idtruong && diemToan.NamHoc == namhoc
                                               && diemToan.MaLop == malop && diemToan.MaMonHoc == "TOAN" && diemToan.MaHocSinh == st.MaHocSinh
                                           select diemToan.DiemCN).FirstOrDefault(),
                                   KQMonHocToan = (from diemToan in db.HocSinh_Diem_NhanXet
                                                   where diemToan.idTruong == idtruong && diemToan.NamHoc == namhoc
                                                       && diemToan.MaLop == malop && diemToan.MaMonHoc == "TOAN" && diemToan.MaHocSinh == st.MaHocSinh
                                                   select diemToan.KQMonHocCN).FirstOrDefault(),

                                   TV = (from diemTV in db.HocSinh_Diem_NhanXet
                                         where diemTV.idTruong == idtruong && diemTV.NamHoc == namhoc
                                               && diemTV.MaLop == malop && diemTV.MaMonHoc == "TV" && diemTV.MaHocSinh == st.MaHocSinh
                                         select diemTV.DiemCN).FirstOrDefault(),

                                   KQMonHocTV = (from diemTV in db.HocSinh_Diem_NhanXet
                                                 where diemTV.idTruong == idtruong && diemTV.NamHoc == namhoc
                                                       && diemTV.MaLop == malop && diemTV.MaMonHoc == "TV" && diemTV.MaHocSinh == st.MaHocSinh
                                                 select diemTV.KQMonHocCN).FirstOrDefault(),

                                   TNXH = (from diemTNXH in db.HocSinh_Diem_NhanXet
                                           where diemTNXH.idTruong == idtruong && diemTNXH.NamHoc == namhoc
                                               && diemTNXH.MaLop == malop && diemTNXH.MaMonHoc == "TNXH" && diemTNXH.MaHocSinh == st.MaHocSinh
                                           select diemTNXH.DiemCN).FirstOrDefault(),

                                   KQMonHocTNXH = (from diemTNXH in db.HocSinh_Diem_NhanXet
                                                   where diemTNXH.idTruong == idtruong && diemTNXH.NamHoc == namhoc
                                                       && diemTNXH.MaLop == malop && diemTNXH.MaMonHoc == "TNXH" && diemTNXH.MaHocSinh == st.MaHocSinh
                                                   select diemTNXH.KQMonHocCN).FirstOrDefault(),

                                   KH = (from diemKH in db.HocSinh_Diem_NhanXet
                                         where diemKH.idTruong == idtruong && diemKH.NamHoc == namhoc
                                             && diemKH.MaLop == malop && diemKH.MaMonHoc == "KH" && diemKH.MaHocSinh == st.MaHocSinh
                                         select diemKH.DiemCN).FirstOrDefault(),

                                   KQMonHocKH = (from diemKH in db.HocSinh_Diem_NhanXet
                                                 where diemKH.idTruong == idtruong && diemKH.NamHoc == namhoc
                                                       && diemKH.MaLop == malop && diemKH.MaMonHoc == "KH" && diemKH.MaHocSinh == st.MaHocSinh
                                                 select diemKH.KQMonHocCN).FirstOrDefault(),

                                   LS = (from diemLS in db.HocSinh_Diem_NhanXet
                                         where diemLS.idTruong == idtruong && diemLS.NamHoc == namhoc
                                             && diemLS.MaLop == malop && diemLS.MaMonHoc == "LS" && diemLS.MaHocSinh == st.MaHocSinh
                                         select diemLS.DiemCN).FirstOrDefault(),

                                   KQMonHocLS = (from diemLS in db.HocSinh_Diem_NhanXet
                                                 where diemLS.idTruong == idtruong && diemLS.NamHoc == namhoc
                                                       && diemLS.MaLop == malop && diemLS.MaMonHoc == "LS" && diemLS.MaHocSinh == st.MaHocSinh
                                                 select diemLS.KQMonHocCN).FirstOrDefault(),

                                   DD = (from diemDD in db.HocSinh_Diem_NhanXet
                                         where diemDD.idTruong == idtruong && diemDD.NamHoc == namhoc
                                             && diemDD.MaLop == malop && diemDD.MaMonHoc == "DD" && diemDD.MaHocSinh == st.MaHocSinh
                                         select diemDD.DiemCN).FirstOrDefault(),

                                   KQMonHocDD = (from diemDD in db.HocSinh_Diem_NhanXet
                                                 where diemDD.idTruong == idtruong && diemDD.NamHoc == namhoc
                                                       && diemDD.MaLop == malop && diemDD.MaMonHoc == "DD" && diemDD.MaHocSinh == st.MaHocSinh
                                                 select diemDD.KQMonHocCN).FirstOrDefault(),


                                   TC = (from diemTC in db.HocSinh_Diem_NhanXet
                                         where diemTC.idTruong == idtruong && diemTC.NamHoc == namhoc
                                             && diemTC.MaLop == malop && diemTC.MaMonHoc == "TC" && diemTC.MaHocSinh == st.MaHocSinh
                                         select diemTC.DiemCN).FirstOrDefault(),

                                   KQMonHocTC = (from diemTC in db.HocSinh_Diem_NhanXet
                                                 where diemTC.idTruong == idtruong && diemTC.NamHoc == namhoc
                                                       && diemTC.MaLop == malop && diemTC.MaMonHoc == "TC" && diemTC.MaHocSinh == st.MaHocSinh
                                                 select diemTC.KQMonHocCN).FirstOrDefault(),

                                   KT = (from diemKT in db.HocSinh_Diem_NhanXet
                                         where diemKT.idTruong == idtruong && diemKT.NamHoc == namhoc
                                             && diemKT.MaLop == malop && diemKT.MaMonHoc == "KT" && diemKT.MaHocSinh == st.MaHocSinh
                                         select diemKT.DiemCN).FirstOrDefault(),

                                   KQMonHocKT = (from diemKT in db.HocSinh_Diem_NhanXet
                                                 where diemKT.idTruong == idtruong && diemKT.NamHoc == namhoc
                                                       && diemKT.MaLop == malop && diemKT.MaMonHoc == "KT" && diemKT.MaHocSinh == st.MaHocSinh
                                                 select diemKT.KQMonHocCN).FirstOrDefault(),

                                   MT = (from diemMT in db.HocSinh_Diem_NhanXet
                                         where diemMT.idTruong == idtruong && diemMT.NamHoc == namhoc
                                             && diemMT.MaLop == malop && diemMT.MaMonHoc == "MT" && diemMT.MaHocSinh == st.MaHocSinh
                                         select diemMT.DiemCN).FirstOrDefault(),

                                   KQMonHocMT = (from diemMT in db.HocSinh_Diem_NhanXet
                                                 where diemMT.idTruong == idtruong && diemMT.NamHoc == namhoc
                                                       && diemMT.MaLop == malop && diemMT.MaMonHoc == "MT" && diemMT.MaHocSinh == st.MaHocSinh
                                                 select diemMT.KQMonHocCN).FirstOrDefault(),

                                   AN = (from diemAN in db.HocSinh_Diem_NhanXet
                                         where diemAN.idTruong == idtruong && diemAN.NamHoc == namhoc
                                             && diemAN.MaLop == malop && diemAN.MaMonHoc == "AN" && diemAN.MaHocSinh == st.MaHocSinh
                                         select diemAN.DiemCN).FirstOrDefault(),

                                   KQMonHocAN = (from diemAN in db.HocSinh_Diem_NhanXet
                                                 where diemAN.idTruong == idtruong && diemAN.NamHoc == namhoc
                                                       && diemAN.MaLop == malop && diemAN.MaMonHoc == "AN" && diemAN.MaHocSinh == st.MaHocSinh
                                                 select diemAN.KQMonHocCN).FirstOrDefault(),

                                   TD = (from diemTD in db.HocSinh_Diem_NhanXet
                                         where diemTD.idTruong == idtruong && diemTD.NamHoc == namhoc
                                             && diemTD.MaLop == malop && diemTD.MaMonHoc == "TD" && diemTD.MaHocSinh == st.MaHocSinh
                                         select diemTD.DiemCN).FirstOrDefault(),

                                   KQMonHocTD = (from diemTD in db.HocSinh_Diem_NhanXet
                                                 where diemTD.idTruong == idtruong && diemTD.NamHoc == namhoc
                                                       && diemTD.MaLop == malop && diemTD.MaMonHoc == "TD" && diemTD.MaHocSinh == st.MaHocSinh
                                                 select diemTD.KQMonHocCN).FirstOrDefault(),

                                   NN = (from diemNN in db.HocSinh_Diem_NhanXet
                                         where diemNN.idTruong == idtruong && diemNN.NamHoc == namhoc
                                             && diemNN.MaLop == malop && diemNN.MaMonHoc == "NN" && diemNN.MaHocSinh == st.MaHocSinh
                                         select diemNN.DiemCN).FirstOrDefault(),

                                   KQMonHocNN = (from diemNN in db.HocSinh_Diem_NhanXet
                                                 where diemNN.idTruong == idtruong && diemNN.NamHoc == namhoc
                                                       && diemNN.MaLop == malop && diemNN.MaMonHoc == "NN" && diemNN.MaHocSinh == st.MaHocSinh
                                                 select diemNN.KQMonHocCN).FirstOrDefault(),


                                   TinHoc = (from diemTH in db.HocSinh_Diem_NhanXet
                                             where diemTH.idTruong == idtruong && diemTH.NamHoc == namhoc
                                             && diemTH.MaLop == malop && diemTH.MaMonHoc == "TH" && diemTH.MaHocSinh == st.MaHocSinh
                                             select diemTH.DiemCN).FirstOrDefault(),

                                   KQMonHocTinHoc = (from diemNN in db.HocSinh_Diem_NhanXet
                                                     where diemNN.idTruong == idtruong && diemNN.NamHoc == namhoc
                                                           && diemNN.MaLop == malop && diemNN.MaMonHoc == "TH" && diemNN.MaHocSinh == st.MaHocSinh
                                                     select diemNN.KQMonHocCN).FirstOrDefault(),

                                   TiengDanToc = (from diemTDT in db.HocSinh_Diem_NhanXet
                                                  where diemTDT.idTruong == idtruong && diemTDT.NamHoc == namhoc
                                             && diemTDT.MaLop == malop && diemTDT.MaMonHoc == "TDT" && diemTDT.MaHocSinh == st.MaHocSinh
                                                  select diemTDT.DiemCN).FirstOrDefault(),

                                   KQMonHocTiengDanToc = (from diemTDT in db.HocSinh_Diem_NhanXet
                                                          where diemTDT.idTruong == idtruong && diemTDT.NamHoc == namhoc
                                                       && diemTDT.MaLop == malop && diemTDT.MaMonHoc == "TDT" && diemTDT.MaHocSinh == st.MaHocSinh
                                                          select diemTDT.KQMonHocCN).FirstOrDefault(),

                                   NXNangLucTX1 = (from nxnl in db.HocSinh_TongHopNX
                                                   where nxnl.idTruong == idtruong && nxnl.NamHoc == namhoc
                                                && nxnl.MaLop == malop && nxnl.MaHocSinh == st.MaHocSinh && nxnl.HocKy == 3
                                                   select nxnl.NXNangLucTX1).FirstOrDefault(),

                                   NXNangLucTX2 = (from nxnl in db.HocSinh_TongHopNX
                                                   where nxnl.idTruong == idtruong && nxnl.NamHoc == namhoc
                                                && nxnl.MaLop == malop && nxnl.MaHocSinh == st.MaHocSinh && nxnl.HocKy == 3
                                                   select nxnl.NXNangLucTX2).FirstOrDefault(),

                                   NXNangLucTX3 = (from nxnl in db.HocSinh_TongHopNX
                                                   where nxnl.idTruong == idtruong && nxnl.NamHoc == namhoc
                                                && nxnl.MaLop == malop && nxnl.MaHocSinh == st.MaHocSinh && nxnl.HocKy == 3
                                                   select nxnl.NXNangLucTX3).FirstOrDefault(),


                                   NXPhamChatTX1 = (from nxpc in db.HocSinh_TongHopNX
                                                    where nxpc.idTruong == idtruong && nxpc.NamHoc == namhoc
                                                && nxpc.MaLop == malop && nxpc.MaHocSinh == st.MaHocSinh && nxpc.HocKy == 3
                                                    select nxpc.NXPhamChatTX1).FirstOrDefault(),

                                   NXPhamChatTX2 = (from nxpc in db.HocSinh_TongHopNX
                                                    where nxpc.idTruong == idtruong && nxpc.NamHoc == namhoc
                                                && nxpc.MaLop == malop && nxpc.MaHocSinh == st.MaHocSinh && nxpc.HocKy == 3
                                                    select nxpc.NXPhamChatTX2).FirstOrDefault(),

                                   NXPhamChatTX3 = (from nxpc in db.HocSinh_TongHopNX
                                                    where nxpc.idTruong == idtruong && nxpc.NamHoc == namhoc
                                                && nxpc.MaLop == malop && nxpc.MaHocSinh == st.MaHocSinh && nxpc.HocKy == 3
                                                    select nxpc.NXPhamChatTX3).FirstOrDefault(),

                                   NXPhamChatTX4 = (from nxpc in db.HocSinh_TongHopNX
                                                    where nxpc.idTruong == idtruong && nxpc.NamHoc == namhoc
                                                && nxpc.MaLop == malop && nxpc.MaHocSinh == st.MaHocSinh && nxpc.HocKy == 3
                                                    select nxpc.NXPhamChatTX4).FirstOrDefault(),

                                   KhenThuong = (from hskt in db.HocSinh_KhenThuongKyLuat where hskt.idTruong == idtruong && hskt.MaHocKy == mahk && hskt.MaLop == malop && hskt.MaHocSinh == st.MaHocSinh select hskt),

                                   hoanThanhCT = (from nl in db.HocSinh_TongHopNX
                                                  where nl.idTruong == idtruong && nl.NamHoc == namhoc && nl.MaLop == malop
                                                      && nl.MaHocSinh == st.MaHocSinh && nl.HocKy == 3
                                                  select nl).FirstOrDefault().HoanThanhCT,

                                   DuocLenLop = (from nl in db.HocSinh_TongHopNX
                                                 where nl.idTruong == idtruong && nl.NamHoc == namhoc && nl.MaLop == malop
                                                     && nl.MaHocSinh == st.MaHocSinh && nl.HocKy == 3 
                                                 select nl).FirstOrDefault().DuocLenLop,


                                   nhanXetGVCN = (from nx in db.HocSinh_DanhGiaThang_GVCN
                                                  where nx.idTruong == idtruong && nx.NamHoc == namhoc && nx.MaLop == malop && nx.MaHocSinh == st.MaHocSinh
                                                  select new NhanXetGVCNMonHoc
                                                  {
                                                      NXKT = nx.NXT10_KT,
                                                      NXNL = nx.NXT10_NL,
                                                      NXPC = nx.NXT10_PC
                                                  }).FirstOrDefault()


                           }).ToList();



                List<SoLienLac> hocSinhs = filterHocSinh(mahocsinh, hocSinh);
                string tenLop = Cmon.GetnameClass(malop, idtruong, namhoc);
                string userName = User.Identity.Name.ToString() == "" ? "superadmin" : User.Identity.Name.ToString();
                if (!string.IsNullOrEmpty(mahocsinh) && hocSinhs != null && hocSinhs.Count > 0)
                {
                    string tenDonViChuQuan = Cmon.getDonViChuQuan(idtruong);
                    ExcelFileWriterSoLienLac<object> myExcel = new ExcelWriteSoLienLac();
                    string tenFile = "PhieuLienLac_Template_CN.xls";

                    // định nghĩa thư mục chứa template
                    string rootPathToTemplate = Server.MapPath("~/templateExcell/" + tenFile);

                    string fileName = userName + "_PhieuLienLac";

                    string rootProcessTemplate = Server.MapPath(System.IO.Path.GetDirectoryName("~/ProcessTemplate/")); // thư mục chứa các template sẽ được đem xử lý đổ dữ liệu

                    string fileNameExcute = rootProcessTemplate + "\\" + fileName + ".xls"; // tạo một template để chuẩn bị cho xử lý
                    System.IO.File.Delete(fileNameExcute);// xóa file tạm trước đó và tạo lại
                    System.IO.File.Copy(rootPathToTemplate, fileNameExcute, true);// copy từ thư mục template tới thư mục chuẩn bị xử lý

                    // Xử lý đổ dữ liệu vào các ô tương ứng
                    myExcel.ExportSoLienLacToExcel(fileNameExcute, mahk, namhoc, tenDonViChuQuan, tentruong, tenLop, makhoi, hocSinhs, gvcn, malop);

                    System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
                    Cmon.stopExcel();

                    // lấy ngày giờ hiện tại của hệ thống 
                    DateTime ngayHienTai = DateTime.Now;
                    string ngayThang = ngayHienTai.Date.ToString("dd/MM/yyyy") + "_" + ngayHienTai.Hour.ToString() + "_" + ngayHienTai.Minute.ToString() + "_" + ngayHienTai.Second.ToString();

                    return File(fileNameExcute, "application/ms-excel", "PhieuLienLac_" + userName + "_" + ngayThang + ".xls");


                }


            }
            return File("PhieuLienLac.xls", "application/ms-excel", "PhieuLienLac.xls");
        }


 
    }
}