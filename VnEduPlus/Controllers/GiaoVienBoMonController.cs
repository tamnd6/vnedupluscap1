﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Lib.Web.Mvc.JQuery.JqGrid;
using VnEduPlus.CmonFunction;
using VnEduPlus.Models;
using System.Globalization;
using System.Threading;
using System.Data.SqlClient;
using System.Data;

namespace VnEduPlus.Controllers
{
    public class GiaoVienBoMonController: Controller
    {
        EDUNP_CAP1Entities db = new EDUNP_CAP1Entities();
        /// <summary>
        /// lay username dang nhap 
        /// </summary>
        /// <returns></returns>
        public string getUserName()
        {
            return User.Identity.Name.ToString() == "" ? "superadmin" : User.Identity.Name.ToString();

        }

        #region Giáo viên bộ môn nhận xét thường xuyên

        [AcceptVerbs(HttpVerbs.Get)]
        [Authorize]
        public ActionResult GiaoVienBoMon()
        {
            // lay thong tin truong 
            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            string namhoc = Cmon.GetNamHienTai(idTruong);
            if (!Request.IsAuthenticated)
            {
                return View(@"~/Views/ThongBao/Index.cshtml");

            }



            bool isHieuTruong = Cmon.isHieuTruong(idTruong, userName);
            ViewBag.isHieuTruong = isHieuTruong;
            ViewBag.idtruong = idTruong;
            ViewBag.danhMucTieuChi = Cmon.createDanhMuc();
            ViewBag.danhmucMonHoc = listMonHoc(userName);
            ViewBag.danhMucThang = Cmon.createCacThangCoDinh(idTruong, db);


            ViewBag.loaiNhapDiem = Cmon.checkLoaiNhapDiemCuaTruong(idTruong);
            ViewBag.ShowDTBM = Cmon.checkShowOrHiddenDTBMCuaTruong(idTruong);
            ViewBag.style = Cmon.checkShowOrHiddenDTBMCuaTruong(idTruong);

            string namHoc = Cmon.GetNamHienTai(idTruong);
           
            ViewBag.namhientai = namhoc;
            ViewBag.tenuser = Cmon.GetnameUser(userName);

            var listkhoi = (from khoi in db.DM_Khoi
                            let makhoiList = from lophoc in db.LopHocs
                                             join userlh in db.Users_LopHoc on lophoc.MaLop equals userlh.MaLop
                                             where userlh.UserName == userName && userlh.idTruong == idTruong && userlh.NamHoc == namHoc
                                             select lophoc.MaKhoi
                            where makhoiList.Contains(khoi.MaKhoi) && khoi.idTruong == idTruong && khoi.NamHoc == namHoc
                            select khoi).ToList();

            if (listkhoi.Count()  == 0)
            {
                listkhoi = (from khoi in db.DM_Khoi
                             where khoi.idTruong == idTruong && khoi.NamHoc == namHoc
                            select khoi).ToList();

            }
            ViewBag.makhoi = new SelectList(listkhoi, "MaKhoi", "TenKhoi");


            return View();
        }

     
        [HttpGet]
        public JsonResult LoadMonHocTheoLop(string maLop)
        {
            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            string namhoc = Cmon.GetNamHienTai(idTruong);

            var lstMHQLLop = (from umh in db.Users_LopHoc.Where(t => t.UserName == userName && t.idTruong == idTruong && t.NamHoc == namhoc && t.MaLop == maLop)
                         join mh in db.ASSIGN_LOP_MonHoc on new { umh.idTruong, umh.MaLop} equals new { mh.idTruong, mh.MaLop}
                         select new DanhMucMonHoc
                         {
                             MaMon = mh.MaMonHoc.Trim(),
                             TenMon = mh.TenMonHoc
                         }
                         ).GroupBy(x => x.MaMon).Select(z => z.OrderBy(i => i.MaMon).FirstOrDefault()).ToList();
             
            var lstMH = (from umh in db.Users_Monhoc.Where(t => t.UserName == userName && t.idTruong == idTruong && t.NamHoc == namhoc && t.MaLop == maLop)
                         join mh in db.ASSIGN_LOP_MonHoc on new { umh.idTruong, umh.MaLop, umh.MaMonHoc } equals new { mh.idTruong, mh.MaLop, mh.MaMonHoc }
                         select new DanhMucMonHoc
                         {
                             MaMon = mh.MaMonHoc.Trim(),
                             TenMon = mh.TenMonHoc
                         }
                          ).GroupBy(x => x.MaMon).Select(z => z.OrderBy(i => i.MaMon).FirstOrDefault()).ToList();
            //DanhMucMonHoc dmnh = new DanhMucMonHoc();
            //dmnh.MaMon = "-1";
            //dmnh.TenMon = "--Chọn môn học--";
            //lstMH.Insert(0, dmnh);
            var monHoc_LopHoc = lstMH;
            return Json(monHoc_LopHoc, JsonRequestBehavior.AllowGet);

        }

        private List<DanhMucMonHoc> listMonHoc(string userName)
        {
            int idTruong = Cmon.getIdtruong(userName, db);
            string namhoc = Cmon.GetNamHienTai(idTruong);
            
            var lstMH = (from umh in db.Users_Monhoc.Where(t => t.UserName == userName && t.idTruong == idTruong && t.NamHoc == namhoc)
                         join mh in db.ASSIGN_LOP_MonHoc on new { umh.idTruong, umh.MaLop, umh.MaMonHoc } equals new { mh.idTruong,mh.MaLop,mh.MaMonHoc}
                                         select new DanhMucMonHoc
                                         {
                                             MaMon = mh.MaMonHoc.Trim(),
                                             TenMon = mh.TenMonHoc
                                         }
                         ).GroupBy(x => x.MaMon).Select(z => z.OrderBy(i => i.MaMon).FirstOrDefault()).ToList();
            DanhMucMonHoc dmnh = new DanhMucMonHoc();
            dmnh.MaMon = "-1";
            dmnh.TenMon="--Chọn môn học--";
            lstMH.Insert(0, dmnh);
            return lstMH;
        }

        private bool CheckPermission(int idTruong, string namHoc)
        {
            string userName = getUserName();
            //var _userName = (from ul in db.Users_LopHoc
            //                 join um in db.Users_Monhoc on new { ul.idTruong, ul.NamHoc, ul.UserName, ul.MaLop } equals new { um.idTruong, um.NamHoc, um.UserName, um.MaLop }
            //                 where ul.UserName == userName
            //                 select ul).FirstOrDefault();

            // var lh = (from l in db.LopHocs.Where(t => t.idTruong == idTruong && t.NamHoc == namHoc)
            //          join um in db.Users_Monhoc on new { l.MaLop, l.idTruong, l.NamHoc } equals new { um.MaLop, um.idTruong, um.NamHoc }
            //          join ul in db.Users_LopHoc on new { l.MaLop, l.idTruong, l.NamHoc } equals new { ul.MaLop, ul.idTruong, ul.NamHoc }
            //          where ul.UserName == username && um.UserName == username

            var _userName = (from l in db.LopHocs.Where(t => t.idTruong == idTruong && t.NamHoc == namHoc)
                      join um in db.Users_Monhoc on new { l.MaLop, l.idTruong, l.NamHoc } equals new { um.MaLop, um.idTruong, um.NamHoc }
                      join ul in db.Users_LopHoc on new { l.MaLop, l.idTruong, l.NamHoc } equals new { ul.MaLop, ul.idTruong, ul.NamHoc }
                      where ul.UserName == userName && um.UserName == userName
                              select l).FirstOrDefault();


            if (_userName != null && !string.IsNullOrEmpty(_userName.MaLop))
                return true;
            else
                return false;
        }

        

        [HttpGet]
        public ActionResult hienDanhSachNhanXet(int idTruong, int type, int thang, string maMonHoc)
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Thongbao");

            }
            string userName = getUserName();

            var dsNhanXet = (from nhanxet in db.DM_NhanXetMau_GVBM
                             where nhanxet.idTruong == idTruong && nhanxet.LoaiNhanXet == type && nhanxet.Thang == thang && nhanxet.MaMonHoc == maMonHoc && nhanxet.UserCreated == userName
                             orderby nhanxet.TenNhanXet
                             select nhanxet).ToList();

            return View(@"~/Views/GiaoVienBoMon/HienThiDanhSachNhanXet.cshtml", dsNhanXet);
        }

        [HttpPost]
        public ActionResult ThemDanhSachThuVienMauCau(string maKhoi, string thangNX, string loaiNX, string maMonHoc, int[] danhSachMau)
        {
            //1. Kieem tra xem noi dung dua vao da co hay chua
            //2. Neu chua co thi thuc hien viec chen moi, neu co roi thi thuc hien cap nhat


            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            string namHoc = Cmon.GetNamHienTai(idTruong);
            string tenMonHoc = Cmon.GetnameMonhoc(maMonHoc);
            string mess = "";
            try
            {
                for (int i = 0; i < danhSachMau.Length; i++)
                {
                    int id = danhSachMau[i];
                    var thuvienMau = (from thuvien in db.ThuVien_NhanXetMauGVBM
                                      where thuvien.ID == id
                                      select thuvien.Noidung).FirstOrDefault();
                    // lay ra noi dung chen vao bang
                    DM_NhanXetMau_GVBM DMNXGVBM = new DM_NhanXetMau_GVBM();
                    DMNXGVBM.idTruong = idTruong;
                    DMNXGVBM.LoaiNhanXet = (short)Convert.ToInt16(loaiNX);
                    DMNXGVBM.Thang = (short)Convert.ToInt16(thangNX);
                    DMNXGVBM.MaNhanXet = DateTime.Now.Ticks.ToString();
                    DMNXGVBM.MaMonHoc = maMonHoc;                    
                    DMNXGVBM.TenNhanXet = thuvienMau;
                    DMNXGVBM.UserCreated = userName;
                    DMNXGVBM.isDefault = false;
                    db.AddToDM_NhanXetMau_GVBM(DMNXGVBM);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                mess = "ERROR";

            }

            return Json(mess, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult danhSachThuVienMauCau(string maKhoi, string monHoc, string loaiNX, int thang)
        {
            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            string namhoc = Cmon.GetNamHienTai(idTruong);

            ViewBag.idtruong = idTruong;
            ViewBag.tenTruong = Cmon.GetnameSchool(idTruong);
            ViewBag.namHoc = namhoc;
            ViewBag.maKhoi = maKhoi;
            ViewBag.thang = thang;
            ViewBag.loaiNX = loaiNX;
            ViewBag.maMonHoc = monHoc;
            var comments = (from maunx in db.ThuVien_NhanXetMauGVBM
                            where maunx.LoaiNX == loaiNX && maunx.Thang == thang && maunx.MaKhoi == maKhoi && maunx.MaMonHoc == monHoc
                            orderby maunx.ID descending
                            select maunx).ToList();
            return View(@"~/Views/GiaoVienBoMon/DanhSachMauNhanXet.cshtml", comments);
        }
        

        [AcceptVerbs(HttpVerbs.Get)]
        [Authorize]
        public ActionResult HienThiDanhSach(int idTruong, string namHoc, int thang, int idLoaiNhanXet, string mamonhoc)
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Thongbao");

            }
            string username = getUserName();
            var danhsach = (from ds in db.DM_NhanXetMau_GVBM
                              where ds.idTruong == idTruong && ds.Thang == thang && ds.UserCreated == username && ds.LoaiNhanXet==idLoaiNhanXet && ds.MaMonHoc==mamonhoc
                              orderby ds.MaNhanXet 
                              select ds).OrderByDescending(x => x.MaNhanXet).ToList();
            string viewname = "HienThiDanhSachNhanXet"; 
            return PartialView(viewname, danhsach);
        }

        public void UpdateStatusBM(int loaiNhanXet,string manx, int thang, string maMon, string userName)
        {
            // Cập nhật lại toàn bộ trạng thái isDefault = false ngoại trừ bảng ghi này được set là true

            var dsNhanXet = (from kthuc in db.DM_NhanXetMau_GVBM  select kthuc).ToList();
            
            //dsNhanXet.ForEach(a => { a.isDefault = false; });
            // db.SaveChanges();

        }

        [HttpGet]
        public JsonResult ThemMoiTieuChi(string tentieuchi, bool isdefault, string thang, int idtruong, short loainhanxet, string mamomhoc)
        {
              string userName = getUserName();
              
            // luoon them moi nhan xet ma ko can quan tam co trung hay khong
                string mess = "";
                
                DM_NhanXetMau_GVBM kt = new DM_NhanXetMau_GVBM();
                kt.Thang = Convert.ToInt16(thang);
                kt.LoaiNhanXet = loainhanxet;
                kt.MaMonHoc = mamomhoc;
                kt.idTruong = idtruong;
                kt.MaNhanXet = DateTime.Now.Ticks.ToString();
                kt.TenNhanXet = tentieuchi;
                kt.UserCreated = getUserName();              
                db.DM_NhanXetMau_GVBM.AddObject(kt);
                int rs = db.SaveChanges();
                if (rs > 0)
                    mess = "OK";
                else
                    mess = "Thêm mới tiêu chí không thành công!";
                     
            return Json(mess, JsonRequestBehavior.AllowGet);
            
        }

        
        public bool CheckExistsTieuChi(int type, string maNhanXet, string thang, int idTruong)
        {
            bool isExit = false;
            short th = Convert.ToInt16(thang);
            DM_NhanXetMau_GVBM kt = (from kthuc in db.DM_NhanXetMau_GVBM.Where(t => t.LoaiNhanXet == type && t.MaNhanXet == maNhanXet && t.Thang == th && t.idTruong == idTruong) select kthuc).FirstOrDefault();
            if (kt != null)
            {
                isExit = true;
            }
            return isExit;

        }


        [HttpGet]
        public JsonResult CapNhatTieuChi(string matieuchi, string tentieuchi, bool isdefault, string thang, int idtruong, short loainhanxet, string mamomhoc)
        {
            string mess = "";
            //if (string.IsNullOrEmpty(matieuchi.Trim()))
            //    mess = "Yêu cầu nhập mã tiêu chí";
            //else if (string.IsNullOrEmpty(thang.Trim()) || thang == "0")
            //    mess = "Yêu cầu chọn tháng để nhập tiêu chí";
            //else if (string.IsNullOrEmpty(tentieuchi.Trim()))
            //    mess = "Yêu cầu nhập tên tiêu chí";
            //else
            //{
                string username=getUserName();
           
                short th = Convert.ToInt16(thang);
                DM_NhanXetMau_GVBM kt = (from kthuc in db.DM_NhanXetMau_GVBM.Where(t => t.MaNhanXet == matieuchi && t.Thang == th && t.idTruong == idtruong && t.UserCreated == username && t.LoaiNhanXet==loainhanxet && t.MaMonHoc==mamomhoc) select kthuc).FirstOrDefault();
                if (kt != null)
                {
                    kt.TenNhanXet = tentieuchi;
                    kt.UserCreated = username;
                    kt.isDefault = isdefault;
                    int rs = db.SaveChanges();
                    if (rs > 0)
                        mess = "OK";
                    else
                        mess = "Cập nhật tiêu chí không thành công!";
                }
                else
                {
                    mess = "Không có thông tin cập nhật!";
                }
            //}
            return Json(mess, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult DeleteTieuChi(string matieuchi, string thang, string mamonhoc, int idtruong, short loaitieuchi)
        {
            string mess = "";
            short th = Convert.ToInt16(thang);
            string username = getUserName();
            DM_NhanXetMau_GVBM kt = (from kthuc in db.DM_NhanXetMau_GVBM.Where(t => t.MaNhanXet == matieuchi && t.Thang == th && t.idTruong == idtruong && t.UserCreated == username && t.MaMonHoc==mamonhoc && t.LoaiNhanXet==loaitieuchi) select kthuc).FirstOrDefault();
            if (kt != null)
            {

                db.DM_NhanXetMau_GVBM.DeleteObject(kt);
                int del = db.SaveChanges();
                if (del > 0)
                    mess = "OK";
                else
                    mess = "Xóa tiêu chí không thành công!";
            }
            else
            {
                mess = "Không có thông tin để xóa!";
            }
            return Json(mess, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Edit(string makt, string thang, int idtruong, string mamonhoc, string loainhanxet)
        {
            short lnx = Convert.ToInt16(loainhanxet);
            short th = Convert.ToInt16(thang);
            var kthuc = (from kt in db.DM_NhanXetMau_GVBM.Where(t => t.MaNhanXet == makt && t.Thang == th && t.idTruong == idtruong && t.MaMonHoc == mamonhoc && t.LoaiNhanXet == lnx) select kt).FirstOrDefault();
            string partialview = "";
            switch (lnx)
            {
                case 1:
                    partialview = "EditKienThuc.cshtml";
                    break;
                case 2:
                    partialview = "EditNangLuc.cshtml";
                    break;
                default:
                    partialview = "EditPhamChat.cshtml";
                    break;
            }
            return PartialView(@"~/Views/GiaoVienBoMon/" + partialview, kthuc);
        }
        public ActionResult Add(short loainhanxet)
        {
             
            string partialview = "AddNhanXet.cshtml";
            return PartialView(@"~/Views/GiaoVienBoMon/" + partialview);
        }
        #endregion

        #region Giáo viên bộ môn nhận xét theo nhóm học sinh

        public ActionResult NhanXetLopHoc()
        {
            // lay thong tin truong 
            string userName = getUserName();

            int idTruong = Cmon.getIdtruong(userName, db);
            string namhoc = Cmon.GetNamHienTai(idTruong);
            if (!Request.IsAuthenticated)
            {
                return View(@"~/Views/ThongBao/Index.cshtml");

            }

            bool isHieuTruong = Cmon.isHieuTruong(idTruong, userName);
            ViewBag.isHieuTruong = isHieuTruong;
            ViewBag.idtruong = idTruong;
            ViewBag.danhMucTieuChi = Cmon.createDanhMuc();
            //ViewBag.danhmucMonHoc = listMonHoc(userName);
            ViewBag.danhMucThang = Cmon.createCacThangCoDinh(idTruong, db);
            ViewBag.danhmucLop = GetListLopHoc(userName);
            ViewBag.danhmucHocKy = Cmon.CreateHocKy();

            ViewBag.loaiNhapDiem = Cmon.checkLoaiNhapDiemCuaTruong(idTruong);
            ViewBag.ShowDTBM = Cmon.checkShowOrHiddenDTBMCuaTruong(idTruong);
            ViewBag.style = Cmon.checkShowOrHiddenDTBMCuaTruong(idTruong);

           
            ViewBag.namhientai = namhoc;

            ViewBag.tenuser = Cmon.GetnameUser(userName);

            return View(@"~/Views/GiaoVienBoMon/HienThiDanhSachHocSinh.cshtml");
        }


       

        private List<LopHocGiaoVien> GetListLopHoc(string username)
        {
            int idTruong = Cmon.getIdtruong(username, db);
            string namhoc = Cmon.GetNamHienTai(idTruong);
            List<LopHocGiaoVien> lstLopHoc = new List<LopHocGiaoVien>();

            var lopHoc = (from lh in db.LopHocs
                          let allowlops =
                          (from usermon in db.Users_Monhoc where usermon.UserName == username && usermon.NamHoc == namhoc && usermon.idTruong == idTruong select usermon.MaLop).
                          Union(
                          from userLop in db.Users_LopHoc.Where(userLop => userLop.idTruong == idTruong && userLop.NamHoc == namhoc && userLop.UserName == username)
                          select userLop.MaLop)
                          where allowlops.Contains(lh.MaLop) && lh.idTruong == idTruong && lh.NamHoc == namhoc
                          select new LopHocGiaoVien
                          {
                              MaLop = lh.MaLop,
                              TenLop = lh.TenLop,
                              IdTruong = lh.idTruong,
                              NamHoc = lh.NamHoc
                          }).Distinct().ToList();


            LopHocGiaoVien lhgv=new LopHocGiaoVien();
            lhgv.MaLop="";
            lhgv.TenLop="--Chọn lớp học--";
            lopHoc.Insert(0, lhgv);
            return lopHoc;
        }

        [HttpGet]
        public ActionResult soBoMon(string malop,int thang, string mamon)
        {
            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            string namhoc = Cmon.GetNamHienTai(idTruong);

            ViewBag.idtruong = idTruong;
            ViewBag.tenTruong = Cmon.GetnameSchool(idTruong);
            ViewBag.namHoc = namhoc;
            ViewBag.tenuser = Cmon.GetnameUser(userName);
            ViewBag.danhMucTieuChi = Cmon.createDanhMuc();
            
            ViewBag.danhMucThang = Cmon.createCacThangCoDinh(idTruong, db);
            ViewBag.danhmucLop = GetListLopHoc(userName);
            ViewBag.danhmucHocKy = Cmon.CreateHocKy();

            ViewBag.loaiNhapDiem = Cmon.checkLoaiNhapDiemCuaTruong(idTruong);
            ViewBag.ShowDTBM = Cmon.checkShowOrHiddenDTBMCuaTruong(idTruong);
            ViewBag.style = Cmon.checkShowOrHiddenDTBMCuaTruong(idTruong);

            // Biết mã lớp thì sẽ đi xác định các môn học của lớp đó
            var lstMHLop = (from umh in db.Users_LopHoc.Where(t => t.UserName == userName && t.idTruong == idTruong && t.NamHoc == namhoc && t.MaLop == malop)
                         join mh in db.ASSIGN_LOP_MonHoc on new { umh.idTruong, umh.MaLop} equals new { mh.idTruong, mh.MaLop}
                         select new DanhMucMonHoc
                         {
                             MaMon = mh.MaMonHoc.Trim(),
                             TenMon = mh.TenMonHoc
                         }
                         ).GroupBy(x => x.MaMon).Select(z => z.OrderBy(i => i.MaMon).FirstOrDefault()).ToList();

            
            var lstMH = (from umh in db.Users_Monhoc.Where(t => t.UserName == userName && t.idTruong == idTruong && t.NamHoc == namhoc && t.MaLop == malop)
                         join mh in db.ASSIGN_LOP_MonHoc on new { umh.idTruong, umh.MaLop, umh.MaMonHoc } equals new { mh.idTruong, mh.MaLop, mh.MaMonHoc }
                         select new DanhMucMonHoc
                         {
                             MaMon = mh.MaMonHoc.Trim(),
                             TenMon = mh.TenMonHoc
                         }
                         ).GroupBy(x => x.MaMon).Select(z => z.OrderBy(i => i.MaMon).FirstOrDefault()).ToList();

            var monHoc_LopHoc = (lstMHLop != null && lstMHLop.Count >0) ? lstMHLop : lstMH;
            ViewBag.danhsachMH = monHoc_LopHoc;
            
            ViewBag.namhientai = namhoc;

           // ViewBag.tenuser = Cmon.GetnameUser(userName);
            
            int[] thangs = { 1, 2, 3, 4, 5, 8, 9, 10, 11, 12 };
            if (!thangs.Contains(thang))
            {
                thang = 8;
            }

            ViewBag.thang = thang;
            mamon = lstMH.Count >0 ? lstMH[0].MaMon : "";
            ViewBag.maMon = mamon;

            var tenGiaoVienLists = (
                                     from usrs in db.ScrUsers
                                     join um in db.Users_Monhoc on usrs.UserName equals um.UserName
                                     join RU in db.ScrJRoleUsers on um.UserName equals RU.UserName
                                     join RF in db.ScrJRoleFunctions on RU.RoleName equals RF.RoleName
                                     where um.idTruong == idTruong && um.NamHoc == namhoc && um.MaLop == malop && um.MaMonHoc == mamon
                                     && RF.Enable == true
                                     && RF.Read_ == true
                                     && RF.Insert_ == true
                                     && RF.Update_ == true
                                     && RF.FunctionID == 119
                                     select new user
                                     {
                                         fullName = usrs.FullName
                                     }).Distinct().ToList();

            string giaoVienBoMon = "";
            List<VnEduPlus.Models.user> users = (List<VnEduPlus.Models.user>)tenGiaoVienLists;
            if (users != null && users.Count > 0)
            {
                for (int i = 0; i < users.Count; i++)
                {
                    giaoVienBoMon += users[i].fullName + ";";
                }

            }

            giaoVienBoMon = giaoVienBoMon.Trim().Length > 0 ? giaoVienBoMon.Substring(0, giaoVienBoMon.LastIndexOf(";")) : "";
            ViewBag.tenuser = giaoVienBoMon;

            var hsnx=(from hs in db.HocSinhs
                      where hs.idTruong == idTruong && hs.MaLop == malop && hs.NamHoc == namhoc && !(from hsqt in db.HocSinh_QuaTrinh.Where(hsqt => hsqt.idTruong == idTruong && hsqt.NamHoc == namhoc && hsqt.MaLop == malop && hsqt.MaQuaTrinh == "BH")
                              select hsqt.MaHocSinh).Contains(hs.MaHocSinh)                      
                      join hst in db.HocSinh_DanhGiaMonHoc.Where(k=>k.MaMonHoc==mamon) on new{hs.MaHocSinh, hs.idTruong, hs.MaLop, hs.NamHoc} equals new {hst.MaHocSinh, hst.idTruong, hst.MaLop, hst.NamHoc} into hocsinh
                          from hsj in hocsinh.DefaultIfEmpty()
                           orderby hs.STT ascending
                          //where(hsj.idTruong==idTruong && hsj.MaLop==malop && hs.NamHoc==namhoc)
                        select new HocSinhNhanXetThang
                        {
                            STT = hs.STT,
                            IdTruong = hs.idTruong,
                            NamHoc = hs.NamHoc,
                            MaLop = hsj.MaLop,
                            ngaySinh = hs.NgaySinh,
                            MaHocSinh = hs.MaHocSinh,
                            MaMonHoc = hsj.MaMonHoc,
                            HoTen = hs.HoTen,
                            NXKT = (thang == 8 ? hsj.NXT1_KT : thang == 9 ? hsj.NXT2_KT : thang == 10 ? hsj.NXT3_KT : thang == 11 ? hsj.NXT4_KT : thang == 12 ? hsj.NXT5_KT : thang == 1 ? hsj.NXT6_KT : thang == 2 ? hsj.NXT7_KT : thang == 3 ? hsj.NXT8_KT : thang == 4 ? hsj.NXT9_KT : hsj.NXT10_KT),
                            NXPC = (thang == 8 ? hsj.NXT1_PC : thang == 9 ? hsj.NXT2_PC : thang == 10 ? hsj.NXT3_PC : thang == 11 ? hsj.NXT4_PC : thang == 12 ? hsj.NXT5_PC : thang == 1 ? hsj.NXT6_PC : thang == 2 ? hsj.NXT7_PC : thang == 3 ? hsj.NXT8_PC : thang == 4 ? hsj.NXT9_PC : hsj.NXT10_PC),
                            NXNL = (thang == 8 ? hsj.NXT1_NL : thang == 9 ? hsj.NXT2_NL : thang == 10 ? hsj.NXT3_NL : thang == 11 ? hsj.NXT4_NL : thang == 12 ? hsj.NXT5_NL : thang == 1 ? hsj.NXT6_NL : thang == 2 ? hsj.NXT7_NL : thang == 3 ? hsj.NXT8_NL : thang == 4 ? hsj.NXT9_NL : hsj.NXT10_NL),
                        }
                          ).ToList();
            ViewBag.maKhoi = Cmon.getKhoi(malop,namhoc,idTruong);
           // ViewBag.monHoc = Cmon.GetnameMonhoc(mamon);


            
            var QuyenNhapDiem = (from RU in db.ScrJRoleUsers
                                 join RF in db.ScrJRoleFunctions
                                     on RU.RoleName equals RF.RoleName
                                 where RU.UserName == userName && RF.idTruong == idTruong
                                 && RF.FunctionID == 62 && RF.Insert_ == true && RF.Enable == true && RF.Update_ == true
                                 select RF
                       ).FirstOrDefault();
            ViewBag.phanQuyen = QuyenNhapDiem != null ? 1 : 0;
            soBoMon soBM = new soBoMon();
            soBM.hocSinhNhanXetThang = hsnx;
            ViewBag.PhongGiaoDuc = Cmon.getDonViChuQuan(idTruong);
            return View(@"~/Views/GiaoVienBoMon/SoBoMon.cshtml", soBM);
        }


        [HttpGet]
        public ActionResult GetHocSinh(string malop, int thang, string mamon)
        {
            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            string namhoc = Cmon.GetNamHienTai(idTruong);

            var hsnx = (from hs in db.HocSinhs.Where(t => t.idTruong == idTruong && t.MaLop == malop && t.NamHoc == namhoc)
                        where !(from hsqt in db.HocSinh_QuaTrinh.Where(hsqt => hsqt.idTruong == idTruong && hsqt.NamHoc == namhoc && hsqt.MaLop == malop && hsqt.MaQuaTrinh == "BH")
                                select hsqt.MaHocSinh).Contains(hs.MaHocSinh)
                        join hst in db.HocSinh_DanhGiaMonHoc.Where(k => k.MaMonHoc == mamon) on new { hs.MaHocSinh, hs.idTruong, hs.MaLop, hs.NamHoc } equals new { hst.MaHocSinh, hst.idTruong, hst.MaLop, hst.NamHoc } into hocsinh
                        from hsj in hocsinh.DefaultIfEmpty()
                        orderby hs.STT ascending
                        //where(hsj.idTruong==idTruong && hsj.MaLop==malop && hs.NamHoc==namhoc)
                        select new HocSinhNhanXetThang
                        {
                            STT= hs.STT,
                            IdTruong = hs.idTruong,
                            NamHoc = hs.NamHoc,
                            MaLop = hsj.MaLop,
                            ngaySinh = hs.NgaySinh,
                            MaHocSinh = hs.MaHocSinh,
                            MaMonHoc = hsj.MaMonHoc,
                            HoTen = hs.HoTen,
                            NXKT = (thang == 8 ? hsj.NXT1_KT : thang == 9 ? hsj.NXT2_KT : thang == 10 ? hsj.NXT3_KT : thang == 11 ? hsj.NXT4_KT : thang == 12 ? hsj.NXT5_KT : thang == 1 ? hsj.NXT6_KT : thang == 2 ? hsj.NXT7_KT : thang == 3 ? hsj.NXT8_KT : thang == 4 ? hsj.NXT9_KT : hsj.NXT10_KT),
                            NXPC = (thang == 8 ? hsj.NXT1_PC : thang == 9 ? hsj.NXT2_PC : thang == 10 ? hsj.NXT3_PC : thang == 11 ? hsj.NXT4_PC : thang == 12 ? hsj.NXT5_PC : thang == 1 ? hsj.NXT6_PC : thang == 2 ? hsj.NXT7_PC : thang == 3 ? hsj.NXT8_PC : thang == 4 ? hsj.NXT9_PC : hsj.NXT10_PC),
                            NXNL = (thang == 8 ? hsj.NXT1_NL : thang == 9 ? hsj.NXT2_NL : thang == 10 ? hsj.NXT3_NL : thang == 11 ? hsj.NXT4_NL : thang == 12 ? hsj.NXT5_NL : thang == 1 ? hsj.NXT6_NL : thang == 2 ? hsj.NXT7_NL : thang == 3 ? hsj.NXT8_NL : thang == 4 ? hsj.NXT9_NL : hsj.NXT10_NL),
                        } 
                          ).ToList();
            ViewBag.maKhoi = Cmon.getKhoi(malop, namhoc, idTruong);
            // ViewBag.monHoc = Cmon.GetnameMonhoc(mamon);

            var QuyenNhapDiem = (from RU in db.ScrJRoleUsers
                                 join RF in db.ScrJRoleFunctions
                                     on RU.RoleName equals RF.RoleName
                                 where RU.UserName == userName && RF.idTruong == idTruong
                                 && RF.FunctionID == 62 && RF.Insert_ == true && RF.Enable == true && RF.Update_ == true
                                 select RF
                       ).FirstOrDefault();
            ViewBag.phanQuyen = QuyenNhapDiem != null ? 1 : 0;


            return PartialView(@"~/Views/GiaoVienBoMon/ViewHocSinh.cshtml", hsnx);
        }


        [HttpGet]
        public ActionResult ThongTinSoBoMon(string malop, int thang, string mamon)
        {
            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            string namhoc = Cmon.GetNamHienTai(idTruong);

            var tenGiaoVienLists = (
                                     from usrs in db.ScrUsers
                                     join um in db.Users_Monhoc on usrs.UserName equals um.UserName
                                     join RU in db.ScrJRoleUsers on um.UserName equals RU.UserName
                                     join RF in db.ScrJRoleFunctions on RU.RoleName equals RF.RoleName
                                     where um.idTruong == idTruong && um.NamHoc == namhoc && um.MaLop == malop && um.MaMonHoc == mamon
                                     && RF.Enable == true
                                     && RF.Read_ == true
                                     && RF.Insert_ == true
                                     && RF.Update_ == true
                                     && RF.FunctionID == 119
                                     select new user
                                     {
                                         fullName = usrs.FullName
                                     }).Distinct().ToList();
            
            string giaoVienBoMon = "";
            List<VnEduPlus.Models.user> users  = (List<VnEduPlus.Models.user>) tenGiaoVienLists; 
            if (users!=null && users.Count >0) {
                for(int i =0 ; i<users.Count; i++ )
                {
                    giaoVienBoMon += users[i].fullName + ";";
                }
                
            }

            giaoVienBoMon = giaoVienBoMon.Trim().Length >0 ? giaoVienBoMon.Substring(0, giaoVienBoMon.LastIndexOf(";")) : "";
            var hsnx = (from hs in db.HocSinhs.Where(t => t.idTruong == idTruong && t.MaLop == malop && t.NamHoc == namhoc)
                        where !(from hsqt in db.HocSinh_QuaTrinh.Where(hsqt => hsqt.idTruong == idTruong && hsqt.NamHoc == namhoc && hsqt.MaLop == malop && hsqt.MaQuaTrinh == "BH")
                                select hsqt.MaHocSinh).Contains(hs.MaHocSinh)
                        join hst in db.HocSinh_DanhGiaMonHoc.Where(hsdg => hsdg.MaMonHoc == mamon 
                            && hsdg.idTruong == idTruong
                            && hsdg.MaLop == malop
                            && hsdg.NamHoc == namhoc

                            )  on new { hs.MaHocSinh, hs.idTruong, hs.MaLop, hs.NamHoc } equals new { hst.MaHocSinh, hst.idTruong, hst.MaLop, hst.NamHoc } into hocsinh
                        from hsj in hocsinh.DefaultIfEmpty()
                        orderby hs.STT ascending
                        //where(hsj.MaMonHoc == mamon && hsj.idTruong == idTruong && hsj.NamHoc == namhoc && hsj.MaLop == malop)
                        select new HocSinhNhanXetThang
                        {
                            IdTruong = hs.idTruong,
                            NamHoc = hs.NamHoc,
                            MaLop = hsj.MaLop,
                            ngaySinh = hs.NgaySinh,
                            MaHocSinh = hs.MaHocSinh,
                            MaMonHoc = hsj.MaMonHoc,
                            HoTen = hs.HoTen,
                            NXKT = (thang == 8 ? hsj.NXT1_KT : thang == 9 ? hsj.NXT2_KT : thang == 10 ? hsj.NXT3_KT : thang == 11 ? hsj.NXT4_KT : thang == 12 ? hsj.NXT5_KT : thang == 1 ? hsj.NXT6_KT : thang == 2 ? hsj.NXT7_KT : thang == 3 ? hsj.NXT8_KT : thang == 4 ? hsj.NXT9_KT : hsj.NXT10_KT),
                            NXPC = (thang == 8 ? hsj.NXT1_PC : thang == 9 ? hsj.NXT2_PC : thang == 10 ? hsj.NXT3_PC : thang == 11 ? hsj.NXT4_PC : thang == 12 ? hsj.NXT5_PC : thang == 1 ? hsj.NXT6_PC : thang == 2 ? hsj.NXT7_PC : thang == 3 ? hsj.NXT8_PC : thang == 4 ? hsj.NXT9_PC : hsj.NXT10_PC),
                            NXNL = (thang == 8 ? hsj.NXT1_NL : thang == 9 ? hsj.NXT2_NL : thang == 10 ? hsj.NXT3_NL : thang == 11 ? hsj.NXT4_NL : thang == 12 ? hsj.NXT5_NL : thang == 1 ? hsj.NXT6_NL : thang == 2 ? hsj.NXT7_NL : thang == 3 ? hsj.NXT8_NL : thang == 4 ? hsj.NXT9_NL : hsj.NXT10_NL),
                        }
                          ).ToList();
            ViewBag.maKhoi = Cmon.getKhoi(malop, namhoc, idTruong);
            
            var QuyenNhapDiem = (from RU in db.ScrJRoleUsers
                                 join RF in db.ScrJRoleFunctions
                                     on RU.RoleName equals RF.RoleName
                                 where RU.UserName == userName && RF.idTruong == idTruong
                                 && RF.FunctionID == 62 && RF.Insert_ == true && RF.Enable == true && RF.Update_ == true
                                 select RF
                       ).FirstOrDefault();
            ViewBag.phanQuyen = QuyenNhapDiem != null ? 1 : 0;
            GiaoVienBoMonNhanXetThang GVBM_NhanXetMonHoc = new GiaoVienBoMonNhanXetThang();
            GVBM_NhanXetMonHoc.hocSinhNhanXetThang = hsnx;
            GVBM_NhanXetMonHoc.giaoVienBoMon = giaoVienBoMon;
            
            return View(@"~/Views/GiaoVienBoMon/ThongTinSoBoMon.cshtml", GVBM_NhanXetMonHoc);
        }



        public ActionResult ShowMauCau(short thang, string mamonhoc)
        {
            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            string namhoc = Cmon.GetNamHienTai(idTruong);
            
            var kth = (from kt in db.DM_NhanXetMau_GVBM.Where(t=>t.UserCreated==userName && t.Thang==thang && t.MaMonHoc==mamonhoc && t.idTruong==idTruong && t.LoaiNhanXet==1)
                                select new MauNhanXet
                                {
                                    manhanxet=kt.MaNhanXet,
                                    tennhanxet=kt.TenNhanXet
                                }
                                    ).ToList();

            
            MauNhanXet mkt = new MauNhanXet();
            mkt.manhanxet = "";
            mkt.tennhanxet = "Chọn kiến thức";
            kth.Insert(0, mkt);
            ViewBag.kienthuc = kth;
            
            var nlc = (from nl in db.DM_NhanXetMau_GVBM.Where(t => t.UserCreated == userName && t.Thang == thang && t.MaMonHoc == mamonhoc && t.idTruong == idTruong && t.LoaiNhanXet == 2)
                               select new MauNhanXet
                               {
                                   manhanxet = nl.MaNhanXet,
                                   tennhanxet = nl.TenNhanXet
                               }
                                    ).ToList();


            MauNhanXet mnl = new MauNhanXet();
            mnl.manhanxet = "";
            mnl.tennhanxet = "Chọn năng lực";
            nlc.Insert(0, mnl);
            ViewBag.nangluc = nlc;

            var pch = (from pc in db.DM_NhanXetMau_GVBM.Where(t => t.UserCreated == userName && t.Thang == thang && t.MaMonHoc == mamonhoc && t.idTruong == idTruong && t.LoaiNhanXet == 3)
                                select new MauNhanXet
                                {
                                    manhanxet = pc.MaNhanXet,
                                    tennhanxet = pc.TenNhanXet
                                }
                                    ).ToList();

           
            
            MauNhanXet mpc = new MauNhanXet();
            mpc.manhanxet = "";
            mpc.tennhanxet = "Chọn phẩm chất";
            pch.Insert(0, mpc);
            ViewBag.phamchat = pch;

            return PartialView(@"~/Views/GiaoVienBoMon/FormNhanXet.cshtml");
        }

        [HttpGet]
        public JsonResult CapNhatNoiDungNhanXet(string mahocsinh, string mamonhoc, string thang, string malop, string kienthuc, string nangluc, string phamchat, string hoten)
        {
            string mess = "";
            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            string namhoc = Cmon.GetNamHienTai(idTruong);
            var hsTonTai = (from hs in db.HocSinh_DanhGiaMonHoc.Where(t => t.idTruong == idTruong && t.NamHoc == namhoc && t.MaHocSinh == mahocsinh && t.MaMonHoc == mamonhoc && t.MaLop == malop)
                            select hs
                              ).FirstOrDefault();
            if (hsTonTai!=null)
            {
                //Update
                switch (thang)
                {
                    case "8":
                        {
                            hsTonTai.NXT1_KT = kienthuc;
                            hsTonTai.NXT1_NL = nangluc;
                            hsTonTai.NXT1_PC = phamchat;
                        }
                        break;
                    case "9":
                        {
                            hsTonTai.NXT2_KT = kienthuc;
                            hsTonTai.NXT2_NL = nangluc;
                            hsTonTai.NXT2_PC = phamchat;
                        }
                        break;
                    case "10":
                        {
                            hsTonTai.NXT3_KT = kienthuc;
                            hsTonTai.NXT3_NL = nangluc;
                            hsTonTai.NXT3_PC = phamchat;
                        }
                        break;
                    case "11":
                        {
                            hsTonTai.NXT4_KT = kienthuc;
                            hsTonTai.NXT4_NL = nangluc;
                            hsTonTai.NXT4_PC = phamchat;
                        }
                        break;
                    case "12":
                        {
                            hsTonTai.NXT5_KT = kienthuc;
                            hsTonTai.NXT5_NL = nangluc;
                            hsTonTai.NXT5_PC = phamchat;
                        }
                        break;
                    case "1":
                        {
                            hsTonTai.NXT6_KT = kienthuc;
                            hsTonTai.NXT6_NL = nangluc;
                            hsTonTai.NXT6_PC = phamchat;
                        }
                        break;
                    case "2":
                        {
                            hsTonTai.NXT7_KT = kienthuc;
                            hsTonTai.NXT7_NL = nangluc;
                            hsTonTai.NXT7_PC = phamchat;
                        }
                        break;
                    case "3":
                        {
                            hsTonTai.NXT8_KT = kienthuc;
                            hsTonTai.NXT8_NL = nangluc;
                            hsTonTai.NXT8_PC = phamchat;
                        }
                        break;
                    case "4":
                        {
                            hsTonTai.NXT9_KT = kienthuc;
                            hsTonTai.NXT9_NL = nangluc;
                            hsTonTai.NXT9_PC = phamchat;
                        }
                        break;
                    case "5":
                        {
                            hsTonTai.NXT10_KT = kienthuc;
                            hsTonTai.NXT10_NL = nangluc;
                            hsTonTai.NXT10_PC = phamchat;
                        }
                        break;

                }
                int rs = db.SaveChanges();
                if (rs > 0)
                    mess = "1";
                else
                    mess = "0";
            }
            else
            {
                //Insert
                HocSinh_DanhGiaMonHoc hsdg = new HocSinh_DanhGiaMonHoc();
                hsdg.DateCreated = DateTime.Now;
                hsdg.DateUpdated = DateTime.Now;
                hsdg.HoTen = hoten;
                hsdg.idTruong = idTruong;
                hsdg.MaHocSinh = mahocsinh;
                hsdg.MaHocSinhFriendly = mahocsinh;
                hsdg.MaLop = malop;
                hsdg.MaMonHoc = mamonhoc;
                hsdg.NamHoc = namhoc;
                hsdg.UserCreated = userName;
                hsdg.UserUpdated = userName;
                
                switch (thang)
                {
                    case "8":
                        {
                            hsdg.NXT1_KT = kienthuc;
                            hsdg.NXT1_NL = nangluc;
                            hsdg.NXT1_PC = phamchat;
                        }
                        break;
                    case "9":
                        {
                            hsdg.NXT2_KT = kienthuc;
                            hsdg.NXT2_NL = nangluc;
                            hsdg.NXT2_PC = phamchat;
                        }
                        break;
                    case "10":
                        {
                            hsdg.NXT3_KT = kienthuc;
                            hsdg.NXT3_NL = nangluc;
                            hsdg.NXT3_PC = phamchat;
                        }
                        break;
                    case "11":
                        {
                            hsdg.NXT4_KT = kienthuc;
                            hsdg.NXT4_NL = nangluc;
                            hsdg.NXT4_PC = phamchat;
                        }
                        break;
                    case "12":
                        {
                            hsdg.NXT5_KT = kienthuc;
                            hsdg.NXT5_NL = nangluc;
                            hsdg.NXT5_PC = phamchat;
                        }
                        break;
                    case "1":
                        {
                            hsdg.NXT6_KT = kienthuc;
                            hsdg.NXT6_NL = nangluc;
                            hsdg.NXT6_PC = phamchat;
                        }
                        break;
                    case "2":
                        {
                            hsdg.NXT7_KT = kienthuc;
                            hsdg.NXT7_NL = nangluc;
                            hsdg.NXT7_PC = phamchat;
                        }
                        break;
                    case "3":
                        {
                            hsdg.NXT8_KT = kienthuc;
                            hsdg.NXT8_NL = nangluc;
                            hsdg.NXT8_PC = phamchat;
                        }
                        break;
                    case "4":
                        {
                            hsdg.NXT9_KT = kienthuc;
                            hsdg.NXT9_NL = nangluc;
                            hsdg.NXT9_PC = phamchat;
                        }
                        break;
                    case "5":
                        {
                            hsdg.NXT10_KT = kienthuc;
                            hsdg.NXT10_NL = nangluc;
                            hsdg.NXT10_PC = phamchat;
                        }
                        break;

                }
                db.HocSinh_DanhGiaMonHoc.AddObject(hsdg);
                int rs = db.SaveChanges();
                if (rs > 0)
                    mess = "1";
                else
                    mess = "0";
            }
            return Json(mess, JsonRequestBehavior.AllowGet);
        }

        #region Form nhập điểm các môn học 
        //public ActionResult NhapDiemHocSinh()
        //{

        //    // lay thong tin truong 
        //    string userName = getUserName();

        //    int idTruong = Cmon.getIdtruong(userName, db);
        //    string namhoc = Cmon.GetNamHienTai(idTruong);
        //    ViewBag.idtruong = idTruong;
           
        //    ViewBag.danhmucMonHoc = listMonHoc(userName);
        //    ViewBag.danhMucThang = Cmon.createCacThangCoDinh(idTruong, db);
        //    ViewBag.danhmucLop = GetListLopHoc(userName);
        //    ViewBag.hocKy = Cmon.CreateHocKy();          
            
        //    return View();
        //}

        public ActionResult NhapDiemHocSinh()
        {
            string userName = getUserName();

            int idTruong = Cmon.getIdtruong(userName, db);
            string namhoc = Cmon.GetNamHienTai(idTruong);
            bool isHieuTruong = Cmon.isHieuTruong(idTruong, userName);
            ViewBag.isHieuTruong = isHieuTruong;

            var khoi = (from k in db.DM_Khoi.Where(t => t.idTruong == idTruong && t.NamHoc == namhoc)
                        select k
                          ).ToList();
            DM_Khoi kl = new DM_Khoi();
            kl.MaKhoi = "";
            kl.TenKhoi = "--Chọn khối--";
            kl.idTruong = idTruong;
            kl.NamHoc = namhoc;
            khoi.Insert(0, kl);

            ViewBag.khoi = khoi;

            var lophoc = (from lh in db.LopHocs
                             let allowlops =
                             (from usermon in db.Users_Monhoc where usermon.UserName == userName && usermon.NamHoc == namhoc && usermon.idTruong == idTruong select usermon.MaLop).
                             Union(
                             from userLop in db.Users_LopHoc.Where(userLop => userLop.idTruong == idTruong && userLop.NamHoc == namhoc && userLop.UserName == userName)
                             select userLop.MaLop)
                             where allowlops.Contains(lh.MaLop) && lh.idTruong == idTruong && lh.NamHoc == namhoc
                             select new LopHocGiaoVien
                             {
                                 MaLop = lh.MaLop,
                                 TenLop = lh.TenLop,
                                 IdTruong = lh.idTruong,
                                  NamHoc = lh.NamHoc
                          }).Distinct().ToList();


             
            
            LopHocGiaoVien lhoc = new LopHocGiaoVien();
            lhoc.MaLop = "";
            lhoc.TenLop = "--Chọn lớp học--";
            lhoc.NamHoc = namhoc;
            lhoc.IdTruong = idTruong;
            lophoc.Insert(0, lhoc);            
            ViewBag.namhientai = namhoc;
            ViewBag.tenuser = Cmon.GetnameUser(userName);
            ViewBag.lophoc = lophoc;
            ViewBag.idTruong = idTruong;
            ViewBag.hocky = Cmon.CreateHocKyTheoDanhGia();
            ViewBag.hocKyMacDinh = new SelectList(Cmon.CreateHocKyTheoDanhGia(), "maHocKy", "tenHocKy", "GHK1");
            ViewBag.firstClass = new SelectList(lophoc, "MaLop", "TenLop", lophoc[1].MaLop.ToString());
                

            return View();
        }


        [HttpGet]
        public ActionResult DiemHocSinh(string malop, string hocKy)
        {
            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            string namhoc = Cmon.GetNamHienTai(idTruong);
            // xac dinh danh sach cac mon hoc cua mot lop

            var danhSachMonHoc = (from dsmh in db.ASSIGN_LOP_MonHoc where dsmh.idTruong == idTruong && dsmh.NamHoc == namhoc && dsmh.MaLop == malop select dsmh).ToList();
            var danhSachHocSinh = (from hocsinh in db.HocSinhs
                                   where !(from hsqt in db.HocSinh_QuaTrinh.Where(hsqt => hsqt.idTruong == idTruong && hsqt.NamHoc == namhoc && hsqt.MaLop == malop && hsqt.MaQuaTrinh == "BH")
                                           select hsqt.MaHocSinh).Contains(hocsinh.MaHocSinh)
                                   join hsnx in db.HocSinh_Diem_NhanXet
                                   on hocsinh.MaHocSinh equals hsnx.MaHocSinh
                                   let mamonhoc = (from monhoc in db.ASSIGN_LOP_MonHoc where monhoc.idTruong == idTruong && monhoc.NamHoc == namhoc && monhoc.MaLop == malop select monhoc.MaMonHoc)
                                   where hocsinh.idTruong == idTruong && hocsinh.NamHoc == namhoc && hocsinh.MaLop == malop && mamonhoc.Contains(hsnx.MaMonHoc)
                                   select new NhapDiemHocSinh
                                   {
                                       maHocSinh = hocsinh.MaHocSinh

                                   }).ToList();

          

            return PartialView(@"~/Views/GiaoVienBoMon/ViewHocSinh.cshtml", "");
        }

        [HttpGet]
        public ActionResult LoadMonHoc(string lophoc, string hocky)
        {
            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            string namhoc = Cmon.GetNamHienTai(idTruong);
            var monhoc = (from mh in db.ASSIGN_LOP_MonHoc.Where(t => t.idTruong == idTruong && t.NamHoc == namhoc && t.MaLop == lophoc)
                          select new DanhMucMonHoc
                          {
                              MaMon = mh.MaMonHoc,
                              TenMon = mh.TenMonHoc
                          }
                            ).ToList();
            return View(@"~/Views/TongHop/LoadMonHoc.cshtml", monhoc);
        }

        public JsonResult LoadLopHoc(string makhoi)
        {
            string userName = getUserName();

            int idTruong = Cmon.getIdtruong(userName, db);
            string namhoc = Cmon.GetNamHienTai(idTruong);
            var lophoc = (from lh in db.Users_LopHoc.Where(t => t.idTruong == idTruong && t.NamHoc == namhoc && t.UserName == userName)
                          join l in db.LopHocs.Where(i => i.idTruong == idTruong && i.NamHoc == namhoc && i.MaKhoi == makhoi)
                          on new { lh.idTruong, lh.NamHoc } equals new { l.idTruong, l.NamHoc }
                          select new LopHocGiaoVien
                          {
                              MaLop = l.MaLop,
                              TenLop = l.TenLop,
                              IdTruong = l.idTruong,
                              NamHoc = l.NamHoc
                          }
                            ).GroupBy(x => x.MaLop).Select(z => z.OrderBy(i => i.MaLop).FirstOrDefault()).ToList();
            LopHocGiaoVien lhoc = new LopHocGiaoVien();
            lhoc.MaLop = "";
            lhoc.TenLop = "--Chọn lớp học--";
            lhoc.NamHoc = namhoc;
            lhoc.IdTruong = idTruong;
            lophoc.Insert(0, lhoc);

            return Json(lophoc, JsonRequestBehavior.AllowGet);
        }


        public ActionResult SoNhapDiemXuatExcell(string maLop, string mahk)
        {
            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            string namhoc = Cmon.GetNamHienTai(idTruong);
            string tenTruong = Cmon.GetnameSchool(idTruong);
            string tenPhongGiaoDuc = Cmon.getDonViChuQuan(idTruong);
         
            DBHelper helper = new DBHelper();
            SqlParameter[] arParms = new SqlParameter[5];

            arParms[0] = new SqlParameter("@idTruong", SqlDbType.VarChar);
            arParms[0].Value = idTruong.ToString();

            arParms[1] = new SqlParameter("@namHoc", SqlDbType.VarChar);
            arParms[1].Value = namhoc;

            arParms[2] = new SqlParameter("@maLop", SqlDbType.NVarChar);
            arParms[2].Value = maLop;

            arParms[3] = new SqlParameter("@userName", SqlDbType.VarChar);
            arParms[3].Value = userName;

            arParms[4] = new SqlParameter("@hocky", SqlDbType.VarChar);
            arParms[4].Value = mahk;

            DataSet ds = helper.ExecuteDatasetPRO("spAutoGenerateMonHoc", arParms);

            var tenGiaoVienLists = (
                                     from usrs in db.ScrUsers
                                     join um in db.Users_Monhoc on usrs.UserName equals um.UserName
                                     join RU in db.ScrJRoleUsers on um.UserName equals RU.UserName
                                     join RF in db.ScrJRoleFunctions on RU.RoleName equals RF.RoleName
                                     where um.idTruong == idTruong && um.NamHoc == namhoc && um.MaLop == maLop
                                     && RF.Enable == true
                                     && RF.Read_ == true
                                     && RF.Insert_ == true
                                     && RF.Update_ == true
                                     && RF.FunctionID == 119
                                     select new user
                                     {
                                         fullName = usrs.FullName
                                     }).Distinct().ToList();

            string giaoVienBoMon = "";
            List<VnEduPlus.Models.user> users = (List<VnEduPlus.Models.user>)tenGiaoVienLists;
            if (users != null && users.Count > 0)
            {
                for (int i = 0; i < users.Count; i++)
                {
                    giaoVienBoMon += users[i].fullName + ";";
                }

            }

            giaoVienBoMon = giaoVienBoMon.Trim().Length > 0 ? giaoVienBoMon.Substring(0, giaoVienBoMon.LastIndexOf(";")) : "";
            string tenLop = Cmon.GetnameClass(maLop, idTruong, namhoc);
            if (ds != null && ds.Tables.Count > 0)
            {

                ExcelFileWriterSoLienLac<object> myExcel = new ExcelWriteSoLienLac();
                string tenFile = "SoNhapDiem_Template.xls";

                // định nghĩa thư mục chứa template
                string rootPathToTemplate = Server.MapPath("~/templateExcell/" + tenFile);

                string fileName = userName + "_SoNhapDiem";

                string rootProcessTemplate = Server.MapPath(System.IO.Path.GetDirectoryName("~/ProcessTemplate/")); // thư mục chứa các template sẽ được đem xử lý đổ dữ liệu

                string fileNameExcute = rootProcessTemplate + "\\" + fileName + ".xls"; // tạo một template để chuẩn bị cho xử lý
                System.IO.File.Delete(fileNameExcute);// xóa file tạm trước đó và tạo lại
                System.IO.File.Copy(rootPathToTemplate, fileNameExcute, true);// copy từ thư mục template tới thư mục chuẩn bị xử lý
                // Xử lý đổ dữ liệu vào các ô tương ứng
                myExcel.ExportSoNhapDiemToExcel(fileNameExcute, namhoc, tenTruong, tenLop, (DataTable) ds.Tables[0], giaoVienBoMon, tenPhongGiaoDuc);

                System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
                Cmon.stopExcel();
                return File(fileNameExcute, "application/ms-excel", "SoNhapDiem_" + tenLop + "_" + DateTime.Now.Day.ToString() + "_" + DateTime.Now.Month.ToString() + "_" + DateTime.Now.Year.ToString() + "_" + DateTime.Now.Hour.ToString() + "_" + DateTime.Now.Minute.ToString() + "_" + DateTime.Now.Second.ToString() + ".xls");


            }


           
           return File("PhieuLienLac.xls", "application/ms-excel", "PhieuLienLac.xls");
       }
        
                
        public ActionResult ViewHocSinhDiemMonHoc(string lophoc, string mahk, bool exportExcell = false)
        {
            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            string namhoc = Cmon.GetNamHienTai(idTruong);
            ViewBag.namhoc = namhoc;
            ViewBag.idTruong = idTruong;
            ViewBag.maLop = lophoc;
            ViewBag.exportExcell = exportExcell;
            ViewBag.isKhoaKy = Cmon.checkKhoaHocKy(idTruong, namhoc, lophoc, mahk, db) == true ? 1 : 0 ; 

            DBHelper helper = new DBHelper();
            SqlParameter[] arParms = new SqlParameter[5];

            arParms[0] = new SqlParameter("@idTruong", SqlDbType.VarChar);
            arParms[0].Value = idTruong.ToString();

            arParms[1] = new SqlParameter("@namHoc", SqlDbType.VarChar);
            arParms[1].Value = namhoc;

            arParms[2] = new SqlParameter("@maLop", SqlDbType.NVarChar);
            arParms[2].Value = lophoc;

            arParms[3] = new SqlParameter("@userName", SqlDbType.VarChar);
            arParms[3].Value = userName;

            arParms[4] = new SqlParameter("@hocky", SqlDbType.VarChar);
            arParms[4].Value = mahk;

            DataSet ds = helper.ExecuteDatasetPRO("spAutoGenerateMonHocTheoUsr", arParms);

            if (exportExcell)
            {
                string contentDisposition = "inline; filename=diemhocsinh.xls";
                
                Response.AppendHeader("content-disposition", contentDisposition);
                Response.AddHeader("Content-Type", "application/vnd.ms-excel");
            }


            var QuyenNhapDiem = (from RU in db.ScrJRoleUsers
                          join RF in db.ScrJRoleFunctions
                              on RU.RoleName equals RF.RoleName
                          where RU.UserName == userName && RF.idTruong == idTruong
                          && RF.FunctionID == 112 && RF.Insert_ == true && RF.Enable == true && RF.Update_ == true
                          select RF
                            ).FirstOrDefault();

            ViewBag.QuyenNhapDiem = QuyenNhapDiem != null ? 1 : 0;
             
            if (ds != null && ds.Tables.Count>0)
            {
                return View(@"~/Views/GiaoVienBoMon/ViewHocSinhDiemMonHoc.cshtml", ds.Tables[0]);
            }
            else
            {
                return View(@"~/Views/GiaoVienBoMon/ViewHocSinhDiemMonHoc.cshtml", null);
            }

        }


        public ActionResult ExportExcellHocSinhDiem(string lophoc, string mahk)
        {
            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            string namhoc = Cmon.GetNamHienTai(idTruong);
            ViewBag.namhoc = namhoc;
            ViewBag.idTruong = idTruong;
            ViewBag.maLop = lophoc;
            ViewBag.isKhoaKy = Cmon.checkKhoaHocKy(idTruong, namhoc, lophoc, mahk, db) == true ? 1 : 0;
            string tenTruong = Cmon.GetnameSchool(idTruong);
            DBHelper helper = new DBHelper();
            SqlParameter[] arParms = new SqlParameter[5];

            arParms[0] = new SqlParameter("@idTruong", SqlDbType.VarChar);
            arParms[0].Value = idTruong.ToString();

            arParms[1] = new SqlParameter("@namHoc", SqlDbType.VarChar);
            arParms[1].Value = namhoc;

            arParms[2] = new SqlParameter("@maLop", SqlDbType.NVarChar);
            arParms[2].Value = lophoc;

            arParms[3] = new SqlParameter("@userName", SqlDbType.VarChar);
            arParms[3].Value = userName;

            arParms[4] = new SqlParameter("@hocky", SqlDbType.VarChar);
            arParms[4].Value = mahk;

            DataSet ds = helper.ExecuteDatasetPRO("spAutoGenerateMonHoc", arParms);

            

            var QuyenNhapDiem = (from RU in db.ScrJRoleUsers
                                 join RF in db.ScrJRoleFunctions
                                     on RU.RoleName equals RF.RoleName
                                 where RU.UserName == userName && RF.idTruong == idTruong
                                 && RF.FunctionID == 112 && RF.Insert_ == true && RF.Enable == true && RF.Update_ == true
                                 select RF
                            ).FirstOrDefault();

            ViewBag.QuyenNhapDiem = QuyenNhapDiem != null ? 1 : 0;

            ExcelFileWriter<object> myExcel = new ExcelWrite();
            string path = ControllerContext.HttpContext.Server.MapPath("~/DiemHocSinh.xls");
          //  myExcel.WriteDateToExcelHocSinhDiem(path, ds.Tables[0], tenTruong, Cmon.GetnameClass(lophoc), mahk,namhoc);
            System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
            Cmon.stopExcel();
            return File(path, "application/octet-stream", "BangDiem_" + "_" + lophoc + "_" + DateTime.Now.Day.ToString() + "_" + DateTime.Now.Month.ToString() + "_" + DateTime.Now.Year.ToString() + "_" + DateTime.Now.Hour.ToString() + "_" + DateTime.Now.Minute.ToString() + ".xls");

            if (ds != null && ds.Tables.Count > 0)
            {
                return View(@"~/Views/GiaoVienBoMon/ViewHocSinhDiemMonHoc.cshtml", ds.Tables[0]);
            }
            else
            {
                return View(@"~/Views/GiaoVienBoMon/ViewHocSinhDiemMonHoc.cshtml", null);
            }

        }


        public bool isExitsMaHocSinh(int idTruong, string namHoc, string maLop, string maHS, string maMonHoc, EDUNP_CAP1Entities db)
        {

            var hocSinh = (from hocsinh in db.HocSinh_Diem_NhanXet where hocsinh.idTruong == idTruong && hocsinh.NamHoc == namHoc && hocsinh.MaLop == maLop && hocsinh.MaMonHoc == maMonHoc && hocsinh.MaHocSinh == maHS select hocsinh).FirstOrDefault();
            return hocSinh != null ? true : false;
        }


        public string getHoTenHocSinh(int idTruong, string namHoc, string maLop, string maHS, EDUNP_CAP1Entities db)
        {

            return (from hocsinh in db.HocSinhs where hocsinh.idTruong == idTruong && hocsinh.NamHoc == namHoc && hocsinh.MaLop == maLop && hocsinh.MaHocSinh == maHS select hocsinh).FirstOrDefault().HoTen.ToString();
            
        }

 
       [Authorize]
       
        public ActionResult SoBoMonXuatExcell(string maLop, int thang, string maMonHoc)
       {
           if (!Request.IsAuthenticated)
           {
               return RedirectToAction("Index", "Thongbao");
           }
           else
           {

               string userName = getUserName();
               int idTruong = Cmon.getIdtruong(userName, db);
               string namhoc = Cmon.GetNamHienTai(idTruong);
               string tenTruong = Cmon.GetnameSchool(idTruong);
               var tenGiaoVienLists = (
                                        from usrs in db.ScrUsers
                                        join um in db.Users_Monhoc on usrs.UserName equals um.UserName
                                        join RU in db.ScrJRoleUsers on um.UserName equals RU.UserName
                                        join RF in db.ScrJRoleFunctions on RU.RoleName equals RF.RoleName
                                        where um.idTruong == idTruong && um.NamHoc == namhoc && um.MaLop == maLop && um.MaMonHoc == maMonHoc
                                        && RF.Enable == true
                                        && RF.Read_ == true
                                        && RF.Insert_ == true
                                        && RF.Update_ == true
                                        && RF.FunctionID == 119
                                        select new user
                                        {
                                            fullName = usrs.FullName
                                        }).Distinct().ToList();

               string giaoVienBoMon = "";
               List<VnEduPlus.Models.user> users = (List<VnEduPlus.Models.user>)tenGiaoVienLists;
               if (users != null && users.Count > 0)
               {
                   for (int i = 0; i < users.Count; i++)
                   {
                       giaoVienBoMon += users[i].fullName + ";";
                   }

               }

               giaoVienBoMon = giaoVienBoMon.Trim().Length > 0 ? giaoVienBoMon.Substring(0, giaoVienBoMon.LastIndexOf(";")) : "";
               var hsnx = (from hs in db.HocSinhs.Where(t => t.idTruong == idTruong && t.MaLop == maLop && t.NamHoc == namhoc)
                           where !(from hsqt in db.HocSinh_QuaTrinh.Where(hsqt => hsqt.idTruong == idTruong && hsqt.NamHoc == namhoc && hsqt.MaLop == maLop && hsqt.MaQuaTrinh == "BH")
                                   select hsqt.MaHocSinh).Contains(hs.MaHocSinh)
                           join hst in db.HocSinh_DanhGiaMonHoc.Where(hsdg => hsdg.MaMonHoc == maMonHoc
                               && hsdg.idTruong == idTruong
                               && hsdg.MaLop == maLop
                               && hsdg.NamHoc == namhoc

                               ) on new { hs.MaHocSinh, hs.idTruong, hs.MaLop, hs.NamHoc } equals new { hst.MaHocSinh, hst.idTruong, hst.MaLop, hst.NamHoc } into hocsinh
                           from hsj in hocsinh.DefaultIfEmpty()
                           //where(hsj.MaMonHoc == mamon && hsj.idTruong == idTruong && hsj.NamHoc == namhoc && hsj.MaLop == malop)
                           select new HocSinhNhanXetThang
                           {
                               IdTruong = hs.idTruong,
                               NamHoc = hs.NamHoc,
                               MaLop = hsj.MaLop,
                               ngaySinh = hs.NgaySinh,
                               MaHocSinh = hs.MaHocSinh,
                               MaMonHoc = hsj.MaMonHoc,
                               HoTen = hs.HoTen,
                               NXKT = (thang == 8 ? hsj.NXT1_KT : thang == 9 ? hsj.NXT2_KT : thang == 10 ? hsj.NXT3_KT : thang == 11 ? hsj.NXT4_KT : thang == 12 ? hsj.NXT5_KT : thang == 1 ? hsj.NXT6_KT : thang == 2 ? hsj.NXT7_KT : thang == 3 ? hsj.NXT8_KT : thang == 4 ? hsj.NXT9_KT : hsj.NXT10_KT),
                               NXPC = (thang == 8 ? hsj.NXT1_PC : thang == 9 ? hsj.NXT2_PC : thang == 10 ? hsj.NXT3_PC : thang == 11 ? hsj.NXT4_PC : thang == 12 ? hsj.NXT5_PC : thang == 1 ? hsj.NXT6_PC : thang == 2 ? hsj.NXT7_PC : thang == 3 ? hsj.NXT8_PC : thang == 4 ? hsj.NXT9_PC : hsj.NXT10_PC),
                               NXNL = (thang == 8 ? hsj.NXT1_NL : thang == 9 ? hsj.NXT2_NL : thang == 10 ? hsj.NXT3_NL : thang == 11 ? hsj.NXT4_NL : thang == 12 ? hsj.NXT5_NL : thang == 1 ? hsj.NXT6_NL : thang == 2 ? hsj.NXT7_NL : thang == 3 ? hsj.NXT8_NL : thang == 4 ? hsj.NXT9_NL : hsj.NXT10_NL),
                           }
                             ).ToList();
               string maKhoi = Cmon.getKhoi(maLop, namhoc, idTruong);

               
               string tenLop = Cmon.GetnameClass(maLop, idTruong, namhoc);
               
               if ( hsnx != null && hsnx.Count > 0)
               {

                   ExcelFileWriterSoLienLac<object> myExcel = new ExcelWriteSoLienLac();
                   string tenFile = "SoBM_Template.xls";

                   // định nghĩa thư mục chứa template
                   string rootPathToTemplate = Server.MapPath("~/templateExcell/" + tenFile);

                   string fileName = userName + "_SoBoMon";

                   string rootProcessTemplate = Server.MapPath(System.IO.Path.GetDirectoryName("~/ProcessTemplate/")); // thư mục chứa các template sẽ được đem xử lý đổ dữ liệu

                   string fileNameExcute = rootProcessTemplate + "\\" + fileName + ".xls"; // tạo một template để chuẩn bị cho xử lý
                   System.IO.File.Delete(fileNameExcute);// xóa file tạm trước đó và tạo lại
                   System.IO.File.Copy(rootPathToTemplate, fileNameExcute, true);// copy từ thư mục template tới thư mục chuẩn bị xử lý
                   string tenPhongGiaoDuc = Cmon.getDonViChuQuan(idTruong);
                   // Xử lý đổ dữ liệu vào các ô tương ứng
                   myExcel.ExportSoBoMonToExcel(fileNameExcute, thang, namhoc, tenTruong, tenLop, hsnx, giaoVienBoMon, tenPhongGiaoDuc);

                   System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
                   Cmon.stopExcel();
                   return File(fileNameExcute, "application/ms-excel", "SoBoMonTheoDoiChatLuongGiaoDuc.xls");


               }


           }
           return File("PhieuLienLac.xls", "application/ms-excel", "PhieuLienLac.xls");
       }
        


        [AcceptVerbs(HttpVerbs.Post)]
        [Authorize]
        public JsonResult LuuDiemHocSinh(int idTruong, string maLop, string hocKy, List<HocSinhDiemMonHoc> diemHocSinhs)
        {

            string mess = "OK";
           
            string userName = getUserName();
            string namHoc = Cmon.GetNamHienTai(idTruong);

            try
            {

                if (diemHocSinhs != null && diemHocSinhs.Count > 0)
                {
                    foreach (HocSinhDiemMonHoc HSD in diemHocSinhs)
                    {
                        bool isExitsMaHS = isExitsMaHocSinh(idTruong, namHoc, maLop, HSD.maHocSinh, HSD.maMonHoc, db);

                        if (!isExitsMaHS)
                        {
                            // thêm mới 
                            ThemMoiDiemHocSinh(idTruong, maLop, hocKy, namHoc, HSD, userName);
                        }
                        else
                        {
                            // cập nhật
                            CapNhatDiemHocSinh(idTruong, maLop, hocKy, namHoc, HSD, userName);
                        }

                    }
                    mess = "OK";

                }
                else { mess = "NOTUPDATE"; }
            }
            catch
            {
                
                mess = "NOTUPDATE";
            }
            return Json(mess, JsonRequestBehavior.AllowGet);
        }

        public void ThemMoiDiemHocSinh(int idTruong, string maLop, string hocKy, string namHoc, HocSinhDiemMonHoc HSD, string usrName)
        {
            HocSinh_Diem_NhanXet HocSinhDiem = new HocSinh_Diem_NhanXet();
             
            HocSinhDiem.idTruong = idTruong;
            HocSinhDiem.NamHoc = namHoc;
            HocSinhDiem.MaHocSinh = HSD.maHocSinh;
            HocSinhDiem.MaHocSinhFriendly = HSD.maHocSinh;
            HocSinhDiem.HoTen = getHoTenHocSinh(idTruong, namHoc, maLop, HSD.maHocSinh, db); ;
            HocSinhDiem.MaLop = maLop;
            HocSinhDiem.MaMonHoc = HSD.maMonHoc;
            HocSinhDiem.UserCreated = usrName;
            HocSinhDiem.DateCreated = DateTime.Now;

            switch (hocKy)
            {
                case "HK1":
                    HocSinhDiem.DiemHK1 = HSD.diemMonHoc == null ? (decimal?)null : Decimal.Parse(HSD.diemMonHoc);
                    break;
                case "GHK1":
                    HocSinhDiem.DiemGHK1 = HSD.diemMonHoc == null ? (decimal?)null : Decimal.Parse(HSD.diemMonHoc);
                    break;

                case "GHK2":
                    HocSinhDiem.DiemGHK2 = HSD.diemMonHoc == null ? (decimal?)null : Decimal.Parse(HSD.diemMonHoc);
                    break;

                case "HK2":
                    HocSinhDiem.DiemCN = HSD.diemMonHoc == null ? (decimal?)null : Decimal.Parse(HSD.diemMonHoc);
                    break;

                default:

                    break;
            }


            db.HocSinh_Diem_NhanXet.AddObject(HocSinhDiem);
            db.SaveChanges();
            
        }


        public void CapNhatDiemHocSinh(int idTruong, string maLop, string hocKy, string namHoc, HocSinhDiemMonHoc HSD, string usrName)
        {
            var hocSinh = (from hocsinh in db.HocSinh_Diem_NhanXet where hocsinh.idTruong == idTruong && hocsinh.NamHoc == namHoc && hocsinh.MaLop == maLop && hocsinh.MaHocSinh == HSD.maHocSinh && hocsinh.MaMonHoc == HSD.maMonHoc select hocsinh).FirstOrDefault();

            hocSinh.UserCreated = usrName;
            hocSinh.DateUpdated = DateTime.Now;
            switch (hocKy)
            { case "HK1":
                    hocSinh.DiemHK1 = HSD.diemMonHoc == null ? (decimal?)null : Decimal.Parse(HSD.diemMonHoc);
                    break;
                case "GHK1":
                    hocSinh.DiemGHK1 = HSD.diemMonHoc == null ? (decimal?)null : Decimal.Parse(HSD.diemMonHoc);
                    break;
                
                case "GHK2":
                    hocSinh.DiemGHK2 = HSD.diemMonHoc == null ? (decimal?)null : Decimal.Parse(HSD.diemMonHoc);
                    break;

                case "HK2":
                    hocSinh.DiemCN = HSD.diemMonHoc == null ? (decimal?)null : Decimal.Parse(HSD.diemMonHoc);
                    break;
                
                default:
                    
                    break;     
            }

            db.SaveChanges();
            
             
        }

        #endregion
        #endregion

    }
}

        