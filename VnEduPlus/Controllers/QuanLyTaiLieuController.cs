﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VnEduPlus.CmonFunction;
using VnEduPlus.Models;
using Lib.Web.Mvc.JQuery.JqGrid;
using System.IO;
 

namespace VnEduPlus.Controllers
{
    public class QuanLyTaiLieuController : Controller
    {
        EDUNP_CAP1Entities db = new EDUNP_CAP1Entities();

        //
        // GET: /diemHocSinh/
        [Authorize]
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// lay username dang nhap 
        /// </summary>
        /// <returns></returns>
        public string getUserName()
        {
            return User.Identity.Name.ToString() == "" ? "superadmin" : User.Identity.Name.ToString();

        }
        public bool checkRightClass(string malop, string userName ){            
            int idTruong = Cmon.getIdtruong(userName, db);
            string namhoc = Cmon.GetNamHienTai(idTruong);
            bool result=false;
            var q1 =(from lophoc in db.LopHocs
                                 join userlh in
                                     ((from l1 in db.Users_LopHoc where l1.UserName == userName && l1.idTruong == idTruong && l1.NamHoc == namhoc select l1.MaLop).Union
                                     (from l2 in db.Users_Monhoc where l2.UserName == userName && l2.idTruong == idTruong && l2.NamHoc == namhoc select l2.MaLop))
                                 on lophoc.MaLop equals userlh
                                 where lophoc.idTruong == idTruong && lophoc.NamHoc == namhoc && lophoc.MaLop == malop
                                 orderby lophoc.MaLop
                                 select lophoc.MaLop).ToList();


            if (q1 != null) { result = true; }
            return result; 
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult getFillesBaseClasses()
        {
            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            string pathForSearch = Server.MapPath(@"~/Content/uploads/" + idTruong.ToString() + "/FileBaoCao/"); // đọc các file theo lần lượt từng lớp
            
            DirectoryInfo directory = new DirectoryInfo(pathForSearch);
            
            List<FilesForClasses> entitiesfiles = new List<FilesForClasses>();

            DirectoryInfo[] directories = directory.GetDirectories();

            foreach (DirectoryInfo folder in directories)
            { 
                    string subPath = pathForSearch + folder.Name + "\\";
                    foreach (string file in Directory.GetFiles(subPath))
                    {
                         FilesForClasses  entitiesFile =  new FilesForClasses();
                         entitiesFile.idTrương = idTruong.ToString();
                         entitiesFile.malop = folder.Name;
                         entitiesFile.fileName ="SoChuNhiem_" + folder;
                         entitiesFile.isExits = true;
                         entitiesfiles.Add(entitiesFile);
                           
                    }
                }

          
            return new JqGridJsonResult() { Data = entitiesfiles };
        }

        [HttpPost]
        public JsonResult addFiles(int idTruong, string fileName, string description, string path)
        {
            string mess = "";
            
            UploadFile uploadFile = new UploadFile();
            uploadFile.idTruong = idTruong;
            uploadFile.fileName = fileName;
            uploadFile.desciption = description;
            uploadFile.path = path;
            db.UploadFiles.AddObject(uploadFile);
            int rs = db.SaveChanges();
            if (rs > 0)
                mess = "OK";
            else
                mess = "Thêm mới file không thành công!";
            // }
            return Json(mess, JsonRequestBehavior.AllowGet);
        }

        

        /// <summary>
        /// Thực hiện việc xóa file trong table upload file và thực hiện xóa vật lý file trên server
        /// sau đó load lại dữ liệu để đảm bảo dữ liệu trả về là mới nhất
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        public ActionResult DeleteFile(int idFileUpload)
        {
             // lay thong tin truong 
            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            UploadFile uploadFile = getUploadFileEntity(idFileUpload);
            string fileNameCurrent = uploadFile.fileName;
            string path = Server.MapPath("~/Content/uploads/" + idTruong + "/TaiLieuMau/");
            db.UploadFiles.DeleteObject(uploadFile);
            db.SaveChanges();
            //// sau khi xóa xong trên table thì thực hiện xóa file đó trên server
            ////string fileNameDeleting = path + fileNameCurrent;
            ////System.IO.File.Delete(fileNameDeleting);
            //string fileNameDeleting = System.IO.Path.Combine(path, fileNameCurrent);
            //try
            //{
            //    System.IO.File.Delete(fileNameDeleting);
            //}
            //catch (System.IO.IOException e)
            //{
            //    Console.WriteLine(e.Message);
            //    //return;
            //}

            //System.IO.FileInfo fi = new System.IO.FileInfo(fileNameDeleting);
            //try
            //{
            //    fi.Delete();
            //}
            //catch (System.IO.IOException e)
            //{
            //    Console.WriteLine(e.Message);
            //}

           
            var currentUploadFile = (from fileName in db.UploadFiles where fileName.idTruong == idTruong select fileName).ToList();
            HieuTruongUploadFiles hieuTruongUploadFiles = new HieuTruongUploadFiles();
            hieuTruongUploadFiles.idtruong = idTruong;
            hieuTruongUploadFiles.fileuploads = currentUploadFile;
            return View(@"~/Views/QuanLyTaiLieu/DanhSachTaiLieuMauMoiNhat.cshtml", hieuTruongUploadFiles);
            
        }

        private UploadFile getUploadFileEntity(int idFileUpload)
        {
           return db.UploadFiles.First(entity => entity.ID == idFileUpload);
        }



        [AcceptVerbs(HttpVerbs.Get)]
        [Authorize]
        public ActionResult TaiLieuMau()
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Thongbao");
            }
            // lay thong tin truong 
            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            string namhoc = Cmon.GetNamHienTai(idTruong);
            ViewBag.loaiNhapDiem = Cmon.checkLoaiNhapDiemCuaTruong(idTruong);
            ViewBag.style = Cmon.checkFormNhapDiemCuaTruong(idTruong); 
            string namHienTai = namhoc;
            DateTime ngayHH = DateTime.Now;

			ViewBag.rightAccess = Cmon.getRole(userName, idTruong, 131, db);
           
            var q1 = (from lophoc in db.LopHocs
                      join userlh in
                          ((from l1 in db.Users_LopHoc where l1.UserName == userName && l1.idTruong == idTruong && l1.NamHoc == namhoc select l1.MaLop).Union
                          (from l2 in db.Users_Monhoc where l2.UserName == userName && l2.idTruong == idTruong && l2.NamHoc == namhoc select l2.MaLop))
                      on lophoc.MaLop equals userlh
                      where lophoc.idTruong == idTruong && lophoc.NamHoc == namhoc
                      orderby lophoc.MaLop

                      select new TaiLieuMau
                      {
                          
                          tenGiaoVienLists = (
                                     from usrs in db.ScrUsers
                                     join ul in db.Users_LopHoc on usrs.UserName equals ul.UserName
                                     where ul.idTruong == idTruong && ul.NamHoc == namhoc && ul.MaLop == lophoc.MaLop && usrs.IsAdmin == false
                                     && (from RU in db.ScrJRoleUsers
                                         join RF in db.ScrJRoleFunctions
                                         on RU.RoleName equals RF.RoleName
                                         where RU.UserName == usrs.UserName && RF.idTruong == idTruong
                                         //&& RU.RoleName=="GV" 
                                         && RF.Update_ == true 
                                         && RF.Read_ == true
                                         && RF.Insert_ == true
                                         && RF.FunctionID == 118
                                         select RU.UserName).Contains(usrs.UserName)
                                     
                                     select new user
                                     {
                                         fullName = usrs.FullName
                                     }),


 
                          Tengiaovien = (from users in db.ScrUsers
                                         where users.idTruong == idTruong && users.UserName == userName
                                         join userslop in db.Users_LopHoc on users.UserName equals userslop.UserName
                                         where userslop.idTruong == idTruong && userslop.MaLop == lophoc.MaLop
                                         select users.FullName).FirstOrDefault()
                                           ,
                          quyenUploadFile = (from usr in db.ScrUsers
                                             join
                                                 userslop in db.Users_LopHoc on usr.UserName equals userslop.UserName
                                             where usr.UserName == userName && usr.idTruong == idTruong && userslop.MaLop == lophoc.MaLop
                                             && userslop.idTruong == idTruong
                                             && (from RU in db.ScrJRoleUsers
                                                 join RF in db.ScrJRoleFunctions
                                                 on RU.RoleName equals RF.RoleName
                                                 where RU.UserName == usr.UserName && RF.idTruong == idTruong
                                       && RU.RoleName == "GV"  
                                       && RF.Update_ == true
                                       && RF.Read_ == true
                                       && RF.Insert_ == true
                                       && RF.FunctionID == 118
                                                 select RU.UserName).Contains(userName)
                                             select usr).Any(),
                          //maky = HKs,
                          makhoi = lophoc.MaKhoi,
                          malop =lophoc.MaLop,
                          tenLop=lophoc.TenLop
                      });

            string danhsachfile = "";

            string pathForSearch = Server.MapPath(@"~/Content/uploads/" + idTruong.ToString() + "/FileBaoCao/"); // đọc các file theo lần lượt từng lớp

            DirectoryInfo directory = new DirectoryInfo(pathForSearch);

            List<FilesForClasses> entitiesfiles = new List<FilesForClasses>();
            if (Directory.Exists(pathForSearch))
            {
                DirectoryInfo[] directories = directory.GetDirectories();

                foreach (DirectoryInfo folder in directories)
                {
                    string subPath = pathForSearch + folder.Name + "\\";
                    foreach (string file in Directory.GetFiles(subPath))
                    {
                        string fileNamePath = subPath + namHienTai + "_" + "SoChuNhiem_" + folder + Path.GetExtension(file);
                        if (System.IO.File.Exists(fileNamePath))
                        {
                            danhsachfile += namHienTai + "_" + "SoChuNhiem_" + folder + Path.GetExtension(file) + ",";
                        }
                        
                    }
                }
             
            }
           

            // lay danh sách các năm học từ trước tới nay 

           var danhSachNamHoc = (from dsnh in db.DM_NamHoc
                                      where dsnh.idTruong == idTruong
                                      orderby dsnh.NamHoc descending
                                        select dsnh
                                   ).Distinct().ToList();


              
            string ApplicationPath = (!string.IsNullOrWhiteSpace(Request.ApplicationPath) && Request.ApplicationPath != "/") ? Request.ApplicationPath : "";
            string Domain = Request.Url.Scheme + System.Uri.SchemeDelimiter + Request.Url.Host + (Request.Url.IsDefaultPort ? "" : ":" + Request.Url.Port);
            string link = "";
            link = Domain + ApplicationPath;
            //Get full path
            string _fileBaoCaopath = link + "/Content/uploads/" + idTruong.ToString() + "/FileBaoCao/";

            ViewBag.danhsachTailieu = danhsachfile;
            ViewBag.path = _fileBaoCaopath;
            ViewBag.namTaiLieu = namHienTai;

            DM_HocKy hkcn = new DM_HocKy();
            hkcn.MaHocKy = "CN";
            hkcn.TenHocKy = "Cả năm";
            var listhocky = db.DM_HocKy.Where(hk => hk.idTruong == idTruong && hk.NamHoc == namhoc).ToList();
            listhocky.Add(hkcn);
            bool isHieuTruong = Cmon.isHieuTruong(idTruong, userName);
            ViewBag.isHieuTruong = isHieuTruong;
           // ViewBag.currenHocKy = HKs;
            ViewBag.MaHocKy = new SelectList(listhocky, "MaHocKy", "TenHocKy", null);
            ViewBag.idtruong = idTruong;
            ViewBag.tentruong = Cmon.GetnameSchool(idTruong);
            ViewBag.danhsach = q1;
            ViewBag.namhientai = namhoc;
            ViewBag.namTaiLieu = namhoc;
            ViewBag.tendangnhap = userName;
           
            TaiLieuThamKhao tltk = new TaiLieuThamKhao();
            tltk.taiLieuMau = q1;

            tltk.namHoc = danhSachNamHoc;
            tltk.namTaiLieu = "namTaiLieu";

            return View(tltk);

        }


        [AcceptVerbs(HttpVerbs.Get)]
        [Authorize]
        public ActionResult TaiLieuMauHangNam(string namHienTai)
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Thongbao");
            }
            // lay thong tin truong 
            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            string namhoc = Cmon.GetNamHienTai(idTruong);
            ViewBag.loaiNhapDiem = Cmon.checkLoaiNhapDiemCuaTruong(idTruong);
            ViewBag.style = Cmon.checkFormNhapDiemCuaTruong(idTruong);

            namHienTai = namHienTai == null ? namhoc : namHienTai;
            DateTime ngayHH = DateTime.Now;

            var q1 = (from lophoc in db.LopHocs
                      join userlh in
                          ((from l1 in db.Users_LopHoc where l1.UserName == userName && l1.idTruong == idTruong && l1.NamHoc == namHienTai select l1.MaLop).Union
                          (from l2 in db.Users_Monhoc where l2.UserName == userName && l2.idTruong == idTruong && l2.NamHoc == namHienTai select l2.MaLop))
                      on lophoc.MaLop equals userlh
                      where lophoc.idTruong == idTruong && lophoc.NamHoc == namHienTai
                      orderby lophoc.MaLop

                      select new TaiLieuMau
                      {

                          tenGiaoVienLists = (
                                     from usrs in db.ScrUsers
                                     join ul in db.Users_LopHoc on usrs.UserName equals ul.UserName
                                     where ul.idTruong == idTruong && ul.NamHoc == namHienTai && ul.MaLop == lophoc.MaLop && usrs.IsAdmin == false
                                     && (from RU in db.ScrJRoleUsers
                                         join RF in db.ScrJRoleFunctions
                                         on RU.RoleName equals RF.RoleName
                                         where RU.UserName == usrs.UserName && RF.idTruong == idTruong
                                             // && RU.RoleName=="GVCN" 
                                         && RF.Update_ == true
                                         && RF.Read_ == true
                                         && RF.Insert_ == true
                                         && RF.FunctionID == 118
                                         select RU.UserName).Contains(usrs.UserName)

                                     select new user
                                     {
                                         fullName = usrs.FullName
                                     }),



                          Tengiaovien = (from users in db.ScrUsers
                                         where users.idTruong == idTruong && users.UserName == userName
                                         join userslop in db.Users_LopHoc on users.UserName equals userslop.UserName
                                         where userslop.idTruong == idTruong && userslop.MaLop == lophoc.MaLop
                                         select users.FullName).FirstOrDefault(),

                           quyenUploadFile = (from usr in db.ScrUsers join 
                                               userslop in db.Users_LopHoc on usr.UserName equals userslop.UserName
                                              where usr.UserName == userName && usr.idTruong == idTruong && userslop.MaLop == lophoc.MaLop
                                              && userslop.idTruong == idTruong 
                                              &&  (from RU in db.ScrJRoleUsers
                                         join RF in db.ScrJRoleFunctions
                                         on RU.RoleName equals RF.RoleName
                                                   where RU.UserName == usr.UserName && RF.idTruong == idTruong
                                         && RU.RoleName == "GV" 
                                         && RF.Update_ == true
                                         && RF.Read_ == true
                                         && RF.Insert_ == true
                                         && RF.FunctionID == 118
                                         select RU.UserName).Contains(userName)select usr).Any(),
                           
                          //maky = HKs,
                          makhoi = lophoc.MaKhoi,
                          malop = lophoc.MaLop,
                          tenLop = lophoc.TenLop
                      });

            string danhsachfile = "";

            string pathForSearch = Server.MapPath(@"~/Content/uploads/" + idTruong.ToString() + "/FileBaoCao/"); // đọc các file theo lần lượt từng lớp

            DirectoryInfo directory = new DirectoryInfo(pathForSearch);

            List<FilesForClasses> entitiesfiles = new List<FilesForClasses>();
            if (Directory.Exists(pathForSearch))
            {
                DirectoryInfo[] directories = directory.GetDirectories();

                foreach (DirectoryInfo folder in directories)
                {
                    string subPath = pathForSearch + folder.Name + "\\";
                    foreach (string file in Directory.GetFiles(subPath))
                    {
                        string fileNamePath = subPath + namHienTai + "_" + "SoChuNhiem_" + folder + Path.GetExtension(file);
                        if (System.IO.File.Exists(fileNamePath))
                        {
                            danhsachfile += namHienTai + "_" + "SoChuNhiem_" + folder + Path.GetExtension(file) + ",";
                        }

                    }
                }

            }

            var danhSachNamHoc = (from dsnh in db.DM_NamHoc
                                  where dsnh.idTruong == idTruong
                                  orderby dsnh.NamHoc descending
                                  select dsnh
                                 ).Distinct().ToList();

            string ApplicationPath = (!string.IsNullOrWhiteSpace(Request.ApplicationPath) && Request.ApplicationPath != "/") ? Request.ApplicationPath : "";
            string Domain = Request.Url.Scheme + System.Uri.SchemeDelimiter + Request.Url.Host + (Request.Url.IsDefaultPort ? "" : ":" + Request.Url.Port);
            string link = "";
            link = Domain + ApplicationPath;
            //Get full path
            string _fileBaoCaopath = link + "/Content/uploads/" + idTruong.ToString() + "/FileBaoCao/";

            ViewBag.danhsachTailieu = danhsachfile;
            ViewBag.path = _fileBaoCaopath;
            ViewBag.namTaiLieu = namHienTai;

            DM_HocKy hkcn = new DM_HocKy();
            hkcn.MaHocKy = "CN";
            hkcn.TenHocKy = "Cả năm";
            var listhocky = db.DM_HocKy.Where(hk => hk.idTruong == idTruong && hk.NamHoc == namhoc).ToList();
            listhocky.Add(hkcn);
            bool isHieuTruong = Cmon.isHieuTruong(idTruong, userName);
            ViewBag.isHieuTruong = isHieuTruong;
            // ViewBag.currenHocKy = HKs;
            ViewBag.MaHocKy = new SelectList(listhocky, "MaHocKy", "TenHocKy", null);
            ViewBag.idtruong = idTruong;
            ViewBag.tentruong = Cmon.GetnameSchool(idTruong);
            ViewBag.danhsach = q1;
            ViewBag.namhientai = namhoc;
            ViewBag.tendangnhap = userName;
            TaiLieuThamKhao tltk = new TaiLieuThamKhao();
            tltk.taiLieuMau = q1;

            tltk.namHoc = danhSachNamHoc;
            tltk.namTaiLieu = "namTaiLieu";

            return View(@"~/Views/QuanLyTaiLieu/TaiLieuMauHangNam.cshtml", tltk);

        }


        
        [AcceptVerbs(HttpVerbs.Get)]
        [Authorize]
        public ActionResult DanhSachTaiLieuMau()
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Thongbao");
            }
            // lay thong tin truong 
            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            string namhoc = Cmon.GetNamHienTai(idTruong);
            bool isHieuTruong = Cmon.isHieuTruong(idTruong, userName);
            ViewBag.isHieuTruong = isHieuTruong;
            ViewBag.idTruong = idTruong;
            var taiLieuMaus = (from fileName in db.UploadFiles where fileName.idTruong == idTruong select fileName).ToList();
            HieuTruongUploadFiles hieuTruongUploadFiles = new HieuTruongUploadFiles();
            hieuTruongUploadFiles.idtruong = idTruong;
            hieuTruongUploadFiles.fileuploads = taiLieuMaus;
            return View(@"~/Views/QuanLyTaiLieu/DanhSachTaiLieuMau.cshtml", hieuTruongUploadFiles);
             

        }


        [AcceptVerbs(HttpVerbs.Get)]
        [Authorize]
        public ActionResult DanhSachTaiLieuMauMoiNhat(int idTruong)
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Thongbao");
            }
            // lay thong tin truong 
            ViewBag.idTruong = idTruong;
            var taiLieuMaus = (from fileName in db.UploadFiles where fileName.idTruong == idTruong select fileName).ToList();
            HieuTruongUploadFiles hieuTruongUploadFiles = new HieuTruongUploadFiles();
            hieuTruongUploadFiles.idtruong = idTruong;
            hieuTruongUploadFiles.fileuploads = taiLieuMaus;
            return View(@"~/Views/QuanLyTaiLieu/DanhSachTaiLieuMauMoiNhat.cshtml", hieuTruongUploadFiles);
            
        }

        public ActionResult DocFileOnline()
        {

            //return View();
           return View(@"~/Views/QuanLyTaiLieu/DocFileOnline.cshtml");
            
        }

        

        [AcceptVerbs(HttpVerbs.Get)]
        [Authorize]
        public ActionResult TaiLieuMauGiaoVien()
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Thongbao");
            }
            // lay thong tin truong 
            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            ViewBag.tentruong = Cmon.GetnameSchool(idTruong);
            // lay thong tin truong 
            ViewBag.idTruong = idTruong;
            ViewBag.namhientai = Cmon.GetNamHienTai(idTruong);
            var taiLieuMaus = (from fileName in db.UploadFiles where fileName.idTruong == idTruong select fileName).ToList();
            HieuTruongUploadFiles hieuTruongUploadFiles = new HieuTruongUploadFiles();
            hieuTruongUploadFiles.idtruong = idTruong;
            hieuTruongUploadFiles.fileuploads = taiLieuMaus;
            return View(@"~/Views/QuanLyTaiLieu/TaiLieuMauGiaoVien.cshtml", hieuTruongUploadFiles);

        }
 

        [Authorize]
        public ActionResult danhsachmonhoc(string malop)
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Thongbao");

            }
            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);

            string namhoc = Cmon.GetNamHienTai(idTruong);
             
            var danhsachcacmonhoc = (from lopMonHoc in db.ASSIGN_LOP_MonHoc 
                                     join monhoc in db.DM_MonHoc
                                     on lopMonHoc.MaMonHoc equals monhoc.MaMonHoc
                                     where lopMonHoc.NamHoc == namhoc && lopMonHoc.idTruong == idTruong && lopMonHoc.MaLop == malop
                                     orderby monhoc.MaMonHoc ascending
                                     select new MonHocDS
                                     {
                                         Mamonhoc = monhoc.MaMonHoc,
                                         Tenmonhoc = monhoc.TenMonHoc,

                                         tenGiaoVienLists = (
                                             from usrs in db.ScrUsers
                                             join um in db.Users_Monhoc on usrs.UserName equals um.UserName
                                             join RU in db.ScrJRoleUsers on um.UserName equals RU.UserName
                                             join RF in db.ScrJRoleFunctions on RU.RoleName equals RF.RoleName
                                             where usrs.idTruong == idTruong && um.MaMonHoc == lopMonHoc.MaMonHoc && um.MaLop == lopMonHoc.MaLop && um.NamHoc == namhoc
                                             && RF.Enable == true
                                             && RF.Read_ == true
                                             && RF.Insert_ == true
                                             && RF.Update_ == true
                                             && RF.FunctionID == 119 // giao vien duoc quyen truy xuat doc ghi o mon hoc trong bang function no co Id=49 //bbimonhoc
                                            select new user
                                            {
                                                fullName = usrs.FullName
                                            }).Distinct()


                                     }).ToList();

           
            ViewBag.tentruong = Cmon.GetnameSchool(idTruong);
            ViewBag.tenlop = Cmon.GetnameClass(malop, idTruong, namhoc);
            ViewBag.tendangnhap = Cmon.GetnameUserName(userName, idTruong);
            ViewBag.tongso = danhsachcacmonhoc.Count();
            ViewBag.namhoc = namhoc;
            ViewBag.malop = malop;
            ViewBag.idtruong = idTruong;
            ViewBag.username = userName;
            ViewBag.PhongGiaoDuc = Cmon.getDonViChuQuan(idTruong);
            return View(@"~/Views/quanLyLopHoc/danhsachmonhoc.cshtml", danhsachcacmonhoc);


        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Authorize]
        public ActionResult ViewDanhSachMonHoc(JqGridRequest request, string malop, string userName, string namhoc, int idTruong)
        {

            int totalRecords = (from lopMonHoc in db.ASSIGN_LOP_MonHoc
                                where lopMonHoc.NamHoc == namhoc && lopMonHoc.idTruong == idTruong && lopMonHoc.MaLop == malop
                                select lopMonHoc).Count();



            //Prepare JqGridData instance
            JqGridResponse response = new JqGridResponse()
            {
                //Total pages count
                TotalPagesCount = (int)Math.Ceiling((float)totalRecords / (float)request.RecordsCount),
                //Page number
                PageIndex = request.PageIndex,
                //Total records count
                TotalRecordsCount = totalRecords
            };

            var pageIndex = request.PageIndex;
            var pageSize = request.RecordsCount;
            var pagesCount = request.PagesCount.HasValue ? request.PagesCount.Value : 1;

            var rowschuyencanList = (from lopMonHoc in db.ASSIGN_LOP_MonHoc.Where(lopMon => lopMon.NamHoc == namhoc && lopMon.idTruong == idTruong && lopMon.MaLop == malop)
                                     join monhoc in db.DM_MonHoc
                                     on lopMonHoc.MaMonHoc equals monhoc.MaMonHoc
                                     orderby monhoc.MaMonHoc ascending
                                     select new MonHocDS
                                     {
                                         Mamonhoc = monhoc.MaMonHoc,
                                         Tenmonhoc = monhoc.TenMonHoc,
                                         tenGiaoVienLists = (
                                         from usrs in db.ScrUsers
                                         join ul in db.Users_Monhoc on usrs.UserName equals ul.UserName
                                         where ul.idTruong == idTruong && ul.NamHoc == namhoc && ul.MaLop == malop && ul.MaMonHoc == monhoc.MaMonHoc


                                         let RoleName = from RU in db.ScrJRoleUsers
                                                        join RF in db.ScrJRoleFunctions
                                                        on RU.RoleName equals RF.RoleName
                                                        where RU.UserName == usrs.UserName && RF.idTruong == idTruong
                                                        && RF.FunctionID == 62 && RF.Update_ == true
                                                        select RU.UserName
                                         where RoleName.Contains(usrs.UserName)

                                         select new user
                                         {
                                             fullName = usrs.FullName
                                         })

                                     }).Skip(pageIndex * pageSize).Take(pageSize).ToList();



            // table lay du lieu kieu khac
            int stt = 1;
            foreach (MonHocDS monhoc in rowschuyencanList)
            {
                string tengv = "";
                foreach (user tengiaovien in monhoc.tenGiaoVienLists)
                {
                    tengv += tengiaovien.fullName + ",";

                }
                if (tengv != null && tengv.Length > 0)
                {
                    tengv = tengv.Substring(0, tengv.Length - 1);
                }

                response.Records.Add(new JqGridRecord(monhoc.Mamonhoc, new List<object>()
                {
					stt,
                    monhoc.Mamonhoc,
                    monhoc.Tenmonhoc,
                    tengv
                     
                }));

                stt++;
            }


            //Return data as json
            return new JqGridJsonResult() { Data = response };
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Authorize]
        public ActionResult thongKeLopHocTheoHocKy(string hocky)
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Thongbao");
            }
            // lay thong tin truong 
            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            string namhoc = Cmon.GetNamHienTai(idTruong);
            // DateTime ngayhientai=DateTime.Now;
            //string kyhientai = Cmon.getHocKyHienTai(ngayhientai, idTruong, namhoc);
            //Tim ngay bat dau va ngay ket thuc trong 1 hoc ky
            DateTime? ngayBD = null;
            DateTime? ngayKT = null;
            var HKs = "HK1";// mac dinh lay gia tri la hoc ky 1
 
            if (hocky == "CN")
            {
                HKs = "CN";
                
                //db.DM_HocKy.Where(hk => hk.MaHocKy == hocky && hk.NamHoc == namhoc && hk.idTruong == idTruong).SingleOrDefault();
                ngayBD = db.DM_HocKy.Where(hk => hk.MaHocKy == "HK1" && hk.NamHoc == namhoc && hk.idTruong == idTruong).First().NgayBatDau;
                ngayKT = db.DM_HocKy.Where(hk => hk.MaHocKy == "HK2" && hk.NamHoc == namhoc && hk.idTruong == idTruong).First().NgayKetThuc;
            }
            else {
                var HK = db.DM_HocKy.Where(hk => hk.MaHocKy == hocky && hk.NamHoc == namhoc && hk.idTruong == idTruong).SingleOrDefault();
                HKs = HK.MaHocKy;
               // ViewBag.currenHocKy = HK;
                ngayBD = HK.NgayBatDau;
                ngayKT = HK.NgayKetThuc;
            }

            var q1 = (from lophoc in db.LopHocs
                      join userlh in
                          ((from l1 in db.Users_LopHoc where l1.UserName == userName && l1.idTruong == idTruong && l1.NamHoc == namhoc select l1.MaLop).Union
                          (from l2 in db.Users_Monhoc where l2.UserName == userName && l2.idTruong == idTruong && l2.NamHoc == namhoc select l2.MaLop))
                      on lophoc.MaLop equals userlh
                      where lophoc.idTruong == idTruong && lophoc.NamHoc == namhoc
                      orderby lophoc.MaLop

                      select new Viewthongkelophoc
                      {

                          tenGiaoVienLists = (
                                    from usrs in db.ScrUsers
                                    join ul in db.Users_LopHoc on usrs.UserName equals ul.UserName
                                    where ul.idTruong == idTruong && ul.NamHoc == namhoc && ul.MaLop == lophoc.MaLop && usrs.IsAdmin == false
                                    && (from RU in db.ScrJRoleUsers
                                        join RF in db.ScrJRoleFunctions
                                        on RU.RoleName equals RF.RoleName
                                        where RU.UserName == usrs.UserName && RF.idTruong == idTruong
                                            // && RU.RoleName=="GVCN" 
                                        && RF.Update_ == true
                                        && RF.Read_ == true
                                        && RF.Insert_ == true
                                        && RF.FunctionID == 118
                                        select RU.UserName).Contains(usrs.UserName)

                                    select new user
                                    {
                                        fullName = usrs.FullName
                                    }),



                          Tengiaovien = (from users in db.ScrUsers
                                         where users.idTruong == idTruong && users.UserName == userName
                                         join userslop in db.Users_LopHoc on users.UserName equals userslop.UserName
                                         where userslop.idTruong == idTruong && userslop.MaLop == lophoc.MaLop
                                         select users.FullName).FirstOrDefault()
                                           ,
                          maky = HKs,
                          makhoi = lophoc.MaKhoi,
                          malop = lophoc.MaLop,
                          tenLop = lophoc.TenLop,
                          siso = (from hs in db.HocSinhs.Where(hs => hs.idTruong == idTruong && hs.MaLop == lophoc.MaLop && hs.NamHoc == namhoc)
                                  where !(from hsqt in db.HocSinh_QuaTrinh.Where(hsqt => hsqt.idTruong == idTruong && hsqt.NamHoc == namhoc && hsqt.MaLop == lophoc.MaLop && hsqt.MaQuaTrinh == "BH")
                                          select hsqt.MaHocSinh).Contains(hs.MaHocSinh)
                                  select hs).Count(),

                          cophep = (from loainghihoc in db.HocSinh_NghiHoc
                                    where loainghihoc.MaLop == lophoc.MaLop && loainghihoc.NamHoc == namhoc && loainghihoc.idTruong == idTruong && lophoc.idTruong == idTruong &&
                                    (!ngayBD.HasValue || loainghihoc.Ngay >= ngayBD.Value) &&
                                    (!ngayKT.HasValue || loainghihoc.Ngay <= ngayKT.Value) &&
                                    lophoc.NamHoc == namhoc && (loainghihoc.LoaiNghi == "P" || loainghihoc.LoaiNghi == "CP")
                                    select loainghihoc).Count(),
                          khongphep = (from loainghihoc in db.HocSinh_NghiHoc
                                       where loainghihoc.MaLop == lophoc.MaLop && loainghihoc.NamHoc == namhoc && loainghihoc.idTruong == idTruong && lophoc.idTruong == idTruong &&
                                       (!ngayBD.HasValue || loainghihoc.Ngay >= ngayBD.Value) &&
                                       (!ngayKT.HasValue || loainghihoc.Ngay <= ngayKT.Value) &&
                                       lophoc.NamHoc == namhoc && (loainghihoc.LoaiNghi == "KP" || loainghihoc.LoaiNghi == "K")
                                       select loainghihoc).Count()


                      });


            DM_HocKy hkcn = new DM_HocKy();
            hkcn.MaHocKy = "CN";
            hkcn.TenHocKy = "Cả năm";
            var listhocky = db.DM_HocKy.Where(hk => hk.idTruong == idTruong && hk.NamHoc == namhoc).ToList();
            listhocky.Add(hkcn);

            ViewBag.currenHocKy = HKs;
            ViewBag.MaHocKy = new SelectList(listhocky, "MaHocKy", "TenHocKy", null);
            ViewBag.idtruong = idTruong;
            ViewBag.tentruong = Cmon.GetnameSchool(idTruong);
            ViewBag.danhsach = q1;
            ViewBag.namhientai = namhoc;
            ViewBag.tendangnhap = userName;
            return PartialView(@"~/Views/quanLyLopHoc/thongKeLopHocTheoHocKy.cshtml", q1);
            

        }
        private static string GetTenHocKy(string hocky)
        {
            string tenhocky = "";
            if (hocky == "K1" || hocky == "HK1")
            {
                tenhocky = "Học kỳ 1";
            }
            else if (hocky == "K2" || hocky == "HK2")
            {
                tenhocky = "Học kỳ 2";
            }
            else tenhocky = "Cả năm";
            return tenhocky;
        }

      
        [AcceptVerbs(HttpVerbs.Get)]
        [Authorize]
        public ActionResult Chitietlophoc(string malop)
        {

            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            string namhoc = Cmon.GetNamHienTai(idTruong);
            var danhsachhs = (from lisths in db.HocSinhs
                              where lisths.idTruong == idTruong && lisths.NamHoc == namhoc && lisths.MaLop == malop

                              select lisths).ToList();
            ViewBag.malop = malop;
            var lop = db.LopHocs.Where(l => l.MaLop == malop).SingleOrDefault();
            ViewBag.tenlop = lop.TenLop;
            ViewBag.makhoi = lop.MaKhoi;
            //   ViewBag.malop = danhsachhs.ma;
            ViewBag.tentruong = Cmon.GetnameSchool(idTruong);
            //  ViewBag.tenlop = Cmon.GetnameClass(malop);
            ViewBag.tendangnhap = Cmon.GetnameUserName(userName, idTruong);
            ViewBag.tongso = danhsachhs.Count();
            ViewBag.namhoc = namhoc;
            return View(danhsachhs);


        }


        [AcceptVerbs(HttpVerbs.Get)]
        [Authorize]
        public JsonResult LoadLophoc(string id)
        {
            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            string namhoc = Cmon.GetNamHienTai(idTruong);


            var modelData = (from lop in db.LopHocs
                             where lop.idTruong == idTruong && lop.NamHoc == namhoc && lop.MaKhoi == id
                             let allowlops =
                             (from userLop in db.Users_LopHoc where userLop.UserName == userName && userLop.NamHoc == namhoc && userLop.idTruong == idTruong select userLop.MaLop).
                             Union(from usermon in db.Users_Monhoc where usermon.idTruong == idTruong && usermon.NamHoc == namhoc && usermon.UserName == userName select usermon.MaLop)

                             where allowlops.Contains(lop.MaLop) && lop.MaKhoi == id && lop.NamHoc == namhoc
                             select new SelectListItem()
                             {
                                 Value = lop.MaLop,
                                 Text = lop.TenLop,

                             }).ToList();


            return Json(modelData, JsonRequestBehavior.AllowGet);

        }

        [Authorize]
        public ActionResult ExportDiemDanh(int idtruong, int thang, int nam, string namhoc, string malop)
        {

            int tongsongaytrongthang = Cmon.songaytrongthang(thang, nam);
            var listHS = (from hs in db.HocSinhs.Where(hs => hs.MaLop == malop && hs.idTruong == idtruong && hs.NamHoc == namhoc)
                          orderby hs.STT ascending
                          select hs).ToList();

            DateTime ngayBd = new DateTime(nam, thang, 1);
            DateTime ngayKt = new DateTime(nam, thang, tongsongaytrongthang);
            var nghiHocs = db.HocSinh_NghiHoc.Where(hs => hs.MaLop == malop &&
                                                    hs.idTruong == idtruong && hs.NamHoc == namhoc &&
                                                    ngayBd <= hs.Ngay && hs.Ngay <= ngayKt).ToList();

            int tongsohocsinh = listHS.Count;
            List<object> dblist = new List<object>();


            var objHs = new string[tongsongaytrongthang + 3];
            objHs[0] = "SỔ ĐIỂM DANH- Tháng " + thang + " Năm " + nam + " Lớp " + malop;
            dblist.Add(objHs);

            objHs = new string[tongsongaytrongthang + 3];
            string[] Thus = new string[] { "CN", "T2", "T3", "T4", "T5", "T6", "T7" };

            objHs[0] = "STT";
            objHs[1] = "Mã học sinh";
            objHs[2] = "Họ tên";
            int thuBD = (int)ngayBd.DayOfWeek;
            for (int j = 1; j <= tongsongaytrongthang; j++)
            {
                int dayOfWeek = (thuBD + j - 1) % 7;

                objHs[j + 2] = j.ToString() + "\n" + Thus[dayOfWeek];
            }
            dblist.Add(objHs);


            for (int i = 0; i < tongsohocsinh; i++)
            {
                objHs = new string[tongsongaytrongthang + 3];
                objHs[0] = (i + 1).ToString();
                objHs[1] = listHS[i].MaHocSinh;
                objHs[2] = listHS[i].HoTen;
                for (int j = 1; j <= tongsongaytrongthang; j++)
                {
                    var nghihoc = nghiHocs.Where(nh => nh.MaHocSinh == listHS[i].MaHocSinh && nh.Ngay.Day == j).SingleOrDefault();
                    if (nghihoc != null)
                        objHs[j + 2] = nghihoc.LoaiNghi;
                    else
                        objHs[j + 2] = "";
                }
                dblist.Add(objHs);
            }

            string path = ControllerContext.HttpContext.Server.MapPath("~/diemdanh.xls");

            var truong = db.DM_Truong.Where(t => t.idTruong == idtruong).SingleOrDefault();
            var lopHoc = db.LopHocs.Where(lop => lop.MaLop == malop && lop.idTruong == idtruong && lop.NamHoc == namhoc).SingleOrDefault();
            ExcelFileWriter<object> myExcel = new DiemdanhWrite();
            myExcel.ColumnCount = tongsongaytrongthang + 3;
           // myExcel.WriteDateToExcel(path, dblist, truong.TenTruong, lopHoc.TenLop);
            System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
            return File(path, "application/octet-stream", "DiemDanh.xls");
        }

      

        [AcceptVerbs(HttpVerbs.Get)]
        [Authorize]
        public ActionResult Diemdanhhocsinh()
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Thongbao");

            }
            // lay thong tin truong 
            string userName = User.Identity.Name.ToString() == "" ? "superadmin" : User.Identity.Name.ToString();
            ScrUser userchange = (from us in db.ScrUsers
                                  where us.UserName == userName
                                  select us).FirstOrDefault();
            int idTruong = userchange.idTruong != null ? (int)userchange.idTruong : 26;
            ViewBag.idtruong = idTruong;

            string namhoc = Cmon.GetNamHienTai(idTruong);
            var q1 = (from khoi in db.DM_Khoi
                      let makhoiList = from lophoc in db.LopHocs
                                       join userlh in db.Users_LopHoc on lophoc.MaLop equals userlh.MaLop
                                       where userlh.UserName == userName && userlh.idTruong == idTruong && userlh.NamHoc == namhoc
                                       select lophoc.MaKhoi
                      where makhoiList.Contains(khoi.MaKhoi) && khoi.idTruong == idTruong && khoi.NamHoc == namhoc
                      select khoi).ToList();

            ViewBag.danhsach = q1;
            ViewBag.namhientai = namhoc;
            ViewBag.namtruoc = namhoc.Substring(0, 4);
            ViewBag.namsau = namhoc.Substring(5, 4);

            ViewBag.MaKhoi = new SelectList(q1, "MaKhoi", "TenKhoi", null);
            ViewBag.MaLop = new SelectList(new List<LopHoc>(), "MaLop", "TenLop", null);

            return View();

        }





        [AcceptVerbs(HttpVerbs.Get)]
        [Authorize]
        public ActionResult thongkechuyencan(string malop)
        {

            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            string namhoc = Cmon.GetNamHienTai(idTruong);

            ViewBag.idtruong = idTruong;
            ViewBag.tenlop = Cmon.GetnameClass(malop, idTruong, namhoc);
            ViewBag.malop = malop;
            ViewBag.tendangnhap = Cmon.GetnameUserName(userName, idTruong);
            ViewBag.namhoc = namhoc;
            return View();
        }


        [AcceptVerbs(HttpVerbs.Post)]
        [Authorize]
        public ActionResult ViewDanhSachHocSinhChuyenCan(JqGridRequest request, string malop, string namhoc, int idtruong, string fromDate, string toDate)
        {

            int totalRecords = db.HocSinhs.Where(hs => hs.MaLop == malop && hs.idTruong == idtruong && hs.NamHoc == namhoc).Count();
            //Prepare JqGridData instance
            JqGridResponse response = new JqGridResponse()
            {
                //Total pages count
                TotalPagesCount = (int)Math.Ceiling((float)totalRecords / (float)request.RecordsCount),
                //Page number
                PageIndex = request.PageIndex,
                //Total records count
                TotalRecordsCount = totalRecords
            };


            var pageIndex = request.PageIndex;
            var pageSize = request.RecordsCount;
            var pagesCount = request.PagesCount.HasValue ? request.PagesCount.Value : 1;


            DateTime? ngayBD;
            DateTime? ngayKT;
            GetThongKeRange(namhoc, idtruong, fromDate, toDate, out ngayBD, out ngayKT);

            var listHS = 
                        (from hs in db.HocSinhs.Where(hs => hs.MaLop == malop && hs.idTruong == idtruong && hs.NamHoc == namhoc)
                          where !(from hsqt in db.HocSinh_QuaTrinh.Where(hsqt => hsqt.idTruong == idtruong && hsqt.NamHoc == namhoc && hsqt.MaLop == malop && hsqt.MaQuaTrinh == "BH")
                                  select hsqt.MaHocSinh).Contains(hs.MaHocSinh)   
                         orderby hs.STT ascending 
                          select new HocSinhChuyenCan
                          {
                              Stt = hs.STT,
                              MaHocSinh = hs.MaHocSinh,
                              NgaySinh = (DateTime)hs.NgaySinh,
                              GioiTinh = hs.GioiTinh,
                              HoTen = hs.HoTen,
                              Ngaycophep = (from loainghihoc in db.HocSinh_NghiHoc
                                            where loainghihoc.MaLop == malop &&
                                            (!ngayBD.HasValue || loainghihoc.Ngay >= (DateTime)ngayBD) &&
                                            (!ngayKT.HasValue || loainghihoc.Ngay <= (DateTime)ngayKT) &&
                                            (loainghihoc.LoaiNghi == "P" || loainghihoc.LoaiNghi == "CP") && loainghihoc.MaHocSinh == hs.MaHocSinh
                                            select loainghihoc).Count(),
                              Ngaykhongphep = (from loainghihoc in db.HocSinh_NghiHoc
                                               where loainghihoc.MaLop == malop &&
                                              (!ngayBD.HasValue || loainghihoc.Ngay >= (DateTime)ngayBD) &&
                                              (!ngayKT.HasValue || loainghihoc.Ngay <= (DateTime)ngayKT) &&
                                              (loainghihoc.LoaiNghi == "KP" || loainghihoc.LoaiNghi == "K") && loainghihoc.MaHocSinh == hs.MaHocSinh
                                               select loainghihoc).Count()

                          }
                          ).Skip(pageIndex * pageSize).Take(pagesCount * pageSize).ToList();


            foreach (HocSinhChuyenCan hocsinh in listHS)
            {
                response.Records.Add(new JqGridRecord(hocsinh.MaHocSinh, new List<object>()
                {
                 	hocsinh.Stt,
                    hocsinh.HoTen ,
                    hocsinh.NgaySinh ,
					hocsinh.GioiTinh ,
					hocsinh.Ngaycophep ,
                    hocsinh.Ngaykhongphep,
				
                }));
            }


            //Return data as json
            return new JqGridJsonResult() { Data = response };
        }

        private void GetThongKeRange(string namhoc, int idtruong, string fromDate, string toDate, out DateTime? ngayBD, out DateTime? ngayKT)
        {
            ngayBD = null;
            ngayKT = null;

            if (string.IsNullOrEmpty(fromDate) && string.IsNullOrEmpty(toDate))
            {
                //Tim ngay bat dau va ngay ket thuc trong 1 hoc ky
                DateTime ngayHH = DateTime.Now;
                var hocky = db.DM_HocKy.Where(hk => hk.NgayBatDau <= ngayHH && hk.NgayKetThuc >= ngayHH && hk.NamHoc == namhoc && hk.idTruong == idtruong).SingleOrDefault();
                if (hocky != null)
                {
                    ngayBD = hocky.NgayBatDau;
                    ngayKT = hocky.NgayKetThuc;
                }
                else { // van không thuộc học kỳ nào thì lấy ngày bắt đầu là ngày bắt đầu kỳ 1, và ngày kết thúc là ngày kết thúc kỳ 2

                    ngayBD = db.DM_HocKy.Where(hk =>hk.NamHoc == namhoc && hk.idTruong == idtruong && hk.MaHocKy=="HK1").SingleOrDefault().NgayBatDau;
                    ngayKT = db.DM_HocKy.Where(hk => hk.NamHoc == namhoc && hk.idTruong == idtruong && hk.MaHocKy == "HK2").SingleOrDefault().NgayKetThuc;
                }

            }
            else
            {
                if (!string.IsNullOrEmpty(fromDate))
                    ngayBD = DateTime.ParseExact(fromDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                if (!string.IsNullOrEmpty(toDate))
                    ngayKT = DateTime.ParseExact(toDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            }
        }


    }
}
