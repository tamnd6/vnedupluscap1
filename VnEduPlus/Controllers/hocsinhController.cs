﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using VnEduPlus.CmonFunction;
using VnEduPlus.Models;
using System.Web;
using VnEduPlus;
using Lib.Web.Mvc.JQuery.JqGrid;
using System.Globalization;
using System.Threading;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using VnEduPlus.Repositories;
using System.IO;

namespace VnEduPlus.Controllers
{
    public class hocsinhController : Controller
    {
        //
        // GET: /hocsinh/
        EDUNP_CAP1Entities db = new EDUNP_CAP1Entities();
        HocsinhListRepository _hocSinhListRePository;// khai bao bien interface
        public string getUserName()
        {
            return User.Identity.Name.ToString() == "" ? "superadmin" : User.Identity.Name.ToString();

        }


        [Authorize]
        public ActionResult Index(string malop)
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Thongbao");

            }
            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            string namhoc = Cmon.GetNamHienTai(idTruong);       
            


            var danhsachhs = (from hocsinh in db.HocSinhs
                        where hocsinh.MaLop == malop && hocsinh.idTruong == idTruong && hocsinh.NamHoc == namhoc
                        where !(from hsqt in db.HocSinh_QuaTrinh.Where(hsqt => hsqt.idTruong == idTruong && hsqt.NamHoc == namhoc && hsqt.MaLop == malop && hsqt.MaQuaTrinh == "BH")
                                select hsqt.MaHocSinh).Contains(hocsinh.MaHocSinh)
                        orderby hocsinh.STT
                        select new AllHocSinh
                        {
                            stt = hocsinh.STT,
                            MaHocSinh = hocsinh.MaHocSinh,
                            NgaySinh = (DateTime)hocsinh.NgaySinh,
                            HoTen = hocsinh.HoTen,
                            GioiTinh = hocsinh.GioiTinh,
                            Dantoc = (from dt in db.DM_DanToc
                                      where dt.MaDanToc == hocsinh.MaDanToc
                                      select dt).FirstOrDefault().TenDanToc,
                            diachihiennay = hocsinh.Diachilienlac,
                            hotenChavaNgheNghiep = hocsinh.HoTenCha + (hocsinh.NgheCha.Length > 0 ? (": " + hocsinh.NgheCha) : ""),
                            hotenMevaNgheNghiep = hocsinh.HoTenMe + (hocsinh.NgheMe.Length > 0 ? (": " + hocsinh.NgheMe) : ""),
                            conThuongBenhBinh = (hocsinh.ConLietSi == true ? "Con Liệt Sĩ," : " ") + (hocsinh.CoCongCM == true ? " Con cách mạng, " : "  ") +
                            (from cs in db.DM_ChinhSach
                             where cs.MaChinhSach == hocsinh.MaChinhSach
                             select cs).FirstOrDefault().TenChinhSach

                        }).ToList();

            



            //   ViewBag.malop = danhsachhs.ma;
            ViewBag.tentruong = Cmon.GetnameSchool(idTruong);
            //  ViewBag.tenlop = Cmon.GetnameClass(malop);
            ViewBag.tendangnhap = Cmon.GetnameUserName(userName,idTruong);
            ViewBag.idtruong = idTruong;
            ViewBag.makhoi = Cmon.getKhoi(malop, namhoc, idTruong);
            ViewBag.namhoc = namhoc;
            ViewBag.tongso = danhsachhs.Count();
            ViewBag.malop = malop;
            var lop = db.LopHocs.Where(l => l.MaLop == malop && l.idTruong == idTruong && l.NamHoc == namhoc).SingleOrDefault();

            ViewBag.tenlop = lop.TenLop;
            ViewBag.makhoi = lop.MaKhoi;
            ViewBag.PhongGiaoDuc = Cmon.getDonViChuQuan(idTruong);
            return View(@"~/Views/hocsinh/index.cshtml", danhsachhs);
        }

        [Authorize]
        public ActionResult studenstList()
        {
            return View();
        }

        //---------------------------------------------------------------------------------------------------
        //---------------------------------------------------------------------------------------------------
        public hocsinhController()
            : this(new MemoryHocsinhRespository())
        {
            CultureInfo ci = new CultureInfo("vi-VN");
            Thread.CurrentThread.CurrentCulture = ci;
        }

        public hocsinhController(HocsinhListRepository hocSinhListRepository)
        {
            _hocSinhListRePository = hocSinhListRepository;
        }

        public ActionResult ViewSearchHocSinh()
        {
            string userName = getUserName();

            int idTruong = Cmon.getIdtruong(userName, db);
            string namhoc = Cmon.GetNamHienTai(idTruong);
            ViewBag.namhientai = namhoc;
            ViewBag.tenuser = Cmon.GetnameUser(userName);
            ViewBag.idTruong = idTruong;
            ViewBag.hocKyMacDinh = new SelectList(Cmon.CreateHocKyTheoDanhGia(), "maHocKy", "tenHocKy", "GHK1");
            var listkhoi = (from khoi in db.DM_Khoi
                            let makhoiList = from lophoc in db.LopHocs
                                             join userlh in db.Users_LopHoc on lophoc.MaLop equals userlh.MaLop
                                             where userlh.UserName == userName && userlh.idTruong == idTruong && userlh.NamHoc == namhoc
                                             select lophoc.MaKhoi
                            where makhoiList.Contains(khoi.MaKhoi) && khoi.idTruong == idTruong && khoi.NamHoc == namhoc
                            select khoi).ToList();

            if (listkhoi.Count() == 0)
            {
                listkhoi = (from khoi in db.DM_Khoi
                            where khoi.idTruong == idTruong && khoi.NamHoc == namhoc
                            select khoi).ToList();

            }
            ViewBag.KhoiLop = new SelectList(listkhoi, "MaKhoi", "TenKhoi");

            var listDoiTuongChinhSach = (from doituong in db.DM_ChinhSach select doituong).ToList();
            ViewBag.ChinhSach = new SelectList(listDoiTuongChinhSach, "MaChinhSach", "TenChinhSach");

            string MaTinh = (from truong in db.DM_Truong where truong.idTruong == idTruong select truong.MaTinh).FirstOrDefault();
            string MaHuyen = (from truong in db.DM_Truong where truong.idTruong == idTruong select truong.MaHuyen).FirstOrDefault();

            var listQuanHuyen = (from quanhuyen in db.DM_QuanHuyen where quanhuyen.MaTinh == MaTinh select quanhuyen).ToList();
            ViewBag.QuanHuyen = new SelectList(listQuanHuyen, "MaHuyen", "TenHuyen", MaHuyen);

            return View(@"~/Views/hocsinh/ViewSearchHocSinh.cshtml");
        }
        
        public ActionResult ViewDanhSachHocSinhTheoLop()
        {
            string userName = getUserName();

            int idTruong = Cmon.getIdtruong(userName, db);
            string namhoc = Cmon.GetNamHienTai(idTruong);
            ViewBag.namhientai = namhoc;
            ViewBag.tenuser = Cmon.GetnameUser(userName);
            ViewBag.idTruong = idTruong;
            ViewBag.hocKyMacDinh = new SelectList(Cmon.CreateHocKyTheoDanhGia(), "maHocKy", "tenHocKy", "GHK1");
            var listkhoi = (from khoi in db.DM_Khoi
                            let makhoiList = from lophoc in db.LopHocs
                                             join userlh in db.Users_LopHoc on lophoc.MaLop equals userlh.MaLop
                                             where userlh.UserName == userName && userlh.idTruong == idTruong && userlh.NamHoc == namhoc
                                             select lophoc.MaKhoi
                            where makhoiList.Contains(khoi.MaKhoi) && khoi.idTruong == idTruong && khoi.NamHoc == namhoc
                            select khoi).ToList();

            if (listkhoi.Count() == 0)
            {
                listkhoi = (from khoi in db.DM_Khoi
                            where khoi.idTruong == idTruong && khoi.NamHoc == namhoc
                            select khoi).ToList();

            }
            ViewBag.KhoiLop = new SelectList(listkhoi, "MaKhoi", "TenKhoi");

            return View(@"~/Views/hocsinh/ViewDanhSachHocSinhTheoLop.cshtml");
        }

        public ActionResult ListDanhSachHocSinh(string HoTen, string NamSinh, string MaDoiTuong, string KhuyetTat, string MaQuanHuyen, string MaXaPhuong, string MaToDanPho)
        {
            string userName = getUserName();

            int idTruong = Cmon.getIdtruong(userName, db);
            string namhoc = Cmon.GetNamHienTai(idTruong);
            ViewBag.CapQuanLy = Cmon.getDonViChuQuan(idTruong);
            ViewBag.TenTruong = Cmon.GetnameSchool(idTruong);

            ViewBag.NamHoc = namhoc;
            ViewBag.TenKhoi = Session["TenKhoi"];
            DataTable dt = new DataTable();
            HNAFramework.DataAccess.Framework.DBHelper dbHelper = new HNAFramework.DataAccess.Framework.DBHelper();

            SqlParameter[] para = new SqlParameter[9];
            para[0] = new SqlParameter("@IdTruong", idTruong);
            para[1] = new SqlParameter("@HoTen", HoTen);
            para[2] = new SqlParameter("@NamSinh", NamSinh);
            para[3] = new SqlParameter("@MaDoiTuong", MaDoiTuong);
            para[4] = new SqlParameter("@KhuyetTat", KhuyetTat);
            para[5] = new SqlParameter("@MaQuanHuyen", MaQuanHuyen);
            para[6] = new SqlParameter("@MaXaPhuong", MaXaPhuong);
            para[7] = new SqlParameter("@MaToDanPho", MaToDanPho);
            para[8] = new SqlParameter("@NamHoc", namhoc);
            dt = dbHelper.GetDataTabel("spDanhSachHocSinh", para);
            return PartialView(@"~/Views/hocsinh/ListHocSinh.cshtml", dt);
        }

        public ActionResult DanhSachHocSinhTheoLop(string MaKhoi,string TenKhoi, string MaLop, string MoRong,string TenLop)
        {
            string userName = getUserName();

            int idTruong = Cmon.getIdtruong(userName, db);
            string namhoc = Cmon.GetNamHienTai(idTruong);
            ViewBag.CapQuanLy = Cmon.getDonViChuQuan(idTruong);
            ViewBag.TenTruong = Cmon.GetnameSchool(idTruong);
            
            ViewBag.NamHoc = namhoc;
            if (MaLop == "0")
                Session["TenKhoi"] = TenKhoi;
            else
                Session["TenKhoi"] = TenKhoi + " - Lớp " + TenLop;
            ViewBag.TenKhoi = Session["TenKhoi"];
            DataTable dt = new DataTable();
            HNAFramework.DataAccess.Framework.DBHelper dbHelper = new HNAFramework.DataAccess.Framework.DBHelper();

            SqlParameter[] para = new SqlParameter[4];
            para[0] = new SqlParameter("@IdTruong", idTruong);
            para[1] = new SqlParameter("@NamHoc", namhoc);
            para[2] = new SqlParameter("@MaKhoi", MaKhoi);
            para[3] = new SqlParameter("@MaLop", MaLop);
            dt = dbHelper.GetDataTabel("spDanhSachHocSinhTheoLop", para);
            Session["DanhSachHocSinhTheoLop"] = dt;
            if(MoRong=="0")
                return PartialView(@"~/Views/hocsinh/DanhSachHocSinhTheoLop.cshtml", dt);
            else
                return PartialView(@"~/Views/hocsinh/DanhSachHocSinhTheoLopMoRong.cshtml", dt);
        }

        public JsonResult DanhSachHocSinhTheoLopExcel()
        {
            string json = "NoData";
            string userName = getUserName();

            int idTruong = Cmon.getIdtruong(userName, db);
            string CapQuanLy = Cmon.getDonViChuQuan(idTruong);
            string TenTruong = Cmon.GetnameSchool(idTruong);
            string namhoc = Cmon.GetNamHienTai(idTruong);
            string file = "DANHSACHHOCSINHTHEOLOP.xls";
            string templateFile = Path.Combine(Server.MapPath("~/templateExcell"), file);
            FileInfo f = new FileInfo(templateFile);
            string newfile = "DANHSACHHOCSINHTHEOLOP_" + userName + "_" + DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + ".xls";

            if(Session["DanhSachHocSinhTheoLop"]!=null)
            {
                DataTable dt = (DataTable)Session["DanhSachHocSinhTheoLop"];

                string filename = Path.Combine(Server.MapPath("~/ProcessTemplate"), newfile);
                f.CopyTo(filename);

                Microsoft.Office.Interop.Excel.Application oXL = null;
                Microsoft.Office.Interop.Excel._Workbook oWB = null;
                Microsoft.Office.Interop.Excel._Worksheet oSheet = null;

                oXL = new Microsoft.Office.Interop.Excel.Application();
                oWB = oXL.Workbooks.Open(filename);
                oSheet = String.IsNullOrEmpty("DSHS") ? (Microsoft.Office.Interop.Excel._Worksheet)oWB.ActiveSheet : (Microsoft.Office.Interop.Excel._Worksheet)oWB.Worksheets["DSHS"];
                oSheet.Cells[1, 1] = CapQuanLy;
                oSheet.Cells[2, 1] = TenTruong;

                oSheet.Cells[4, 1] = "DANH SÁCH HỌC SINH NĂM HỌC " + namhoc;
                oSheet.Cells[5, 1] = Session["TenKhoi"];

                int TongLop = 0;
                int TongKhoi = 0;
                bool isCreateFoolter = false;
                int row = 7;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    TongLop++;
                    TongKhoi++;
                    int c = i + 1;
                    string LopCurrent = dt.Rows[i]["MaLop"].ToString();
                    string LopNext = "";
                    if (c == dt.Rows.Count)
                    {
                        LopNext = dt.Rows[0]["MaLop"].ToString();
                    }
                    else
                    {
                        LopNext = dt.Rows[c]["MaLop"].ToString();
                    }
                    //row = 8;
                    row = row + 1;

                    oSheet.Cells[row, 1] = dt.Rows[i]["STT"].ToString();
                    SetBorder(oSheet, row, 1, false, false);

                    oSheet.Cells[row, 2] = dt.Rows[i]["HoTen"].ToString();
                    SetBorder(oSheet, row, 2, false, false);

                    oSheet.Cells[row, 3] = "'"+dt.Rows[i]["NgaySinh"];
                    SetBorder(oSheet, row, 3, false, false);

                    oSheet.Cells[row, 4] = dt.Rows[i]["GioiTinh"].ToString();
                    SetBorder(oSheet, row, 4, false, false);

                    oSheet.Cells[row, 5] = dt.Rows[i]["Ghichu"].ToString();
                    SetBorder(oSheet, row, 5, true, false);
                    if (LopNext!=LopCurrent)
                    {
                        row = row + 1;
                        isCreateFoolter = true;
                        oSheet.Range[oSheet.Cells[row, 1], oSheet.Cells[row, 5]].Merge();
                        oSheet.Cells[row, 1] = "Lớp "+dt.Rows[i]["TenLop"].ToString()+" có "+ TongLop.ToString()+" học sinh";
                        oSheet.Cells[row, 1].Font.Bold = true;
                        SetBorderTong(oSheet, row, 1, false, true);
                        SetBorderTong(oSheet, row, 2, false, true);
                        SetBorderTong(oSheet, row, 3, false, true);
                        SetBorderTong(oSheet, row, 4, false, true);
                        SetBorderTong(oSheet, row, 5, false, true);
                        TongLop = 0;
                    }
                }
                if(!isCreateFoolter)
                {
                    row = row + 1;
                    isCreateFoolter = true;
                    oSheet.Range[oSheet.Cells[row, 1], oSheet.Cells[row, 5]].Merge();
                    oSheet.Cells[row, 1] = "Lớp " + dt.Rows[0]["TenLop"].ToString() + " có " + TongLop.ToString() + " học sinh";
                    oSheet.Cells[row, 1].Font.Bold = true;
                    SetBorderTong(oSheet, row, 1, false, true);
                    SetBorderTong(oSheet, row, 2, false, true);
                    SetBorderTong(oSheet, row, 3, false, true);
                    SetBorderTong(oSheet, row, 4, false, true);
                    SetBorderTong(oSheet, row, 5, true, true);
                }
                row = row + 1;
                isCreateFoolter = false;
                oSheet.Range[oSheet.Cells[row, 1], oSheet.Cells[row, 5]].Merge();
                oSheet.Cells[row, 1] = Session["TenKhoi"] + " có " + TongKhoi.ToString() + " học sinh";
                oSheet.Cells[row, 1].Font.Bold = true;
                SetBorderTong(oSheet, row, 1, false, true);
                SetBorderTong(oSheet, row, 2, false, true);
                SetBorderTong(oSheet, row, 3, false, true);
                SetBorderTong(oSheet, row, 4, false, true);
                SetBorderTong(oSheet, row, 5, true, true);

                oWB.Save();
                oWB.Close();

                json = newfile;
            }
            return Json(json, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DanhSachHocSinhTheoLopMoRongExcel()
        {
            string json = "NoData";
            string userName = getUserName();

            int idTruong = Cmon.getIdtruong(userName, db);
            string CapQuanLy = Cmon.getDonViChuQuan(idTruong);
            string TenTruong = Cmon.GetnameSchool(idTruong);
            string namhoc = Cmon.GetNamHienTai(idTruong);
            string file = "DANHSACHHOCSINHTHEOLOPMORONG.xls";
            string templateFile = Path.Combine(Server.MapPath("~/templateExcell"), file);
            FileInfo f = new FileInfo(templateFile);
            string newfile = "DANHSACHHOCSINHTHEOLOPMORONG_" + userName + "_" + DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + ".xls";
            if (Session["DanhSachHocSinhTheoLop"] != null)
            {
                DataTable dt = (DataTable)Session["DanhSachHocSinhTheoLop"];

                string filename = Path.Combine(Server.MapPath("~/ProcessTemplate"), newfile);
                f.CopyTo(filename);

                Microsoft.Office.Interop.Excel.Application oXL = null;
                Microsoft.Office.Interop.Excel._Workbook oWB = null;
                Microsoft.Office.Interop.Excel._Worksheet oSheet = null;

                oXL = new Microsoft.Office.Interop.Excel.Application();
                oWB = oXL.Workbooks.Open(filename);
                oSheet = String.IsNullOrEmpty("DSHS") ? (Microsoft.Office.Interop.Excel._Worksheet)oWB.ActiveSheet : (Microsoft.Office.Interop.Excel._Worksheet)oWB.Worksheets["DSHS"];
                oSheet.Cells[1, 1] = CapQuanLy;
                oSheet.Cells[2, 1] = TenTruong;

                oSheet.Cells[4, 1] = "DANH SÁCH HỌC SINH NĂM HỌC " + namhoc;
                oSheet.Cells[5, 1] = Session["TenKhoi"];

                int TongLop = 0;
                int TongKhoi = 0;
                bool isCreateFoolter = false;
                int row = 7;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    TongLop++;
                    TongKhoi++;
                    int c = i + 1;
                    string LopCurrent = dt.Rows[i]["MaLop"].ToString();
                    string LopNext = "";
                    if (c == dt.Rows.Count)
                    {
                        LopNext = dt.Rows[0]["MaLop"].ToString();
                    }
                    else
                    {
                        LopNext = dt.Rows[c]["MaLop"].ToString();
                    }
                    //row = 8;
                    row = row + 1;

                    oSheet.Cells[row, 1] = dt.Rows[i]["STT"].ToString();
                    SetBorder(oSheet, row, 1, false, false);

                    oSheet.Cells[row, 2] = dt.Rows[i]["HoTen"].ToString();
                    SetBorder(oSheet, row, 2, false, false);

                    oSheet.Cells[row, 3] = "'" + dt.Rows[i]["NgaySinh"];
                    SetBorder(oSheet, row, 3, false, false);

                    oSheet.Cells[row, 4] = dt.Rows[i]["GioiTinh"].ToString();
                    SetBorder(oSheet, row, 4, false, false);

                    oSheet.Cells[row, 5] = dt.Rows[i]["Diachilienlac"].ToString();
                    SetBorder(oSheet, row, 5, false, false);

                    oSheet.Cells[row, 6] = dt.Rows[i]["HoTenCha"].ToString();
                    SetBorder(oSheet, row, 6, false, false);

                    oSheet.Cells[row, 7] = dt.Rows[i]["NgheCha"].ToString();
                    SetBorder(oSheet, row, 7, false, false);

                    oSheet.Cells[row, 8] = dt.Rows[i]["HoTenMe"].ToString();
                    SetBorder(oSheet, row, 8, false, false);

                    oSheet.Cells[row, 9] = dt.Rows[i]["NgheMe"].ToString();
                    SetBorder(oSheet, row, 9, false, false);

                    oSheet.Cells[row, 10] = dt.Rows[i]["TenKP"].ToString();
                    SetBorder(oSheet, row, 10, false, false);

                    oSheet.Cells[row, 11] = dt.Rows[i]["TenXa"].ToString();
                    SetBorder(oSheet, row, 11, false, false);

                    oSheet.Cells[row, 12] = dt.Rows[i]["TenHuyen"].ToString();
                    SetBorder(oSheet, row, 12, false, false);

                    oSheet.Cells[row, 13] = dt.Rows[i]["DienThoai"].ToString();
                    SetBorder(oSheet, row, 13, false, false);

                    oSheet.Cells[row, 14] = dt.Rows[i]["TenChinhSach"].ToString();
                    SetBorder(oSheet, row, 14, true, false);

                    if (LopNext != LopCurrent)
                    {
                        row = row + 1;
                        isCreateFoolter = true;
                        oSheet.Range[oSheet.Cells[row, 1], oSheet.Cells[row, 14]].Merge();
                        oSheet.Cells[row, 1] = "Lớp " + dt.Rows[i]["TenLop"].ToString() + " có " + TongLop.ToString() + " học sinh";
                        oSheet.Cells[row, 1].Font.Bold = true;
                        for (int j = 1; j <= 13; j++)
                        {
                            SetBorderTong(oSheet, row, j, false, true);
                        }
                        SetBorderTong(oSheet, row, 14, true, true);
                        TongLop = 0;
                    }
                }

                if (!isCreateFoolter)
                {
                    row = row + 1;
                    isCreateFoolter = true;
                    oSheet.Range[oSheet.Cells[row, 1], oSheet.Cells[row, 14]].Merge();
                    oSheet.Cells[row, 1] = "Lớp " + dt.Rows[0]["TenLop"].ToString() + " có " + TongLop.ToString() + " học sinh";
                    oSheet.Cells[row, 1].Font.Bold = true;
                    for (int j = 1; j <= 13; j++)
                    {
                        SetBorderTong(oSheet, row, j, false, true);
                    }
                    SetBorderTong(oSheet, row, 14, true, true);
                }

                row = row + 1;
                isCreateFoolter = false;
                oSheet.Range[oSheet.Cells[row, 1], oSheet.Cells[row, 14]].Merge();
                oSheet.Cells[row, 1] = Session["TenKhoi"] + " có " + TongKhoi.ToString() + " học sinh";
                oSheet.Cells[row, 1].Font.Bold = true;
                for (int j = 1; j <= 13; j++)
                {
                    SetBorderTong(oSheet, row, j, false, true);
                }
                SetBorderTong(oSheet, row, 14, true, true);

                oWB.Save();
                oWB.Close();

                json = newfile;
            }
            return Json(json, JsonRequestBehavior.AllowGet);
        }

        private void SetBorderTong(Microsoft.Office.Interop.Excel._Worksheet worksheet, int row, int col, bool ColEnd, bool RowEnd)
        {
            worksheet.Cells[row, col].Borders[Excel.XlBordersIndex.xlEdgeLeft].Weight = 2d;
            if (ColEnd)
                worksheet.Cells[row, col].Borders[Excel.XlBordersIndex.xlEdgeRight].Weight = 2d;

            worksheet.Cells[row, col].Borders[Excel.XlBordersIndex.xlEdgeBottom].Weight = 1d;
            if (RowEnd)
                worksheet.Cells[row, col].Borders[Excel.XlBordersIndex.xlEdgeBottom].Weight = 2d;
            worksheet.Cells[row, col].Borders[Excel.XlBordersIndex.xlEdgeTop].Weight = 2d;
        }
        private void SetBorder(Microsoft.Office.Interop.Excel._Worksheet worksheet, int row, int col, bool ColEnd, bool RowEnd)
        {
            worksheet.Cells[row, col].Borders[Excel.XlBordersIndex.xlEdgeLeft].Weight = 2d;
            if (ColEnd)
                worksheet.Cells[row, col].Borders[Excel.XlBordersIndex.xlEdgeRight].Weight = 2d;

            worksheet.Cells[row, col].Borders[Excel.XlBordersIndex.xlEdgeBottom].Weight = 1d;
            if (RowEnd)
                worksheet.Cells[row, col].Borders[Excel.XlBordersIndex.xlEdgeBottom].Weight = 2d;
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Authorize]
        public ActionResult viewDanhSachHocSinh(JqGridRequest request, string malop,int idtruong, string namhoc)
        {
            var listDanhSachHocSinh = (from hs in db.HocSinhs.Where(hs => hs.idTruong == idtruong && hs.MaLop == malop && hs.NamHoc == namhoc)
                                    where !(from hsqt in db.HocSinh_QuaTrinh.Where(hsqt => hsqt.idTruong == idtruong && hsqt.NamHoc == namhoc && hsqt.MaLop == malop && hsqt.MaQuaTrinh == "BH")
                                            select hsqt.MaHocSinh).Contains(hs.MaHocSinh)
                                    orderby hs.STT ascending
                                    select new HocSinhDanhHieu
                                    {
                                        stt = hs.STT,
                                        MaHocSinh = hs.MaHocSinh,
                                        Hoten = hs.HoTen,
                                        Ngaysinh = hs.NgaySinh,

                                    }

                           ).ToList();
            int totalRecords = listDanhSachHocSinh.Count();
            //Fix for grouping, because it adds column name instead of index to SortingName
            string sortingName = "it." + request.SortingName;//.Replace("MaTinh","MaTinh");

            //Prepare JqGridData instance
            JqGridResponse response = new JqGridResponse()
            {
                //Total pages count
                TotalPagesCount = (int)Math.Ceiling((float)totalRecords / (float)request.RecordsCount),
                //Page number
                PageIndex = request.PageIndex,
                //Total records count
                TotalRecordsCount = totalRecords
            };

            // table lay du lieu kieu khac
            int stt = 0;
            foreach (AllHocSinh hocsinh in _hocSinhListRePository.getAllHocSinh(malop,idtruong,namhoc, sortingName, request.PageIndex, request.RecordsCount, request.PagesCount.HasValue ? request.PagesCount.Value : 1))
            {
                stt++;
                response.Records.Add(new JqGridRecord(hocsinh.MaHocSinh, new List<object>()
                {
					stt,
                   	hocsinh.HoTen ,
					hocsinh.NgaySinh ,
                    hocsinh.GioiTinh,
					hocsinh.Dantoc ,
                    hocsinh.diachihiennay,
                    hocsinh.conThuongBenhBinh,
                    hocsinh.hotenChavaNgheNghiep,
                    hocsinh.hotenMevaNgheNghiep, 
                      
                }));
            }


            //Return data as json
            return new JqGridJsonResult() { Data = response };
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Authorize]
        public ActionResult capNhatHocsinh(string mahocsinh, HocSinh viewModel)
        {
            HocSinh hocsinh = _hocSinhListRePository.getHocSinh(mahocsinh);

            hocsinh.HoTen = viewModel.HoTen;
            hocsinh.NgaySinh = viewModel.NgaySinh;
            hocsinh.GioiTinh = viewModel.GioiTinh;
            try
            {

                _hocSinhListRePository.SavingChanges();

            }
            catch
            {
                return Json(false);
            }

            return Json(true);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [Authorize]
        public ActionResult InsertHocSinh(HocSinh hocsinh)
        {
            return Json(_hocSinhListRePository.addHocSinh(hocsinh));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult deleteHocsinh(string id)
        {
            return Json(_hocSinhListRePository.delHocSinh(id));
        }


        //---------------------------------------------------------------------------------------------


        [AcceptVerbs(HttpVerbs.Get)]
        [Authorize]
        public ActionResult danhsachhocsinh(string malop, string makhoi)
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Thongbao");

            }

            // lay thong tin truong 
            string userName = User.Identity.Name.ToString() == "" ? "superadmin" : User.Identity.Name.ToString();
            ScrUser userchange = (from us in db.ScrUsers
                                  where us.UserName == userName
                                  select us).FirstOrDefault();
            int idTruong = userchange.idTruong != null ? (int)userchange.idTruong : 26;
            ViewBag.idtruong = idTruong;
            bool isHieuTruong = Cmon.isHieuTruong(idTruong, userName);
            ViewBag.isHieuTruong = isHieuTruong;
            ViewBag.loaiNhapDiem = Cmon.checkLoaiNhapDiemCuaTruong(idTruong);
            ViewBag.style = Cmon.checkFormNhapDiemCuaTruong(idTruong);
            string namhoc = Cmon.GetNamHienTai(idTruong);
            var q1 = (from khoi in db.DM_Khoi
                      let makhoiList = from lophoc in db.LopHocs
                                       join userlh in db.Users_LopHoc on lophoc.MaLop equals userlh.MaLop
                                       where userlh.UserName == userName && userlh.idTruong == idTruong && userlh.NamHoc == namhoc
                                       select lophoc.MaKhoi
                      where makhoiList.Contains(khoi.MaKhoi) && khoi.idTruong == idTruong && khoi.NamHoc == namhoc
                      select khoi).ToList();


            var lopHocPhuTrach = (from lh in db.LopHocs
                          let allowlops =
                          (from usermon in db.Users_Monhoc where usermon.UserName == userName && usermon.NamHoc == namhoc && usermon.idTruong == idTruong select usermon.MaLop).
                          Union(
                          from userLop in db.Users_LopHoc.Where(userLop => userLop.idTruong == idTruong && userLop.NamHoc == namhoc && userLop.UserName == userName)
                          select userLop.MaLop)
                          where allowlops.Contains(lh.MaLop) && lh.idTruong == idTruong && lh.NamHoc == namhoc
                          select new LopHocGiaoVien
                          {
                              MaLop = lh.MaLop,
                              TenLop = lh.TenLop,
                              IdTruong = lh.idTruong,
                              NamHoc = lh.NamHoc
                          }).Distinct().ToList();

            ViewBag.firstClass = "";
            if (lopHocPhuTrach != null && lopHocPhuTrach.Count > 0)
            {
                ViewBag.firstClass = lopHocPhuTrach[0].MaLop;
            }
            ViewBag.danhsach = q1;
            ViewBag.namhientai = namhoc;
            
            ViewBag.makhoi = new SelectList(q1, "MaKhoi", "TenKhoi", makhoi);
            if (string.IsNullOrEmpty(malop))
                ViewBag.malop = new SelectList(lopHocPhuTrach, "MaLop", "TenLop", null);
            else
            {
                var lops = (from lop in db.LopHocs
                            let allowlops = from userLop in db.Users_LopHoc where userLop.UserName == userName && userLop.NamHoc == namhoc select userLop.MaLop
                            where allowlops.Contains(lop.MaLop) && lop.MaKhoi == makhoi && lop.NamHoc == namhoc
                            select lop).ToList();
                ViewBag.malop = new SelectList(lops, "MaLop", "TenLop", malop);
            }
         
            return View();

        }


        [Authorize]
        public ActionResult SearchHocSinhTheoKhoangDiem()
        {
            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            string namhoc = Cmon.GetNamHienTai(idTruong);
            var listhocky = db.DM_HocKy.Where(hk => hk.idTruong == idTruong && hk.NamHoc == namhoc).ToList();
            ViewBag.idtruong = idTruong;
            ViewBag.tentruong = Cmon.GetnameSchool(idTruong);
            ViewBag.tendangnhap = Cmon.GetnameUserName(userName, idTruong);

            ViewBag.loaiNhapDiem = Cmon.checkLoaiNhapDiemCuaTruong(idTruong);
            ViewBag.ShowDTBM = Cmon.checkShowOrHiddenDTBMCuaTruong(idTruong);
            ViewBag.style = Cmon.checkShowOrHiddenDTBMCuaTruong(idTruong);

            ViewBag.MaHocKy = new SelectList(listhocky, "MaHocKy", "TenHocKy", null);
            var listkhoi = (from khoi in db.DM_Khoi
                            let makhoiList = from lophoc in db.LopHocs
                                             join userlh in db.Users_LopHoc on lophoc.MaLop equals userlh.MaLop
                                             where userlh.UserName == userName && userlh.idTruong == idTruong && userlh.NamHoc == namhoc
                                             select lophoc.MaKhoi
                            where makhoiList.Contains(khoi.MaKhoi) && khoi.idTruong == idTruong && khoi.NamHoc == namhoc
                            select khoi).ToList();

            ViewBag.makhoi = new SelectList(listkhoi, "MaKhoi", "TenKhoi");


            var listMonhoc = (from monhoc in db.DM_MonHoc
                              let allowlops = from usermon in db.Users_Monhoc.Where(usermon => usermon.UserName == userName && usermon.NamHoc == namhoc && usermon.idTruong == idTruong)
                                              join khoimon in db.Khoi_MonHoc.Where(khoimon => khoimon.idTruong == idTruong && khoimon.NamHoc == namhoc && khoimon.Tinhdiem == true)
                                               on usermon.MaMonHoc equals khoimon.MaMonHoc
                                              select usermon.MaMonHoc
                              where allowlops.Contains(monhoc.MaMonHoc) && monhoc.Active == true
                              select new SelectListItem()
                              {
                                  Value = monhoc.MaMonHoc,
                                  Text = monhoc.TenMonHoc,

                              }).ToList();
            return View();
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult loadMonHoc(string makhoi)
        {
            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            string namhoc = Cmon.GetNamHienTai(idTruong);

            var modelData = (from monhoc in db.DM_MonHoc
                             let allowlops = from usermon in db.Users_Monhoc.Where(usermon => usermon.UserName == userName && usermon.NamHoc == namhoc && usermon.idTruong == idTruong)
                                             join truongmon in db.ASSIGN_LOP_MonHoc.Where(tm=>tm.idTruong==idTruong && tm.NamHoc==namhoc && tm.Tinhdiem==true)
                                              on usermon.MaMonHoc equals truongmon.MaMonHoc
                                             select usermon.MaMonHoc
                             where allowlops.Contains(monhoc.MaMonHoc) && monhoc.Active == true
                             select new SelectListItem()
                             {
                                 Value = monhoc.MaMonHoc,
                                 Text = monhoc.TenMonHoc,

                             }).ToList();


            return Json(modelData, JsonRequestBehavior.AllowGet);

        }

        [Authorize]
        public ActionResult ViewHocSinhTheoKhoangDiem(string mamonhoc, string hocky, string khoilop, int diem_start, int diem_end, int socot)
        {
            string connectionStr = ConfigurationSettings.AppSettings["ConnectionStr"].ToString();
            DBHelper dbConnect = new DBHelper(connectionStr);
            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            string namhoc = Cmon.GetNamHienTai(idTruong);

            SqlParameter[] arParms = new SqlParameter[8];
            arParms[0] = new SqlParameter("@idtruong", SqlDbType.Int);
            arParms[0].Value = idTruong;

            arParms[1] = new SqlParameter("@namhoc", SqlDbType.VarChar);
            arParms[1].Value = namhoc;

            arParms[2] = new SqlParameter("@makhoi", SqlDbType.VarChar);
            arParms[2].Value = khoilop;

            arParms[3] = new SqlParameter("@hocky", SqlDbType.VarChar);
            arParms[3].Value = hocky;

            arParms[4] = new SqlParameter("@mamonhoc", SqlDbType.VarChar);
            arParms[4].Value = mamonhoc;

            arParms[5] = new SqlParameter("@startmath", SqlDbType.Int);
            arParms[5].Value = diem_start;

            arParms[6] = new SqlParameter("@endmath", SqlDbType.Int);
            arParms[6].Value = diem_end;

            arParms[7] = new SqlParameter("@value", SqlDbType.Int);
            arParms[7].Value = socot;

            DataSet ds = dbConnect.ExecuteDatasetPRO("spSearchHocSinhDiemOver", arParms);
            List<SearchHocSinhTheoKhoangDiem> lstSearch = new List<SearchHocSinhTheoKhoangDiem>();
            if (ds != null && ds.Tables != null)
            {

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    SearchHocSinhTheoKhoangDiem search = new SearchHocSinhTheoKhoangDiem();

                    search.HoTen = dr["HoTen"].ToString();
                    search.IdTruong = idTruong;
                    search.MaHocKy = dr["MaHocKy"].ToString();
                    search.MaHocSinh = dr["MaHocSinh"].ToString();
                    search.MaLop = dr["MaLop"].ToString();
                    search.MaMonHoc = dr["MaMonHoc"].ToString();
                    search.NamHoc = dr["NamHoc"].ToString();
                    search.TenLop = dr["TenLop"].ToString();

                    search.M1 = dr["M1"] == null ? "" : dr["M1"].ToString();
                    search.M2 = dr["M2"] == null ? "" : dr["M2"].ToString();
                    search.M3 = dr["M3"] == null ? "" : dr["M3"].ToString();
                    search.M4 = dr["M4"] == null ? "" : dr["M4"].ToString();
                    search.M5 = dr["M5"] == null ? "" : dr["M5"].ToString();

                    search.V15P1 = dr["V15P1"] == null ? "" : dr["V15P1"].ToString();
                    search.V15P2 = dr["V15P2"] == null ? "" : dr["V15P2"].ToString();
                    search.V15P3 = dr["V15P3"] == null ? "" : dr["V15P3"].ToString();
                    search.V15P4 = dr["V15P4"] == null ? "" : dr["V15P4"].ToString();
                    search.V15P5 = dr["V15P5"] == null ? "" : dr["V15P5"].ToString();

                    search.TH15P1 = dr["TH15P1"] == null ? "" : dr["TH15P1"].ToString();
                    search.TH15P2 = dr["TH15P2"] == null ? "" : dr["TH15P2"].ToString();
                    search.TH15P3 = dr["TH15P3"] == null ? "" : dr["TH15P3"].ToString();
                    search.TH15P4 = dr["TH15P4"] == null ? "" : dr["TH15P4"].ToString();
                    search.TH15P5 = dr["TH15P5"] == null ? "" : dr["TH15P5"].ToString();

                    search.V45P1 = dr["V45P1"] == null ? "" : dr["V45P1"].ToString();
                    search.V45P2 = dr["V45P2"] == null ? "" : dr["V45P2"].ToString();
                    search.V45P3 = dr["V45P3"] == null ? "" : dr["V45P3"].ToString();
                    search.V45P4 = dr["V45P4"] == null ? "" : dr["V45P4"].ToString();
                    search.V45P5 = dr["V45P5"] == null ? "" : dr["V45P5"].ToString();

                    search.TH45P1 = dr["TH45P1"] == null ? "" : dr["TH45P1"].ToString();
                    search.TH45P2 = dr["TH45P2"] == null ? "" : dr["TH45P2"].ToString();
                    search.TH45P3 = dr["TH45P3"] == null ? "" : dr["TH45P3"].ToString();
                    search.TH45P4 = dr["TH45P4"] == null ? "" : dr["TH45P4"].ToString();
                    search.TH45P5 = dr["TH45P5"] == null ? "" : dr["TH45P5"].ToString();

                    search.HK = dr["HK"] == null ? "" : dr["HK"].ToString();
                    search.TBM = dr["TBM"] == null ? "" : dr["TBM"].ToString();
                    lstSearch.Add(search);
                }
            }

            User_Profile data = (from user in db.User_Profile
                                 where user.UserName == userName
                                 select user).FirstOrDefault();
            if (data.ColDiemChiTietKy == 1)
                return PartialView("ViewHocSinhTheoKhoangDiemFull", lstSearch.ToList());
            else
                return PartialView("ViewHocSinhTheoKhoangDiemThuGon", lstSearch.ToList());
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Authorize]
        public ActionResult Diemdanhhocsinh()
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Thongbao");

            }
            // lay thong tin truong 
            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            ViewBag.idtruong = idTruong;
            string namhoc = Cmon.GetNamHienTai(idTruong);
            ViewBag.loaiNhapDiem = Cmon.checkLoaiNhapDiemCuaTruong(idTruong);
            ViewBag.style = Cmon.checkFormNhapDiemCuaTruong(idTruong);
            
            var update = (from RU in db.ScrJRoleUsers
                          join RF in db.ScrJRoleFunctions
                              on RU.RoleName equals RF.RoleName
                          where RU.UserName == userName && RF.idTruong == idTruong
                          && RF.FunctionID == 61
                          select RF
                        ).FirstOrDefault().Update_;

            int writerAccess = 1;

            if (update)
                writerAccess = 1;
            else
                writerAccess = 0;

            ViewBag.WriteAccess = writerAccess;

            var namHH = db.DM_NamHoc.Where(nam => nam.idTruong == idTruong && nam.NamHoc == namhoc && nam.IsHienTai == true).SingleOrDefault();

            var listkhoi = (from khoi in db.DM_Khoi
                            let makhoiList = from lophoc in db.LopHocs
                                             join userlh in db.Users_LopHoc on lophoc.MaLop equals userlh.MaLop
                                             where userlh.UserName == userName && userlh.idTruong == idTruong && userlh.NamHoc == namhoc
                                             select lophoc.MaKhoi
                            where makhoiList.Contains(khoi.MaKhoi) && khoi.idTruong == idTruong && khoi.NamHoc == namhoc
                            select khoi).ToList();

            User_Profile data = (from user in db.User_Profile
                                 where user.UserName == userName
                                 select user).FirstOrDefault();

            ViewBag.spyTye = data.SpyType;
            ViewBag.danhsach = listkhoi;
            ViewBag.namhientai = namhoc;
            ViewBag.namtruoc = namhoc.Substring(0, 4);
            ViewBag.namsau = namhoc.Substring(5, 4);
            ViewBag.ngayhientaihethong = System.DateTime.Now.ToString("dd/MM/yyyy");


            ViewBag.MaKhoi = new SelectList(listkhoi, "MaKhoi", "TenKhoi", null);
            ViewBag.MaLop = new SelectList(new List<LopHoc>(), "MaLop", "TenLop", null);

            DateTime ngayDauNam = Cmon.getNgayDauNamHoc(idTruong, namHH.NamHoc, db);//namHH.NgayBatDau.Value;

            var tuans = new List<SelectListItem>();
            for (int i = 1; i < 38; i = i + 4)
            {

                tuans.Add(new SelectListItem() { Value = ngayDauNam.AddDays((i - 1) * 7).ToString("dd/MM/yyyy"), Text = i.ToString() + " - " + (i + 3).ToString() });
            }
            ViewBag.MaTuan = new SelectList(tuans, "Value", "Text");



            DateTime ngayCuoiNam = Cmon.getNgayKetThucNamHoc(idTruong, namHH.NamHoc, db);
            int fromMonth = ngayDauNam.Month;// lay thang dau tien cua nam hoc
            int namHK1 = ngayDauNam.Year;
            int endMonth = ngayCuoiNam.Month;
            int namHK2 = ngayCuoiNam.Year;

            var thangs = new List<SelectListItem>();// cua nam o HK 1

            for (int i = fromMonth; i <= 12; i++)
            {
                if (i < 10)
                {
                    thangs.Add(new SelectListItem() { Value = "01" + "/" + "0" + i.ToString() + "/" + namHK1, Text = "Tháng " + i.ToString() });
                }
                else
                {
                    thangs.Add(new SelectListItem() { Value = "01" + "/" + i.ToString() + "/" + namHK1, Text = "Tháng " + i.ToString() });
                }
            }
            for (int i = 1; i <= endMonth; i++)
            {
                thangs.Add(new SelectListItem() { Value = "01" + "/" + "0" + i.ToString() + "/" + namHK2, Text = "Tháng " + i.ToString() });
            }

            ViewBag.mathang = new SelectList(thangs, "Value", "Text");

            return View();

        }

       /* [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ViewDiemdanhhocsinh(string strFromDate, string strToDate, string malop, int idTruong, string namhoc)
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Thongbao");

            }

            DateTime ngayBd = DateTime.ParseExact(strFromDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            DateTime ngayKt = DateTime.ParseExact(strToDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            var thang = ngayBd.Month;
            var nam = ngayBd.Year;
            int tongsongaytrongthang = Cmon.songaytrongthang(thang, nam);
            var listHS = (from hs in db.HocSinhs.Where(hs => hs.MaLop == malop && hs.idTruong == idTruong && hs.NamHoc == namhoc)
                          orderby hs.STT ascending
                          select hs).ToList();


            var nghiHocs = db.HocSinh_NghiHoc.Where(hs => hs.MaLop == malop &&
                                                    hs.idTruong == idTruong && hs.NamHoc == namhoc &&
                                                    ngayBd <= hs.Ngay && hs.Ngay <= ngayKt).ToList();
            ViewBag.ThuBD = (int)ngayBd.DayOfWeek;

            ViewBag.tongsohocsinh = listHS.Count();
            ViewBag.thang = thang;
            ViewBag.nam = nam;
            ViewBag.ngayBd = ngayBd;
            ViewBag.dayStart = ngayBd.Day;
            ViewBag.songay = tongsongaytrongthang;

            var model = new DiemdanhModel();
            model.ListHS = listHS;
            model.ListNghiHoc = nghiHocs;
            return PartialView("ViewDiemdanhhocsinh", model);

        }
        */

        [AcceptVerbs(HttpVerbs.Get)]
        [Authorize]
        public ActionResult ViewDiemdanhhocsinh(string strFromDate, string strToDate, string malop, int idTruong, string namhoc, string typeDiemDanh)
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Thongbao");

            }

            DateTime ngayBd = DateTime.ParseExact(strFromDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            DateTime ngayKt = DateTime.ParseExact(strToDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            var thang = ngayBd.Month;
            var nam = ngayBd.Year;
            int tongsongaytrongthang = Cmon.songaytrongthang(thang, nam);
            var listHS = (from hs in db.HocSinhs.Where(hs => hs.MaLop == malop && hs.idTruong == idTruong && hs.NamHoc == namhoc)
                          where !(from hsqt in db.HocSinh_QuaTrinh.Where(hsqt => hsqt.idTruong == idTruong && hsqt.NamHoc == namhoc && hsqt.MaLop == malop && hsqt.MaQuaTrinh == "BH")
                                  select hsqt.MaHocSinh).Contains(hs.MaHocSinh)  
                          orderby hs.STT ascending
                          select hs).ToList();


            var nghiHocs = db.HocSinh_NghiHoc.Where(hs => hs.MaLop == malop &&
                                                    hs.idTruong == idTruong && hs.NamHoc == namhoc &&
                                                    ngayBd <= hs.Ngay && hs.Ngay <= ngayKt).ToList();
            ViewBag.ThuBD = (int)ngayBd.DayOfWeek;

            ViewBag.tongsohocsinh = listHS.Count();
            ViewBag.thang = thang;
            ViewBag.nam = nam;
            ViewBag.ngayBd = ngayBd;
            ViewBag.dayStart = ngayBd.Day;
            ViewBag.songay = tongsongaytrongthang;

            var model = new DiemdanhModel();
            model.ListHS = listHS;
            model.ListNghiHoc = nghiHocs;
            if (typeDiemDanh.Equals("1"))
            {
                return PartialView("ViewDiemdanhhocsinh", model);
            }

            return PartialView("ViewDiemDanhHocSinhTheoThang", model);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Authorize]
        public ActionResult ViewDiemDanhHocSinhTheoThang(string strFromDate, string strToDate, string malop, int idTruong, string namhoc)
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Thongbao");

            }

            DateTime ngayBd = DateTime.ParseExact(strFromDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            DateTime ngayKt = DateTime.ParseExact(strToDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            var thang = ngayBd.Month;

            var nam = ngayBd.Year;
            int tongsongaytrongthang = Cmon.songaytrongthang(thang, nam);
            var listHS = (from hs in db.HocSinhs.Where(hs => hs.MaLop == malop && hs.idTruong == idTruong && hs.NamHoc == namhoc)
                          where !(from hsqt in db.HocSinh_QuaTrinh.Where(hsqt => hsqt.idTruong == idTruong && hsqt.NamHoc == namhoc && hsqt.MaLop == malop && hsqt.MaQuaTrinh == "BH")
                                  select hsqt.MaHocSinh).Contains(hs.MaHocSinh)   
                          orderby hs.STT ascending
                          select hs).ToList();


            var nghiHocs = db.HocSinh_NghiHoc.Where(hs => hs.MaLop == malop &&
                                                    hs.idTruong == idTruong && hs.NamHoc == namhoc &&
                                                    ngayBd <= hs.Ngay && hs.Ngay <= ngayKt).ToList();

            ViewBag.ThuBD = (int)ngayBd.DayOfWeek;

            ViewBag.tongsohocsinh = listHS.Count();
            ViewBag.thang = thang;
            ViewBag.nam = nam;
            ViewBag.ngayBd = ngayBd;
            ViewBag.dayStart = ngayBd.Day;
            ViewBag.songay = tongsongaytrongthang;

            var model = new DiemdanhModel();
            model.ListHS = listHS;
            model.ListNghiHoc = nghiHocs;
            return PartialView("ViewDiemdanhhocsinh", model);

        }




        [Authorize]
        public ActionResult ExportDiemDanhTheoTuan(int idtruong, string strFromDate, string strToDate, string namhoc, string malop)
        {
            DateTime ngayBd = DateTime.ParseExact(strFromDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            DateTime ngayKt = DateTime.ParseExact(strToDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
          

            int tongsongaytrongthang = 28;//Cmon.songaytrongthang(thang, nam);
            var listHS = (from hs in db.HocSinhs.Where(hs => hs.MaLop == malop && hs.idTruong == idtruong && hs.NamHoc == namhoc)
                          where !(from hsqt in db.HocSinh_QuaTrinh.Where(hsqt => hsqt.idTruong == idtruong && hsqt.NamHoc == namhoc && hsqt.MaLop == malop && hsqt.MaQuaTrinh == "BH")
                                  select hsqt.MaHocSinh).Contains(hs.MaHocSinh)  
                          orderby hs.STT ascending
                          select hs).ToList();

            var nghiHocs = db.HocSinh_NghiHoc.Where(hs => hs.MaLop == malop &&
                                                    hs.idTruong == idtruong && hs.NamHoc == namhoc &&
                                                    ngayBd <= hs.Ngay && hs.Ngay <= ngayKt).ToList();

            int tongsohocsinh = listHS.Count;
            List<object> dblist = new List<object>();


            var objHs = new string[tongsongaytrongthang + 3];
            objHs[0] = "SỔ ĐIỂM DANH- Từ ngày " + strFromDate + " đến ngày " + strToDate + " Lớp " + malop;
            dblist.Add(objHs);

            objHs = new string[tongsongaytrongthang + 3];
            string[] Thus = new string[] { "CN", "T2", "T3", "T4", "T5", "T6", "T7" };

            objHs[0] = "STT";
            objHs[1] = "Mã học sinh";
            objHs[2] = "Họ tên";
            int thuBD = (int)ngayBd.DayOfWeek;
            for (int j = 1; j <= tongsongaytrongthang; j++)
            {
                int dayOfWeek = (thuBD + j - 1) % 7;

                objHs[j + 2] = j.ToString() + "\n" + Thus[dayOfWeek];
            }
            dblist.Add(objHs);


            for (int i = 0; i < tongsohocsinh; i++)
            {
                objHs = new string[tongsongaytrongthang + 3];
                objHs[0] = (i + 1).ToString();
                objHs[1] = listHS[i].MaHocSinh;
                objHs[2] = listHS[i].HoTen;
                DateTime  ngay= ngayBd;
                for (int j = 0; j < tongsongaytrongthang; j++)
                {

                    var nghihoc = nghiHocs.Where(nh => nh.MaHocSinh == listHS[i].MaHocSinh && nh.Ngay == ngay).SingleOrDefault();
                    if (nghihoc != null)
                        objHs[j + 2] = nghihoc.LoaiNghi;
                    else
                        objHs[j + 2] = "";
                    ngay = ngay.AddDays(1);
                }
                dblist.Add(objHs);
            }

            string path = ControllerContext.HttpContext.Server.MapPath("~/diemdanh.xls");

            var truong = db.DM_Truong.Where(t => t.idTruong == idtruong).SingleOrDefault();
            var lopHoc = db.LopHocs.Where(lop => lop.MaLop == malop && lop.idTruong == idtruong && lop.NamHoc == namhoc).SingleOrDefault();
            ExcelFileWriter<object> myExcel = new DiemdanhWrite();
            myExcel.ColumnCount = tongsongaytrongthang + 3;
           // myExcel.WriteDateToExcel(path, dblist, truong.TenTruong, lopHoc.TenLop);
            System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
            return File(path, "application/octet-stream", "DiemDanh.xls");
        }


        [Authorize]
        public ActionResult ExportDiemDanhTheoThang(int idtruong, string strFromDate, string strToDate,int countDayOfMonth, string namhoc, string malop)
        {
            DateTime ngayBd = DateTime.ParseExact(strFromDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            DateTime ngayKt = DateTime.ParseExact(strToDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);


            int tongsongaytrongthang = countDayOfMonth;//Cmon.songaytrongthang(thang, nam);            

            var listHS = (from hs in db.HocSinhs.Where(hs => hs.MaLop == malop && hs.idTruong == idtruong && hs.NamHoc == namhoc)
                           where !(from hsqt in db.HocSinh_QuaTrinh.Where(hsqt => hsqt.idTruong == idtruong && hsqt.NamHoc == namhoc && hsqt.MaLop == malop && hsqt.MaQuaTrinh == "BH")
                                 select hsqt.MaHocSinh).Contains(hs.MaHocSinh)
                          orderby hs.STT ascending
                          select hs).ToList();


            var nghiHocs = db.HocSinh_NghiHoc.Where(hs => hs.MaLop == malop &&
                                                    hs.idTruong == idtruong && hs.NamHoc == namhoc).ToList();

            int tongsohocsinh = listHS.Count;
            List<object> dblist = new List<object>();


            var objHs = new string[tongsongaytrongthang + 3];
            objHs[0] = "SỔ ĐIỂM DANH- Từ ngày " + strFromDate + " đến ngày " + strToDate + " Lớp " + Cmon.GetnameClass(malop,idtruong,namhoc);
            dblist.Add(objHs);

            objHs = new string[tongsongaytrongthang + 3];
            string[] Thus = new string[] { "CN", "T2", "T3", "T4", "T5", "T6", "T7" };

            objHs[0] = "STT";
            objHs[1] = "Mã học sinh";
            objHs[2] = "Họ tên";
            int thuBD = (int)ngayBd.DayOfWeek;
            for (int j = 1; j <= tongsongaytrongthang; j++)
            {
                int dayOfWeek = (thuBD + j - 1) % 7;

                objHs[j + 2] = j.ToString() + "\n" + Thus[dayOfWeek];
            }
            dblist.Add(objHs);


            for (int i = 0; i < tongsohocsinh; i++)
            {
                objHs = new string[tongsongaytrongthang + 3];
                objHs[0] = (i + 1).ToString();
                objHs[1] = listHS[i].MaHocSinh;
                objHs[2] = listHS[i].HoTen;
                DateTime ngay = ngayBd;
                for (int j = 0; j < tongsongaytrongthang; j++)
                {

                   // var nghihoc = db.HocSinh_NghiHoc.Where(hs => hs.MaLop == malop &&
                    //                                hs.idTruong == idtruong && hs.NamHoc == namhoc && hs.MaHocSinh == listHS[i].MaHocSinh && hs.Ngay == ngay).FirstOrDefault();
                    var nghihoc = nghiHocs.Where(nh => nh.MaHocSinh == listHS[i].MaHocSinh && nh.Ngay == ngay).SingleOrDefault();
                    if (nghihoc != null)
                        objHs[j + 3] = nghihoc.LoaiNghi;
                    else
                        objHs[j + 3] = "";
                    ngay = ngay.AddDays(1);
                }
                dblist.Add(objHs);
            }

            string path = ControllerContext.HttpContext.Server.MapPath("~/diemdanh.xls");

            var truong = db.DM_Truong.Where(t => t.idTruong == idtruong).SingleOrDefault();
            var lopHoc = db.LopHocs.Where(lop => lop.MaLop == malop && lop.idTruong == idtruong && lop.NamHoc == namhoc).SingleOrDefault();
            ExcelFileWriter<object> myExcel = new DiemdanhWrite();
            myExcel.ColumnCount = tongsongaytrongthang + 3;
           // myExcel.WriteDateToExcel(path, dblist, truong.TenTruong, lopHoc.TenLop);
            System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
            return File(path, "application/octet-stream", "DiemDanh.xls");
        }


      /*  [HttpPost]
        public ActionResult SaveDiemDanh(int idtruong, string namHoc, string malop,
                                         string[] MaHocSinhDeleteds, string[] NgayDeleteds,
                                         string[] MaHocSinhs, string[] Ngays, string[] LoaiNghis, string[] LyDoNghis)
        {

            //delete dữ liệu
            if (MaHocSinhDeleteds != null)
            {
                for (int i = 0; i < MaHocSinhDeleteds.Length; i++)
                {
                    string maHs = MaHocSinhDeleteds[i];
                    if (!string.IsNullOrEmpty(maHs) && !string.IsNullOrEmpty(NgayDeleteds[i]))
                    {
                        DateTime ngayNghi = DateTime.ParseExact(NgayDeleteds[i], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        HocSinh_NghiHoc data = (from dd in db.HocSinh_NghiHoc
                                                where dd.idTruong == idtruong &&
                                                 dd.NamHoc == namHoc &&
                                                  dd.MaLop == malop &&
                                                   dd.MaHocSinh == maHs &&
                                                   dd.Ngay == ngayNghi
                                                select dd).FirstOrDefault();
                        if (data != null)
                            db.DeleteObject(data);
                    }

                }
            }


            if (MaHocSinhs != null)
            {
                var hocsinhs = db.HocSinhs.Where(hs => MaHocSinhs.Contains(hs.MaHocSinh)).ToList();
                var hockys = db.DM_HocKy.Where(hk => hk.NamHoc == namHoc && hk.idTruong == idtruong).ToList();

                for (int i = 0; i < MaHocSinhs.Length; i++)
                {
                    string maHs = MaHocSinhs[i];
                    if (!string.IsNullOrEmpty(maHs) && !string.IsNullOrEmpty(Ngays[i]))
                    {
                        DateTime NgayNghi = DateTime.ParseExact(Ngays[i], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        var hocky = hockys.Where(hk => hk.NgayBatDau <= NgayNghi && NgayNghi <= hk.NgayKetThuc).SingleOrDefault();
                        if (hocky != null)
                        {
                            string mahocky = hocky.MaHocKy;
                            string ghichu =string.Empty;
                            if (LyDoNghis != null && LyDoNghis[i] != null)
                            {
                                ghichu = LyDoNghis[i];
                            }

                            HocSinh_NghiHoc data = (from dd in db.HocSinh_NghiHoc
                                                    where dd.idTruong == idtruong &&
                                                     dd.NamHoc == namHoc &&
                                                      dd.MaHocKy == mahocky &&
                                                      dd.MaLop == malop &&
                                                       dd.MaHocSinh == maHs &&
                                                       dd.Ngay == NgayNghi
                                                    select dd).FirstOrDefault();
                            if (data == null)
                            {
                                db.AddToHocSinh_NghiHoc(
                                    new HocSinh_NghiHoc
                                    {
                                        idTruong = idtruong,
                                        NamHoc = namHoc,
                                        MaHocKy = mahocky,
                                        MaLop = malop,
                                        MaHocSinh = maHs,
                                        MaHocSinhFriendly = maHs,
                                        HoTen = hocsinhs.Where(hs => hs.MaHocSinh == maHs).SingleOrDefault().HoTen,
                                        Ngay = NgayNghi,
                                        LoaiNghi = LoaiNghis[i],
                                        Ghichu = ghichu
                                    });
                            }
                            else
                            {
                                data.LoaiNghi = LoaiNghis[i];
                                data.Ghichu = ghichu;
                            }
                        }
                    }

                }
            }

            db.SaveChanges();
            return Json(new { success = true });
        }
        */

        [HttpPost]
        [Authorize]
        public ActionResult SaveDiemDanh(int idtruong, string namHoc, string malop,                                         
                                         string[] MaHocSinhs, string[] Ngays, string[] LoaiNghis, string[] LyDoNghis)
        {
             

            if (MaHocSinhs != null)
            {
                var hocsinhs = db.HocSinhs.Where(hs => MaHocSinhs.Contains(hs.MaHocSinh)).ToList();
                var hockys = db.DM_HocKy.Where(hk => hk.NamHoc == namHoc && hk.idTruong == idtruong).ToList();

                for (int i = 0; i < MaHocSinhs.Length; i++)
                {
                    string maHs = MaHocSinhs[i];
                    if (!string.IsNullOrEmpty(maHs) && !string.IsNullOrEmpty(Ngays[i]))
                    {
                        DateTime NgayNghi = DateTime.ParseExact(Ngays[i], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        var hocky = hockys.Where(hk => hk.NgayBatDau <= NgayNghi && NgayNghi <= hk.NgayKetThuc).FirstOrDefault();
                        if (hocky != null)
                        {
                            string mahocky = hocky.MaHocKy;
                            string ghichu = string.Empty; //ly do ở đây chờ có giao diện làm tiếp
                           // if (!string.IsNullOrEmpty(LyDoNghis) && !string.IsNullOrEmpty(LyDoNghis[i]))
                           // {
                           //     ghichu = LyDoNghis[i];
                           // }

                            HocSinh_NghiHoc data = (from dd in db.HocSinh_NghiHoc
                                                    where dd.idTruong == idtruong &&
                                                     dd.NamHoc == namHoc &&
                                                      dd.MaHocKy == mahocky &&
                                                      dd.MaLop == malop &&
                                                       dd.MaHocSinh == maHs &&
                                                       dd.Ngay == NgayNghi
                                                    select dd).FirstOrDefault();
                            if (data == null)
                            {
                                db.AddToHocSinh_NghiHoc(
                                    new HocSinh_NghiHoc
                                    {
                                        idTruong = idtruong,
                                        NamHoc = namHoc,
                                        MaHocKy = mahocky,
                                        MaLop = malop,
                                        MaHocSinh = maHs,
                                        MaHocSinhFriendly = maHs,
                                        HoTen = hocsinhs.Where(hs => hs.MaHocSinh == maHs && hs.NamHoc == namHoc).SingleOrDefault().HoTen,
                                        Ngay = NgayNghi,
                                        LoaiNghi = LoaiNghis[i],
                                        Ghichu = ghichu
                                    });
                            }
                            else
                            {
                                data.LoaiNghi = LoaiNghis[i];
                                data.Ghichu = ghichu;
                            }
                            db.SaveChanges();
                        }
                    }

                }
            }

           
            return Json(new { success = true });
        }


        [HttpPost]
        [Authorize]
        public ActionResult Save(VIEWALSTUDENTINFOMATION model)
        {
            model.hocSinh.MaHocSinhFriendly = model.hocSinh.MaHocSinh;


            HocSinh HocsinhEnties = (from hocsinh in db.HocSinhs
                                     where hocsinh.idTruong == model.hocSinh.idTruong && hocsinh.MaLop == model.hocSinh.MaLop && hocsinh.MaHocSinh == model.hocSinh.MaHocSinh
                                     select hocsinh).FirstOrDefault();
            HocSinh_Details hschitiet = (from hsdt in db.HocSinh_Details
                                    where hsdt.idTruong == model.hocSinh.idTruong && hsdt.MaLop == model.hocSinh.MaLop
                                    && hsdt.NamHoc == model.hocSinh.NamHoc && hsdt.MaHocSinh == model.hocSinh.MaHocSinh
                                    select hsdt).FirstOrDefault();


            if (model.makhuyettat != null)
            {
                model.hocSinh.KhuyetTat = true;
                if (hschitiet != null)
                {
                    hschitiet.MaKhuyetTat = model.makhuyettat;
                    hschitiet.CoHoSoKhuyetTat = model.coHoSoKhuyetTat;
                    db.SaveChanges();
                }
                else
                {
                    HocSinh_Details hsdt = new HocSinh_Details();
                    hsdt.idTruong = model.hocSinh.idTruong;
                    hsdt.NamHoc = model.hocSinh.NamHoc;
                    hsdt.MaLop = model.hocSinh.MaLop;
                    hsdt.MaHocSinh = model.hocSinh.MaHocSinh;
                    hsdt.MaKhuyetTat = model.makhuyettat;
                    hsdt.CoHoSoKhuyetTat = model.coHoSoKhuyetTat;
                    db.AddToHocSinh_Details(hsdt);
                    db.SaveChanges();

                }
            }
            else {
                model.hocSinh.KhuyetTat = false;
                //tim kiem va xoa hoc sinh trong bang hoc sinh detail
                var hocsinhdetail = (from hocsinh in db.HocSinh_Details
                                     where hocsinh.idTruong == model.hocSinh.idTruong && hocsinh.NamHoc == model.hocSinh.NamHoc
                                         && hocsinh.MaHocSinh == model.hocSinh.MaHocSinh
                                     select hocsinh).FirstOrDefault();
                if (hocsinhdetail != null)
                {
                    db.HocSinh_Details.DeleteObject(db.HocSinh_Details.First(p => p.idTruong == model.hocSinh.idTruong && p.NamHoc == model.hocSinh.NamHoc
                    && p.MaLop == model.hocSinh.MaLop && p.MaHocSinh == model.hocSinh.MaHocSinh));
                    db.SaveChanges();
                }
                

                
            }
            

            if (HocsinhEnties != null)
            {
                //copy data from model to HocsinhEnties (ngoai tru truong khoa)
                HocsinhEnties = Cmon.CopyEntity(HocsinhEnties, model.hocSinh, false);
                db.SaveChanges();
                // Cập nhật lại thông tin họ tên cho học sinh ở bảng hocsinh_diemnhanxet

                var hocSinhDiemNX = (from hsnx in db.HocSinh_Diem_NhanXet
                                     where hsnx.idTruong == HocsinhEnties.idTruong && hsnx.NamHoc == HocsinhEnties.NamHoc && hsnx.MaLop == HocsinhEnties.MaLop
                                         && hsnx.MaHocSinh == HocsinhEnties.MaHocSinh
                                     select hsnx).FirstOrDefault();
                if (hocSinhDiemNX != null)
                {
                    // cập nhật lại thông tin họ tên cho học sinh này để tránh t/h có thay đổi họ tên nhưng trong bảng học sinh điểm nhận xét lại không thay đổi dẫn đến việc truy vấn kết quả bị sinh ra 2 record
                    hocSinhDiemNX.HoTen = HocsinhEnties.HoTen;
                    db.SaveChanges();
                }
                

            }
            else
            {
                //insert            
                db.AddToHocSinhs(model.hocSinh);
                db.SaveChanges();
            }

            return Json(new { success = true });
        }


        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult LoadLophoc(string id)
        {


            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            string namhoc = Cmon.GetNamHienTai(idTruong);
            /*var modelData = (from lop in db.LopHocs
                             where lop.idTruong == idTruong && lop.NamHoc == namhoc && lop.MaKhoi == id
                             let allowlops =
                             (from userLop in db.Users_LopHoc where userLop.UserName == userName && userLop.NamHoc == namhoc && userLop.idTruong == idTruong select userLop.MaLop).
                             Union(from usermon in db.Users_Monhoc where usermon.idTruong == idTruong && usermon.NamHoc == namhoc && usermon.UserName == userName select usermon.MaLop)

                             where allowlops.Contains(lop.MaLop) && lop.MaKhoi == id && lop.NamHoc == namhoc
                             select new SelectListItem()
                             {
                                 Value = lop.MaLop,
                                 Text = lop.TenLop,

                             }).ToList();
            */

            var modelData = (from lop in db.LopHocs.Where(lop=> lop.idTruong==idTruong && lop.NamHoc==namhoc && lop.MaKhoi==id)
                             join l1 in db.Users_LopHoc.Where(l1 => l1.UserName == userName && l1.idTruong == idTruong && l1.NamHoc == namhoc)
                              on lop.MaLop equals l1.MaLop
                             where lop.idTruong == idTruong && lop.NamHoc == namhoc
                             orderby lop.MaLop
                             select new SelectListItem()
                             {
                                 Value = lop.MaLop,
                                 Text = lop.TenLop,

                             }).ToList();

            return Json(modelData, JsonRequestBehavior.AllowGet);

        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult LoadHocsinhtheolop(int idTruong, string malop)
        {
            string namhoc = Cmon.GetNamHienTai(idTruong);

             
            var dblist = (from hs in db.HocSinhs.Where(hs => hs.MaLop == malop && hs.idTruong == idTruong && hs.NamHoc == namhoc)
                  where !(from hsqt in db.HocSinh_QuaTrinh.Where(hsqt => hsqt.idTruong == idTruong && hsqt.NamHoc == namhoc && hsqt.MaLop == malop && hsqt.MaQuaTrinh == "BH")
                          select hsqt.MaHocSinh).Contains(hs.MaHocSinh)                       
                  orderby hs.STT ascending     
                          select new
                          {
                              MaHocSinh = hs.MaHocSinh,
                              HoTen = hs.HoTen,
                              STT = hs.STT,
                              ngaysinh = hs.NgaySinh == null ? null : hs.NgaySinh,
                              noisinh = hs.NoiSinh == null ? "" : hs.NoiSinh,

                          }).ToList();
            ViewBag.tongsohocsinh = dblist.Count();
            return Json(dblist, JsonRequestBehavior.AllowGet);


        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult LoadQuanHuyen(string idTinh)
        {

            var modelData = (from quan in db.DM_QuanHuyen

                             where quan.MaTinh == idTinh
                             select new SelectListItem()
                             {
                                 Value = quan.MaHuyen,
                                 Text = quan.TenHuyen,

                             }).ToList();

            return Json(modelData, JsonRequestBehavior.AllowGet);

        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult LoadXaPhuong(string idHuyen)
        {

            var modelData = (from xa in db.DM_XaPhuong

                             where xa.MaHuyen == idHuyen
                             select new SelectListItem()
                             {
                                 Value = xa.MaXa,
                                 Text = xa.TenXa,

                             }).ToList();

            return Json(modelData, JsonRequestBehavior.AllowGet);

        }
        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult LoadDoiTuongChinhSach()
        {

            var modelData = (from cs in db.DM_ChinhSach
                             
                             select new SelectListItem()
                             {
                                 Value = cs.MaChinhSach,
                                 Text = cs.TenChinhSach,

                             }).ToList();

            return Json(modelData, JsonRequestBehavior.AllowGet);

        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult LoadKhoiPho(string idXa)
        {

            var modelData = (from kp in db.DM_KhoiPho

                             where kp.MaXa == idXa
                             select new SelectListItem()
                             {
                                 Value = kp.MaKP,
                                 Text = kp.TenKP,

                             }).ToList();

            return Json(modelData, JsonRequestBehavior.AllowGet);

        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Authorize]
        public ActionResult Addhocsinh(string malop)
        {

            var HocsinhEnties = new HocSinh();

            ViewBag.NoiO_MaTinh = new SelectList(db.DM_TinhThanh.ToList(), "MaTinh", "TenTinh", HocsinhEnties.NoiO_MaTinh);
            ViewBag.NoiO_MaHuyen = new SelectList(new List<DM_QuanHuyen>(), "MaHuyen", "TenHuyen", HocsinhEnties.NoiO_MaHuyen);
            ViewBag.NoiO_MaXa = new SelectList(new List<DM_XaPhuong>(), "MaXa", "TenXa", HocsinhEnties.NoiO_MaXa);
            ViewBag.NoiO_MaKP = new SelectList(new List<DM_KhoiPho>(), "MaKP", "TenKP", HocsinhEnties.NoiO_MaKP);

            ViewBag.MaDanToc = new SelectList(db.DM_DanToc.ToList(), "MaDanToc", "TenDanToc", HocsinhEnties.MaDanToc);
            ViewBag.MaChinhSach = new SelectList(db.DM_ChinhSach.ToList(), "MaChinhSach", "TenChinhSach", HocsinhEnties.MaChinhSach);
            ViewBag.MaTonGiao = new SelectList(db.DM_TonGiao.ToList(), "MaTonGiao", "TenTonGiao", HocsinhEnties.MaTonGiao);
            ViewBag.MaDoanDoi = new SelectList(db.DM_DoanDoi.ToList(), "MaDoanDoi", "TenDoanDoi", HocsinhEnties.MaDoanDoi);

            //edit cac du lieu ko nhap tren form
           

            string userName = User.Identity.Name.ToString() == "" ? "superadmin" : User.Identity.Name.ToString();
            ScrUser userchange = (from us in db.ScrUsers
                                  where us.UserName == userName
                                  select us).FirstOrDefault();
            int idTruong = userchange.idTruong != null ? (int)userchange.idTruong : 26;
            HocsinhEnties.NamHoc = Cmon.GetNamHienTai(idTruong);
            HocsinhEnties.idTruong = idTruong;

            HocsinhEnties.MaLop = malop;

            return PartialView("Viewhocsinh", HocsinhEnties);

        }

        
        [AcceptVerbs(HttpVerbs.Get)]
        [Authorize]
        public ActionResult ViewhocsinhReadOnly(string mahs, string malop, string makhoi, bool? inline)
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Thongbao");

            }
          

            VIEWALSTUDENTINFOMATION viewAllINFS = new VIEWALSTUDENTINFOMATION();// hien thi toan bo thong tin cua hoc sinh 


            // lay thong tin truong 
            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            ViewBag.idtruong = idTruong;

            string namhoc = Cmon.GetNamHienTai(idTruong); 
           int writerAccess = 1;
           var update = (from RU in db.ScrJRoleUsers
                         join RF in db.ScrJRoleFunctions
                             on RU.RoleName equals RF.RoleName
                         where RU.UserName == userName && RF.idTruong == idTruong
                         && RF.FunctionID == 92
                         select RF
                       ).FirstOrDefault().Update_;

           if (update)
               writerAccess = 1;
           else
               writerAccess = 0;
           ViewBag.WriteAccess = writerAccess;
            
           
            HocSinh HocsinhEnties = (from hocsinh in db.HocSinhs
                                     where hocsinh.idTruong == idTruong && hocsinh.MaLop == malop && hocsinh.MaHocSinh == mahs
                                     select hocsinh).FirstOrDefault();


            viewAllINFS.hocSinh = HocsinhEnties;// gan cacs thong tin cua hoc sinh vao doi tuong 
            var dblist = (from quatrinh in db.HocSinh_QuaTrinh.Where(quatrinh => quatrinh.idTruong == idTruong && quatrinh.MaHocSinh == mahs)
                          join user in db.ScrUsers on quatrinh.UserName equals user.UserName
                          orderby quatrinh.Thoigiansukien descending
                          select new QTHOCTAP
                          {
                              IDQuaTrinh = quatrinh.IDQuaTrinh,
                              NamHoc = quatrinh.NamHoc,
                              idTruong = quatrinh.idTruong,
                              MaKhoi = quatrinh.MaKhoi,
                              MaLop = quatrinh.MaLop,
                              MaHocSinh = quatrinh.MaHocSinh,
                              tenuser = user.FullName,
                              Thoigiansukien = quatrinh.Thoigiansukien,
                              Noidung = quatrinh.Noidung,
                              UserName = quatrinh.UserName,
                              DateCreated = quatrinh.DateCreated

                          }
                         ).ToList();



            viewAllINFS.list_QTHT = dblist;


            ViewBag.namhientai = namhoc;
           
            ViewBag.NoiO_MaTinh = new SelectList(db.DM_TinhThanh.ToList(), "MaTinh", "TenTinh", HocsinhEnties.NoiO_MaTinh);

            var listQuanHuyen = db.DM_QuanHuyen.Where(q => q.MaTinh == HocsinhEnties.NoiO_MaTinh).ToList();
            ViewBag.NoiO_MaHuyen = new SelectList(listQuanHuyen, "MaHuyen", "TenHuyen", HocsinhEnties.NoiO_MaHuyen);

            var listXaPhuong = db.DM_XaPhuong.Where(q => q.MaHuyen == HocsinhEnties.NoiO_MaHuyen).ToList();

            ViewBag.NoiO_MaXa = new SelectList(listXaPhuong, "MaXa", "TenXa", HocsinhEnties.NoiO_MaXa);

            var listKhoiPho = db.DM_KhoiPho.Where(q => q.MaKP == HocsinhEnties.NoiO_MaKP).ToList();

            ViewBag.NoiO_MaKP = new SelectList(listKhoiPho, "MaKP", "TenKP", HocsinhEnties.NoiO_MaKP);

            ViewBag.MaDanToc = new SelectList(db.DM_DanToc.ToList(), "MaDanToc", "TenDanToc", HocsinhEnties.MaDanToc);


            ViewBag.MaChinhSach = new SelectList(db.DM_ChinhSach.ToList(), "MaChinhSach", "TenChinhSach", HocsinhEnties.MaChinhSach);
            ViewBag.MaTonGiao = new SelectList(db.DM_TonGiao.ToList(), "MaTonGiao", "TenTonGiao", HocsinhEnties.MaTonGiao);
            ViewBag.MaDoanDoi = new SelectList(db.DM_DoanDoi.ToList(), "MaDoanDoi", "TenDoanDoi", HocsinhEnties.MaDoanDoi);


            ViewBag.namhientai = namhoc;
            ViewBag.makhoi = makhoi;
            ViewBag.malop = malop;
            ViewBag.mahocsinh = mahs;
            ViewBag.username = userName;

            ViewBag.btnId = "btnRefresh";
            //ViewBag.formId = "listhocsinh";

            ViewBag.btnId = "btnRefresh";
            ViewBag.formId = "formqt";

            if (inline.HasValue)
            {
                ViewBag.inline = true;
                return PartialView("ViewhocsinhReadOnly", viewAllINFS);
            }
            else
            {
                ViewBag.inline = false;

                return View("ViewhocsinhReadOnly", viewAllINFS);
            }

        }


        [AcceptVerbs(HttpVerbs.Get)]
        [Authorize]
        public ActionResult Viewhocsinh(string mahs, string malop, bool? inline)
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Thongbao");

            }

            VIEWALSTUDENTINFOMATION viewAllINFS = new VIEWALSTUDENTINFOMATION();// hien thi toan bo thong tin cua hoc sinh 


            // lay thong tin truong 
            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            ViewBag.idtruong = idTruong;

            string namhoc = Cmon.GetNamHienTai(idTruong);
            HocSinh HocsinhEnties = (from hocsinh in db.HocSinhs
                                     where hocsinh.idTruong == idTruong && hocsinh.MaLop == malop && hocsinh.MaHocSinh == mahs 
                                     join hsqt in db.HocSinh_QuaTrinh.Where(hsqt => hsqt.idTruong == idTruong && hsqt.NamHoc == namhoc && hsqt.MaLop == malop && hsqt.MaQuaTrinh != "BH")
                         on hocsinh.MaHocSinh equals hsqt.MaHocSinh
                                     select hocsinh).FirstOrDefault();
            var hocSinhDetail = (from hocsinhDetail in db.HocSinh_Details
                                 where hocsinhDetail.idTruong == idTruong && hocsinhDetail.NamHoc == namhoc
                                 && hocsinhDetail.MaLop == malop
                                 && hocsinhDetail.MaHocSinh == mahs
                                 select hocsinhDetail).FirstOrDefault();

             
            viewAllINFS.hocSinh = HocsinhEnties;// gan cacs thong tin cua hoc sinh vao doi tuong 
            viewAllINFS.hocSinhDetail = hocSinhDetail;
            var dblist = (from quatrinh in db.HocSinh_QuaTrinh.Where(quatrinh => quatrinh.idTruong == idTruong  && quatrinh.MaLop == malop && quatrinh.MaHocSinh == mahs)
                          join user in db.ScrUsers on quatrinh.UserName equals user.UserName
                          orderby quatrinh.Thoigiansukien descending
                          select new QTHOCTAP
                          {
                              IDQuaTrinh = quatrinh.IDQuaTrinh,
                              MaQuaTrinh = quatrinh.MaQuaTrinh,
                              NamHoc = quatrinh.NamHoc,
                              idTruong = quatrinh.idTruong,
                              MaKhoi = quatrinh.MaKhoi,
                              MaLop = quatrinh.MaLop,
                              MaHocSinh = quatrinh.MaHocSinh,
                              tenuser = user.FullName,
                              Thoigiansukien = quatrinh.Thoigiansukien,
                              Noidung = quatrinh.Noidung,
                              UserName = quatrinh.UserName,
                              DateCreated = quatrinh.DateCreated

                          }
                         ).ToList();



            viewAllINFS.list_QTHT = dblist;

            // load toàn bộ nhận xét của giáo viên chủ nhiệm trong năm học cho một học sinh

            var giaoVienChuNhiemNhanXetHocSinh =  ( from nhanxet in db.HocSinh_DanhGiaThang_GVCN.Where(nhanxet => nhanxet.idTruong == idTruong 
                                                    && nhanxet.MaLop == malop && nhanxet.NamHoc == namhoc && nhanxet.MaHocSinh == mahs)
                                                    
                                                    select new GIAOVIENCHUNHIEMNHANXETHOCSINH {
                                                     
                                                        thang8 = new BoMonNhanXet {
                                                         kienThuc = nhanxet.NXT1_KT,
                                                         nangLuc = nhanxet.NXT1_NL,
                                                         phamChat = nhanxet.NXT1_PC,
                                                        },

                                                        thang9 = new BoMonNhanXet {
                                                         kienThuc = nhanxet.NXT2_KT,
                                                         nangLuc = nhanxet.NXT2_NL,
                                                         phamChat = nhanxet.NXT2_PC,
                                                        },

                                                        thang10 = new BoMonNhanXet {
                                                         kienThuc = nhanxet.NXT3_KT,
                                                         nangLuc = nhanxet.NXT3_NL,
                                                         phamChat = nhanxet.NXT3_PC,
                                                        },

                                                        thang11 = new BoMonNhanXet {
                                                         kienThuc = nhanxet.NXT4_KT,
                                                         nangLuc = nhanxet.NXT4_NL,
                                                         phamChat = nhanxet.NXT4_PC,
                                                        },

                                                        thang12 = new BoMonNhanXet {
                                                         kienThuc = nhanxet.NXT5_KT,
                                                         nangLuc = nhanxet.NXT5_NL,
                                                         phamChat = nhanxet.NXT5_PC,
                                                        },

                                                        thang1 = new BoMonNhanXet {
                                                         kienThuc = nhanxet.NXT6_KT,
                                                         nangLuc = nhanxet.NXT6_NL,
                                                         phamChat = nhanxet.NXT6_PC,
                                                        },
                                                        
                                                        thang2 = new BoMonNhanXet {
                                                         kienThuc = nhanxet.NXT7_KT,
                                                         nangLuc = nhanxet.NXT7_NL,
                                                         phamChat = nhanxet.NXT7_PC,
                                                        },

                                                        thang3 = new BoMonNhanXet {
                                                         kienThuc = nhanxet.NXT8_KT,
                                                         nangLuc = nhanxet.NXT8_NL,
                                                         phamChat = nhanxet.NXT8_PC,
                                                        },

                                                        thang4 = new BoMonNhanXet {
                                                         kienThuc = nhanxet.NXT9_KT,
                                                         nangLuc = nhanxet.NXT9_NL,
                                                         phamChat = nhanxet.NXT9_PC,
                                                        },

                                                        thang5 = new BoMonNhanXet {
                                                         kienThuc = nhanxet.NXT10_KT,
                                                         nangLuc = nhanxet.NXT10_NL,
                                                         phamChat = nhanxet.NXT10_PC,
                                                        },
                                                         
                                                    }).FirstOrDefault();
             if (giaoVienChuNhiemNhanXetHocSinh != null) {

                viewAllINFS.giaoVienChuNhiemNhanXetHocSinh = giaoVienChuNhiemNhanXetHocSinh;
             }

            ViewBag.namhientai = namhoc;
         
            ViewBag.NoiO_MaTinh = new SelectList(db.DM_TinhThanh.ToList(), "MaTinh", "TenTinh", HocsinhEnties.NoiO_MaTinh);

            var listQuanHuyen = db.DM_QuanHuyen.Where(q => q.MaTinh == HocsinhEnties.NoiO_MaTinh).ToList();
            ViewBag.NoiO_MaHuyen = new SelectList(listQuanHuyen, "MaHuyen", "TenHuyen", HocsinhEnties.NoiO_MaHuyen);

            var listXaPhuong = db.DM_XaPhuong.Where(q => q.MaHuyen == HocsinhEnties.NoiO_MaHuyen).ToList();

            ViewBag.NoiO_MaXa = new SelectList(listXaPhuong, "MaXa", "TenXa", HocsinhEnties.NoiO_MaXa);

            var listKhoiPho = db.DM_KhoiPho.Where(q => q.MaXa == HocsinhEnties.NoiO_MaXa).ToList();

            ViewBag.NoiO_MaKP = new SelectList(listKhoiPho, "MaKP", "TenKP", HocsinhEnties.NoiO_MaKP);


            ViewBag.MaDanToc = new SelectList(db.DM_DanToc.ToList(), "MaDanToc", "TenDanToc", HocsinhEnties.MaDanToc);
            ViewBag.coHoSoKhuyetTatcb = 0;
            if (hocSinhDetail != null)
            {
                ViewBag.coHoSoKhuyetTatcb = hocSinhDetail.CoHoSoKhuyetTat == true ? 1 : 0;
                ViewBag.KhuyetTat = new SelectList(db.DM_KhuyetTat.ToList(), "MaKhuyetTat", "TenKhuyetTat", hocSinhDetail.MaKhuyetTat);
            }
            else {
                ViewBag.KhuyetTat = new SelectList(db.DM_KhuyetTat.ToList(), "MaKhuyetTat", "TenKhuyetTat", "");
                ViewBag.coHoSoKhuyetTatcb = 0;
            }
            ViewBag.MaChinhSach = new SelectList(db.DM_ChinhSach.ToList(), "MaChinhSach", "TenChinhSach", HocsinhEnties.MaChinhSach);
            ViewBag.MaTonGiao = new SelectList(db.DM_TonGiao.ToList(), "MaTonGiao", "TenTonGiao", HocsinhEnties.MaTonGiao);
            ViewBag.MaDoanDoi = new SelectList(db.DM_DoanDoi.ToList(), "MaDoanDoi", "TenDoanDoi", HocsinhEnties.MaDoanDoi);
            
            ViewBag.khuyettatcheckbox = "0";
            
            if (HocsinhEnties != null)
            {
                ViewBag.khuyettatcheckbox = HocsinhEnties.KhuyetTat == true ? "1" : "0";
            }

            ViewBag.namhientai = namhoc;
            //ViewBag.makhoi = makhoi;
            ViewBag.malop = malop;
            ViewBag.mahocsinh = mahs;
            
            ViewBag.username = userName;

            ViewBag.btnId = "btnRefresh";
            //ViewBag.formId = "listhocsinh";

            ViewBag.btnId = "btnRefresh";
            ViewBag.formId = "formqt";

            if (inline.HasValue)
            {
                ViewBag.inline = true;
                return PartialView("Viewhocsinh", viewAllINFS);
            }
            else
            {
                ViewBag.inline = false;

                return View("Viewhocsinh", viewAllINFS);
            }

        }


         
        [HttpPost]
        [Authorize]
        public ActionResult Delete(string mahs, string malop, int idTruong)
        {
            HocSinh HocsinhEnties = (from hocsinh in db.HocSinhs
                                     where hocsinh.idTruong == idTruong && hocsinh.MaLop == malop && hocsinh.MaHocSinh == mahs
                                     select hocsinh).FirstOrDefault();
            db.DeleteObject(HocsinhEnties);
            db.SaveChanges();
            return Json(new { success = true });
        }

        //--------------doan trinh sau se thuc hien cac nhiem vu cua quan ly quan trinh cua hoc sinh--------

        /// <summary>
        ///  Them moi qua trih cua hoc sinh
        /// </summary>
        /// <param name="mahs"></param>
        /// <param name="malop"></param>
        /// <param name="makhoi"></param>
        /// <param name="inline"></param>
        /// <returns></returns>
        
        [Authorize]
        public ActionResult Themmoiquatrinh(string makhoi, string malop, string mahocsinh, string formid, string btnRefresh)
        {

            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            string namhoc = Cmon.GetNamHienTai(idTruong);
            ViewBag.idtruong = idTruong;
            ViewBag.namhoc = namhoc;
            ViewBag.makhoi = makhoi;
            ViewBag.malop = malop;
            ViewBag.mahocsinh = mahocsinh;
            ViewBag.username = userName;
            ViewBag.btnId = btnRefresh;
            ViewBag.formId = formid;


            return View();

        }

        /// <summary>
        /// lay thong tin de chuan bi sua qua trinh
        /// </summary>
        /// <param name="idquatrinh"></param>
        /// <param name="makhoi"></param>
        /// <param name="malop"></param>
        /// <param name="mahocsinh"></param>
        /// <returns></returns>
        
        [Authorize]
        public ActionResult SuaQuaTrinh(int idquatrinh, string makhoi, string malop, string mahocsinh, string formid, string btnRefresh)
        {

            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            string namhoc = Cmon.GetNamHienTai(idTruong);
            ViewBag.idtruong = idTruong;
            ViewBag.namhoc = namhoc;
            ViewBag.makhoi = makhoi;
            ViewBag.malop = malop;
            ViewBag.mahocsinh = mahocsinh;
            ViewBag.username = userName;
            ViewBag.idquatrinh = idquatrinh;
            ViewBag.btnId = btnRefresh;
            ViewBag.formId = formid;

            HocSinh_QuaTrinh HocSinhQuaTrinh = (from hsqt in db.HocSinh_QuaTrinh
                                                where hsqt.IDQuaTrinh == idquatrinh
                                                select hsqt).FirstOrDefault();

            return View("SuaQuaTrinh", HocSinhQuaTrinh);

        }

        /// <summary>
        /// Xoa qua trinh
        /// </summary>
        /// <param name="mahs"></param>
        /// <param name="malop"></param>
        /// <param name="idTruong"></param>
        /// <returns></returns>

        [HttpPost]
        [Authorize]
        public ActionResult DeleteQT(int idquatrinh, string Username)
        {

            HocSinh_QuaTrinh HocsinhQT = (from quatrinh in db.HocSinh_QuaTrinh
                                          where quatrinh.IDQuaTrinh == idquatrinh && quatrinh.UserName == Username
                                          select quatrinh).FirstOrDefault();

            if (HocsinhQT != null)
            {
                db.DeleteObject(HocsinhQT);
                db.SaveChanges();
                return Json(new { success = "Valid" });
            }
            else
            {
                return Json(new { error = "Invalid" });
                //throw new Exception("oh no");

            }



        }


        /// <summary>
        /// Luu qua trinh cho mot hoc sinh
        /// </summary>
        /// <param name="password"></param>
        /// <param name="userinfo"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public ActionResult saveQuaTrinh(string maquatrinh, string noidung, string tgsk, string nguoitao, string ngaytao, string idtruong, string namhoc, string makhoi, string malop, string mahocsinh)
        {

            // string userName = User.Identity.Name.ToString() == "" ? "superadmin" : User.Identity.Name.ToString();


            HocSinh_QuaTrinh HSQT = new HocSinh_QuaTrinh();
            HSQT.MaHocSinh = mahocsinh;
            HSQT.MaHocSinhFriendly = mahocsinh;
            HSQT.MaQuaTrinh = maquatrinh;
            HSQT.Noidung = noidung;
            HSQT.Thoigiansukien = DateTime.ParseExact(tgsk, "dd/MM/yyyy", null);
            HSQT.MaKhoi = makhoi;
            HSQT.MaLop = malop;
            HSQT.UserName = nguoitao;
            HSQT.idTruong = Int32.Parse(idtruong);
            HSQT.DateCreated = DateTime.ParseExact(ngaytao, "dd/MM/yyyy", null);
            HSQT.NamHoc = namhoc;
            HSQT.AutoInsert = true;

            db.HocSinh_QuaTrinh.AddObject(HSQT);
            db.SaveChanges();


            return new JqGridJsonResult() { Data = "success" };
        }



        /// <summary>
        /// LuuSuaquatrinh cho mot hoc sinh
        /// </summary>
        /// <param name="password"></param>
        /// <param name="userinfo"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public ActionResult LuuSuaquatrinh(int idquatrinh, string maquatrinh, string noidung, string tgsk, string nguoitao, string ngaytao, string idtruong, string namhoc, string makhoi, string malop, string mahocsinh)
        {

            // string userName = User.Identity.Name.ToString() == "" ? "superadmin" : User.Identity.Name.ToString();


            HocSinh_QuaTrinh HSQT = (from quatrinh in db.HocSinh_QuaTrinh where quatrinh.IDQuaTrinh == idquatrinh select quatrinh).FirstOrDefault();
            HSQT.MaHocSinh = mahocsinh;
            HSQT.MaHocSinhFriendly = mahocsinh;
            HSQT.MaQuaTrinh = maquatrinh;
            HSQT.Noidung = noidung;
            HSQT.Thoigiansukien = DateTime.ParseExact(tgsk, "dd/MM/yyyy", null);
            HSQT.MaKhoi = makhoi;
            HSQT.MaLop = malop;
            HSQT.UserName = nguoitao;
            HSQT.idTruong = Int32.Parse(idtruong);
            HSQT.DateCreated = DateTime.ParseExact(ngaytao, "dd/MM/yyyy", null);
            HSQT.NamHoc = namhoc;
            HSQT.AutoInsert = true;

            db.SaveChanges();


            return new JqGridJsonResult() { Data = "success" };
        }



        [AcceptVerbs(HttpVerbs.Get)]
        [Authorize]
        public ActionResult reloadata(string mahs, string malop, string makhoi, bool? inline)
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Thongbao");

            }
            VIEWALSTUDENTINFOMATION viewAllINFS = new VIEWALSTUDENTINFOMATION();// hien thi toan bo thong tin cua hoc sinh 


            // lay thong tin truong 
            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            ViewBag.idtruong = idTruong;

            string namhoc = Cmon.GetNamHienTai(idTruong);
            HocSinh HocsinhEnties = (from hocsinh in db.HocSinhs
                                     where hocsinh.idTruong == idTruong && hocsinh.MaLop == malop && hocsinh.MaHocSinh == mahs
                                     select hocsinh).FirstOrDefault();

            viewAllINFS.hocSinh = HocsinhEnties;// gan cacs thong tin cua hoc sinh vao doi tuong 

          /*  var dblist = (from quatrinh in db.HocSinh_QuaTrinh
                          where quatrinh.idTruong == idTruong && quatrinh.NamHoc == namhoc && quatrinh.MaKhoi == makhoi && quatrinh.MaLop == malop && quatrinh.MaHocSinh == mahs
                          orderby quatrinh.DateCreated
                          select quatrinh
                         ).ToList();

          */
            var dblist = (from quatrinh in db.HocSinh_QuaTrinh.Where(quatrinh => quatrinh.idTruong == idTruong && quatrinh.MaKhoi == makhoi && quatrinh.MaLop == malop && quatrinh.MaHocSinh == mahs)
                          join user in db.ScrUsers on quatrinh.UserName equals user.UserName
                          orderby quatrinh.Thoigiansukien descending
                          select new QTHOCTAP
                          {
                              IDQuaTrinh = quatrinh.IDQuaTrinh,
                              NamHoc = quatrinh.NamHoc,
                              MaQuaTrinh= quatrinh.MaQuaTrinh,
                              idTruong = quatrinh.idTruong,
                              MaKhoi = quatrinh.MaKhoi,
                              MaLop = quatrinh.MaLop,
                              MaHocSinh = quatrinh.MaHocSinh,
                              tenuser = user.FullName,
                              Thoigiansukien = quatrinh.Thoigiansukien,
                              Noidung = quatrinh.Noidung,
                              UserName = quatrinh.UserName,
                              DateCreated = quatrinh.DateCreated

                          }
                      ).ToList();
             

            viewAllINFS.list_QTHT = dblist;
            ViewBag.NoiO_MaTinh = new SelectList(db.DM_TinhThanh.ToList(), "MaTinh", "TenTinh", HocsinhEnties.NoiO_MaTinh);

            var listQuanHuyen = db.DM_QuanHuyen.Where(q => q.MaTinh == HocsinhEnties.NoiO_MaTinh).ToList();
            ViewBag.NoiO_MaHuyen = new SelectList(listQuanHuyen, "MaHuyen", "TenHuyen", HocsinhEnties.NoiO_MaHuyen);
            var listXaPhuong = db.DM_XaPhuong.Where(q => q.MaHuyen == HocsinhEnties.NoiO_MaHuyen).ToList();
            ViewBag.NoiO_MaXa = new SelectList(listXaPhuong, "MaXa", "TenXa", HocsinhEnties.NoiO_MaXa);
            var listKhoiPho = db.DM_KhoiPho.Where(q => q.MaXa == HocsinhEnties.NoiO_MaXa).ToList();
            ViewBag.NoiO_MaKP = new SelectList(listKhoiPho, "MaKP", "TenKP", HocsinhEnties.NoiO_MaKP);

            ViewBag.MaDanToc = new SelectList(db.DM_DanToc.ToList(), "MaDanToc", "TenDanToc", HocsinhEnties.MaDanToc);
            ViewBag.MaChinhSach = new SelectList(db.DM_ChinhSach.ToList(), "MaChinhSach", "TenChinhSach", HocsinhEnties.MaChinhSach);
            ViewBag.MaTonGiao = new SelectList(db.DM_TonGiao.ToList(), "MaTonGiao", "TenTonGiao", HocsinhEnties.MaTonGiao);
            ViewBag.MaDoanDoi = new SelectList(db.DM_DoanDoi.ToList(), "MaDoanDoi", "TenDoanDoi", HocsinhEnties.MaDoanDoi);

            ViewBag.namhientai = namhoc;
            ViewBag.makhoi = makhoi;
            ViewBag.malop = malop;
            ViewBag.mahocsinh = mahs;
            ViewBag.username = userName;

            ViewBag.btnId = "btnRefresh";
            ViewBag.formId = "formqt";

            return PartialView("Viewhocsinh", viewAllINFS);
        

        }


        public ActionResult thunao()
        {
            Response.Write("test");
            return View();
        }
    }
}
