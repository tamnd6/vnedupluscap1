﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Lib.Web.Mvc.JQuery.JqGrid;
using VnEduPlus.CmonFunction;
using VnEduPlus.Models;
using System.Globalization;
using System.Threading;
namespace VnEduPlus.Controllers
{
    public class GiaoVienChuNhiemVaLopHocController : Controller
    {

        /// <summary>
        /// Khai bao bien toan cuc
        /// </summary>
        EDUNP_CAP1Entities db = new EDUNP_CAP1Entities();

        /// <summary>
        /// lay username dang nhap 
        /// </summary>
        /// <returns></returns>
        public string getUserName()
        {
            return User.Identity.Name.ToString() == "" ? "superadmin" : User.Identity.Name.ToString();

        }
        [Authorize]
        public ActionResult Index()
        {
            ViewBag.Message = "Chương trình ứng dụng công nghệ thông tin quản lý trường học 2012-2020!";

            return View();
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Authorize]

        #region Quản lý nhận xét

        public ActionResult TemplateNhanXetThang()
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Thongbao");

            }

            // lay thong tin truong 
            string userName = getUserName();
            int idTruong = Cmon.getIdtruong(userName, db);
            bool isHieuTruong = Cmon.isHieuTruong(idTruong, userName);
            ViewBag.isHieuTruong = isHieuTruong;
            string namHoc = Cmon.GetNamHienTai(idTruong); 
            ViewBag.namhoc = namHoc; 
            ViewBag.idtruong = idTruong;
            ViewBag.danhMucTieuChi = Cmon.createDanhMuc();
            ViewBag.danhMucThang = Cmon.createCacThangCoDinh(idTruong, db);
            ViewBag.tenuser = Cmon.GetnameUser(userName);
			DateTime d = DateTime.Now;
			string month = (d.Month < 9 && d.Month > 5) ? "09" : d.Month.ToString();

			ViewBag.thanght = month;
			//"01/" + month + "/" + d.Year.ToString();

            // Load lop hoc cua user


            var lopHoc = (from lophoc in db.LopHocs
                          let maLop = (from userLop in db.Users_LopHoc
                                      join userMon in db.Users_Monhoc
                                      on userLop.UserName equals userMon.UserName
                                      where userLop.idTruong == idTruong && userLop.NamHoc == namHoc && userLop.UserName == userName
                                          select userLop.MaLop).Distinct()
                         where maLop.Contains(lophoc.MaLop) && lophoc.idTruong == idTruong && lophoc.NamHoc== namHoc
                         select lophoc).ToList();

			string maLopMacDinh =  (lopHoc != null && lopHoc.Count > 0) ? lopHoc[0].MaLop.ToString() : "";
			ViewBag.maLopMacDinh = maLopMacDinh;

            LopHoc emptyLopHoc = new LopHoc();
            emptyLopHoc.MaLop = "-1";
            emptyLopHoc.TenLop = "--Chọn lớp học--";
            lopHoc.Insert(0, emptyLopHoc);
            ViewBag.lopHoc = lopHoc;
             

            return View();
        }

		[AcceptVerbs(HttpVerbs.Get)]
		[Authorize]

		#region Quản lý nhận xét định kì

		public ActionResult TemplateNhanXetDinhKy()
		{
			if (!Request.IsAuthenticated)
			{
				return RedirectToAction("Index", "Thongbao");

			}

			// lay thong tin truong 
			string userName = getUserName();
			int idTruong = Cmon.getIdtruong(userName, db);
			bool isHieuTruong = Cmon.isHieuTruong(idTruong, userName);
			ViewBag.isHieuTruong = isHieuTruong;
			string namHoc = Cmon.GetNamHienTai(idTruong);
			ViewBag.namhoc = namHoc;
			ViewBag.idtruong = idTruong;
			ViewBag.danhMucTieuChi = Cmon.createDanhMuc();
			ViewBag.danhMucThang = Cmon.createCacThangCoDinh(idTruong, db);
			ViewBag.tenuser = Cmon.GetnameUser(userName);

			ViewBag.hocKy = Cmon.CreateHocKyTheoDanhGia();

			// Load lop hoc cua user


			var lopHoc = (from lophoc in db.LopHocs
						  let maLop = (from userLop in db.Users_LopHoc
									   join userMon in db.Users_Monhoc
									   on userLop.UserName equals userMon.UserName
									   where userLop.idTruong == idTruong && userLop.NamHoc == namHoc && userLop.UserName == userName
									   select userLop.MaLop).Distinct()
						  where maLop.Contains(lophoc.MaLop) && lophoc.idTruong == idTruong && lophoc.NamHoc == namHoc
						  select lophoc).ToList();

			string maLopMacDinh = (lopHoc != null && lopHoc.Count > 0) ? lopHoc[0].MaLop.ToString() : "";
			ViewBag.maLopMacDinh = maLopMacDinh;

			LopHoc emptyLopHoc = new LopHoc();
			emptyLopHoc.MaLop = "-1";
			emptyLopHoc.TenLop = "--Chọn lớp học--";
			lopHoc.Insert(0, emptyLopHoc);
			ViewBag.lopHoc = lopHoc;


			return View();
		}
		#endregion


		#region Quản lý thêm mới nhận xét

		public ActionResult AddNhanXet()
        {
            return PartialView(@"~/Views/GiaoVienChuNhiem/AddNhanXet.cshtml");
        }

        [HttpGet]
        public JsonResult ThemMoiNhanXet(int type, string maNhanXet, string tenNhanXet, bool isdefault, string thang, int idtruong)
        {
            string mess = "";
            if (string.IsNullOrEmpty(maNhanXet.Trim()))
                mess = "Yêu cầu nhập mã nhận xét";
            else if (string.IsNullOrEmpty(tenNhanXet.Trim()))
                mess = "Yêu cầu nhập tên nhận xét";
            else
            {
                Cmon.UpdateStatusCNNX(type, Int16.Parse(thang), db);
                DM_NhanXetMau_GVCN kt = new DM_NhanXetMau_GVCN();
                kt.Thang = Convert.ToInt16(thang);

                kt.idTruong = idtruong;
                kt.LoaiNhanXet = (short)type;
                kt.TenNhanXet = tenNhanXet;
                kt.MaNhanXet = maNhanXet.Trim();
                kt.UserCreated = getUserName();
                kt.isDefault = isdefault;
                db.DM_NhanXetMau_GVCN.AddObject(kt);
                int rs = db.SaveChanges();
                if (rs > 0)
                    mess = "OK";
                else
                    mess = "Thêm mới tiêu chí không thành công!";
            }
            return Json(mess, JsonRequestBehavior.AllowGet);
        }


        #endregion

        #region Quản lý sửa nhận xét

        public ActionResult EditNhanXet(string loaiNhanXet, string manx, string thang, int idTruong)
        {

            short th = Convert.ToInt16(thang);
            short loaiNX = Convert.ToInt16(loaiNhanXet);
            var kthuc = (from kt in db.DM_NhanXetMau_GVCN.Where(t => t.LoaiNhanXet == loaiNX && t.Thang == th && t.MaNhanXet == manx && t.idTruong == idTruong) select kt).FirstOrDefault();

            return PartialView(@"~/Views/GiaoVienChuNhiem/EditNhanXet.cshtml", kthuc);
        }

        [HttpGet]
        public JsonResult UpdateNhanXet(int type, string maNhanXet, string tenNhanXet, bool isdefault, string thang, int idtruong)
        {
            string mess = "";
            if (string.IsNullOrEmpty(maNhanXet.Trim()))
                mess = "Yêu cầu nhập mã nhận xét";
            else if (string.IsNullOrEmpty(tenNhanXet.Trim()))
                mess = "Yêu cầu nhập tên nhận xét";
            else
            {
                Cmon.UpdateStatusCNNX(type, Int16.Parse(thang), db);
                short th = Convert.ToInt16(thang);
                DM_NhanXetMau_GVCN kt = (from kthuc in db.DM_NhanXetMau_GVCN.Where(t => t.LoaiNhanXet == type && t.MaNhanXet == maNhanXet && t.Thang == th && t.idTruong == idtruong) select kthuc).FirstOrDefault();
                if (kt != null)
                {
                    kt.MaNhanXet = maNhanXet.Trim();
                    kt.TenNhanXet = tenNhanXet;
                    kt.UserCreated = getUserName();
                    kt.isDefault = isdefault;
                    int rs = db.SaveChanges();
                    if (rs > 0)
                        mess = "OK";
                    else
                        mess = "Cập nhật nhận xét không thành công!";
                }
                else
                {
                    mess = "Không có thông tin cập nhật!";
                }
            }
            return Json(mess, JsonRequestBehavior.AllowGet);
        }


        #endregion

        #region Quản lý xóa nhận xét
        [HttpGet]
        public JsonResult DeleteNhanXet(int type, string maNhanXet, string thang, int idtruong)
        {
            string mess = "";
            short th = Convert.ToInt16(thang);
            DM_NhanXetMau_GVCN kt = (from kthuc in db.DM_NhanXetMau_GVCN.Where(t => t.MaNhanXet == maNhanXet && t.LoaiNhanXet == type && t.Thang == th && t.idTruong == idtruong) select kthuc).FirstOrDefault();
            if (kt != null)
            {

                db.DM_NhanXetMau_GVCN.DeleteObject(kt);
                int del = db.SaveChanges();
                if (del > 0)
                    mess = "OK";
                else
                    mess = "Xóa tiêu chí không thành công!";
            }
            else
            {
                mess = "Không có thông tin để xóa!";
            }
            return Json(mess, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Quản lý hiện thị danh sách nhận xét

        public List<HocSinhNhanXetNhom> getDanhSachHocSinh(int idTruong, int thang, string maLop, string namHoc)
        {
            List<HocSinhNhanXetNhom> danhSachHocSinh = new List<HocSinhNhanXetNhom>();
            switch (thang)
            {
                case 8:
                   danhSachHocSinh = (from hs in db.HocSinhs
                                       where hs.idTruong == idTruong && hs.MaLop == maLop && hs.NamHoc == namHoc && !(from hsqt in db.HocSinh_QuaTrinh.Where(hsqt => hsqt.idTruong == idTruong && hsqt.NamHoc == namHoc && hsqt.MaLop == maLop && hsqt.MaQuaTrinh == "BH")
                                                                                                                      select hsqt.MaHocSinh).Contains(hs.MaHocSinh)
                                       join hst in db.HocSinh_DanhGiaThang_GVCN on new { hs.MaHocSinh, hs.idTruong, hs.MaLop, hs.NamHoc } equals new { hst.MaHocSinh, hst.idTruong, hst.MaLop, hst.NamHoc } into hocsinh
                                       from hsj in hocsinh.DefaultIfEmpty()
                                       orderby hs.STT ascending
                                       select new HocSinhNhanXetNhom
                                       {
                                           STT = hs.STT,
                                           maHocSinh = hs.MaHocSinh,
                                           tenHocSinh = hs.HoTen,
                                           ngaySinh = hs.NgaySinh,
                                           nhanXetKienThuc = hsj.NXT1_KT,
                                           nhanXetNangLuc = hsj.NXT1_NL,
                                           nhanXetPhamChat = hsj.NXT1_PC

                                       }).ToList();   
                    break;
                case 9:
                    danhSachHocSinh = (from hs in db.HocSinhs
                                       where hs.idTruong == idTruong && hs.MaLop == maLop && hs.NamHoc == namHoc && !(from hsqt in db.HocSinh_QuaTrinh.Where(hsqt => hsqt.idTruong == idTruong && hsqt.NamHoc == namHoc && hsqt.MaLop == maLop && hsqt.MaQuaTrinh == "BH")
                                                                                                                      select hsqt.MaHocSinh).Contains(hs.MaHocSinh)
                                       join hst in db.HocSinh_DanhGiaThang_GVCN on new { hs.MaHocSinh, hs.idTruong, hs.MaLop, hs.NamHoc } equals new { hst.MaHocSinh, hst.idTruong, hst.MaLop, hst.NamHoc } into hocsinh
                                       from hsj in hocsinh.DefaultIfEmpty()
                                       orderby hs.STT ascending
                                       select new HocSinhNhanXetNhom
                                       {
                                           STT = hs.STT,
                                           maHocSinh = hs.MaHocSinh,
                                           tenHocSinh = hs.HoTen,
                                           ngaySinh = hs.NgaySinh,
                                           nhanXetKienThuc = hsj.NXT2_KT,
                                           nhanXetNangLuc = hsj.NXT2_NL,
                                           nhanXetPhamChat = hsj.NXT2_PC

                                       }).ToList();   

                    break;

                case 10:
                    danhSachHocSinh = (from hs in db.HocSinhs
                                       where hs.idTruong == idTruong && hs.MaLop == maLop && hs.NamHoc == namHoc && !(from hsqt in db.HocSinh_QuaTrinh.Where(hsqt => hsqt.idTruong == idTruong && hsqt.NamHoc == namHoc && hsqt.MaLop == maLop && hsqt.MaQuaTrinh == "BH")
                                        select hsqt.MaHocSinh).Contains(hs.MaHocSinh)
                                       join hst in db.HocSinh_DanhGiaThang_GVCN on new { hs.MaHocSinh, hs.idTruong, hs.MaLop, hs.NamHoc } equals new { hst.MaHocSinh, hst.idTruong, hst.MaLop, hst.NamHoc } into hocsinh
                                        from hsj in hocsinh.DefaultIfEmpty()
                                        orderby hs.STT ascending
                                         select new HocSinhNhanXetNhom
                                         {
                                             STT = hs.STT,
                                             maHocSinh = hs.MaHocSinh,
                                             tenHocSinh = hs.HoTen,
                                             ngaySinh = hs.NgaySinh,
                                             nhanXetKienThuc = hsj.NXT3_KT,
                                             nhanXetNangLuc = hsj.NXT3_NL,
                                             nhanXetPhamChat = hsj.NXT3_PC

                                         }).ToList();   

                    break;

                case 11:
                    danhSachHocSinh = (from hs in db.HocSinhs
                                       where hs.idTruong == idTruong && hs.MaLop == maLop && hs.NamHoc == namHoc && !(from hsqt in db.HocSinh_QuaTrinh.Where(hsqt => hsqt.idTruong == idTruong && hsqt.NamHoc == namHoc && hsqt.MaLop == maLop && hsqt.MaQuaTrinh == "BH")
                                                                                                                      select hsqt.MaHocSinh).Contains(hs.MaHocSinh)
                                       join hst in db.HocSinh_DanhGiaThang_GVCN on new { hs.MaHocSinh, hs.idTruong, hs.MaLop, hs.NamHoc } equals new { hst.MaHocSinh, hst.idTruong, hst.MaLop, hst.NamHoc } into hocsinh
                                       from hsj in hocsinh.DefaultIfEmpty()
                                       orderby hs.STT ascending
                                       select new HocSinhNhanXetNhom
                                       {
                                           STT = hs.STT,
                                           maHocSinh = hs.MaHocSinh,
                                           tenHocSinh = hs.HoTen,
                                           ngaySinh = hs.NgaySinh,
                                           nhanXetKienThuc = hsj.NXT4_KT,
                                           nhanXetNangLuc = hsj.NXT4_NL,
                                           nhanXetPhamChat = hsj.NXT4_PC

                                       }).ToList();   

                    break;

                case 12:
                    danhSachHocSinh = (from hs in db.HocSinhs
                                       where hs.idTruong == idTruong && hs.MaLop == maLop && hs.NamHoc == namHoc && !(from hsqt in db.HocSinh_QuaTrinh.Where(hsqt => hsqt.idTruong == idTruong && hsqt.NamHoc == namHoc && hsqt.MaLop == maLop && hsqt.MaQuaTrinh == "BH")
                                                                                                                      select hsqt.MaHocSinh).Contains(hs.MaHocSinh)
                                       join hst in db.HocSinh_DanhGiaThang_GVCN on new { hs.MaHocSinh, hs.idTruong, hs.MaLop, hs.NamHoc } equals new { hst.MaHocSinh, hst.idTruong, hst.MaLop, hst.NamHoc } into hocsinh
                                       from hsj in hocsinh.DefaultIfEmpty()
                                       orderby hs.STT ascending
                                       select new HocSinhNhanXetNhom
                                       {
                                           STT = hs.STT,
                                           maHocSinh = hs.MaHocSinh,
                                           tenHocSinh = hs.HoTen,
                                           ngaySinh = hs.NgaySinh,
                                           nhanXetKienThuc = hsj.NXT5_KT,
                                           nhanXetNangLuc = hsj.NXT5_NL,
                                           nhanXetPhamChat = hsj.NXT5_PC

                                       }).ToList();   

                    break;


                case 1:
                    danhSachHocSinh = (from hs in db.HocSinhs
                                       where hs.idTruong == idTruong && hs.MaLop == maLop && hs.NamHoc == namHoc && !(from hsqt in db.HocSinh_QuaTrinh.Where(hsqt => hsqt.idTruong == idTruong && hsqt.NamHoc == namHoc && hsqt.MaLop == maLop && hsqt.MaQuaTrinh == "BH")
                                                                                                                      select hsqt.MaHocSinh).Contains(hs.MaHocSinh)
                                       join hst in db.HocSinh_DanhGiaThang_GVCN on new { hs.MaHocSinh, hs.idTruong, hs.MaLop, hs.NamHoc } equals new { hst.MaHocSinh, hst.idTruong, hst.MaLop, hst.NamHoc } into hocsinh
                                       from hsj in hocsinh.DefaultIfEmpty()
                                       orderby hs.STT ascending
                                       select new HocSinhNhanXetNhom
                                       {
                                           STT = hs.STT,
                                           maHocSinh = hs.MaHocSinh,
                                           tenHocSinh = hs.HoTen,
                                           ngaySinh = hs.NgaySinh,
                                           nhanXetKienThuc = hsj.NXT6_KT,
                                           nhanXetNangLuc = hsj.NXT6_NL,
                                           nhanXetPhamChat = hsj.NXT6_PC

                                       }).ToList();   

                    break;

                case 2:
                    danhSachHocSinh = (from hs in db.HocSinhs
                                       where hs.idTruong == idTruong && hs.MaLop == maLop && hs.NamHoc == namHoc && !(from hsqt in db.HocSinh_QuaTrinh.Where(hsqt => hsqt.idTruong == idTruong && hsqt.NamHoc == namHoc && hsqt.MaLop == maLop && hsqt.MaQuaTrinh == "BH")
                                                                                                                      select hsqt.MaHocSinh).Contains(hs.MaHocSinh)
                                       join hst in db.HocSinh_DanhGiaThang_GVCN on new { hs.MaHocSinh, hs.idTruong, hs.MaLop, hs.NamHoc } equals new { hst.MaHocSinh, hst.idTruong, hst.MaLop, hst.NamHoc } into hocsinh
                                       from hsj in hocsinh.DefaultIfEmpty()
                                       orderby hs.STT ascending
                                       select new HocSinhNhanXetNhom
                                       {
                                           STT = hs.STT,
                                           maHocSinh = hs.MaHocSinh,
                                           tenHocSinh = hs.HoTen,
                                           ngaySinh = hs.NgaySinh,
                                           nhanXetKienThuc = hsj.NXT7_KT,
                                           nhanXetNangLuc = hsj.NXT7_NL,
                                           nhanXetPhamChat = hsj.NXT7_PC

                                       }).ToList();   

                    break;

                case 3:
                    danhSachHocSinh = (from hs in db.HocSinhs
                                       where hs.idTruong == idTruong && hs.MaLop == maLop && hs.NamHoc == namHoc && !(from hsqt in db.HocSinh_QuaTrinh.Where(hsqt => hsqt.idTruong == idTruong && hsqt.NamHoc == namHoc && hsqt.MaLop == maLop && hsqt.MaQuaTrinh == "BH")
                                                                                                                      select hsqt.MaHocSinh).Contains(hs.MaHocSinh)
                                       join hst in db.HocSinh_DanhGiaThang_GVCN on new { hs.MaHocSinh, hs.idTruong, hs.MaLop, hs.NamHoc } equals new { hst.MaHocSinh, hst.idTruong, hst.MaLop, hst.NamHoc } into hocsinh
                                       from hsj in hocsinh.DefaultIfEmpty()
                                       orderby hs.STT ascending
                                       select new HocSinhNhanXetNhom
                                       {
                                           STT = hs.STT,
                                           maHocSinh = hs.MaHocSinh,
                                           tenHocSinh = hs.HoTen,
                                           ngaySinh = hs.NgaySinh,
                                           nhanXetKienThuc = hsj.NXT8_KT,
                                           nhanXetNangLuc = hsj.NXT8_NL,
                                           nhanXetPhamChat = hsj.NXT8_PC

                                       }).ToList();   

                    break; 

                case 4:
                    danhSachHocSinh = (from hs in db.HocSinhs
                                       where hs.idTruong == idTruong && hs.MaLop == maLop && hs.NamHoc == namHoc && !(from hsqt in db.HocSinh_QuaTrinh.Where(hsqt => hsqt.idTruong == idTruong && hsqt.NamHoc == namHoc && hsqt.MaLop == maLop && hsqt.MaQuaTrinh == "BH")
                                                                                                                      select hsqt.MaHocSinh).Contains(hs.MaHocSinh)
                                       join hst in db.HocSinh_DanhGiaThang_GVCN on new { hs.MaHocSinh, hs.idTruong, hs.MaLop, hs.NamHoc } equals new { hst.MaHocSinh, hst.idTruong, hst.MaLop, hst.NamHoc } into hocsinh
                                       from hsj in hocsinh.DefaultIfEmpty()
                                       orderby hs.STT ascending
                                       select new HocSinhNhanXetNhom
                                       {
                                           STT = hs.STT,
                                           maHocSinh = hs.MaHocSinh,
                                           tenHocSinh = hs.HoTen,
                                           ngaySinh = hs.NgaySinh,
                                           nhanXetKienThuc = hsj.NXT9_KT,
                                           nhanXetNangLuc = hsj.NXT9_NL,
                                           nhanXetPhamChat = hsj.NXT9_PC

                                       }).ToList();   

                    break; 
                case 5:
                    danhSachHocSinh = (from hs in db.HocSinhs
                                       where hs.idTruong == idTruong && hs.MaLop == maLop && hs.NamHoc == namHoc && !(from hsqt in db.HocSinh_QuaTrinh.Where(hsqt => hsqt.idTruong == idTruong && hsqt.NamHoc == namHoc && hsqt.MaLop == maLop && hsqt.MaQuaTrinh == "BH")
                                                                                                                      select hsqt.MaHocSinh).Contains(hs.MaHocSinh)
                                       join hst in db.HocSinh_DanhGiaThang_GVCN on new { hs.MaHocSinh, hs.idTruong, hs.MaLop, hs.NamHoc } equals new { hst.MaHocSinh, hst.idTruong, hst.MaLop, hst.NamHoc } into hocsinh
                                       from hsj in hocsinh.DefaultIfEmpty()
                                       orderby hs.STT ascending
                                       select new HocSinhNhanXetNhom
                                       {
                                           STT = hs.STT,
                                           maHocSinh = hs.MaHocSinh,
                                           tenHocSinh = hs.HoTen,
                                           ngaySinh = hs.NgaySinh,
                                           nhanXetKienThuc = hsj.NXT10_KT,
                                           nhanXetNangLuc = hsj.NXT10_NL,
                                           nhanXetPhamChat = hsj.NXT10_PC

                                       }).ToList();   

                    break;
                default:
                    {
                        
                        break;
                    }
            }

            return danhSachHocSinh;
        }

         

        [AcceptVerbs(HttpVerbs.Get)]
        [Authorize]
        public ActionResult hienDanhSachNhanXet(int idTruong, int thang, string maLop)
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Thongbao");

            }
            
            string namHoc = Cmon.GetNamHienTai(idTruong);
            string userName = getUserName();
            ViewBag.maKhoi = Cmon.getKhoi(maLop, namHoc, idTruong);
            MauCauNhanXet mauCauNhanXet = new MauCauNhanXet();

            var dsKienThuc = (from dskt in db.DM_NhanXetMau_GVCN
                              where dskt.idTruong == idTruong && dskt.LoaiNhanXet == 1 && dskt.Thang == thang && dskt.UserCreated == userName
                              select new DmTieuChi {
                                  maNhanXet = dskt.MaNhanXet,
                                  tenNhanXet = dskt.TenNhanXet
                              
                              }).ToList();

            var dsNangLuc = (from dskt in db.DM_NhanXetMau_GVCN
                             where dskt.idTruong == idTruong && dskt.LoaiNhanXet == 2 && dskt.Thang == thang && dskt.UserCreated == userName

                             select new DmTieuChi
                             {
                                 maNhanXet = dskt.MaNhanXet,
                                 tenNhanXet = dskt.TenNhanXet

                             }).ToList();

           
            var dsPhamChat = (from dskt in db.DM_NhanXetMau_GVCN where dskt.idTruong == idTruong && dskt.LoaiNhanXet == 3 && dskt.Thang == thang && dskt.UserCreated== userName
                              select new DmTieuChi
                              {
                                  maNhanXet = dskt.MaNhanXet,
                                  tenNhanXet = dskt.TenNhanXet

                              }).ToList();
           

            DmTieuChi dmNX = new DmTieuChi();
            dmNX.maNhanXet = "-1";
            dmNX.tenNhanXet = "-- Chọn mẫu câu --";
            
            dsKienThuc.Insert(0, dmNX);
            dsNangLuc.Insert(0, dmNX);
            dsPhamChat.Insert(0, dmNX);

            mauCauNhanXet.hocSinhNhanXetNhom = getDanhSachHocSinh(idTruong,thang,maLop,namHoc);
            mauCauNhanXet.DMKienThuc = dsKienThuc;
            mauCauNhanXet.DMNangLuc = dsNangLuc;
            mauCauNhanXet.DMPhamChat = dsPhamChat;

            var QuyenNhapDiem = (from RU in db.ScrJRoleUsers
                                 join RF in db.ScrJRoleFunctions
                                     on RU.RoleName equals RF.RoleName
                                 where RU.UserName == userName && RF.idTruong == idTruong
                                 && RF.FunctionID == 112 && RF.Insert_ == true && RF.Enable == true && RF.Update_ == true
                                 select RF
                      ).FirstOrDefault();
            ViewBag.phanQuyen = QuyenNhapDiem != null ? 1 : 0;

			return PartialView("HienThiDanhSachNhanXet", mauCauNhanXet);
        }



		[AcceptVerbs(HttpVerbs.Get)]
		[Authorize]
		public ActionResult hienDanhSachNhanXetDinhKy(int idTruong, int thang, string maLop)
		{
			if (!Request.IsAuthenticated)
			{
				return RedirectToAction("Index", "Thongbao");

			}

			string namHoc = Cmon.GetNamHienTai(idTruong);
			string userName = getUserName();
			ViewBag.maKhoi = Cmon.getKhoi(maLop, namHoc, idTruong);
			MauCauNhanXet mauCauNhanXet = new MauCauNhanXet();

			var dsKienThuc = (from dskt in db.DM_NhanXetMau_GVCN
							  where dskt.idTruong == idTruong && dskt.LoaiNhanXet == 1 && dskt.Thang == thang && dskt.UserCreated == userName
							  select new DmTieuChi
							  {
								  maNhanXet = dskt.MaNhanXet,
								  tenNhanXet = dskt.TenNhanXet

							  }).ToList();

			var dsNangLuc = (from dskt in db.DM_NhanXetMau_GVCN
							 where dskt.idTruong == idTruong && dskt.LoaiNhanXet == 2 && dskt.Thang == thang && dskt.UserCreated == userName

							 select new DmTieuChi
							 {
								 maNhanXet = dskt.MaNhanXet,
								 tenNhanXet = dskt.TenNhanXet

							 }).ToList();


			var dsPhamChat = (from dskt in db.DM_NhanXetMau_GVCN
							  where dskt.idTruong == idTruong && dskt.LoaiNhanXet == 3 && dskt.Thang == thang && dskt.UserCreated == userName
							  select new DmTieuChi
							  {
								  maNhanXet = dskt.MaNhanXet,
								  tenNhanXet = dskt.TenNhanXet

							  }).ToList();


			DmTieuChi dmNX = new DmTieuChi();
			dmNX.maNhanXet = "-1";
			dmNX.tenNhanXet = "-- Chọn mẫu câu --";

			dsKienThuc.Insert(0, dmNX);
			dsNangLuc.Insert(0, dmNX);
			dsPhamChat.Insert(0, dmNX);

			mauCauNhanXet.hocSinhNhanXetNhom = getDanhSachHocSinh(idTruong, thang, maLop, namHoc);
			mauCauNhanXet.DMKienThuc = dsKienThuc;
			mauCauNhanXet.DMNangLuc = dsNangLuc;
			mauCauNhanXet.DMPhamChat = dsPhamChat;

			var QuyenNhapDiem = (from RU in db.ScrJRoleUsers
								 join RF in db.ScrJRoleFunctions
									 on RU.RoleName equals RF.RoleName
								 where RU.UserName == userName && RF.idTruong == idTruong
								 && RF.FunctionID == 112 && RF.Insert_ == true && RF.Enable == true && RF.Update_ == true
								 select RF
					  ).FirstOrDefault();
			ViewBag.phanQuyen = QuyenNhapDiem != null ? 1 : 0;

			return PartialView("HienThiDanhSachNhanXetDinhKy", mauCauNhanXet);
		}




        [Authorize]
        public ActionResult SoChuNhiemXuatExcell(string maLop, int thang)
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Thongbao");
            }
            else
            {

                string userName = getUserName();
                int idTruong = Cmon.getIdtruong(userName, db);
                string namHoc = Cmon.GetNamHienTai(idTruong);
                string tenTruong = Cmon.GetnameSchool(idTruong);
                var tenGiaoVienLists = (
                                     from usrs in db.ScrUsers
                                     join ul in db.Users_LopHoc on usrs.UserName equals ul.UserName
                                     join RU in db.ScrJRoleUsers on ul.UserName equals RU.UserName
                                     join RF in db.ScrJRoleFunctions on RU.RoleName equals RF.RoleName
                                     where ul.idTruong == idTruong && ul.NamHoc == namHoc && ul.MaLop == maLop
                                     && RF.Enable == true
                                     && RF.Read_ == true
                                     && RF.Insert_ == true
                                     && RF.Update_ == true
                                     && RF.FunctionID == 118
                                     select new user
                                     {
                                         fullName = usrs.FullName
                                     }).Distinct().ToList();

               
                List<VnEduPlus.Models.user> users = (List<VnEduPlus.Models.user>)tenGiaoVienLists;
                string giaoVienChuNhiem = "";
                if (users != null && users.Count > 0)
                {
                    for (int i = 0; i < users.Count; i++)
                    {
                        giaoVienChuNhiem += users[i].fullName + ";";
                    }
                }

                giaoVienChuNhiem = giaoVienChuNhiem.Trim().Length > 0 ? giaoVienChuNhiem.Substring(0, giaoVienChuNhiem.LastIndexOf(";")) : "";


                var lopHoc = (from lophoc in db.LopHocs
                              let maLops = (from userLop in db.Users_LopHoc
                                            join userMon in db.Users_Monhoc
                                            on userLop.UserName equals userMon.UserName
                                            where userLop.idTruong == idTruong && userLop.NamHoc == namHoc && userLop.UserName == userName
                                            select userLop.MaLop).Distinct()
                              where maLops.Contains(lophoc.MaLop) && lophoc.idTruong == idTruong && lophoc.NamHoc == namHoc
                              select lophoc).ToList();

                LopHoc emptyLopHoc = new LopHoc();
                emptyLopHoc.MaLop = "-1";
                emptyLopHoc.TenLop = "--Chọn lớp học--";
                lopHoc.Insert(0, emptyLopHoc);
                ViewBag.lopHoc = lopHoc;
                ViewBag.maLop = maLop;

                int[] thangs = { 1, 2, 3, 4, 5, 8, 9, 10, 11, 12 };
                if (!thangs.Contains(thang))
                {
                    thang = 8;
                }

                ViewBag.thang = thang;
                ViewBag.maKhoi = Cmon.getKhoi(maLop, namHoc, idTruong);
                MauCauNhanXet mauCauNhanXet = new MauCauNhanXet();

                var dsKienThuc = (from dskt in db.DM_NhanXetMau_GVCN
                                  where dskt.idTruong == idTruong && dskt.LoaiNhanXet == 1 && dskt.Thang == thang
                                  select new DmTieuChi
                                  {
                                      maNhanXet = dskt.MaNhanXet,
                                      tenNhanXet = dskt.TenNhanXet

                                  }).ToList();



                var dsNangLuc = (from dskt in db.DM_NhanXetMau_GVCN
                                 where dskt.idTruong == idTruong && dskt.LoaiNhanXet == 2 && dskt.Thang == thang
                                 select new DmTieuChi
                                 {
                                     maNhanXet = dskt.MaNhanXet,
                                     tenNhanXet = dskt.TenNhanXet

                                 }).ToList();

                var dsPhamChat = (from dskt in db.DM_NhanXetMau_GVCN
                                  where dskt.idTruong == idTruong && dskt.LoaiNhanXet == 3 && dskt.Thang == thang
                                  select new DmTieuChi
                                  {
                                      maNhanXet = dskt.MaNhanXet,
                                      tenNhanXet = dskt.TenNhanXet

                                  }).ToList();


                string maKhoi = Cmon.getKhoi(maLop, namHoc, idTruong);


                string tenLop = Cmon.GetnameClass(maLop, idTruong, namHoc);

                List<HocSinhNhanXetNhom> danhSachHocSinh = getDanhSachHocSinh(idTruong, thang, maLop, namHoc);
                if (danhSachHocSinh != null && danhSachHocSinh.Count > 0)
                {
                    ExcelFileWriterSoLienLac<object> myExcel = new ExcelWriteSoLienLac();
                    string tenFile = "SoCN_Template.xls";

                    // định nghĩa thư mục chứa template
                    string rootPathToTemplate = Server.MapPath("~/templateExcell/" + tenFile);

                    string fileName = userName + "_SoChuNhiem";

                    string rootProcessTemplate = Server.MapPath(System.IO.Path.GetDirectoryName("~/ProcessTemplate/")); // thư mục chứa các template sẽ được đem xử lý đổ dữ liệu

                    string fileNameExcute = rootProcessTemplate + "\\" + fileName + ".xls"; // tạo một template để chuẩn bị cho xử lý
                    System.IO.File.Delete(fileNameExcute);// xóa file tạm trước đó và tạo lại
                    System.IO.File.Copy(rootPathToTemplate, fileNameExcute, true);// copy từ thư mục template tới thư mục chuẩn bị xử lý
                    string tenPhongGiaoDuc = Cmon.getDonViChuQuan(idTruong);
                    // Xử lý đổ dữ liệu vào các ô tương ứng
                    myExcel.ExportSoChuNhiemToExcel(fileNameExcute, thang, namHoc, tenTruong, tenLop, danhSachHocSinh, giaoVienChuNhiem, tenPhongGiaoDuc);

                    System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
                    Cmon.stopExcel();
                    return File(fileNameExcute, "application/ms-excel", "SoChuNhiemTheoDoiChatLuongGiaoDuc.xls");


                }


            }
            return File("PhieuLienLac.xls", "application/ms-excel", "PhieuLienLac.xls");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Authorize]
        public ActionResult hienDanhSachNhanXetSoChuNhiem(int idTruong, int thang, string maLop)
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Thongbao");

            }


            string namHoc = Cmon.GetNamHienTai(idTruong);
            string userName = getUserName();
            ViewBag.maKhoi = Cmon.getKhoi(maLop, namHoc, idTruong);

            var tenGiaoVienLists = (
                                     from usrs in db.ScrUsers
                                     join ul in db.Users_LopHoc on usrs.UserName equals ul.UserName
                                     join RU in db.ScrJRoleUsers on ul.UserName equals RU.UserName
                                     join RF in db.ScrJRoleFunctions on RU.RoleName equals RF.RoleName
                                     where ul.idTruong == idTruong && ul.NamHoc == namHoc && ul.MaLop == maLop
                                     && RF.Enable == true
                                     && RF.Read_ == true
                                     && RF.Insert_ == true
                                     && RF.Update_ == true
                                     && RF.FunctionID == 118
                                     select new user
                                     {
                                         fullName = usrs.FullName
                                     }).Distinct().ToList();


            List<VnEduPlus.Models.user> users = (List<VnEduPlus.Models.user>)tenGiaoVienLists;
            string giaoVienChuNhiem = "";
            if (users != null && users.Count > 0)
            {
                for (int i = 0; i < users.Count; i++)
                {
                    giaoVienChuNhiem += users[i].fullName + ";";
                }
            }

            giaoVienChuNhiem = giaoVienChuNhiem.Trim().Length > 0 ? giaoVienChuNhiem.Substring(0, giaoVienChuNhiem.LastIndexOf(";")) : "";

            ViewBag.tenusers = giaoVienChuNhiem; 
            
            MauCauNhanXet mauCauNhanXet = new MauCauNhanXet();

            var dsKienThuc = (from dskt in db.DM_NhanXetMau_GVCN
                              where dskt.idTruong == idTruong && dskt.LoaiNhanXet == 1 && dskt.Thang == thang
                              select new DmTieuChi
                              {
                                  maNhanXet = dskt.MaNhanXet,
                                  tenNhanXet = dskt.TenNhanXet

                              }).ToList();



            var dsNangLuc = (from dskt in db.DM_NhanXetMau_GVCN
                             where dskt.idTruong == idTruong && dskt.LoaiNhanXet == 2 && dskt.Thang == thang
                             select new DmTieuChi
                             {
                                 maNhanXet = dskt.MaNhanXet,
                                 tenNhanXet = dskt.TenNhanXet

                             }).ToList();

            var dsPhamChat = (from dskt in db.DM_NhanXetMau_GVCN
                              where dskt.idTruong == idTruong && dskt.LoaiNhanXet == 3 && dskt.Thang == thang
                              select new DmTieuChi
                              {
                                  maNhanXet = dskt.MaNhanXet,
                                  tenNhanXet = dskt.TenNhanXet

                              }).ToList();

            DmTieuChi dmNX = new DmTieuChi();
            dmNX.maNhanXet = "-1";
            dmNX.tenNhanXet = "-- Chọn mẫu câu --";
            dsKienThuc.Insert(0, dmNX);
            dsNangLuc.Insert(0, dmNX);
            dsPhamChat.Insert(0, dmNX);

            mauCauNhanXet.hocSinhNhanXetNhom = getDanhSachHocSinh(idTruong, thang, maLop, namHoc);
            mauCauNhanXet.DMKienThuc = dsKienThuc;
            mauCauNhanXet.DMNangLuc = dsNangLuc;
            mauCauNhanXet.DMPhamChat = dsPhamChat;

            var QuyenNhapDiem = (from RU in db.ScrJRoleUsers
                                 join RF in db.ScrJRoleFunctions
                                     on RU.RoleName equals RF.RoleName
                                 where RU.UserName == userName && RF.idTruong == idTruong
                                 && RF.FunctionID == 112 && RF.Insert_ == true && RF.Enable == true && RF.Update_ == true
                                 select RF
                      ).FirstOrDefault();
            ViewBag.phanQuyen = QuyenNhapDiem != null ? 1 : 0;
            ViewBag.giaoVienChuNhiems = giaoVienChuNhiem;

            return View(@"~/Views/GiaoVienChuNhiemVaLopHoc/NoiDungSoChuNhiem.cshtml", mauCauNhanXet);

        }



        [AcceptVerbs(HttpVerbs.Get)]
        [Authorize]
        public ActionResult soChuNhiem(int idTruong, int thang, string maLop)
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Thongbao");

            }


            string userName = getUserName();
            string namHoc = Cmon.GetNamHienTai(idTruong);
            ViewBag.namhoc = namHoc;
            ViewBag.idtruong = idTruong;
            ViewBag.tenTruong = Cmon.GetnameSchool(idTruong);
            ViewBag.danhMucThang = Cmon.createCacThangCoDinh(idTruong, db);

            var tenGiaoVienLists = (
                                     from usrs in db.ScrUsers
                                     join ul in db.Users_LopHoc on usrs.UserName equals ul.UserName
                                     join RU in db.ScrJRoleUsers on ul.UserName equals RU.UserName
                                     join RF in db.ScrJRoleFunctions on RU.RoleName equals RF.RoleName
                                     where ul.idTruong == idTruong && ul.NamHoc == namHoc && ul.MaLop == maLop && RF.idTruong == idTruong
                                     && RF.Enable == true
                                     && RF.Read_ == true
                                     && RF.Insert_ == true
                                     && RF.Update_ == true
                                     && RF.FunctionID == 118
                                     select new user
                                     {
                                         fullName = usrs.FullName
                                     }).Distinct().ToList();


            List<VnEduPlus.Models.user> users = (List<VnEduPlus.Models.user>)tenGiaoVienLists;
            string giaoVienChuNhiem = "";
            if (users != null && users.Count > 0)
            {
                for (int i = 0; i < users.Count; i++)
                {
                    giaoVienChuNhiem += users[i].fullName + ";";
                }
            }

            giaoVienChuNhiem = giaoVienChuNhiem.Trim().Length > 0 ? giaoVienChuNhiem.Substring(0, giaoVienChuNhiem.LastIndexOf(";")) : "";
            ViewBag.tenusers = giaoVienChuNhiem; 
            // Load lop hoc cua user
            
            var lopHoc = (from lophoc in db.LopHocs
                          let maLops = (from userLop in db.Users_LopHoc
                                       join userMon in db.Users_Monhoc
                                       on userLop.UserName equals userMon.UserName
                                       where userLop.idTruong == idTruong && userLop.NamHoc == namHoc && userLop.UserName == userName
                                       select userLop.MaLop).Distinct()
                          where maLops.Contains(lophoc.MaLop) && lophoc.idTruong == idTruong && lophoc.NamHoc == namHoc
                          select lophoc).ToList();

            LopHoc emptyLopHoc = new LopHoc();
            emptyLopHoc.MaLop = "-1";
            emptyLopHoc.TenLop = "--Chọn lớp học--";
            lopHoc.Insert(0, emptyLopHoc);
            ViewBag.lopHoc = lopHoc;
            ViewBag.maLop = maLop;

            int[] thangs = { 1, 2, 3, 4, 5, 8, 9, 10, 11, 12 };
            if (!thangs.Contains(thang))
            {
                thang = 8;
            }

            ViewBag.thang = thang; 
            ViewBag.maKhoi = Cmon.getKhoi(maLop, namHoc, idTruong);
            MauCauNhanXet mauCauNhanXet = new MauCauNhanXet();

            var dsKienThuc = (from dskt in db.DM_NhanXetMau_GVCN
                              where dskt.idTruong == idTruong && dskt.LoaiNhanXet == 1 && dskt.Thang == thang
                              select new DmTieuChi
                              {
                                  maNhanXet = dskt.MaNhanXet,
                                  tenNhanXet = dskt.TenNhanXet

                              }).ToList();



            var dsNangLuc = (from dskt in db.DM_NhanXetMau_GVCN
                             where dskt.idTruong == idTruong && dskt.LoaiNhanXet == 2 && dskt.Thang == thang
                             select new DmTieuChi
                             {
                                 maNhanXet = dskt.MaNhanXet,
                                 tenNhanXet = dskt.TenNhanXet

                             }).ToList();

            var dsPhamChat = (from dskt in db.DM_NhanXetMau_GVCN
                              where dskt.idTruong == idTruong && dskt.LoaiNhanXet == 3 && dskt.Thang == thang
                              select new DmTieuChi
                              {
                                  maNhanXet = dskt.MaNhanXet,
                                  tenNhanXet = dskt.TenNhanXet

                              }).ToList();

            DmTieuChi dmNX = new DmTieuChi();
            dmNX.maNhanXet = "-1";
            dmNX.tenNhanXet = "-- Chọn mẫu câu --";
            dsKienThuc.Insert(0, dmNX);
            dsNangLuc.Insert(0, dmNX);
            dsPhamChat.Insert(0, dmNX);

            mauCauNhanXet.hocSinhNhanXetNhom = getDanhSachHocSinh(idTruong, thang, maLop, namHoc);
            mauCauNhanXet.DMKienThuc = dsKienThuc;
            mauCauNhanXet.DMNangLuc = dsNangLuc;
            mauCauNhanXet.DMPhamChat = dsPhamChat;

            var QuyenNhapDiem = (from RU in db.ScrJRoleUsers
                                 join RF in db.ScrJRoleFunctions
                                     on RU.RoleName equals RF.RoleName
                                 where RU.UserName == userName && RF.idTruong == idTruong
                                 && RF.FunctionID == 112 && RF.Insert_ == true && RF.Enable == true && RF.Update_ == true
                                 select RF
                      ).FirstOrDefault();
            ViewBag.phanQuyen = QuyenNhapDiem != null ? 1 : 0;
            ViewBag.PhongGiaoDuc = Cmon.getDonViChuQuan(idTruong);
            return View(@"~/Views/GiaoVienChuNhiemVaLopHoc/SoChuNhiem.cshtml", mauCauNhanXet);
        }


        #endregion


        #endregion Quản lý nhận xét


        #region Quản lý nhận xét theo học kỳ

            public ActionResult TemplateNhanXetKy()
            {
                if (!Request.IsAuthenticated)
                {
                    return RedirectToAction("Index", "Thongbao");

                }

                // lay thong tin truong 
                string userName = getUserName();
                int idTruong = Cmon.getIdtruong(userName, db);
                ViewBag.namhoc = Cmon.GetNamHienTai(idTruong); 
                ViewBag.idtruong = idTruong;
                ViewBag.danhMucTieuChi = Cmon.createDanhMuc();
                ViewBag.danhMucThang = Cmon.createCacThangCoDinh(idTruong, db);
                ViewBag.tenuser = Cmon.GetnameUser(userName); 
                ViewBag.danhmucMonHoc = LoadMonhoc();
                ViewBag.nangLuc = Cmon.CreateNangLuc();
                ViewBag.phamChat = Cmon.CreatePhamChat();
                ViewBag.hocKy = Cmon.CreateHocKy();

                return View();
            }


            /// <summary>
            /// id : la ma lop
            /// load nhung mon hoc thuoc mot lop hoc
            /// </summary>
            /// <param name="id"></param>
            /// <returns></returns>
            
            public List<MonHoc> LoadMonhoc()
            {
                string userName = getUserName();
                int idTruong = Cmon.getIdtruong(userName, db);
                string namhoc = Cmon.GetNamHienTai(idTruong);

                List<MonHoc> monHocs =  new List<MonHoc>();
                MonHoc mhoc = new MonHoc();
                mhoc.maMonHoc="-1";
                mhoc.tenMonHoc="--Lựa chọn môn học--";
                monHocs.Add(mhoc);
                 


                var modelData = (from monhoc in db.ASSIGN_LOP_MonHoc
                                 join userMon in db.Users_LopHoc
                                 on monhoc.MaLop equals userMon.MaLop
                                 where userMon.UserName == userName && userMon.NamHoc== namhoc && userMon.idTruong == idTruong
                                 orderby monhoc.MaMonHoc
                                 select new MonHoc
                                 {
                                    maMonHoc= monhoc.MaMonHoc,
                                    tenMonHoc= monhoc.TenMonHoc

                                 }).Distinct().ToList();

                modelData.Insert(0, mhoc);
                 
                return modelData;
            }
        
        #region Quản lý nhận xét theo học kỳ

            [AcceptVerbs(HttpVerbs.Get)]
            [Authorize]
            public ActionResult hienDanhSachNhanXetTheoHocKy(int idTruong, int type, int hocKy, string maMonHoc)
            {
                if (!Request.IsAuthenticated)
                {
                    return RedirectToAction("Index", "Thongbao");

                }
                 
                var dsNhanXet = (from nhanxet in db.DM_NhanXetMau_CuoiKyNam
                                 where nhanxet.idTruong == idTruong && nhanxet.LoaiNhanXet == type && nhanxet.MaMonHoc == maMonHoc && nhanxet.HocKy == hocKy
                                 orderby nhanxet.TenNhanXet
                                 select nhanxet).ToList();

                return PartialView("HienThiDanhSachNhanXetTheoHocKy", dsNhanXet);
            }


            [AcceptVerbs(HttpVerbs.Post)]
            [Authorize]
            public JsonResult LuuNhanXetGiaoVienChuNhiem(NoiDungNhanXetThangs noiDungNhanXet)
            {

                string mess = "OK";
                string userName = getUserName();
                int idTruong = noiDungNhanXet.idTruong;
                string namHoc = Cmon.GetNamHienTai(idTruong);
                string maLop = noiDungNhanXet.maLop;
                string maHS = noiDungNhanXet.maHs;
                string hoTen = Cmon.getHoTenHocSinh(maHS, idTruong, maLop, namHoc);
                DateTime ngayTao = DateTime.Now; 

                try
                {
                    bool isExitsMaHS = Cmon.isExitsMaHocSinh(idTruong, namHoc, maLop, maHS, db);
                    if (!isExitsMaHS)
                        {
                            // thêm mới 
                            //ThemMoiNhanXets(idTruong, thang, maLop, namHoc, boNX, userName);
                            //1. Them thong tin hoc sinh vao truoc
                            // 2. goi ham cap nhat thong tin nhan xet cho hoc sinh 
                           ThemMoiNhanXetHocSinh(idTruong,namHoc, maLop, maHS,hoTen,userName,ngayTao);
                           CapNhatNhanXets(idTruong, maLop, namHoc, maHS, noiDungNhanXet.noiDungNhanXets, userName);
                        }
                        else
                        {
                            // cập nhật
                            CapNhatNhanXets(idTruong, maLop, namHoc, maHS, noiDungNhanXet.noiDungNhanXets, userName);
                        }
                     
                }
                catch (Exception ex)
                {
                    mess = ex.ToString();

                }
                 
                return Json(mess, JsonRequestBehavior.AllowGet);
            }


         [AcceptVerbs(HttpVerbs.Post)]
            [Authorize]
            public JsonResult XuLyGanNhanXetNhomHocSinh(int idTruong, int thang, string maLop, List<TapNhanXet> nhanXets)
            {

                string mess = "OK";
               string userName = getUserName();
                string namHoc = Cmon.GetNamHienTai(idTruong);
                try
                {
                    foreach (TapNhanXet boNX in nhanXets)
                    {
                        bool isExitsMaHS = Cmon.isExitsMaHocSinh(idTruong, namHoc, maLop, boNX.maHS, db);
                        if (!isExitsMaHS)
                        {
                            // thêm mới 
                            ThemMoiBoCauNhanXet(idTruong, thang, maLop, namHoc, boNX, userName);
                        }
                        else
                        {
                            // cập nhật
                            CapNhatBoCauNhanXet(idTruong, thang, maLop, namHoc, boNX, userName);
                        }

                    }

                }
                catch (Exception ex)
                {
                    mess = ex.ToString();
                    
                }
              
              
                
                return Json(mess, JsonRequestBehavior.AllowGet);
            }

         public void ThemMoiBoCauNhanXet(int idTruong, int thang, string maLop, string namHoc, TapNhanXet nhanXet, string usrName)
         {
             HocSinh_DanhGiaThang_GVCN GVCNNX = new HocSinh_DanhGiaThang_GVCN();

             GVCNNX.idTruong = idTruong;
             GVCNNX.NamHoc = namHoc;
             GVCNNX.MaHocSinh = nhanXet.maHS;
             GVCNNX.MaHocSinhFriendly = nhanXet.maHS;
             GVCNNX.HoTen = nhanXet.hoTen;
             GVCNNX.MaLop = maLop;
             GVCNNX.UserCreated = usrName;
             GVCNNX.DateCreated = DateTime.Now;
             switch (thang)
             { 
                 case 8:
                       GVCNNX.NXT1_KT = nhanXet.kienThuc;
                       GVCNNX.NXT1_NL = nhanXet.nangLuc; 
                       GVCNNX.NXT1_PC = nhanXet.phamChat;
                       break;

                 case 9:
                     GVCNNX.NXT2_KT = nhanXet.kienThuc; 
                     GVCNNX.NXT2_NL = nhanXet.nangLuc; 
                     GVCNNX.NXT2_PC = nhanXet.phamChat; 
                     break;

                 case 10:
                      GVCNNX.NXT3_KT = nhanXet.kienThuc; 
                     GVCNNX.NXT3_NL = nhanXet.nangLuc; 
                     GVCNNX.NXT3_PC = nhanXet.phamChat; 
                     break;

                 case 11:
                     GVCNNX.NXT4_KT = nhanXet.kienThuc; 
                     GVCNNX.NXT4_NL = nhanXet.nangLuc; 
                     GVCNNX.NXT4_PC = nhanXet.phamChat; 
                     break;

                 case 12:
                     GVCNNX.NXT5_KT = nhanXet.kienThuc; 
                     GVCNNX.NXT5_NL = nhanXet.nangLuc; 
                     GVCNNX.NXT5_PC = nhanXet.phamChat; 
                     break;

                 case 1:
                     GVCNNX.NXT6_KT = nhanXet.kienThuc; 
                     GVCNNX.NXT6_NL = nhanXet.nangLuc; 
                     GVCNNX.NXT6_PC = nhanXet.phamChat; 
                     break;

                 case 2:
                     GVCNNX.NXT7_KT = nhanXet.kienThuc; 
                     GVCNNX.NXT7_NL = nhanXet.nangLuc; 
                     GVCNNX.NXT7_PC = nhanXet.phamChat; 
                     break;

                 case 3:
                     GVCNNX.NXT8_KT = nhanXet.kienThuc; 
                     GVCNNX.NXT8_NL = nhanXet.nangLuc; 
                     GVCNNX.NXT8_PC = nhanXet.phamChat; 
                     break;

                 case 4:
                     GVCNNX.NXT9_KT = nhanXet.kienThuc; 
                     GVCNNX.NXT9_NL = nhanXet.nangLuc; 
                     GVCNNX.NXT9_PC = nhanXet.phamChat; 
                     break;

                 case 5:
                     GVCNNX.NXT10_KT = nhanXet.kienThuc; 
                     GVCNNX.NXT10_NL = nhanXet.nangLuc; 
                     GVCNNX.NXT10_PC = nhanXet.phamChat; 
                     break;

                 default: break;

             }// end switch


                db.HocSinh_DanhGiaThang_GVCN.AddObject(GVCNNX);
                db.SaveChanges();
                
         }
         

         public void CapNhatBoCauNhanXet(int idTruong, int thang, string maLop, string namHoc, TapNhanXet nhanXet, string usrName)
         {
             var hocSinh = (from hocsinh in db.HocSinh_DanhGiaThang_GVCN where hocsinh.idTruong == idTruong && hocsinh.NamHoc == namHoc && hocsinh.MaLop == maLop && hocsinh.MaHocSinh == nhanXet.maHS select hocsinh).FirstOrDefault();
              
                 hocSinh.UserCreated = usrName;
                 hocSinh.DateUpdated = DateTime.Now;
                 switch (thang)
                 {
                     case 8:
                         hocSinh.NXT1_KT = nhanXet.kienThuc; 
                         hocSinh.NXT1_NL = nhanXet.nangLuc; 
                         hocSinh.NXT1_PC = nhanXet.phamChat; 
                         break;

                     case 9:
                         hocSinh.NXT2_KT = nhanXet.kienThuc; 
                         hocSinh.NXT2_NL = nhanXet.nangLuc; 
                         hocSinh.NXT2_PC = nhanXet.phamChat; 
                         break;

                     case 10:
                         hocSinh.NXT3_KT = nhanXet.kienThuc; 
                         hocSinh.NXT3_NL = nhanXet.nangLuc; 
                         hocSinh.NXT3_PC = nhanXet.phamChat; 
                         break;

                     case 11:
                         hocSinh.NXT4_KT = nhanXet.kienThuc; 
                         hocSinh.NXT4_NL = nhanXet.nangLuc; 
                         hocSinh.NXT4_PC = nhanXet.phamChat; 
                         break;

                     case 12:
                         hocSinh.NXT5_KT = nhanXet.kienThuc; 
                         hocSinh.NXT5_NL = nhanXet.nangLuc; 
                         hocSinh.NXT5_PC = nhanXet.phamChat; 
                         break;

                     case 1:
                         hocSinh.NXT6_KT = nhanXet.kienThuc; 
                         hocSinh.NXT6_NL = nhanXet.nangLuc; 
                         hocSinh.NXT6_PC = nhanXet.phamChat; 
                         break;

                     case 2:
                         hocSinh.NXT7_KT = nhanXet.kienThuc; 
                         hocSinh.NXT7_NL = nhanXet.nangLuc; 
                         hocSinh.NXT7_PC = nhanXet.phamChat; 
                         break;

                     case 3:
                         hocSinh.NXT8_KT = nhanXet.kienThuc; 
                         hocSinh.NXT8_NL = nhanXet.nangLuc; 
                         hocSinh.NXT8_PC = nhanXet.phamChat; 
                         break;

                     case 4:
                         hocSinh.NXT9_KT = nhanXet.kienThuc; 
                         hocSinh.NXT9_NL = nhanXet.nangLuc; 
                         hocSinh.NXT9_PC = nhanXet.phamChat; 
                         break;

                     case 5:
                         hocSinh.NXT10_KT = nhanXet.kienThuc; 
                         hocSinh.NXT10_NL = nhanXet.nangLuc; 
                         hocSinh.NXT10_PC = nhanXet.phamChat; 
                         break;

                     default: break;

                 }// end switch
           

              db.SaveChanges();
             
         }
         
        /// ThemMoiNhanXetHocSinh(idTruong,namHoc, maLop, maHS,hoTen,userName,ngayTao)
        // Them moi nhan xet
         public void ThemMoiNhanXetHocSinh(int idTruong, string namHoc, string maLop, string maHS, string hoTen, string userName, DateTime ngayTao)
         {
             HocSinh_DanhGiaThang_GVCN GVCNNX = new HocSinh_DanhGiaThang_GVCN();
             GVCNNX.idTruong = idTruong;
             GVCNNX.NamHoc = namHoc;
             GVCNNX.MaLop = maLop;
             GVCNNX.MaHocSinh = maHS;
             GVCNNX.MaHocSinhFriendly = maHS;
             GVCNNX.HoTen = hoTen;
             GVCNNX.UserCreated = userName;
             GVCNNX.DateCreated = ngayTao;
             db.AddToHocSinh_DanhGiaThang_GVCN(GVCNNX);
             db.SaveChanges();

         }
        //-- Cap nhat nhan xet cua giao vien chur nhiem  ---

         public void CapNhatNhanXets(int idTruong, string maLop, string namHoc, string maHS, List<noiDungNhanXet> noiDungNhanXets, string usrName)
         {
             var hocSinh = (from hocsinh in db.HocSinh_DanhGiaThang_GVCN where hocsinh.idTruong == idTruong && hocsinh.NamHoc == namHoc && hocsinh.MaLop == maLop && hocsinh.MaHocSinh == maHS select hocsinh).FirstOrDefault();

             hocSinh.UserCreated = usrName;
             hocSinh.DateUpdated = DateTime.Now;

             foreach (noiDungNhanXet noiDungNX in noiDungNhanXets)
             {
                 string thang = noiDungNX.id.Substring(2);
                 string type = noiDungNX.id.Substring(0, 2);

                 if (thang == "8")
                 {
                     if (type == "nl")
                     {
                         hocSinh.NXT1_NL = noiDungNX.value;
                     }

                     if (type == "pc")
                     {
                         hocSinh.NXT1_PC = noiDungNX.value;
                     }

                     if (type == "kt")
                     {
                         hocSinh.NXT1_KT = noiDungNX.value;
                     } 

                 }

                 if (thang == "9")
                 {
                     if (type == "nl")
                     {
                         hocSinh.NXT2_NL = noiDungNX.value;
                     }

                     if (type == "pc")
                     {
                         hocSinh.NXT2_PC = noiDungNX.value;
                     }

                     if (type == "kt")
                     {
                         hocSinh.NXT2_KT = noiDungNX.value;
                     }

                 }


                 if (thang == "10")
                 {
                     if (type == "nl")
                     {
                         hocSinh.NXT3_NL = noiDungNX.value;
                     }

                     if (type == "pc")
                     {
                         hocSinh.NXT3_PC = noiDungNX.value;
                     }

                     if (type == "kt")
                     {
                         hocSinh.NXT3_KT = noiDungNX.value;
                     }
                 }
                      

                if (thang == "11")
                {
                    if (type == "nl")
                    {
                        hocSinh.NXT4_NL = noiDungNX.value;
                    }

                    if (type == "pc")
                    {
                        hocSinh.NXT4_PC = noiDungNX.value;
                    }

                    if (type == "kt")
                    {
                        hocSinh.NXT4_KT = noiDungNX.value;
                    }

                }


                if (thang == "12")
                {
                    if (type == "nl")
                    {
                        hocSinh.NXT5_NL = noiDungNX.value;
                    }

                    if (type == "pc")
                    {
                        hocSinh.NXT5_PC = noiDungNX.value;
                    }

                    if (type == "kt")
                    {
                        hocSinh.NXT5_KT = noiDungNX.value;
                    }

                }

                if (thang == "1")
                {
                    if (type == "nl")
                    {
                        hocSinh.NXT6_NL = noiDungNX.value;
                    }

                    if (type == "pc")
                    {
                        hocSinh.NXT6_PC = noiDungNX.value;
                    }

                    if (type == "kt")
                    {
                        hocSinh.NXT6_KT = noiDungNX.value;
                    }

                }

                if (thang == "2")
                {
                    if (type == "nl")
                    {
                        hocSinh.NXT7_NL = noiDungNX.value;
                    }

                    if (type == "pc")
                    {
                        hocSinh.NXT7_PC = noiDungNX.value;
                    }

                    if (type == "kt")
                    {
                        hocSinh.NXT7_KT = noiDungNX.value;
                    }

                }

                if (thang == "3")
                {
                    if (type == "nl")
                    {
                        hocSinh.NXT8_NL = noiDungNX.value;
                    }

                    if (type == "pc")
                    {
                        hocSinh.NXT8_PC = noiDungNX.value;
                    }

                    if (type == "kt")
                    {
                        hocSinh.NXT8_KT = noiDungNX.value;
                    }

                }

                if (thang == "4")
                {
                    if (type == "nl")
                    {
                        hocSinh.NXT9_NL = noiDungNX.value;
                    }

                    if (type == "pc")
                    {
                        hocSinh.NXT9_PC = noiDungNX.value;
                    }

                    if (type == "kt")
                    {
                        hocSinh.NXT9_KT = noiDungNX.value;
                    }

                }

                if (thang == "5")
                {
                    if (type == "nl")
                    {
                        hocSinh.NXT10_NL = noiDungNX.value;
                    }

                    if (type == "pc")
                    {
                        hocSinh.NXT10_PC = noiDungNX.value;
                    }

                    if (type == "kt")
                    {
                        hocSinh.NXT10_KT = noiDungNX.value;
                    }

                } 
             
                 } //end for
            
              
             db.SaveChanges();

         }


        #endregion


        #region Quản lý thêm mới nhận xét theo học kỳ 

            public ActionResult AddNhanXetTheoHocKy()
            {
                return PartialView(@"~/Views/GiaoVienChuNhiem/AddNhanXetTheoHocKy.cshtml");
            }

            [HttpGet]
            public JsonResult ThemMoiNhanXetTheoHocKy(int type, string maNhanXet, string tenNhanXet, bool isdefault, int hocKy, int idtruong, string maMonHoc)
            {
                string mess = "";
                if (string.IsNullOrEmpty(maNhanXet.Trim()))
                    mess = "Yêu cầu nhập mã nhận xét";
                else if (string.IsNullOrEmpty(tenNhanXet.Trim()))
                    mess = "Yêu cầu nhập tên nhận xét";
                else
                {
                    Cmon.UpdateStatusNhanXetTheoHocKy(type, maMonHoc,hocKy, db);
                    DM_NhanXetMau_CuoiKyNam kt = new DM_NhanXetMau_CuoiKyNam();
                    kt.HocKy =(short) hocKy;
                    kt.idTruong = idtruong;
                    kt.MaMonHoc = maMonHoc;
                    kt.LoaiNhanXet = (short)type;
                    kt.TenNhanXet = tenNhanXet;
                    kt.MaNhanXet = maNhanXet.Trim();
                    kt.UserCreated = getUserName();
                    kt.isDefault = isdefault;
                    db.DM_NhanXetMau_CuoiKyNam.AddObject(kt);
                    int rs = db.SaveChanges();
                    if (rs > 0)
                        mess = "OK";
                    else
                        mess = "Thêm mới tiêu chí không thành công!";
                }
                return Json(mess, JsonRequestBehavior.AllowGet);
            }

        #endregion

        #region Quản lý sửa nhận xét theo học kỳ

            public ActionResult EditNhanXetTheoHocKy(string loaiNhanXet, string manx, int hocKy, int idTruong, string maMonHoc)
            {

                short hky = Convert.ToInt16(hocKy);
                short loaiNX = Convert.ToInt16(loaiNhanXet);
                var kthuc = (from kt in db.DM_NhanXetMau_CuoiKyNam.Where(t => t.LoaiNhanXet == loaiNX && t.HocKy == hky && t.MaNhanXet == manx && t.idTruong == idTruong && t.MaMonHoc == maMonHoc) select kt).FirstOrDefault();

                return PartialView(@"~/Views/GiaoVienChuNhiem/EditNhanXetTheoHocKy.cshtml", kthuc);
            }

            [HttpGet]
            public JsonResult UpdateNhanXetTheoHocKy(int type, string maNhanXet, string tenNhanXet, bool isdefault, int hocKy, int idtruong, string maMonHoc)
            {
                string mess = "";
                if (string.IsNullOrEmpty(maNhanXet.Trim()))
                    mess = "Yêu cầu nhập mã nhận xét";
                else if (string.IsNullOrEmpty(tenNhanXet.Trim()))
                    mess = "Yêu cầu nhập tên nhận xét";
                else
                {
                    Cmon.UpdateStatusNhanXetTheoHocKy(type, maMonHoc,hocKy, db);
                    short hKy = Convert.ToInt16(hocKy);
                    short loaiNhanXet = Convert.ToInt16(type);
                    DM_NhanXetMau_CuoiKyNam kt = (from kthuc in db.DM_NhanXetMau_CuoiKyNam.Where(t => t.LoaiNhanXet == loaiNhanXet && t.MaNhanXet == maNhanXet && t.HocKy == hKy && t.idTruong == idtruong && t.MaMonHoc == maMonHoc) select kthuc).FirstOrDefault();
                    if (kt != null)
                    {
                        kt.MaNhanXet = maNhanXet.Trim();
                        kt.TenNhanXet = tenNhanXet;
                        kt.isDefault = isdefault;
                        int rs = db.SaveChanges();
                        if (rs > 0)
                            mess = "OK";
                        else
                            mess = "Cập nhật nhận xét không thành công!";
                    }
                    else
                    {
                        mess = "Không có thông tin cập nhật!";
                    }
                }
                return Json(mess, JsonRequestBehavior.AllowGet);
            }


            #endregion


        #region Quản lý xóa nhận xét
            [HttpGet]
            public JsonResult DeleteNhanXetTheoHocKy(int type, string maNhanXet, int hocKy, int idtruong, string maMonHoc)
            {
                string mess = "";
                short hky = Convert.ToInt16(hocKy);
                DM_NhanXetMau_CuoiKyNam kt = (from kthuc in db.DM_NhanXetMau_CuoiKyNam.Where(t => t.MaNhanXet == maNhanXet && t.LoaiNhanXet == type && t.MaMonHoc == maMonHoc && t.HocKy == hky && t.idTruong == idtruong) select kthuc).FirstOrDefault();
                if (kt != null)
                {
                    db.DM_NhanXetMau_CuoiKyNam.DeleteObject(kt);
                    int del = db.SaveChanges();
                    if (del > 0)
                        mess = "OK";
                    else
                        mess = "Xóa tiêu chí không thành công!";
                }
                else
                {
                    mess = "Không có thông tin để xóa!";
                }
                return Json(mess, JsonRequestBehavior.AllowGet);
            }

            #endregion

        #endregion Quản lý nhận xét theo học kỳ

    }
    
}
