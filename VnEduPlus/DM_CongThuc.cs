//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace VnEduPlus
{
    using System;
    using System.Collections.Generic;
    
    public partial class DM_CongThuc
    {
        public int idTruong { get; set; }
        public string NamHoc { get; set; }
        public string MaCongThuc { get; set; }
        public string TenCongThuc { get; set; }
        public string CongThuc { get; set; }
        public bool IsHienTai { get; set; }
        public string Ghichu { get; set; }
    
        public virtual DM_NamHoc DM_NamHoc { get; set; }
        public virtual DM_Truong DM_Truong { get; set; }
    }
}
