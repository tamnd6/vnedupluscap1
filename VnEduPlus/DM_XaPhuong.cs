//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace VnEduPlus
{
    using System;
    using System.Collections.Generic;
    
    public partial class DM_XaPhuong
    {
        public DM_XaPhuong()
        {
            this.DM_KhoiPho = new HashSet<DM_KhoiPho>();
            this.DM_Truong = new HashSet<DM_Truong>();
            this.HocSinhs = new HashSet<HocSinh>();
        }
    
        public string MaHuyen { get; set; }
        public string MaXa { get; set; }
        public string TenXa { get; set; }
        public string Ghichu { get; set; }
    
        public virtual ICollection<DM_KhoiPho> DM_KhoiPho { get; set; }
        public virtual DM_QuanHuyen DM_QuanHuyen { get; set; }
        public virtual ICollection<DM_Truong> DM_Truong { get; set; }
        public virtual ICollection<HocSinh> HocSinhs { get; set; }
    }
}
