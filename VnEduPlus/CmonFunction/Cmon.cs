﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
 
using System.Security.Cryptography;
using System.Text;
using System.Configuration;
using System.Data.Objects.DataClasses;
using System.Reflection;
using System.Data.SqlClient;
using System.Data;
using VnEduPlus.Models;
using System.Web.Mvc;

namespace VnEduPlus.CmonFunction
{
    public class Cmon
    {
        public static List<DM_MonHoc> listMonHoc = new List<DM_MonHoc>();
        public static List<DanhMucMonHoc> dmMonHoc = new List<DanhMucMonHoc>();
        public static List<string> bangTongHop = new List<string> (new string[] {"nangluc1", "nangluc2", "nangluc3", "phamchat1", "phamchat2", "phamchat3", "phamchat4","khenthuong", "htct"});
         
        public static List<DanhMucTieuChi> createDanhMuc() 
        {
            List<DanhMucTieuChi> list = new List<DanhMucTieuChi>();
            DanhMucTieuChi danhMucTieuChi = new DanhMucTieuChi();

            list.Add(new DanhMucTieuChi
            {
                id = -1,
                name = "-- Lựa chọn tiêu chí -- " 
            });

            list.Add(new DanhMucTieuChi
            {
                id = 1,
                name = "Kiến thức"
            });

            list.Add(new DanhMucTieuChi
            {
                id = 2,
                name = "Năng lực"
            });

            list.Add(new DanhMucTieuChi
            {
                id = 3,
                name = "Phẩm chất"
            });
            return list;

        }


        public static List<Month> createMonths(int idTruong, string namHoc, EDUNP_CAP1Entities db)
        {
            DateTime ngayDauNam = Cmon.getNgayDauNamHoc(idTruong, namHoc, db);//namHH.NgayBatDau.Value;
            DateTime ngayCuoiNam = Cmon.getNgayKetThucNamHoc(idTruong, namHoc, db);
            int fromMonth = ngayDauNam.Month;// lay thang dau tien cua nam hoc
            int namHK1 = ngayDauNam.Year;
            int endMonth = ngayCuoiNam.Month;
            int namHK2 = ngayCuoiNam.Year;

            var thangs = new List<Month>();// cua nam o HK 1

            thangs.Add(new Month
            {
                id = -1,
                name = "-- Lựa chọn tháng -- "
            });

            for (int i = fromMonth; i <= 12; i++)
            {
                thangs.Add(new Month() { id = i, name = "Tháng " + i.ToString() });
             }
            for (int i = 1; i < endMonth; i++)
            {
                thangs.Add(new Month() { id = i, name = "Tháng " + i.ToString() });
            }


            return thangs;
             
        
        }

        public static List<Month> createCacThangCoDinh(int idTruong, EDUNP_CAP1Entities db)
        {
            var thangs = new List<Month>();// cua nam o HK 1

            thangs.Add(new Month
            {
                id = -1,
                name = "-- Lựa chọn tháng -- "
            });

            for (int i = 8; i <= 12; i++)
            {
                thangs.Add(new Month() { id = i, name = "Tháng " + i.ToString() });
            }
            for (int i = 1; i < 6; i++)
            {
                thangs.Add(new Month() { id = i, name = "Tháng " + i.ToString() });
            }

            return thangs;

        }

        

        public static List<NangLuc> CreateNangLuc()
        {
            List<NangLuc> list = new List<NangLuc>();

            list.Add(new NangLuc
            {
                maNangLuc = "-1",
                tenNangLuc = "-- Lựa chọn năng lực -- "
            });
            
            list.Add(new NangLuc
            {
                maNangLuc = "NANGLUC1",
                tenNangLuc = "Tự phục vụ tự quảng"
            });


            list.Add(new NangLuc
            {
                maNangLuc = "NANGLUC2",
                tenNangLuc = "Giao tiếp, hợp tác"
            });

            list.Add(new NangLuc
            {
                maNangLuc = "NANGLUC3",
                tenNangLuc = "Tự học và giải quyết vấn đề"
            });

            
            return list;
        }

        public static List<PhamChat> CreatePhamChat()
        {
            List<PhamChat> list = new List<PhamChat>();

            list.Add(new PhamChat
            {
                maPhamChat = "-1",
                tenPhamChat = "-- Lựa chọn phẩm chất -- "
            });

            list.Add(new PhamChat
            {
                maPhamChat = "PHAMCHAT1",
                tenPhamChat = "Chăm học chăm làm"
            });


            list.Add(new PhamChat
            {
                maPhamChat = "PHAMCHAT2",
                tenPhamChat = "Tự trọng, tự tin, tự chịu trách nhiệm"
            });

            list.Add(new PhamChat
            {
                maPhamChat = "PHAMCHAT3",
                tenPhamChat = "Trung thực, kỹ luật, đoàn kết"
            });

            list.Add(new PhamChat
            {
                maPhamChat = "PHAMCHAT4",
                tenPhamChat = "Yêu gia đình, bạn bè"
            });


            return list;
        }

        public static List<HocKy> CreateHocKy()
        {
            List<HocKy> list = new List<HocKy>();

            list.Add(new HocKy
            {
                maHocKy = "-1",
                tenHocKy = "-- Lựa chọn học kỳ -- "
            });

            list.Add(new HocKy
            {
                maHocKy = "0",
                tenHocKy = "Học kỳ 1"
            });


            list.Add(new HocKy
            {
                maHocKy = "1",
                tenHocKy = "Cả năm"
            });

             
            return list;
        }

        public static List<HocKy> CreateHocKyTheoDanhGia()
        {
            List<HocKy> list = new List<HocKy>();

            list.Add(new HocKy
            {
                maHocKy = "-1",
                tenHocKy = "-- Lựa chọn học kỳ -- "
            });

            list.Add(new HocKy
            {
                maHocKy = "GHK1",
                tenHocKy = "Giữa Học kỳ 1"
            });

            list.Add(new HocKy
            {
                maHocKy = "HK1",
                tenHocKy = "Học kỳ 1"
            });

            list.Add(new HocKy
            {
                maHocKy = "GHK2",
                tenHocKy = "Giữa Học kỳ 2"
            });

            list.Add(new HocKy
            {
                maHocKy = "HK2",
                tenHocKy = "Cả năm"
            });


            return list;
        }


       

        public static void UpdateStatusCNNX(int loaiNhanXet, int thang, EDUNP_CAP1Entities db)
        {


            // Cập nhật lại toàn bộ trạng thái isDefault = false ngoại trừ bảng ghi này được set là true
            var dsNhanXet = (from kthuc in db.DM_NhanXetMau_GVCN.Where(t => t.isDefault == true && t.LoaiNhanXet == loaiNhanXet && t.Thang == thang) select kthuc).ToList();
            dsNhanXet.ForEach(a => { a.isDefault = false; });
             
            db.SaveChanges();

        }


        public static void UpdateStatusNhanXetTheoHocKy(int type,string maMonHoc, int hocKy, EDUNP_CAP1Entities db)
        {

             
                // Cập nhật lại toàn bộ trạng thái isDefault = false ngoại trừ bảng ghi này được set là true

                var dsNhanXet = (from kthuc in db.DM_NhanXetMau_CuoiKyNam.Where(t => t.isDefault == true && t.MaMonHoc == maMonHoc && t.LoaiNhanXet == type && t.HocKy == hocKy) select kthuc).ToList();

                dsNhanXet.ForEach(a => { a.isDefault = false; });

                db.SaveChanges();

        }


        public static bool isExitsMaHocSinh( int idTruong, string namHoc, string maLop, string maHS, EDUNP_CAP1Entities db) {
                
            var hocSinh = ( from hocsinh in db.HocSinh_DanhGiaThang_GVCN where hocsinh.idTruong == idTruong && hocsinh.NamHoc == namHoc && hocsinh.MaLop == maLop && hocsinh.MaHocSinh == maHS select hocsinh).FirstOrDefault();
            return hocSinh!=null ? true : false;
        }

        public static string getTenHocKy(int idTruong, string namHoc, string maHK, EDUNP_CAP1Entities db)
        {

            var hocKy = (from hk in db.DM_HocKy where hk.idTruong == idTruong && hk.NamHoc == namHoc && hk.MaHocKy == maHK select hk).FirstOrDefault();
            return hocKy != null ? hocKy.TenHocKy : "";
        }



        public static bool checkKhoaHocKy(int idTruong, string namHoc, string maLop, string maHK, EDUNP_CAP1Entities db)
        {
            bool isfixed = false;
            var khoaKy = (from kk in db.KhoaKies where kk.idTruong == idTruong && kk.NamHoc == namHoc && kk.MaLop == maLop && kk.MaHocKy == maHK select kk).FirstOrDefault();
            if (khoaKy != null)
            {
                isfixed = true;
            }

            return isfixed;
        }


        public static string getKhoi(string malop, string namhoc, int idTruong)
        {
            EDUNP_CAP1Entities db = new EDUNP_CAP1Entities();
            return db.LopHocs.Where(lop => lop.idTruong == idTruong && lop.NamHoc == namhoc && lop.MaLop == malop).SingleOrDefault().MaKhoi;

        }


		public static bool getRole(string userName, int idTruong, int functionID, EDUNP_CAP1Entities db) {
			var writerAccess_ = (from RU in db.ScrJRoleUsers
								 join RF in db.ScrJRoleFunctions
									 on RU.RoleName equals RF.RoleName
								 where RU.UserName == userName && RF.idTruong == idTruong
								 && RF.FunctionID == functionID
								 select RF).FirstOrDefault();
			bool writerAccess = (writerAccess_ == null || (writerAccess_ != null && writerAccess_.Update_ == false)) ? false : true;
			if (writerAccess)
			{
				return true;	
			}
			return false;
		}


        //-------Những hàm bên dưới sẽ phải được remove khi đã hoàn thành các chức năng cho cấp 1, vì các hàm bên dưới sẽ có nhiều hàm ko
        //sửa dụng đến
        
        

        public enum ExcelAlign { Center, Justify, left, right }
        public static T CopyEntity<T>(T clone, T entity, bool copyKeys = false) where T : EntityObject
        {
            
            PropertyInfo[] pis = entity.GetType().GetProperties();
            foreach (PropertyInfo pi in pis)
            {
                EdmScalarPropertyAttribute[] attrs = (EdmScalarPropertyAttribute[])pi.GetCustomAttributes(typeof(EdmScalarPropertyAttribute), false);

                foreach (EdmScalarPropertyAttribute attr in attrs)
                {
                    if (!copyKeys && attr.EntityKeyProperty)
                        continue;

                    pi.SetValue(clone, pi.GetValue(entity, null), null);
                }
            }

            return clone;
        }

        public static void stopExcel()
        {
            try
            {
                System.Diagnostics.Process[] ps = System.Diagnostics.Process.GetProcesses();
                foreach (System.Diagnostics.Process p in ps)
                {
                    if (p.ProcessName.ToLower().Equals("excel"))
                        p.Kill();
                }
            }
            catch
            {
            }

        }


        public  static string GetTenHocKy(string hocky)
        {
            string tenhocky = "";
            if (hocky == "K1" || hocky == "HK1")
            {
                tenhocky = "Học kỳ 1";
            }
            else if (hocky == "K2" || hocky == "HK2")
            {
                tenhocky = "Học kỳ 2";
            }
            else tenhocky = "Cả năm";
            return tenhocky;

        }

       
        public static string base64Encode(string data)
        {
            try
            {
                byte[] encData_byte = new byte[data.Length];
                encData_byte = System.Text.Encoding.UTF8.GetBytes(data);
                string encodedData = Convert.ToBase64String(encData_byte);
                return encodedData;
            }
            catch (Exception e)
            {
                throw new Exception("Error in base64Encode" + e.Message);
            }
        }


        public static string DecodeFrom64(string encodedData)
        {

            byte[] encodedDataAsBytes

                = System.Convert.FromBase64String(encodedData);

            string returnValue =

               System.Text.ASCIIEncoding.ASCII.GetString(encodedDataAsBytes);

            return returnValue;

        }

        /// <summary>
        /// get ten don vi chu quan cua truong  
        /// </summary>
        /// <param name="idTruong"></param>
        /// <returns></returns>
        public static string getDonViChuQuan(int idTruong)
        {

            EDUNP_CAP1Entities db = new EDUNP_CAP1Entities();
            int idparent = db.DM_Truong.Where(truong => truong.idTruong == idTruong).SingleOrDefault().idParent;
            return db.DM_Truong.Where(truong => truong.idTruong == idparent).SingleOrDefault().TenTruong;

        }
        /// <summary>
        /// ham nay thuc hien lay ra idtruong cua user dang nhap vao 
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static int getIdtruong(string userName, EDUNP_CAP1Entities db)
        {
            try
            {
                ScrUser userchange = (from us in db.ScrUsers
                                      where us.UserName == userName
                                      select us).FirstOrDefault();
                return userchange.idTruong != null ? (int)userchange.idTruong : 26;
            }
            catch( Exception e)
            {
                return 0;
            }
        }

        /// <summary>
        /// Hàm này sẽ có tác dụng kiểm tra xem một user có quyền như chúng ta cần kiểm tra hay không 
        /// ví dụ: isRight(GV) --> nếu đúng là GV thì sẽ cho kết quả đúng
        /// </summary>
        /// <param name="roleName"></param>
        /// <returns></returns>
        public static bool isRight(string userName, string roleName, EDUNP_CAP1Entities db)
        {
            var userRole = (from role in db.ScrJRoleUsers
                            where role.UserName == userName && role.RoleName == roleName
                            select role).FirstOrDefault();
            return userRole != null ? true: false;
        
        }

        public static bool isRightsHTAndGV(string userName, string roleGV, string roleHT, EDUNP_CAP1Entities db)
        {

            var userRole = (from role in db.ScrJRoleUsers
                            where role.UserName == userName
                            select role.RoleName).ToList();

            return userRole != null && userRole.Contains(roleGV) && userRole.Contains(roleHT) ? true : false;

        }

        public static bool isGVCN(string userName, string malop, EDUNP_CAP1Entities db)
        {
            var userRole = (from usrLop in db.Users_LopHoc
                             join rol in db.ScrJRoleUsers  on usrLop.UserName equals rol.UserName
                            where rol.UserName == userName && usrLop.MaLop == malop && rol.RoleName == "GV"
                            select usrLop).FirstOrDefault();
            return userRole != null ? true : false;

        }


        /// <summary>
        /// Id=37 là chức năng tạo năm học trong bảng chức năng(ScrFunction), mà hiệu trưởng có
        /// </summary>
        /// <param name="idTruong"></param>
        /// <param name="userName"></param>
        /// <returns></returns>
        public static bool isHieuTruong(int idTruong, string userName)
        {
            EDUNP_CAP1Entities db = new EDUNP_CAP1Entities();

            var query = (from roleUser in db.ScrJRoleUsers
                         join roleFunction in db.ScrJRoleFunctions
                         on roleUser.RoleName equals roleFunction.RoleName
                         where roleUser.UserName == userName && roleFunction.idTruong == idTruong && roleFunction.FunctionID == 37
                         select roleUser).FirstOrDefault();
            
            return (query!=null ? true: false);

        }
        public static string GetNamHienTai(int idtruong)
        {
            EDUNP_CAP1Entities db = new EDUNP_CAP1Entities();
            return db.DM_NamHoc.Where(nam => nam.idTruong==idtruong && nam.IsHienTai == true).SingleOrDefault().NamHoc;
        }

        public static string GetnameSchool(int idtruong)
        {
            EDUNP_CAP1Entities db = new EDUNP_CAP1Entities();
            return db.DM_Truong.Where(truong => truong.idTruong == idtruong).SingleOrDefault().TenTruong;
          
        }


        public static bool checkLoaiNhapDiemCuaTruong(int idtruong) 
        {
            EDUNP_CAP1Entities db = new EDUNP_CAP1Entities();
            
            return db.DM_Truong.Where(truong=>truong.idTruong == idtruong).SingleOrDefault().isKhoaCotDiem;
        }

        public static bool checkFormNhapDiemCuaTruong(int idtruong)
        {
            bool flag = false;
            EDUNP_CAP1Entities db = new EDUNP_CAP1Entities();
            var styleForm = db.DM_Truong_Details.Where(truong => truong.idTruong == idtruong).SingleOrDefault();            
            if (styleForm == null  || styleForm.SimpleForm == true)
            {
                flag = true;

            }
            return flag;
        }


        
        public static bool checkShowOrHiddenDTBMCuaTruong(int idtruong)
        {
            EDUNP_CAP1Entities db = new EDUNP_CAP1Entities();
            bool flag = false;
            var tongKetDiem = db.DM_Truong_Details.Where(truong => truong.idTruong == idtruong).SingleOrDefault();
            if (tongKetDiem == null || tongKetDiem.TongKetKhiDuDiem == true)
            {
                flag = true;

            }
            return flag;
             
        }


        //public static string GetnameClass(string malop)
        //{
        //    EDUNP_CAP1Entities db = new EDUNP_CAP1Entities();
        //    return db.LopHocs.Where(lop => lop.MaLop == malop).SingleOrDefault().TenLop;

        //}
        public static string GetnameClass(string malop, int idtruong, string namhoc)
        {
            EDUNP_CAP1Entities db = new EDUNP_CAP1Entities();
            return db.LopHocs.Where(lop => lop.MaLop== malop && lop.idTruong==idtruong && lop.NamHoc==namhoc).SingleOrDefault().TenLop;

        }

        public static string GetnameUserName(string username, int idTruong)
        {
            EDUNP_CAP1Entities db = new EDUNP_CAP1Entities();
            return db.ScrUsers.Where(ten => ten.UserName == username && ten.idTruong == idTruong).SingleOrDefault().FullName;

        }


        public static string getHoTenHocSinh(string mahocsinh, int idtruong,string malop, string namhoc)
        {
            EDUNP_CAP1Entities db = new EDUNP_CAP1Entities();
            return db.HocSinhs.Where(hs => hs.idTruong == idtruong
                && hs.MaLop == malop && hs.NamHoc == namhoc && hs.MaHocSinh == mahocsinh).SingleOrDefault().HoTen;
        }


        public static string GetnameMonhoc(string mamon)
        {
            EDUNP_CAP1Entities db = new EDUNP_CAP1Entities();
            return db.DM_MonHoc.Where(monhoc => monhoc.MaMonHoc == mamon).SingleOrDefault().TenMonHoc;

        }

        public static string getHocKyHienTai(DateTime ngayhientai, int idtruong, string namhoc)
        { 
            string kyhientai="";
            EDUNP_CAP1Entities db = new EDUNP_CAP1Entities();
            var listky1 = db.DM_HocKy.Where(hocky => hocky.idTruong == idtruong && hocky.NamHoc == namhoc && (hocky.MaHocKy == "K1" || hocky.MaHocKy == "HK1"||hocky.MaHocKy == "HKI" )
                && hocky.NgayBatDau <= ngayhientai && ngayhientai <= hocky.NgayKetThuc).ToList();

            var listky2 = db.DM_HocKy.Where(hocky => hocky.idTruong == idtruong && hocky.NamHoc == namhoc && (hocky.MaHocKy == "K2" || hocky.MaHocKy == "HK2" || hocky.MaHocKy == "HKII")
                && hocky.NgayBatDau <= ngayhientai && ngayhientai <= hocky.NgayKetThuc).ToList();
            
            if (listky1.Count > 0) kyhientai = "HKI";

            if (listky2.Count > 0) kyhientai = "HKII";
            return kyhientai;
        
        }

       
        

        public static DateTime getNgayDauNamHoc(int idtruong, string namhoc, EDUNP_CAP1Entities db)
        {

            return (DateTime)db.DM_HocKy.Where(hk => hk.idTruong == idtruong && hk.NamHoc == namhoc && (hk.MaHocKy == "HK1" || hk.MaHocKy == "HKI" || hk.MaHocKy == "K1")).FirstOrDefault().NgayBatDau;

        }

        public static DateTime getNgayKetThucNamHoc(int idtruong, string namhoc, EDUNP_CAP1Entities db)
        {

            return (DateTime)db.DM_HocKy.Where(hk => hk.idTruong == idtruong && hk.NamHoc == namhoc && (hk.MaHocKy == "HK2" || hk.MaHocKy == "HKII" || hk.MaHocKy == "K2")).FirstOrDefault().NgayKetThuc;

        }

        public static string dayOfWeek(DateTime adSource)
        {

            // Calender Functionality cloned by used defined logic
            DayOfWeek dWeek = adSource.DayOfWeek;

            switch (dWeek)
            {

                case DayOfWeek.Monday:
                    return "T2";

                case DayOfWeek.Tuesday:
                    return "T3";

                case DayOfWeek.Wednesday:
                    return "T4";

                case DayOfWeek.Thursday:
                    return "T5";

                case DayOfWeek.Friday:
                    return "T6";

                case DayOfWeek.Saturday:
                    return "T7";

                case DayOfWeek.Sunday:
                    return "CN";

            }

            return "CN";

        }

         
        public static string GetnameUser(string username)
        {
            EDUNP_CAP1Entities db = new EDUNP_CAP1Entities();
            return db.ScrUsers.Where(user => user.UserName == username).SingleOrDefault().FullName;

        }

        // Tull
        public static string Encrypt(string toEncrypt, bool useHashing)
        {
            byte[] keyArray;
            byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(toEncrypt);

            System.Configuration.AppSettingsReader settingsReader = new AppSettingsReader();
            // Get the key from config file
            string key = (string)settingsReader.GetValue("SecurityKey", typeof(String));
            //System.Windows.Forms.MessageBox.Show(key);
            if (useHashing)
            {
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
                hashmd5.Clear();
            }
            else
                keyArray = UTF8Encoding.UTF8.GetBytes(key);

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = keyArray;
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateEncryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
            tdes.Clear();
            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }

       

        public static string genPassword(string userLogIn, string pass)
        {
            System.Configuration.AppSettingsReader settingsReader = new AppSettingsReader();
            string key = (string)settingsReader.GetValue("SecurityKey", typeof(String));
            string info = userLogIn + pass + key;
            string mk = Encrypt(info, true);
            return mk;
        }

        /// <summary>
        /// Tinhs so ngay trong thang
        /// </summary>
        /// <param name="thang"></param>
        /// <returns></returns>
        public static int songaytrongthang(int thang,int nam)
        {
            int songay = 0;
            if (thang == 2)
            {
                if ((nam % 4 == 0 && nam % 100 != 0) || (nam % 400 == 0))
                    songay = 29;
                else
                    songay = 28;
            }
            else
            {

                switch (thang)
                {
                    case 1: songay = 31; break;
                    case 3: songay = 31; break;
                    case 5: songay = 31; break;
                    case 7: songay = 31; break;
                    case 8: songay = 31; break;
                    case 10: songay = 31; break;
                    case 12: songay = 31; break;
                    case 4: songay = 30; break;
                    case 6: songay = 30; break;
                    case 9: songay = 30; break;
                    case 11: songay = 30; break;
                    default: break;
                }
            
            }
            return songay;

        }
         
        
    }

    public class CotDiem
    {
        public string Value { get; set; }
        public string Text { get; set; }
    }


    /// <summary>
    /// Class này chính là khởi tạo ADO để thực thi database
    /// </summary>
    public class DBHelper
    {
        string connectionString;
        SqlConnection connection1;
        public DBHelper()
        {
            connectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnectionStr"].ToString();
        }
        public DBHelper(string connectionStr)
        {
            connectionString = connectionStr;
        }

        public SqlConnection Connect()
        {
            try
            {
                connection1 = new SqlConnection(connectionString);
                connection1.Open();
                return connection1;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public void ConnectClose()
        {
            connection1.Close();
        }

        public DataSet ExecuteDataset(string commandText, string tbname)
        {
            try
            {
                SqlDataAdapter adapter1 = new SqlDataAdapter(commandText, Connect());
                DataSet set1 = new DataSet();
                adapter1.Fill(set1, tbname);
                return set1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ConnectClose();
            }
        }

        public DataSet ExecuteDatasetPRO(string proname, SqlParameter[] sqlparam)
        {
            try
            {
                SqlDataAdapter cmd = new SqlDataAdapter(proname, Connect());
                cmd.SelectCommand.CommandType = CommandType.StoredProcedure;
                //cmd.SelectCommand.CommandTimeout = 0;
                foreach (SqlParameter Sparam in sqlparam)
                {
                    cmd.SelectCommand.Parameters.Add(Sparam);
                }
               
                DataSet myDataSet = new DataSet();
                cmd.Fill(myDataSet);
                return myDataSet;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ConnectClose();
            }
        }
        public string ExecutePro(string proname, SqlParameter[] sqlparam, string returnparam)
        {
            try
            {
                SqlCommand command1 = new SqlCommand(proname, Connect());
                command1.CommandType = CommandType.StoredProcedure;
                command1.CommandTimeout = 0;
                foreach (SqlParameter parameter1 in sqlparam)
                {
                    command1.Parameters.Add(parameter1);
                }
                command1.ExecuteNonQuery();
                return command1.Parameters[returnparam].Value.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ConnectClose();
            }
        }

         
    }
}