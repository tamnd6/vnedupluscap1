﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace HNAFramework.DataAccess.Framework
{
    
    #region DBHelper
    public class DBHelper 
    {
        private static string connectionString = ConfigurationManager.ConnectionStrings["chuoiKetnoi"].ConnectionString;
        //private static string connectionString = "";
        //string connectionString;
        SqlConnection connection1;
        /// <summary>
        /// Auto get connectionstring
        /// </summary>
        public DBHelper(string strConnection)
        {
            connectionString = strConnection;
        }

        public DBHelper()
        {
        }
        //public DBHelper(string connectionStr)
        //{
        //    connectionString = connectionStr;
        //}

        public SqlConnection Connect()
        {
            try
            {
                connection1 = new SqlConnection(connectionString);
                connection1.Open();
                return connection1;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public void ConnectClose()
        {
            connection1.Close();
        }

        public void RunGivenSQLScript(string scriptFile)
        {
            SqlConnection sqlcon = new SqlConnection(connectionString);
            sqlcon.Open();
            SqlCommand sqlcmd = new SqlCommand();
            sqlcmd.Connection = sqlcon;
            sqlcmd.CommandType = CommandType.Text;
            StreamReader reader = null;
            reader = new StreamReader(scriptFile);
            string script = reader.ReadToEnd();
            try
            {
                ExecuteLongSql(sqlcon, script);
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                reader.Close();
                sqlcon.Close();
            }

        }

        public void ExecuteLongSql(SqlConnection connection, string Script)
        {
            string sql = "";
            sql = Script;
            Regex regex = new Regex("^GO", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            string[] lines = regex.Split(sql);
            SqlTransaction transaction = connection.BeginTransaction();
            using (SqlCommand cmd = connection.CreateCommand())
            {
                cmd.Connection = connection;
                cmd.Transaction = transaction;
                foreach (string line in lines)
                {

                    if (line.Length > 0)
                    {
                        cmd.CommandText = line;
                        cmd.CommandType = CommandType.Text;
                        try
                        {
                            cmd.ExecuteNonQuery();
                        }
                        catch
                        {
                            transaction.Rollback();
                            throw;
                        }
                    }
                }
            }
            transaction.Commit();
        }

        public DataSet ExecuteDataset(string commandText, string tbname)
        {
            DataSet set1 = new DataSet();
            try
            {
                SqlDataAdapter adapter1 = new SqlDataAdapter(commandText, Connect());
                
                adapter1.Fill(set1, tbname);
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ConnectClose();
            }
            return set1;
        }

        public DataSet ExecuteDatasetPRO(string proname, SqlParameter[] sqlparam)
        {
            DataSet myDataSet = new DataSet();
            try
            {
                SqlDataAdapter cmd = new SqlDataAdapter(proname, Connect());
                cmd.SelectCommand.CommandType = CommandType.StoredProcedure;
                cmd.SelectCommand.CommandTimeout = 0;
                foreach (SqlParameter Sparam in sqlparam)
                {
                    cmd.SelectCommand.Parameters.Add(Sparam);
                }
                
                cmd.Fill(myDataSet);
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ConnectClose();
            }
            return myDataSet;
        }
        public int ExecuteSqlQuery(string sqlquery)
        {
            try
            {
                SqlCommand command1 = new SqlCommand(sqlquery, Connect());
                return command1.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ConnectClose();
            }
        }

        public DataTable GetDataTabel(string proname, SqlParameter[] sqlparam)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlDataAdapter cmd = new SqlDataAdapter(proname, Connect());
                cmd.SelectCommand.CommandType = CommandType.StoredProcedure;
                cmd.SelectCommand.CommandTimeout = 0;
                foreach (SqlParameter Sparam in sqlparam)
                {
                    cmd.SelectCommand.Parameters.Add(Sparam);
                }

                cmd.Fill(dt);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ConnectClose();
            }
            return dt;
        }

        public DataTable GetData(string sqlQuery)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlDataAdapter cmd = new SqlDataAdapter(sqlQuery, Connect());
                cmd.SelectCommand.CommandTimeout = 1800;
                cmd.Fill(dt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ConnectClose();
            }

            return dt;
        }
        public string ExecutePro(string proname, SqlParameter[] sqlparam, string returnparam)
        {
            try
            {
                SqlCommand command1 = new SqlCommand(proname, Connect());
                command1.CommandType = CommandType.StoredProcedure;
                command1.CommandTimeout = 0;
                foreach (SqlParameter parameter1 in sqlparam)
                {
                    command1.Parameters.Add(parameter1);
                }
                command1.ExecuteNonQuery();
                return command1.Parameters[returnparam].Value.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ConnectClose();
            }
        }

        public string ExecutePro(string proname, SqlParameter[] sqlparam)
        {
            try
            {
                SqlCommand command1 = new SqlCommand(proname, Connect());
                command1.CommandType = CommandType.StoredProcedure;
                command1.CommandTimeout = 0;
                foreach (SqlParameter parameter1 in sqlparam)
                {
                    command1.Parameters.Add(parameter1);
                }
                return command1.ExecuteNonQuery().ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ConnectClose();
            }
        }
    }
    #endregion

    #region Commons
    public static class Commons
    {
        //public static List<TSource> ToList<TSource>(this DataTable dataTable) where TSource : new()
        //{
        //    var dataList = new List<TSource>();

        //    const BindingFlags flags = BindingFlags.Public | BindingFlags.Instance | BindingFlags.NonPublic;
        //    var objFieldNames = (from PropertyInfo aProp in typeof(TSource).GetProperties(flags)
        //                         select new
        //                         {
        //                             Name = aProp.Name,
        //                             Type = Nullable.GetUnderlyingType(aProp.PropertyType) ??
        //                     aProp.PropertyType
        //                         }).ToList();
        //    var dataTblFieldNames = (from DataColumn aHeader in dataTable.Columns
        //                             select new
        //                             {
        //                                 Name = aHeader.ColumnName,
        //                                 Type = aHeader.DataType
        //                             }).ToList();
        //    var commonFields = objFieldNames.Intersect(dataTblFieldNames).ToList();

        //    foreach (DataRow dataRow in dataTable.AsEnumerable().ToList())
        //    {
        //        var aTSource = new TSource();
        //        foreach (var aField in commonFields)
        //        {
        //            PropertyInfo propertyInfos = aTSource.GetType().GetProperty(aField.Name);
        //            var value = (dataRow[aField.Name] == DBNull.Value) ?
        //            null : dataRow[aField.Name]; //if database field is nullable
        //            propertyInfos.SetValue(aTSource, value, null);
        //        }
        //        dataList.Add(aTSource);
        //    }
        //    return dataList;
        //}

        //public List<T> ConvertToList<T>(DataTable dt)
        //{
        //    var columnNames = dt.Columns.Cast<DataColumn>()
        //        .Select(c => c.ColumnName)
        //        .ToList();
        //    var properties = typeof(T).GetProperties();
        //    return dt.AsEnumerable().Select(row =>
        //    {
        //        var objT = Activator.CreateInstance<T>();
        //        foreach (var pro in properties)
        //        {
        //            if (columnNames.Contains(pro.Name))
        //                pro.SetValue(objT, row[pro.Name]);
        //        }
        //        return objT;
        //    }).ToList();
        //}

    }
    #endregion

    #region IniFile
    class IniFile
    {
        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section, string key, string val, string filePath);

        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retVal, int size, string filePath);

        public static void IniWriteValue(string path, string Key, string Value,string title)
        {
            WritePrivateProfileString(title, Key, Value, path);
        }

        public static string IniReadValue(string path, string Key,string title)
        {
            StringBuilder temp = new StringBuilder(255);
            int i = GetPrivateProfileString(title, Key, "", temp, 255, path);
            return temp.ToString();
        }
    }
    #endregion

    #region Writelog
    public class HistoryLogger
    {
        static System.Threading.Semaphore locking = new System.Threading.Semaphore(1, 1);
        public static void WriteLog(string message)
        {
            try
            {
                locking.WaitOne();
                StreamWriter log;
                string _path = System.AppDomain.CurrentDomain.BaseDirectory;

                string folder = _path + "/LogFolder";

                bool folderExists = Directory.Exists(folder);
                if (!folderExists)
                    Directory.CreateDirectory(folder);

                string fileName = folder + "/" + DateTime.Today.ToString("yyyyMMdd") + ".txt";
                if (!File.Exists(fileName))
                    log = new StreamWriter(fileName);
                else
                    log = File.AppendText(fileName);
                log.WriteLine(message);
                //log.WriteLine("------------------------------------------------------------");
                log.Flush();
                log.Close();

            }

            catch (Exception)
            {

            }

            finally
            {

                locking.Release();

            }

        }

    }
    #endregion
}