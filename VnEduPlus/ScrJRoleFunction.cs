//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace VnEduPlus
{
    using System;
    using System.Collections.Generic;
    
    public partial class ScrJRoleFunction
    {
        public int idTruong { get; set; }
        public string RoleName { get; set; }
        public int FunctionID { get; set; }
        public bool Enable { get; set; }
        public bool Read_ { get; set; }
        public bool Update_ { get; set; }
        public bool Insert_ { get; set; }
        public bool Delete_ { get; set; }
    
        public virtual DM_Truong DM_Truong { get; set; }
        public virtual ScrFunction ScrFunction { get; set; }
    }
}
