[T4Scaffolding.Scaffolder(Description = "Enter a description of MyView here")][CmdletBinding()]
param(        
    [string]$Controller,
    [string]$ViewName="Index",	
	[string]$ModelType=$Controller,
	[alias("MasterPage")]$Layout,	# If not set, we'll use the default layout
	[string[]]$SectionNames,
	[string]$PrimarySectionName,
	[switch]$ReferenceScriptLibraries = $false,
    [string]$Project,
	[string]$CodeLanguage,
	[string[]]$TemplateFolders,
	[switch]$Force = $false

)

# Ensure we have a controller name, plus a model type if specified
if ($ModelType) {
	$foundModelType = Get-ProjectType $ModelType -Project $Project
	if (!$foundModelType) { return }
	$primaryKeyName = [string](Get-PrimaryKey $foundModelType.FullName -Project $Project)
}

$controllerFolder = $Controller.Replace("DM_","").ToLower() ;
$controllerFolder = $controllerFolder.Substring(0,1).ToUpper() + $controllerFolder.Substring(1);

# Decide where to put the output
$outputFolderName = Join-Path Views $controllerFolder

if ($foundModelType) { $relatedEntities = [Array](Get-RelatedEntities $foundModelType.FullName -Project $project) }
if (!$relatedEntities) { $relatedEntities = @() }

# Render the T4 template, adding the output to the Visual Studio project
$outputPath = Join-Path $outputFolderName $ViewName

Add-ProjectItemViaTemplate $outputPath -Template MyViewTemplate -Model @{
ModelType = [MarshalByRefObject]$foundModelType; 
		PrimaryKey = [string]$primaryKey; 
		RelatedEntities = $relatedEntities;	
} -SuccessMessage "Added $ViewName view at '{0}'" -TemplateFolders $TemplateFolders -Project $Project -CodeLanguage $CodeLanguage -Force:$Force

