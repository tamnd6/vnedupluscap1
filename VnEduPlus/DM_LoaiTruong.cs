//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace VnEduPlus
{
    using System;
    using System.Collections.Generic;
    
    public partial class DM_LoaiTruong
    {
        public DM_LoaiTruong()
        {
            this.DM_Truong = new HashSet<DM_Truong>();
        }
    
        public string MaLoaiTruong { get; set; }
        public string TenLoaiTruong { get; set; }
    
        public virtual ICollection<DM_Truong> DM_Truong { get; set; }
    }
}
